<?php

namespace Civi\Core\Event;

use Symfony\Contracts\EventDispatcher\Event;

/**
 * Triggered from \CRM_Core_Error::statusBounce when an action wants to set a message and redirect / bounce back to the referrer
 * Event: civi.error.statusBounce
 *
 * Class StatusBounceEvent
 * @package Civi\Core\Event
 */
class StatusBounceEvent extends Event {
  /**
   * The status message set
   * @var string
   */
  protected $status;

  /**
   * URL to redirect to
   * @var string
   */
  protected $redirect;

  /**
   * Title for the error message box
   * @var string
   */
  protected $title;

  /**
   * @return string
   */
  public function getStatus() {
    return $this->status;
  }

  /**
   * @return string
   */
  public function getRedirect() {
    return $this->redirect;
  }

  /**
   * @return string
   */
  public function getTitle() {
    return $this->title;
  }

  /**
   * @param $status
   * @param $redirect
   * @param $title
   */
  public function __construct($status, $redirect, $title) {
    $this->status = $status;
    $this->redirect = $redirect;
    $this->title = $title;
  }

}
