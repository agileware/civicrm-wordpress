<?php

/**
 * Pdf.Createmulti API specification (optional)
 * This is used for documentation and validation.
 *
 * @param array $spec description of fields supported by this API call
 * @return void
 * @see http://wiki.civicrm.org/confluence/display/CRM/API+Architecture+Standards
 */
function _civicrm_api3_pdf_Createmulti_spec(&$spec) {
  $spec['contact_id'] = [
    'name' => 'contact_id',
    'title' => 'contact ID',
    'description' => 'ID of the CiviCRM contact',
    'api.required' => 0,
    'type' => CRM_Utils_Type::T_INT,
  ];
  $spec['to_email'] = [
    'name' => 'to_email',
    'title' => 'to email address',
    'description' => 'the e-mail address the PDF will be sent to',
    'api.required' => 0,
    'type' => CRM_Utils_Type::T_STRING,
  ];
  $spec['cc_email'] = [
    'name' => 'cc_email',
    'title' => 'cc email address',
    'description' => 'the CC e-mail address the PDF will be sent to',
    'api.required' => 0,
    'type' => CRM_Utils_Type::T_STRING,
  ];
  $spec['bcc_email'] = [
    'name' => 'bcc_email',
    'title' => 'bcc email address',
    'description' => 'the BCC e-mail address the PDF will be sent to',
    'api.required' => 0,
    'type' => CRM_Utils_Type::T_STRING,
  ];
  $spec['from_email'] = [
    'name' => 'from_email',
    'title' => 'from email address',
    'description' => 'the e-mail address the PDF will be sent from',
    'api.required' => 0,
    'type' => CRM_Utils_Type::T_STRING,
  ];
  $spec['from_name'] = [
    'name' => 'from_name',
    'title' => 'from email name',
    'description' => 'the e-mail name the PDF will be sent from',
    'api.required' => 0,
    'type' => CRM_Utils_Type::T_STRING,
  ];
  $spec['body_template_id'] = [
    'name' => 'body_template_id',
    'title' => 'template ID email body',
    'description' => 'ID of the template that will be used for the email body',
    'api.required' => 0,
    'type' => CRM_Utils_Type::T_INT,
  ];
  $spec['email_subject'] = [
    'name' => 'email_subject',
    'title' => 'Email subject',
    'description' => 'Subject of the email that sends the PDF',
    'api.required' => 0,
    'type' => CRM_Utils_Type::T_STRING,
  ];
  $spec['pdf_activity'] = [
    'name' => 'pdf_activity',
    'title' => 'Print PDF activity?',
    'description' => 'Log Print PDF activity for contact?',
    'api.required' => 0,
    'type' => CRM_Utils_Type::T_BOOLEAN,
  ];
  $spec['email_activity'] = [
    'name' => 'email_activity',
    'title' => 'Email activity?',
    'description' => 'Log Email activity for contact?',
    'api.required' => 0,
    'type' => CRM_Utils_Type::T_BOOLEAN,
  ];
  $spec['combine'] = [
    'name' => 'combine',
    'title' => 'Combine PDFs into one?',
    'description' => 'Combine the pdfs into one file?',
    'api.required' => 0,
    'type' => CRM_Utils_Type::T_BOOLEAN,
  ];
  $spec['pdf_files'] = [
    'name' => 'pdfs',
    'title' => 'PDFs',
    'description' => 'array/json data containing the following: [{contact_id, template_id, case_id}]',
    'api.required' => 1,
    'type' => CRM_Utils_Type::T_TEXT,
  ];
  $spec['skip_send_email'] = [
    'name' => 'skip_send_email',
    'title' => 'Skip sending email?',
    'description' => 'Avoid send email in case that you are not setting',
    'api.required' => 0,
    'type' => CRM_Utils_Type::T_BOOLEAN,
  ];
}

/**
 * Pdf.Createmulti API
 *
 * @param array $params
 * @return array API result descriptor
 * @see civicrm_api3_create_success
 * @see civicrm_api3_create_error
 */
function civicrm_api3_pdf_Createmulti($params) {
  $pdf_files = $params['pdf_files'];
  if (is_string($pdf_files)) {
    $pdf_files = json_decode($pdf_files, JSON_OBJECT_AS_ARRAY);
  }
  $pdf = new CRM_Pdfapi_Pdf($params);
  foreach($pdf_files as $pdf_file) {
    $pdf->create($pdf_file);
  }
  $combine = isset($params['combine']) && $params['combine'] ? true : false;
  $pdf->processPdf($combine, $params);
  if ($pdf->hasErrors()) {
    return civicrm_api3_create_error(implode("\r\n", $pdf->getErrors()));
  }
  return civicrm_api3_create_success($pdf->getCreatedPdf(), $params, 'Pdf', 'Create');
}
