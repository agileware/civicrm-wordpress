<?php
/**
 * Copyright (C) 2021  Jaap Jansma (jaap.jansma@civicoop.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace Civi\Pdfapi;

use Civi\ActionProvider\Action\AbstractAction;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use CRM_Pdfapi_ExtensionUtil as E;

class CompilerPass implements CompilerPassInterface {

  /**
   * You can modify the container here before it is dumped to PHP code.
   */
  public function process(ContainerBuilder $container) {
    if ($container->hasDefinition('action_provider')) {
      $actionTypeFactoryDefinition = $container->getDefinition('action_provider');
      $actionTypeFactoryDefinition->addMethodCall('addAction', [
        'Pdfapi_CreatePdf',
        '\Civi\Pdfapi\ActionProvider\CreatePdf',
        E::ts('Communication: Create PDF (PDF API Version)'),
        [AbstractAction::SINGLE_CONTACT_ACTION_TAG, AbstractAction::SEND_MESSAGES_TO_CONTACTS]
      ]);
    }
  }


}
