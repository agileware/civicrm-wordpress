<?php
/*
 +--------------------------------------------------------------------+
 | Copyright CiviCRM LLC. All rights reserved.                        |
 |                                                                    |
 | This work is published under the GNU AGPLv3 license with some      |
 | permitted exceptions and without any warranty. For full license    |
 | and copyright information, see https://civicrm.org/licensing       |
 +--------------------------------------------------------------------+
 */

use CRM_Pdfapi_ExtensionUtil as E;

return [
  'pdfapi_pregreplace' => [
    'name' => 'pdfapi_pregreplace',
    'type' => 'String',
    'html_type' => 'text',
    'default' => '',
    'is_domain' => 1,
    'is_contact' => 0,
    'title' => E::ts('Find/Replace on template before PDF generation.'),
    'description' => E::ts('Optional regex to modify template before rendering PDF. For example to convert image URL to path on server: "/https:\/\/www\.example\.org(\/.*\.(?:png|jpg|css))/;/home/example/www$1"'),
    'html_attributes' => [
      'size' => 100,
    ],
    'settings_pages' => [
      'pdfapi' => [
         'weight' => 10,
      ]
    ],
  ],
];
