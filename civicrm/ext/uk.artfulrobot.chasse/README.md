# Chassé Supporter Journeys

![Screenshot](/docs/img/chasse-overview.png)

Get your supporters chassé-ing through an automated email-driven donor journey
to improve your organisation's onboarding experience.

Please see full documentation at [docs.civicrm.org](https://docs.civicrm.org/chasse/en/latest/).
## Changelog

### v2.3.1

- civix upgrade
- documentation/notes/translations.

### v2.3.0

- Can now associate a journey with a campaign (if CiviCampaign is enabled). Thanks @kurund

### v2.2.0

- CSS/layout fix for #29 that prevented select2s working under certain themes/environments.
- New feature (Thanks to Sandor Semsey for contributing!): you can now set different Reply To addresses for each step. (It defaults to the From address.)
- Tested with CiviCRM 5.57

### v2.1.1

No functional changes, just a civix upgrade meaning it works with PHP 7.4 (and 8).

### v2.1

- Bugfix in CiviRules. Thanks @semseysandor for identifying and fixing, thanks @ufundo for testing.

- Allow uses with the `edit message templates` permission to administer
  Chassé via the Mailings » Chassé Supporter Journeys screen. Thanks
  @ufundo for this.

### v2.0

Various feature improvements (per-person delays rather than weekly schedule for example) and CiviRules integration.
