Version 2.3.2
============

* Fixed issue with order of price set options.
* Added handling of a break exception.

Version 2.3.1
============

* Fixed regression bug #92: TypeError: array_merge

Version 2.3.0
============

* Improvement to calculation functionality.

Version 2.2.1
============

* Fixed regression issue around using validation in the calculation.

Version 2.2.0
============

* Make use of the parameters of the actions in validation and calcuation. See #89 and !87
* Fixed regression with multi-value parameter mappings.
* Fixed #88 Removing a condition from an action not possible. See !84
* Fixed #90: export and import of calucate actions
* Fixed #91: Performance issue when saving and editing a form processor. See !89

Version 2.1.6
============

* Fixed issue price set options field type and in form processor calculation.

Version 2.1.5
============

* Added Price Set Option Field Type
* Added default values for input configuration.

Version 2.1.4
============

* Fixed issue with action: Stop Execution. Which did not behave correctly anymore.
* Removed a htmlentities modifier (not supported by Smarty5)

Version 2.1.3
============

* Fix for issue #82. Configuration of actions with multiple options did not work correctly anymore. Such Contact: Update Subscriptions.
* Fixed issue with compatibility with Smarty5.

Version 2.1.2
============

* Fix for issue #83. Option Group type and normalizing values.

Version 2.1.1
============

* Fixed minor issue around calculations

Version 2.1.0
============

* Save form processor when checking or unchecking of retrieval of default data.
* Fixed you have unsaved changes messages upon saving a form processor.
* Fixed issue with Yes/No option list type and value label 0 for the No.
* Added error handling in Date input type.
* Added possibility to remove of leading and trailing spaces at short text types.
* Added Formatted Inputs back to the mapping.
* Replace jQuery by CRM.$ so it also works with Joomla (the name of a forms processor can now be edited in Joomla, just as in Drupal)
* Refactor of Runner so that the retrieval of defaults also executes delayed actions
* Enable parameters with NULL values (before this release they were removed)
* Make multiple values possible for country, useful for custom fields

Version 2.0.11
============

* Fixed regression issue from #79. Where editring default data actions gives a fatal error.

Version 2.0.10
============
* Fixed issue with saving delays on default data, calculation and validation.
* Fixed issue with try out and validation of required fields during load defaults.
* Fixed issue 79: make function to get provider in configurationaction public so can be picked up by form hooks
* Fixed PHP 7 compatability. See #78 and !77. Thanks to @dotu
* Fixed issue with retrieval of defaults.

Version 2.0.9
============

* Make OptionValueType resilient to null values. See #76
* Fixed issue with Try out and retrieving of defaults. Defaults where retrieved when the form was submitted. Now the defaults are retrieved when the button Load is pressed.
* Accept a Blob as a valid datatype.
* Report on an Error from an action in the same way as an exception.
* Updated docs with new screen prints and info on try-out, import, export, validation and calculation.
* Do not check include in formatted parameters as default
* Fixed issue with adding validations to inputs.
* Fixed error when hitting cancel on validator. See #74

Version 2.0.8
============

* Fix for issue with retrieval of defaults where not all custom fields are visible.

Version 2.0.7
============

* Fixed tryout functionality.

Version 2.0.6
============

* Fixed issue when an  input type does not exists anymore.

Version 2.0.5
============

* Fix for #70: Adding Custom Options field does not save values. See !72

Version 2.0.4
============

* Fixed regression bug during import.

Version 2.0.3
============

* Fixed issue with redirect.

Version 2.0.2
============

* Fixed regression bug. See #67

Version 2.0.1
============

* Add addFormsProcessorType to TypeFactory. This makes it possible that extensions can add their own types to the forms processor.

Version 2.0.0
============

* Added Contact Entity Type. Which is compatibile for retrieving contact entities from form builder.
* Added API version 4 entities for the form processor database tables.
* Added Dynamic Inputs functionality.
* Added an Event Price Input type. This input shows a list of price options for a given event. This input could be populated with retrieval
  of default data.
* Added Calculation functionality. So that a form processor can do calculations on the input data and return this
  to the calling application. For example on your remote webform you have an event signup form at which one can register for an event
  and provide how many people are coming. This calculation functionality can then be used to calculated the total amount due and show this
  to the user.
* Added API for retrieving the possible output fields of a form processor. `FormProcessorInstance`.`get_output`
  with parameter `form_processor_name` will give a list of fields outputted by the form processor.
* The output of Send Everything is also restructured. Instead of returning array into an array (e.g. action[get_contact][id] = 123 ) it is now
  a one dimensional array (e.f. action.get_contact.id = 123)
* New user interface
