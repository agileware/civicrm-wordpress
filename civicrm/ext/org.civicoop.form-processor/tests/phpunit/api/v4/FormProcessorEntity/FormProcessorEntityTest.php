<?php
namespace api\v4\EckEntity;

use Civi\Api4\CustomField;
use Civi\Api4\CustomGroup;
use Civi\Api4\EckEntityType;
use Civi\Api4\Entity;
use Civi\Api4\OptionValue;
use Civi\Test\HeadlessInterface;

/**
 * @group headless
 */
class FormProcessorEntityTest extends \PHPUnit\Framework\TestCase implements HeadlessInterface {

  public function setUpHeadless() {
    return \Civi\Test::headless()
      ->installMe(__DIR__)
      ->apply();
  }

  public function testCreateEntityType():void {
    // TODO: APIv4
    $instance = civicrm_api3('FormProcessorInstance', 'create', [
      'name' => 'my_processor',
      'title' => 'My Processor',
      'is_active' => 1,
      'description' => 'Test processor entity',
      'output_handler' => 'OutputAllActionOutput',
      'output_handler_configuration' => [],
      'default_data_output_configuration' => [],
      'permission' => 'add contacts',
    ]);
    civicrm_api3('FormProcessorInput', 'create', [
      'form_processor_id' => $instance['id'],
      'title' => 'Test 1',
      'name' => 'test_1',
      'type' => 'Integer',
      'is_required' => 1,
      'configuration' => [],
    ]);

    // Ensure APIv4 entity has been created
    $entity = Entity::get(FALSE)
      ->addWhere('name', '=', 'FormProcessor_my_processor')
      ->setChain(['fields' => ['$name', 'getFields']])
      ->execute()->single();

    $this->assertEquals('Test processor entity', $entity['description']);
    $this->assertCount(1, $entity['fields']);
  }

}
