<?php
// This file declares an Angular module which can be autoloaded
// in CiviCRM. See also:
// \https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_angularModules/n
return [
  'js' => [
    'ang/afFormProcessorGui.js',
    'ang/afFormProcessorGui/*.js',
    'ang/afFormProcessorGui/*/*.js',
  ],
  'css' => [
    'ang/afFormProcessorGui.css',
  ],
  'partials' => [
    'ang/afFormProcessorGui',
  ],
  'basePages' => ['civicrm/admin/afform'],
  /*'requires' => [
    'crmUi',
    'crmUtil',
    'ngRoute',
  ],
  'settings' => [],*/
];
