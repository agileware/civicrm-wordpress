<?php
use CRM_FormProcessor_ExtensionUtil as E;
return [
  'type' => 'search',
  'title' => E::ts('Form Processors'),
  'description' => E::ts('Administer civicrm'),
  'icon' => 'fa-list-alt',
  'server_route' => 'civicrm/admin/formprocessor/search',
  'permission' => ['administer CiviCRM'],
];
