<?php

require_once 'form_processor.civix.php';
use CRM_FormProcessor_ExtensionUtil as E;

use \Symfony\Component\DependencyInjection\ContainerBuilder;
use \Civi\FormProcessor\Symfony\Component\DependencyInjection\DefinitionAdapter;

/**
 * Implements hook_civicrm_container()
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_container/
 */
function form_processor_civicrm_container(ContainerBuilder $container) {
  // Register the TypeFactory
  $typeFactoryDefinition = DefinitionAdapter::createDefinitionClass('Civi\FormProcessor\Type\Factory');
  $container->setDefinition('form_processor_type_factory', $typeFactoryDefinition);
  // Register the OutputHandlerFactory
  $outputHandlerDefinition = DefinitionAdapter::createDefinitionClass('Civi\FormProcessor\OutputHandler\Factory');
  $container->setDefinition('form_processor_output_handler_factory', $outputHandlerDefinition);
  // Register the DelayedAction Factoory
  $delayedActionFactoryDefinition = DefinitionAdapter::createDefinitionClass('Civi\FormProcessor\DelayedAction\Factory');
  $container->setDefinition('form_processor_delayed_action_factory', $delayedActionFactoryDefinition);
  // Register the validationFactory
  $validationFactoryDefinition = DefinitionAdapter::createDefinitionClass('Civi\FormProcessor\Validation\Factory');
  $validationFactoryDefinition->setFactory(['Civi\FormProcessor\Validation\Factory', 'singleton']);
  $container->setDefinition('form_processor_validation_factory', $validationFactoryDefinition);


  // Register our API Providers.
  // The API provider is used to process incoming api calls and process them
  // with the form processor logic.
  $apiKernelDefinition = $container->getDefinition('civi_api_kernel');
  $apiProviderDefinition = DefinitionAdapter::createDefinitionClass('Civi\FormProcessor\API\FormProcessor');
  $apiKernelDefinition->addMethodCall('registerApiProvider', array($apiProviderDefinition));
  $apiProviderDefaultsDefinition = DefinitionAdapter::createDefinitionClass('Civi\FormProcessor\API\FormProcessorDefaults');
  $apiKernelDefinition->addMethodCall('registerApiProvider', array($apiProviderDefaultsDefinition));
  $apiProviderValidationDefinition = DefinitionAdapter::createDefinitionClass('Civi\FormProcessor\API\FormProcessorValidation');
  $apiKernelDefinition->addMethodCall('registerApiProvider', array($apiProviderValidationDefinition));
  $apiProviderCalculationDefinition = DefinitionAdapter::createDefinitionClass('Civi\FormProcessor\API\FormProcessorCalculation');
  $apiKernelDefinition->addMethodCall('registerApiProvider', array($apiProviderCalculationDefinition));
  // Register APIv4 provider
  $api4Definition = DefinitionAdapter::createDefinitionClass('Civi\FormProcessor\API4\FormProcessorProvider');
  $apiKernelDefinition->addMethodCall('registerApiProvider', [$api4Definition]);



  $container->findDefinition('dispatcher')
    ->addMethodCall('addSubscriber', [new \Symfony\Component\DependencyInjection\Definition('Civi\FormProcessor\EventListener\FormatSubscriber')])
    ->addMethodCall('addSubscriber', [new \Symfony\Component\DependencyInjection\Definition('Civi\FormProcessor\EventListener\MetadataSubscriber')])
  ;
}

/**
 * Implements hook_civicrm_alterAPIPermissions()
 *
 * All Form Processor api's are under the Administer CiviCRM permission.
 * Except for the FormProcessorExecutor api.
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_alterAPIPermissions/
 */
function form_processor_civicrm_alterAPIPermissions($entity, $action, &$params, &$permissions) {
  $permissions['form_processor_instance']['list'] = [];
  $permissions['form_processor_instance']['get_calculation_triggers'] = [];
  $permissions['form_processor_instance']['getoutput'] = [];
  $permissions['form_processor_instance']['get'] = ['administer CiviCRM'];
  $permissions['form_processor_instance']['create'] = ['administer CiviCRM'];
  $permissions['form_processor_instance']['delete'] = ['administer CiviCRM'];
  $permissions['form_processor_instance']['export'] = ['administer CiviCRM'];
  $permissions['form_processor_instance']['import'] = ['administer CiviCRM'];
  $permissions['form_processor_instance']['revert'] = ['administer CiviCRM'];
  $permissions['form_processor_instance']['check_name'] = ['administer CiviCRM'];

  $permissions['form_processor_input']['get'] = ['administer CiviCRM'];
  $permissions['form_processor_input']['create'] = ['administer CiviCRM'];
  $permissions['form_processor_input']['delete'] = ['administer CiviCRM'];
  $permissions['form_processor_input']['check_name'] = ['administer CiviCRM'];

  $permissions['form_processor_validation']['get'] = ['administer CiviCRM'];
  $permissions['form_processor_validation']['create'] = ['administer CiviCRM'];
  $permissions['form_processor_validation']['delete'] = ['administer CiviCRM'];

  $permissions['form_processor_action']['get'] = ['administer CiviCRM'];
  $permissions['form_processor_action']['create'] = ['administer CiviCRM'];
  $permissions['form_processor_action']['delete'] = ['administer CiviCRM'];
  $permissions['form_processor_action']['check_name'] = ['administer CiviCRM'];

  $permissions['form_processor_default_data_input']['get'] = ['administer CiviCRM'];
  $permissions['form_processor_default_data_input']['create'] = ['administer CiviCRM'];
  $permissions['form_processor_default_data_input']['delete'] = ['administer CiviCRM'];
  $permissions['form_processor_default_data_input']['check_name'] = ['administer CiviCRM'];

  $permissions['form_processor_default_data_action']['get'] = ['administer CiviCRM'];
  $permissions['form_processor_default_data_action']['create'] = ['administer CiviCRM'];
  $permissions['form_processor_default_data_action']['delete'] = ['administer CiviCRM'];
  $permissions['form_processor_default_data_action']['check_name'] = ['administer CiviCRM'];

  $permissions['form_processor_validate_action']['get'] = ['administer CiviCRM'];
  $permissions['form_processor_validate_action']['create'] = ['administer CiviCRM'];
  $permissions['form_processor_validate_action']['delete'] = ['administer CiviCRM'];
  $permissions['form_processor_validate_action']['check_name'] = ['administer CiviCRM'];

  $permissions['form_processor_validate_validator']['get'] = ['administer CiviCRM'];
  $permissions['form_processor_validate_validator']['create'] = ['administer CiviCRM'];
  $permissions['form_processor_validate_validator']['delete'] = ['administer CiviCRM'];
  $permissions['form_processor_validate_validator']['check_name'] = ['administer CiviCRM'];

  $permissions['form_processor_calculate_action']['get'] = ['administer CiviCRM'];
  $permissions['form_processor_calculate_action']['create'] = ['administer CiviCRM'];
  $permissions['form_processor_calculate_action']['delete'] = ['administer CiviCRM'];
  $permissions['form_processor_calculate_action']['check_name'] = ['administer CiviCRM'];

  if (in_array($entity, [
      'form_processor',
      'form_processor_defaults',
      'form_processor_validation',
      'form_processor_calculation',
    ]) && $action != 'getactions') {
    $formProcessName = $action;
    if ($action == 'getfields' && isset($params['api_action'])) {
      $formProcessName = $params['api_action'];
    }

    $permissions[$entity][$action] = [];
    $formProcessorPermission = CRM_FormProcessor_BAO_FormProcessorInstance::getPermission($formProcessName);
    if (!empty($formProcessorPermission)) {
      $permissions[$entity][$action][] = $formProcessorPermission;
    }
  }
}

/**
 * Implementation of hook_civicrm_pageRun
 *
 * Handler for pageRun hook.
 */
function form_processor_civicrm_pageRun(&$page) {
  $f = '_' . __FUNCTION__ . '_' . get_class($page);
  if (function_exists($f)) {
    $f($page);
  }
}

function _form_processor_civicrm_pageRun_CRM_Admin_Page_Extensions(&$page) {
  _form_processor_prereqCheck();
}

function _form_processor_prereqCheck() {
  $unmet = CRM_FormProcessor_Upgrader::checkExtensionDependencies();
  CRM_FormProcessor_Upgrader::displayDependencyErrors($unmet);
}

/**
 * @return \Civi\FormProcessor\DelayedAction\Factory|null
 */
function form_processor_get_delayed_action_factory():? \Civi\FormProcessor\DelayedAction\Factory {
  $container = \Civi::container();
  if ($container->has('form_processor_delayed_action_factory')) {
    /** \Civi\FormProcessor\DelayedAction\Factory */
    $factory = $container->get('form_processor_delayed_action_factory');
    if ($factory instanceof Civi\FormProcessor\DelayedAction\Factory) {
      return $factory;
    }
  }
  return null;
}

/**
 * @return \Civi\ActionProvider\Provider
 */
function form_processor_get_action_provider() {
  $container = \Civi::container();
  if ($container->has('action_provider')) {
    $action_provider_container = $container->get('action_provider');
    return $action_provider_container->getProviderByContext('form_processor');
  }
  return null;
}

/**
 * @return \Civi\ActionProvider\Provider
 */
function form_processor_get_action_provider_for_default_data() {
  $container = \Civi::container();
  if ($container->has('action_provider')) {
    $action_provider_container = $container->get('action_provider');
    return $action_provider_container->getProviderByContext('form_processor');
  }
  return null;
}

/**
 * @return \Civi\ActionProvider\Provider
 */
function form_processor_get_action_provider_for_validation() {
  $container = \Civi::container();
  if ($container->has('action_provider')) {
    $action_provider_container = $container->get('action_provider');
    return $action_provider_container->getProviderByContext('form_processor');
  }
  return null;
}

/**
 * @return \Civi\ActionProvider\Provider
 */
function form_processor_get_action_provider_for_calculation() {
  $container = \Civi::container();
  if ($container->has('action_provider')) {
    $action_provider_container = $container->get('action_provider');
    return $action_provider_container->getProviderByContext('form_processor');
  }
  return null;
}

/**
 * Implementation of hook_civicrm_navigationMenu.
 *
 * Adds Automation navigation items just before the Administer menu.
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_navigationMenu
 */
function form_processor_civicrm_navigationMenu(&$params) {
  if (!_form_processor_menu_exists($params, 'Administer/automation')) {
    _form_processor_civix_insert_navigation_menu($params, 'Administer', [
      'label' => E::ts('Automation'),
      'name' => 'automation',
      'url' => NULL,
      'permission' => 'administer CiviCRM',
      'operator' => NULL,
      'separator' => 0,
    ]);
  }
  _form_processor_civix_insert_navigation_menu($params, 'Administer/automation', array(
    'label' => E::ts('Form processors'),
    'name' => 'form_processors',
    'url' => 'civicrm/admin/formprocessor/search',
    'permission' => 'administer CiviCRM',
    'operator' => NULL,
  ));
  _form_processor_civix_navigationMenu($params);
}

/**
 * Checks whether a navigation menu item exists.
 *
 * @param array $menu - menu hierarchy
 * @param string $path - path to parent of this item, e.g. 'my_extension/submenu'
 *    'Mailing', or 'Administer/System Settings'
 * @return bool
 */
function _form_processor_menu_exists(&$menu, $path) {
  // Find an recurse into the next level down
  $found = FALSE;
  $path = explode('/', $path);
  $first = array_shift($path);
  foreach ($menu as $key => &$entry) {
    if ($entry['attributes']['name'] == $first) {
      if (empty($path)) {
        return true;
      }
      $found = _form_processor_menu_exists($entry['child'], implode('/', $path));
      if ($found) {
        return true;
      }
    }
  }
  return $found;
}

/**
 * Implements hook_civicrm_config().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_config
 */
function form_processor_civicrm_config(&$config) {
  _form_processor_civix_civicrm_config($config);
}

/**
 * Implements hook_civicrm_install().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_install
 */
function form_processor_civicrm_install() {
  _form_processor_civix_civicrm_install();
}

/**
 * Implements hook_civicrm_enable().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_enable
 */
function form_processor_civicrm_enable() {
  _form_processor_civix_civicrm_enable();
}

/**
 * Implements hook_civicrm_managed().
 *
 * Generate a list of entities to create/deactivate/delete when this module
 * is installed, disabled, uninstalled.
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_managed
 */
function form_processor_civicrm_managed(&$entities) {
  \Civi\FormProcessor\Utils\Cache::clear();
}
