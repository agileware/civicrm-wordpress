# Example of a basic form to sign up for a newsletter from a Wordpress website

This example details how to create a form on my public wordpress website where a visitor can enter *first name, last name and email* and click to sign up for our monhtly newsletter.
The **Form Processor** extension will make sure this is processed in the CiviCRM backend.

The public website is on another server than CiviCRM and I am using Wordpress on my public website example.

## Defining the form processor

The general information for my form processor looks like this:

![Form Processor General](img/newsletter-general-fp.png)

On my form processor I will accept 3 inputs: first name, last name and email.

![Form Processor Inputs](img/newsletter-inputs-fp.png)

Once the form is sent from the public website I would like the following to happen:

* check if we already have a contact with the email entered and if so, use that contact
* if there is no contact with the email yet, create a new contact with the email, first and last name from the form
* add the found or created contact to our newsletter group

So here is what the actions look like:

![Form Processor Actions](img/newsletter-actions-fp.png)

The action to find or create the contact has been specified so:

![Action to Find or Create Contact](img/newsletter-find-action.png)

Finally in the action to add the contact to the group I have selected the group the contact should be added to and specified that the contact ID found in the find action should be used.

![Action to Add to Group](img/action-add-to-group.png)

## Defining the form in Wordpress

I now need to design a form on my public website that communicates with my form processor. In this example I use Wordpress.

### Contact Form 7 CiviCRM Integration

To be able to communicate with a CiviCRM installation on another server the **Contact Form 7 CiviCRM integration** plugin is installed, and I have also installed the **Contact Form 7** plugin. (see the [Requirements](./requirements.md) section):

![CiviMRF modules](img/newsletter-wp-plugins.png)

Once I have installed the Contact Form 7 plugins I can specify how to connect to CiviCRM under Settings and then CiviCRM settings:

![CiviMRF Profile Menu](img/newsletter-wp-civicrm-settings.png).

In this settings screen I specify the URL to the REST interface of CiviCRM and added the site and API keys.
The URL is broken down in two parts: first is the hostname including http or https, the
second part is the link to the rest.php in your civicrm installation.

!!! Note "Site and API keys"
    (for background information check [System Administrator Guide on Site Key][sitekey] and [StackExchange on API key][apikey]).

### Creating the form with Contact Form 7

I am now going to create my form in Wordpress by clicking *Contact*, then *Add new*. I then create my form in a big textarea.
The contact form 7 plugin works in this way that you design your form describing it in text.

The text to design my form looks like this:

![New empty form](img/newsletter-wp-contact-form-design.png)

Make sure that your fields have the same name as the name defined in the form processor.  The field is defined between
brackets `[]` after that the type of the field is specified. And an asterix `*` indicates that this field is required.
Then a space and the name of the field.

Press save and if there are any errors on the *Mail* tab correct those by clicking on the *Mail* tab. You can also
disable the e-mail by setting the following under additional settings:

![Additional settings](img/newsletter-wp-contact-form-additional-settings.png)

Now click on the *CiviCRM* tab and you will see the following:

![CiviCRM Settings](img/newsletter-wp-contact-form-civicrm.png)

In this screen you specify to which API you want to send your form to. With the form processor the Entity is always _FormProcessor_
and the action is the *name* of your form processor.

### Add the form a page

In the top of the form you see the short code which you can use to display the form on a page:

![shortcode](img/newsletter-wp-contact-form-shortcode.png)

Copy this short code.

It is time to create a page and paste our short code:

![page](img/newsletter-wp-new-page.png)

Save the page.

### The end result of the form

After we have saved the page our form looks like this. I did not apply any styling or theming in my wordpress installation:

![Resulting form](img/newsletter-wp-form.png)

## Result!

If I now enter data in my form as you can see here:

![Form for John Doe](img/newsletter-wp-jd-form.png)

The result in CiviCRM will be that a contact is created and he is added to the newsletter group:

![CiviCRM John Doe](img/newsletter-wp-jd-civi1.png)

![CiviCRM John Doe](img/newsletter-wp-jd-civi2.png)

[sitekey]:https://docs.civicrm.org/sysadmin/en/latest/setup/site-key/
[apikey]:https://civicrm.stackexchange.com/questions/31092/where-is-the-api-key
