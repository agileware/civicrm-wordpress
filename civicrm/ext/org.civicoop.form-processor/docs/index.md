# Introduction

**[Form Processor][formprocessorrepo]** is an extension originally developed by [CiviCooP][civicoop], mainly funded by [Roparun][roparun], [Velt][velt], [Barnekreftforeningen][barnekreft] and CiviCooP themselves.

The aim of the extension is to create a _framework_ to enable swift development (increasingly without coding) of forms that are used on the public website and communicate with CiviCRM.

The configuration we had in mind when developing is a CiviCRM installation on a different server than the public website and communication with CiviCRM using the CiviMRF framework (see [the CiviMRF GitHub repo][cmrf-repo]).

!!! Note "About CiviMRF"
    **CiviMRF** is a framework that enables communication between a public website and CiviCRM on a separate server. It contains a generic core, a Drupal 7 specific implementation, a Wordpress specific implementation, a Drupal 8 specific implementation etc.

This is not required, you can just as well use the **Form Processor** extension to communicate with a CiviCRM installation on the same server as the public website.


## Contents

This guide is a first attempt to explain the basic concept of the **Form Processor** and a few examples on how to use the form processor.

Actually we should mention here that it is the combination of the **Form Processor** extension _and_ the **Action Provider** extension (see [Action Provider extension][actionproviderrepo]) in combination that provide the framework. The **Action Provider** provides predefined *actions* that can be used to do stuff in CiviCRM with the data entered on the form, or *retrieval methods* to provide default data to the form.

There are a few requirements to be able to get the full benefits of the **Form Processor** extension:

- [Requirements](./requirements.md)

The basic concepts of the **Form Processor** are explained on:

- [Basic Concepts](./basic-concept.md)

In this guide you will also find an example to set up forms with the **Form Processor** without coding and an example how to develop your own *actions* and *retrieval criteria*, as well as an introduction on how to create your own:

- [How to create a basic form to sign up for a newsletter (Drupal 7 website)](./sign-up-newsletter.md)
- [How to create a basic form to sign up for a newsletter (Wordpress website)](./sign-up-newsletter-wordpress.md)
- [How to create a form for your email preferences (Drupal 7 website)](./email-preferences.md)

The latter How To will give you an example of how to develop your own actions!

## CiviCRM versions

The **Form Processor** extension has initially been developed with CiviCRM 4.7.4 and has been tested with CiviCRM 5.x on various sites.

!!! Note
    If you want the **Form Processor** updated to a newer version you can do so. Alternatively, if you want us to do it and have some funding, contact Jaap Jansma (<jaap.jansma@civicoop.org>) or Erik Hommel(<erik.hommel@civicoop.org>). You can also find them on the [CiviCRM Mattermost Channel][mattermost] using the handles **jaapjansma** or **ehommel**.

## Screen prints

In this guide you will find a number of screenshots. These were all taken from an installation with the [Finsbury Park Theme][finsbury].

[civicoop]: http://www.civicoop.org/
[roparun]:https://www.roparun.nl/en/
[velt]:https://velt.be/
[barnekreft]:https://www.barnekreftforeningen.no/
[cmrf-repo]:https://github.com/CiviMRF
[actionproviderrepo]:https://lab.civicrm.org/extensions/action-provider
[formprocessorrepo]:https://lab.civicrm.org/extensions/form-processor
[mattermost]:https://chat.civicrm.org/civicrm/
[finsbury]:https://civicrm.org/extensions/finsbury-park-cross-cms-admin-theme
