# Basic Concept of the Form Processor Extension

The **Form Processor** extension allows you to specify the data that will be processed with a form on your public website.
In CiviCRM you specify what data you want to display on the form, if and what default values you want to load and what should happen with the data that is submitted with the form.

For example: I want a form that allows me to enter my name and email address so I can sign up for the newsletter.
You can specify all that in the **Form Processor** and also specify that a new contact should be created if one can not be found with the email address, and that the contact found or created should be added to the newsletter group in CiviCRM.

In this chapter we will briefly discuss the forms and screens you need to go through if you are specifying a form with the **Form Processor**.

We will start with a fairly basic Form Processor and in further sections explain how you can get a form prefilled with data, how you can validate data before it is processed in CiviCRM and how you can prepopulate input fields with calculated values (like the number of participants * the price, the age of the contact or stuff like that).

!!! Note "Techie stuff"
    On a technical level: the extension actually generates an API action for the FormProcessor API entity.

## Form Processors Page

Once you installed the **Form Processor** extension you will have an additional option in the **Administer>Automation>Form processors** option which will lead you to the first view of all the form processors in your installation.
This will be empty if you just installed the extension.

The actions you can perform with each form processor will be in the little menu after each entry.

![Form Processors Page](img/init-form.png)

!!! Note "Import button"
With the import button you can _import_ a form processor from a JSON file. For each form processor you have an action _Export_ which exports the definition to a JSON file. This allows you to easily move defined Form Processors from for example a test to a live environment.

## New Form Processor - define form processor
On this page you can click on the **Add Form Processor** button to create a new form processor. If you do this you will see a form like the one below:

![New Form Processor](img/new-processor.png)

On this form you specify generic information about your form. You can enter the general form processor information:

* have to specify a *title* for the form processor
* a *name* will be suggested based on the *title* but can be changed if you click on the lock behind the field first!

!!! Note
    A *name* can not contain any spaces! And should be unique!

* you can specify a detailed description (and it makes sense to do so if you expect to build a few forms!)
* you can tick if the form processor should be enabled (on by default)
* you can select what permissions are required to be able to process the form. For example you could decide you can only send your address details if you have the *CiviCRM:edit my contact* permission.

It will look something like this:

![My example](img/new_first.png)

When you are happy you hit _Save_ to create your form processor and move to the next bit of configuration.

### Input fields on your form processor

Once saved your form will look like the one below. You can still change title, name, description and the other generic information if you want to (make sure to hit the _Save_ button then before you do anything else) but you can also do much more.

![My example once saved](img/example-once-saved.png)

In the next part under the heading **Inputs** you can specify the input fields that should be on your form.

For each field you can select the type of field (short text, numeric no decimal, date, yes/no, option group etc.), specify a title for the input field, check if the input is required for the form processor, check if you want a formatted value (for example when you want to use the input in an email in a HTML-formatted fashion), a default value and potentially change the generated system name. Let's for example take the first name, last name and email.

Adding the input field for the *email* will probably have to look like this:

![Input field for email]( img/new-input-email.png)

Once I have added all the fields the list will look something like this:

![List of input fields](img/new-list-input-fields.png)

What I have done now is specify that I expect my form to show 4 fields for the website visitor to enter: First Name, Last Name and Email and Newsletter. I have entered them in a different sequence but I used the arrows behind the inputs in the list to put them in the sequence I want.

### Actions on your form processor

In the bottom part of the form (under the heading **Actions**) you can specify what needs to happen once CiviCRM receives the data from the form.
We do that by adding *actions* to the form processor.

So in our example, we should find a contact by the email. Next the contact found should be added to the newsletter group if they specified they want to.
Initially the part of the form where we can specify actions will look like this:

![Action part of the form](img/new-action-part.png)

If you click on the _Add action_ button you will get a form where you can enter the details of the action
In this example we have a first step: find the contact with the data from the form or create a new one.

If you click on the select box for the _Type_ you will get a list of all the actions that have been developed so far.

They are there because organizations funded the development of actions they needed.
As time goes by and more people start using and enhancing the [**Action Provider** extension][actionproviderrepo], the list will grow but there are quite a few useful ones already.

In this example we select the action _Contact:Get or Create By Email and Names_.

You can also add a _Title_ for the action. A system name will be generated based on that title (which you can change if you want to).
Depending on the action you will be able to configure values that will be used for the action under the _Configuration_ tab or enter the inputs or action results under the _Parameter mapping_ tab.
In this example you can see that I use the inputs _First Name, Last Name_ and _Email_.

![Find or create contact action](img/action-find-contact.png)

!!! Note "Create your own"
    It is possible to develop your own specific actions, or indeed generic ones that others can use too! Check the relevant sections in [Example of Email Preferences](email-preferences.md)

Once I have found the contact it should be added to the newsletter group.
If I select the *add to group* action I can select the group and specify that I want to use the contact ID found in the previous action:

![Add to newsletter group action](img/action-add-to-group.png)

Obviously I only want to add the contact to the newsletter group if they have said they want to. To achieve that I want to add a condition:

![Add a condition](img/action-condition.png)

When I click the _Add condition_ link I get to enter the configuration for my condition. As you can see here I want to use the type _Parameter has value_, the value should be _TRUE_ and the input in question is the _Newsletter_. Meaning this action is ONLY executed if the Newsletter input has the value _TRUE_.

![Newsletter condition](img/newsletter-condition.png)

Obviously there are more types of conditions, have a play with them to find out which one suits your needs the best.

## Try out - testing the Form Processor in CiviCRM

In the action menu behind each Form Processor as well as in the tab you have the _Try out_ option:

![Try out action link](img/try-out-actionlink.png)

![Try out tab](img/try-out-tab.png)

With this option you can try your form processor to check if the actions are performed correctly in CiviCRM with everything you have configured. It should really be part of your testing process, making sure the form processor does what you expect in CiviCRM before you test it with a form on your website (or any other data coming in from outside).

## Export and import - moving form processors around

Once you have created a nice working form processor on your test environment you do not want to re-develop it in your live environment. And the other way around too, if there is a problem with a live form processor you would want to take it to your test environment and make sure it is exactly that same.

This is where the _export_ and _import_ comes in.

In the list of form processors you will have an export action link in the menu behind each form processor:

![Export](img/fp-export.png)

This will export the definition of your form processor to a JSON file which you can then save on your computer or on the network.

You also have the _Import_ option in your form processor page which allows you to import a JSON file into a form processor.

![Import](img/fp-import.png)

## New Form Processor - retrieval of defaults

With the **Form Processor** extension it is possible to pre-populate your form with default data, for example for a *My Address Data* form.
We could pass a parameter (a checksum for example) in the URL of the form and based on that retrieve the current values from CiviCRM and prepopulate the form with this data.

First step is to click on the _Retrieval of defaults_ tab and then check the box in there to enable the defaults.
![Defaults tab](img/defaults-tab.png)

Once I have done that I will get a form to configure my retrieval of defaults.

![New retrieval of defaults](img/new-retrieval.png)

As you can see the input fields from my form processor have already been loaded.
I can specify here what criteria I want to use, fore example a text input for a checksum.

At the *retrieval methods* part I can select how I want to retrieve my data.
Here I would like an action called *Find contact with checksum*. Unfortunately that is not available yet because it has not been developed yet, but you get the gist.
And it will be developed in the section [Email Preferences](./email-preferences.md).

For each of my input fields I can finally specify what data should be loaded here, which should come from the result of my action.

!!! Note "Techie stuff"
    On a technical level: If you have enabled default data retrieval it actually generates an API action for the FormProcessorDefaults API entity.

## New Form Processor - Using the Validator
With the **Form Processor** extension it is possible to validate data on the website form before it is actually processed in CiviCRM. For example you could validate if a contact exists, an event is still open for registration or a certain activity type exists before you process the contact, event registration or activity in CiviCRM.

The error message you specify with the validator will appear on your Webform in Drupal before the data is actually sent to CiviCRM.

!!! Note "Only with Drupal 8 and later"
    Please note that the validator option currently only works with Drupal 8, 9 and 10. In theory it could also work with other CMS's but it would require a specific customization for that CMS to process the validation data from the Form Processor. So it could work for example for Caldera Forms in Wordpress if Caldera Forms is updated to proces the Form Processor validation.

The validation can be added by clicking on the _Validation_ tab in the Form Processor screen. Once I do that I will get a form looking like this:

![Validation tab](img/validation-tab.png)

On this tab you can add actions like you can for the inputs part of the Form Processor and use the results of these actions in the validation under the heading _Actions_.

Under the heading Validators you can add the actual validations you like. In my example I am going to create a validator that checks if a parameter is not empty.

First I will click on the Add Validator button:
![Add Validator](img/add-validator.png)

First I select the type of validator, in this case _Parameter is empty_.
Next is the title, from which a _system name_ is generated (which you can change if you want to).

At _Show messages for inputs_ I can select which of the inputs is the one that sort of caused the error, which should be flagged by the CMS as the field in error. As in this case I want make sure that the Campaign ID is not empty I select the input  _Campaign ID_.

Under the _Configuration_ tab I can specify the error message I want to use for my validation if it fails.

Under the tab _Parameter Mapping_ I can select the parameter I want to validate.

![My validation example](img/parameter-empty-validator.png)

!!! Note "Little side note"
    Obviously I could also have achieved this by making the parameter _required_ but it suffices as an example.

The list of possible validators is still relatively empty, we expect this to grow as more and more people in the community start using this and adding validators, for example if a campaign actually exists or if an event is still active etc.

## New Form Processor - Using the Calculator

With the **Form Processor** extension you can prepopulate fields with calculated data, and whenever something on your form is changed it will check if the calculated value needs to be changed.

For example you could have a form where you can register for an event and there will be a calculated field for the total amount which will initally show a value of 0. If you change the number of people you want to register the total amount will change (the number of participant * the registration fee).

Unfortunately at this point in time you can not use the _Try out_ option for calculated data yet.

!!! Note "Only with Drupal 8 and later"
Please note that the calculator option currently only works with Drupal 8, 9 and 10. In theory it could also work with other CMS's but it would require a specific customization for that CMS to process the calculated data from the Form Processor. So it could work for example for Caldera Forms in Wordpress if Caldera Forms is updated to proces the Form Processor calculations.

## That's it!

So far the basic concept. Obviously the public website part needs to be done too, check the [Example Sign Up Newsletter](./sign-up-newsletter.md) section for that part, this section just covers the basis principles.

[actionproviderrepo]:https://lab.civicrm.org/extensions/action-provider

