<?php

use CRM_FormProcessor_ExtensionUtil as E;

/**
 * FormProcessorAction.Delete API specification (optional)
 * This is used for documentation and validation.
 *
 * @param array $spec description of fields supported by this API call
 * @return void
 * @see http://wiki.civicrm.org/confluence/display/CRMDOC/API+Architecture+Standards
 */
function _civicrm_api3_form_processor_action_Delete_spec(&$spec) {
  $spec['id'] = array(
    'title' => E::ts('ID'),
    'type' => CRM_Utils_Type::T_INT,
    'api.required' => true
  );
}

/**
 * FormProcessorAction.Delete API
 *
 * @param array $params
 * @return array API result descriptor
 * @see civicrm_api3_create_success
 * @see civicrm_api3_create_error
 * @throws API_Exception
 */
function civicrm_api3_form_processor_action_Delete($params) {
  if (!array_key_exists('id', $params) || empty($params['id'])) {
    throw new API_Exception('Parameter id is mandatory and can not be empty in ' . __METHOD__, 0010);
  } else {
    $formProcessorId = civicrm_api3('FormProcessorAction', 'getvalue', array('id' => $params['id'], 'return' => 'form_processor_id'));
    CRM_FormProcessor_BAO_FormProcessorInstance::updateAndChekStatus($formProcessorId);
    $return = civicrm_api3_create_success(CRM_FormProcessor_BAO_FormProcessorAction::deleteWithId($params['id']), $params, 'FormProcessorAction', 'Delete');
    // Clear the cache
    \Civi\FormProcessor\Utils\Cache::cacheClearForFormProcessorById($formProcessorId);
    return $return;
  }
}

