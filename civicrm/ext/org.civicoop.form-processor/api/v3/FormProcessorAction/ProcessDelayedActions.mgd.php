<?php

return array (
  0 =>
    array (
      'name' => 'Cron:FormProcessorAction.ProcessDelayedActions',
      'entity' => 'Job',
      'params' =>
        array (
          'version' => 3,
          'name' => 'Process delayed form processor actions',
          'description' => '',
          'run_frequency' => 'Always',
          'api_entity' => 'FormProcessorAction',
          'api_action' => 'process_delayed_actions',
          'parameters' => '',
          'is_active' => '1',
        ),
    ),
);