<?php

/**
 * CiviRuleAction.process API
 *
 * Process delayed actions
 *
 * @param array $params
 * @return array API result descriptor
 * @see civicrm_api3_create_success
 * @see civicrm_api3_create_error
 * @throws API_Exception
 */
function civicrm_api3_form_processor_action_process_delayed_actions($params) {
  $returnValues = \Civi\FormProcessor\API\FormProcessor::processDelayedActions(60);

  // Spec: civicrm_api3_create_success($values = 1, $params = array(), $entity = NULL, $action = NULL)
  return civicrm_api3_create_success($returnValues, $params, 'FormProcessorAction', 'ProcessDelayedActions');
}