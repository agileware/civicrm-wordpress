<?php
/**
 * FormProcessorInput.Get API
 *
 * @param array $params
 * @return array API result descriptor
 * @see civicrm_api3_create_success
 * @see civicrm_api3_create_error
 * @throws API_Exception
 */
function civicrm_api3_form_processor_input_get($params) {
  $returnValues = CRM_FormProcessor_BAO_FormProcessorInput::getValues($params);
  foreach($returnValues as $index => $input) {
    if (isset($input['type']) && is_object($input['type'])) {
      $returnValues[$index]['type'] = $input['type']->toArray();
    }
    foreach($input['validators'] as $key => $validator) {
      $returnValues[$index]['validators'][$key]['validator'] = $validator['validator']->toArray();
    }
  }
  return civicrm_api3_create_success($returnValues, $params, 'FormProcessorInput', 'Get');
}

/**
 * FormProcessorInput.Get API specification (optional)
 * This is used for documentation and validation.
 *
 * @param array $spec description of fields supported by this API call
 * @return void
 * @see http://wiki.civicrm.org/confluence/display/CRM/API+Architecture+Standards
 */
function _civicrm_api3_form_processor_input_get_spec(&$spec) {
  $fields = CRM_FormProcessor_BAO_FormProcessorInput::fields();
  foreach($fields as $fieldname => $field) {
    $spec[$fieldname] = $field;
  }
}

