<?php

use CRM_FormProcessor_ExtensionUtil as E;

/**
 * FormProcessorInstance.Revert API
 *
 * @param array $params
 * @return array API result descriptor
 * @see civicrm_api3_create_success
 * @see civicrm_api3_create_error
 * @throws API_Exception
 */
function civicrm_api3_form_processor_instance_revert($params) {
  $exporter = new \Civi\FormProcessor\Exporter\ExportToJson();
  $returnValues = array();
  if ($exporter->revert($params['id'])) {
    $returnValues = civicrm_api3('FormProcessorInstance', 'getsingle', array('id' => $params['id']));
    // Clear the cache
    \Civi\FormProcessor\Utils\Cache::clear();
  } else {
    return civicrm_api3_create_error('Could not revert form processor '.$params['id']);
  }
  return $returnValues;
}

/**
 * FormProcessorInstance.Revert API specification (optional)
 * This is used for documentation and validation.
 *
 * @param array $spec description of fields supported by this API call
 * @return void
 * @see http://wiki.civicrm.org/confluence/display/CRM/API+Architecture+Standards
 */
function _civicrm_api3_form_processor_instance_revert_spec(&$spec) {
  $spec['id'] = array(
    'title' => E::ts('ID'),
    'type' => CRM_Utils_Type::T_INT,
    'api.required' => true
  );
}
