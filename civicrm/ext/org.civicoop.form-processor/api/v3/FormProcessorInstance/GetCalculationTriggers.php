<?php

use CRM_FormProcessor_ExtensionUtil as E;

/**
 * FormProcessorInstance.Getcalculationtriggers API
 *
 * @param array $params
 * @return array API result descriptor
 * @see civicrm_api3_create_success
 * @see civicrm_api3_create_error
 * @throws API_Exception
 */
function civicrm_api3_form_processor_instance_get_calculation_triggers($params) {
  $fields = civicrm_api3('FormProcessor', 'getfields', $params);
  $triggers = CRM_FormProcessor_BAO_FormProcessorInstance::getCalculationTriggers($params['api_action']);
  $outputs = CRM_FormProcessor_BAO_FormProcessorInstance::getCalculationOutputs($params['api_action']);
  $returnValues = [];
  foreach($fields['values'] as $field) {
    if (in_array($field['name'], $triggers) || in_array($field['name'], $outputs)) {
      $isOutput = false;
      if (in_array($field['name'], $outputs)) {
        $isOutput = true;
      }
      $field['api.return'] = $isOutput;
      $returnValues[$field['name']] = $field;
    }
  }
  return civicrm_api3_create_success($returnValues, $params, 'FormProcessorInstance', 'Getoutput');
}

/**
 * FormProcessorInstance.Getcalculationtriggers API specification (optional)
 * This is used for documentation and validation.
 *
 * @param array $spec description of fields supported by this API call
 * @return void
 * @see http://wiki.civicrm.org/confluence/display/CRM/API+Architecture+Standards
 */
function _civicrm_api3_form_processor_instance_get_calculation_triggers_spec(&$spec) {
  $formProcessors = civicrm_api3('FormProcessorInstance', 'list', []);
  $options = [];
  foreach($formProcessors['values'] as $formProcessor) {
    $options[$formProcessor['name']] = $formProcessor['title'];
  }
  $spec['api_action'] = [
    'type' => 'String',
    'name' => 'api_action',
    'title' => E::ts('Form Processor'),
    'options' => $options,
    'api.required' => true,
  ];
}

