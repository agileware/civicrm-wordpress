<?php

use Civi\FormProcessor\Runner;

/**
 * FormProcessorInstance.List API
 *
 * @param array $params
 * @return array API result descriptor
 * @see civicrm_api3_create_success
 * @see civicrm_api3_create_error
 * @throws API_Exception
 */
function civicrm_api3_form_processor_instance_list($params) {
  $returnValues = Runner::listFormProcessors();
  if(isset($params['name'])){
    $returnValues = array_filter($returnValues,function($value) use ($params) {
      return $value['name']==$params['name'];
    });
  }
  return civicrm_api3_create_success($returnValues, $params, 'FormProcessorInstance', 'List');
}

