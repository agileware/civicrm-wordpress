<?php

use CRM_FormProcessor_ExtensionUtil as E;

/**
 * FormProcessorInstance.check_name API specification
 *
 * @param $params
 */
function _civicrm_api3_form_processor_instance_check_name_spec($params) {
  $params['id'] = array(
    'name' => 'id',
    'title' => E::ts('ID'),
  );
  $params['title'] = array(
    'name' => 'title',
    'title' => E::ts('Title'),
    'api.required' => true,
  );
  $params['name'] = array(
    'name' => 'name',
    'title' => E::ts('Name'),
  );
}

/**
 * FormProcessorInstance.check_name API
 *
 * @param $params
 */
function civicrm_api3_form_processor_instance_check_name($params) {
  $name = CRM_FormProcessor_BAO_FormProcessorInstance::checkName($params['title'], $params['id'], $params['name']);
  return array(
    'name' => $name,
  );
}
