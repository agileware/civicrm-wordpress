<?php
/**
 * Copyright (C) 2023  Jaap Jansma (jaap.jansma@civicoop.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

use Civi\ActionProvider\Provider;
use CRM_FormProcessor_ExtensionUtil as E;

abstract class CRM_FormProcessor_Form_AbstractConfigurationDelay extends CRM_Core_Form {

  /** @var int */
  protected $formProcessorId;

  protected $actionId;

  protected $actionType;

  protected $action = array();

  protected $actionConfiguration = array();

  protected $actionMapping = array();

  protected $availableFields = array();

  protected $delayType;

  protected $delayConfiguration = array();

  /**
   * @var \Civi\FormProcessor\DelayedAction\AbstractDelayedAction
   */
  protected $delayClass;

  protected $snippet;

  /**
   * Returns the API 3 entity name.
   * E.g. FormProcessorValidateAction or FormProcessorDefaultDataAction
   * @return string
   */
  abstract protected function getApi3Entity(): string;

  /**
   * Returns the return url
   *
   * @return string
   */
  abstract protected function getReturnUrl(): string;

  abstract protected function getBaseUrl(): string;

  public function preProcess() {
    parent::preProcess();
    $this->snippet = CRM_Utils_Request::retrieve('snippet', 'String');
    if ($this->snippet) {
      $this->assign('suppressForm', TRUE);
      $this->controller->_generateQFKey = FALSE;
    }

    $this->actionId = CRM_Utils_Request::retrieve('id', 'Integer');
    $this->formProcessorId = CRM_Utils_Request::retrieve('form_processor_id', 'Integer', $this, TRUE);
    $this->assign('form_processor_id', $this->formProcessorId);
    /** @var \Civi\FormProcessor\DelayedAction\Factory $provider */
    $delayProvider = \Civi::service('form_processor_delayed_action_factory');

    $this->action = civicrm_api3($this->getApi3Entity(), 'getsingle', ['id' => $this->actionId]);
    $this->assign('actionObject', $this->action);
    $this->actionType = $this->action['type'];
    if (isset($this->action['configuration'])) {
      $this->actionConfiguration = $this->action['configuration'];
    }
    if (isset($this->action['mapping'])) {
      $this->actionMapping = $this->action['mapping'];
    }
    if (isset($this->action['delay']) && !empty($this->action['delay'])) {
      $this->delayType = $this->action['delay'];
      $this->delayClass = $delayProvider->getHandlerByName($this->delayType);
      if (isset($this->action['delay_configuration']) && is_array($this->action['delay_configuration'])) {
        $this->delayConfiguration = $this->action['delay_configuration'];
      } else {
        $this->delayConfiguration = [];
        $this->action['delay_configuration'] = [];
      }
    } else {
      $this->action['delay_configuration'] = [];
      $this->delayConfiguration = [];
    }

    $type = CRM_Utils_Request::retrieve('type', 'String');
    if ($type) {
      $this->delayType = $type;
      $this->delayClass = $delayProvider->getHandlerByName($this->delayType);
    }
    $this->assign('delayClass', $this->delayClass);
    $this->assign('base_url', $this->getBaseUrl());
    $this->assign('api3_entity_name', $this->getApi3Entity());
    $this->assign('isSubmitted', $this->isSubmitted());
  }

  public function buildQuickForm() {
    /** @var \Civi\FormProcessor\DelayedAction\Factory $provider */
    $delayProvider = \Civi::service('form_processor_delayed_action_factory');

    if (!$this->snippet) {
      $this->add('hidden', 'id');
      $this->add('hidden', 'form_processor_id');
    }

    if ($this->_action == CRM_Core_Action::DELETE) {
      $this->addButtons(array(
        array('type' => 'next', 'name' => E::ts('Delete'), 'isDefault' => TRUE,),
        array('type' => 'cancel', 'name' => E::ts('Cancel'))
      ));
    } else {
      $delayTypes = [];
      foreach($delayProvider->getHandlers() as $delay_name => $delay) {
        $delayTypes[$delay_name] = $delay->getTitle();
      }
      $this->add('select', 'type', E::ts('Type'), $delayTypes, false, array(
        'style' => 'min-width:250px',
        'class' => 'crm-select2 huge',
        'placeholder' => E::ts('- no delay -'),
      ));

      $this->assign('configurationElementNames', []);
      if ($this->delayClass) {
        $this->addConfigToForm($this->delayClass, $this->delayType);
        $defaultValues = array();
        foreach($this->delayClass->getConfigurationSpecification() as $config_field) {
          if (isset($this->delayConfiguration[$config_field->getName()])) {
            $defaultValues[$this->delayType.$config_field->getName()] = $this->delayConfiguration[$config_field->getName()];
          } elseif (!empty($config_field->getDefaultValue()) || strlen($config_field->getDefaultValue())) {
            $defaultValues[$this->delayType.$config_field->getName()] = $config_field->getDefaultValue();
          }
        }
        $this->setDefaults($defaultValues);
        $this->assign('isSubmitted', $this->isSubmitted());
      }
      $this->addButtons([
        ['type' => 'next', 'name' => E::ts('Save'), 'isDefault' => TRUE,],
        ['type' => 'cancel', 'name' => E::ts('Cancel')]
      ]);
    }

    parent::buildQuickForm();
  }

  /**
   * Function to set default values (overrides parent function)
   *
   * @return array $defaults
   * @access public
   */
  function setDefaultValues(): array {
    $defaults = [];
    $defaults['form_processor_id'] = $this->formProcessorId;
    $defaults['id'] = $this->actionId;
    $defaults['type'] = $this->delayType;
    return $defaults;
  }

  /**
   * Function that can be defined in Form to override or.
   * perform specific action on cancel action
   */
  public function cancelAction() {
    $this->formProcessorId = CRM_Utils_Request::retrieve('form_processor_id', 'Integer', $this, TRUE);
    CRM_Utils_System::redirect($this->getReturnUrl());
  }

  public function postProcess() {
    $redirectUrl = $this->getReturnUrl();
    $values = $this->exportValues();
    $delayConfiguration = 'null';
    if ($this->delayClass) {
      $delayConfiguration = array();
      $prefix = $this->delayType;
      foreach($this->delayClass->getConfigurationSpecification() as $config_field) {
        if (isset($values[$prefix.$config_field->getName()])) {
          $delayConfiguration[$config_field->getName()] = $values[$prefix.$config_field->getName()];
        }
      }
    }
    $params = $this->action;
    $params['delay'] = $values['type'];
    $params['delay_configuration'] = $delayConfiguration;

    $result = civicrm_api3($this->getApi3Entity(), 'create', $params);

    CRM_Utils_System::redirect($redirectUrl);
  }

  /**
   * BuildForm helper. Adds the elements to the form.
   *
   * @param AbstractType $type
   *   The input type class
   * @param $prefix
   */
  public function addConfigToForm(\Civi\FormProcessor\DelayedAction\AbstractDelayedAction $type, string $prefix=null) {
    // Check whether the actionProviderElementNames is already set if so get
    // the variable and append the configuration for this action to it.
    // Because we dont want to overwrite the current actionProviderElementNames.
    $elementNames = $this->get_template_vars('configurationElementNames');
    if (empty($elementNames)) {
      $elementNames = array();
    }
    if (!$prefix) {
      $prefix = get_class($type);
    }
    $configurationElementPreHtml = array();
    $configurationElementPostHtml = array();
    $configurationElementDescriptions = array();
    $elementNames[$prefix] = array();
    foreach($type->getConfigurationSpecification() as $config_field) {
      $field_name = $prefix.$config_field->getName();
      if ($config_field->getDescription()) {
        $configurationElementDescriptions[$field_name] = $config_field->getDescription();
      }
      $attributes = array(
        'class' => $prefix,
      );

      if (!empty($config_field->getFkEntity())) {
        $attributes['entity'] = $config_field->getFkEntity();
        $attributes['placeholder'] = E::ts('- Select -');
        $attributes['select'] = array('minimumInputLength' => 0);
        if ($config_field->isMultiple()) {
          $attributes['multiple'] = true;
        }
        $this->addEntityRef($field_name, $config_field->getTitle(), $attributes, $config_field->isRequired());
        $elementNames[$prefix][] = $field_name;
      } elseif (!empty($config_field->getOptions())) {
        $attributes['class'] .= ' crm-select2 huge';
        $attributes['placeholder'] = E::ts('- Select -');
        if ($config_field->isMultiple()) {
          $attributes['multiple'] = TRUE;
          $options = $config_field->getOptions();
        }
        else {
          $options = $config_field->getOptions();
        }
        $this->add('select', $field_name, $config_field->getTitle(), $options, $config_field->isRequired(), $attributes);
        $elementNames[$prefix][] = $field_name;
      } elseif ($config_field instanceof WysiwygSpecification && $config_field->getWysiwyg()) {
        $attributes = ['cols' => '80', 'rows' => '8', 'onkeyup' => "return verify(this)", 'preset' => 'civimail'];
        $this->add('wysiwyg', $field_name, $config_field->getTitle(), $attributes, $config_field->isRequired());
        $elementNames[$prefix][] = $field_name;
        $configurationElementPreHtml[$field_name] = '<div style="overflow: hidden;"><div style="float: right;"><input class="crm-token-selector big" id="token'.$field_name.'" data-field="'.$field_name.'" /></div></div>';
        $configurationElementPreHtml[$field_name] .= "
        <script type=\"text/javascript\">
            CRM.$(function($) {
              var token{$field_name} = ".json_encode($config_field->getAvailableTokens()).";
              $('input#token{$field_name}')
              .addClass('crm-action-menu fa-code')
              .change(function() {
                CRM.wysiwyg.insert('#{$field_name}', $(this).val());
                $(this).select2('val', '');
              })
              .crmSelect2({
                  data: token{$field_name},
                  placeholder: '".E::ts('Tokens')."'
               });
            });
         </script>";
      } else {
        $attributes['class'] .= ' huge';
        $this->add('text', $field_name, $config_field->getTitle(), $attributes, $config_field->isRequired());
        $elementNames[$prefix][] = $field_name;
      }

    }
    $this->assign('configurationElementNames', $elementNames);
    $this->assign('configurationElementDescriptions', $configurationElementDescriptions);
    $this->assign('configurationElementPreHtml', $configurationElementPreHtml);
    $this->assign('configurationElementPostHtml', $configurationElementPostHtml);
  }

}
