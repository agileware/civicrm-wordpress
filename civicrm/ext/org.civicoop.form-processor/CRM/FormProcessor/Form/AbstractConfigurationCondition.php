<?php
/**
 * Copyright (C) 2023  Jaap Jansma (jaap.jansma@civicoop.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

use Civi\ActionProvider\Provider;
use Civi\ActionProvider\Utils\UserInterface\AddConditionConfigToQuickForm;
use Civi\ActionProvider\Utils\UserInterface\AddMappingToQuickForm;
use CRM_FormProcessor_ExtensionUtil as E;

abstract class CRM_FormProcessor_Form_AbstractConfigurationCondition extends CRM_Core_Form {

  /** @var int */
  protected $formProcessorId;

  protected $actionId;

  protected $actionType;

  protected $action = array();

  protected $actionConfiguration = array();

  protected $actionMapping = array();

  protected $availableFields = array();

  /**
   * @var Civi\ActionProvider\Action\AbstractAction
   */
  protected $actionClass;

  protected $conditionType;

  protected $conditionConfiguration = array();

  protected $conditionMapping = array();

  /**
   * @var Civi\ActionProvider\Condition\AbstractCondition
   */
  protected $conditionClass;

  protected $snippet;

  /**
   * Returns the action provider
   *
   * @return \Civi\ActionProvider\Provider
   */
  abstract protected function getActionProvider(): Provider;

  /**
   * Returns the API 3 entity name.
   * E.g. FormProcessorValidateAction or FormProcessorDefaultDataAction
   * @return string
   */
  abstract protected function getApi3Entity(): string;

  /**
   * Returns an array of available fields for the mapping
   *
   * @return array
   */
  abstract protected function getAvailableFieldsForMapping(Provider $provider, int $formProcessorId, int $actionObjectId = null): array;

  /**
   * Returns the return url
   *
   * @return string
   */
  abstract protected function getReturnUrl(): string;

  abstract protected function getBaseUrl(): string;

  public function preProcess() {
    parent::preProcess();
    $this->snippet = CRM_Utils_Request::retrieve('snippet', 'String');
    if ($this->snippet) {
      $this->assign('suppressForm', TRUE);
      $this->controller->_generateQFKey = FALSE;
    }

    $this->actionId = CRM_Utils_Request::retrieve('id', 'Integer');
    $this->formProcessorId = CRM_Utils_Request::retrieve('form_processor_id', 'Integer', $this, TRUE);
    $this->assign('form_processor_id', $this->formProcessorId);
    /** @var Civi\ActionProvider\Provider $provider */
    $provider = $this->getActionProvider();


    $this->action = civicrm_api3($this->getApi3Entity(), 'getsingle', ['id' => $this->actionId]);
    $this->assign('actionObject', $this->action);
    $this->actionClass = $provider->getActionByName($this->action['type']);
    $this->actionType = $this->action['type'];
    if (isset($this->action['configuration'])) {
      $this->actionConfiguration = $this->action['configuration'];
    }
    if (isset($this->action['mapping'])) {
      $this->actionMapping = $this->action['mapping'];
    }
    if (isset($this->action['condition_configuration']) && is_array($this->action['condition_configuration'])) {
      if (isset($this->action['condition_configuration']['name'])) {
        $this->conditionType = $this->action['condition_configuration']['name'];
        $this->conditionClass = $provider->getConditionByName($this->conditionType);
      }
      if (isset($this->action['condition_configuration']['configuration'])) {
        $this->conditionConfiguration = $this->action['condition_configuration']['configuration'];
      } else {
        $this->conditionConfiguration = [];
        $this->action['condition_configuration']['configuration'] = [];
      }
      if (isset($this->action['condition_configuration']['parameter_mapping'])) {
        $this->conditionMapping = $this->action['condition_configuration']['parameter_mapping'];
      } else {
        $this->conditionMapping = [];
        $this->action['condition_configuration']['parameter_mapping'] = [];
      }
    } else {
      $this->action['condition_configuration'] = [];
      $this->conditionConfiguration = [];
      $this->action['condition_configuration']['configuration'] = [];
      $this->conditionMapping = [];
      $this->action['condition_configuration']['parameter_mapping'] = [];
      $this->action['condition_configuration']['output_mapping'] = [];
    }

    $type = CRM_Utils_Request::retrieve('type', 'String');
    if ($type) {
      $this->conditionType = $type;
      $this->conditionClass = $provider->getConditionByName($type);
    } elseif ($this->isSubmitted()) {
      $this->conditionType = null;
      $this->conditionClass = null;
    }
    $this->assign('conditionClass', $this->conditionClass);
    $this->assign('base_url', $this->getBaseUrl());
    $this->assign('api3_entity_name', $this->getApi3Entity());
    $this->assign('isSubmitted', $this->isSubmitted());

    $this->availableFields = $this->getAvailableFieldsForMapping($provider, $this->formProcessorId, $this->actionId);
  }

  public function buildQuickForm() {
    /** @var Civi\ActionProvider\Provider $provider */
    $provider = $this->getActionProvider();
    if (!$this->snippet) {
      $this->add('hidden', 'id');
      $this->add('hidden', 'form_processor_id');
    }

    if ($this->_action == CRM_Core_Action::DELETE) {
      $this->addButtons(array(
        array('type' => 'next', 'name' => E::ts('Delete'), 'isDefault' => TRUE,),
        array('type' => 'cancel', 'name' => E::ts('Cancel'))
      ));
    } else {
      $conditions = [];
      foreach($provider->getConditions() as $condition_name => $condition) {
        $conditions[$condition_name] = $condition->getTitle();
      }
      $this->add('select', 'type', E::ts('Type'), $conditions, false, array(
        'style' => 'min-width:250px',
        'class' => 'crm-select2 huge',
        'placeholder' => E::ts('- no condition -'),
      ));

      if ($this->conditionClass) {
        AddConditionConfigToQuickForm::buildForm($this, $this->conditionClass, $this->conditionType);
        $defaults = AddConditionConfigToQuickForm::setDefaultValues($this->conditionClass, $this->conditionConfiguration, $this->conditionType);
        $this->setDefaults($defaults);
        AddMappingToQuickForm::addMapping($this->conditionType.'_parameter_', $this->conditionClass->getParameterSpecification(), $this->action['condition_configuration']['parameter_mapping'], $this, $this->availableFields);
        AddMappingToQuickForm::addMapping($this->conditionType.'_output_', $this->actionClass->getOutputSpecification(), $this->action['condition_configuration']['output_mapping'], $this, $this->availableFields);
        $this->assign('parameter_mapping_prefix', $this->conditionType.'_parameter_');
        $this->assign('output_mapping_prefix', $this->conditionType.'_output_');
        $this->assign('isSubmitted', $this->isSubmitted());
      } else {
        $this->assign('actionProviderElementNames', []);
        $this->assign('actionProviderElementDescriptions', []);
        $this->assign('actionProviderElementPreHtml', []);
        $this->assign('actionProviderElementPostHtml', []);
        $this->assign('actionProviderMappingFields', []);
        $this->assign('actionProviderGroupedMappingFields', []);
        $this->assign('actionProviderCollectionMappingFields', []);
        $this->assign('actionProviderMappingDescriptions', []);
      }

      $this->addButtons([
        ['type' => 'next', 'name' => E::ts('Save'), 'isDefault' => TRUE,],
        ['type' => 'cancel', 'name' => E::ts('Cancel')]
      ]);
    }

    parent::buildQuickForm();
  }

  /**
   * Function to set default values (overrides parent function)
   *
   * @return array $defaults
   * @access public
   */
  function setDefaultValues(): array {
    $defaults = [];
    $defaults['form_processor_id'] = $this->formProcessorId;
    $defaults['id'] = $this->actionId;
    $defaults['type'] = $this->conditionType;
    return $defaults;
  }

  /**
   * Function that can be defined in Form to override or.
   * perform specific action on cancel action
   */
  public function cancelAction() {
    $this->formProcessorId = CRM_Utils_Request::retrieve('form_processor_id', 'Integer', $this, TRUE);
    CRM_Utils_System::redirect($this->getReturnUrl());
  }

  public function postProcess() {
    $redirectUrl = $this->getReturnUrl();
    $values = $this->exportValues();
    $condition_configuration = 'null';
    if (!empty($values['type'])) {
      $condition_configuration = array();
      $condition_configuration['name'] = $this->conditionType;
      $condition_configuration['configuration'] = AddConditionConfigToQuickForm::getSubmittedConfiguration($this, $this->conditionClass, $this->conditionType);
      $condition_configuration['parameter_mapping'] = AddMappingToQuickForm::processMapping($values, $this->conditionType.'_parameter_', $this->conditionClass->getParameterSpecification());
      $condition_configuration['output_mapping'] = AddMappingToQuickForm::processMapping($values, $this->conditionType.'_output_', $this->actionClass->getOutputSpecification());
    }
    $params = $this->action;
    $params['condition_configuration'] = $condition_configuration;

    $result = civicrm_api3($this->getApi3Entity(), 'create', $params);

    CRM_Utils_System::redirect($redirectUrl);
  }

}
