<?php
/**
 * Copyright (C) 2023  Jaap Jansma (jaap.jansma@civicoop.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

use Civi\ActionProvider\Parameter\WysiwygSpecification;
use Civi\FormProcessor\Validation\AbstractValidator;
use CRM_FormProcessor_ExtensionUtil as E;

abstract class CRM_FormProcessor_Form_AbstractConfigurationInputValidation extends CRM_Core_Form {

  /** @var int */
  protected int $entityId;

  /** @var int */
  protected int $formProcessorId;

  protected ?int $validationId;

  protected string $validationType;

  protected array $validation = array();

  protected array $validationConfiguration = array();

  /**
   * @var \Civi\FormProcessor\Validation\AbstractValidator
   */
  protected ?AbstractValidator $validatorCLass = null;

  protected ?string $snippet;

  /**
   * Returns the API 3 entity name.
   * E.g. FormProcessorValidateAction or FormProcessorDefaultDataAction
   * @return string
   */
  abstract protected function getApi3Entity(): string;

  /**
   * Returns the return url
   *
   * @return string
   */
  abstract protected function getReturnUrl(): string;

  /**
   * @return string
   */
  abstract protected function getBaseUrl(): string;

  public function preProcess() {
    parent::preProcess();
    $this->snippet = CRM_Utils_Request::retrieve('snippet', 'String');
    if ($this->snippet) {
      $this->assign('suppressForm', TRUE);
      $this->controller->_generateQFKey = FALSE;
    }

    $this->validationId = CRM_Utils_Request::retrieve('id', 'Integer');
    $this->entityId = CRM_Utils_Request::retrieve('entity_id', 'Integer', $this, TRUE);
    $this->formProcessorId = CRM_Utils_Request::retrieve('form_processor_id', 'Integer', $this, TRUE);
    $this->assign('entity_id', $this->entityId);
    $this->assign('form_processor_id', $this->formProcessorId);

    /** @var \Civi\FormProcessor\Validation\Factory $validationFactory */
    $validationFactory = Civi::service('form_processor_validation_factory');
    if ($this->validationId) {
      try {
        $this->validation = civicrm_api3('FormProcessorValidation', 'getsingle', ['id' => $this->validationId]);
        $this->assign('validationObject', $this->validation);
        $this->validatorCLass = $validationFactory->getValidatorByName($this->validation['validator']['name']);
        $this->validationType = $this->validation['validator']['name'];
        if (isset($this->validation['configuration'])) {
          $this->validationConfiguration = $this->validation['configuration'];
        }
      } catch (API_Exception $e) {
      }
    }

    $type = CRM_Utils_Request::retrieve('type', 'String');
    if ($type) {
      $this->validationType = $type;
      $this->validatorCLass = $validationFactory->getValidatorByName($type);
    }
    $this->assign('validationClass', $this->validatorCLass);
    $this->assign('base_url', $this->getBaseUrl());
    $this->assign('api3_entity_name', $this->getApi3Entity());
  }

  public function buildQuickForm() {
    if (!$this->snippet) {
      $this->add('hidden', 'id');
      $this->add('hidden', 'entity_id');
      $this->add('hidden', 'form_processor_id');
    }

    if ($this->_action == CRM_Core_Action::DELETE) {
      $this->addButtons(array(
        array('type' => 'next', 'name' => E::ts('Delete'), 'isDefault' => TRUE,),
        array('type' => 'cancel', 'name' => E::ts('Cancel'))
      ));
    } else {
      /** @var \Civi\FormProcessor\Validation\Factory $validationFactory */
      $validationFactory = Civi::service('form_processor_validation_factory');
      $this->add('select', 'type', E::ts('Type'), $validationFactory->getValidatorLabels(), TRUE, [
        'style' => 'min-width:250px',
        'class' => 'crm-select2 huge',
        'placeholder' => E::ts('- select -'),
      ]);
      if ($this->validatorCLass) {
        $this->addConfigToForm($this->validatorCLass, $this->validationType);
        $defaultValues = array();
        foreach($this->validatorCLass->getConfigurationSpecification() as $config_field) {
          if (isset($this->validationConfiguration[$config_field->getName()])) {
            $defaultValues[$this->validationType.$config_field->getName()] = $this->validationConfiguration[$config_field->getName()];
          } elseif ($config_field->getDefaultValue() !== null) {
            $defaultValues[$this->validationType.$config_field->getName()] = $config_field->getDefaultValue();
          }
        }
        $this->setDefaults($defaultValues);
      }

      $this->addButtons([
        ['type' => 'next', 'name' => E::ts('Save'), 'isDefault' => TRUE,],
        ['type' => 'cancel', 'name' => E::ts('Cancel')]
      ]);
    }

    parent::buildQuickForm();
  }

  /**
   * Function to set default values (overrides parent function)
   *
   * @return array $defaults
   * @access public
   */
  function setDefaultValues(): array {
    $defaults = [];
    $defaults['entity_id'] = $this->entityId;
    $defaults['form_processor_id'] = $this->formProcessorId;
    if ($this->validationId) {
      $defaults['id'] = $this->validationId;
      $defaults['type'] = $this->validation['validator'];
    }
    return $defaults;
  }

  /**
   * Function that can be defined in Form to override or.
   * perform specific action on cancel action
   */
  public function cancelAction() {
    $this->formProcessorId = CRM_Utils_Request::retrieve('form_processor_id', 'Integer', $this, TRUE);
    CRM_Utils_System::redirect($this->getReturnUrl());
  }

  public function postProcess() {
    $redirectUrl = $this->getReturnUrl();
    if ($this->_action == CRM_Core_Action::DELETE) {
      $session = CRM_Core_Session::singleton();
      civicrm_api3('FormProcessorValidation', 'delete', array('id' => $this->validationId));
      $session->setStatus(E::ts('Validatotion rule removed'), E::ts('Removed'), 'success');
      CRM_Utils_System::redirect($redirectUrl);
    }

    $values = $this->exportValues();
    $params['validator'] = $values['type'];
    $params['entity'] = $this->getApi3Entity();
    $params['entity_id'] = $this->entityId;
    if ($this->validationId) {
      $params['id'] = $this->validationId;
    }
    if ($this->validatorCLass) {
      $configuration = array();
      $prefix = $this->validationType;
      foreach($this->validatorCLass->getConfigurationSpecification() as $config_field) {
        if (isset($values[$prefix.$config_field->getName()])) {
          $configuration[$config_field->getName()] = $values[$prefix.$config_field->getName()];
        }
      }
      $params['configuration'] = $configuration;
    }

    $result = civicrm_api3('FormProcessorValidation', 'create', $params);

    CRM_Utils_System::redirect($redirectUrl);
  }

  /**
   * BuildForm helper. Adds the elements to the form.
   *
   * @param AbstractValidator $validator
   *   The input validator class
   * @param $prefix
   */
  public function addConfigToForm(AbstractValidator $validator, string $prefix=null) {
    // Check whether the actionProviderElementNames is already set if so get
    // the variable and append the configuration for this action to it.
    // Because we dont want to overwrite the current actionProviderElementNames.
    $elementNames = $this->get_template_vars('configurationElementNames');
    if (empty($elementNames)) {
      $elementNames = array();
    }
    if (!$prefix) {
      $prefix = get_class($validator);
    }
    $configurationElementPreHtml = array();
    $configurationElementPostHtml = array();
    $configurationElementDescriptions = array();
    $elementNames[$prefix] = array();
    foreach($validator->getConfigurationSpecification() as $config_field) {
      $field_name = $prefix.$config_field->getName();
      if ($config_field->getDescription()) {
        $configurationElementDescriptions[$field_name] = $config_field->getDescription();
      }
      $attributes = array(
        'class' => $prefix,
      );

      if (!empty($config_field->getFkEntity())) {
        $attributes['entity'] = $config_field->getFkEntity();
        $attributes['placeholder'] = E::ts('- Select -');
        $attributes['select'] = array('minimumInputLength' => 0);
        if ($config_field->isMultiple()) {
          $attributes['multiple'] = true;
        }
        $this->addEntityRef($field_name, $config_field->getTitle(), $attributes, $config_field->isRequired());
        $elementNames[$prefix][] = $field_name;
      } elseif (!empty($config_field->getOptions())) {
        $attributes['class'] .= ' crm-select2 huge';
        $attributes['placeholder'] = E::ts('- Select -');
        if ($config_field->isMultiple()) {
          $attributes['multiple'] = TRUE;
          $options = $config_field->getOptions();
        }
        else {
          $options = $config_field->getOptions();
        }
        $this->add('select', $field_name, $config_field->getTitle(), $options, $config_field->isRequired(), $attributes);
        $elementNames[$prefix][] = $field_name;
      } elseif ($config_field instanceof WysiwygSpecification && $config_field->getWysiwyg()) {
        $attributes = ['cols' => '80', 'rows' => '8', 'onkeyup' => "return verify(this)", 'preset' => 'civimail'];
        $this->add('wysiwyg', $field_name, $config_field->getTitle(), $attributes, $config_field->isRequired());
        $elementNames[$prefix][] = $field_name;
        $configurationElementPreHtml[$field_name] = '<div style="overflow: hidden;"><div style="float: right;"><input class="crm-token-selector big" id="token'.$field_name.'" data-field="'.$field_name.'" /></div></div>';
        $configurationElementPreHtml[$field_name] .= "
        <script type=\"text/javascript\">
            CRM.$(function($) {
              var token{$field_name} = ".json_encode($config_field->getAvailableTokens()).";
              $('input#token{$field_name}')
              .addClass('crm-action-menu fa-code')
              .change(function() {
                CRM.wysiwyg.insert('#{$field_name}', $(this).val());
                $(this).select2('val', '');
              })
              .crmSelect2({
                  data: token{$field_name},
                  placeholder: '".E::ts('Tokens')."'
               });
            });
         </script>";
      } else {
        $attributes['class'] .= ' huge';
        $this->add('text', $field_name, $config_field->getTitle(), $attributes, $config_field->isRequired());
        $elementNames[$prefix][] = $field_name;
      }

    }
    $this->assign('configurationElementNames', $elementNames);
    $this->assign('configurationElementDescriptions', $configurationElementDescriptions);
    $this->assign('configurationElementPreHtml', $configurationElementPreHtml);
    $this->assign('configurationElementPostHtml', $configurationElementPostHtml);
  }

}
