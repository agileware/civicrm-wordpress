<?php
/**
 * Copyright (C) 2023  Jaap Jansma (jaap.jansma@civicoop.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

use Civi\ActionProvider\Provider;
use CRM_FormProcessor_ExtensionUtil as E;

class CRM_FormProcessor_Form_Configuration extends CRM_FormProcessor_Form_AbstractConfiguration {

  private $availableFields = [];

  protected $outputHandler;

  /**
   * @var \Civi\FormProcessor\OutputHandler\OutputHandlerInterface
   */
  protected $outputHandlerClass;

  protected $outputHandlerConfig = [];

  protected $snippet;

  public function preProcess() {
    parent::preProcess();
    $this->snippet = CRM_Utils_Request::retrieve('snippet', 'String');
    $provider = form_processor_get_action_provider_for_default_data();
    if ($this->formProcessorId) {
      $this->availableFields = CRM_FormProcessor_BAO_FormProcessorAction::getFieldsForMapping($provider, $this->formProcessorId);
    }
    $this->addInputs();
    $this->assign('base_url', $this->getBaseUrl());
    $this->assign('input_base_url', $this->getEditInputUrl());
    $this->assign('validator_base_url', $this->getEditInpuValidatortUrl());
    if (!empty($this->formProcessor['output_handler'])) {
      $this->outputHandler = $this->formProcessor['output_handler']['name'];
    }
    if (isset($this->formProcessor['output_handler_configuration']) && is_array($this->formProcessor['output_handler_configuration'])) {
      $this->outputHandlerConfig = $this->formProcessor['output_handler_configuration'];
    }
    $this->assign('show_only_output_handler_config', false);
    $output_handler = CRM_Utils_Request::retrieve('output_handler', 'String');
    if ($output_handler) {
      $this->outputHandler = $output_handler;
      if ($this->snippet) {
        $this->assign('show_only_output_handler_config', true);
        $this->assign('suppressForm', TRUE);
        $this->controller->_generateQFKey = FALSE;
      }
    }
    if ($this->outputHandler) {
      /** @var \Civi\FormProcessor\OutputHandler\Factory $outputHandlerFactory */
      $outputHandlerFactory = Civi::service('form_processor_output_handler_factory');
      $this->outputHandlerClass = $outputHandlerFactory->getHandlerByName($this->outputHandler);
    }
    $this->assign('outputHandler', $this->outputHandler);
    $this->assign('outputConfigurationTitle', E::ts('Output configuration'));
  }

  /**
   * Returns the action provider
   *
   * @return \Civi\ActionProvider\Provider
   */
  protected function getActionProvider(): Provider {
    return form_processor_get_action_provider();
  }

  protected function getBaseUrl(): string {
    return'civicrm/admin/automation/formprocessor/configuration';
  }

  /**
   * Returns the current url
   *
   * @return string
   */
  protected function getCurrentCurl(): string {
    return CRM_Utils_System::url('civicrm/admin/automation/formprocessor/configuration', array('reset' => 1, 'action' => 'update', 'id' => $this->formProcessorId));
  }

  /**
   * Returns the url for editing a condition.
   *
   * @return string
   */
  protected function getEditConditionUrl(): string {
    return 'civicrm/admin/automation/formprocessor/configuration/condition';
  }

  /**
   * Returns the url for editing an action.
   *
   * @return string
   */
  protected function getEditActionUrl(): string {
    return 'civicrm/admin/automation/formprocessor/configuration/action';
  }

  /**
   * Returns the url for editing a condition.
   *
   * @return string
   */
  protected function getEditDelayUrl(): string {
    return 'civicrm/admin/automation/formprocessor/configuration/actiondelay';
  }

  /**
   * Returns the url for editing an input.
   *
   * @return string
   */
  protected function getEditInputUrl(): string {
    return 'civicrm/admin/automation/formprocessor/configuration/input';
  }

  /**
   * Returns the url for editing an input.
   *
   * @return string
   */
  protected function getEditInpuValidatortUrl(): string {
    return 'civicrm/admin/automation/formprocessor/configuration/inputvalidation';
  }

  /**
   * Returns the name of the Dao Class of the action.
   *
   * @return string
   */
  protected function getActionDaoClass(): string {
    return 'CRM_FormProcessor_DAO_FormProcessorAction';
  }

  /**
   * Returns the action of this configuration
   *
   * @param int $formProcessorId
   *
   * @return array
   */
  protected function getActions(int $formProcessorId): array {
    return CRM_FormProcessor_BAO_FormProcessorAction::getValues(array('form_processor_id' => $formProcessorId));
  }

  public function buildQuickForm() {
    parent::buildQuickForm();
    if ($this->_action == CRM_Core_Action::DELETE) {
      $this->addButtons([
        ['type' => 'next', 'name' => E::ts('Delete'), 'isDefault' => TRUE,],
        ['type' => 'cancel', 'name' => E::ts('Cancel')]
      ]);
    } elseif ($this->_action == CRM_Core_Action::REVERT) {
      $this->addButtons(array(
        array('type' => 'next', 'name' => E::ts('Restore to imported version'), 'isDefault' => TRUE,),
        array('type' => 'cancel', 'name' => E::ts('Cancel'))
      ));
    } else {
      /** @var \Civi\FormProcessor\OutputHandler\Factory $outputHandlerFactory */
      $outputHandlerFactory = Civi::service('form_processor_output_handler_factory');
      $this->add('text', 'title', E::ts('Title'), ['class' => 'huge'], TRUE);
      $this->add('text', 'name', E::ts('Name'), ['class' => 'huge'], FALSE);
      $this->add('textarea', 'description', E::ts('Description'), [
        'size' => 100,
        'rows' => 6,
        'maxlength' => 256,
        'class' => 'huge'
      ]);
      $this->add('checkbox', 'is_active', E::ts('Enabled?'), [], FALSE);
      $this->add('select', 'permission', E::ts('Permission'), CRM_Core_Permission::basicPermissions(), FALSE, [
        'style' => 'min-width:250px',
        'class' => 'huge crm-select2',
        'placeholder' => E::ts('- select -'),
      ]);
      $this->add('select', 'output_handler', E::ts('Output Handler'), $outputHandlerFactory->getHandlerTitles(), TRUE, [
        'style' => 'min-width:250px',
        'class' => 'huge crm-select2',
        'placeholder' => E::ts('- select -'),
      ]);

      if ($this->outputHandlerClass) {
        CRM_FormProcessor_Utils_AddConfigToForm::addConfig('output_handler', $this->outputHandlerClass->getConfigurationSpecification(), $this->outputHandlerConfig, $this, $this->availableFields);
      }
      else {
        $this->assign('configFields', ['output_handler' => []]);
        $this->assign('configDescriptions', ['output_handler' => []]);
        $this->assign('configCollectionFields', ['output_handler' => []]);
      }
    }
  }

  /**
   * Function to set default values (overrides parent function)
   *
   * @return array $defaults
   * @access public
   */
  function setDefaultValues(): array {
    $defaults =  parent::setDefaultValues();
    if (!empty($this->formProcessor)) {
      $defaults['is_active'] = !empty($this->formProcessor['is_active']) ? '1' : '0';
      $defaults['title'] = $this->formProcessor['title'];
      $defaults['name'] = $this->formProcessor['name'];
      $defaults['description'] = !empty($this->formProcessor['description']) ? $this->formProcessor['description'] : '';
      $defaults['permission'] = $this->formProcessor['permission'];
      $defaults['output_handler'] = $this->formProcessor['output_handler']['name'];
    } else {
      /** @var \Civi\FormProcessor\OutputHandler\Factory $outputHandlerFactory */
      $outputHandlerFactory = Civi::service('form_processor_output_handler_factory');
      $defaults['output_handler'] = array_key_first($outputHandlerFactory->getHandlerTitles());
      $defaults['is_active'] = '1';
    }
    return $defaults;
  }

  public function postProcess() {
    if ($this->_action == CRM_Core_Action::DELETE) {
      $session = CRM_Core_Session::singleton();
      civicrm_api3('FormProcessorInstance', 'delete', ['id' => $this->formProcessorId]);
      $session->setStatus(E::ts('Form Processor removed'), E::ts('Removed'), 'success');
      $url = CRM_Utils_System::url('civicrm/admin/formprocessor/search', [], TRUE);
      CRM_Utils_System::redirect($url);
    }
    elseif ($this->_action == CRM_Core_Action::REVERT) {
      $session = CRM_Core_Session::singleton();
      civicrm_api3('FormProcessorInstance', 'revert', array('id' => $this->formProcessorId));
      $session->setStatus(E::ts('Form Processor set to imported version'), E::ts('Form Processor Updated'), 'success');
    } else {
      $submittedValues = $this->getSubmittedValues();
      if ($this->formProcessorId) {
        $params['id'] = $this->formProcessorId;
      }
      $params['is_active'] = !empty($submittedValues['is_active']) ? '1' : '0';
      $params['title'] = $submittedValues['title'];
      if (empty($submittedValues['name'])) {
        $result = civicrm_api3('form_processor_instance', 'check_name', [
          'title' => $submittedValues['title'],
          'id' => $this->formProcessorId
        ]);
        if (!empty($result['name'])) {
          $submittedValues['name'] = $result['name'];
        }
      }
      $params['name'] = $submittedValues['name'];
      $params['description'] = $submittedValues['description'];
      $params['permission'] = $submittedValues['permission'];
      $params['output_handler'] = $submittedValues['output_handler'];
      if ($this->outputHandlerClass) {
        $this->outputHandlerConfig = CRM_FormProcessor_Utils_AddConfigToForm::processConfig($submittedValues, 'output_handler', $this->outputHandlerClass->getConfigurationSpecification());
      }
      $params['output_handler_configuration'] = $this->outputHandlerConfig;
      $result = civicrm_api3('FormProcessorInstance', 'create', $params);
      $this->formProcessorId = $result['id'];
    }
    parent::postProcess();
  }

  protected function addInputs() {
    $inputs = [];
    if ($this->formProcessorId) {
      $inputs = CRM_FormProcessor_BAO_FormProcessorInput::getValues(['form_processor_id' => $this->formProcessorId]);
    }
    $inputs = $this->correctWeight($inputs);
    CRM_Utils_Weight::addOrder($inputs, 'CRM_FormProcessor_DAO_FormProcessorInput', 'id', $this->currentUrl, 'form_processor_id='.$this->formProcessorId);
    foreach($inputs as $idx => $input) {
      $inputs[$idx]['type_label'] = '<i aria-hidden="true" class="crm-i fa-exclamation-triangle" title="{ts}Invalid type{/ts}"></i>';
      if (!empty($input['type']) && $input['type'] instanceof \Civi\FormProcessor\Type\AbstractType) {
        $inputs[$idx]['type_label'] = $input['type']->getLabel();
      } elseif (is_string($input['type'])) {
        $inputs[$idx]['type_label'] .= $input['type'];
      }
    }
    $this->assign('inputs', $inputs);
  }

  protected function requireFormProcessorId(): bool {
    return false;
  }

}
