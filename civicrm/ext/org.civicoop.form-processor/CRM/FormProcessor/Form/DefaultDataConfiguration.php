<?php
/**
 * Copyright (C) 2023  Jaap Jansma (jaap.jansma@civicoop.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

use Civi\ActionProvider\Provider;
use CRM_FormProcessor_ExtensionUtil as E;

class CRM_FormProcessor_Form_DefaultDataConfiguration extends CRM_FormProcessor_Form_AbstractConfiguration {

  private $availableFields = [];

  public function preProcess() {
    parent::preProcess();
    $provider = form_processor_get_action_provider_for_default_data();
    $this->availableFields = CRM_FormProcessor_BAO_FormProcessorDefaultDataAction::getFieldsForMapping($provider, $this->formProcessorId);
    $this->addInputs();
    $this->assign('input_base_url', $this->getEditInputUrl());
    $this->assign('validator_base_url', $this->getEditInpuValidatortUrl());
  }

  /**
   * Returns the action provider
   *
   * @return \Civi\ActionProvider\Provider
   */
  protected function getActionProvider(): Provider {
    return form_processor_get_action_provider_for_default_data();
  }

  /**
   * Returns the current url
   *
   * @return string
   */
  protected function getCurrentCurl(): string {
    return CRM_Utils_System::url('civicrm/admin/automation/formprocessor/defaultdataconfiguration', array('reset' => 1, 'action' => 'update', 'id' => $this->formProcessorId));
  }

  /**
   * Returns the url for editing a condition.
   *
   * @return string
   */
  protected function getEditConditionUrl(): string {
    return 'civicrm/admin/automation/formprocessor/defaultdataconfiguration/condition';
  }

  /**
   * Returns the url for editing a condition.
   *
   * @return string
   */
  protected function getEditDelayUrl(): string {
    return 'civicrm/admin/automation/formprocessor/defaultdataconfiguration/actiondelay';
  }

  /**
   * Returns the url for editing an action.
   *
   * @return string
   */
  protected function getEditActionUrl(): string {
    return 'civicrm/admin/automation/formprocessor/defaultdataconfiguration/action';
  }

  /**
   * Returns the url for editing an input.
   *
   * @return string
   */
  protected function getEditInputUrl(): string {
    return 'civicrm/admin/automation/formprocessor/defaultdataconfiguration/input';
  }

  /**
   * Returns the url for editing an input.
   *
   * @return string
   */
  protected function getEditInpuValidatortUrl(): string {
    return 'civicrm/admin/automation/formprocessor/defaultdataconfiguration/inputvalidation';
  }

  /**
   * Returns the name of the Dao Class of the action.
   *
   * @return string
   */
  protected function getActionDaoClass(): string {
    return 'CRM_FormProcessor_DAO_FormProcessorDefaultDataAction';
  }

  /**
   * Returns the action of this configuration
   *
   * @param int $formProcessorId
   *
   * @return array
   */
  protected function getActions(int $formProcessorId): array {
    return CRM_FormProcessor_BAO_FormProcessorDefaultDataAction::getValues(array('form_processor_id' => $formProcessorId));
  }

  public function buildQuickForm() {
    parent::buildQuickForm();
    $inputs = CRM_FormProcessor_BAO_FormProcessorInput::getValues(['form_processor_id' => $this->formProcessorId]);
    $default_data_output_configuration = [];
    foreach ($inputs as $input) {
      $this->add('select','input_'.$input['name'], E::ts('Input :: %1', [1 => $input['title']]), $this->availableFields, FALSE, [
        'style' => 'min-width:250px',
        'class' => 'crm-select2 huge',
        'placeholder' => E::ts('- select -'),
        'multiple' => false,
      ]);
      $default_data_output_configuration[] = 'input_'.$input['name'];
    }
    $this->assign('default_data_output_configuration', $default_data_output_configuration);
    $this->add('checkbox', 'enable_default_data', E::ts('Enable default data retrieval?'), [], FALSE);
  }

  /**
   * Function to set default values (overrides parent function)
   *
   * @return array $defaults
   * @access public
   */
  function setDefaultValues(): array {
    $defaults =  parent::setDefaultValues();
    $default_data_output_configuration = [];
    foreach ($this->formProcessor['default_data_output_configuration'] as $field => $mapping) {
      $defaults['input_'.$field] =$mapping;
    }
    $defaults['enable_default_data'] = !empty($this->formProcessor['enable_default_data']) ?  '1' : '0';
    return $defaults;
  }

  public function postProcess() {
    $submittedValues = $this->getSubmittedValues();
    $inputs = CRM_FormProcessor_BAO_FormProcessorInput::getValues(['form_processor_id' => $this->formProcessorId]);
    $default_data_output_configuration = [];
    foreach ($inputs as $input) {
      if (!empty($submittedValues['input_'.$input['name']])) {
        $default_data_output_configuration[$input['name']] = $submittedValues['input_'.$input['name']];
      }
    }
    $params['id'] = $this->formProcessorId;
    $params['default_data_output_configuration'] = $default_data_output_configuration;
    $params['enable_default_data'] = !empty($submittedValues['enable_default_data']) ?  '1' : '0';
    civicrm_api3('FormProcessorInstance', 'create', $params);

    parent::postProcess();
  }

  protected function addInputs() {
    $inputs = [];
    if ($this->formProcessorId) {
      $inputs = CRM_FormProcessor_BAO_FormProcessorDefaultDataInput::getValues(['form_processor_id' => $this->formProcessorId]);
    }
    $inputs = $this->correctWeight($inputs);
    CRM_Utils_Weight::addOrder($inputs, 'CRM_FormProcessor_DAO_FormProcessorDefaultDataInput', 'id', $this->currentUrl, 'form_processor_id='.$this->formProcessorId);
    foreach($inputs as $idx => $input) {
      $inputs[$idx]['type_label'] = '<i aria-hidden="true" class="crm-i fa-exclamation-triangle" title="{ts}Invalid type{/ts}"></i>';
      if (!empty($input['type']) && $input['type'] instanceof \Civi\FormProcessor\Type\AbstractType) {
        $inputs[$idx]['type_label'] = $input['type']->getLabel();
      } elseif (is_string($input['type'])) {
        $inputs[$idx]['type_label'] .= $input['type'];
      }
    }
    $this->assign('inputs', $inputs);
  }

}
