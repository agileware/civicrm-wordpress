<?php
/**
 * Copyright (C) 2023  Jaap Jansma (jaap.jansma@civicoop.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

use Civi\ActionProvider\Parameter\WysiwygSpecification;
use Civi\FormProcessor\Config\Specification;
use Civi\FormProcessor\Config\SpecificationBag;
use Civi\FormProcessor\Config\SpecificationCollection;
use Civi\FormProcessor\Type\AbstractType;
use CRM_FormProcessor_ExtensionUtil as E;

abstract class CRM_FormProcessor_Form_AbstractConfigurationInput extends CRM_Core_Form {

  /** @var int */
  protected int $formProcessorId;

  /**
   * @var array
   */
  protected array $formProcessor;

  protected ?int $inputId;

  protected string $inputType = '';

  protected array $input = array();

  protected array $inputConfiguration = array();

  protected array $availableFields = array();

  /**
   * @var \Civi\FormProcessor\Type\AbstractType
   */
  protected ?AbstractType $inputClass = null;

  protected ?string $snippet;

  /**
   * Returns the API 3 entity name.
   * E.g. FormProcessorValidateAction or FormProcessorDefaultDataAction
   * @return string
   */
  abstract protected function getApi3Entity(): string;

  /**
   * Returns the return url
   *
   * @return string
   */
  abstract protected function getReturnUrl(): string;

  /**
   * @return string
   */
  abstract protected function getBaseUrl(): string;

  abstract protected function showDefaultDataMapping(): bool;

  abstract protected function showInputMapping(): bool;

  public function preProcess() {
    parent::preProcess();
    $this->assign('mappingElementNames', ['mapping' => [], 'default_mapping' => []]);
    $this->assign('mappingElementDescriptions', []);
    $this->snippet = CRM_Utils_Request::retrieve('snippet', 'String');
    if ($this->snippet) {
      $this->assign('suppressForm', TRUE);
      $this->controller->_generateQFKey = FALSE;
    }

    $this->inputId = CRM_Utils_Request::retrieve('id', 'Integer');
    $this->formProcessorId = CRM_Utils_Request::retrieve('form_processor_id', 'Integer', $this, TRUE);
    $this->formProcessor = civicrm_api3('FormProcessorInstance', 'getsingle', ['id' => $this->formProcessorId]);
    $this->assign('form_processor_id', $this->formProcessorId);

    /** @var \Civi\FormProcessor\Type\Factory $provider */
    $typeFactory = Civi::service('form_processor_type_factory');
    if ($this->inputId) {
      try {
        $this->input = civicrm_api3($this->getApi3Entity(), 'getsingle', ['id' => $this->inputId]);
        $this->assign('inputObject', $this->input);
        $this->inputClass = $typeFactory->getTypeByName($this->input['type']['name']);
        $this->inputType = $this->input['type']['name'];
        if (isset($this->input['configuration'])) {
          $this->inputConfiguration = $this->input['configuration'];
        }
      } catch (API_Exception $e) {
      }
    }

    $type = CRM_Utils_Request::retrieve('type', 'String');
    if ($type) {
      $this->inputType = $type;
      $this->inputClass = $typeFactory->getTypeByName($type);
    }
    $this->assign('configurationSectionTitle', E::ts('Configuration'));
    $this->assign('inputType', $this->inputType);
    $this->assign('inputClass', $this->inputClass);
    $this->availableFields = $this->getAvailableFieldsForMapping($this->formProcessorId, $this->inputId);
    $this->assign('base_url', $this->getBaseUrl());
    $this->assign('api3_entity_name', $this->getApi3Entity());
  }

  public function buildQuickForm() {
    if (!$this->snippet) {
      $this->add('hidden', 'id');
      $this->add('hidden', 'form_processor_id');
    }

    if ($this->_action == CRM_Core_Action::DELETE) {
      $this->addButtons(array(
        array('type' => 'next', 'name' => E::ts('Delete'), 'isDefault' => TRUE,),
        array('type' => 'cancel', 'name' => E::ts('Cancel')),
      ));
    } else {
      /** @var \Civi\FormProcessor\Type\Factory $provider */
      $typeFactory = Civi::service('form_processor_type_factory');
      $this->add('select', 'type', E::ts('Type'), $typeFactory->getTypeLabels(), TRUE, [
        'style' => 'min-width:250px',
        'class' => 'crm-select2 huge',
        'placeholder' => E::ts('- select -'),
      ]);
      $this->add('text', 'title', E::ts('Title'), [
        'size' => 100,
        'maxlength' => 255,
      ], TRUE);
      $this->add('text', 'name', E::ts('Name'), [
        'size' => 100,
        'maxlength' => 255,
      ], FALSE);
      $this->add('text', 'default_value', E::ts('Default Value'), [
        'maxlength' => 255,
        'class' => 'huge',
      ], FALSE);
      $this->add('checkbox', 'is_required', E::ts('Is required'), [], FALSE);
      $this->add('checkbox', 'include_in_formatted_params', E::ts('Include in formatted parameters'), [], FALSE);

      if ($this->inputClass) {
        CRM_FormProcessor_Utils_AddConfigToForm::addConfig($this->inputType, $this->inputClass->getConfigurationSpecification(), $this->inputConfiguration, $this, $this->availableFields);
        if ($this->showInputMapping()) {
          $config = [];
          if (isset($this->input['parameter_mapping']) && is_array($this->input['parameter_mapping'])) {
            foreach($this->input['parameter_mapping'] as $key => $value) {
              $config['mapping'.$key] = 'input.'.$value;
            }
          }
          $this->addParameterMapping('mapping', $this->inputClass->getParameterSpecification(), $this->getAvailableFieldsForMapping());
          $this->setDefaults($config);
        }
        if ($this->showDefaultDataMapping()) {
          $config = [];
          if (isset($this->input['default_data_parameter_mapping']) && is_array($this->input['default_data_parameter_mapping'])) {
            foreach($this->input['default_data_parameter_mapping'] as $key => $value) {
              $config['default_mapping'.$key] = 'input.'.$value;
            }
          }
          $this->addParameterMapping('default_mapping', $this->inputClass->getDefaultsParameterSpecification(), $this->getAvailableFieldsForDefaultDataMapping());
          $this->setDefaults($config);
        }
      }

      $this->addButtons([
        ['type' => 'next', 'name' => E::ts('Save'), 'isDefault' => TRUE,],
        ['type' => 'cancel', 'name' => E::ts('Cancel')],
      ]);
    }

    parent::buildQuickForm();
  }

  /**
   * Function to set default values (overrides parent function)
   *
   * @return array $defaults
   * @access public
   */
  function setDefaultValues(): array {
    $defaults = [];
    $defaults['form_processor_id'] = $this->formProcessorId;
    if ($this->inputId) {
      $defaults['id'] = $this->inputId;
      $defaults['type'] = $this->input['type'];
      $defaults['title'] = $this->input['title'];
      $defaults['name'] = $this->input['name'];
      $defaults['is_required'] = !empty($this->input['is_required']) ? '1' : '0';
      $defaults['include_in_formatted_params'] = !empty($this->input['include_formatted_params']) ?  '1' : '0';
      $defaults['default_value'] = $this->input['default_value'];
    }
    return $defaults;
  }

  /**
   * Function that can be defined in Form to override or.
   * perform specific action on cancel action
   */
  public function cancelAction() {
    $this->formProcessorId = CRM_Utils_Request::retrieve('form_processor_id', 'Integer', $this, TRUE);
    CRM_Utils_System::redirect($this->getReturnUrl());
  }

  public function postProcess() {
    $redirectUrl = $this->getReturnUrl();
    if ($this->_action == CRM_Core_Action::DELETE) {
      $session = CRM_Core_Session::singleton();
      civicrm_api3($this->getApi3Entity(), 'delete', array('id' => $this->inputId));
      $session->setStatus(E::ts('Input removed'), E::ts('Removed'), 'success');
      CRM_Utils_System::redirect($redirectUrl);
    }

    $values = $this->exportValues();
    $params['type'] = $values['type'];
    $params['title'] = $values['title'];
    $params['default_value'] = $values['default_value'];
    $params['is_required'] = !empty($values['is_required']) ? '1' : '0';
    $params['include_formatted_params'] = !empty($values['include_in_formatted_params']) ? '1' : '0';
    if (empty($values['name'])) {
      $result = civicrm_api3($this->getApi3Entity(), 'check_name', ['title' => $values['title'], 'form_processor_id' => $this->formProcessorId, 'id' => $this->inputId]);
      if (!empty($result['name'])) {
        $values['name'] = $result['name'];
      }
    }
    $params['name'] = $values['name'];
    $params['form_processor_id'] = $this->formProcessorId;
    if ($this->inputId) {
      $params['id'] = $this->inputId;
    }
    if ($this->inputClass) {
      $configuration = array();
      $prefix = $this->inputType;
      foreach($this->inputClass->getConfigurationSpecification() as $config_field) {
        if ($config_field instanceof SpecificationCollection) {
          $result[$config_field->getName()] = [];
          $count = $values[$config_field->getName() . 'Count'];
          for ($i = 1; $i <= $count; $i++) {
            foreach ($config_field->getSpecificationBag() as $subSpec) {
              $name = $prefix . $subSpec->getName();
              if (isset($values[$name])) {
                $configuration[$config_field->getName()][$i - 1][$subSpec->getName()] = $values[$name][$i];
              }
            }
          }
        }
        elseif (isset($values[$prefix.$config_field->getName()])) {
          $configuration[$config_field->getName()] = $values[$prefix.$config_field->getName()];
        }
      }
      $params['configuration'] = $configuration;
    }
    $params['parameter_mapping'] = [];
    if ($this->showInputMapping()) {
      /** @var \Civi\FormProcessor\Config\SpecificationInterface $parameter */
      foreach($this->inputClass->getParameterSpecification() as $parameter) {
        if (isset($values['mapping'.$parameter->getName()])) {
          // Strip 'input. from the parameter mapping
          $params['parameter_mapping'][$parameter->getName()] = substr($values['mapping'.$parameter->getName()], 6);
        }
      }
    }
    $params['default_data_parameter_mapping'] = [];
    if ($this->showDefaultDataMapping()) {
      /** @var \Civi\FormProcessor\Config\SpecificationInterface $parameter */
      foreach($this->inputClass->getDefaultsParameterSpecification() as $parameter) {
        if (isset($values['default_mapping'.$parameter->getName()])) {
          // Strip 'input. from the parameter mapping
          $params['default_data_parameter_mapping'][$parameter->getName()] = substr($values['default_mapping'.$parameter->getName()], 6);
        }
      }
    }

    $result = civicrm_api3($this->getApi3Entity(), 'create', $params);

    CRM_Utils_System::redirect($redirectUrl);
  }

  public function addParameterMapping(string $prefix, SpecificationBag $paremeters, array $availableFields) {
    $elementNames = $this->get_template_vars('mappingElementNames');
    $elementDescriptions = $this->get_template_vars('mappingElementDescriptions');
    if (empty($elementNames)) {
      $elementNames = array();
    }
    if (empty($elementNames[$prefix])) {
      $elementNames[$prefix] = [];
    }
    if (empty($elementDescriptions)) {
      $elementDescriptions = array();
    }

    /** @var \Civi\FormProcessor\Config\SpecificationInterface $paremeter */
    foreach($paremeters as $paremeter) {
      if ($paremeter instanceof Specification) {
        $name = $prefix.$paremeter->getName();
        $attributes = [];
        $attributes['class'] = $prefix . ' crm-select2 huge';
        $attributes['placeholder'] = E::ts('- Select -');
        $this->add('select', $name, $paremeter->getTitle(), $availableFields, $paremeter->isRequired(), $attributes);
        $elementDescriptions[$name] = $paremeter->getDescription();
        $elementNames[$prefix][] = $name;
      }
    }
    $this->assign('mappingElementNames', $elementNames);
    $this->assign('mappingElementDescriptions', $elementDescriptions);
  }

  /**
   * BuildForm helper. Adds the elements to the form.
   *
   * @param AbstractType $type
   *   The input type class
   * @param $prefix
   */
  public function addConfigToForm(AbstractType $type, string $prefix=null) {
    // Check whether the actionProviderElementNames is already set if so get
    // the variable and append the configuration for this action to it.
    // Because we dont want to overwrite the current actionProviderElementNames.
    $elementNames = $this->get_template_vars('configurationElementNames');
    if (empty($elementNames)) {
      $elementNames = array();
    }
    if (!$prefix) {
      $prefix = get_class($type);
    }
    $configurationElementPreHtml = array();
    $configurationElementPostHtml = array();
    $configurationElementDescriptions = array();
    $elementNames[$prefix] = array();
    foreach($type->getConfigurationSpecification() as $config_field) {
      $field_name = $prefix.$config_field->getName();
      if ($config_field->getDescription()) {
        $configurationElementDescriptions[$field_name] = $config_field->getDescription();
      }
      $attributes = array(
        'class' => $prefix,
      );

      if (!empty($config_field->getFkEntity())) {
        $attributes['entity'] = $config_field->getFkEntity();
        $attributes['placeholder'] = E::ts('- Select -');
        $attributes['select'] = array('minimumInputLength' => 0);
        if ($config_field->isMultiple()) {
          $attributes['multiple'] = true;
        }
        $this->addEntityRef($field_name, $config_field->getTitle(), $attributes, $config_field->isRequired());
        $elementNames[$prefix][] = $field_name;
      } elseif (!empty($config_field->getOptions())) {
        $attributes['class'] .= ' crm-select2 huge';
        $attributes['placeholder'] = E::ts('- Select -');
        if ($config_field->isMultiple()) {
          $attributes['multiple'] = TRUE;
          $options = $config_field->getOptions();
        }
        else {
          $options = $config_field->getOptions();
        }
        $this->add('select', $field_name, $config_field->getTitle(), $options, $config_field->isRequired(), $attributes);
        $elementNames[$prefix][] = $field_name;
      } elseif ($config_field instanceof WysiwygSpecification && $config_field->getWysiwyg()) {
        $attributes = ['cols' => '80', 'rows' => '8', 'onkeyup' => "return verify(this)", 'preset' => 'civimail'];
        $this->add('wysiwyg', $field_name, $config_field->getTitle(), $attributes, $config_field->isRequired());
        $elementNames[$prefix][] = $field_name;
        $configurationElementPreHtml[$field_name] = '<div style="overflow: hidden;"><div style="float: right;"><input class="crm-token-selector big" id="token'.$field_name.'" data-field="'.$field_name.'" /></div></div>';
        $configurationElementPreHtml[$field_name] .= "
        <script type=\"text/javascript\">
            CRM.$(function($) {
              var token{$field_name} = ".json_encode($config_field->getAvailableTokens()).";
              $('input#token{$field_name}')
              .addClass('crm-action-menu fa-code')
              .change(function() {
                CRM.wysiwyg.insert('#{$field_name}', $(this).val());
                $(this).select2('val', '');
              })
              .crmSelect2({
                  data: token{$field_name},
                  placeholder: '".E::ts('Tokens')."'
               });
            });
         </script>";
      } else {
        $attributes['class'] .= ' huge';
        $this->add('text', $field_name, $config_field->getTitle(), $attributes, $config_field->isRequired());
        $elementNames[$prefix][] = $field_name;
      }

    }
    $this->assign('configurationElementNames', $elementNames);
    $this->assign('configurationElementDescriptions', $configurationElementDescriptions);
    $this->assign('configurationElementPreHtml', $configurationElementPreHtml);
    $this->assign('configurationElementPostHtml', $configurationElementPostHtml);
  }

  /**
   * Returns an array of available fields for the mapping
   *
   * @return array
   */
  protected function getAvailableFieldsForMapping(): array {
    $return = [];
    foreach($this->formProcessor['inputs'] as $input) {
      if ($input['id'] == $this->inputId) {
        break;
      }
      $return['input.'.$input['name']] = $input['title'];
    }
    return $return;
  }

  /**
   * Returns an array of available fields for the default data mapping
   *
   * @return array
   */
  protected function getAvailableFieldsForDefaultDataMapping(): array {
    $return = [];
    foreach($this->formProcessor['default_data_inputs'] as $input) {
      if ($input['id'] == $this->inputId) {
        break;
      }
      $return['input.'.$input['name']] = $input['title'];
    }
    return $return;
  }

}
