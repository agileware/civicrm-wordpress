<?php
/**
 * @author Jaap Jansma <jaap.jansma@civicoop.org>
 * @license AGPL-3.0
 */

use CRM_FormProcessor_ExtensionUtil as E;

class CRM_FormProcessor_Form_Export extends CRM_Core_Form {

  private $formProcessorId;

  private $dataProcessor;

  /**
   * @var \Civi\DataProcessor\ProcessorType\AbstractProcessorType
   */
  private $dataProcessorClass;

  private $currentUrl;

  /**
   * Function to perform processing before displaying form (overrides parent function)
   *
   * @access public
   */
  function preProcess() {
    $this->formProcessorId = CRM_Utils_Request::retrieve('id', 'Integer');
    $exporter = new \Civi\FormProcessor\Exporter\ExportToJson();
    $data = (array) $exporter->export($this->formProcessorId);
    $file_download_name = $data['name'].'.json';
    $mime_type = 'application/json';
    $buffer = json_encode($data, JSON_PRETTY_PRINT);
    CRM_Utils_System::download(
      $file_download_name,
      $mime_type,
      $buffer,
      NULL,
      TRUE,
      'download'
    );
  }

}
