<?php
/**
 * @author Jaap Jansma <jaap.jansma@civicoop.org>
 * @license AGPL-3.0
 */

use CRM_FormProcessor_ExtensionUtil as E;

class CRM_FormProcessor_Form_Import extends CRM_Core_Form {

  /**
   * Function to perform processing before displaying form (overrides parent function)
   *
   * @access public
   */
  function preProcess() {
    parent::preProcess();
  }

  public function buildQuickForm() {
    $this->add('file', 'uploadFile', E::ts('Import Form Processor File'), ['size' => 30, 'maxlength' => 255], TRUE);
    $this->addButtons(array(
      array('type' => 'next', 'name' => E::ts('Import'), 'isDefault' => TRUE,),
      array('type' => 'cancel', 'name' => E::ts('Cancel'))
    ));
  }

  public function postProcess() {
    $values = $this->getSubmitValues(True);
    // Check for JSON extension required
    $filetype = $values['uploadFile']['type'];
    $redirectUrl = CRM_Utils_System::url('civicrm/admin/formprocessor/search');
    if($filetype != 'application/json'){
      CRM_Core_Session::setStatus(E::ts('Imported file should be a json file'), '', 'error');
    } else {
      $config = CRM_Core_Config::singleton();
      $uniqID = md5(uniqid(rand(), TRUE));
      $filename = $values['uploadFile']['name'].'.'.$uniqID;
      $directoryName = $config->customFileUploadDir . 'FormProcessor';
      CRM_Utils_File::createDir($directoryName);
      CRM_Utils_File::restrictAccess($directoryName.DIRECTORY_SEPARATOR, TRUE);
      if (!rename($values['uploadFile']['tmp_name'], $directoryName . DIRECTORY_SEPARATOR . $filename)) {
        CRM_Core_Error::statusBounce(ts('Could not move custom file to custom upload directory'));
      }
      $result = civicrm_api3('FormProcessorInstance', 'import', ['file' => $filename, 'import_locally' => '1']);
      CRM_Core_Session::setStatus(E::ts('Imported form processor'), '', 'success');
    }
    CRM_Utils_System::redirect($redirectUrl);
  }

}
