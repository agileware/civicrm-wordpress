<?php
/**
 * Copyright (C) 2023  Jaap Jansma (jaap.jansma@civicoop.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

use Civi\ActionProvider\Provider;
use CRM_FormProcessor_ExtensionUtil as E;

class CRM_FormProcessor_Form_ValidationConfiguration extends CRM_FormProcessor_Form_AbstractConfiguration {

  /**
   * Returns the action provider
   *
   * @return \Civi\ActionProvider\Provider
   */
  protected function getActionProvider(): Provider {
    return form_processor_get_action_provider_for_validation();
  }

  /**
   * Returns the current url
   *
   * @return string
   */
  protected function getCurrentCurl(): string {
    return CRM_Utils_System::url('civicrm/admin/automation/formprocessor/validatorconfiguration', array('reset' => 1, 'action' => 'update', 'id' => $this->formProcessorId));
  }

  /**
   * Returns the url for editing a condition.
   *
   * @return string
   */
  protected function getEditConditionUrl(): string {
    return 'civicrm/admin/automation/formprocessor/validatorconfiguration/condition';
  }

  /**
   * Returns the url for editing a condition.
   *
   * @return string
   */
  protected function getEditDelayUrl(): string {
    return 'civicrm/admin/automation/formprocessor/validatorconfiguration/actiondelay';
  }

  /**
   * Returns the url for editing an action.
   *
   * @return string
   */
  protected function getEditActionUrl(): string {
    return 'civicrm/admin/automation/formprocessor/validatorconfiguration/action';
  }

  /**
   * Returns the action of this configuration
   *
   * @param int $formProcessorId
   *
   * @return array
   */
  protected function getActions(int $formProcessorId): array {
    return CRM_FormProcessor_BAO_FormProcessorValidateAction::getValues(array('form_processor_id' => $formProcessorId));
  }

  /**
   * Returns the name of the Dao Class of the action.
   *
   * @return string
   */
  protected function getActionDaoClass(): string {
    return 'CRM_FormProcessor_DAO_FormProcessorValidateAction';
  }

  public function preProcess() {
    parent::preProcess();
    $this->addValidators();
  }

  protected function addValidators() {
    /** @var \Civi\ActionProvider\Provider $provider */
    $provider = form_processor_get_action_provider_for_validation();
    $this->assign('validator_types', $provider->getValidatorTitles());
    $validators = CRM_FormProcessor_BAO_FormProcessorValidateValidator::getValues(array('form_processor_id' => $this->formProcessorId));
    $validators = $this->correctWeight($validators);
    CRM_Utils_Weight::addOrder($validators, 'CRM_FormProcessor_DAO_FormProcessorValidateValidator', 'id', $this->currentUrl, 'form_processor_id='.$this->formProcessorId);
    $this->assign('validators', $validators);
  }


}
