<?php

use CRM_FormProcessor_ExtensionUtil as E;

class CRM_FormProcessor_Status {

  const IN_DATABASE = 1;
  const IN_CODE = 2;
  const OVERRIDDEN = 3;

  public static function statusLabels() {
    return array(
      CRM_FormProcessor_Status::IN_DATABASE => E::ts('Created locally'),
      CRM_FormProcessor_Status::IN_CODE => E::ts('Imported'),
      CRM_FormProcessor_Status::OVERRIDDEN => E::ts('Imported and changed locally'),
    );
  }

}
