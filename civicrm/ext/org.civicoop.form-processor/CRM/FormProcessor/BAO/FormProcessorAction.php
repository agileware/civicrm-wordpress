<?php

use CRM_FormProcessor_ExtensionUtil as E;

/**
 * @author Jaap Jansma (CiviCooP) <jaap.jansma@civicoop.org>
 * @license http://www.gnu.org/licenses/agpl-3.0.html
 */
class CRM_FormProcessor_BAO_FormProcessorAction extends CRM_FormProcessor_DAO_FormProcessorAction {

  /**
   * Function to get values
   *
   * @return array $result found rows with data
   * @access public
   * @static
   */
  public static function getValues($params) {
    $actionProvider = form_processor_get_action_provider();

    $result = [];
    $action = new CRM_FormProcessor_BAO_FormProcessorAction();
    if (!empty($params)) {
      $fields = self::fields();
      foreach ($params as $key => $value) {
        if (isset($fields[$key])) {
          $action->$key = $value;
        }
      }
    }
    $action->orderBy('weight');
    $action->find();
    while ($action->fetch()) {
      $row = [];
      self::storeValues($action, $row);
      if (!empty($row['form_processor_id'])) {
        if (isset($row['configuration']) && is_string($row['configuration'])) {
          $row['configuration'] = json_decode($row['configuration'], TRUE);
        }
        if (empty($row['configuration']) || !is_array($row['configuration'])) {
          $row['configuration'] = array();
        }

        if (isset($row['mapping']) && is_string($row['mapping'])) {
          $row['mapping'] = json_decode($row['mapping'], TRUE);
        }
        if (empty($row['mapping']) || !is_array($row['mapping'])) {
          $row['mapping'] = [];
        }

        if (isset($row['condition_configuration']) && is_string($row['condition_configuration'])) {
          $row['condition_configuration'] = json_decode($row['condition_configuration'], TRUE);
        }
        if (empty($row['condition_configuration']) || !is_array($row['condition_configuration'])) {
          $row['condition_configuration'] = null;
        }
        if (isset($row['condition_configuration']) && $row['condition_configuration']) {
          $conditionClass = $actionProvider->getConditionByName($row['condition_configuration']['name']);
          $row['condition_configuration']['title'] = $conditionClass->getTitle();
        }
        if (isset($row['delay_configuration']) && is_string($row['delay_configuration'])) {
          $row['delay_configuration'] = json_decode($row['delay_configuration'], TRUE);
        }
        if (empty($row['delay_configuration']) || !is_array($row['delay_configuration'])) {
          $row['delay_configuration'] = null;
        }

        $result[$row['id']] = $row;
      }
      else {
        //invalid input because no there is no form processor
        CRM_FormProcessor_BAO_FormProcessorAction::deleteWithId($row['id']);
      }
    }
    return $result;
  }

  /**
   * Function to add or update form processor action
   *
   * @param array $params
   *
   * @return array $result
   * @access public
   * @throws Exception when params is empty
   * @static
   */
  public static function add($params) {
    $result = [];
    if (empty($params)) {
      throw new Exception('Params can not be empty when adding or updating a form processor action');
    }

    if (empty($params['id']) && empty($params['weight'])) {
      $params['weight'] = \CRM_Core_DAO::singleValueQuery("SELECT COUNT(*) + 1 FROM `".self::getTableName()."` WHERE `form_processor_id` = %1", [1=>[$params['form_processor_id'], 'Integer']]);
    }

    if (!empty($params['id'])) {
      CRM_Utils_Hook::pre('edit', 'FormProcessorAction', $params['id'], $params);
    }
    else {
      CRM_Utils_Hook::pre('create', 'FormProcessorAction', NULL, $params);
    }

    $action = new CRM_FormProcessor_BAO_FormProcessorAction();
    $fields = self::fields();
    foreach ($params as $key => $value) {
      if (isset($fields[$key])) {
        $action->$key = $value;
      }
    }

    if (isset($action->configuration) && is_array($action->configuration)) {
      $action->configuration = json_encode($action->configuration);
    }
    if (isset ($action->mapping) && is_array($action->mapping)) {
      $action->validateSavedMapping();
      $action->mapping = json_encode($action->mapping);
    }
    if (isset($action->condition_configuration) && is_array($action->condition_configuration)) {
      // Delete condition if set to "No condition".
      if ($action->condition_configuration['name']) {
        $action->condition_configuration = json_encode($action->condition_configuration);
      }
      else {
        $action->condition_configuration = '';
      }
    }
    if (isset($action->delay_configuration) && is_array($action->delay_configuration)) {
      $action->delay_configuration = json_encode($action->delay_configuration);
    }
    $action->save();
    self::storeValues($action, $result);

    if (!empty($params['id'])) {
      CRM_Utils_Hook::post('edit', 'FormProcessorAction', $action->id, $action);
    }
    else {
      CRM_Utils_Hook::post('create', 'FormProcessorAction', $action->id, $action);
    }

    return $result;
  }

  /**
   * Function to delete a form processor action with id
   *
   * @param int $id
   *
   * @throws Exception when $id is empty
   * @access public
   * @static
   */
  public static function deleteWithId($id) {
    if (empty($id)) {
      throw new Exception('id can not be empty when attempting to delete a form processor action');
    }

    CRM_Utils_Hook::pre('delete', 'FormProcessorAction', $id, CRM_Core_DAO::$_nullArray);

    $action = new CRM_FormProcessor_BAO_FormProcessorAction();
    $action->id = $id;
    if ($action->find(TRUE)) {
      $action->delete();
    }

    CRM_Utils_Hook::post('delete', 'FormProcessorAction', $id, CRM_Core_DAO::$_nullArray);

    return;
  }

  /**
   * Function to delete all actions with a form processor instance id
   *
   * @param int $formProcessorId
   *
   * @access public
   * @static
   */
  public static function deleteWithFormProcessorInstanceId(int $formProcessorId) {
    $action = new CRM_FormProcessor_BAO_FormProcessorAction();
    $action->form_processor_id = $formProcessorId;
    $action->find(FALSE);
    while ($action->fetch()) {
      self::deleteWithId($action->id);
    }
  }

  /**
   * Ensure that all the mapping values are valid (e.g. field exists, input exists) before writing to db.
   */
  private function validateSavedMapping() : void {
    $actionProvider = form_processor_get_action_provider();
    $actionClass = $actionProvider->getActionByName($this->type);
    if (!$actionClass) {
      // Action class could not be found.
      // This could be the case during the import of a form processor
      // with custom actions.
      return;
    }
    $specificationBag = $actionClass->getParameterSpecification();
    $validInputNames = array_column(CRM_FormProcessor_BAO_FormProcessorInput::getValues(['form_processor_id' => $this->form_processor_id]), 'name');
    foreach ($this->mapping as $key => $value) {
      // First make sure this mapping is still valid (e.g. field hasn't been deleted).
      $spec = $specificationBag->getSpecificationByName($key);
      if (!$spec) {
        unset($this->mapping[$key]);
        continue;
      }
      // Now make sure the inputs are still deleted (e.g. haven't been deleted).
      if ($spec->isMultiple()) {
        $valueArray = is_array($value) ? $value : explode(',', $value);
        foreach ($valueArray as $k => $singleValue) {
          $inputTypeAndName = explode('.', $singleValue);
          if (!empty($inputTypeAndName[0]) && $inputTypeAndName[0] === 'input' && !empty($inputTypeAndName[1]) && !in_array($inputTypeAndName[1], $validInputNames)) {
            unset($valueArray[$k]);
          }
        }
        $this->mapping[$key] = $value;
      }
      elseif (!empty($value)) {
        $inputTypeAndName = explode('.', $value);
        if (!empty($inputTypeAndName[0]) && $inputTypeAndName[0] === 'input' && !empty($inputTypeAndName[1]) && !in_array($inputTypeAndName[1], $validInputNames)) {
          $this->mapping[$key] = '';
        }
      }
    }
  }

  public static function checkName($title, $form_processor_id, $id=null,$name=null): string {
    if (!$name) {
      $name = preg_replace('@[^a-z0-9_]+@','_',strtolower($title));
    }

    $name = preg_replace('@[^a-z0-9_]+@','_',strtolower($name));
    $name_part = $name;

    $sql = "SELECT COUNT(*) FROM `civicrm_form_processor_action` WHERE `name` = %1 AND `form_processor_id` = %2";
    $sqlParams[1] = array($name, 'String');
    $sqlParams[2] = array($form_processor_id, 'String');
    if (isset($id)) {
      $sql .= " AND `id` != %3";
      $sqlParams[3] = array($id, 'Integer');
    }

    $i = 1;
    while(CRM_Core_DAO::singleValueQuery($sql, $sqlParams) > 0) {
      $i++;
      $name = $name_part .'_'.$i;
      $sqlParams[1] = array($name, 'String');
    }
    return $name;
  }

  /**
   * Returns an array with all the fields available for parameter mapping
   *
   * @param $form_processor_id
   * @param int|null $id
   * @param bool $onlyInputs
   * @param bool $skipInputs
   * @return array
   */
  public static function getFieldsForMapping($actionProvider, int $form_processor_id, int $id=null, $onlyInputs = false, $skipInputs = false): array {
    $return = [];
    if (!$skipInputs) {
      $inputs = CRM_FormProcessor_BAO_FormProcessorInput::getValues(['form_processor_id' => $form_processor_id]);
      $return = \Civi\FormProcessor\Runner::getFormProcessorInputMapping('input', $inputs);
    }

    $return = array_merge($return, \Civi\FormProcessor\Runner::getFormattedMapping());

    if (!$onlyInputs) {
      $validateActions = CRM_FormProcessor_BAO_FormProcessorValidateAction::getValues(['form_processor_id' => $form_processor_id]);
      $calculateActions = CRM_FormProcessor_BAO_FormProcessorCalculateAction::getValues(['form_processor_id' => $form_processor_id]);
      $actions = CRM_FormProcessor_BAO_FormProcessorAction::getValues(['form_processor_id' => $form_processor_id]);
      $return = array_merge($return, \Civi\FormProcessor\Runner::getActionMapping('validation', E::ts('Validation Action'), $validateActions, $actionProvider, $id));
      $return = array_merge($return, \Civi\FormProcessor\Runner::getActionMapping('calculation', E::ts('Calculation Action'), $calculateActions, $actionProvider, $id));
      $return = array_merge($return, \Civi\FormProcessor\Runner::getActionMapping('action', E::ts('Action'), $actions, $actionProvider, $id));
    }
    return $return;
  }

}
