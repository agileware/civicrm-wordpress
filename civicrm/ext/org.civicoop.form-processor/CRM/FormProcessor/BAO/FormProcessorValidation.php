<?php

use CRM_FormProcessor_ExtensionUtil as E;

/**
 * @author Jaap Jansma (CiviCooP) <jaap.jansma@civicoop.org>
 * @license http://www.gnu.org/licenses/agpl-3.0.html
 */
class CRM_FormProcessor_BAO_FormProcessorValidation extends CRM_FormProcessor_DAO_FormProcessorValidation {

   /**
   * Function to get values
   *
   * @return array $result found rows with data
   * @access public
   * @static
   */
  public static function getValues($params) {
    $result = array();
    $validation = new CRM_FormProcessor_BAO_FormProcessorValidation();
    if (!empty($params)) {
      $fields = self::fields();
      foreach ($params as $key => $value) {
        if (isset($fields[$key])) {
          $validation->$key = $value;
        }
      }
    }
    $validation->find();
    while ($validation->fetch()) {
      $row = array();
      self::storeValues($validation, $row);
      $validator = \Civi::service('form_processor_validation_factory')->getValidatorByName($row['validator']);
      if ($validator) {
        $configuration = $validator->getDefaultConfiguration();
        if (isset($row['configuration']) && is_string($row['configuration'])) {
          $row['configuration'] = json_decode($row['configuration'], true);
        }
        if (empty($row['configuration']) || !is_array($row['configuration'])) {
          $row['configuration'] = array();
        }
        foreach($row['configuration'] as $name => $value) {
          $configuration->set($name, $value);
        }
        $validator->setConfiguration($configuration);

        $row['validator'] = $validator;
      }
      $result[$row['id']] = $row;
    }
    return $result;
  }

  /**
   * Function to add or update form processor validation
   *
   * @param array $params
   * @return array $result
   * @access public
   * @throws Exception when params is empty
   * @static
   */
  public static function add($params) {
    $result = array();
    if (empty($params)) {
      throw new Exception('Params can not be empty when adding or updating a form processor input validation');
    }

    if (!empty($params['id'])) {
      CRM_Utils_Hook::pre('edit', 'FormProcessorValidation', $params['id'], $params);
    }
    else {
      CRM_Utils_Hook::pre('create', 'FormProcessorValidation', NULL, $params);
    }

    $validation = new CRM_FormProcessor_BAO_FormProcessorValidation();
    $fields = self::fields();
    foreach ($params as $key => $value) {
      if (isset($fields[$key])) {
        $validation->$key = $value;
      }
    }
    if (is_array($validation->configuration)) {
      $validation->configuration = json_encode($validation->configuration);
    }
    $validation->save();
    self::storeValues($validation, $result);

    if (!empty($params['id'])) {
      CRM_Utils_Hook::post('edit', 'FormProcessorValidation', $validation->id, $validation);
    }
    else {
      CRM_Utils_Hook::post('create', 'FormProcessorValidation', $validation->id, $validation);
    }

    return $result;
  }

  /**
   * Function to delete a form processor validation with id
   *
   * @param int $id
   * @throws Exception when $id is empty
   * @access public
   * @static
   */
  public static function deleteWithId($id) {
    if (empty($id)) {
      throw new Exception('id can not be empty when attempting to delete a form processor validation');
    }

    CRM_Utils_Hook::pre('delete', 'FormProcessorValidation', $id, CRM_Core_DAO::$_nullArray);

    $validation = new CRM_FormProcessor_BAO_FormProcessorValidation();
    $validation->id = $id;
    if ($validation->find(true)) {
      $validation->delete();
    }

    CRM_Utils_Hook::post('delete', 'FormProcessorValidation', $id, CRM_Core_DAO::$_nullArray);

    return;
  }

  /**
   * Function to delete all inputs with an entity_id
   *
   * @param int $formProcessorInputId
   * @access public
   * @static
   */
  public static function deleteWithEntityId($entity, $entity_id) {
    $validation = new CRM_FormProcessor_BAO_FormProcessorValidation();
    $validation->entity = $entity;
    $validation->entity_id = $entity_id;
    $validation->find(FALSE);
    while ($validation->fetch()) {
      self::deleteWithId($validation->id);
    }
  }


}