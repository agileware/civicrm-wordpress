<?php

use Civi\FormProcessor\Runner;

/**
 * @author Jaap Jansma (CiviCooP) <jaap.jansma@civicoop.org>
 * @license http://www.gnu.org/licenses/agpl-3.0.html
 */

 class CRM_FormProcessor_BAO_FormProcessorInstance extends CRM_FormProcessor_DAO_FormProcessorInstance {

   static $importingFormProcessors = array();

  /**
   * Function to get values
   *
   * @return array $result found rows with data
   * @access public
   * @static
   */
  public static function getValues($params) {
    $result = array();
    $formProcessorInstance = new CRM_FormProcessor_BAO_FormProcessorInstance();
    if (!empty($params)) {
      $fields = self::fields();
      foreach ($params as $key => $value) {
        if (isset($fields[$key])) {
          $formProcessorInstance->$key = $value;
        }
      }
    }
    $formProcessorInstance->find();
    while ($formProcessorInstance->fetch()) {
      $row = array();
      self::storeValues($formProcessorInstance, $row);
      $row['inputs'] = array_values(CRM_FormProcessor_BAO_FormProcessorInput::getValues(array('form_processor_id' => $formProcessorInstance->id)));
      $row['actions'] = array_values(CRM_FormProcessor_BAO_FormProcessorAction::getValues(array('form_processor_id' => $formProcessorInstance->id)));
      $row['default_data_inputs'] = array_values(CRM_FormProcessor_BAO_FormProcessorDefaultDataInput::getValues(array('form_processor_id' => $formProcessorInstance->id)));
      $row['default_data_actions'] = array_values(CRM_FormProcessor_BAO_FormProcessorDefaultDataAction::getValues(array('form_processor_id' => $formProcessorInstance->id)));
      $row['validate_actions'] = array_values(CRM_FormProcessor_BAO_FormProcessorValidateAction::getValues(array('form_processor_id' => $formProcessorInstance->id)));
      $row['validate_validators'] = array_values(CRM_FormProcessor_BAO_FormProcessorValidateValidator::getValues(array('form_processor_id' => $formProcessorInstance->id)));
      $row['calculate_actions'] = array_values(CRM_FormProcessor_BAO_FormProcessorCalculateAction::getValues(array('form_processor_id' => $formProcessorInstance->id)));
      if (isset($row['output_handler_configuration']) && is_string($row['output_handler_configuration'])) {
        $row['output_handler_configuration'] = json_decode($row['output_handler_configuration'], true);
      }
      if (empty($row['output_handler_configuration']) || !is_array($row['output_handler_configuration'])) {
        $row['output_handler_configuration'] = array();
      }

      $handler = null;
      if ($row['output_handler']) {
        $handler = \Civi::service('form_processor_output_handler_factory')->getHandlerByName($row['output_handler']);
        if ($handler) {
          $configuration = $handler->getDefaultConfiguration();
          foreach ($row['output_handler_configuration'] as $name => $value) {
            $configuration->set($name, $value);
          }
          $handler->setConfiguration($configuration);
          $row['output_handler'] = $handler;
        }
      }

      if (isset($row['default_data_output_configuration']) && is_string($row['default_data_output_configuration'])) {
        $row['default_data_output_configuration'] = json_decode($row['default_data_output_configuration'], true);
      }
      if (empty($row['default_data_output_configuration']) || !is_array($row['default_data_output_configuration'])) {
        $row['default_data_output_configuration'] = array();
      }
      if (isset($row['calculation_output_configuration']) && is_string($row['calculation_output_configuration'])) {
        $row['calculation_output_configuration'] = json_decode($row['calculation_output_configuration'], true);
      }
      if (empty($row['calculation_output_configuration']) || !is_array($row['calculation_output_configuration'])) {
        $row['calculation_output_configuration'] = array();
      }

      $result[$row['id']] = $row;
    }
    return $result;
  }

  /**
   * Function to add or update a FormProcessorInstance
   *
   * @param array $params
   * @return array $result
   * @access public
   * @throws Exception when params is empty
   * @static
   */
  public static function add($params) {
    $result = array();
    if (empty($params)) {
      throw new Exception('Params can not be empty when adding or updating a formsprocessor instance');
    }

    if (empty($params['id']) && empty($params['permission'])) {
      $params['permission'] = '';
    }

    if (!empty($params['id'])) {
      CRM_Utils_Hook::pre('edit', 'FormProcessorInstance', $params['id'], $params);
    }
    else {
      CRM_Utils_Hook::pre('create', 'FormProcessorInstance', NULL, $params);
    }

    $formProcessorInstance = new CRM_FormProcessor_BAO_FormProcessorInstance();
    $fields = self::fields();
    foreach ($params as $key => $value) {
      if (isset($fields[$key])) {
        $formProcessorInstance->$key = $value;
      }
    }
    if (!isset($formProcessorInstance->name) || empty($formProcessorInstance->name)) {
      if (isset($formProcessorInstance->title)) {
        $formProcessorInstance->name = CRM_FormProcessor_BAO_FormProcessorInstance::checkName($formProcessorInstance->title, $formProcessorInstance->id);
      }
    }
    if (isset($formProcessorInstance->output_handler_configuration) && is_array($formProcessorInstance->output_handler_configuration)) {
      $formProcessorInstance->output_handler_configuration = json_encode($formProcessorInstance->output_handler_configuration);
    }
    if (isset($formProcessorInstance->default_data_output_configuration) && is_array($formProcessorInstance->default_data_output_configuration)) {
      $formProcessorInstance->default_data_output_configuration = json_encode($formProcessorInstance->default_data_output_configuration);
    }
    if (isset($formProcessorInstance->calculation_output_configuration) && is_array($formProcessorInstance->calculation_output_configuration)) {
      $formProcessorInstance->calculation_output_configuration = json_encode($formProcessorInstance->calculation_output_configuration);
    }

    $formProcessorInstance->save();
    self::storeValues($formProcessorInstance, $result);

    if (!empty($params['id'])) {
      CRM_Utils_Hook::post('edit', 'FormProcessorInstance', $formProcessorInstance->id, $formProcessorInstance);
    }
    else {
      CRM_Utils_Hook::post('create', 'FormProcessorInstance', $formProcessorInstance->id, $formProcessorInstance);
    }

    return $result;
  }

  /**
   * Function to delete a FormProcessorInstance with id
   *
   * @param int $id
   * @throws Exception when $id is empty
   * @access public
   * @static
   */
  public static function deleteWithId($id) {
    if (empty($id)) {
      throw new Exception('id can not be empty when attempting to delete a form processor');
    }

    CRM_Utils_Hook::pre('delete', 'FormProcessorInstance', $id, CRM_Core_DAO::$_nullArray);

    // First delete all inputs
    CRM_FormProcessor_BAO_FormProcessorInput::deleteWithFormProcessorInstanceId($id);
    CRM_FormProcessor_BAO_FormProcessorDefaultDataInput::deleteWithFormProcessorInstanceId($id);
    // And all actions
    CRM_FormProcessor_BAO_FormProcessorAction::deleteWithFormProcessorInstanceId($id);
    CRM_FormProcessor_BAO_FormProcessorDefaultDataAction::deleteWithFormProcessorInstanceId($id);
    CRM_FormProcessor_BAO_FormProcessorValidateAction::deleteWithFormProcessorInstanceId($id);
    CRM_FormProcessor_BAO_FormProcessorValidateValidator::deleteWithFormProcessorInstanceId($id);

    $formProcessorInstance = new CRM_FormProcessor_BAO_FormProcessorInstance();
    $formProcessorInstance->id = $id;
    if ($formProcessorInstance->find(TRUE)) {
      if (!empty($formProcessorInstance->source_file)) {
        \Civi\FormProcessor\Exporter\ExportToJson::deleteUploadedFile($formProcessorInstance->source_file);
      }
      $formProcessorInstance->delete();
    }

    CRM_Utils_Hook::post('delete', 'FormProcessorInstance', $id, CRM_Core_DAO::$_nullArray);

    return;
  }

  /**
   * Returns the permission for the form processor. When no permission is required
   * return an empty string.
   *
   * @param string $formProcessorName
   *   The name of the form processor.
   * @return string
   *   The required permission.
   */
  public static function getPermission($formProcessorName) {
    $sql = "SELECT `permission` FROM `civicrm_form_processor_instance` WHERE `name` = %1";
    $params[1] = array($formProcessorName, 'String');
    $permission = CRM_Core_DAO::singleValueQuery($sql, $params);
    return $permission;
  }

  /**
   * Returns the id of the form processor.
   *
   * @param string $formProcessorName
   *   The name of the form processor.
   * @return int
   *   The id of the form processor.
   */
  public static function getId($formProcessorName) {
    $sql = "SELECT `id` FROM `civicrm_form_processor_instance` WHERE `name` = %1";
    $params[1] = array($formProcessorName, 'String');
    $id = CRM_Core_DAO::singleValueQuery($sql, $params);
    return $id;
  }

   /**
    * Returns the id of the form processor.
    *
    * @param int $formProcessorId
    *   The ID of the form processor.
    * @return string
    *   The name of the form processor.
    */
   public static function getName(int $formProcessorId) {
     static $names = [];
     if (!isset($names[$formProcessorId])) {
       $sql = "SELECT `name` FROM `civicrm_form_processor_instance` WHERE `id` = %1";
       $params[1] = array($formProcessorId, 'Integer');
       $names[$formProcessorId] = CRM_Core_DAO::singleValueQuery($sql, $params);
     }
     return $names[$formProcessorId];
   }

  /**
   * Returns the status of the form processor.
   * @See CRM_FormProcessor_Status for possible values.
   *
   * @param string $formProcessorName
   *   The name of the form processor.
   * @return int
   *   The status of the form processor.
   */
  public static function getStatus($formProcessorName) {
    $sql = "SELECT `status` FROM `civicrm_form_processor_instance` WHERE `name` = %1";
    $params[1] = array($formProcessorName, 'String');
    $status = CRM_Core_DAO::singleValueQuery($sql, $params);
    return $status;
  }

   /**
    * Returns the status of the form processor.
    * @See CRM_FormProcessor_Status for possible values.
    *
    * @param string $formProcessorName
    *   The name of the form processor.
    * @param integer $status
    *   The new status
    */
   public static function setStatus($formProcessorName, $status) {
     $sql = "UPDATE `civicrm_form_processor_instance` SET `status` = %1 WHERE `name` = %2";
     $params[1] = array($status, 'Integer');
     $params[2] = array($formProcessorName, 'String');
     CRM_Core_DAO::executeQuery($sql, $params);

   }

   /**
   * Updates the status and source file of the form processor.
   * @See CRM_FormProcessor_Status for possible status values.
   *
   * @param string $formProcessorName
   *   The name of the form processor.
   * @param int $status
   *   The status value.
   * @param string $source_file
   *   The source file. Leave empty when status is IN_DATABASE.
   */
  public static function setStatusAndSourceFile($formProcessorName, $status, $source_file) {
    $sql = "UPDATE `civicrm_form_processor_instance` SET `status` = %2, `source_file` = %3 WHERE `name` = %1";
    $params[1] = array($formProcessorName, 'String');
    $params[2] = array($status, 'Integer');
    $params[3] = array($source_file, 'String');
    CRM_Core_DAO::executeQuery($sql, $params);
  }

   /**
    * Update the status from in code to overriden when a form processor has been changed
    *
    * @param $formProcessorId
    */
   public static function updateAndChekStatus($formProcessorId) {
     $sql = "SELECT `status`, `name` FROM `civicrm_form_processor_instance` WHERE `id` = %1";
     $params[1] = array($formProcessorId, 'Integer');
     $dao = CRM_Core_DAO::executeQuery($sql, $params);
     if ($dao->fetch()) {
       if (!in_array($dao->name, self::$importingFormProcessors) && $dao->status == CRM_FormProcessor_Status::IN_CODE) {
         $sql = "UPDATE `civicrm_form_processor_instance` SET `status` = %2 WHERE `id` = %1";
         $params[1] = array($formProcessorId, 'String');
         $params[2] = array(CRM_FormProcessor_Status::OVERRIDDEN, 'Integer');
         CRM_Core_DAO::executeQuery($sql, $params);
       }
     }
   }

   /**
    * Store the form processor name so we know that we are importing this form processor
    * and should not update its status on the way.
    *
    * @param $formProcessorName
    */
   public static function setFormProcessorToImportingState($formProcessorName) {
     self::$importingFormProcessors[] = $formProcessorName;
   }


   public static function checkName($title, $id=null,$name=null): string {
     if (!$name) {
       $name = preg_replace('@[^a-z0-9_]+@', '_', strtolower($title));
     }

     $name = preg_replace('@[^a-z0-9_]+@', '_', strtolower($name));
     $name_part = $name;

     $sql = "SELECT COUNT(*) FROM `civicrm_form_processor_instance` WHERE `name` = %1";
     $sqlParams[1] = [$name, 'String'];
     if (!empty($id)) {
       $sql .= " AND `id` != %2";
       $sqlParams[2] = [$id, 'Integer'];
     }

     $i = 1;
     while (CRM_Core_DAO::singleValueQuery($sql, $sqlParams) > 0) {
       $i++;
       $name = $name_part . '_' . $i;
       $sqlParams[1] = [$name, 'String'];
     }
     return $name;
   }

   /**
    * Returns a list of inputs which are used as triggers for the calculation
    *
    * @param string $formProcessorName
    *
    * @return array
    */
   public static function getCalculationTriggers(string $formProcessorName): array {
     try {
       $formProcessor = Runner::getFormProcessor($formProcessorName);
     } catch (\Exception $e) {
       return [];
     }

     $usedInputs = [];
     $usedValidationInputs = [];
     foreach($formProcessor['calculation_output_configuration'] as $key) {
       if (strpos($key, 'input.') === 0 && !in_array($key, $usedInputs)) {
         $usedInputs[] = substr($key, 6);
       } elseif (strpos($key, 'validation.') === 0) {
         $usedValidationInputs[] = substr($key, 11);
       }
     }
     $usedValidationInputs = array_merge($usedValidationInputs, self::getInputParametersFromActions($formProcessor['calculate_actions'], 'validation'));
     $usedInputs = array_merge($usedInputs, self::getInputParametersFromActions($formProcessor['calculate_actions'], 'input'));
     if (count($usedValidationInputs)) {
       $usedInputs = array_merge($usedInputs, self::getInputParametersFromActions($formProcessor['validate_actions'], 'input'));
     }
     return array_unique($usedInputs);
   }

   public static function getCalculationOutputs(string $formProcessorName): array {
     try {
       $formProcessor = Runner::getFormProcessor($formProcessorName);
     } catch (\Exception $e) {
       return [];
     }

     $usedOutputs = [];
     foreach($formProcessor['calculation_output_configuration'] as $key => $v) {
       if (strlen($v)) {
         $usedOutputs[] = $key;
       }
     }
     return array_unique($usedOutputs);
   }

   private static function getInputParametersFromActions(array $actions, string $type): array {
     $usedInputs = [];
     foreach($actions as $action) {
       foreach($action['mapping'] as $key) {
         if (is_array($key)) {
           foreach ($key as $subkey) {
             if ($r = self::isMappingToAnInput($subkey, $type)) {
               $usedInputs[] = substr($r, strlen($type)+1);
             }
           }
         } else {
           if ($r = self::isMappingToAnInput($key, $type)) {
             $usedInputs[] = substr($r, strlen($type)+1);
           }
         }
       }
       if (isset($action['condition_configuration']['parameter_mapping']) && is_array($action['condition_configuration']['parameter_mapping'])) {
         foreach ($action['condition_configuration']['parameter_mapping'] as $key) {
           if (is_array($key)) {
             foreach ($key as $subkey) {
               if (is_array($subkey) && isset($subkey['parameter_mapping']) && is_array($subkey['parameter_mapping'])) {
                  foreach ($subkey['parameter_mapping'] as $subsubkey) {
                    if ($r = self::isMappingToAnInput($subsubkey, $type)) {
                      $usedInputs[] = substr($r, strlen($type) + 1);
                    }
                  }
               } else {
                 if ($r = self::isMappingToAnInput($subkey, $type)) {
                   $usedInputs[] = substr($r, strlen($type) + 1);
                 }
               }
             }
           } else {
             if ($r = self::isMappingToAnInput($key, $type)) {
               $usedInputs[] = substr($r, strlen($type)+1);
             }
           }
         }
       }
       if (isset($action['condition_configuration']['output_mapping']) && is_array($action['condition_configuration']['output_mapping'])) {
         foreach ($action['condition_configuration']['output_mapping'] as $key) {
           if (is_array($key)) {
             foreach ($key as $subkey) {
               if ($r = self::isMappingToAnInput($subkey, $type)) {
                 $usedInputs[] = substr($r, strlen($type)+1);
               }
             }
           } else {
             if ($r = self::isMappingToAnInput($key, $type)) {
               $usedInputs[] = substr($r, strlen($type)+1);
             }
           }
         }
       }
     }
     return array_unique($usedInputs);
   }

   private static function isMappingToAnInput(string $key, string $type):? string {
     if (is_array($key)) {
       foreach ($key as $k) {
         if ($r = self::isMappingToAnInput($k, $type)) {
           return $r;
         }
       }
     } elseif (strpos($key, $type.'.') === 0) {
       return $key;
     }
     return null;
   }

 }
