<?php

use CRM_FormProcessor_ExtensionUtil as E;

/**
 * @author Jaap Jansma (CiviCooP) <jaap.jansma@civicoop.org>
 * @license http://www.gnu.org/licenses/agpl-3.0.html
 */
class CRM_FormProcessor_BAO_FormProcessorValidateValidator extends CRM_FormProcessor_DAO_FormProcessorValidateValidator {

   /**
   * Function to get values
   *
   * @return array $result found rows with data
   * @access public
   * @static
   */
  public static function getValues($params) {
    $result = array();
    $validator = new CRM_FormProcessor_BAO_FormProcessorValidateValidator();
    if (!empty($params)) {
      $fields = self::fields();
      foreach ($params as $key => $value) {
        if (isset($fields[$key])) {
          $validator->$key = $value;
        }
      }
    }
    $validator->orderBy('weight');
    $validator->find();
    while ($validator->fetch()) {
      $row = array();
      self::storeValues($validator, $row);
      if (!empty($row['form_processor_id'])) {
        if (isset($row['configuration']) && is_string($row['configuration'])) {
          $row['configuration'] = json_decode($row['configuration'], TRUE);
        }
        if (empty($row['configuration']) || !is_array($row['configuration'])) {
          $row['configuration'] = array();
        }
        if (isset($row['mapping']) && is_string($row['mapping'])) {
          $row['mapping'] = json_decode($row['mapping'], true);
        }
        if (empty($row['mapping']) || !is_array($row['mapping'])) {
          $row['mapping'] = array();
        }

        if (isset($row['inputs']) && is_string($row['inputs'])) {
          $row['inputs'] = json_decode($row['inputs'], TRUE);
        }
        if (empty($row['inputs']) || !is_array($row['inputs'])) {
          $row['inputs'] = null;
        }

        $result[$row['id']] = $row;
      } else {
        //invalid input because no there is no form processor
        CRM_FormProcessor_BAO_FormProcessorValidateAction::deleteWithId($row['id']);
      }
    }
    return $result;
  }

  /**
   * Function to add or update form processor action
   *
   * @param array $params
   * @return array $result
   * @access public
   * @throws Exception when params is empty
   * @static
   */
  public static function add($params) {
    $result = array();
    if (empty($params)) {
      throw new Exception('Params can not be empty when adding or updating a form processor action');
    }

    if (empty($params['id']) && empty($params['weight'])) {
      $params['weight'] = \CRM_Core_DAO::singleValueQuery("SELECT COUNT(*) + 1 FROM `".self::getTableName()."` WHERE `form_processor_id` = %1", [1=>[$params['form_processor_id'], 'Integer']]);
    }

    if (!empty($params['id'])) {
      CRM_Utils_Hook::pre('edit', 'FormProcessorValidateValidator', $params['id'], $params);
    }
    else {
      CRM_Utils_Hook::pre('create', 'FormProcessorValidateValidator', NULL, $params);
    }

    $validator = new CRM_FormProcessor_BAO_FormProcessorValidateValidator();
    $fields = self::fields();
    foreach ($params as $key => $value) {
      if (isset($fields[$key])) {
        $validator->$key = $value;
      }
    }

    if ($validator->configuration && is_array($validator->configuration)) {
      $validator->configuration = json_encode($validator->configuration);
    }
    if ($validator->mapping && is_array($validator->mapping)) {
      $validator->mapping = json_encode($validator->mapping);
    }
    if ($validator->inputs && is_array($validator->inputs)) {
      $validator->inputs = json_encode($validator->inputs);
    }
    $validator->save();
    self::storeValues($validator, $result);

    if (!empty($params['id'])) {
      CRM_Utils_Hook::post('edit', 'FormProcessorValidateValidator', $validator->id, $validator);
    }
    else {
      CRM_Utils_Hook::post('create', 'FormProcessorValidateValidator', $validator->id, $validator);
    }

    return $result;
  }

  /**
   * Function to delete a form processor action with id
   *
   * @param int $id
   * @throws Exception when $id is empty
   * @access public
   * @static
   */
  public static function deleteWithId($id) {
    if (empty($id)) {
      throw new Exception('id can not be empty when attempting to delete a form processor action');
    }
    CRM_Utils_Hook::pre('delete', 'FormProcessorValidateValidator', $id, CRM_Core_DAO::$_nullArray);
    $validator = new CRM_FormProcessor_BAO_FormProcessorValidateValidator();
    $validator->id = $id;
    if ($validator->find(true)) {
      $validator->delete();
    }
    CRM_Utils_Hook::post('delete', 'FormProcessorValidateValidator', $id, CRM_Core_DAO::$_nullArray);
  }

  /**
   * Function to delete all actions with a form processor instance id
   *
   * @param int $formProcessorId
   * @access public
   * @static
   */
  public static function deleteWithFormProcessorInstanceId($formProcessorId) {
    $validator = new CRM_FormProcessor_BAO_FormProcessorValidateValidator();
    $validator->form_processor_id = $formProcessorId;
    $validator->find(FALSE);
    while ($validator->fetch()) {
      self::deleteWithId($validator->id);
    }
  }

  public static function checkName($title, $form_processor_id, $id=null,$name=null): string {
    if (!$name) {
      $name = preg_replace('@[^a-z0-9_]+@','_',strtolower($title));
    }

    $name = preg_replace('@[^a-z0-9_]+@','_',strtolower($name));
    $name_part = $name;

    $sql = "SELECT COUNT(*) FROM `civicrm_form_processor_validate_validator` WHERE `name` = %1 AND `form_processor_id` = %2";
    $sqlParams[1] = array($name, 'String');
    $sqlParams[2] = array($form_processor_id, 'String');
    if (isset($id)) {
      $sql .= " AND `id` != %3";
      $sqlParams[3] = array($id, 'Integer');
    }

    $i = 1;
    while(CRM_Core_DAO::singleValueQuery($sql, $sqlParams) > 0) {
      $i++;
      $name = $name_part .'_'.$i;
      $sqlParams[1] = array($name, 'String');
    }
    return $name;
  }

}
