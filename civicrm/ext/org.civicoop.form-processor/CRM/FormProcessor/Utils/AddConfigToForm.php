<?php
/**
 * @author Jaap Jansma <jaap.jansma@civicoop.org>
 * @license AGPL-3.0
 */

use Civi\ActionProvider\Parameter\SpecificationGroup;
use Civi\FormProcessor\Config\Specification;
use Civi\FormProcessor\Config\SpecificationBag;
use Civi\FormProcessor\Config\SpecificationCollection;
use Civi\FormProcessor\Config\SpecificationFields;
use Civi\FormProcessor\Config\SpecificationInterface;
use CRM_FormProcessor_ExtensionUtil as E;

class CRM_FormProcessor_Utils_AddConfigToForm {

  public static function addConfig($prefix, SpecificationBag $parameterSpecs, $currentConfig, CRM_Core_Form $form, $availableFields) {
    $configFields = $form->get_template_vars('configFields');
    if (!is_array($configFields)) {
      $configFields = array($prefix=>array());
    }
    if (!isset($configFields[$prefix])) {
      $configFields[$prefix] = array();
    }
    $configDescriptions = $form->get_template_vars('configDescriptions');
    if (!is_array($configDescriptions)) {
      $configDescriptions = array($prefix=>array());
    }
    if (!isset($configDescriptions[$prefix])) {
      $configDescriptions[$prefix] = array();
    }
    /*$actionProviderGroupedMappingFields = $form->get_template_vars('actionProviderGroupedMappingFields');
    if (!is_array($actionProviderGroupedMappingFields)) {
      $actionProviderGroupedMappingFields = array($prefix=>array());
    }
    if (!isset($actionProviderGroupedMappingFields[$prefix])) {
      $actionProviderGroupedMappingFields[$prefix] = array();
    }*/
    $configCollectionFields = $form->get_template_vars('configCollectionFields');
    if (!is_array($configCollectionFields)) {
      $configCollectionFields = array($prefix=>array());
    }
    if (!isset($configCollectionFields[$prefix])) {
      $configCollectionFields[$prefix] = array();
    }
    $defaults = [];
    foreach($parameterSpecs as $spec) {
      if ($spec instanceof SpecificationCollection) {
        $configCollectionFields[$prefix][$spec->getName()]['title'] = $spec->getTitle();
        $configCollectionFields[$prefix][$spec->getName()]['name'] = $spec->getName();
        $configCollectionFields[$prefix][$spec->getName()]['min'] = $spec->getMin();
        $configCollectionFields[$prefix][$spec->getName()]['max'] = $spec->getMax();

        $currentCount = 0;
        if (isset($currentConfig[$spec->getName()]) && is_array($currentConfig[$spec->getName()])) {
          $currentCount = count($currentConfig[$spec->getName()]);
        }
        $countName = $spec->getName() . 'Count';
        $form->add('hidden', $countName);
        $count = $form->getSubmitValue($countName) ? $form->getSubmitValue($countName) : $currentCount;
        $defaults[$countName] = $count;
        $form->setDefaults($defaults);
        $configCollectionFields[$prefix][$spec->getName()]['count'] = $count;
        $configCollectionFields[$prefix][$spec->getName()]['elements'] = [];
        for ($i = 1; $i <= $count; $i++) {
          $configCollectionFields[$prefix][$spec->getName()]['elements'][] = $i;
        }

        foreach ($spec->getSpecificationBag() as $subSpec) {
          $singleName = $prefix.$subSpec->getName();
          $description = null;
          if ($subSpec->getDescription()) {
            $description = $subSpec->getDescription();
          }

          for($i=0; $i <= $count; $i++) {
            $isRequired = $subSpec->isRequired();
            if ($i == 0) {
              $isRequired = false;
            }
            self::addConfigField($subSpec, $prefix, '['.$i.']', $form, $availableFields, $isRequired, $singleName);
            if ($i >= 1 && isset($currentConfig[$spec->getName()]) && isset($currentConfig[$spec->getName()][$i-1]) && isset($currentConfig[$spec->getName()][$i-1][$subSpec->getName()])) {
              $defaults[$singleName.'['.$i.']'] = $currentConfig[$spec->getName()][$i-1][$subSpec->getName()];
            } elseif ($i >= 1) {
              $defaults[$singleName.'['.$i.']'] = $subSpec->getDefaultValue();
            }
          }

          $configCollectionFields[$prefix][$spec->getName()]['fields'][] = $singleName;
          $configCollectionFields[$prefix][$spec->getName()]['is_required'][$singleName] = $subSpec->isRequired();
          if ($description) {
            $configDescriptions[$prefix][$singleName] = $description;
          }
        }
      } else {
        [$name, $description] = self::addConfigField($spec, $prefix, '', $form, $availableFields, $spec->isRequired());
        if (isset($currentConfig[$spec->getName()])) {
          $defaults[$name] = $currentConfig[$spec->getName()];
        } else {
          $defaults[$name] = $spec->getDefaultValue();
        }
        $configFields[$prefix][] = $name;
        if ($description) {
          $configDescriptions[$prefix][$name] = $description;
        }
      }
    }
    if (count($defaults)) {
      $form->setDefaults($defaults);
    }
    $form->assign('configFields', $configFields);
    //$form->assign('actionProviderGroupedMappingFields', $actionProviderGroupedMappingFields);
    $form->assign('configCollectionFields', $configCollectionFields);
    $form->assign('configDescriptions', $configDescriptions);
  }

  protected static function addConfigField(SpecificationInterface $config_field, $prefix, $suffix, CRM_Core_Form $form, array $availableFields = null, bool $isRequired=false, string $singleName=null) {
    $field_name = $prefix.$config_field->getName().$suffix;
    $description = null;
    if ($config_field->getDescription()) {
      $description = $config_field->getDescription();
    }
    $attributes = array(
      'class' => $prefix,
    );
    if ($singleName) {
      $attributes['data-single-name'] = $singleName;
    }

    if ($config_field instanceof SpecificationFields) {
      $attributes['class'] .= ' crm-select2 huge';
      $attributes['placeholder'] = E::ts('- Select -');
      $form->add('select', $field_name, $config_field->getTitle(), $availableFields, $isRequired, $attributes);
    } elseif (!empty($config_field->getFkEntity())) {
      $attributes['entity'] = $config_field->getFkEntity();
      $attributes['placeholder'] = E::ts('- Select -');
      $attributes['select'] = array('minimumInputLength' => 0);
      if ($config_field->isMultiple()) {
        $attributes['multiple'] = true;
      }
      $form->addEntityRef($field_name, $config_field->getTitle(), $attributes, $isRequired);
    } elseif (!empty($config_field->getOptions())) {
      $attributes['class'] .= ' crm-select2 huge';
      $attributes['placeholder'] = E::ts('- Select -');
      if ($config_field->isMultiple()) {
        $attributes['multiple'] = TRUE;
        $options = $config_field->getOptions();
      }
      else {
        $options = $config_field->getOptions();
      }
      $form->add('select', $field_name, $config_field->getTitle(), $options, $isRequired, $attributes);
    } elseif ($config_field instanceof  Specification && $config_field->getDataType() == 'Boolean') {
      $form->add('checkbox', $field_name, $config_field->getTitle(), $attributes, $isRequired );
    } else {
      $attributes['class'] .= ' huge';
      $form->add('text', $field_name, $config_field->getTitle(), $attributes, $isRequired);
    }

    return [$field_name, $description];
  }

  public static function processConfig($submittedValues, $prefix, SpecificationBag $specificationBag): array {
    $return = array();
    foreach($specificationBag as $spec) {
      if ($spec instanceof SpecificationCollection) {
        $result[$spec->getName()] = [];
        $count = $submittedValues[$spec->getName().'Count'];
        for($i=1; $i <= $count; $i++) {
          foreach($spec->getSpecificationBag() as $subSpec) {
            $name = $prefix.$subSpec->getName();
            if (isset($submittedValues[$name])) {
              $return[$spec->getName()][$i-1][$subSpec->getName()] = $submittedValues[$name][$i];
            }
          }
        }
      } else {
        $name = $prefix.$spec->getName();
        if (isset($submittedValues[$name])) {
          $return[$spec->getName()] = $submittedValues[$name];
        }}
    }
    return $return;
  }

}
