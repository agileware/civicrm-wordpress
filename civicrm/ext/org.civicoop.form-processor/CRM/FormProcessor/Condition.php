<?php
/**
 * @author Jaap Jansma <jaap.jansma@civicoop.org>
 * @license AGPL-3.0
 */

class CRM_FormProcessor_Condition {

  /**
   * Returns null or a ActionProvider AbstractCondition class.
   * @param $condition_configuration
   *
   * @return \Civi\ActionProvider\Condition\AbstractCondition|null
   */
  public static function getConditionClass($condition_configuration) {
    if (!is_array($condition_configuration) || empty($condition_configuration) || !isset($condition_configuration['name'])) {
      return null;
    }
    $provider = form_processor_get_action_provider();
    $condition = $provider->getConditionByName($condition_configuration['name']);
    if ($condition) {
      $configuration = $condition->getDefaultConfiguration();
      if (isset($condition_configuration['configuration']) && is_string($condition_configuration['configuration'])) {
        $condition_configuration['configuration'] = json_decode($condition_configuration['configuration'], TRUE);
      }
      if (empty($condition_configuration['configuration']) || !is_array($condition_configuration['configuration'])) {
        $condition_configuration['configuration'] = [];
      }
      foreach ($condition_configuration['configuration'] as $name => $value) {
        $configuration->setParameter($name, $value);
      }
      $condition->setConfiguration($configuration);

      return $condition;
    }
    return null;
  }

}
