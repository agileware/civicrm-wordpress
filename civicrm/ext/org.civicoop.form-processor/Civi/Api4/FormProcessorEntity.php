<?php
/**
 * @author Jaap Jansma (CiviCooP) <jaap.jansma@civicoop.org>
 * @license http://www.gnu.org/licenses/agpl-3.0.html
 */

namespace Civi\Api4;

use Civi\Api4\Action\FormProcessorEntity\FormProcessorGetAction;
use Civi\Api4\Action\FormProcessorEntity\FormProcessorGetFieldsAction;
use Civi\Api4\Action\FormProcessorEntity\FormProcessorSaveAction;
use Civi\Api4\Action\GetActions;
use Civi\Api4\Generic\CheckAccessAction;

class FormProcessorEntity {

  /**
   * @param string $name
   * @param bool $checkPermissions
   * @return FormProcessorGetFieldsAction
   */
  public static function getFields(string $name, $checkPermissions = TRUE) {
    return (new FormProcessorGetFieldsAction('FormProcessor_' . $name, __FUNCTION__))
      ->setCheckPermissions($checkPermissions);
  }

  /**
   * @param string $name
   * @param bool $checkPermissions
   * @return FormProcessorGetAction
   * @throws \API_Exception
   */
  public static function get(string $name, $checkPermissions = TRUE) {
    return (new FormProcessorGetAction('FormProcessor_' . $name, __FUNCTION__))
      ->setCheckPermissions($checkPermissions);
  }

  /**
   * @param string $name
   * @param bool $checkPermissions
   * @return FormProcessorSaveAction
   * @throws \API_Exception
   */
  public static function save(string $name, $checkPermissions = TRUE) {
    return (new FormProcessorSaveAction('FormProcessor_' . $name, __FUNCTION__))
      ->setCheckPermissions($checkPermissions);
  }

  /**
   * @param string $name
   * @param bool $checkPermissions
   * @return GetActions
   */
  public static function getActions(string $name, $checkPermissions = TRUE) {
    return (new GetActions('FormProcessor_' . $name, __FUNCTION__))
      ->setCheckPermissions($checkPermissions);
  }

  /**
   * @param string $name
   * @return CheckAccessAction
   * @throws \API_Exception
   */
  public static function checkAccess(string $name) {
    return new CheckAccessAction('FormProcessor_' . $name, __FUNCTION__);
  }

  /**
   * @return array
   */
  public static function permissions() {
    return [];
  }

}
