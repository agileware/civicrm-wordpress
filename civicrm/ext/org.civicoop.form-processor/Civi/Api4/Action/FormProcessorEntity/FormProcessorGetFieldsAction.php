<?php
/**
 * @author Jaap Jansma (CiviCooP) <jaap.jansma@civicoop.org>
 * @license http://www.gnu.org/licenses/agpl-3.0.html
 */

namespace Civi\Api4\Action\FormProcessorEntity;

use Civi\Api4\Generic\AbstractSaveAction;
use Civi\Api4\Generic\BasicGetFieldsAction;
use Civi\Api4\Generic\Result;
use Civi\FormProcessor\Runner;
use Civi\FormProcessor\Type\OptionListInterface;

class FormProcessorGetFieldsAction extends BasicGetFieldsAction {

  /**
   * @return array[]
   */
  public function getRecords() {
    $formProcessorName = substr($this->getEntityName(), strlen('FormProcessor_'));
    $result = [];

    $processor = Runner::getFormProcessor($formProcessorName);
    /** @var \CRM_FormProcessor_BAO_FormProcessorInput $input */
    foreach ($processor['inputs'] as $input) {
      $crmType = null;
      if (is_object($input['type'])) {
        $crmType = $input['type']->getCrmType();
      }
      $field = [
        'name' => $input['name'],
        'type' => 'Field',
        'title' => $input['title'],
        'label' => $input['title'],
        'required' => !empty($input['is_required']),
        'default_value' => $input['default_value'] ?? NULL,
        'data_type' => \CRM_Utils_Type::typeToString($crmType),
        'suffixes' => ['name', 'label'],
      ];
      if ($input['type'] instanceof OptionListInterface) {
        $field['options'] = $input['type']->getOptions([]);
      }
      if (is_object($input['type'])) {
        $result[] = $input['type']->alterApi4FieldSpec($field);
      }
    }
    return $result;
  }

}
