<?php
/**
 * @author Jaap Jansma (CiviCooP) <jaap.jansma@civicoop.org>
 * @license http://www.gnu.org/licenses/agpl-3.0.html
 */

namespace Civi\Api4\Action\FormProcessorEntity;

use Civi\Api4\Generic\AbstractGetAction;
use Civi\Api4\Generic\Result;
use Civi\FormProcessor\Runner;

class FormProcessorGetAction extends AbstractGetAction {

  /**
   * @param \Civi\Api4\Generic\Result $result
   */
  public function _run(Result $result) {
    // This is a dummy function otherwise the prefilling fails.
    // The prefilling is done with an event listener in
    // \Civi\FormProcessor\API4\FormProcessorProvider.onApiRespond
    // Where we respond on the Afform.Prefill API
    // We need this action otherwise the Afform.prefill API will fail.
  }

  public function getPermissions() {
    $formProcessorName = substr($this->getEntityName(), strlen('FormProcessor_'));
    $processor = Runner::getFormProcessor($formProcessorName);
    return (array) ($processor['permission'] ?: NULL);
  }

}
