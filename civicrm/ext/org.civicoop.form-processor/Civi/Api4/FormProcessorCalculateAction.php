<?php
namespace Civi\Api4;

/**
 * FormProcessorCalculateAction entity.
 *
 * Provided by the Form Processor extension.
 *
 * @package Civi\Api4
 */
class FormProcessorCalculateAction extends Generic\DAOEntity {

}
