<?php
namespace Civi\Api4;

/**
 * FormProcessorDefaultDataInput entity.
 *
 * Provided by the Form Processor extension.
 *
 * @package Civi\Api4
 */
class FormProcessorDefaultDataInput extends Generic\DAOEntity {

}
