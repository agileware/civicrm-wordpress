<?php
namespace Civi\Api4;

/**
 * FormProcessorDefaultDataAction entity.
 *
 * Provided by the Form Processor extension.
 *
 * @package Civi\Api4
 */
class FormProcessorDefaultDataAction extends Generic\DAOEntity {

}
