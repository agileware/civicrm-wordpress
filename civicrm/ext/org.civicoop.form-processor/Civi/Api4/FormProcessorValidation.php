<?php
namespace Civi\Api4;

/**
 * FormProcessorValidation entity.
 *
 * Provided by the Form Processor extension.
 *
 * @package Civi\Api4
 */
class FormProcessorValidation extends Generic\DAOEntity {

}
