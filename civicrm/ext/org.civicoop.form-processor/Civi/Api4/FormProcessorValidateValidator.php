<?php
namespace Civi\Api4;

/**
 * FormProcessorValidateValidator entity.
 *
 * Provided by the Form Processor extension.
 *
 * @package Civi\Api4
 */
class FormProcessorValidateValidator extends Generic\DAOEntity {

}
