<?php
namespace Civi\Api4;

/**
 * FormProcessorInstance entity.
 *
 * Provided by the Form Processor extension.
 *
 * @package Civi\Api4
 */
class FormProcessorInstance extends Generic\DAOEntity {

}
