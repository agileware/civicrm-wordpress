<?php
namespace Civi\Api4;

/**
 * FormProcessorInput entity.
 *
 * Provided by the Form Processor extension.
 *
 * @package Civi\Api4
 */
class FormProcessorInput extends Generic\DAOEntity {

}
