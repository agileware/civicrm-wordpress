<?php
namespace Civi\Api4;

/**
 * FormProcessorAction entity.
 *
 * Provided by the Form Processor extension.
 *
 * @package Civi\Api4
 */
class FormProcessorAction extends Generic\DAOEntity {

}
