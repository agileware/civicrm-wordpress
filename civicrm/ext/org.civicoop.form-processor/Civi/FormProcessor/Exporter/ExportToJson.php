<?php

namespace Civi\FormProcessor\Exporter;

use Civi\FormProcessor\Utils\Cache;
use CiviCRM_API3_Exception;
use CRM_Core_Config;
use CRM_Core_DAO;
use CRM_FormProcessor_BAO_FormProcessorAction;
use CRM_FormProcessor_BAO_FormProcessorDefaultDataAction;
use CRM_FormProcessor_BAO_FormProcessorDefaultDataInput;
use CRM_FormProcessor_BAO_FormProcessorInput;
use CRM_FormProcessor_BAO_FormProcessorInstance;
use CRM_FormProcessor_BAO_FormProcessorValidation;
use CRM_FormProcessor_BAO_FormProcessorValidateAction;
use CRM_FormProcessor_BAO_FormProcessorValidateValidator;
use CRM_FormProcessor_Status;
use Exception;

class ExportToJson {

  /**
   * Exports a form processor to json.
   *
   * @param int $form_processor_id
   *   The form processor.
   *
   * @return array
   * @throws \CiviCRM_API3_Exception
   */
  public function export(int $form_processor_id): array {
    $form_processor = civicrm_api3('FormProcessorInstance', 'getsingle', array('id' => $form_processor_id));
    // Strip all IDs
    unset($form_processor['id']);
    unset($form_processor['created_user_id']);
    unset($form_processor['created_date']);
    unset($form_processor['modified_user_id']);
    unset($form_processor['modified_date']);
    unset($form_processor['status']);
    unset($form_processor['source_file']);
    unset($form_processor['try_out_url']);
    unset($form_processor['validator_configuration_url']);
    unset($form_processor['export_url']);

    $form_processor['output_handler'] = $form_processor['output_handler']['name'];

    foreach($form_processor['inputs'] as $key => $input) {
      unset($form_processor['inputs'][$key]['id']);
      unset($form_processor['inputs'][$key]['form_processor_id']);

      $form_processor['inputs'][$key]['type'] = $input['type']['name'];

      foreach($input['validators'] as $validator_key => $validator) {
        unset($form_processor['inputs'][$key]['validators'][$validator_key]['id']);
        unset($form_processor['inputs'][$key]['validators'][$validator_key]['entity']);
        unset($form_processor['inputs'][$key]['validators'][$validator_key]['entity_id']);
        $form_processor['inputs'][$key]['validators'][$validator_key]['validator'] = $validator['validator']['name'];
      }
    }
    foreach($form_processor['actions'] as $key => $action) {
      unset($form_processor['actions'][$key]['id']);
      unset($form_processor['actions'][$key]['form_processor_id']);
      unset($form_processor['actions'][$key]['weight']);
      if (isset($form_processor['actions'][$key]['condition_configuration']) && isset($form_processor['actions'][$key]['condition_configuration']['title'])) {
        unset($form_processor['actions'][$key]['condition_configuration']['title']);
      }
    }

    foreach($form_processor['default_data_inputs'] as $key => $input) {
      unset($form_processor['default_data_inputs'][$key]['id']);
      unset($form_processor['default_data_inputs'][$key]['form_processor_id']);

      $form_processor['default_data_inputs'][$key]['type'] = $input['type']['name'];

      foreach($input['validators'] as $validator_key => $validator) {
        unset($form_processor['default_data_inputs'][$key]['validators'][$validator_key]['id']);
        unset($form_processor['default_data_inputs'][$key]['validators'][$validator_key]['entity']);
        unset($form_processor['default_data_inputs'][$key]['validators'][$validator_key]['entity_id']);
        $form_processor['default_data_inputs'][$key]['validators'][$validator_key]['validator'] = $validator['validator']['name'];
      }
    }
    foreach($form_processor['default_data_actions'] as $key => $action) {
      unset($form_processor['default_data_actions'][$key]['id']);
      unset($form_processor['default_data_actions'][$key]['form_processor_id']);
      unset($form_processor['default_data_actions'][$key]['weight']);
    }
    foreach($form_processor['validate_actions'] as $key => $action) {
      unset($form_processor['validate_actions'][$key]['id']);
      unset($form_processor['validate_actions'][$key]['form_processor_id']);
      unset($form_processor['validate_actions'][$key]['weight']);
    }
    foreach($form_processor['validate_validators'] as $key => $validator) {
      unset($form_processor['validate_validators'][$key]['id']);
      unset($form_processor['validate_validators'][$key]['form_processor_id']);
      unset($form_processor['validate_validators'][$key]['weight']);
    }
    foreach($form_processor['calculate_actions'] as $key => $action) {
      unset($form_processor['calculate_actions'][$key]['id']);
      unset($form_processor['calculate_actions'][$key]['form_processor_id']);
      unset($form_processor['calculate_actions'][$key]['weight']);
    }

    return $form_processor;
  }

  /**
   * Returns true when the filename is uploaded to the system.
   * This will allow us to delete the file.
   *
   * @param $filename
   * @return bool
   */
  public static function isUploadedFile($filename): bool {
    $fullFilePath = $filename;
    if (!file_exists($fullFilePath)) {
      $config = CRM_Core_Config::singleton();
      $fullFilePath = $config->customFileUploadDir . 'FormProcessor' . DIRECTORY_SEPARATOR . $filename;
      return file_exists($fullFilePath);
    }
    return false;
  }

  /**
   * Delete an uploaded file from the file system.
   *
   * @param $filename
   */
  public static function deleteUploadedFile($filename) {
    $fullFilePath = $filename;
    if (!file_exists($fullFilePath)) {
      $config = CRM_Core_Config::singleton();
      $fullFilePath = $config->customFileUploadDir . 'FormProcessor' . DIRECTORY_SEPARATOR . $filename;
      if (file_exists($fullFilePath) && is_file($fullFilePath)) {
        unlink($fullFilePath);
      }
    }
  }

  /**
   * @param string $filename
   * @param bool $importLocally
   * @return array
   */
  public function importFromFile(string $filename, $importLocally=false): array {
    $fullFilePath = $filename;
    if (!file_exists($fullFilePath)) {
      $config = CRM_Core_Config::singleton();
      $fullFilePath = $config->customFileUploadDir . 'FormProcessor' . DIRECTORY_SEPARATOR . $filename;
    }
    $data = json_decode(file_get_contents($fullFilePath), true);
    if (!empty($data['name'])) {
      $status = CRM_FormProcessor_BAO_FormProcessorInstance::getStatus($data['name']);
      if ($status != CRM_FormProcessor_Status::IN_CODE) {
        // it's already overridden -> reset it, so we can override again
        CRM_FormProcessor_BAO_FormProcessorInstance::setStatus($data['name'], CRM_FormProcessor_Status::IN_CODE);
      }
      Cache::clear();
      $return = $this->import($data, $filename, $importLocally);
      $return['original_status'] = $status;
      return $return;
    }
    return ['error' => 'not a valid form processor'];
  }

  /**
   * Import form processor
   *
   * @param array $data
   * @param string $filename
   * @param bool $importLocally
   *
   * @return array
   */
  public function import(array $data, string $filename, bool $importLocally=false): array {
    $form_processor_id = CRM_FormProcessor_BAO_FormProcessorInstance::getId($data['name']);
    $status = CRM_FormProcessor_BAO_FormProcessorInstance::getStatus($data['name']);
    CRM_FormProcessor_BAO_FormProcessorInstance::setFormProcessorToImportingState($data['name']);
    switch ($status) {
      case CRM_FormProcessor_Status::IN_DATABASE:
        // Update to overridden
        CRM_FormProcessor_BAO_FormProcessorInstance::setStatusAndSourceFile($data['name'], CRM_FormProcessor_Status::OVERRIDDEN, $filename);
        $new_id = $form_processor_id;
        $new_status = CRM_FormProcessor_Status::OVERRIDDEN;
        break;
      case CRM_FormProcessor_Status::OVERRIDDEN:
        $new_id = $form_processor_id;
        $new_status = CRM_FormProcessor_Status::OVERRIDDEN;
        break;
      default:
        $new_id = $this->importFormProcessor($data, $filename, $form_processor_id, $importLocally);
        $new_status = CRM_FormProcessor_Status::IN_CODE;
        if ($importLocally) {
          $new_status = CRM_FormProcessor_Status::IN_DATABASE;
        }

        break;
    }

    return [
      'original_id' => $form_processor_id,
      'new_id' => $new_id,
      'original_status' => $status,
      'new_status' => $new_status,
      'file' => $filename,
    ];
  }

  /**
   * @param array $data
   * @param string $filename
   * @param int $form_processor_id
   * @param bool $importLocally
   *
   * @return int|null
   */
  private function importFormProcessor(array $data, string $filename, ?int $form_processor_id, bool $importLocally=false): ?int {
    $params = $data;
    if (!isset($data['validate_actions'])) {
      $data['validate_actions'] = [];
    }
    if (!isset($data['validate_validators'])) {
      $data['validate_validators'] = [];
    }
    unset($params['inputs']);
    unset($params['actions']);
    unset($params['default_data_inputs']);
    unset($params['default_data_actions']);
    unset($params['validate_actions']);
    unset($params['validate_validators']);
    unset($params['calculate_actions']);
    if ($form_processor_id) {
      $params['id'] = $form_processor_id;
    }
    if (!$importLocally) {
      $params['status'] = CRM_FormProcessor_Status::IN_CODE;
      $params['source_file'] = $filename;
    } else {
      $params['status'] = CRM_FormProcessor_Status::IN_DATABASE;
    }
    try {
      $result = CRM_FormProcessor_BAO_FormProcessorInstance::add($params);
      $id = $result['id'];
    } catch (Exception $e) {
      return null;
    }

    // Clear all existing inputs and actions
    CRM_FormProcessor_BAO_FormProcessorInput::deleteWithFormProcessorInstanceId($id);
    CRM_FormProcessor_BAO_FormProcessorAction::deleteWithFormProcessorInstanceId($id);
    CRM_FormProcessor_BAO_FormProcessorDefaultDataInput::deleteWithFormProcessorInstanceId($id);
    CRM_FormProcessor_BAO_FormProcessorDefaultDataAction::deleteWithFormProcessorInstanceId($id);
    CRM_FormProcessor_BAO_FormProcessorValidateValidator::deleteWithFormProcessorInstanceId($id);
    CRM_FormProcessor_BAO_FormProcessorValidateAction::deleteWithFormProcessorInstanceId($id);
    \CRM_FormProcessor_BAO_FormProcessorCalculateAction::deleteWithFormProcessorInstanceId($id);

    foreach($data['inputs'] as $input) {
      $params = $input;
      unset($params['validators']);
      $params['form_processor_id'] = $id;
      try {
        $result = CRM_FormProcessor_BAO_FormProcessorInput::add($params);
        $input_id = $result['id'];
        foreach($input['validators'] as $validator) {
          $validator['entity'] = 'FormProcessorInput';
          $validator['entity_id'] = $input_id;
          try {
            CRM_FormProcessor_BAO_FormProcessorValidation::add($validator);
          } catch (Exception $e) {
          }
        }
      } catch (Exception $e) {
      }
    }

    $weight = 1;
    foreach($data['actions'] as $action) {
      $params = $action;
      $params['form_processor_id'] = $id;
      $params['weight'] = $weight;
      try {
        CRM_FormProcessor_BAO_FormProcessorAction::add($params);
        $weight ++;
      } catch (Exception $e) {
      }
    }

    foreach($data['default_data_inputs'] as $input) {
      $params = $input;
      unset($params['validators']);
      $params['form_processor_id'] = $id;
      try {
        $result = CRM_FormProcessor_BAO_FormProcessorDefaultDataInput::add($params);
        $input_id = $result['id'];
        foreach($input['validators'] as $validator) {
          $validator['entity'] = 'FormProcessorDefaultDataInput';
          $validator['entity_id'] = $input_id;
          try {
            CRM_FormProcessor_BAO_FormProcessorValidation::add($validator);
          } catch (Exception $e) {
          }
        }
        } catch (Exception $e) {
      }
    }

    $weight = 1;
    foreach($data['default_data_actions'] as $action) {
      $params = $action;
      $params['form_processor_id'] = $id;
      $params['weight'] = $weight;
      try {
        CRM_FormProcessor_BAO_FormProcessorDefaultDataAction::add($params);
        $weight ++;
      } catch (Exception $e) {
      }
    }

    $weight = 1;
    foreach($data['validate_actions'] as $action) {
      $params = $action;
      $params['form_processor_id'] = $id;
      $params['weight'] = $weight;
      try {
        CRM_FormProcessor_BAO_FormProcessorValidateAction::add($params);
        $weight ++;
      } catch (Exception $e) {
      }
    }

    $weight = 1;
    foreach($data['validate_validators'] as $validator) {
      $params = $validator;
      $params['form_processor_id'] = $id;
      $params['weight'] = $weight;
      try {
        CRM_FormProcessor_BAO_FormProcessorValidateValidator::add($params);
        $weight ++;
      } catch (Exception $e) {
      }
    }

    $weight = 1;
    foreach($data['calculate_actions'] as $action) {
      $params = $action;
      $params['form_processor_id'] = $id;
      $params['weight'] = $weight;
      try {
        \CRM_FormProcessor_BAO_FormProcessorCalculateAction::add($params);
        $weight ++;
      } catch (Exception $e) {
      }
    }

    return $id;
  }

  /**
   * Imports form processor from files in an extension directory.
   *
   * This scans the extension directory form-processors/ for json files.
   */
  public function importFromExtensions(): array {
    $return = array();
    $importedIds = array();
    $extensions = $this->getExtensionFileListWithFormProcessors();
    foreach($extensions as $ext_file) {
      $data = json_decode($ext_file['data'], true);
      $return[$ext_file['file']] = $this->import($data, $ext_file['file']);
      $importedIds[] = $return[$ext_file['file']]['new_id'];
    }

    // Remove all form processors which are in code or overridden but not imported
    if (!empty($importedIds)) {
      $dao = CRM_Core_DAO::executeQuery("SELECT id, name, source_file FROM civicrm_form_processor_instance WHERE id NOT IN (".implode(",", $importedIds).") AND status IN (". CRM_FormProcessor_Status::IN_CODE.", ". CRM_FormProcessor_Status::OVERRIDDEN.")");
      while ($dao->fetch()) {
        $fullFilePath = $this->getFullPathForSourceFile($dao->source_file);
        if (!file_exists($fullFilePath)) {
          try {
            CRM_FormProcessor_BAO_FormProcessorInstance::deleteWithId($dao->id);
            $return['deleted form processors'][] = $dao->id . ": " . $dao->name;
          } catch (Exception $e) {
          }
        }
      }
    }
    Cache::clear();
    return $return;
  }

  /**
   * Revert a form processor to the state in code.
   *
   * @param int $form_processor_id
   * @return bool
   */
  public function revert(int $form_processor_id): bool {
    $dao = CRM_Core_DAO::executeQuery("SELECT status, source_file FROM civicrm_form_processor_instance WHERE id = %1", [1=>[$form_processor_id, 'Integer']]);
    if (!$dao->fetch()) {
      return false;
    }
    if ($dao->status != CRM_FormProcessor_Status::OVERRIDDEN) {
      return false;
    }

    $sourceFile = $dao->source_file;
    $fullFilePath = $this->getFullPathForSourceFile($sourceFile);
    if (!file_exists($fullFilePath)) {
      return false;
    }
    /** @var string $data */
    $data = file_get_contents($fullFilePath);
    $data = json_decode($data, TRUE);
    $this->importFormProcessor($data, $sourceFile, $form_processor_id);
    Cache::clear();
    return true;
  }

  /**
   * Returns a list with form-processor files within an extension folder.
   *
   * @return array
   */
  private function getExtensionFileListWithFormProcessors(): array {
    $return = array();
    try {
      $extensions = civicrm_api3('Extension', 'get', ['options' => ['limit' => 0]]);
    } catch (CiviCRM_API3_Exception $e) {
      return [];
    }
    foreach($extensions['values'] as $ext) {
      if ($ext['status'] != 'installed') {
        continue;
      }

      $path = $ext['path'].DIRECTORY_SEPARATOR.'form-processors';
      if (!is_dir($path)) {
        continue;
      }

      foreach (glob($path.DIRECTORY_SEPARATOR."*.json") as $file) {
        $return[] = array(
          'file' => $ext['key']. DIRECTORY_SEPARATOR . 'form-processors' . DIRECTORY_SEPARATOR . basename($file),
          'data' => file_get_contents($file),
        );
      }
    }
    return $return;
  }

  /**
   * @param string $sourceFile
   * @return string
   */
  private function getFullPathForSourceFile(string $sourceFile): string {
    $fullFilePath = $sourceFile;
    if (!file_exists($fullFilePath)) {
      $config = CRM_Core_Config::singleton();
      $fullFilePath = $config->customFileUploadDir . 'FormProcessor' . DIRECTORY_SEPARATOR . $sourceFile;
      if (!file_exists($fullFilePath)) {
        $key = substr($sourceFile, 0, stripos($sourceFile, DIRECTORY_SEPARATOR));
        try {
          $extension = civicrm_api3('Extension', 'getsingle', ['key' => $key]);
          $fullFilePath = $extension['path'].substr($sourceFile, stripos($sourceFile, DIRECTORY_SEPARATOR));
        } catch (CiviCRM_API3_Exception $e) {
        }
      }
    }
    return $fullFilePath;
  }

}
