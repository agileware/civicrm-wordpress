<?php
/**
 * Copyright (C) 2023  Jaap Jansma (jaap.jansma@civicoop.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace Civi\FormProcessor\EventListener;

use API_Exception;
use Civi\ActionProvider\Event\InitializeMetadataEvent;
use Civi\FormProcessor\Runner;
use CRM_Core_Exception;
use CRM_Utils_Request;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class MetadataSubscriber implements EventSubscriberInterface {

  public static function getSubscribedEvents(): array {
    return [
      'ActionProviderInitializeMetadataEvent' => 'onInitializeMetadata',
    ];
  }

  public function onInitializeMetadata(InitializeMetadataEvent $event) {
    $formProcessorInstance = null;
    $metadata = CRM_Utils_Request::retrieve('metadata', 'String');
    $formProcessorId = CRM_Utils_Request::retrieve('form_processor_id', 'Integer');
    if (!$metadata) {
      $json = CRM_Utils_Request::retrieve('json', 'String');
      if ($json) {
        $json = json_decode($json, TRUE);
        if (isset($json['metadata'])) {
          $metadata = $json['metadata'];
        }
      }
    } else {
      $metadata = json_decode($metadata, TRUE);
    }
    if ($metadata && isset($metadata['form_processor_name'])) {
      $formProcessorInstance = new \CRM_FormProcessor_BAO_FormProcessorInstance();
      $formProcessorInstance->name = $metadata['form_processor_name'];
    } elseif ($formProcessorId) {
      $formProcessorInstance = new \CRM_FormProcessor_BAO_FormProcessorInstance();
      $formProcessorInstance->id = $formProcessorId;
    }

    if ($formProcessorInstance && $formProcessorInstance->find(TRUE)) {
      Runner::setMetadata($event->getMetadata(), $formProcessorInstance->name, $formProcessorInstance->id);
    }
  }

}
