<?php
/**
 * Copyright (C) 2022  Jaap Jansma (jaap.jansma@civicoop.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace Civi\FormProcessor;

use Civi\FormProcessor\Event\FormatDatabagEvent;
use Civi\FormProcessor\Event\FormatTypeEvent;

class DatabagFormatter {

  /**
   * The key is the name of the format and the value is the label of the format
   *
   * @var array
   */
  protected static $formats;

  /**
   * @return array
   */
  public static function getFormats() {
    if (!self::$formats) {
      $event = new FormatTypeEvent(self::$formats);
      \Civi::dispatcher()->dispatch(FormatTypeEvent::NAME, $event);
      self::$formats = $event->formats;
    }
    return self::$formats;
  }

  public static function formatDatabag(DataBag $dataBag) {
    $event = new FormatDatabagEvent($dataBag);
    \Civi::dispatcher()->dispatch(FormatDatabagEvent::NAME, $event);
  }

}
