<?php
/**
 * @author Jaap Jansma (CiviCooP) <jaap.jansma@civicoop.org>
 * @license http://www.gnu.org/licenses/agpl-3.0.html
 */

 namespace Civi\FormProcessor\API4;

 use Civi\Afform\FormDataModel;
 use Civi\API\Event\RespondEvent;
 use Civi\API\Events;
 use Civi\API\Provider\ProviderInterface as API_ProviderInterface;
 use Civi\Api4\Action\Afform\Prefill;
 use Civi\Core\Event\GenericHookEvent;
 use Civi\FormProcessor\Runner;
 use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * This class connects the enabled form processors to
 * the the api interface of CiviCRM. That way the form processors
 * could be executed over the api.
 */
 class FormProcessorProvider implements API_ProviderInterface, EventSubscriberInterface {

  public function __construct() {
  }

  /**
   * @return array[]
   */
  public static function getSubscribedEvents() {
    return [
      'civi.api4.createRequest' => [['onApi4CreateRequest', Events::W_EARLY]],
      'civi.api4.entityTypes' => [['onApi4EntityTypes', Events::W_EARLY]],
      'civi.api.respond' => [['onApiRespond']],
      'civi.afform_admin.metadata' => [['afformEntityTypes']],
    ];
  }

   /**
    * Prefill the form with defaults from the form processor.
    *
    * @param \Civi\API\Event\RespondEvent $event
    *
    * @return void
    * @throws \API_Exception
    * @throws \CiviCRM_API3_Exception
    * @throws \Civi\API\Exception\NotImplementedException
    */
  public function onApiRespond(RespondEvent $event) {
    $apiRequest = $event->getApiRequest();
    if ($apiRequest instanceof Prefill) {
      $prefilledRecords = $event->getResponse();
      /**
       * @var array
       */
      $afform = (array) civicrm_api4('Afform', 'get', ['where' => [['name', '=', $apiRequest->getName()]]], 0);

      /**
       * @var \Civi\Afform\FormDataModel
       *   List of entities declared by this form.
       */
      $formDataModel = new FormDataModel($afform['layout']);
      $args = $apiRequest->getArgs();
      foreach($formDataModel->getEntities() as $entityName => $entity) {
        if (strpos($entity['type'], 'FormProcessor_')===0) { // && !empty($entity['url-autofill'])) {
          $formProcessorName = substr($entity['type'], strlen('FormProcessor_'));
          $defaultParams = [];
          foreach($args as $arg => $value) {
            if (strpos($arg, $entityName.'_')===0) {
              $defaultParams[substr($arg, strlen($entityName.'_'))] = $value;
              unset($args[$arg]);
            }
          }
          try {
            $defaults = Runner::runDefaults($formProcessorName, $defaultParams);
            $prefilledRecords[] = [
              'values' => [0 => ['fields' => $defaults]],
              'name' => $entityName,
            ];
          } catch (\API_Exception $ex) {
            // Do nothing.
          }
        }
      }
      $event->setResponse($prefilledRecords);
    }
  }

  /**
   * Register each FormProcessor as an APIv4 entity.
   *
   * Callback for `civi.api4.entityTypes` event.
   *
   * @param GenericHookEvent $event
   */
  public function onApi4EntityTypes(GenericHookEvent $event) {
    try {
      $sql = 'SELECT `name`, `title`, `description` FROM civicrm_form_processor_instance WHERE is_active = 1;';
      $dao = \CRM_Core_DAO::executeQuery($sql);
      while ($dao->fetch()) {
        $name = 'FormProcessor_' . $dao->name;
        $event->entities[$name] = [
          'name' => $name,
          'title' => $dao->title,
          'title_plural' => $dao->title,
          'description' => (string) ($dao->description ?? ''),
          'primary_key' => ['id'],
          'type' => ['FormProcessor'],
          'table_name' => NULL,
          'class_args' => [$dao->name],
          'label_field' => NULL,
          'searchable' => 'none',
          'paths' => [],
          'class' => 'Civi\Api4\FormProcessorEntity',
          'icon' => 'fa-gears',
        ];
      }
    } catch (\Exception $e) {
      // Do nothing.
    }
  }

  /**
   * Callback for `civi.api4.createRequest` event.
   *
   * @param \Civi\Api4\Event\CreateApi4RequestEvent $event
   */
  public function onApi4CreateRequest($event) {
    if (strpos($event->entityName, 'FormProcessor_') === 0) {
      $name = substr($event->entityName, strlen('FormProcessor_'));
      $event->className = 'Civi\Api4\FormProcessorEntity';
      $event->args = [$name];
    }
  }

   /**
    * @param GenericHookEvent $e
    */
  public static function afformEntityTypes(GenericHookEvent $e) {
    foreach (\CRM_FormProcessor_BAO_FormProcessorInstance::getValues(['is_active' => 1]) as $processor) {
      $name = 'FormProcessor_' . $processor['name'];
      $default_data_inputs = [];
      foreach($processor['default_data_inputs'] as $default_data_input) {
        $default_data_inputs[] = $default_data_input['name'];
      }
      $e->entities[$name] = [
        'entity' => $name,
        'label' => $processor['title'],
        'icon' => 'fa-gears',
        'type' => 'primary',
        'admin_tpl' => '~/afFormProcessorGui/entityConfig/FormProcessor.html',
        'defaults' => '{}',
        'enable_default_data' => $processor['enable_default_data'] ? '1' : '0',
        'default_data_inputs' => $default_data_inputs,
      ];
    }
  }

  /**
   * Not needed for APIv4
   * @param int $version
   * @return array
   */
  public function getEntityNames($version) {
    return [];
  }

  /**
   * Not needed for APIv4
   * @param int $version
   * @param string $entity
   * @return array
   */
  public function getActionNames($version, $entity) {
    return [];
  }

  /**
   * Not needed for APIv4
   */
  public function invoke($apiRequest) {
  }




 }
