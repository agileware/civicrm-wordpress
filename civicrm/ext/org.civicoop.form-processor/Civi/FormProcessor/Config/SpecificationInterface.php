<?php

namespace Civi\FormProcessor\Config;

interface SpecificationInterface {

  /**
   * Returns the type of specifcation
   *
   * @return string
   */
  public function getType();

  /**
   * Returns the name of the specification
   *
   * @return string
   */
  public function getName();

  /**
   * Returns the title of the specification
   *
   * @return string
   */
  public function getTitle();

  /**
   * Returns the descripyion of the specification
   *
   * @return string
   */
  public function getDescription();

  /**
   * Validates the given value
   *
   * @param mixed $value
   * @return bool
   */
  public function validateValue($value);

  /**
   * Returns the default value
   *
   * @return mixed
   */
  public function getDefaultValue();

  /**
   * Returns the specification object as an array
   *
   * @return array
   */
  public function toArray();

}
