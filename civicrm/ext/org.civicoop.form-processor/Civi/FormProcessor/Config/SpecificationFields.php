<?php

namespace Civi\FormProcessor\Config;

use Civi\FormProcessor\Config\ConfigurationBag;
use Civi\FormProcessor\Config\SpecificationBag;
use Civi\FormProcessor\Config\SpecificationInterface;

class SpecificationFields implements SpecificationInterface {

  /**
   * @var string
   */
  protected $name;
  /**
   * @var string
   */
  protected $title;
  /**
   * @var string
   */
  protected $description;

  /**
   * @var bool
   */
  protected $required = FALSE;

  /**
   * The key is the name of the field and the value the label
   *
   * @var array
   */
  protected $fields = array();

  /**
   * @param string $name
   * @param string $title,
   * @param bool
   * @param array
   * @param string
   */
  public function __construct($name, $title, $required=false, $fields=array(), $description='') {
    $this->setName($name);
    $this->setTitle($title);
    $this->fields = $fields;
    $this->setDescription($description);
    $this->setRequired($required);
  }


  public function getType() {
    return 'fields';
  }

  /**
   * @return bool
   */
  public function isRequired() {
    return $this->required;
  }

  /**
   * @param bool $required
   *
   * @return Field
   */
  public function setRequired($required) {
    $this->required = $required;
    return $this;
  }

  /**
   * @return string
   */
  public function getName() {
    return $this->name;
  }

  /**
   * @param string $name
   *
   * @return $this
   */
  public function setName($name) {
    $this->name = $name;
    return $this;
  }

  /**
   * @return string
   */
  public function getTitle() {
    return $this->title;
  }

  /**
   * @param string $title
   *
   * @return $this
   */
  public function setTitle($title) {
    $this->title = $title;
    return $this;
  }

  /**
   * @return string
   */
  public function getDescription() {
    return $this->description;
  }

  /**
   * @param string $description
   *
   * @return $this
   */
  public function setDescription($description) {
    $this->description = $description;
    return $this;
  }

  public function addField($name, $label) {
    $this->field[$name] = $label;
  }

  /**
   * @return array<Field>
   */
  public function getFields() {
    return $this->fields;
  }

  /**
   * Validates the given value
   *
   * @param mixed $value
   * @return bool
   */
  public function validateValue($value) {
    return true;
  }

  /**
   * Returns the default value
   *
   * @return mixed
   */
  public function getDefaultValue() {
    return null;
  }

  /**
   * Converts the object to an array.
   *
   * @return array
   */
  public function toArray() {
    return array(
      'type' => $this->getType(),
      'name' => $this->getName(),
      'title' => $this->getTitle(),
      'description' => $this->getDescription(),
      'fields' => $this->getFields(),
      'required' => $this->isRequired(),
    );
  }

}