<?php

namespace Civi\FormProcessor\Config;

use Civi\FormProcessor\Config\SpecificationInterface;
use Civi\FormProcessor\Config\ConfigurationBag;

class SpecificationBag implements \IteratorAggregate  {

  protected $parameterSpecifications = array();

  /**
   * @param array <SpecificationInterface>
   */
  public function __construct($specifcations = array()) {
    foreach($specifcations as $spec) {
      $this->parameterSpecifications[$spec->getName()] = $spec;
    }
  }

  public function getDefaultConfiguration() {
    $default = new ConfigurationBag();
    foreach($this->parameterSpecifications as $spec) {
      $default->set($spec->getName(), $spec->getDefaultValue());
    }
    return $default;
  }

  /**
   * Validates the configuration.
   *
   * @param ConfigurationBag $configuration
   * @param SpecificationBag $specification
   * @return bool
   */
  public static function validate(ConfigurationBag $configuration, SpecificationBag $specification) {
    foreach($specification as $spec) {
      // First check whether the value is present and should be present.
      $value = $configuration->get($spec->getName());
      if (!$spec->validateValue($value)) {
        return false;
      }
    }
    return true;
  }

  /**
   * @param SpecificationInterface $specification
   *   The specification object.
   * @return SpecificationBag
   */
  public function addSpecification(SpecificationInterface $specification) {
    $this->parameterSpecifications[$specification->getName()] = $specification;
    return $this;
  }

  /**
   * @param SpecificationInterface $specification
   *   The specification object.
   * @return SpecificationBag
   */
  public function removeSpecification(SpecificationInterface $specification) {
    foreach($this->parameterSpecifications as $key => $spec) {
      if ($spec == $specification) {
        unset($this->parameterSpecifications[$key]);
      }
    }
    return $this;
  }

  /**
   * @param string $name
   *   The name of the parameter.
   * @return SpecificationBag
   */
  public function removeSpecificationbyName($name) {
    foreach($this->parameterSpecifications as $key => $spec) {
      if ($spec->getName() == $name) {
        unset($this->parameterSpecifications[$key]);
      }
    }
    return $this;
  }

  /**
   * @param string $name
   *   The name of the parameter.
   * @return SpecificationInterface|null
   */
  public function getSpecificationByName($name) {
    foreach($this->parameterSpecifications as $key => $spec) {
      if ($spec->getName() == $name) {
        return $this->parameterSpecifications[$key];
      }
    }
    return null;
  }

  public function getIterator(): \Traversable {
    return new \ArrayIterator($this->parameterSpecifications);
  }

  /**
   * Converts the object to an array.
   *
   * @return array
   */
  public function toArray() {
    $return = array();
    $return['parameter_specifications'] = array();
    foreach($this->parameterSpecifications as $spec) {
      $return['parameter_specifications'][] = $spec->toArray();
    }
    $return['default_configuration'] = $this->getDefaultConfiguration()->toArray();
    return $return;
  }

}
