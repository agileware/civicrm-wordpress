<?php
/**
 * @author Jaap Jansma <jaap.jansma@civicoop.org>
 * @license AGPL-3.0
 */

namespace Civi\FormProcessor\DelayedAction;

class Factory {

  /**
   * @var array<\Civi\FormProcessor\DelayedAction\AbstractDelayedAction>
   */
  protected $handlers = array();

  public function __construct() {
    $this->addHandler(new DelayByXMinutes());
  }

  /**
   * Add a handler
   *
   * @param \Civi\FormProcessor\DelayedAction\AbstractDelayedAction $handler
   * @return \Civi\FormProcessor\DelayedAction\Factory
   * @throws \Exception
   */
  public function addHandler(AbstractDelayedAction $handler) {
    $this->handlers[$handler->getName()] = $handler;
    return $this;
  }

  /**
   * Return the handler
   *
   * @param string $name
   * @return \Civi\FormProcessor\DelayedAction\AbstractDelayedAction|null
   */
  public function getHandlerByName($name) {
    if (isset($this->handlers[$name])) {
      return $this->handlers[$name];
    }
    return null;
  }

  /**
   * @return array<\Civi\FormProcessor\DelayedAction\AbstractDelayedAction>
   */
  public function getHandlers() {
    return $this->handlers;
  }

  /**
   * Returns an array with the name and the array of the handler
   *
   * @return array<array>
   */
  public function getHandlersAsArray() {
    $return = array();
    foreach($this->handlers as $handler) {
      $return[] = $handler->toArray();
    }
    return $return;
  }

}