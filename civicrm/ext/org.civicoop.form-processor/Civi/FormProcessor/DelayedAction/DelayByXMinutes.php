<?php
/**
 * @author Jaap Jansma <jaap.jansma@civicoop.org>
 * @license AGPL-3.0
 */

namespace Civi\FormProcessor\DelayedAction;

use Civi\FormProcessor\Config\Specification;
use Civi\FormProcessor\Config\SpecificationBag;
use Civi\FormProcessor\DataBag;
use CRM_FormProcessor_ExtensionUtil as E;

class DelayByXMinutes extends AbstractDelayedAction {

  /**
   * @return String
   */
  public function getTitle() {
    return E::ts('Delay by X minutes');
  }

  /**
   * @param DataBag $data
   * @return \DateTime
   * @throws \Exception
   */
  public function getDelayedTo(DataBag $data) {
    $date = new \DateTime();
    $minutes = $this->getConfiguration()->get('minutes');
    $date->modify("+{$minutes} minutes");
    return $date;
  }

  /**
   * Get the configuration specification
   *
   * @return SpecificationBag
   */
  public function getConfigurationSpecification() {
    return new SpecificationBag(array(
      new Specification('minutes', 'Integer', E::ts('Minutes'))
    ));
  }

}