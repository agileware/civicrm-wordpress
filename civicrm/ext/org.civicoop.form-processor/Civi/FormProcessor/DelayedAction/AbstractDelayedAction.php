<?php
/**
 * @author Jaap Jansma <jaap.jansma@civicoop.org>
 * @license AGPL-3.0
 */

namespace Civi\FormProcessor\DelayedAction;

use Civi\FormProcessor\Config\ConfigurationBag;
use \Civi\FormProcessor\Config\SpecificationBag;
use Civi\FormProcessor\DataBag;

abstract class AbstractDelayedAction {

  /**
   * @var ConfigurationBag
   */
  protected $configuration;

  /**
   * @var ConfigurationBag
   */
  protected $defaultConfiguration;

  /**
   * @return String
   */
  abstract public function getTitle();

  /**
   * @param DataBag $data
   * @return \DateTime
   */
  abstract public function getDelayedTo(DataBag $data);

  /**
   * Get the configuration specification
   *
   * @return SpecificationBag
   */
  abstract public function getConfigurationSpecification();

  public function __construct() {
    $this->configuration = null;
    $this->defaultConfiguration = null;
  }

  /**
   * @return ConfigurationBag
   */
  public function getConfiguration() {
    if (!$this->configuration) {
      $this->configuration = clone $this->getDefaultConfiguration();
    }
    return $this->configuration;
  }

  /**
   * @return ConfigurationBag
   */
  public function getDefaultConfiguration() {
    if (!$this->defaultConfiguration) {
      $this->defaultConfiguration = new ConfigurationBag();
      foreach($this->getConfigurationSpecification() as $spec) {
        $this->defaultConfiguration->set($spec->getName(), $spec->getDefaultValue());
      }
    }
    return $this->defaultConfiguration;
  }

  /**
   * @param ConfigurationBag $configuration
   * @return \Civi\FormProcessor\DelayedAction\AbstractDelayedAction
   */
  public function setConfiguration(ConfigurationBag $configuration) {
    $this->configuration = $configuration;
    return $this;
  }

  /**
   * Converts the handler to an array
   *
   * @return array
   */
  public function toArray() {
    return array(
      'name' => $this->getName(),
      'title' => $this->getTitle(),
      'configuration_spec' => $this->getConfigurationSpecification()->toArray(),
      'configuration' => $this->getConfiguration()->toArray(),
      'default_configuration' => $this->getDefaultConfiguration()->toArray(),
    );
  }

  /**
   * Returns the name of the output handler.
   *
   * @return string
   * @throws \Exception
   */
  public function getName() {
    $reflect = new \ReflectionClass($this);
    $className = $reflect->getShortName();
    return $className;
  }

}