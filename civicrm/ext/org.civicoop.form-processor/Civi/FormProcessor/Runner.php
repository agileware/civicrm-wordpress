<?php
/**
 * Copyright (C) 2022  Jaap Jansma (jaap.jansma@civicoop.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace Civi\FormProcessor;

use API_Exception;
use Civi\ActionProvider\Exception\BreakException;
use Civi\ActionProvider\Metadata;
use Civi\ActionProvider\Parameter\ParameterBag;
use Civi\ActionProvider\Parameter\Specification;
use Civi\ActionProvider\Parameter\SpecificationBag;
use Civi\ActionProvider\Provider;
use Civi\FormProcessor\Utils\Cache;
use CRM_Core_Transaction;
use CRM_FormProcessor_BAO_FormProcessorDefaultDataAction;
use CRM_FormProcessor_BAO_FormProcessorDefaultDataInput;
use CRM_FormProcessor_BAO_FormProcessorInput;
use CRM_FormProcessor_BAO_FormProcessorInstance;
use CRM_FormProcessor_Condition;
use CRM_Queue_DAO_QueueItem;
use CRM_Queue_Service;
use CRM_Queue_Task;
use CRM_Utils_Time;
use DateTime;
use Throwable;
use CRM_FormProcessor_ExtensionUtil as E;

class Runner {

  /**
   * Returns an empty databag
   *
   * @return \Civi\FormProcessor\Databag
   */
  public static function createDatabag(): Databag {
    return new DataBag();
  }

  public static function getFormProcessorInputMapping(string $key, array $inputs): array {
    $return = [];
    foreach ($inputs as $input) {
      $return[$key . '.' . $input['name']] = E::ts('Input :: ') . $input['title'];
    }
    return $return;
  }

  public static function getFormattedMapping(): array {
    $return = [];
    $formattedFormats = DatabagFormatter::getFormats();
    foreach($formattedFormats as $format => $formatLabel) {
      $return['formatted.' . $format] = E::ts('Formatted :: ') . $formatLabel;
    }
    return $return;
  }

  public static function getActionMapping(string $key, string $label, array $actions, Provider $provider, $id=null): array {
    $return = [];
    foreach ($actions as $action) {
      if ($id && $action['id'] == $id) {
        break;
      }
      $actionClass = $provider->getActionByName($action['type']);
      if ($actionClass) {
        foreach ($actionClass->getOutputSpecification() as $spec) {
          $return[$key . '.' . $action['name'] . '.' . $spec->getName()] = $label . ' :: ' . $action['title'] . ' :: ' . $spec->getTitle();
        }
      }
    }
    return $return;
  }

  /**
   * @param string $formProcessorName
   * @param array $params
   *
   * @return array
   * @throws \API_Exception
   */
  public static function run(string $formProcessorName, array $params): array {
    $actionProvider = form_processor_get_action_provider();
    $dataBag = self::createDatabag();

    $validationErrors = self::runValidation($dataBag,$formProcessorName, $params);
    if (count($validationErrors)) {
      throw new API_Exception('Form Processor Validation Failed: '. implode(", ", $validationErrors));
    }
    $calculatedValues = self::runCalculations($dataBag, $formProcessorName, $params, FALSE);
    foreach ($calculatedValues as $param => $value) {
      $params[$param] = $value;
    }

    try {
      $formProcessor = self::getFormProcessor($formProcessorName);
    }
    catch (Throwable $e) {
      throw new API_Exception('Could not load form processor: '.$formProcessorName);
    }
    self::setMetadata($actionProvider->getMetadata(), $formProcessor['name'], $formProcessor['id']);

    $inputErrors = self::processInputs($dataBag, $formProcessor,  $params,'inputs', 'parameter_mapping');
    if (count($inputErrors)) {
      // TODO: When PHP 8.1 is the min supported version, this can be just `array_merge(...$inputErrors);`
      $inputErrorsFlat = array_merge(...array_values($inputErrors));
      throw new API_Exception('Could not process inputs: '. implode(", ", $inputErrorsFlat));
    }
    $actionErrors = self::executeActions($dataBag, $formProcessor, 'actions');
    if (count($actionErrors)) {
      throw new API_Exception('Could not process actions: '. implode(", ", $actionErrors));
    }

    return $formProcessor['output_handler']->handle($dataBag, $formProcessorName);
  }

  /**
   * @param string $formProcessorName
   * @param array $params
   *
   * @return array
   * @throws \API_Exception
   */
  public static function runDefaults(string $formProcessorName, array $params): array {
    $dataBag = new DataBag();
    try {
      $formProcessor = self::getFormProcessor($formProcessorName);
    }
    catch (Throwable $e) {
      throw new API_Exception('Could not load form processor: '.$formProcessorName);
    }
    $actionProvider = form_processor_get_action_provider();
    self::setMetadata($actionProvider->getMetadata(), $formProcessor['name'], $formProcessor['id']);
    if (empty($formProcessor['enable_default_data'])) {
      throw new API_Exception('Default data is not enabled');
    }

    $inputErrors = self::processInputs($dataBag, $formProcessor,  $params,'default_data_inputs', 'default_data_parameter_mapping');
    if (count($inputErrors)) {
      // TODO: When PHP 8.1 is the min supported version, this can be just `array_merge(...$inputErrors);`
      $inputErrorsFlat = array_merge(...array_values($inputErrors));
      throw new API_Exception('Could not process inputs: '. implode(", ", $inputErrorsFlat));
    }
    $actionErrors = self::executeActions($dataBag, $formProcessor, 'default_data_actions');
    if (count($actionErrors)) {
      throw new API_Exception('Could not process actions: '. implode(", ", $actionErrors));
    }

    $return = array();
    foreach ($formProcessor['default_data_output_configuration'] as $field => $alias) {
      foreach ($formProcessor['inputs'] as $input) {
        if ($input['name'] == $field) {
          $return[$field] = $input['type']->denormalizeValue($dataBag->getDataByAlias($alias));
        }
      }
    }
    return $return;
  }

  /**
   * Process the inputs and add them to the databag.
   *
   * @param \Civi\FormProcessor\DataBag $dataBag
   * @param $formProcessor
   * @param array $params
   * @param string $inputSubset
   * @param string $inputParameterMapping
   * @param array|null $limitToCertainInputs
   *
   * @return array
   */
  protected static function processInputs(DataBag $dataBag, $formProcessor, array $params, string $inputSubset, string $inputParameterMapping, array $limitToCertainInputs = null): array {
    // Validate the parameters.
    $inputErrors = [];
    foreach ($formProcessor[$inputSubset] as $input) {
      if (is_array($limitToCertainInputs) && !in_array($input['name'], $limitToCertainInputs)) {
        continue;
      }

      /** @var \Civi\FormProcessor\Type\AbstractType $inputType */
      $inputType = $input['type'];
      $inputType->setParameters(self::inputParameterMapping($input[$inputParameterMapping], $params));
      unset($input['type']);
      $objInput = new CRM_FormProcessor_BAO_FormProcessorDefaultDataInput();
      $objInput->copyValues($input);

      // Set default value
      if (!isset($params[$input['name']]) && isset($input['default_value']) && $input['default_value'] != '') {
        $params[$input['name']] = $inputType->getDefaultValue($input['default_value']);
      }

      if ($input['is_required'] && !isset($params[$input['name']])) {
        $inputErrors[$input['name']][] = E::ts('Parameter '.$input['name'].' is required');
      }
      if (!empty($params[$input['name']]) && !$inputType->validateValue($params[$input['name']], $params)) {
        $inputErrors[$input['name']][] = E::ts('Parameter '.$input['name'].' is invalid');
      }
      // Check the validations on the input.
      if (isset($params[$input['name']]) && $params[$input['name']] != "") {
        foreach ($input['validators'] as $validator) {
          if (!$validator['validator']->validate($params[$input['name']], $inputType)) {
            $inputErrors[$input['name']][] = $validator['validator']->getInvalidMessage();
          }
        }
      }

      if (key_exists($objInput->name, $params)) {
        $dataBag->setInputData($objInput, $inputType->normalizeValue($params[$input['name']]), $inputType);
      }
    }
    return $inputErrors;
  }

  /**
   * Execute the actions subset of a form processor.
   *
   * @param \Civi\FormProcessor\DataBag $dataBag
   * @param array $formProcessor
   * @param string $actionSubset
   *
   * @return array
   */
  protected static function executeActions(DataBag $dataBag, array $formProcessor, string $actionSubset): array {
    $errors = [];
    $transaction = new CRM_Core_Transaction();
    $delayedActionFactory = form_processor_get_delayed_action_factory();
    $actionProvider = form_processor_get_action_provider_for_default_data();

    foreach ($formProcessor[$actionSubset] as $action) {
      try {
        $actionClass = $actionProvider->getActionByName($action['type']);
        $actionClass->getConfiguration()->fromArray($action['configuration'], $actionClass->getConfigurationSpecification());
        $objAction = new CRM_FormProcessor_BAO_FormProcessorDefaultDataAction();
        $objAction->copyValues($action);
        $action['mapping'] = self::processMultipleValueParameters($actionClass->getParameterSpecification(), $action['mapping']);
        // Create a parameter bag for the action
        $mappedParameterBag = self::convertDataBagToMappedParameterBag($dataBag, $action['mapping'], $actionProvider);

        // Create a condition class for this action
        if (!is_array($action['condition_configuration'])) {
          $action['condition_configuration'] = [];
        }
        if (!isset($action['condition_configuration']['parameter_mapping']) || !is_array($action['condition_configuration']['parameter_mapping'])) {
          $action['condition_configuration']['parameter_mapping'] = [];
        }
        $mappedConditionParameterBag = self::convertDataBagToMappedParameterBag($dataBag, $action['condition_configuration']['parameter_mapping'], $actionProvider);
        if (!isset($action['condition_configuration']['output_mapping']) || !is_array($action['condition_configuration']['output_mapping'])) {
          $action['condition_configuration']['output_mapping'] = [];
        }
        $mappedConditionOutputParameterBag = self::convertDataBagToMappedParameterBag($dataBag, $action['condition_configuration']['output_mapping'], $actionProvider);
        $condition = CRM_FormProcessor_Condition::getConditionClass($action['condition_configuration']);
        $actionClass->setCondition($condition);
        if (isset($action['delay']) && $action['delay']) {
          $delayClass = $delayedActionFactory->getHandlerByName($action['delay']);
          $configuration = $delayClass->getDefaultConfiguration();
          if (is_array($action['delay_configuration'])) {
            foreach ($action['delay_configuration'] as $name => $value) {
              $configuration->set($name, $value);
            }
          }
          $delayClass->setConfiguration($configuration);
          $delayTo = $delayClass->getDelayedTo($dataBag);
          self::queueAction($formProcessor['name'], $action['name'], $mappedParameterBag, $mappedConditionParameterBag, $delayTo, $actionSubset);
        }
        else {
          // Now execute the action
          $outputBag = $actionClass->execute($mappedParameterBag, $mappedConditionParameterBag, $mappedConditionOutputParameterBag);
          // Add the output of the action to the data bag of this action.
          $dataBag->setActionDataFromActionProviderParameterBag($objAction, $outputBag);
        }
      }
      catch (BreakException $e) {
        // BreakException should halt flow, but not rollback
        break;
      }
      catch (Throwable $e) {
        $rollback = true;
        if (method_exists($e, 'rollbackDBTransaction')) {
          $rollback = $e->rollbackDBTransaction();
        }
        if ($rollback) {
          $transaction->rollback();
        }
        $errors[] = E::ts('Error in action %1: %2', [1=>$action['name'], 2=>$e->getMessage()]);
        break;
      }
    }

    $transaction->commit();
    return $errors;
  }

  /**
   * @param \Civi\FormProcessor\DataBag $dataBag
   * @param string $formProcessorName
   * @param array $params
   *
   * @return array
   * @throws \API_Exception
   */
  public static function runValidation(DataBag $dataBag, string $formProcessorName, array $params): array {
    $genericErrors = [];
    $actionProvider = form_processor_get_action_provider_for_validation();
    $dataBag->setValidationState();
    try {
      $formProcessor = self::getFormProcessor($formProcessorName);
    }
    catch (Throwable $e) {
      throw new API_Exception('Could not load form processor: '.$formProcessorName);
    }
    self::setMetadata($actionProvider->getMetadata(), $formProcessor['name'], $formProcessor['id']);
    $inputErrors = self::processInputs($dataBag, $formProcessor, $params, 'inputs', 'parameter_mapping');
    if (!count($inputErrors)) {
      $genericErrors = self::executeActions($dataBag, $formProcessor, 'validate_actions');
    }

    if (!count($inputErrors) && !count($genericErrors)) {
      // Execute the validation rules
      foreach ($formProcessor['validate_validators'] as $validator) {
        $validatorClass = $actionProvider->getValidatorByName($validator['type']);
        $validatorClass->getConfiguration()
          ->fromArray($validator['configuration'], $validatorClass->getConfigurationSpecification());

        // Handle multiple-value parameters.
        $validator['mapping'] = self::processMultipleValueParameters($validatorClass->getParameterSpecification(), $validator['mapping']);
        // Create a parameter bag for the action
        $mappedParameterBag = self::convertDataBagToMappedParameterBag($dataBag, $validator['mapping'], $actionProvider);
        try {
          $message = $validatorClass->validate($mappedParameterBag);
        }
        catch (Throwable $e) {
          $message = $e->getMessage();
        }
        if (!empty($message)) {
          if ($validator['inputs']) {
            foreach ($validator['inputs'] as $validatorInput) {
              $key = substr($validatorInput, strlen("input."));
              $inputErrors[$key][] = $message;
            }
          }
          else {
            $genericErrors[] = $message;
          }
        }
      }
    }
    $dataBag->resetState();

    if (count($genericErrors)) {
      foreach($formProcessor['inputs'] as $input) {
        if (!isset($inputErrors[$input['name']])) {
          $inputErrors[$input['name']] = [];
        }
        $inputErrors[$input['name']] = array_merge($genericErrors, $inputErrors[$input['name']]);
      }
    }
    foreach($inputErrors as $input => $messages) {
      $inputErrors[$input] = implode("\r\n", $messages);
    }
    return array_filter($inputErrors, function($v) {
      return !empty($v);
    });
  }

  /**
   * @param \Civi\FormProcessor\DataBag $dataBag
   * @param string $formProcessorName
   * @param array $params
   *
   * @return array
   * @throws \API_Exception
   */
  public static function runCalculations(DataBag $dataBag, string $formProcessorName, array $params, bool $runValidation): array {
    if ($runValidation) {
      $validationErrors = self::runValidation($dataBag,$formProcessorName, $params);
      if (count($validationErrors)) {
        throw new API_Exception('Form Processor Validation Failed: '. implode(", ", $validationErrors));
      }
    }

    $dataBag->setCalcuationState();
    $return = [];
    $actionProvider = form_processor_get_action_provider_for_calculation();
    try {
      $formProcessor = self::getFormProcessor($formProcessorName);
    }
    catch (Throwable $e) {
      throw new API_Exception('Could not load form processor: '.$formProcessorName);
    }
    self::setMetadata($actionProvider->getMetadata(), $formProcessor['name'], $formProcessor['id']);

    $triggers = CRM_FormProcessor_BAO_FormProcessorInstance::getCalculationTriggers($formProcessorName);
    $inputErrors = self::processInputs($dataBag, $formProcessor, $params, 'inputs', 'parameter_mapping', $triggers);
    if (count($inputErrors)) {
      // TODO: When PHP 8.1 is the min supported version, this can be just `array_merge(...$inputErrors);`
      $inputErrorsFlat = array_merge(...array_values($inputErrors));
      throw new API_Exception('Could not process inputs: '. implode(", ", $inputErrorsFlat));
    }
    $actionErrors = self::executeActions($dataBag, $formProcessor, 'calculate_actions');
    if (count($actionErrors)) {
      throw new API_Exception('Could not process actions: '. implode(", ", $actionErrors));
    }
    foreach ($formProcessor['calculation_output_configuration'] as $field => $alias) {
      foreach ($formProcessor['inputs'] as $input) {
        if ($input['name'] == $field) {
          $return[$field] = $input['type']->denormalizeValue($dataBag->getDataByAlias($alias));
        }
      }
    }
    $dataBag->resetState();

    return $return;
  }

  /**
   * Gets the form processor from the cache.
   * @param $name
   *
   * @return array
   * @throws \Exception
   */
  public static function getFormProcessor($name): array {
    $cachekey = 'FormProcessor.run.get.'.$name;
    if (!$formProcessor = Cache::get($cachekey)) {
      // Find the form processor
      $formProcessors = CRM_FormProcessor_BAO_FormProcessorInstance::getValues(['name' => $name]);
      if (count($formProcessors) != 1) {
        throw new API_Exception('Could not find a form processor');
      }
      $formProcessor = reset($formProcessors);
      Cache::setForFormProcessor($name, $cachekey, $formProcessor);
    }
    return $formProcessor;
  }

  /**
   * Return a list of all form processors.
   *
   * @return array
   * @throws \CRM_Core_Exception
   */
  public static function listFormProcessors(): array {
    $cachekey = 'FormProcessor.run.lists';
    try {
      $options = Cache::get($cachekey);
    }
    catch (Throwable $e) {
      $options = null;
    }
    if (!$options || !is_array($options)) {
      $options = [];
      $params['is_active'] = 1;
      $form_processors = CRM_FormProcessor_BAO_FormProcessorInstance::getValues($params);
      foreach ($form_processors as $form_processor) {
        $options[$form_processor['name']] = [
          'name' => $form_processor['name'],
          'title' => $form_processor['title'],
        ];
      }
      Cache::set( $cachekey, $options);
    }
    return $options;
  }

  public static function setMetadata(Metadata $metadata, string $form_processor_name, int $form_processor_id): void {
    $metadata->getSpecificationBag()->addSpecification(new Specification('form_processor_name', 'String', E::ts('Form Processor Name')));
    $metadata->getSpecificationBag()->addSpecification(new Specification('form_processor_id', 'Integer', E::ts('Form Processor ID')));
    $metadata->getMetadata()->setParameter('form_processor_name', $form_processor_name);
    $metadata->getMetadata()->setParameter('form_processor_id', $form_processor_id);
  }

  /**
   * @param string $formProcessorName
   *
   * @return array
   * @throws API_Exception
   */
  public static function getFormProcessorOutputs(string $formProcessorName): array {
    $fields = [];
    $actionProvider = form_processor_get_action_provider();
    try {
      $formProcessor = self::getFormProcessor($formProcessorName);
    }
    catch (Throwable $e) {
      throw new API_Exception('Could not load form processor: '.$formProcessorName);
    }
    self::setMetadata($actionProvider->getMetadata(), $formProcessor['name'], $formProcessor['id']);
    $outputSpecs = $formProcessor['output_handler']->getOutputSpecification($formProcessorName);
    /** @var \Civi\FormProcessor\Config\Specification $outputSpec */
    foreach($outputSpecs as $outputSpec) {
      $field = array(
        'name' => $outputSpec->getName(),
        'title' => $outputSpec->getTitle(),
        'description' => $outputSpec->getDescription(),
        'type' => $outputSpec->getDataType(),
        'api.required' => $outputSpec->isRequired(),
        'api.aliases' => array(),
        'entity' => 'FormProcessor',
      );
      if (is_array($outputSpec->getOptions())) {
        $field['options'] = $outputSpec->getOptions();
      }
      $fields[$outputSpec->getName()] = $field;
    }
    return $fields;
  }

  /**
   * Converts a DataBag object to a mapped parameterBag.
   *
   * @param DataBag $dataBag
   * @param array $mapping
   * @param \Civi\ActionProvider\Provider $actionProvider
   *
   * @return ParameterBag;
   */
  protected static function convertDataBagToMappedParameterBag(DataBag $dataBag, array $mapping, Provider $actionProvider): ParameterBag {
    $parameterBag = $actionProvider->createParameterBag();
    $fields = $dataBag->getAllAliases();
    foreach($fields as $field) {
      $parameterBag->setParameter($field, $dataBag->getDataByAlias($field));
    }
    return $actionProvider->createdMappedParameterBag($parameterBag, $mapping);
  }

  /**
   * Returns a mapped array
   *
   * @param array $mapping
   * @param array $params
   *
   * @return array
   */
  public static function inputParameterMapping(array $mapping, array $params): array {
    $return = [];
    foreach($mapping as $key => $param) {
      if (isset($params[$param])) {
        $return[$key] = $params[$param];
      }
    }
    return $return;
  }

  /**
   * Save a delayed action into the queue
   *
   * @param string $form_processor_name
   * @param string $action_name
   * @param ParameterBag $mappedParameterBag
   * @param ParameterBag $mappedConditionParameterBag
   * @param \DateTime $delayedTo
   * @param string $actionSubset
   */
  private static function queueAction(string $form_processor_name, string $action_name, ParameterBag $mappedParameterBag, ParameterBag $mappedConditionParameterBag, DateTime $delayedTo, string $actionSubset = 'actions'): void {
    $queue = CRM_Queue_Service::singleton()->create(array(
      'type' => 'FormProcessor',
      'name' => 'FormProcessorDelayedActions',
      'reset' => false, //do not flush queue upon creation
    ));

    //create a task with the action and eventData as parameters
    $task = new CRM_Queue_Task(
      array('Civi\FormProcessor\API\FormProcessor', 'executeDelayedAction'), //call back method
      array($form_processor_name, $action_name, $mappedParameterBag->toArray(), $mappedConditionParameterBag->toArray(), $actionSubset),
      'Delayed Form Processor Action: '.$form_processor_name.'.'.$action_name
    );

    //save the task with a delay
    $dao              = new CRM_Queue_DAO_QueueItem();
    $dao->queue_name  = $queue->getName();
    $dao->submit_time = CRM_Utils_Time::date('YmdHis');
    $dao->data        = serialize($task);
    $dao->weight      = 0; //weight, normal priority
    $dao->release_time = $delayedTo->format('YmdHis');
    $dao->save();
  }

  /**
   * Convert multiple-value parameter mappings to arrays.
   */
  private static function processMultipleValueParameters(SpecificationBag $parameterSpecificationBag, array $actionMapping) : array {
    foreach ($actionMapping as $mappedField => $mappedInput) {
      $spec = $parameterSpecificationBag->getSpecificationByName($mappedField);
      if ($spec !== NULL && $spec->isMultiple() && is_string($mappedInput)) {
        $actionMapping[$mappedField] = explode(',', $mappedInput);
      }
    }
    return $actionMapping;
  }

}
