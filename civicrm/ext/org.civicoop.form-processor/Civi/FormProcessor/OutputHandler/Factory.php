<?php

/**
 * @author Jaap Jansma (CiviCooP) <jaap.jansma@civicoop.org>
 * @license http://www.gnu.org/licenses/agpl-3.0.html
 */

 namespace Civi\FormProcessor\OutputHandler;

 use \Civi\FormProcessor\OutputHandler\OutputHandlerInterface;
 use \Civi\FormProcessor\OutputHandler\OutputAllActionOutput;
 use \Civi\FormProcessor\OutputHandler\FormatOutput;

 class Factory {

  /**
   * @var array<OutputHandlerInterface>
   */
  protected $handlers = array();

   /**
    * @var array
    */
  protected $handlerTitles = array();

  /**
   * @var OutputHandlerInterface
   */
  protected $defaultHandler;

  public function __construct() {
    $this->defaultHandler = new OutputAllActionOutput();
    $this->addHandler($this->defaultHandler);
    $this->addHandler(new FormatOutput());
  }

  /**
   * Returns the default output handler
   *
   * @return OutputHandlerInterface
   */
  public function getDefaultHandler() {
    return $this->defaultHandler;
  }

  /**
   * Add a handler
   *
   * @param OutputHandlerInterface $handler
   */
  public function addHandler(OutputHandlerInterface $handler) {
    $this->handlers[$handler->getName()] = $handler;
    $this->handlerTitles[$handler->getName()] = $handler->getLabel();
    return $this;
  }

  /**
   * Return the handler
   *
   * @param string $name
   * @return OutputHandlerInterface|null
   */
  public function getHandlerByName($name) {
    if (isset($this->handlers[$name])) {
      return $this->handlers[$name];
    }
    return null;
  }

  /**
   * @return array<OutputHandlerInterface>
   */
  public function getHandlers() {
    return $this->handlers;
  }

   /**
    * @return array
    */
  public function getHandlerTitles(): array {
    return $this->handlerTitles;
  }

  /**
   * Returns an array with the name and the array of the handler
   *
   * @return array<array>
   */
  public function getHandlersAsArray() {
    $return = array();
    foreach($this->handlers as $handler) {
      $return[] = $handler->toArray();
    }
    return $return;
  }

 }
