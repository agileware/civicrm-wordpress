<?php

/**
 * @author Jaap Jansma (CiviCooP) <jaap.jansma@civicoop.org>
 * @license http://www.gnu.org/licenses/agpl-3.0.html
 */

 namespace Civi\FormProcessor\OutputHandler;

 use CRM_Core_Exception;
 use Civi\ActionProvider\Parameter\SpecificationGroup;
 use Civi\FormProcessor\Config\ConvertFromActionProvider;
 use Civi\FormProcessor\Config\Specification;
 use Civi\FormProcessor\Config\SpecificationBag;
 use Civi\FormProcessor\DataBag;
 use Civi\FormProcessor\Runner;
 use CRM_FormProcessor_ExtensionUtil as E;
 use CRM_Utils_Type;

 class OutputAllActionOutput extends AbstractOutputHandler {

  /**
   * Get the configuration specification
   *
   * @return SpecificationBag
   */
  public function getConfigurationSpecification(): SpecificationBag {
    return new SpecificationBag();
  }

  /**
   * Convert the action data to output data.
   *
   * @param DataBag $dataBag
   * @param string $formProcessorName
   * @return array
   */
  public function handle(DataBag $dataBag, string $formProcessorName): array {
    $output = [];
    $actionProvider = form_processor_get_action_provider();
    try {
      $formProcessor = Runner::getFormProcessor($formProcessorName);
      foreach($dataBag->getAllInputs() as $input) {
        $output['input.' . $input->name] = $dataBag->getInputData($input);
      }
      foreach($formProcessor['actions'] as $action) {
        if (empty($action['delay'])) {
          $actionClass = $actionProvider->getActionByName($action['type']);
          $actionClass->getConfiguration()->fromArray($action['configuration'], $actionClass->getConfigurationSpecification());

          $actionKey = 'action.' . $action['name'] . '.';
          /** @var \Civi\ActionProvider\Parameter\Specification $outputSpec */
          foreach ($actionClass->getOutputSpecification() as $outputSpec) {
            if ($outputSpec instanceof SpecificationGroup) {
              /** @var \Civi\ActionProvider\Parameter\Specification $subOutputSpec */
              foreach ($outputSpec->getSpecificationBag() as $subOutputSpec) {
                $data = $dataBag->getDataByAlias($actionKey . $subOutputSpec->getName());
                $output[$actionKey . $subOutputSpec->getName()] = $data ?? '';
              }
            } else {
              $data = $dataBag->getDataByAlias($actionKey . $outputSpec->getName());
              $output[$actionKey . $outputSpec->getName()] = $data ?? '';
            }
          }
        }
      }
    }
    catch (CRM_Core_Exception $e) {
    }
    return $output;
  }

   /**
    * Returns the output of a form processor.
    *
    * @param string $formProcessorName
    * @return \Civi\FormProcessor\Config\SpecificationBag
    */
   public function getOutputSpecification(string $formProcessorName): SpecificationBag {
     $actionProvider = form_processor_get_action_provider();
     $specs = new SpecificationBag();
     try {
       $formProcessor = Runner::getFormProcessor($formProcessorName);
       foreach($formProcessor['inputs'] as $input) {
         /** @var  \Civi\FormProcessor\Type\AbstractType $inputType */
         $inputType = $input['type'];
         $dataType = CRM_Utils_Type::typeToString($inputType->getCrmType());
         if ($dataType == 'Int') {
           $dataType = 'Integer';
         }
         if ($dataType) {
           $specs->addSpecification(new Specification('input.' . $input['name'], $dataType, E::ts('Input :: %1', [1 => $input['title']])));
         }
       }
       foreach($formProcessor['actions'] as $action) {
         if (empty($action['delay'])) {
           $actionClass = $actionProvider->getActionByName($action['type']);
           $actionClass->getConfiguration()->fromArray($action['configuration'], $actionClass->getConfigurationSpecification());

           $actionKey = 'action.' . $action['name'] . '.';
           /** @var \Civi\ActionProvider\Parameter\Specification $outputSpec */
           foreach ($actionClass->getOutputSpecification() as $outputSpec) {
             if ($outputSpec instanceof SpecificationGroup) {
               foreach ($outputSpec->getSpecificationBag() as $subOutputSpec) {
                 $spec = ConvertFromActionProvider::convertSpecification($subOutputSpec);
                 $spec->setName($actionKey . $spec->getName());
                 $spec->setTitle(E::ts('Action %1 :: %2', [
                   1 => $action['title'],
                   2 => $spec->getTitle(),
                 ]));
                 $specs->addSpecification($spec);
               }
             } else {
               $spec = ConvertFromActionProvider::convertSpecification($outputSpec);
               $spec->setName($actionKey . $spec->getName());
               $spec->setTitle(E::ts('Action %1 :: %2', [
                 1 => $action['title'],
                 2 => $spec->getTitle(),
               ]));
               $specs->addSpecification($spec);
             }
           }
         }
       }
     }
     catch (CRM_Core_Exception $e) {
     }
     return $specs;
   }

  /**
   * Returns the label of the output handler.
   *
   * @return string
   */
  public function getLabel(): string {
    return E::ts('Send everything');
  }

 }
