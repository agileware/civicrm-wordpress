<?php

/**
 * @author Jaap Jansma (CiviCooP) <jaap.jansma@civicoop.org>
 * @license http://www.gnu.org/licenses/agpl-3.0.html
 */

namespace Civi\FormProcessor\Type;

use Civi\FormProcessor\Config\ConfigurationBag;
use Civi\FormProcessor\Config\Specification;
use Civi\FormProcessor\Config\SpecificationBag;
use Civi\FormProcessor\Config\SpecificationCollection;

use \CRM_FormProcessor_ExtensionUtil as E;

class CustomOptionListType extends OptionGroupType implements OptionListInterface {

  /**
   * Get the configuration specification
   *
   * @return SpecificationBag
   */
  public function getConfigurationSpecification() {
    $optionSpeBag = new SpecificationBag([
      new Specification('value', 'String', E::ts('Value'), TRUE, NULL, NULL, NULL, NULL, ''),
      new Specification('label', 'String', E::ts('Label'), TRUE, NULL, NULL, NULL, NULL, ''),
    ]);

    return new SpecificationBag([
      new Specification('multiple', 'Boolean', E::ts('Multiple'), FALSE, 0, NULL, [
        0 => E::ts('Single value'),
        1 => E::ts('Multiple values'),
      ]),
      new SpecificationCollection('options', E::ts('Options'), $optionSpeBag, 1, NULL),
    ]);
  }

  protected function loadOptions() {
    if ($this->options != NULL && $this->normalizedOptions != NULL && !$this->denormalizedOptions != NULL) {
      return;
    }

    $this->options = [];
    $this->normalizedOptions = [];
    $this->denormalizedOptions = [];
    foreach ($this->configuration->get('options') as $option) {
      $this->options[$option['value']] = $option['label'];
      $this->normalizedOptions[$option['value']] = $option['value'];
      $this->denormalizedOptions[$option['value']] = $option['value'];
    }

    $this->optionsLoaded = TRUE;
  }

}
