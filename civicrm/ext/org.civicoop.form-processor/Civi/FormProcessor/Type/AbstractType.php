<?php

/**
 * @author Jaap Jansma (CiviCooP) <jaap.jansma@civicoop.org>
 * @license http://www.gnu.org/licenses/agpl-3.0.html
 */

namespace Civi\FormProcessor\Type;

use \Civi\FormProcessor\Config\ConfigurationBag;
use \Civi\FormProcessor\Config\Specification;
use \Civi\FormProcessor\Config\SpecificationBag;

abstract class AbstractType implements \JsonSerializable {

  /**
   * @var string
   */
  protected $name;

  /**
   * @var string
   */
  protected $label;

  /**
   * @var ConfigurationBag
   */
  protected $configuration;

  /**
   * @var string
   */
  protected $default_value_description;

  /**
   * @var ConfigurationBag
   */
  protected $defaultConfiguration;

  /** @var array  */
  protected $parameters = [];

  public function __construct($name, $label) {
    $this->name = $name;
    $this->label = $label;
  }

  /**
   * Returns whether the provided value is a valid
   *
   * @param mixed $value
   * @param array $allValues
   *
   * @return bool
   */
  abstract public function validateValue($value, $allValues = []);

  /**
   * Returns the name of the type.
   *
   * @return string
   */
  public function getName() {
    return $this->name;
  }

  /**
   * Returns the type number from CRM_Utils_Type
   */
  public function getCrmType() {
    return FALSE;
  }

  /**
   * @param $field
   *
   * @return mixed
   */
  public function alterApi4FieldSpec($field) {
    $field['input_type'] = 'Text';
    if ($this instanceof OptionListInterface) {
      $field['input_type'] = 'Select';
      $field['input_attrs'] =[
        'multiple' => $this->isMultiple(),
      ];
    }
    return $field;
  }

  /**
   * Returns the label of the type.
   *
   * @return string
   */
  public function getLabel() {
    return $this->label;
  }

  /**
   * Get the configuration specification
   *
   * @return SpecificationBag
   */
  public function getConfigurationSpecification() {
    return new SpecificationBag();
  }

  /**
   * Override this function in a child class to when you expect
   * to build a validation of an option list based on the submitted values.
   *
   * @return \Civi\FormProcessor\Config\SpecificationBag
   */
  public function getParameterSpecification() {
    return new SpecificationBag();
  }

  /**
   * Override this function in a child class to when you expect
   * to build a. option list based on the default values.
   *
   * @return \Civi\FormProcessor\Config\SpecificationBag
   */
  public function getDefaultsParameterSpecification() {
    return new SpecificationBag();
  }

  public function setParameters(array $parameters) {
    $this->parameters = $parameters;
  }

  /**
   * @return bool;
   */
  public function validateConfiguration() {
    if ($this->configuration === NULL) {
      return FALSE;
    }
    return SpecificationBag::validate($this->configuration, $this->getConfigurationSpecification());
  }

  /**
   * @return ConfigurationBag
   */
  public function getConfiguration() {
    return $this->configuration;
  }

  /**
   * @return ConfigurationBag
   */
  public function getDefaultConfiguration() {
    return $this->defaultConfiguration;
  }

  /**
   * Sets the default values of this action
   */
  public function setDefaults() {
    $this->configuration = new ConfigurationBag();
    $this->defaultConfiguration = new ConfigurationBag();

    foreach ($this->getConfigurationSpecification() as $spec) {
      if ($spec->getDefaultValue() !== NULL) {
        $this->configuration->set($spec->getName(), $spec->getDefaultValue());
        $this->defaultConfiguration->set($spec->getName(), $spec->getDefaultValue());
      }
    }
  }

  /**
   * Normalize the input value.
   *
   * @param $value
   *
   * @return mixed
   */
  public function normalizeValue($value) {
    return $value;
  }

  /**
   * Denormalize the input value.
   *
   * @param $value
   *
   * @return mixed
   */
  public function denormalizeValue($value) {
    return $value;
  }

  /**
   * @param ConfigurationBag $configuration
   */
  public function setConfiguration(ConfigurationBag $configuration) {
    $this->configuration = $configuration;
    return $this;
  }

  /**
   * Returns the default value
   *
   * @param $defaultValue
   *
   * @return mixed
   */
  public function getDefaultValue($defaultValue) {
    return $defaultValue;
  }

  public function toArray() {
    $return['name'] = $this->name;
    $return['label'] = $this->label;
    $return['default_value_description'] = $this->default_value_description;
    $return['configuration_spec'] = $this->getConfigurationSpecification()
      ->toArray();
    $return['parameter_spec'] = $this->getParameterSpecification()
      ->toArray();
    $return['default_configuration'] = NULL;
    if ($this->getDefaultConfiguration()) {
      $return['default_configuration'] = $this->getDefaultConfiguration()
        ->toArray();
    }
    return $return;
  }

  /**
   * Returns the data structure to serialize it as a json
   */
  public function jsonSerialize(): array {
    $return = $this->toArray();
    // An empty array goes wrong with the default confifuration.
    if (empty($return['default_configuration'])) {
      $return['default_configuration'] = new \stdClass();;
    }
    return $return;
  }

  /**
   * @param $value
   *
   * @return array|mixed
   */
  protected function removeNullValues($value) {
    /*
     *  converts ['3'=>'3','5'=>'5','1'=>null,'2'=>null,'4'=>null] in
     *  ['3'=>'3','5'=>'5'] See issue 76
     */
    if (is_array($value)) {
      $value = array_filter($value, function ($var) {
        return $var !== NULL;
      });
    };
    return $value;
  }

}
