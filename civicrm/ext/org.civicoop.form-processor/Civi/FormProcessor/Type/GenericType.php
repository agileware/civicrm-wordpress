<?php

/**
 * @author Jaap Jansma (CiviCooP) <jaap.jansma@civicoop.org>
 * @license http://www.gnu.org/licenses/agpl-3.0.html
 */

namespace Civi\FormProcessor\Type;

use CRM_Core_Exception;
use CRM_Utils_Type;

class GenericType extends AbstractType {

  public function validateValue($value, $allValues = []) {
    try {
      if (CRM_Utils_Type::validate($value, $this->name, FALSE) === NULL) {
        return FALSE;
      }
    }
    catch (CRM_Core_Exception $e) {
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Returns the type number from CRM_Utils_Type
   */
  public function getCrmType() {
    $typeName = $this->name;
    if ($typeName == 'Integer') {
      $typeName = 'Int';
    }
    $coreTypes = [
      'Int' => CRM_Utils_Type::T_INT,
      'String' => CRM_Utils_Type::T_STRING,
      'Enum' => CRM_Utils_Type::T_ENUM,
      'Date' => CRM_Utils_Type::T_DATE,
      'Time' => CRM_Utils_Type::T_TIME,
      'Boolean' => CRM_Utils_Type::T_BOOLEAN,
      'Text' => CRM_Utils_Type::T_TEXT,
      'Blob' => CRM_Utils_Type::T_BLOB,
      'Timestamp' => CRM_Utils_Type::T_TIMESTAMP,
      'Float' => CRM_Utils_Type::T_FLOAT,
      'Money' => CRM_Utils_Type::T_MONEY,
      'Email' => CRM_Utils_Type::T_EMAIL,
      'Mediumblob' => CRM_Utils_Type::T_MEDIUMBLOB,
    ];
    if (isset($coreTypes[$typeName])) {
      return $coreTypes[$typeName];
    }
    return FALSE;
  }

  public function alterApi4FieldSpec($field) {
    $field = parent::alterApi4FieldSpec($field);
    switch ($this->name) {
      case 'Integer':
      case 'Float':
        $field['input_type'] = 'Number';
        break;
      case 'Date':
        $field['input_type'] = 'Date';
        break;
      case 'Text':
        $field['input_type'] = 'TextArea';
        break;
    }
    return $field;
  }


  /**
   * Denormalize the input value.
   *
   * @param $value
   *
   * @return int|bool|float
   */
  public function denormalizeValue($value) {
    switch ($this->getCrmType()) {
      case \CRM_Utils_Type::T_FLOAT:
      case \CRM_Utils_Type::T_MONEY:
        return (float) $value;
        break;
      case \CRM_Utils_Type::T_BOOLEAN:
        return $value ? TRUE : FALSE;
        break;
      case \CRM_Utils_Type::T_INT:
        return (int) $value;
        break;
    }
    return $value;
  }

  /**
   * Normalize the input value.
   *
   * @param $value
   *
   * @return mixed
   */
  public function normalizeValue($value) {
    if ($this->getCrmType() == CRM_Utils_Type::T_BOOLEAN) {
      $value = (bool) $value;
    }
    return parent::normalizeValue($value);
  }


}
