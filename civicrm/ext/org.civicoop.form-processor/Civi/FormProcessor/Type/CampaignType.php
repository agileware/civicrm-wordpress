<?php

/**
 * @author Jaap Jansma (CiviCooP) <jaap.jansma@civicoop.org>
 * @license http://www.gnu.org/licenses/agpl-3.0.html
 */

namespace Civi\FormProcessor\Type;

use Civi\FormProcessor\Config\ConfigurationBag;
use \Civi\FormProcessor\Config\Specification;
use \Civi\FormProcessor\Config\SpecificationBag;
use \Civi\FormProcessor\Type\AbstractType;
use \Civi\FormProcessor\Type\OptionListInterface;

use \CRM_FormProcessor_ExtensionUtil as E;

class CampaignType extends AbstractType implements OptionListInterface {

  protected $normalizedOptions;

  protected $denormalizedOptions;

  protected $options;

  /**
   * Get the configuration specification
   *
   * @return SpecificationBag
   */
  public function getConfigurationSpecification() {
    return new SpecificationBag([
      new Specification('value_field', 'String', E::ts('Value field'), TRUE, 'id', NULL, [
        'id' => E::ts('ID'),
        'name' => E::ts('Name of the campaign'),
      ]),
    ]);
  }


  public function validateValue($value, $allValues = []) {
    $options = $this->getOptions($allValues);

    $valueAttribute = $use_label = $this->configuration->get('value_field') ? $this->configuration->get('value_field') : 'id';
    $type = 'String';
    if ($valueAttribute == 'id') {
      $type = 'Integer';
    }


    if (!is_array($value)) {
      if (\CRM_Utils_Type::validate($value, $type, FALSE) === NULL) {
        return FALSE;
      }
      if (!isset($options[$value])) {
        return FALSE;
      }
    }
    else {
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Returns the type number from CRM_Utils_Type
   */
  public function getCrmType() {
    $valueAttribute = $use_label = $this->configuration->get('value_field') ? $this->configuration->get('value_field') : 'id';
    if ($valueAttribute == 'id') {
      return \CRM_Utils_Type::T_INT;
    }
    return \CRM_Utils_Type::T_STRING;
  }

  /**
   * Normalize the input value.
   *
   * @param $value
   *
   * @return mixed
   */
  public function normalizeValue($value) {
    $this->loadOptions();
    if (isset($this->normalizedOptions[$value])) {
      return $this->normalizedOptions[$value];
    }
    else {
      return NULL;
    }
  }

  /**
   * Denormalize the input value.
   *
   * @param $value
   *
   * @return mixed
   */
  public function denormalizeValue($value) {
    $this->loadOptions();
    if (isset($this->denormalizedOptions[$value])) {
      return $this->denormalizedOptions[$value];
    }
    else {
      return NULL;
    }
  }

  public function getOptions($params) {
    $this->loadOptions();
    return $this->options;
  }

  protected function loadOptions() {
    if ($this->options != NULL && $this->normalizedOptions != NULL && !$this->denormalizedOptions != NULL) {
      return;
    }
    $valueAttribute = $use_label = $this->configuration->get('value_field') ? $this->configuration->get('value_field') : 'id';
    $campaigns = civicrm_api3('Campaign', 'get', [
      'is_active' => 1,
      'options' => ['limit' => 0],
    ]);
    $this->normalizedOptions = [];
    $this->options = [];
    foreach ($campaigns['values'] as $campaign) {
      $this->options[$campaign[$valueAttribute]] = $campaign['title'];
      $this->normalizedOptions[$campaign[$valueAttribute]] = $campaign['id'];
      $this->denormalizedOptions[$campaign['id']] = $campaign[$valueAttribute];
    }
  }

  public function setConfiguration(ConfigurationBag $configuration) {
    parent::setConfiguration($configuration);
    $this->normalizedOptions = NULL;
    $this->denormalizedOptions = NULL;
    $this->options = NULL;
    return $this;
  }

  /**
   * Sets the default values of this action
   */
  public function setDefaults() {
    parent::setDefaults();
    $this->normalizedOptions = NULL;
    $this->denormalizedOptions = NULL;
    $this->options = NULL;
  }

  /**
   * Returns true when this field is a multiple field.
   *
   * @return bool
   */
  public function isMultiple() {
    return FALSE;
  }


}
