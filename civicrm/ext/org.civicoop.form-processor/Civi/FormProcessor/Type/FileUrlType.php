<?php

/**
 * @author Jaap Jansma (CiviCooP) <jaap.jansma@civicoop.org>
 * @license http://www.gnu.org/licenses/agpl-3.0.html
 */

namespace Civi\FormProcessor\Type;

use \Civi\FormProcessor\Type\GenericType;
use \Civi\FormProcessor\Config\Specification;
use \Civi\FormProcessor\Config\SpecificationBag;

use \CRM_FormProcessor_ExtensionUtil as E;

class FileUrlType extends GenericType {

  /**
   * Normalize the input value.
   *
   * @param $value
   *
   * @return mixed
   */
  public function normalizeValue($value) {
    if ( $value) {
      $finfo = new \finfo(FILEINFO_MIME_TYPE);
      $pathinfo = pathinfo($value);
      $content = file_get_contents($value);
      $normalizedValue['content']  = base64_encode($content);
      $normalizedValue['name'] = $pathinfo['basename'];
      $normalizedValue['mime_type'] = $finfo->buffer($content);

      return $normalizedValue;
    }
    return $value;
  }


  /**
   * Get the configuration specification.
   *
   * The configuration is
   *   format: a string containing the input format.
   *           see for valid formats
   * http://nl1.php.net/manual/en/datetime.createfromformat.php#refsect1-datetime.createfromformat-parameters
   *
   * @return SpecificationBag
   */
  public function getConfigurationSpecification() {
    return new SpecificationBag([]);
  }

  public function validateValue($value, $allValues = []) {
    if (\CRM_Utils_Type::validate($value, 'String', FALSE) === NULL) {
      return FALSE;
    }
    if (filter_var($value, FILTER_VALIDATE_URL) === false) {
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Returns the type number from CRM_Utils_Type
   */
  public function getCrmType() {
    return \CRM_Utils_Type::T_BLOB;
  }

}
