<?php

/**
 * @author Klaas Eikelboom (CiviCooP) <klaas.eikelboom@civicoop.org>
 * @license http://www.gnu.org/licenses/agpl-3.0.html
 */

namespace Civi\FormProcessor\Type;

use \Civi\FormProcessor\Type\AbstractType;
use \Civi\FormProcessor\Type\OptionListInterface;

class LanguageType extends AbstractType implements OptionListInterface {

  protected $options;

  /**
   * Returns true when this field is a multiple field.
   *
   * @return bool
   */
  public function isMultiple() {
    return FALSE;
  }

  public function validateValue($value, $allValues = []) {
    if (\CRM_Utils_Type::validate($value, 'String', FALSE) === NULL) {
      return FALSE;
    }

    $languageTypes = $this->getOptions($allValues);
    if (!isset($languageTypes[$value])) {
      return FALSE;
    }

    return TRUE;
  }

  /**
   * Returns the type number from CRM_Utils_Type
   */
  public function getCrmType() {
    return \CRM_Utils_Type::T_STRING;
  }

  public function getOptions($params) {
    if (!$this->options) {
      $this->options = [];
      $optionsApi = civicrm_api3('OptionValue', 'get', [
        'option_group_id' => 'languages',
        'is_active' => 1,
        'options' => ['sort' => 'weight', 'limit' => 0],
      ]);
      foreach ($optionsApi['values'] as $option) {
        $this->options[$option['name']] = $option['label'];
      }
    }
    return $this->options;
  }

  /**
   * Sets the default values of this action
   */
  public function setDefaults() {
    parent::setDefaults();
    $this->options = NULL;
  }

}
