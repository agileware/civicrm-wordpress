<?php
/**
 * Copyright (C) 2023  Jaap Jansma (jaap.jansma@civicoop.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace Civi\FormProcessor\Type;

use Civi\FormProcessor\Config\Specification;
use Civi\FormProcessor\Config\SpecificationBag;
use CRM_FormProcessor_ExtensionUtil as E;

class EntityType extends AbstractEntityType {

  public function __construct($label) {
    parent::__construct('Entity', $label);
  }

  /**
   * Returns the entity name.
   *
   * @return string
   */
  protected function getFkEntity(): string {
    return $this->configuration->get('entity');
  }

  /**
   * Get the configuration specification
   *
   * @return SpecificationBag
   */
  public function getConfigurationSpecification() {
    $entityOptions = [];
    $entities = \Civi::service('action_object_provider')->getEntities();
    foreach($entities as $entity) {
      $entityOptions[$entity['name']] = $entity['title'];
    }
    return new SpecificationBag([
      new Specification('entity', 'String', E::ts('Use CiviCRM Entity'), TRUE, 0, NULL, $entityOptions),
    ]);
  }

}
