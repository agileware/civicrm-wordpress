<?php

/**
 * @author Jaap Jansma (CiviCooP) <jaap.jansma@civicoop.org>
 * @license http://www.gnu.org/licenses/agpl-3.0.html
 */

namespace Civi\FormProcessor\Type;

use \Civi\FormProcessor\Config\Specification;
use \Civi\FormProcessor\Config\SpecificationBag;
use CRM_FormProcessor_ExtensionUtil as E;

class FormattedDecimalType extends AbstractType {

  public function __construct($label) {
    $this->name = 'FormattedDecimal';
    $this->label = $label;
  }

  public function validateValue($value, $allValues = []) {
    $value = $this->normalizeValue($value);
    if (\CRM_Utils_Type::validate($value, 'Float', FALSE) === NULL) {
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Returns the type number from CRM_Utils_Type
   */
  public function getCrmType() {
    return \CRM_Utils_Type::T_FLOAT;
  }

  /**
   * Get the configuration specification
   *
   * @return SpecificationBag
   */
  public function getConfigurationSpecification() {
    return new SpecificationBag([
      new Specification('decimal_point', 'String', E::ts('Decimal point'), TRUE, '.', NULL, NULL),
      new Specification('thousand_sep', 'String', E::ts('Thousand separator'), FALSE, ',', NULL, NULL),
      new Specification('decimals', 'Integer', E::ts('Decimals'), TRUE, 2, NULL, NULL),
    ]);
  }

  /**
   * Denormalize the input value.
   *
   * @param $value
   *
   * @return mixed
   */
  public function denormalizeValue($value) {
    $decimal_point = $this->configuration->doesConfigExists('decimal_point') ? $this->configuration->get('decimal_point') : NULL;
    $thousand_sep = $this->configuration->doesConfigExists('thousand_sep') ? $this->configuration->get('thousand_sep') : '';
    $decimals = $this->configuration->doesConfigExists('decimals') ? $this->configuration->get('decimals') : 0;
    return number_format($value, $decimals, $decimal_point, $thousand_sep);
  }

  /**
   * Normalize the input value
   *
   * @param $value
   *
   * @return float|mixed
   */
  public function normalizeValue($value) {
    // Taken from: https://www.php.net/manual/en/function.floatval.php#114486
    $dotPos = strrpos($value, '.');
    $commaPos = strrpos($value, ',');
    $sep = (($dotPos > $commaPos) && $dotPos) ? $dotPos :
      ((($commaPos > $dotPos) && $commaPos) ? $commaPos : FALSE);

    if (!$sep) {
      return floatval(preg_replace("/[^0-9]/", "", $value));
    }

    return floatval(
      preg_replace("/[^0-9]/", "", substr($value, 0, $sep)) . '.' .
      preg_replace("/[^0-9]/", "", substr($value, $sep + 1, strlen($value)))
    );
  }


}
