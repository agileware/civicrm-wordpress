<?php

/**
 * @author Jaap Jansma (CiviCooP) <jaap.jansma@civicoop.org>
 * @license http://www.gnu.org/licenses/agpl-3.0.html
 */

namespace Civi\FormProcessor\Type;

use \Civi\FormProcessor\Type\GenericType;
use \Civi\FormProcessor\Config\Specification;
use \Civi\FormProcessor\Config\SpecificationBag;

use \CRM_FormProcessor_ExtensionUtil as E;

class FileType extends GenericType {


  /**
   * Get the configuration specification.
   *
   * The configuration is
   *   format: a string containing the input format.
   *           see for valid formats
   * http://nl1.php.net/manual/en/datetime.createfromformat.php#refsect1-datetime.createfromformat-parameters
   *
   * @return SpecificationBag
   */
  public function getConfigurationSpecification() {
    return new SpecificationBag([]);
  }

  public function validateValue($value, $allValues = []) {
    if (!is_array($value)) {
      return FALSE;
    }
    if (!isset($value['id'])) {
      if (!isset($value['name']) || !isset($value['mime_type']) || (!isset($value['content']) && !isset($value['url']))) {
        return FALSE;
      }
    }
    return TRUE;

  }

  /**
   * Returns the type number from CRM_Utils_Type
   */
  public function getCrmType() {
    return \CRM_Utils_Type::T_BLOB;
  }

}
