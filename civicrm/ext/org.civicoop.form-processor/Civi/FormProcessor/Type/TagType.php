<?php

/**
 * @author Jaap Jansma (CiviCooP) <jaap.jansma@civicoop.org>
 * @license http://www.gnu.org/licenses/agpl-3.0.html
 */

namespace Civi\FormProcessor\Type;

use Civi\FormProcessor\Config\Specification;
use Civi\FormProcessor\Config\SpecificationBag;
use Civi\FormProcessor\Type\AbstractType;
use Civi\FormProcessor\Type\OptionListInterface;

use CRM_FormProcessor_ExtensionUtil as E;

class TagType extends AbstractType implements OptionListInterface {

  /**
   * Returns true when this field is a multiple field.
   *
   * @return bool
   */
  public function isMultiple() {
    return $this->configuration->get('multiple') == 1 ? TRUE : FALSE;
  }

  /**
   * Get the configuration specification
   *
   * @return SpecificationBag
   */
  public function getConfigurationSpecification() {
    $tagsParams['used_for'] = 'civicrm_contact';
    $tagsParams['options']['limit'] = 0;
    $tagApi = civicrm_api3('Tag', 'get', $tagsParams);
    $tags = [];
    $tagSets = [];
    foreach ($tagApi['values'] as $tag) {
      if (isset($tag['is_tagset']) && $tag['is_tagset']) {
        $tagSets[$tag['name']] = $tag['name'];
      } else {
        $tags[$tag['name']] = $tag['name'];
      }
    }

    return new SpecificationBag([
      new Specification('multiple', 'Boolean', E::ts('Multiple'), TRUE, 0, NULL, [
        0 => E::ts('Single value'),
        1 => E::ts('Multiple values'),
      ]),
      new Specification('tagsets', 'String', E::ts("Limit to tagset"), false, null, null, $tagSets, true),
      new Specification('tags', 'String', E::ts("Limit to tags"), false, null, null, $tags, true),
    ]);
  }

  public function validateValue($value, $allValues = []) {
    $multiple = $this->configuration->get('multiple') ? TRUE : FALSE;
    $groups = $this->getOptions($allValues);
    if ($multiple && is_array($value)) {
      foreach ($value as $valueItem) {
        if (\CRM_Utils_Type::validate($valueItem, 'Integer', FALSE) === NULL) {
          return FALSE;
        }
        if (!isset($groups[$valueItem])) {
          return FALSE;
        }
      }
    }
    elseif (!is_array($value) && $value) {
      if (\CRM_Utils_Type::validate($value, 'Integer', FALSE) === NULL) {
        return FALSE;
      }

      if (!isset($groups[$value])) {
        return FALSE;
      }
    }
    else {
      return FALSE;
    }

    return TRUE;
  }

  /**
   * Returns the type number from CRM_Utils_Type
   */
  public function getCrmType() {
    return \CRM_Utils_Type::T_INT;
  }

  /**
   * Normalize the input value.
   *
   * @param $value
   *
   * @return mixed
   */
  public function normalizeValue($value) {
    $multiple = $this->configuration->get('multiple') ? TRUE : FALSE;

    // Correct array values when field is not multiple.
    // this could be caused for example by a drupal webform with
    // a checkbox field. The value is submitted as an array, altough it contains
    // only one value.
    if (!$multiple && is_array($value) && count($value) == 1) {
      $value = reset($value);
    }

    return $value;
  }

  public function getOptions($params) {
    static $return;
    if ($return) {
      return $return;
    }
    $tagsParams['used_for'] = 'civicrm_contact';
    $tagsParams['is_tagset'] = '0';
    if (is_array($this->configuration->get('tagsets')) && count($this->configuration->get('tagsets'))) {
      $tagsParams['parent_id'] = ['IN' => $this->configuration->get('tagsets')];
    } elseif (strlen($this->configuration->get('tagsets'))) {
      $tagsParams['parent_id'] = $this->configuration->get('tagsets');
    }
    $tagsParams['options']['limit'] = 0;
    $tags = civicrm_api3('Tag', 'get', $tagsParams);
    $return = [];
    foreach ($tags['values'] as $tag) {
      if (is_array($this->configuration->get('tags')) && count($this->configuration->get('tags'))) {
        if (in_array($tag['name'], $this->configuration->get('tags'))) {
          $return[$tag['id']] = $tag['name'];
        }
      } else {
        $return[$tag['id']] = $tag['name'];
      }
    }
    return $return;
  }

}
