<?php

/**
 * @author Jaap Jansma (CiviCooP) <jaap.jansma@civicoop.org>
 * @license http://www.gnu.org/licenses/agpl-3.0.html
 */

namespace Civi\FormProcessor\Type;

interface OptionListInterface {

  /**
   * Returns an array with the options.
   *
   * The key of the array is the value and the array item is the label.
   *
   * @param array $params
   *   This array contains the params used to call getFields. This way we could
   *   build conditional option lists.
   *
   * @return array
   */
  public function getOptions($params);

  /**
   * Returns true when this field is a multiple field.
   *
   * @return bool
   */
  public function isMultiple();

}
