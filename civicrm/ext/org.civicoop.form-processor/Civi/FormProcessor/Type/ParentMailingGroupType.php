<?php

/**
 * @author Erik Hommel (CiviCooP) <erik.hommel@civicoop.org>
 * @date 18 Sep 2019
 * @license http://www.gnu.org/licenses/agpl-3.0.html
 */

namespace Civi\FormProcessor\Type;

use \Civi\FormProcessor\Config\Specification;
use \Civi\FormProcessor\Config\SpecificationBag;
use \Civi\FormProcessor\Type\AbstractType;
use \Civi\FormProcessor\Type\OptionListInterface;

use \CRM_FormProcessor_ExtensionUtil as E;

class ParentMailingGroupType extends AbstractType implements OptionListInterface {

  private $_childGroups;

  private $_groupsLoaded = FALSE;

  /**
   * Returns true when this field is a multiple field.
   *
   * @return bool
   */
  public function isMultiple() {
    return $this->configuration->get('multiple') == 1 ? TRUE : FALSE;
  }

  /**
   * Get the configuration specification
   *
   * @return SpecificationBag
   */
  public function getConfigurationSpecification() {
    $parentGroups = [];
    try {
      $parentGroupsApi = civicrm_api3('Group', 'get', [
        'return' => ["title", "description"],
        'is_active' => 1,
        'children' => ['IS NOT NULL' => 1],
        'options' => ['limit' => 0],
      ]);
    } catch (\CiviCRM_API3_Exception $ex) {
    }
    foreach ($parentGroupsApi['values'] as $groupdId => $groupData) {
      $groupText = $groupData['title'];
      if (isset($groupData['description'])) {
        if (strlen($groupData['description']) > 80) {
          $groupText .= " (" . substr($groupData['description'], 0, 80) . "...)";
        }
        else {
          $groupText .= " (" . $groupData['description'] . ")";
        }
      }
      $parentGroups[$groupdId] = $groupText;
    }
    return new SpecificationBag([
      new Specification('parent_group_id', 'Integer', E::ts('Parent Mailing Group'), TRUE, NULL, NULL, $parentGroups, FALSE),
      new Specification('multiple', 'Boolean', E::ts('Multiple'), FALSE, 0, NULL, [
        0 => E::ts('Single value'),
        1 => E::ts('Multiple values'),
      ]),
      new Specification('visibility', 'Integer', E::ts('Restrict to'), FALSE, 0, NULL, [
        'Public Pages' => E::ts('Public Pages'),
        'User and User Admin Only' => E::ts('User and User Admin Only'),
      ]),
    ]);
  }

  /**
   * Method to validate the input from the api call
   *
   * @param mixed $value
   * @param array $allValues
   *
   * @return bool
   */
  public function validateValue($value, $allValues = []) {
    // todo check if group id is actually an active parent group
    return TRUE;
  }

  /**
   * Returns the type number from CRM_Utils_Type
   */
  public function getCrmType() {
    return \CRM_Utils_Type::T_INT;
  }

  public function getOptions($params) {
    $this->loadChildGroups();
    return $this->_childGroups;
  }

  /**
   * Method to load the active child mailing groups of the selected parent
   *
   * @return array|void
   * @throws \CiviCRM_API3_Exception
   */
  private function loadChildGroups() {
    if ($this->_groupsLoaded) {
      return;
    }
    $this->_childGroups = [];
    $parentGroupId = $this->configuration->get('parent_group_id');
    if (!$parentGroupId) {
      return;
    }
    try {
      $groupsParams = [
        'return' => ["title", "group_type"],
        'parents' => ['LIKE' => $parentGroupId],
        'is_active' => 1,
        'options' => ['limit' => 0],
      ];
      if ($this->configuration->get('visibility')) {
        $groupsParams['visibility'] = $this->configuration->get('visibility');
      }
      $childrenApi = civicrm_api3('Group', 'get', $groupsParams);
      foreach ($childrenApi['values'] as $childId => $child) {
        if (in_array(2, $child['group_type'])) {
          $this->_childGroups[$childId] = $child['title'];
        }
      }
    } catch (\CiviCRM_API3_Exception $ex) {
    }
    $this->_groupsLoaded = TRUE;
  }

  /**
   * Sets the default values of this action
   */
  public function setDefaults() {
    parent::setDefaults();
    $this->_groupsLoaded = FALSE;
    $this->_childGroups = [];
  }

}
