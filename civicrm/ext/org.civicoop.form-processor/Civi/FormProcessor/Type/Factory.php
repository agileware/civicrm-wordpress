<?php

/**
 * @author Jaap Jansma (CiviCooP) <jaap.jansma@civicoop.org>
 * @license http://www.gnu.org/licenses/agpl-3.0.html
 */

namespace Civi\FormProcessor\Type;

use CRM_Extension_System;
use CRM_FormProcessor_ExtensionUtil as E;

class Factory {

  /**
   * @var array<AbstractType>
   */
  protected array $types = [];

  /**
   * @var array
   */
  protected array $typeLabels = [];

  public function __construct() {
    $this->addType(new GenericType('Integer', E::ts('Numeric (no-decimal)')));
    $this->addType(new GenericType('Float', E::ts('Numeric (with-decimal)')));
    $this->addType(new FormattedDecimalType(E::ts('Formatted decimal number')));
    $this->addType(new ContactEntityType(E::ts('Contact Entity (Contact ID)')));
    $this->addType(new ShortTextType(E::ts('Short text')));
    $this->addType(new GenericType('Text', E::ts('Long text')));
    $this->addType(new DateType(E::ts('Date')));
    $this->addType(new TimeType(E::ts('Time')));
    $this->addType(new BooleanType('Boolean', E::ts('Yes/No')));
    $this->addType(new YesNoOptionListType('YesNoOptionList', E::ts('Yes/No as Option List')));
    $this->addType(new OptionGroupType('OptionGroup', E::ts('Option Group')));
    $this->addType(new ContactSubTypeType('ContactSubType', E::ts('Contact Sub Type')));
    $this->addType(new CustomOptionListType('CustomOptionListType', E::ts('Custom Options')));
    $this->addType(new CountryType('Country', E::ts('Country')));
    $this->addType(new CountryIsoCodeType('CountryIsoCode', E::ts('Country ISO Code')));
    $this->addType(new StateProvinceType('StateProvince', E::ts('State/Province')));
    $this->addType(new TagType('Tag', E::ts('Tags')));
    if (CRM_Extension_System::singleton()->getManager()->getStatus('civi_mail')==='installed') {
      $this->addType(new MailingGroupType('MailingGroup', E::ts('Mailing Group')));
      $this->addType(new ParentMailingGroupType('ParentMailingGroup', E::ts('Parent Mailing Group')));
    }
    if (CRM_Extension_System::singleton()->getManager()->getStatus('civi_event')==='installed') {
      $this->addType(new ParticipantStatusType('ParticipantStatusType', E::ts('Participant Status')));
      $this->addType(new EventPriceListType('EventPriceListType', E::ts('Event Price List')));
    }
    if(CRM_Extension_System::singleton()->getManager()->getStatus('civi_campaign')==='installed') {
      $this->addType(new CampaignType('CampaignType', E::ts('Campaign')));
      $this->addType(new MultiCampaignType('MultiCampaignType', E::ts('More campaign(s)')));
    }
    if (CRM_Extension_System::singleton()->getManager()->getStatus('civi_contribute')==='installed') {
      $this->addType(new FinancialTypeType('FinancialTypeType', E::ts('Financial Type')));
    }
    if (CRM_Extension_System::singleton()->getManager()->getStatus('civi_member')==='installed') {
      $this->addType(new MembershipTypeType('MembershipTypeType', E::ts('Membership Type')));
    }
    $this->addType(new FileType('file', E::ts('File')));
    $this->addType(new FileUrlType('file_url', E::ts('File Upload with URL')));
    $this->addType(new WeekDays('WeekDays', E::ts('Week Days')));
    $this->addType(new LanguageType('LanguageType', E::ts('Language')));
    if (CRM_Extension_System::singleton()->getManager()->getStatus('civi_case')==='installed') {
      $this->addType(new CaseTypeType('CaseTypeType', E::ts('Case Type')));
    }
    $this->addType(new EntityType(E::ts('Any CiviCRM Entity (ID)')));
    $this->addType(new PriceSetOptionsFieldType(E::ts('Price Set Options')));
  }

  /**
   * Add a type
   *
   * @param AbstractType $type
   * @return \Civi\FormProcessor\Type\Factory
   */
  public function addType(AbstractType $type): Factory {
    $this->types[$type->getName()] = $type;
    $this->typeLabels[$type->getName()] = $type->getLabel();
    return $this;
  }

  /**
   * @param $typeName
   * @param $typeClass
   * @param $typeLabel
   *
   * @return $this
   */
  public function addFormsProcessorType($typeName, $typeClass, $typeLabel): Factory{
    $type = new $typeClass($typeName, $typeLabel);
    $this->addType($type);
    return $this;
  }

  /**
   * Return the type
   *
   * @param string $name
   *
   * @return AbstractType|null
   */
  public function getTypeByName(string $name):? AbstractType {
    if (isset($this->types[$name])) {
      $type = clone $this->types[$name];
      $type->setDefaults();
      return $type;
    }
    return NULL;
  }

  /**
   * @return array<AbstractType>
   */
  public function getTypes(): array {
    return $this->types;
  }

  public function getTypeLabels(): array {
    return $this->typeLabels;
  }

}
