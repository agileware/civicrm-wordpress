<?php

/**
 * @author Jaap Jansma (CiviCooP) <jaap.jansma@civicoop.org>
 * @license http://www.gnu.org/licenses/agpl-3.0.html
 */

namespace Civi\FormProcessor\Type;

use \Civi\FormProcessor\Config\Specification;
use \Civi\FormProcessor\Config\SpecificationBag;

use \CRM_FormProcessor_ExtensionUtil as E;

class ContactSubTypeType extends AbstractType implements OptionListInterface {

  private $options = [];
  private $normalizedOptions = [];
  private $denormalizedOptions = [];
  private $optionsLoaded = FALSE;

  /**
   * Get the configuration specification
   *
   * @return \Civi\FormProcessor\Config\SpecificationBag
   */
  public function getConfigurationSpecification() {
    return new SpecificationBag([
      new Specification('multiple', 'Boolean', E::ts('Multiple'), FALSE, 0, NULL, [
        0 => E::ts('Single value'),
        1 => E::ts('Multiple values'),
      ]),
    ]);
  }

  /**
   * Returns true when this field is a multiple field.
   *
   * @return bool
   */
  public function isMultiple() {
    return $this->configuration->get('multiple') ? TRUE : FALSE;
  }

  public function validateValue($value, $allValues = []) {
    $multiple = $this->configuration->get('multiple') ? TRUE : FALSE;
    $options = $this->getOptions($allValues);

    // Correct array values when field is not multiple.
    // this could be caused for example by a drupal webform with
    // a checkbox field. The value is submitted as an array, altough it contains
    // only one value.
    if (!$multiple && is_array($value) && count($value) == 1) {
      $value = reset($value);
    }

    if ($multiple && is_array($value)) {
      foreach ($value as $valueItem) {
        if (\CRM_Utils_Type::validate($valueItem, 'String', FALSE) === NULL) {
          return FALSE;
        }
        if (!isset($options[$valueItem])) {
          return FALSE;
        }
      }
    }
    elseif (!is_array($value) && $value) {
      if (\CRM_Utils_Type::validate($value, 'String', FALSE) === NULL) {
        return FALSE;
      }

      if (!isset($options[$value])) {
        return FALSE;
      }
    }
    else {
      return FALSE;
    }

    return TRUE;
  }

  /**
   * Returns the type number from CRM_Utils_Type
   */
  public function getCrmType() {
    return \CRM_Utils_Type::T_STRING;
  }

  /**
   * Normalize the input value.
   *
   * @param $value
   *
   * @return mixed
   */
  public function normalizeValue($value) {
    $this->loadOptions();
    $multiple = $this->configuration->get('multiple') ? TRUE : FALSE;

    // Correct array values when field is not multiple.
    // this could be caused for example by a drupal webform with
    // a checkbox field. The value is submitted as an array, altough it contains
    // only one value.
    if (!$multiple && is_array($value) && count($value) == 1) {
      $value = reset($value);
    }

    if ($multiple && is_array($value)) {
      $return = [];
      foreach ($value as $item) {
        $return[] = $this->normalizedOptions[$item];
      }
      return $return;
    }
    elseif (!is_array($value)) {
      return $this->normalizedOptions[$value];
    }
    else {
      return NULL;
    }
  }

  /**
   * Denormalize the input value.
   *
   * @param $value
   *
   * @return mixed
   */
  public function denormalizeValue($value) {
    $this->loadOptions();
    $multiple = $this->configuration->get('multiple') ? TRUE : FALSE;
    if ($multiple && is_array($value)) {
      $return = [];
      foreach ($value as $item) {
        $return[] = $this->denormalizedOptions[$item];
      }
      return $return;
    }
    elseif (!is_array($value) && strlen($value)) {
      return $this->denormalizedOptions[$value];
    }
    else {
      return NULL;
    }
  }

  public function getOptions($params) {
    $this->loadOptions();
    return $this->options;
  }

  public function loadOptions() {
    if ($this->optionsLoaded) {
      return;
    }
    $contactTypesApi = civicrm_api3('ContactType', 'get', ['is_active' => 1, 'options' => ['limit' => 0]]);
    foreach ($contactTypesApi['values'] as $key => $contactType) {
      if (empty($contactType['parent_id'])) {
        continue;
      }
      $this->options[$contactType['name']] = $contactType['label'];
      $this->normalizedOptions[$contactType['name']] = $contactType['name'];
      $this->denormalizedOptions[$contactType['name']] = $contactType['name'];
    }
    $this->optionsLoaded = TRUE;
  }

}
