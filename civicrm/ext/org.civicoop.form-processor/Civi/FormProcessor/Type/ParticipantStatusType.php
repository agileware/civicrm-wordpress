<?php

/**
 * @author Jaap Jansma (CiviCooP) <jaap.jansma@civicoop.org>
 * @license http://www.gnu.org/licenses/agpl-3.0.html
 */

namespace Civi\FormProcessor\Type;

use \Civi\FormProcessor\Config\Specification;
use \Civi\FormProcessor\Config\SpecificationBag;
use \Civi\FormProcessor\Type\AbstractType;
use \Civi\FormProcessor\Type\OptionListInterface;

use \CRM_FormProcessor_ExtensionUtil as E;

class ParticipantStatusType extends AbstractType implements OptionListInterface {

  /**
   * Returns true when this field is a multiple field.
   *
   * @return bool
   */
  public function isMultiple() {
    return $this->configuration->get('multiple') == 1 ? TRUE : FALSE;
  }

  /**
   * Get the configuration specification
   *
   * @return SpecificationBag
   */
  public function getConfigurationSpecification() {
    return new SpecificationBag([
      new Specification('multiple', 'Boolean', E::ts('Multiple'), FALSE, 0, NULL, [
        0 => E::ts('Single value'),
        1 => E::ts('Multiple values'),
      ]),
    ]);
  }


  public function validateValue($value, $allValues = []) {
    $multiple = $this->configuration->get('multiple') ? TRUE : FALSE;
    $options = $this->getOptions($allValues);

    if ($multiple && is_array($value)) {
      foreach ($value as $valueItem) {
        if (\CRM_Utils_Type::validate($valueItem, 'Integer', FALSE) === NULL) {
          return FALSE;
        }
        if (!isset($options[$valueItem])) {
          return FALSE;
        }
      }
    }
    elseif (!is_array($value)) {
      if (\CRM_Utils_Type::validate($value, 'Integer', FALSE) === NULL) {
        return FALSE;
      }
      if (!isset($options[$value])) {
        return FALSE;
      }
    }
    else {
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Returns the type number from CRM_Utils_Type
   */
  public function getCrmType() {
    return \CRM_Utils_Type::T_INT;
  }

  public function getOptions($params) {
    static $return;
    if ($return) {
      return $return;
    }
    $groups = civicrm_api3('ParticipantStatusType', 'get', [
      'is_active' => 1,
      'options' => ['limit' => 0],
    ]);
    $return = [];
    foreach ($groups['values'] as $group) {
      $return[$group['id']] = $group['label'];
    }
    return $return;
  }

}
