<?php

/**
 * @author Erik Hommel (CiviCooP) <erik.hommel@civicoop.org>
 * @license http://www.gnu.org/licenses/agpl-3.0.html
 */

namespace Civi\FormProcessor\Type;

use Civi\FormProcessor\Config\ConfigurationBag;
use \Civi\FormProcessor\Config\Specification;
use \Civi\FormProcessor\Config\SpecificationBag;
use \Civi\FormProcessor\Type\AbstractType;
use \Civi\FormProcessor\Type\OptionListInterface;

use \CRM_FormProcessor_ExtensionUtil as E;

class MultiCampaignType extends AbstractType implements OptionListInterface {

  protected $options;

  /**
   * Get the configuration specification
   *
   * @return SpecificationBag
   */
  public function getConfigurationSpecification() {
    $campaigns = [];
    $apiCampaigns = $this->getCampaigns();
    foreach ($apiCampaigns as $campaignId => $campaignTitle) {
      $campaigns[$campaignId] = $campaignTitle;
    }
    return new SpecificationBag([
      new Specification('campaign_id', 'Integer', E::ts('Campaign(s)'), TRUE, NULL, NULL, $campaigns, TRUE),
    ]);
  }

  /**
   * Method to get the active campaigns with api3 or api4
   *
   * @return array
   */
  private function getCampaigns() {
    $result = [];
    if (function_exists('civicrm_api4')) {
      try {
        $campaigns = \Civi\Api4\Campaign::get()
          ->addSelect('id', 'title', 'campaign_type_id:label')
          ->addWhere('is_active', '=', TRUE)
          ->execute();
        foreach ($campaigns as $campaign) {
          $result[$campaign['id']] = $campaign['title'] . "(" . $campaign['campaign_type_id:label'] . ")";;
        }
      }
      catch (\API_Exception $ex) {
      }
    }
    else {
      try {
        $campaigns = civicrm_api3('Campaign', 'get', [
          'return' => ["id", "title", "campaign_type_id.label"],
          'is_active' => 1,
          'options' => ['limit' => 0],
        ]);
        foreach ($campaigns['values'] as $campaignId => $campaign) {
          $result[$campaignId] = $campaign['title'] . "(" . $campaign['campaign_type_id.label'] . ")";
        }
      }
      catch (\CiviCRM_API3_Exception $ex) {
      }
    }
    return $result;
  }


  public function validateValue($value, $allValues = []) {
    return TRUE;
  }

  /**
   * Returns the type number from CRM_Utils_Type
   */
  public function getCrmType() {
    return \CRM_Utils_Type::T_INT;
  }

  public function getOptions($params) {
    $this->loadOptions();
    $selectedCampaigns = $this->configuration->get('campaign_id');
    foreach ($this->options as $campaignId => $campaignTitle) {
      if (!in_array($campaignId, $selectedCampaigns)) {
        unset($this->options[$campaignId]);
      }
    }
    return $this->options;
  }

  protected function loadOptions() {
    if ($this->options != NULL) {
      return;
    }
    $campaigns = civicrm_api3('Campaign', 'get', [
      'is_active' => 1,
      'options' => ['limit' => 0],
    ]);
    $this->options = [];
    foreach ($campaigns['values'] as $campaign) {
      $this->options[$campaign['id']] = $campaign['title'];
    }
  }

  public function setConfiguration(ConfigurationBag $configuration) {
    parent::setConfiguration($configuration);
    $this->options = NULL;
    return $this;
  }

  /**
   * Sets the default values of this action
   */
  public function setDefaults() {
    parent::setDefaults();
    $this->options = NULL;
  }

  /**
   * Returns true when this field is a multiple field.
   *
   * @return bool
   */
  public function isMultiple() {
    return TRUE;
  }

}
