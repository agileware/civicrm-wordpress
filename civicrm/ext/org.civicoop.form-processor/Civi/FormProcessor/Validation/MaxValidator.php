<?php

/**
 * @author Jaap Jansma (CiviCooP) <jaap.jansma@civicoop.org>
 * @license http://www.gnu.org/licenses/agpl-3.0.html
 */

 namespace Civi\FormProcessor\Validation;

 use Civi\FormProcessor\Config\Specification;
 use Civi\FormProcessor\Config\SpecificationBag;

 use CRM_FormProcessor_ExtensionUtil as E;

 class MaxValidator extends AbstractValidator {

  public function getLabel() {
    return E::ts('Is less than or equal to');
  }

   /** Returns the invalid message.
    *
    * @return string
    */
   public function getInvalidMessage() {
     return E::ts('Value can not be greater than %1', [1=>$this->configuration->get('max_value')]);
   }

  /**
   * Returns the name of the validator.
   *
   * @return string
   */
  public function getName() {
    return 'max';
  }

  /**
   * Validate the input.
   *
   * @param mixed $input
   * @param \Civi\FormProcessor\Type\AbstractType $inputType
   * @return bool
   */
  public function validate($input, $inputType) {
    return $input <= $this->configuration->get('max_value') ? true : false;
  }

   /**
    * Get the configuration specification
    *
    * @return SpecificationBag
    */
   public function getConfigurationSpecification() {
     return new SpecificationBag(array(
       new Specification('max_value', 'Float', E::ts('Maximum value'), true)
     ));
   }


 }
