<?php

/**
 * @author Jaap Jansma (CiviCooP) <jaap.jansma@civicoop.org>
 * @license http://www.gnu.org/licenses/agpl-3.0.html
 */

 namespace Civi\FormProcessor\Validation;

 use Civi\FormProcessor\Config\Specification;
 use Civi\FormProcessor\Config\SpecificationBag;

 use CRM_FormProcessor_ExtensionUtil as E;

 class MinValidator extends AbstractValidator {

  public function getLabel() {
    return E::ts('Is greater than or equal to');
  }

  /**
   * Returns the name of the validator.
   *
   * @return string
   */
  public function getName() {
    return 'min';
  }

  /** Returns the invalid message.
   *
   * @return string
   */
   public function getInvalidMessage() {
     return E::ts('Value can not be less than %1', [1=>$this->configuration->get('min_value')]);
   }

  /**
   * Validate the input.
   *
   * @param mixed $input
   * @param \Civi\FormProcessor\Type\AbstractType $inputType
   * @return bool
   */
  public function validate($input, $inputType) {
    return $input >= $this->configuration->get('min_value') ? true : false;
  }

   /**
    * Get the configuration specification
    *
    * @return SpecificationBag
    */
   public function getConfigurationSpecification() {
     return new SpecificationBag(array(
       new Specification('min_value', 'Float', E::ts('Minimum value'), true)
     ));
   }


 }
