<?php

/**
 * @author Jaap Jansma (CiviCooP) <jaap.jansma@civicoop.org>
 * @license http://www.gnu.org/licenses/agpl-3.0.html
 */

 namespace Civi\FormProcessor\Validation;

 use \Civi\FormProcessor\Validation\AbstractValidator;

 use CRM_FormProcessor_ExtensionUtil as E;

 class EmailValidator extends AbstractValidator {

  const PATTERN_LOOSE = '/^.+\@\S+\.\S+$/';

  public function getLabel() {
    return E::ts('E-mail');
  }

  /**
   * Returns the name of the validator.
   *
   * @return string
   */
  public function getName() {
    return 'email';
  }

  /**
   * Validate the input.
   *
   * @param mixed $input
   * @param \Civi\FormProcessor\Type\AbstractType $inputType
   * @return bool
   */
  public function validate($input, $inputType) {
    $pattern = EmailValidator::PATTERN_LOOSE;
    return preg_match($pattern, $input);
  }

 }
