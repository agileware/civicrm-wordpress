<?php

/**
 * @author Jens Schuppe (SYSTOPIA) <schuppe@systopia.de>
 * @license http://www.gnu.org/licenses/agpl-3.0.html
 */

namespace Civi\FormProcessor\Validation;

use Civi\FormProcessor\Config\Specification;
use Civi\FormProcessor\Config\SpecificationBag;
use Civi\FormProcessor\Type\AbstractType;
use Civi\FormProcessor\Type\DateType;

use CRM_FormProcessor_ExtensionUtil as E;

class DateValidator extends AbstractValidator {

  public function getLabel() {
    return E::ts('Date validation');
  }

  /**
   * Returns the name of the validator.
   *
   * @return string
   */
  public function getName() {
    return 'date';
  }

  /**
   * Get the configuration specification
   *
   * @return SpecificationBag
   */
  public function getConfigurationSpecification() {
    return new SpecificationBag(
      [
        new Specification(
          'operator',
          'String',
          E::ts('Operator'),
          TRUE,
          'less than or equal',
          NULL,
          [
            'less than or equal' => E::ts('Less than or equal'),
            'less than' => E::ts('Less than'),
            'greater than or equal' => E::ts('Greater than or equal'),
            'greater than' => E::ts('Greater than'),
            'equal' => E::ts('Equal'),
            'not equal' => E::ts('Not equal'),
            'between' => E::ts('Between'),
            'between_left' => E::ts('Between (including lower value)'),
            'between_right' => E::ts('Between (including higher value)'),
            'between_both' => E::ts('Between (including both values)'),
          ]
        ),
        new Specification(
          'value',
          'String',
          E::ts('Value'),
          TRUE
        ),
        new Specification(
          'value2',
          'String',
          E::ts('Value 2'),
          FALSE
        ),
      ]
    );
  }

  public function validateConfiguration() {
    $valid = parent::validateConfiguration();

    // TODO: Validate value2 being there when operator is binary ("between").

    return $valid;
  }

  /**
   * Validate the input.
   *
   * @param mixed $input
   * @param \Civi\FormProcessor\Type\AbstractType $inputType
   * @return bool
   */
  public function validate($input, $inputType) {
    $operator = $this->configuration->get('operator');
    $comparison_date = date_create($this->configuration->get('value'));
    $comparison_date_2 = date_create($this->configuration->get('value2'));
    $parameter = $inputType->normalizeValue($input);
    if (!$date = date_create_from_format('YmdHis', $parameter)) {
      $date = date_create_from_format('Ymd', $parameter)
        ->setTime(0, 0);
      $comparison_date->setTime(0, 0);
      $comparison_date_2->setTime(0, 0);
    }
    switch ($operator) {
      case 'less than or equal':
        $result = ($date <= $comparison_date);
        break;
      case 'less than':
        $result = ($date < $comparison_date);
        break;
      case 'greater than or equal':
        $result = ($date >= $comparison_date);
        break;
      case 'greater than':
        $result = ($date > $comparison_date);
        break;
      case 'equal':
        $result = ($date == $comparison_date);
        break;
      case 'not equal':
        $result = ($date != $comparison_date);
        break;
      case 'between':
        $result = ($date > $comparison_date && $date < $comparison_date_2);
        break;
      case 'between_left':
        $result = ($date >= $comparison_date && $date < $comparison_date_2);
        break;
      case 'between_right':
        $result = ($date > $comparison_date && $date <= $comparison_date_2);
        break;
      case 'between_both':
        $result = ($date >= $comparison_date && $date <= $comparison_date_2);
        break;
      default:
        $result = TRUE;
    }
    return $result;
  }

}
