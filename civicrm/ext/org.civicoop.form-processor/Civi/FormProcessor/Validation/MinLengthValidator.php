<?php

/**
 * @author Jaap Jansma (CiviCooP) <jaap.jansma@civicoop.org>
 * @license http://www.gnu.org/licenses/agpl-3.0.html
 */

 namespace Civi\FormProcessor\Validation;

 use Civi\FormProcessor\Config\Specification;
 use Civi\FormProcessor\Config\SpecificationBag;

 use CRM_FormProcessor_ExtensionUtil as E;

 class MinLengthValidator extends AbstractValidator {

  public function getLabel() {
    return E::ts('Has a minimum length');
  }

  /**
   * Returns the name of the validator.
   *
   * @return string
   */
  public function getName() {
    return 'min_length';
  }

  /**
   * Validate the input.
   *
   * @param mixed $input
   * @param \Civi\FormProcessor\Type\AbstractType $inputType
   * @return bool
   */
  public function validate($input, $inputType) {
    return strlen($input) >= $this->configuration->get('min_length') ? true : false;
  }

   /**
    * Get the configuration specification
    *
    * @return SpecificationBag
    */
   public function getConfigurationSpecification() {
     return new SpecificationBag(array(
       new Specification('min_length', 'Integer', E::ts('Minimum length'), true)
     ));
   }


 }
