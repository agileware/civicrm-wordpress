<?php

/**
 * @author Jaap Jansma (CiviCooP) <jaap.jansma@civicoop.org>
 * @license http://www.gnu.org/licenses/agpl-3.0.html
 */

 namespace Civi\FormProcessor\Validation;

 use \Civi\FormProcessor\Config\ConfigurationBag;
 use \Civi\FormProcessor\Config\Specification;
 use \Civi\FormProcessor\Config\SpecificationBag;

 use CRM_FormProcessor_ExtensionUtil as E;

 abstract class AbstractValidator implements \JsonSerializable {

  /**
   * @var ConfigurationBag
   */
  protected $configuration;

  /**
   * @var ConfigurationBag
   */
  protected $defaultConfiguration;

  /**
   * Returns the name of the validator.
   *
   * @return string
   */
  abstract public function getName();

  /**
   * Returns the label of the validator.
   *
   * @return string
   */
  abstract public function getLabel();

  /**
   * Validate the input.
   *
   * @param mixed $input
   * @param \Civi\FormProcessor\Type\AbstractType $inputType
   * @return bool
   */
  abstract public function validate($input, $inputType);

  public function __construct() {
    $this->configuration = new ConfigurationBag();
    $this->defaultConfiguration = new ConfigurationBag();

    foreach($this->getConfigurationSpecification() as $spec) {
      if ($spec->getDefaultValue()) {
        $this->configuration->set($spec->getName(), $spec->getDefaultValue());
        $this->defaultConfiguration->set($spec->getName(), $spec->getDefaultValue());
      }
    }
  }

  /**
   * Get the configuration specification
   *
   * @return SpecificationBag
   */
  public function getConfigurationSpecification() {
    return new SpecificationBag();
  }

  /**
   * @return bool;
   */
  public function validateConfiguration() {
    if ($this->configuration === null) {
      return false;
    }
    return SpecificationBag::validate($this->configuration, $this->getConfigurationSpecification());
  }

  /**
   * @return ConfigurationBag
   */
  public function getConfiguration() {
    return $this->configuration;
  }

  /**
   * @return ConfigurationBag
   */
  public function getDefaultConfiguration() {
    return $this->defaultConfiguration;
  }


  /**
   * Returns the invalid message.
   *
   * @return string
   */
  public function getInvalidMessage() {
    return E::ts('Invalid %1', array(1=>$this->getLabel()));
  }

  /**
   * @param ConfigurationBag $configuration
   */
  public function setConfiguration(ConfigurationBag $configuration) {
    $this->configuration = $configuration;
    return $this;
  }

  public function toArray(): array {
    return array(
      'name' => $this->getName(),
      'label' => $this->getLabel(),
      'invalid_message' => $this->getInvalidMessage(),
      'configuration_spec' => $this->getConfigurationSpecification()->toArray(),
      'configuration' => $this->getConfiguration()->toArray(),
    );
  }

  public function jsonSerialize(): array {
    return $this->toArray();
  }

 }
