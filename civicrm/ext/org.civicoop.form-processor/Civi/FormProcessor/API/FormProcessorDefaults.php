<?php
/**
 * @author Jaap Jansma (CiviCooP) <jaap.jansma@civicoop.org>
 * @license http://www.gnu.org/licenses/agpl-3.0.html
 */

namespace Civi\FormProcessor\API;

use Civi\API\Event\ResolveEvent;
use Civi\API\Event\RespondEvent;
use Civi\API\Events;
use Civi\API\Provider\ProviderInterface as API_ProviderInterface;
use Civi\FormProcessor\DataBag;
use Civi\FormProcessor\Runner;
use Civi\FormProcessor\Utils\Cache;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * This class connects the enabled form processors to
 * the the api interface of CiviCRM. That way the form processors
 * could be executed over the api.
 */
class FormProcessorDefaults extends FormProcessor implements API_ProviderInterface, EventSubscriberInterface {

  public function __construct() {

  }

  public function onApiResolve(ResolveEvent $event) {
    $apiRequest = $event->getApiRequest();
    if (strtolower($apiRequest['entity']) == 'formprocessordefaults') {
      $event->setApiProvider($this);
      if (strtolower($apiRequest['action']) == 'getfields' || strtolower($apiRequest['action']) == 'getoptions') {
        $event->stopPropagation();
      }
    }
  }

  protected function getFields($formProcessorName, $apiParams, $fieldName = NULL, $useCache=true) {
    $fields = [];
    $hash = md5(json_encode($apiParams));
    $cacheKey = 'FormProcessorDefaults.getfields.'.strtolower($formProcessorName).'.'.$hash;
    if ($useCache && ($cache = Cache::get($cacheKey))) {
      return $cache;
    }

    try {
      $formProcessor = Runner::getFormProcessor($formProcessorName);
    } catch (\API_Exception $e) {
      return;
    }

    // Process all inputs of the formprocessor.
    foreach ($formProcessor['default_data_inputs'] as $input) {
      if ($fieldName && $fieldName != $input['name']) {
        continue;
      }
      $input['type']->setParameters(Runner::inputParameterMapping($input['default_data_parameter_mapping'], $apiParams));
      $field = array(
        'name' => $input['name'],
        'title' => !empty($input['title']) ? $input['title'] : $input['name'],
        'description' => '',
        'type' => $input['type']->getCrmType(),
        'api.required' => $input['is_required'],
        'api.aliases' => [],
        'entity' => 'FormProcessor',
      );
      if ($input['type'] instanceof OptionListInterface) {
        $field['options'] = $input['type']->getOptions($apiParams);
        $field['formprocessor.is_multiple'] = $input['type']->isMultiple(); // We add our own spec here, as an option for is multiple does not exists.
      }
      // Set a default value
      if (isset($input['default_value']) && $input['default_value'] != '') {
        $field['api.default'] = $input['type']->getDefaultValue($input['default_value']);
      }
      if ($input['type']->getCrmType()) {
        $fields[$input['name']] = $field;
      }
    }
    Cache::setForFormProcessor($formProcessorName, $cacheKey, $fields);
    return $fields;
  }

  /**
   * @param array $apiRequest
   *   The full description of the API request.
   *
   * @return array
   *   structured response data (per civicrm_api3_create_success)
   * @throws \Exception
   * @see civicrm_api3_create_success
   */
  public function invoke($apiRequest) {
    switch (strtolower($apiRequest['action'])) {
      case 'getfields':
        // Do get fields
        return $this->invokeGetFields($apiRequest);
        break;
      case 'getoptions':
        // Do get options
        return $this->invokeGetOptions($apiRequest);
        break;
      default:
        return $this->invokeFormProcessorDefaults($apiRequest);
        break;
    }
  }

  /**
   * @param array $apiRequest
   *   The full description of the API request.
   *
   * @return array
   *   structured response data (per civicrm_api3_create_success)
   * @throws \API_Exception
   * @see civicrm_api3_create_success
   */
  public function invokeFormProcessorDefaults($apiRequest) {
    $params = $apiRequest['params'];
    return Runner::runDefaults($apiRequest['action'], $params);
  }

  /**
   * @param int $version
   *   API version.
   *
   * @return array<string>
   */
  public function getEntityNames($version) {
    return array(
      'FormProcessorDefaults',
    );
  }

  /**
   * @param int $version
   *   API version.
   * @param string $entity
   *   API entity.
   *
   * @return array<string>
   */
  public function getActionNames($version, $entity) {
    if (strtolower($entity) != 'formprocessordefaults') {
      return array();
    }

    $cachekey = 'formprocessordefaults.getactions';
    if ($cache = Cache::get($cachekey)) {
      return $cache;
    }

    $params['is_active'] = 1;
    $params['enable_default_data'] = 1;
    $form_processors = \CRM_FormProcessor_BAO_FormProcessorInstance::getValues($params);
    $actions[] = 'getfields';
    $actions[] = 'getoptions';
    foreach ($form_processors as $form_processor) {
      $actions[] = $form_processor['name'];
    }
    Cache::set($cachekey, $actions);
    return $actions;
  }

}
