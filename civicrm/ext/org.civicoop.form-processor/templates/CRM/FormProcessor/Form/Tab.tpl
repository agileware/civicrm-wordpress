<div class="crm-block crm-content-block">
    <div id="mainTabContainer" class="ui-tabs ui-corner-all ui-widget ui-widget-content">
      <ul role="tablist" class="ui-tabs-nav ui-corner-all ui-helper-reset ui-helper-clearfix ui-widget-header">
        {foreach from=$tabHeader key=tabName item=tabValue}
          <li id="tab_{$tabName}" class="crm-tab-button ui-corner-all ui-tabs-tab ui-corner-top ui-state-default ui-tab {if $tabValue.current}ui-tabs-active ui-state-active{/if} {if !$tabValue.valid} disabled{/if} {$tabValue.class}" role="tab" {$tabValue.extra}>
            {if $tabValue.active}
              <a href="{if $tabValue.template}#panel_{$tabName}{else}{$tabValue.link|smarty:nodefaults}{/if}" title="{$tabValue.title|escape}{if !$tabValue.valid} ({ts}disabled{/ts}){/if}" class="ui-tabs-anchor" style="cursor: pointer;">
                {if $tabValue.icon}<i class="{$tabValue.icon}"></i>{/if}
                <span>{$tabValue.title}</span>
                {if is_numeric($tabValue.count)}<em>{$tabValue.count}</em>{/if}
              </a>
            {else}
              <span {if !$tabValue.valid} title="{ts}disabled{/ts}"{/if}>{$tabValue.title}</span>
            {/if}
          </li>
        {/foreach}
      </ul>
      {foreach from=$tabHeader key=tabName item=tabValue}
        {if $tabValue.template}
          <div id="panel_{$tabName}">
            {include file=$tabValue.template}
          </div>
        {/if}
      {/foreach}
    </div>
<div class="clear"></div>
</div>
