{crmScope extensionKey='form-processor'}
{if (!empty($inputType) && isset($configFields.$inputType))}
    {include file="CRM/FormProcessor/Form/Blocks/Configuration.tpl" prefix=$inputType title=$configurationSectionTitle}
{/if}
{if count($mappingElementNames.mapping)}
<div class="crm-accordion-wrapper">
  <div class="crm-accordion-header">{ts}On processing use parameters{/ts}</div>
  <div class="crm-accordion-body">
    {foreach from=$mappingElementNames.mapping item=mappingField}
      <div class="crm-section">
        <div class="label">{$form.$mappingField.label}</div>
        <div class="content">
            {$form.$mappingField.html}
            {if isset($mappingElementDescriptions.$mappingField)}<p class="description">{$mappingElementDescriptions.$mappingField}</p>{/if}
        </div>
        <div class="clear"></div>
      </div>
    {/foreach}
  </div>
</div>
{/if}
{if count($mappingElementNames.default_mapping)}
<div class="crm-accordion-wrapper">
  <div class="crm-accordion-header">{ts}On retrieval of defaults use parameters{/ts}</div>
  <div class="crm-accordion-body">
    {foreach from=$mappingElementNames.default_mapping item=mappingField}
      <div class="crm-section">
        <div class="label">{$form.$mappingField.label}</div>
        <div class="content">
            {$form.$mappingField.html}
            {if isset($mappingElementDescriptions.$mappingField)}<p class="description">{$mappingElementDescriptions.$mappingField}</p>{/if}
        </div>
        <div class="clear"></div>
      </div>
    {/foreach}
  </div>
</div>
{/if}
{/crmScope}
