{crmScope extensionKey='form-processor'}
    <table>
      <tr>
        <th>{ts}Title{/ts}</th>
        <th>{ts}System name{/ts}</th>
        <th>{ts}Type{/ts}</th>
        <th>{ts}Is required{/ts}</th>
        <th>{ts}Default value{/ts}</th>
        <th>{ts}Validation Rules{/ts}</th>
        <th></th>
        <th></th>
      </tr>
        {foreach from=$inputs item=input}
          <tr class="{cycle values="odd-row,even-row"}">
            <td>
                {$input.title}
            </td>
            <td>
              <span class="description">{$input.name}</span>
            </td>
            <td>
              {$input.type_label}
            </td>
            <td>
              {if $input.is_required}
                {ts}Yes{/ts}
              {else}
                  {ts}No{/ts}
              {/if}
            </td>
            <td>{$input.default_value}</td>
            <td>
              {foreach from=$input.validators item=validator}
                {$validator.validator->getLabel()}
                <a href="{crmURL p=$validator_base_url q="reset=1&action=update&form_processor_id=`$input.form_processor_id`&id=`$validator.id`&entity_id=`$input.id`"}">{ts}Edit{/ts}</a>
                <a href="{crmURL p=$validator_base_url q="reset=1&action=delete&form_processor_id=`$input.form_processor_id`&id=`$validator.id`&entity_id=`$input.id`"}">{ts}Remove{/ts}</a> <br />
              {/foreach}
              <a href="{crmURL p=$validator_base_url q="reset=1&action=add&form_processor_id=`$input.form_processor_id`&entity_id=`$input.id`"}">{ts}Add Validation rule{/ts}</a>
            </td>
            <td>{if ($input.weight && !is_numeric($input.weight))}{$input.weight}{/if}</td>
            <td>
              <a href="{crmURL p=$input_base_url q="reset=1&action=update&form_processor_id=`$input.form_processor_id`&id=`$input.id`"}">{ts}Edit{/ts}</a>
              <a href="{crmURL p=$input_base_url q="reset=1&action=delete&form_processor_id=`$input.form_processor_id`&id=`$input.id`"}">{ts}Remove{/ts}</a>
            </td>
          </tr>
        {/foreach}
    </table>
{/crmScope}
