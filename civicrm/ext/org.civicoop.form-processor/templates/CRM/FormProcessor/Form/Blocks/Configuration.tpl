{crmScope extensionKey='form-processor'}
{if (!empty($prefix) && isset($configFields.$prefix))}
  {assign var='configFields' value=$configFields.$prefix}
  {assign var='configDescriptions' value=$configDescriptions.$prefix}
  {assign var='configCollectionFields' value=$configCollectionFields.$prefix}
  {if count($configFields) || count($configCollectionFields)}
    <div class="crm-accordion-wrapper">
      <div class="crm-accordion-header">{$title}</div>
      <div class="crm-accordion-body">
        {foreach from=$configFields item=elementName}
          <div class="crm-section">
            <div class="label">{$form.$elementName.label}</div>
            <div class="content">
              {$form.$elementName.html}
              {if isset($configDescriptions.$elementName)}
                <br /><span class="description">{$configDescriptions.$elementName}</span>
              {/if}
            </div>
            <div class="clear"></div>
          </div>
        {/foreach}

        {foreach from=$configCollectionFields item=group}
          <div class="crm-section">
            {assign var='countName' value="`$group.name`Count"}
            <input type="hidden" name="{$countName}" value="{$group.count}" id="{$countName}" />
            {capture assign="configCollectionTemplate"}{strip}
              <tr>
                  {foreach from=$group.fields item=elementName name=collectionFieldLoop}
                    <td>
                        {$form.$elementName[0].html}
                        {if isset($configDescriptions.$elementName)}
                          <br /><span class="description">{$configDescriptions.$elementName}</span>
                        {/if}
                    </td>
                    {if $smarty.foreach.collectionFieldLoop.last}
                    <td>
                      <a href="#" onclick="return removeItem_{$group.name}(this);">
                        <i class="crm-i fa-trash" aria-hidden="true">&nbsp;</i>{ts}Remove{/ts}
                      </a>
                    </td>
                    {/if}
                  {/foreach}
              </tr>
            {/strip}{/capture}

            <table id="configCollection{$group.name}">
              <tr>
                {foreach from=$group.fields item=elementName}
                  <th>{$form.$elementName[0].label}
                      {if $group.is_required.$elementName}<span class="crm-marker">*</span>{/if}
                  </th>
                {/foreach}
                <th>&nbsp;</th>
              </tr>
              {foreach from=$group.elements item=elementIndex}
                <tr class="{cycle values="odd-row,even-row"}">
                    {foreach from=$group.fields item=elementName name=collectionFieldLoop}
                      <td>
                          {$form.$elementName[$elementIndex].html}
                          {if isset($configDescriptions.$elementName)}
                            <br /><span class="description">{$configDescriptions.$elementName}</span>
                          {/if}
                      </td>
                      {if $smarty.foreach.collectionFieldLoop.last}
                      <td>
                        <a href="#" onclick="return removeItem_{$group.name}(this);">
                          <i class="crm-i fa-trash" aria-hidden="true">&nbsp;</i>{ts}Remove{/ts}
                        </a>
                      </td>
                      {/if}
                    {/foreach}
                </tr>
              {/foreach}
            </table>
            <div class="crm-submit-buttons">
              <script type="text/javascript">
                {literal}
                  function removeItem_{/literal}{$group.name}{literal}(element) {
                    CRM.$(element).parent().parent().remove();
                    var currentRowCount = CRM.$('#configCollection{/literal}{$group.name}{literal} tr').length -1;
                    CRM.$('#{/literal}{$group.name}{literal}Count').val(currentRowCount);
                    return false;
                  }
                  function addItem_{/literal}{$group.name}{literal}() {
                    var template = CRM.$('{/literal}{$configCollectionTemplate|escape:javascript}{literal}');
                    var min = {/literal}{if $group.max}{$group.min}{else}false{/if}{literal};
                    var max = {/literal}{if $group.max}{$group.max}{else}false{/if}{literal};
                    var currentRowCount = CRM.$('#configCollection{/literal}{$group.name}{literal} tr').length -1;
                    if (currentRowCount < 0) {
                      currentRowCount = 0;
                    }
                    var newRowCount = currentRowCount + 1;
                    template.addClass('row-'+newRowCount);
                    if (!max || currentRowCount < max) {
                      CRM.$('#configCollection{/literal}{$group.name}{literal}').append(template);
                      CRM.$('#configCollection{/literal}{$group.name}{literal} tr.row-'+newRowCount+' input').each(function (index, element) {
                        CRM.$(element).attr('name', CRM.$(element).data('single-name') + '[' + newRowCount + ']');
                      });
                      CRM.$('#configCollection{/literal}{$group.name}{literal} tr.row-'+newRowCount+' select.crm-select2').each(function (index, element) {
                        CRM.$(element).attr('name', CRM.$(element).data('single-name') + '[' + newRowCount + ']');
                        CRM.$(element).crmSelect2();
                      });
                      CRM.$('#{/literal}{$group.name}{literal}Count').val(newRowCount);
                    }
                  }
                {/literal}
              </script>
              <button class="crm-button" onclick="addItem_{$group.name}(); return false;"><i class="crm-i fa-plus" aria-hidden="true">&nbsp;</i>{ts}Add item{/ts}</button></div>
          </div>
        {/foreach}
      </div>
    </div>
  {/if}
{/if}
{/crmScope}
