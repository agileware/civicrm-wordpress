{crmScope extensionKey='form-processor'}
{if !empty($configurationElementNames)}
  <div class="crm-accordion-wrapper">
    <div class="crm-accordion-header">
        {ts}Configuration{/ts}
    </div>
    <div class="crm-accordion-body">
        {foreach from=$configurationElementNames item=prefixedElements key=prefix}
            {foreach from=$prefixedElements item=elementName}
              <div class="crm-section">
                <div class="label">{$form.$elementName.label}</div>
                <div class="content">
                    {if isset($configurationElementPreHtml.$elementName)}
                        {$configurationElementPreHtml.$elementName}
                    {/if}
                    {$form.$elementName.html}
                    {if isset($configurationElementDescriptions.$elementName)}
                      <br /><span class="description">{$configurationElementDescriptions.$elementName}</span>
                    {/if}
                    {if isset($configurationElementPostHtml.$elementName)}
                        {$configurationElementPostHtml.$elementName}
                    {/if}
                </div>
                <div class="clear"></div>
              </div>
            {/foreach}
        {/foreach}
    </div>
  </div>
{/if}
{/crmScope}
