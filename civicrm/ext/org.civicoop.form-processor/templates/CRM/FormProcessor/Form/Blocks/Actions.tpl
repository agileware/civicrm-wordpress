{crmScope extensionKey='form-processor'}
    <table>
      <tr>
        <th>{ts}Title{/ts}</th>
        <th>{ts}System name{/ts}</th>
        <th>{ts}Type{/ts}</th>
        <th>{ts}Condition{/ts}</th>
        <th>{ts}Delay{/ts}</th>
        <th></th>
        <th></th>
      </tr>
        {foreach from=$actions item=action}
            {assign var="action_type" value=$action.type}
          <tr class="{cycle values="odd-row,even-row"}">
            <td>
                {$action.title}
            </td>
            <td>
              <span class="description">{$action.name}</span>
            </td>
            <td>{$action_types.$action_type}</td>
            <td>
              {if !empty($action.condition_configuration.name)}
                  {assign var="condition_type" value=$action.condition_configuration.name}
                  {if !empty($condition_types.$condition_type)}
                      {$condition_types.$condition_type->getTitle()}
                  {else}
                      {$condition_type}
                  {/if}
                <a href="{crmURL p=$condition_base_url q="reset=1&action=update&form_processor_id=`$action.form_processor_id`&id=`$action.id`"}">{ts}Change Condition{/ts}</a>
              {else}
                <a href="{crmURL p=$condition_base_url q="reset=1&action=update&form_processor_id=`$action.form_processor_id`&id=`$action.id`"}">{ts}Add Condition{/ts}</a>
              {/if}
            </td>
            <td>
                {if !empty($action.delay)}
                  <a href="{crmURL p=$delay_base_url q="reset=1&action=update&form_processor_id=`$action.form_processor_id`&id=`$action.id`"}">{ts}Change Delay{/ts}</a>
                {else}
                  <a href="{crmURL p=$delay_base_url q="reset=1&action=update&form_processor_id=`$action.form_processor_id`&id=`$action.id`"}">{ts}Add Delay{/ts}</a>
                {/if}
            </td>
            <td>{if ($action.weight && !is_numeric($action.weight))}{$action.weight}{/if}</td>
            <td>
              <a href="{crmURL p=$action_base_url q="reset=1&action=update&form_processor_id=`$action.form_processor_id`&id=`$action.id`"}">{ts}Edit{/ts}</a>
              <a href="{crmURL p=$action_base_url q="reset=1&action=delete&form_processor_id=`$action.form_processor_id`&id=`$action.id`"}">{ts}Remove{/ts}</a>
            </td>
          </tr>
        {/foreach}
    </table>
{/crmScope}
