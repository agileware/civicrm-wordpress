{crmScope extensionKey='form-processor'}
  {* block for rule data *}
  <h3>{ts}Import form processor{/ts}</h3>
  <div class="crm-block crm-form-block crm-form-processor_import-block">	{$form.uploadFile.html}</div>

  <div class="crm-submit-buttons">
    {include file="CRM/common/formButtons.tpl" location="bottom"}
  </div>
{/crmScope}
