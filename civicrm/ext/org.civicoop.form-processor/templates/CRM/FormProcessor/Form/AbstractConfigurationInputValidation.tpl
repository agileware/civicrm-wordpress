{crmScope extensionKey='form-processor'}

{if $action eq 8}
    {* Are you sure to delete form *}
  <div class="crm-block crm-form-block crm-form-processor-input-block">
    <div class="crm-section">{ts 1=$validationObject.validator.label}Are you sure to delete input validation rule '%1'?{/ts}</div>
  </div>

  <div class="crm-submit-buttons">
      {include file="CRM/common/formButtons.tpl" location="bottom"}
  </div>
{elseif (!$snippet)}
  <div class="crm-block crm-form-block crm-form-processor_title-block">
    <div class="crm-section">
      <div class="label">{$form.type.label}</div>
      <div class="content">{$form.type.html}</div>
      <div class="clear"></div>
    </div>
    <div id="type_configuration">
        {include file="CRM/FormProcessor/Form/Blocks/InputValidationConfiguration.tpl"}
    </div>
  </div>

  <div class="crm-submit-buttons">
      {include file="CRM/common/formButtons.tpl" location="bottom"}
  </div>

  <script type="text/javascript">
      {literal}
      CRM.$(function($) {
        var form_processor_id = {/literal}{$form_processor_id}{literal};
        var entity_id = {/literal}{$entity_id}{literal};
        var id = {/literal}{if (!empty($validationObject))}{$validationObject.id}{else}false{/if}{literal};

        $('#type').on('change', function() {
          var type = $('#type').val();
          if (type) {
            var dataUrl = CRM.url('{/literal}{$base_url}{literal}', {type: type, 'form_processor_id': form_processor_id, 'entity_id': entity_id, 'id': id});
            CRM.loadPage(dataUrl, {'target': '#type_configuration'});
          }
        });

        $('#type').change();
      });
      {/literal}
  </script>
{else}
  <div id="type_configuration">{include file="CRM/FormProcessor/Form/Blocks/InputValidationConfiguration.tpl"}</div>
{/if}
{/crmScope}
