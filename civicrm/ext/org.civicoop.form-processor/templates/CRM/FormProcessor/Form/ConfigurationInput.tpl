{crmScope extensionKey='form-processor'}
{if $action eq 8}
  <h3>{ts}Delete Input{/ts}</h3>
{elseif (!$snippet)}
  {if $action eq 1}
    <h3>{ts}Add Input{/ts}</h3>
  {else}
    <h3>{ts}Edit Input{/ts}</h3>
  {/if}
{/if}
{include file="CRM/FormProcessor/Form/AbstractConfigurationInput.tpl"}
{/crmScope}
