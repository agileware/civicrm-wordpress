{crmScope extensionKey='form-processor'}
{if $action eq 8}
  <h3>{ts}Delete Retrieval Criteria Validation Rule{/ts}</h3>
{elseif (!$snippet)}
  {if $action eq 1}
    <h3>{ts}Add Retrieval Criteria Validation Rule{/ts}</h3>
  {else}
    <h3>{ts}Edit Retrieval Criteria Validation Rule{/ts}</h3>
  {/if}
{/if}
{include file="CRM/FormProcessor/Form/AbstractConfigurationInputValidation.tpl"}
{/crmScope}
