{crmScope extensionKey='form-processor'}

{if $action eq 8}
    {* Are you sure to delete form *}
  <div class="crm-block crm-form-block crm-form-processor-input-block">
    <div class="crm-section">{ts 1=$inputObject.title}Are you sure to delete input '%1'?{/ts}</div>
  </div>

  <div class="crm-submit-buttons">
      {include file="CRM/common/formButtons.tpl" location="bottom"}
  </div>
{elseif (!$snippet)}
  <div class="crm-block crm-form-block crm-form-processor_title-block">
    <div class="crm-section">
      <div class="label">{$form.type.label}</div>
      <div class="content">{$form.type.html}</div>
      <div class="clear"></div>
    </div>
    <div class="crm-section">
      <div class="label">{$form.title.label}</div>
      <div class="content">
          {$form.title.html}
        <span class="">
        {ts}System name:{/ts}&nbsp;
        <span id="systemName" style="font-style: italic;">{if (!empty($inputObject))}{$inputObject.name}{/if}</span>
        <a href="javascript:void(0);" onclick="CRM.$('#nameSection').removeClass('hiddenElement'); CRM.$(this).parent().addClass('hiddenElement'); return false;">
          {ts}Change{/ts}
        </a>
        </span>
      </div>
      <div class="clear">
      </div>
    </div>
    <div id="nameSection" class="crm-section hiddenElement">
      <div class="label">{$form.name.label}</div>
      <div class="content">{$form.name.html}</div>
      <div class="clear"></div>
    </div>
    <div class="crm-section">
      <div class="label">{$form.is_required.label}</div>
      <div class="content">{$form.is_required.html}</div>
      <div class="clear"></div>
    </div>
    <div class="crm-section">
      <div class="label">{$form.include_in_formatted_params.label}</div>
      <div class="content">{$form.include_in_formatted_params.html}
        <p class="description">{ts}When you use the formatted parameters for example to send an e-mail with the submitted data in nice HTML format. You can exclude this field by unchecking this box.{/ts}</p>
      </div>
      <div class="clear"></div>
    </div>
    <div class="crm-section">
      <div class="label">{$form.default_value.label}</div>
      <div class="content">{$form.default_value.html}</div>
      <div class="clear"></div>
    </div>

    <div id="type_configuration">
      {include file="CRM/FormProcessor/Form/Blocks/InputConfiguration.tpl"}
    </div>
  </div>

  <div class="crm-submit-buttons">
      {include file="CRM/common/formButtons.tpl" location="bottom"}
  </div>

  <script type="text/javascript">
      {literal}
      CRM.$(function($) {
        var form_processor_id = {/literal}{$form_processor_id}{literal};
        var id = {/literal}{if (!empty($inputObject))}{$inputObject.id}{else}false{/if}{literal};

        $('#type').on('change', function() {
          var type = $('#type').val();
          if (type) {
            var dataUrl = CRM.url('{/literal}{$base_url}{literal}', {type: type, 'form_processor_id': form_processor_id, 'id': id});
            CRM.loadPage(dataUrl, {'target': '#type_configuration'});
          }
        });

        $('#title').on('blur', function() {
          var title = $('#title').val();
          if ($('#nameSection').hasClass('hiddenElement') && !id) {
            CRM.api3('{/literal}{$api3_entity_name}{literal}', 'check_name', {
              'title': title,
              'form_processor_id': form_processor_id
            }).done(function (result) {
              $('#systemName').html(result.name);
              $('#name').val(result.name);
            });
          }
        });

        $('#type').change();
      });
      {/literal}
  </script>
{else}
  <div id="type_configuration">{include file="CRM/FormProcessor/Form/Blocks/InputConfiguration.tpl"}</div>
{/if}
{/crmScope}
