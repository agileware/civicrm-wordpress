{crmScope extensionKey='form-processor'}

{if (!$snippet)}
  <div class="crm-block crm-form-block crm-form-processor_delay_type-block">
    <div class="crm-section">
      <div class="label">{$form.type.label}</div>
      <div class="content">{$form.type.html}</div>
      <div class="clear"></div>
    </div>
    <div id="type_configuration">
      {include file="CRM/FormProcessor/Form/Blocks/DelayConfiguration.tpl"}
    </div>
  </div>

  <div class="crm-submit-buttons">
      {include file="CRM/common/formButtons.tpl" location="bottom"}
  </div>

  <script type="text/javascript">
      {literal}
      CRM.$(function($) {
        var form_processor_id = {/literal}{$form_processor_id}{literal};
        var id = {/literal}{if (!empty($actionObject))}{$actionObject.id}{else}false{/if}{literal};

        $('#type').on('change', function() {
          var type = $('#type').val();
          if (type) {
            var dataUrl = CRM.url('{/literal}{$base_url}{literal}', {type: type, 'form_processor_id': form_processor_id, 'id': id});
            CRM.loadPage(dataUrl, {'target': '#type_configuration'});
            $('#type_configuration').removeClass('hiddenElement');
          } else {
            $('#type_configuration').addClass('hiddenElement');
          }
        });
        $('#type').change();
      });
      {/literal}
  </script>
{else}
  <div id="type_configuration">{include file="CRM/FormProcessor/Form/Blocks/DelayConfiguration.tpl"}</div>
{/if}
{/crmScope}
