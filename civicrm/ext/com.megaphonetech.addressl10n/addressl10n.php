<?php

require_once 'addressl10n.civix.php';
use CRM_Addressl10n_ExtensionUtil as E;

/**
 * Implements hook_civicrm_config().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_config
 */
function addressl10n_civicrm_config(&$config) {
  _addressl10n_civix_civicrm_config($config);
}

/**
 * Implements hook_civicrm_install().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_install
 */
function addressl10n_civicrm_install() {
  _addressl10n_civix_civicrm_install();
}

/**
 * Implements hook_civicrm_enable().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_enable
 */
function addressl10n_civicrm_enable() {
  _addressl10n_civix_civicrm_enable();
}

/**
 * Implements hook_civicrm_navigationMenu().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_navigationMenu
 *
 */
function addressl10n_civicrm_navigationMenu(&$menu) {
  _addressl10n_civix_insert_navigation_menu($menu, 'Administer/Localization', [
    'label' => ts('Country - Address Format', ['domain' => 'com.megaphonetech.addressl10n']),
    'name' => 'country_address_format',
    'url' => CRM_Utils_System::url('civicrm/country/addressformat', 'reset=1', TRUE),
    'active' => 1,
    'operator' => NULL,
    'permission' => 'administer CiviCRM',
  ]);
  _addressl10n_civix_navigationMenu($menu);
}

/**
 * Implements hook_civicrm_apiWrappers().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_apiWrappers
 *
 */
function addressl10n_civicrm_apiWrappers(&$wrappers, $apiRequest) {
  if (strtolower($apiRequest['entity']) == 'addressformat' && $apiRequest['action'] == 'create') {
    $wrappers[] = new CRM_AddressFormat_APIWrapper();
  }
}
