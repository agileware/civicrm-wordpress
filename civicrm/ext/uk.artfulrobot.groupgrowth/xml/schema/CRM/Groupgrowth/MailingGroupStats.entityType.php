<?php
// This file declares a new entity type. For more details, see "hook_civicrm_entityTypes" at:
// https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_entityTypes
return [
  [
    'name' => 'MailingGroupStats',
    'class' => 'CRM_Groupgrowth_DAO_MailingGroupStats',
    'table' => 'civicrm_mailing_group_stats',
  ],
];
