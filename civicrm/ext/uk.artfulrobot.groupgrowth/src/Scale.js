export default class Scale {
  constructor(rangeMax, rangeMin) {
    this.minDomain = 0;
    this.maxDomain = 1;
    this.tick = 0;
    this.rangeMax = rangeMax || 0;
    this.rangeMin = rangeMin || 0;
    console.log("Scale constructed", this, rangeMax);
  }
  setDomain(highest, lowest) {
    this.maxDomain = highest;
    this.minDomain = lowest || 0;
    return this;
  }
  setDomainToContain(highest, lowest) {
    // 123 » 10
    let h = this.nextRoundNumber(highest), l = this.nextRoundNumber(lowest || 0);
    this.maxDomain = h;
    this.minDomain = l;
    return this;
  }
  getMajorTick(x) {
    // Round 
    return Math.pow(10, x.toString().length - 1);
  }
  nextRoundNumber(x) {
    this.tick = Math.pow(10, x.toString().length - 2);
      // 123/10 = 12.3; 12.3 + 5 = 17; *10 = 170
    let nextRoundNumber = Math.ceil(x / this.tick + 0.5) * this.tick;
    return nextRoundNumber;
  }
  getTicks(n) {
    
  }
  toRange(input) {
    let output = input / (this.maxDomain - this.minDomain) * (this.rangeMax - this.rangeMin);
    // console.log("toRange", { scale: this , input, output});
    return output;
  }
  toRangeInv(x) {
    return x / (this.maxDomain - this.minDomain) * (this.rangeMin - this.rangeMax) + this.rangeMax;
  }
}
