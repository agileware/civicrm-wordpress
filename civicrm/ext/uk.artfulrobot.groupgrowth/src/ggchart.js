import GroupGrowthChart from './GroupGrowthChart.svelte';

// import dailyData from './../groupgrowth.json';

window.bootGG = function(dailyData, containerNode) {
  const app = new GroupGrowthChart({
    target: containerNode,
    props: {dailyData, containerNode}
  });
}
