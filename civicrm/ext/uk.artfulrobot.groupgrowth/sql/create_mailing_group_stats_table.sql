-- +--------------------------------------------------------------------+
-- | Copyright CiviCRM LLC. All rights reserved.                        |
-- |                                                                    |
-- | This work is published under the GNU AGPLv3 license with some      |
-- | permitted exceptions and without any warranty. For full license    |
-- | and copyright information, see https://civicrm.org/licensing       |
-- +--------------------------------------------------------------------+
--
-- /*******************************************************
-- *
-- * civicrm_mailing_group_stats
-- *
-- * Holds daily stats about email-able group contacts
-- *
-- *******************************************************/
CREATE TABLE IF NOT EXISTS `civicrm_mailing_group_stats` (
  `id` int unsigned NOT NULL AUTO_INCREMENT COMMENT 'Unique MailingGroupStats ID',
  `group_id` int unsigned COMMENT 'FK to Group',
  `day_ended` datetime NOT NULL,
  `members_count` int unsigned NOT NULL,
  `mailable_count` int unsigned NOT NULL,
  `opt_out_count` int unsigned NOT NULL,
  `removals_count` int unsigned NOT NULL,
  `additions_count` int unsigned NOT NULL,
  `on_hold_count` int unsigned NOT NULL,
  `held_count` int unsigned NOT NULL COMMENT 'The number of contacts put on hold this day.',
  `released_hold_count` int unsigned NOT NULL COMMENT 'The number of contacts released from hold this day.',
  `missing_email_count` int unsigned NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT FK_civicrm_mailing_group_stats_group_id FOREIGN KEY (`group_id`) REFERENCES `civicrm_group`(`id`) ON DELETE CASCADE
)
ENGINE=InnoDB;
