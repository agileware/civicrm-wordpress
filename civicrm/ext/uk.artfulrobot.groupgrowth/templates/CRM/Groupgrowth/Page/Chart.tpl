{if $groups}
  <p>{ts}Please select a group{/ts}<p>
  <ul>
  {foreach $groups as $x => $group}
    <li><a href="?gid={$group.id}">{$group.title}</a></li>
  {/foreach}
  </ul>

{else}
<script src="{$groupgrowthJS}"></script>
<style>
{literal}
{/literal}
</style>
<div id="chart" ></div>
<script type="text/javascript">
{literal}
document.addEventListener('DOMContentLoaded', function() {
  var dailyData = {/literal}{$dailyData}{literal} || [];
  // Let layout settle...
  setTimeout(() => {
    bootGG(dailyData, document.getElementById('chart'));
  }, 1);
});
{/literal}

</script>
{/if}
