<?php
use CRM_Groupgrowth_ExtensionUtil as E;

class CRM_Groupgrowth_Page_Chart extends CRM_Core_Page {

  public function run() {
    $groupId = $_GET['gid'] ?? 0;
    if ($groupId) {
      $group = \Civi\Api4\Group::get()
        ->addWhere('id', '=', $groupId)
        ->execute()
        ->first();

      if (!$group) {
        throw new API_Exception("Group $groupId does not exist.");
      }
      $this->assign('group', $group);
      CRM_Utils_System::setTitle(E::ts($group['title']));

      $result = \Civi\Api4\MailingGroupStats::get(FALSE)
        ->addSelect('day_ended', 'members_count', 'mailable_count', 'opt_out_count', 'removals_count', 'additions_count', 'on_hold_count', 'held_count', 'released_hold_count', 'missing_email_count')
        ->addWhere('group_id', '=', $groupId)
        ->addOrderBy('day_ended')
        ->execute();
      $this->assign('dailyData', json_encode($result->getArrayCopy()));
      $this->assign('groupgrowthJS', E::url('dist/ggchart.js'));
    }
    else {
      CRM_Utils_System::setTitle(E::ts('Chart Group Growth'));
      $groups = \Civi\Api4\Group::get()
        ->addSelect('id', 'title')
        ->addWhere('saved_search_id', 'IS NULL')
        ->addOrderBy('title', 'ASC')
        ->addWhere('is_active', '=', TRUE)
        ->execute();
      $this->assign('groups', $groups);
    }
    parent::run();
  }

}
