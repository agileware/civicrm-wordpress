<?php
use CRM_Groupgrowth_ExtensionUtil as E;

/**
 * Groupgrowth.Get API specification (optional)
 * This is used for documentation and validation.
 *
 * @param array $spec description of fields supported by this API call
 *
 * @see https://docs.civicrm.org/dev/en/latest/framework/api-architecture/
 */
function _civicrm_api3_groupgrowth_Get_spec(&$spec) {
  $spec['group_id'] = [
    'api.required' => 1,
    'description' => 'ID of group to generate stats for',
  ];
  // Default to 6 months ago.
  $spec['since'] = [
    'description' => 'Date to show stats since',
    'api.default' => date('Y-m-01', strtotime('today - 6 months')),
  ];
  $spec['period'] = [
    'description' => 'Period for grouping.',
    'options' => [
      'day',
      'week',
      'month',
    ]
  ];
}

/**
 * Groupgrowth.Get API
 *
 * @param array $params
 *
 * @return array
 *   API result descriptor
 *
 * @see civicrm_api3_create_success
 *
 * @throws API_Exception
 */
function civicrm_api3_groupgrowth_Get($params) {

  $groupId = (int) $params['group_id'];

  switch ($params['period'] ?? 'month') {
  case 'day':
    $sqlDateFormat = '%Y-%m-%d';
    $humanFormat = '%d %b %Y';
    break;

  case 'week':
    $sqlDateFormat = '%X-%V';
    $humanFormat = '%d %b %Y';
    break;

  case 'month':
    $sqlDateFormat = '%Y-%m';
    $humanFormat = '%b %Y';
    break;

  default:
    throw new API_Exception("Invalid period. Expected 'day', 'week' or 'month'");
  }

  $andSince = '';
  if (!empty($params['since'])) {
    $_ = strtotime($params['since']);
    if ($_ === FALSE) {
      throw new API_Exception("Invalid since date.");
    }
    $andSince = "AND `date` >= " . date('Ymd', $_);
  }

  // Get stats
  $sql = "
    SELECT
      MIN(DATE_FORMAT(`date`, '$humanFormat')) period,
      SUM(status = 'Added') gain,
      SUM(status = 'Removed') loss
    FROM civicrm_subscription_history
    WHERE group_id = %1 $andSince
    GROUP BY DATE_FORMAT(`date`, '$sqlDateFormat')
    ORDER BY DATE_FORMAT(`date`, '$sqlDateFormat');
    ";
  $sqlParams = [1 => [$groupId, 'Integer']];
  $stats = CRM_Core_DAO::executeQuery($sql, $sqlParams)->fetchAll('period');

  // Get current stats.
  $count = CRM_Core_DAO::executeQuery('SELECT COUNT(*) FROM civicrm_group_contact
    WHERE group_id = %1 AND status = "Added";', $sqlParams)->fetchValue();

  $today = date('Y-m-d');
  $runningSum = $count;

  for ($i = count($stats) - 1;$i>-1;$i--) {
    $row = & $stats[$i];
    $runningSum -=  $row['gain'] - $row['loss'];
    $row['start'] = $runningSum;
    unset($row);
  }

  return civicrm_api3_create_success($stats, $params, 'Groupgrowth', 'Get');
}
