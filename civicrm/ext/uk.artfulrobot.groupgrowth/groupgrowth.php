<?php

require_once 'groupgrowth.civix.php';
use CRM_Groupgrowth_ExtensionUtil as E;

/**
 * Implements hook_civicrm_config().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_config/ 
 */
function groupgrowth_civicrm_config(&$config) {
  _groupgrowth_civix_civicrm_config($config);
}

/**
 * Implements hook_civicrm_install().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_install
 */
function groupgrowth_civicrm_install() {
  _groupgrowth_civix_civicrm_install();
}

/**
 * Implements hook_civicrm_enable().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_enable
 */
function groupgrowth_civicrm_enable() {
  _groupgrowth_civix_civicrm_enable();
}

// --- Functions below this ship commented out. Uncomment as required. ---

/**
 * Implements hook_civicrm_preProcess().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_preProcess
 *

 // */

/**
 * Implements hook_civicrm_navigationMenu().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_navigationMenu
 */
function groupgrowth_civicrm_navigationMenu(&$menu) {
  _groupgrowth_civix_insert_navigation_menu($menu, 'Contacts', array(
    'label' => E::ts('Group Growth'),
    'name' => 'groupgrowth',
    'url' => 'civicrm/groupgrowth',
    'permission' => 'access CiviCRM',
    'operator' => 'OR',
    'separator' => 0,
  ));
  _groupgrowth_civix_navigationMenu($menu);
}
