# Event Redirect

![Screenshot](/images/screenshot.png)

Allows event admins to redirect an Event Info page to an external URL.

This can be useful when generating event calendars based on CiviCRM Events,
but you also want to be able to include events hosted elsewhere. For example,
this works well with [eventcalendar](https://github.com/osseed/com.osseed.eventcalendar).

This extensions adds the following configurations:

* An Event Type called "External Redirect"
* A custom field that is displayed only on that event type.

When the field has a value in it, visitors will automatically be redirected to that URL.

The extension is licensed under [AGPL-3.0](LICENSE.txt).

## Requirements

* PHP v7.2+
* CiviCRM 5.x

## Installation

Install as a regular CiviCRM extension.

## Usage

Create events with the "External Event" type, and enter an URL.
