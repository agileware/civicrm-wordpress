<?php
namespace Civi\GoCardless;

use GoCardlessPro\Resources\Payment;
use CRM_Core_Payment_GoCardless;
use Civi\Api4\Contribution;

/**
 * A GoCardless Payment possibly maps to a CiviCRM Contribution
 * by Payment.id === Contribution.trxn_id
 */
class PaymentContribution {

  protected CRM_Core_Payment_GoCardless $paymentProcessor;

  public ?Payment $payment = NULL;

  public ?array $contribution = NULL;

  public function __construct(CRM_Core_Payment_GoCardless $paymentProcessor) {
    $this->paymentProcessor = $paymentProcessor;
  }

  /**
   * Create instance starting from a known paymentID
   */
  public static function newFromPaymentID(CRM_Core_Payment_GoCardless $paymentProcessor, string $paymentID): PaymentContribution {
    return (new static($paymentProcessor))->fromPaymentID($paymentID);
  }

  public function fromPaymentID(string $paymentID): PaymentContribution {
    $this->payment = $this->paymentProcessor->getGoCardlessApi()->payments()->get($paymentID);
    $this->findMatchingContribution();
    return $this;
  }

  public function findMatchingContribution(): PaymentContribution {
    if (empty($this->payment)) {
      $this->contribution = NULL;
      return;
    }

    $this->contribution = Contribution::get(FALSE)
      ->addSelect('*', 'contribution_status_id:name')
      ->addWhere('trxn_id', '=', $this->payment->id)
      ->addWhere('is_test', '=', $this->paymentProcessor->isTestMode())
      ->execute()->first();

    return $this;
  }

  public function __set(string $prop, mixed $value): void {
    throw new \InvalidArgumentException("Property $prop does not exist, cannot be set.");
  }

  public function __get(string $prop) {
    throw new \InvalidArgumentException("Property $prop does not exist, cannot be gotten.");
  }

}
