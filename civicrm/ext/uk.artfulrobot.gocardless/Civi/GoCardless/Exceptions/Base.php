<?php
namespace Civi\GoCardless\Exceptions;

/**
 * @file
 * Exception while processing an event in a webhook.
 */
abstract class Base extends \Exception {

  /**
   * @var string*/
  protected $logLevel;

  /**
   * Re use the exception's 'code' prop with a PEAR_LOG_* level.
   */
  public function __construct(string $message, string $logLevel = 'error') {
    parent::__construct($message);
    $this->logLevel = $logLevel;
    \Civi::log('GoCardless')->log($logLevel, $message);
  }

  public function getLogLevel() :string {
    return $this->logLevel;
  }

  /**
   */
  public function isOk() :bool {
    return in_array($this->logLevel, ['debug', 'info', 'notice']);
  }

  /**
   * ERROR: PEAR_LOG_WARNING, PEAR_LOG_ERR
   */
  public function isError() :bool {
    return !$this->isOk();
  }

}
