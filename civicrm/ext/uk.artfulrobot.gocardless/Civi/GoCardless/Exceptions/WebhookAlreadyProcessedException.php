<?php
namespace Civi\GoCardless\Exceptions;

/**
 * @class
 * Exception for when an event in a webhook is detected to have been processed already.
 *
 * Safely ignorable, but worth noting.
 */
class WebhookAlreadyProcessedException extends Base {

  public function __construct(string $message) {
    parent::__construct($message, 'notice');
  }

}
