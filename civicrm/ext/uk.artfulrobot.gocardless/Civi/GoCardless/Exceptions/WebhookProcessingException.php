<?php
namespace Civi\GoCardless\Exceptions;

/**
 * @class
 * Exception for when an event in a webhook failed to process.
 *
 * Errors.
 */
class WebhookProcessingException extends Base {

  public function __construct(string $message, string $logLevel = 'error') {
    parent::__construct($message, $logLevel);
  }

}
