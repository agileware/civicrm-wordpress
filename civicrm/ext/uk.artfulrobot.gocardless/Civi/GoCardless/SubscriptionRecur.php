<?php
namespace Civi\GoCardless;

use GoCardlessPro\Resources\Subscription;
use CRM_Core_Payment_GoCardless;
use Civi\Api4\ContributionRecur;

/**
 * A GoCardless Subscription possibly maps to a CiviCRM ContributionRecur
 * by Subscription.id === ContributionRecur.processor_id
 */
class SubscriptionRecur {

  protected CRM_Core_Payment_GoCardless $paymentProcessor;

  public ?Subscription $subscription = NULL;

  public ?array $contribution = NULL;

  public function __construct(CRM_Core_Payment_GoCardless $paymentProcessor) {
    $this->paymentProcessor = $paymentProcessor;
  }

  /**
   * Create instance starting from a known subscriptionID
   */
  public static function newFromSubscriptionID(CRM_Core_Payment_GoCardless $paymentProcessor, string $subscriptionID): SubscriptionRecur {
    return (new static($paymentProcessor))->fromSubscriptionID($subscriptionID);
  }

  public function fromSubscriptionID(string $subscriptionID): SubscriptionRecur {
    $this->subscription = $this->paymentProcessor->getGoCardlessApi()->subscriptions()->get($subscriptionID);
    $this->findMatchingContributionRecur();
    return $this;
  }

  public function findMatchingContributionRecur(): SubscriptionRecur {
    if (empty($this->subscription)) {
      $this->contribution = NULL;
      return;
    }

    $this->contribution = ContributionRecur::get(FALSE)
      ->addSelect('*', 'contribution_status_id:name')
      ->addWhere('processor_id', '=', $this->subscription->id)
      ->addWhere('is_test', '=', $this->paymentProcessor->isTestMode())
      ->execute()->first();

    return $this;
  }

  public function __set(string $prop, mixed $value): void {
    throw new \InvalidArgumentException("Property $prop does not exist, cannot be set.");
  }

  public function __get(string $prop) {
    throw new \InvalidArgumentException("Property $prop does not exist, cannot be gotten.");
  }

}
