<?php
namespace Civi\GoCardless\Events;

use Civi\Api4\ContributionRecur;
use Civi\Api4\Membership;
use Civi;
use Civi\Api4\Contribution;
use Civi\GoCardless\Exceptions\WebhookAlreadyProcessedException;
use Civi\GoCardless\Exceptions\WebhookIgnoredException;
use Civi\GoCardless\Exceptions\WebhookProcessingException;

class PaymentsConfirmed extends PaymentsBase {

  /**
   * Main calling point.
   *
   * There are three implemented cases.
   */
  public function process(): string {
    if ($this->paymentHasSubscription) {
      if ($this->paymentSubscriptionHasCr) {
        if (!$this->paymentHasCn) {
          if ($this->crHasPendingCn) {
            // Case 1: This is the first (non-instant) payment on a new subscription we know.
            return $this->populatePendingContribution();
          }
          else {
            // Case 2: This is a payment on a subscription we know, but we don't have a pending one.
            return $this->newPaymentOnSubscription();
          }
        }
        else {
          throw new WebhookAlreadyProcessedException(
            "OK: Ignoring subscription payment {$this->payment->id} which is already in the database as cn{$this->cn['id']}.  Possible webhook replay?"
          );
        }
      }
      else {
        throw new WebhookIgnoredException("OK: Ignoring payment as it belongs to a subscription we don't know.");
        // We don't allow the case that we DO know a payment but not the recur/subscription.
      }
    }
    else {/* Payment is not part of a subscription. i.e. it's an instant/one-off payment. */
      if ($this->paymentHasCn) {
        if ($this->cn['contribution_status_id:name'] === 'Completed') {
          throw new WebhookAlreadyProcessedException(
            "OK: Ignoring payment already Completed in the database as cn{$this->cn['id']}. Possible webhook replay?");
        }
        if ($this->cn['contribution_recur_id']) {
          // Case 3: Instant Payment made to kick off a recur. These payments are not part of a subscription,
          // but are part of a Recur.
          return $this->populatePendingContribution();
        }
        // else: Strange, we know about a non-subscription payment that's not part of a recur.
        // In future we may handle this (support one offs) but for now it shouldn't happen.
      }
      else {
        throw new WebhookIgnoredException("OK: Ignoring non-subscription payment that we don't know.");
      }
    }

    // We don't expect other combinations to come in, so log them.
    throw new WebhookProcessingException(
      "Error: Unhandled case in " . __METHOD__ . " "
      . json_encode([
        'payment' => $this->payment->id,
        'paymentHasSubscription' => $this->paymentHasSubscription,
        'paymentSubscriptionHasCr' => $this->paymentSubscriptionHasCr,
        'paymentHasCn' => $this->paymentHasCn,
        'crHasPendingCn' => $this->crHasPendingCn,
      ]));
  }

  /**
   * Typically: this is the first payment to come in on a subscription.
   * It may be an instant payment or just the first bacs payment.
   */
  public function populatePendingContribution(): string {
    $this->ignoreIfPaymentStatusNot('confirmed', 'paid_out');
    $this->recurIsNotOverdue();

    // Check amount matches expected. This is an edge case, but it's a
    // possibility. e.g. someone sets it up through CiviCRM then changes
    // it in GoCardless (either themself or by asking staff to change it)
    // Previously we would update the contribution. But when tested in 5.57
    // this causes complications in the financial items table possibly related
    // to template contributions. So now we throw.
    $paymentAmount = number_format($this->payment->amount / 100, 2, '.', '');
    if ($this->cn['total_amount'] != $paymentAmount) {
      throw new WebhookProcessingException("FAILED: Found a Pending Contribution (ID {$this->cn['id']}) for "
        . "ContributionRecur (ID {$this->cr['id']}) but the amount is wrong: {$paymentAmount} but expected "
        . "{$this->cn['total_amount']}.");
    }

    Civi::log('GoCardless')->info("OK: Adding a Payment to complete Pending Contribution (ID {$this->cn['id']}) "
        . "for ContributionRecur (ID {$this->cr['id']})");
    // Of the params for Payment.create below, only contribution_id and
    // is_send_contribution_notification are passed on to the Contribution.completetranscation API.
    civicrm_api3('Payment', 'create', [
      // Why not 'id' ?
      'contribution_id'                   => $this->cn['id'],
      'total_amount'                      => $paymentAmount,
      'trxn_date'                         => $this->payment->charge_date,
      'trxn_id'                           => $this->payment->id,
      'is_send_contribution_notification' => $this->getSendReceipt(),
    ]);

    $this->paymentProcessorObject->setContributionReceiveDateBySQL($this->cn['id'], $this->payment->charge_date);
    return "OK: PaymentsConfirmed completed. Recur: {$this->cr['id']} Contribution: {$this->cn['id']}";
  }

  /**
   * Typically: this is a 2nd, 3rd, ... payment on a recur.
   */
  public function newPaymentOnSubscription(): string {
    $this->ignoreIfPaymentStatusNot('confirmed', 'paid_out');
    $this->recurIsNotOverdue();

    // Check amount matches expected.
    $paymentAmount = number_format($this->payment->amount / 100, 2, '.', '');
    if ($this->cr['amount'] != $paymentAmount) {
      throw new WebhookProcessingException("FAILED: Payment received for "
        . "ContributionRecur (ID {$this->cr['id']}) but the amount is wrong: {$paymentAmount} but expected "
        . "{$this->cr['amount']}.");
    }

    // Prepare data to either update a pending contribution record, or create a new one
    $repeatTransactionParams = [
      'total_amount'           => $paymentAmount,
      'receive_date'           => $this->payment->charge_date,
      'trxn_date'              => $this->payment->charge_date,
      'trxn_id'                => $this->payment->id,
      'contribution_recur_id'  => $this->cr['id'],
      'financial_type_id'      => $this->cr['financial_type_id'],
      'contact_id'             => $this->cr['contact_id'],
      'is_test'                => $this->paymentProcessorObject->isTestMode() ? 1 : 0,
    ];
    // If our policy is 'always' send a receipt, add that in now.
    if ($this->receiptPolicy === 'always') {
      $repeatTransactionParams['is_email_receipt'] = 1;
    }

    Civi::log('GoCardless')->info("OK: No Pending Contribution found for ContributionRecur "
      . "(ID {$this->cr['id']}); will create repeat Contribution.");

    // Handle receipt policy.
    //
    //                                CN.repeattransaction  Payment.create
    // # Policy  CR.is_email_receipt  is_email_receipt      is_send_contribution_notification
    // 1 never   0                    unset                 0
    // 2 never   1                    0                     0
    // 3 defer   0                    unset                 0  *
    // 4 defer   1                    unset                 0
    // 5 always  0                    unset                 1
    // 6 always  1                    unset                 1
    //
    // * required special code (see below.)

    if (!empty($this->cr['is_email_receipt']) && $this->receiptPolicy === 'never') {
      // Case (#1) Prevent a receipt being sent because that's our configured policy.
      $repeatTransactionParams['is_email_receipt'] = 0;
    }
    else {
      // Do the default thing as per CR.is_email_receipt
      unset($repeatTransactionParams['is_email_receipt']);
    }

    // Find the original_contribution_id - this tries creating a template one if the one it found was failed.
    $ipn = new \CRM_Core_Payment_GoCardlessIPN($this->paymentProcessorObject);
    $orig = $ipn->getOriginalContribution($this->cr);
    if ($orig['_was'] === 'not_found') {
      // We could not find any contribution related to this recur record. This is wrong.
      throw new WebhookProcessingException("FAILED: Could not find ANY contribution record for recur $this->cr[id]");
    }
    // Normal case: we found a Completed Contribution
    $repeatTransactionParams['original_contribution_id'] = $orig['id'];

    // From CiviCRM 5.19.1 (and possibly earlier) we need to specify the
    // membership_id on the contribution, otherwise the membership does not get
    // updated. This may/may not be related to the work on implementing Order API etc.

    if (isset(\CRM_Core_Component::getEnabledComponents()['CiviMember'])) {
      $membership = Membership::get(FALSE)
        ->addWhere('contribution_recur_id', '=', $this->cr['id'])
        ->execute()->first();
      if ($membership) {
        $repeatTransactionParams['membership_id'] = $membership['id'];
      }
    }

    // Contributions need a unique invoice_id. We won't have this yet, so we use trxn_id.
    // Note: it seems that repeattransaction ignores this, so we have to manually add it lateron.
    $repeatTransactionParams['invoice_id'] = $repeatTransactionParams['trxn_id'];

    // We'll copy various fields from the original, needed apparently.
    // source must be *provided* and *queried* as 'source' but the 'get' API returns it as 'contribution_source'!
    $repeatTransactionParams['source'] = $orig['contribution_source'] ?? '';

    // Apparently we might need to correct this.
    $repeatTransactionParams['payment_instrument_id'] = $this->paymentProcessorObject->getPaymentInstrumentID();

    // We always create pending.
    $repeatTransactionParams['contribution_status_id'] = 'Pending';

    // These next rather excessive logs are there to catch the case that GC keeps resending a webhook.
    // See https://lab.civicrm.org/extensions/gocardless/-/issues/124
    Civi::log('GoCardless')->info("OK: About to call repeattransaction");
    if (civicrm_api3('Contribution', 'getcount', ['trxn_id' => $repeatTransactionParams['trxn_id']]) > 0) {
      throw new WebhookAlreadyProcessedException(
        "Skipping processing this event as it looks like it has already been processed; there is already contribution with this payment ID in a trxn_id.",);
    }
    if (civicrm_api3('Contribution', 'getcount', ['invoice_id' => $repeatTransactionParams['trxn_id']]) > 0) {
      throw new WebhookAlreadyProcessedException("WARNING: Skipping processing this event as it looks like it has already been processed; there is already contribution with this payment ID in a invoice_id", 'warning');
    }

    // Normal case.
    $newCn = civicrm_api3('Contribution', 'repeattransaction', $repeatTransactionParams);
    if (!$newCn['id']) {
      throw new WebhookProcessingException("FAILED: repeattransaction did not result in a new contribution.", 'warning');
    }

    // repeattransaction worked.
    Civi::log('GoCardless')->info("OK: New contribution has ID $newCn[id]");

    // First restore/add various fields that the repeattransaction api may overwrite or ignore.
    // As of 5.70.1, this seems to be only invoice_id. Earlier Civi versions fail on other things too,
    // so we check all the ones that have previously been known to fail.
    $cn = Contribution::get(FALSE)
      ->addSelect('invoice_id', 'receive_date', 'payment_instrument_id', 'contribution_status_id', 'contribution_status_id:name')
      ->addWhere('id', '=', $newCn['id'])->execute()->single();
    $params = [];
    if (($cn['invoice_id'] ?? NULL) !== $repeatTransactionParams['invoice_id']) {
      $params['invoice_id'] = $repeatTransactionParams['invoice_id'];
    }
    if (($cn['payment_instrument_id'] ?? NULL) != $repeatTransactionParams['payment_instrument_id']) {
      $params['payment_instrument_id'] = $repeatTransactionParams['payment_instrument_id'];
    }
    if (substr($cn['receive_date'] ?? '', 0, 10) !== substr($repeatTransactionParams['receive_date'], 0, 10)) {
      $params['receive_date'] = $repeatTransactionParams['receive_date'];
    }
    if ($params) {
      // contribution_status_id is required for any BAO updates, and if it is not provided
      // it costs another Contribution.get call, so we provide for efficiency.
      $params['contribution_status_id'] = $cn['contribution_status_id'];

      // Remove the cache table - as of 5.70 it can get out of date and cause contribution.get to fail on the newly created contribution.
      // See https://lab.civicrm.org/extensions/gocardless/-/issues/141
      if (!empty(\Civi::$statics['CRM_Contribute_BAO_Query']['soft_credit_temp_table_name'])) {
        \CRM_Core_DAO::executeQuery('DROP TEMPORARY TABLE IF EXISTS ' . \Civi::$statics['CRM_Contribute_BAO_Query']['soft_credit_temp_table_name']);
        unset(\Civi::$statics['CRM_Contribute_BAO_Query']['soft_credit_temp_table_name']);
      }

      $params['id'] = $newCn['id'];
      Civi::log('GoCardless')->info("About to try to update cn with params that repeattransaction failed to set.", compact('cn', 'params'));
      $result = civicrm_api3('Contribution', 'create', $params);
      if ($result['is_error'] ?? NULL) {
        // This never happens - seems exceptions are thrown instead.
        throw new WebhookProcessingException("FAILED: repeattransaction worked but trying to correct the data it fails on did not.", 'warning');
      }
    }

    // Now complete the new contribution by creating a payment for the same amount.

    // Handle receipt policy. This is stupidly difficult and this code had to
    // change when testing against 5.57 - not sure what version changed things,
    // was fine on 5.49
    $paymentCreateParams = [
      'contribution_id' => $newCn['id'],
      'trxn_id'         => $repeatTransactionParams['trxn_id'],
      'trxn_date'       => $repeatTransactionParams['receive_date'],
      'total_amount'    => $repeatTransactionParams['total_amount'],
    ];
    if ($this->receiptPolicy === 'never') {
      // Cases (#1), (#2)
      $paymentCreateParams['is_send_contribution_notification'] = 0;
    }
    elseif ($this->receiptPolicy === 'always') {
      // Cases (#5), (#6)
      $paymentCreateParams['is_send_contribution_notification'] = 1;
    }
    elseif ($this->receiptPolicy === 'defer' && empty($this->cr['is_email_receipt'])) {
      // Case (#3) - why do we have to do this to prevent a receipt?
      $paymentCreateParams['is_send_contribution_notification'] = 0;
    }

    $result = civicrm_api3('Payment', 'create', $paymentCreateParams);
    if ($result['is_error'] ?? NULL) {
      throw new WebhookProcessingException(
        "FAILED: Payment.create API failed\n" .
          json_encode([
            'API params'   => $paymentCreateParams,
            'Result'       => $result,
            'Contribution' => $newCn,
          ], JSON_PRETTY_PRINT));
    }
    else {
      Civi::log('GoCardless')->info("OK: Added payment $paymentCreateParams[trxn_id] to Contribution ID $newCn[id]");
    }

    return "OK: PaymentsConfirmed completed. Recur: {$this->cr['id']} Contribution: {$newCn['id']}";
  }

  protected function recurIsNotOverdue() {
    if ($this->cr['contribution_status_id:name'] === 'Overdue') {
      ContributionRecur::update(FALSE)
        ->addWhere('id', '=', $this->cr['id'])
        ->addValue('contribution_status_id:name', 'In Progress')
        ->addValue('failure_count', 0)
        ->execute()->first();
      $this->cr = ContributionRecur::get(FALSE)
        ->addSelect('*', 'contribution_status_id:name')
        ->addWhere('id', '=', $this->cr['id'])
        ->execute()->first();
    }
  }

  protected function getSendReceipt(): int {
    $sendReceipt = 0;
    if ($this->receiptPolicy === 'always') {
      $sendReceipt = 1;
    }
    elseif ($this->receiptPolicy === 'defer') {
      $sendReceipt = empty($this->cr['is_email_receipt']) ? 0 : 1;
    }
    return $sendReceipt;
  }

}
