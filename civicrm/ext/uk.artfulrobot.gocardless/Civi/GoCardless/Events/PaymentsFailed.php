<?php
namespace Civi\GoCardless\Events;

use GoCardlessPro\Resources\Payment;
use Civi\Api4\ContributionRecur;
use Civi\Api4\Note;
use Civi\GoCardless\Exceptions\WebhookAlreadyProcessedException;
use Civi\GoCardless\Exceptions\WebhookIgnoredException;
use Civi;
use Civi\GoCardless\Exceptions\WebhookProcessingException;

class PaymentsFailed extends PaymentsBase {

  /**
   * This is only set when we have no match on trxn_id == paymentID.
   */
  public ?bool $crHasPendingCn = NULL;

  /**
   * Main calling point.
   *
   * There are three implemented cases.
   */
  public function process(): string {
    if ($this->paymentHasSubscription) {
      if ($this->paymentSubscriptionHasCr) {
        if (!$this->paymentHasCn) {
          if ($this->crHasPendingCn) {
            // Case 1: This is the first (non-instant) payment on a new subscription we know.
            $this->recurIsOverdue();
            return $this->markCnFailed();
          }
          else {
            // Case 2: This is a new failed payment on a known subscription/cr.
            $this->recurIsOverdue();
            return $this->newPaymentOnSubscription();
          }
        }
        else {/* payment is already a cn. Likely a late failure */
          // Case 3: This is telling us a payment that we have already recorded has now failed.
          $this->recurIsOverdue();
          return $this->markCnFailed('Late failure: it looked like this payment was successful, but it has since failed.');
        }
      }
      else {
        throw new WebhookIgnoredException("OK: Ignoring failed payment as it belongs to a subscription we don't know.");
        // We don't allow the case that we DO know a payment but not the recur/subscription.
      }
    }
    else {/* Payment is not part of a subscription. */
      if ($this->paymentHasCn) {
        if ($this->cn['contribution_recur_id']) {
          // Case 3: Instant Payment made to kick off a recur. These payments are not part of a subscription,
          // but are part of a Recur.
          $this->recurIsOverdue();
          return $this->markCnFailed('Late failure of an Instant Payment: it looked like this was successful, but it has since failed.');
        }
        else {
          // We know about a non-subscription payment that's not part of a recur.
          // One off payments through GC are not currently officially supported, so this codepath should never
          // execute, but in future we may.
          return $this->markCnFailed();
        }
      }
      else {
        throw new WebhookIgnoredException("OK: Ignoring non-subscription payment that we don't know.");
      }
    }

    // We don't expect other combinations to come in, so log them.
    throw new WebhookProcessingException(
      "Error: Unhandled case in " . __METHOD__ . " "
      . json_encode([
        'payment' => $this->payment->id,
        'paymentHasSubscription' => $this->paymentHasSubscription,
        'paymentSubscriptionHasCr' => $this->paymentSubscriptionHasCr,
        'paymentHasCn' => $this->paymentHasCn,
        'crHasPendingCn' => $this->crHasPendingCn,
      ]));
  }

  /**
   */
  public function markCnFailed(string $reason = ''): string {

    // CiviCRM will not let us change from Completed to Failed.
    // So we have to use Refunded if it's not Pending.
    $isPending = $this->cn['contribution_status_id:name'] === 'Pending';

    civicrm_api3('Contribution', 'create', [
      'id'                     => $this->cn['id'],
      'receive_date'           => $this->payment->charge_date,
      'trxn_id'                => $this->payment->id,
      'contribution_status_id' => $isPending ? 'Failed' : 'Refunded',
    ]);

    if ($reason) {
      Note::create(FALSE)
        ->addValue('entity_table', 'civicrm_contribution')
        ->addValue('entity_id', $this->cn['id'])
        ->addValue('note', $reason)
        ->execute();
    }

    return "Marked contribution {$this->cn['id']} Failed. Previously it was {$this->cn['contribution_status_id:name']}.";
  }

  /**
   * Typically: this is a 2nd, 3rd, ... payment on a recur.
   */
  public function newPaymentOnSubscription(): string {
    $this->ignoreIfPaymentStatusNot('failed');
    $this->recurIsOverdue();

    // Check amount matches expected.
    $paymentAmount = number_format($this->payment->amount / 100, 2, '.', '');
    if ($this->cr['amount'] != $paymentAmount) {
      throw new WebhookAlreadyProcessedException("FAILED: Payment received for "
        . "ContributionRecur (ID {$this->cr['id']}) but the amount is wrong: {$paymentAmount} but expected "
        . "{$this->cr['amount']}.");
    }

    // Prepare data to create a new cn
    $repeatTransactionParams = [
      'contribution_status_id' => 'Failed',
      'total_amount'           => $paymentAmount,
      'receive_date'           => $this->payment->charge_date,
      'trxn_date'              => $this->payment->charge_date,
      'trxn_id'                => $this->payment->id,
      'contribution_recur_id'  => $this->cr['id'],
      'financial_type_id'      => $this->cr['financial_type_id'],
      'contact_id'             => $this->cr['contact_id'],
      'is_test'                => $this->paymentProcessorObject->isTestMode() ? 1 : 0,
    ];

    Civi::log('GoCardless')->info("OK: No Pending Contribution found for ContributionRecur "
      . "(ID {$this->cr['id']}); will create repeat Contribution.");

    $ipn = new \CRM_Core_Payment_GoCardlessIPN($this->paymentProcessorObject);
    $orig = $ipn->getOriginalContribution($this->cr);
    if ($orig['_was'] === 'not_found') {
      // We could not find any contribution related to this recur record. This is wrong.
      throw new WebhookProcessingException("FAILED: Could not find ANY contribution record for recur $this->cr[id]");
    }
    // Normal case: we found a Contribution
    $repeatTransactionParams['original_contribution_id'] = $orig['id'];

    $newCn = civicrm_api3('Contribution', 'create', $repeatTransactionParams);
    if ($newCn['is_error'] ?? NULL) {
      throw new WebhookProcessingException("FAILED: Contribution.create for a failed payment ... failed");
    }
    Civi::log('GoCardless')->info("OK: New failed contribution has ID $newCn[id]");
    return "OK: PaymentsFailed completed. Recur: {$this->cr['id']} Contribution: {$newCn['id']}";
  }

  protected function recurIsOverdue() {
    $cr = ContributionRecur::update(FALSE)
      ->addWhere('id', '=', $this->cr['id'])
      ->addWhere('contribution_status_id:name', '=', 'In Progress')
      ->addValue('contribution_status_id:name', 'Overdue')
      ->addValue('failure_count', ($this->cr['failure_count'] ?? 0) + 1)
      ->execute()->first();
    $this->loadCr($this->cr['id']);
    $status = $this->cr['contribution_status_id:name'];
    if (empty($cr)) {
      Civi::log('GoCardless')->notice("cr{$this->cr['id']} not marked Overdue despite payments.failed event because its status is {status}.", compact('status'));
    }
    else {
      Civi::log('GoCardless')->notice("cr{$this->cr['id']} is marked Overdue because of a payments.failed event.");
    }
  }

}
