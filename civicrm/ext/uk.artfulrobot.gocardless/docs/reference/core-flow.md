<!-- this will render in gitlab, but not mkdocs. Can use
     https://mermaid-js.github.io/mermaid-live-editor/edit
     to render for docs.
-->

```mermaid
sequenceDiagram
 participant Civi
 participant Browser
 participant GoCardless
 Civi->>Browser: CiviCRM Contribution page
 note over Browser: Completes form
 Browser->>Civi: data
 note over Civi: CiviContribute creates Pending cr and<br>cn records, then calls doPayment()
 note over Civi: doPayment creates BRQ + BRF<br>with TY page as success URL<br>Store BRQ ID as cr.processor_id
 note over Civi: CRM_Utils_System::redirect($url)
 Civi->>Browser: Location: header $url
 note over Browser, GoCardless: User completes GC BRQ forms
 GoCardless->>Browser: 301 redirect URL to TY page
 Browser->>Civi: TY page url with ?brqID query param
 note over Civi: TY buildForm hook: checks <br>and creates subscription<br>updates cr setting<br>processor_id = subscription ID<br>as current system does.
 Civi->>Browser: TY page
```

