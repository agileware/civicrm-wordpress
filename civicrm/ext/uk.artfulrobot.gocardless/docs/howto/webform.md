# Recipe: Use with Drupal 7's WebForm module

!!!CAUTION
    This extension can be made to work by using a patched version of Drupal 7's WebForm-CiviCRM extension.
    It is untested/probably doesn't work with Drupal 8+'s WebForm.

After 6 years of this being a [problem](https://lab.civicrm.org/extensions/gocardless/-/issues/32) for WebForm users, funding was finally found for a limited (see above) fix.


Here’s how:

## Ingredients

- patched `webform_civicrm` Drupal module. https://github.com/artfulrobot/webform_civicrm/tree/support-gocardless
- 2.0.2+ GoCardless extension.
- a contribution page that uses GoCardless for recurring contributions.
- a WebForm that uses the contribution page.

## Method

The patched `webform_civicrm` Drupal module was branched from the 7.x-5.x (dev) branch. So if you're on some older version I'd upgrade to 5.8 first, then try replacing with the patched module ([zip download link](https://github.com/artfulrobot/webform_civicrm/archive/refs/heads/support-gocardless.zip))

Set up the contribution page. Of course you'll need to ☑ recurring, and select GoCardless as the processor.

Set up the WebForm. Select the contribution page and remember to set the frequency to **Monthly** (*Annual* should work too).

This *should* then work in a live context.

## If you want to test it instead of just going live (and who wouldn't?!)...

...there's a bit more of a fiddle. It seems to me that WebForm might not support test payment processors (if someone knows WebForm well, I'd love to be wrong about this). But it looks to me that if you don't enable live payments then you don't get any payments.

So for the sake of testing only you'll need to do the following. **Please note that doing this on a live site will break your actual real money GoCardless integration.**

Add this to your civicrm.settings.php file:
```
define('CIVI_GOCARDLESS_FORCE_SANDBOX_ENVIRONMENT', 1);
```

Then edit your GoCardless Payment Processor and (a) copy the existing API keys + webhook secrets somewhere safe, then (b) put the test/sandbox versions in civi's "live" fields.

See [Using test credentials on the live processor](../reference/technical.md#using-test-credentials-on-the-live-processor).

## Proof is in the tasting

If you get as far as this, then you go look at a contact who completed the WebForm, you should expect to see a Pending contribution linked to an In Progress recur. The recur will probably have a "processor ID" that begins BRQ... (If it already has one beginninng SB... you're all good.) This means no subscription has been set up yet (just the mandate). However when the webhook comes in, the subscription should become set up (at GoCardless) and you should see the processor ID has changed to one beginning with SB... The Payment will remain Pending until a week later - unless you force it via the developer panel in the gocardless admin.

## On a live site

On a live site you'd just install the new WebForm + gocardless as above, but you wouldn't do any of the civicrm.settings or swapping API keys bits - all that is just for testing.

