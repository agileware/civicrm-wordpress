# Alternative install instructions

As well as installing in-app, you can install from the releases page, or
developers might like to install from `git clone` (see below).

The packaged version of this extension include the GoCardlessPro PHP libraries
and exclude some dev-only bits including the `bin`, `cli` and `tests`
directories.

## Installing from release archive

The [releases
page](https://lab.civicrm.org/extensions/gocardless/-/releases)
lists all the releases; you normally want the latest one, **but do check it is 
suitable for your version of CiviCRM** - the text on that page should make that 
pretty clear.

**Be sure to download the .tgz or .zip file specified in the intro text, not the source code**

Unzip that in your extensions directory, then install.

## Installing from git clone

This extension requires the GoCardlessPro PHP library. Here's how to install
from the [linux/mac] command line. You need
[composer](https://getcomposer.org/download/).

    $ cd /path/to/your/extensions/dir
    $ git clone https://lab.civicrm.org/extensions/gocardless.git
    $ cd gocardless
    $ # git checkout <branchname> # optional
    $ composer install # installs the GoCardlessPro libraries

Finally, install the extension. You can do this through the CiviCRM UI or if you have [cv](https://github.com/civicrm/cv) available:

    $ cv en gocardless

!!! note "Worth a mention"
    Side note: pre 1.9.3, this extension bundled a version of the Guzzle
    library. Since this is already included in CiviCRM (and has been for
    a while) it is now no longer included.

!!! tip "Developers"
    If you are developing this extension and want to work with the phpunit 
    tests, you will need to download the [Tagged 
    Log](https://lab.civicrm.org/extensions/taggedlog) extension too, since it's 
    a test-dependency.
