# Troubleshooting

## Understand this...

There's a few terms you’ll need to understand.

- API. An [API](https://en.wikipedia.org/wiki/API "Wikipedia") is a published way for one system to interact with another system. CiviCRM has an API that developers use to make changes to CiviCRM data. GoCardless has an API that this extension uses to 'talk to' GoCardless, to set up mandates, make changes etc.

- Log. A record of stuff that has happened. There are many types of log, to be found in many different places:
   - Webserver (nginx, apache, php) logs: typically you don't need these unless something's going really wrong, like you get a "white screen of death" or such. Where they live and how to access them varies wildly. Let’s hope we don’t need to get into that!
   - CMS (Drupal, Joomla, WordPress) logs: some CMSes provide a way to log things. CiviCRM can be configured to copy its logs to Drupal's "watchdog" log system, which gives you a nice page (admin » reports » recent log messages) to view them from.
   - CiviCRM logs: These are normally text files. CiviCRM has a main log file that it uses for all its core stuff, but the GoCardless extension has it’s own set of log files. These are where you mostly want to be looking. See next section.
   - GoCardless webhook event logs: you can access these from the <https://manage.gocardless.com> and <https://manage-sandbox.gocardless.com> administrative sites for your account. They show 'events' (things that happened at GoCardless) that were sent via 'webhooks'...
   - Webhook. A machine-to-machine notification about something that has happened, e.g. when a payment is confirmed, GoCardless can send a message about this to CiviCRM.
   - Webhook URL, or Webhook Endpoint. The URL/web address to which these messages will be sent. Note that this URL is not a web **page**; it's not for human use in a browser, and if you visit it you’ll just see a blank page.

- The GoCardless processes. See the [pretty diagram](https://docs.civicrm.org/gocardless/en/latest/reference/technical/). That page should also give you a reasonable understanding of the processes that are going on, which may help you troubleshoot.

## GoCardless logs

The GoCardless extension writes logs with a lot of detail in them. You'll find these as text files along with your other CiviCRM log files - the GoCardless ones have filenames with 'GoCardless' in. The location varies by CMS; see below.  Note that you'll need access to your server's file system.  If you have FTP/SFTP/SSH access, you can use that.  If you have a control panel, check for a "File Manager" plugin:

* **Drupal**: `<drupalroot>/sites/default/files/civicrm/`
* **Joomla**: `<joomlaroot>/media/civicrm/`
* **WordPress**: `<wordpressroot>/wp-content/uploads/civicrm/`
* **WordPress (old)**: `<wordpressroot>/wp-content/plugins/files/civicrm/`
  * Any CiviCRM version 5.29 and later will never have the "old" location.
* **Backdrop**: `<backdroproot>/files/civicrm/`

(Most of the above text was copied from Jon G's [stack exchange answer](https://civicrm.stackexchange.com/questions/15931/where-are-the-configandlog-and-templates-c-directories/15932#15932) - *thanks!*)

## Using the mjwshared extension (v1.2+) gives you an overview

GoCardless webhooks contain one or more 'events'. If you are using the mjwshared extension, you can browse these events. This is a quick way to find things that have failed, and to find event IDs which may be useful in searching the GoCardless log files.

## Some webhooks keep failing, and my logs are full of "already exists" errors

Please see [How to improve webhook management](../howto/mjwshared.md) for a solution to this.

## 400 errors showing on webhooks at manage.gocardless.com

This is typically that you have used the wrong webhook URL or secret. Double check those first, from the **Administer » CiviContribute » GoCardless settings** page, and the relevant webhooks configuration page on manage.gocardless.com

## Recurring Contributions are stuck at "Pending"

This can mean two things:

1. the donor gave up when they got to the GoCardless mandate setup form. Possibly this might also occur if the mandate set-up fails. So if there's a few cases of this problem, that might be it. If *everything* is like this then it's more likely to be...

2. you do not have your account set up correctly, so calls to the GoCardless API are failing. First thing to do is to check your Payment Processor settings are correct. In particular, check that you have the correct API keys in for Live/Test(sandbox) accounts.

If you have the correct credentials, then it's time to look at your logs. Look in the CiviCRM and the GoCardless ones. Best thing to do is to set up a new test/sandbox direct debit (e.g. use the "Test Drive" link for a donation page). You’ll need to use the GoCardless test account details for this, which are `20-00-00` and `55779911`. Hopefully this will reproduce the problem, and give you some fresh logs to look at.

You can then report those logs in the Mattermost chat: [chat.civicrm.org/civicrm/channels/gocardless](https://chat.civicrm.org/civicrm/channels/gocardless) along with your CiviCRM version, your CMS (Drupal/WordPress...), and the version of your GoCardless extension.

## Recurring Contribution is "In Progress" but Contributions are "Pending/Incomplete"

First: have you waited long enough for the payment to become complete? It will typically by 8 days after the mandate is set up that the first payment is *confirmed* (the payment *date* is typically 7 days after set-up, but they allow 24 hours for it to bounce). You can login at GoCardless to see the status of a payment. If it's confirmed or paid out, it's completed and should be showing as Completed in CiviCRM, so read on.

So a payment is completing OK, but CiviCRM has not been updated to show this. This means that the webhook sent by GoCardless is not getting through, or not getting processed correctly by CiviCRM.

- Check your Payment Processor settings in CiviCRM. The 'webhook secrets' must 
  be correct and unique. See [webhook setup](../tutorial/webhook.md)

- Check your webhooks at GoCardless’ end. Don’t get confused between sandbox(test) and live accounts 😄

- Check your webhook URLs are accessible publicly. Get the webhook endpoint URL, which might be like `https://your-civicrm-site.org/civicrm/payment/ipn/123` and put it in a browser **that you are not logged in on** (e.g. using a private browsing/incognito window is a good way to test this). If you get a completely blank screen, *that’s correct*. If you see an error like Access Denied you’ll need to update the permissions in your CMS to allow anonymous access to that page; if this URL cannot be reached publicly, GoCardless’ servers won’t be able to send webhooks to it.

If webhooks are getting through OK, you will see them in the GoCardless log files, so check there for errors, and also check around the same time for any errors in the CiviCRM log files.

Note that you’ll see a lot of "ignored unimplemented webhook ..." lines in the log file. **This is OK!** GoCardless sends a lot of webhooks and we don’t need to care about most of them. This line in the logs just acknowledges that they were sent OK, but we don’t use them. Since v1.12.0, the messages in the logs should be a lot less alarming/more informative.

Webhooks can contain a bunch of events, or a single event.

You’re looking for a Payment Confirmed event - look for: `"resource_type":"payments","action":"confirmed"`

The logs  will show a timestamp for a webhook that was received and two lines, one with headers one with body.

For versions of the extension up to v 1.11.1 then only errors were logged, so there would be nothing else with that timestamp following a successful run. Since 1.12.0, there's more verbose logging to be found which should be much more helpful in seeing what’s gone one.

If the logs don’t help you figure it out, feel free to get in touch on the [friendly community chat channel](https://chat.civicrm.org/civicrm/channels/gocardless) - CiviCRM’s community includes many people who are happy to help others out. Take care not to post any private or sensitive data when explaining your problem.

## Serious errors preventing CiviCRM admin pages from loading or such.

Typically this occurs when you have **downloaded the wrong code** for the extension, e.g. you downloaded (or `git clone`-ed) the *source code*, rather than a release package. The source code does not contain the third party GoCardless libraries. If you *want* to be running from source code, e.g. maybe you're using git to try a particular branch or a not-yet-released version, you’ll need to run `composer install` in the extension's directory. This should create and populate the `vendor/` directory.
