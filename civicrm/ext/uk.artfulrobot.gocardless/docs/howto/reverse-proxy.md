# Running CiviCRM behind a reverse proxy (e.g. in a container)

If you set CiviCRM up behind a reverse proxy, requests to the public URL get intercepted and forwarded to a different, typically private/local, URL.

This can mean that the public https request becomes an http request locally. It's therefore important that CiviCRM, running over plain http, knows that it's supposed to be using https, and that requests are https.

Your httpd server (nginx, Apache etc.) can help matters by ensuring data from the original request are made available.

## nginx

For example, in an nginx `location` block, you might use:

```nginx
    proxy_set_header X-Forwarded-Proto $scheme;
    proxy_set_header Host $host;
    proxy_pass http://localhost:23456;
```

This ensures that `$_SERVER['HTTP_X_FORWARDED_PROTO']` is available and contains the original scheme/protocol (http or https), and that CiviCRM receives the request as if it were made to the original (public) `$host` not the actual one (`localhost:23456` in this example).

## Apache

I believe the key part here is this:


```apache
RequestHeader set "X-Forwarded-Proto" expr=%{REQUEST_SCHEME}
```

(If you are an Apache user, PRs welcome to improve these docs.)
