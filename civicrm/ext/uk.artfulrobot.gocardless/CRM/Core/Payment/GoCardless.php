<?php

/**
 * @file
 * Payment Processor for GoCardless.
 */
use CRM_GoCardless_ExtensionUtil as E;
use Civi\Payment\Exception\PaymentProcessorException;
use Civi\Payment\PropertyBag;
use Civi\Api4\PaymentprocessorWebhook;
use Civi\Api4\ContributionRecur;
use Civi\GoCardless\Exceptions\Base as WebhookException;
use Brick\Money\Money;
use GoCardlessPro\Resources\BillingRequest;
use GoCardlessPro\Resources\BillingRequestFlow;

/**
 *
 */
class CRM_Core_Payment_GoCardless extends CRM_Core_Payment {

  /**
   * There aren't 32 days in the month, but this is a valid
   * value for ContributionRecur.cycle_day so we use it to
   * denote last day of the month.
   */
  const CYCLE_DAY_FOR_LAST_DAY_OF_MONTH = 32;

  public const NO_INSTANT_PAYMENT = 0;
  public const INSTANT_PAYMENT_REQUIRED = 1;
  public const INSTANT_PAYMENT_WITH_FALLBACK = 2;

  /**
   * @var Arrayof\GoCardlessPro\Clientobjectskeyedbypaymentprocessorid
   */
  protected static $gocardless_api;

  /**
   * Fields that affect the schedule and are defined as editable by the processor.
   *
   * This is deliberately blank; for now we only suport changing the amount.
   *
   * @var array
   */
  protected $editableScheduleFields = [];

  /**
   * Constructor
   *
   * @param string $mode the mode of operation: live or test
   * @param $paymentProcessor
   */
  public function __construct($mode, &$paymentProcessor) {
    $this->_paymentProcessor = $paymentProcessor;
    // ? $this->_processorName    = E::ts('GoCardless Processor');
  }

  /**
   * This function checks to see if we have the right config values.
   *
   * artfulrobot: I'm not clear how this is used. It's called when saving a
   * PaymentProcessor from the UI but its output is never shown to the user,
   * so presumably it's used elsewhere. YES: it's used when you visit the
   * Contributions Tab of a contact, for example.
   *
   * @return string the error message if any
   * @throws \CiviCRM_API3_Exception
   */
  public function checkConfig() {
    $paymentProcessorType = civicrm_api3('PaymentProcessorType', 'getsingle', [
      'id' => $this->_paymentProcessor['payment_processor_type_id'],
    ]);

    if (empty($this->_paymentProcessor['user_name'])) {
      $errors[] = E::ts("Missing %1", [1 => $paymentProcessorType['user_name_label']]);
    }
    if (empty($this->_paymentProcessor['url_api'])) {
      $errors[] = E::ts("Missing URL for API. This sould probably be %1 (for live payments) or %2 (for test/sandbox)",
        [
          1 => $paymentProcessorType['url_api_default'],
          2 => $paymentProcessorType['url_api_test_default'],
        ]);
    }

    if (!empty($errors)) {
      $errors = "<ul><li>" . implode('</li><li>', $errors) . "</li></ul>";
      return $errors;
    }

    /* This isn't appropriate as this is called in various places, not just on saving the payment processor config.

    $webhook_url = CRM_Utils_System::url('civicrm/gocardless/webhook', $query=NULL, $absolute=TRUE, $fragment=NULL,  $htmlize=TRUE, $frontend=TRUE);
    CRM_Core_Session::setStatus("Ensure your webhooks are set up at GoCardless. URL is <a href='$webhook_url' >$webhook_url</a>"
    , 'Set up your webhook');
     */
  }

  /**
   * Build the user-facing form.
   *
   * This is minimal because most data is taken in a Go Cardless redirect flow.
   *
   * Nb. Other direct debit schemes's pricing is based upon the number of
   * collections but GC's is just based on transactions. While it may still be
   * nice to offer a collection day choice, this is not implemented here yet.
   */
  public function buildForm(&$form) {
    //$form->add('select', 'preferred_collection_day', E::ts('Preferred Collection Day'), $collectionDaysArray, FALSE);
  }

  /**
   * Attempt to cancel the subscription at GoCardless.
   *
   * @param \Civi\Payment\PropertyBag $propertyBag
   *
   * @return array|null[]
   * @throws \Civi\Payment\Exception\PaymentProcessorException
   */
  public function doCancelRecurring(\Civi\Payment\PropertyBag $propertyBag) {
    // By default we always notify the processor and we don't give the user the option
    // because supportsCancelRecurringNotifyOptional() = FALSE
    // @fixme setIsNotifyProcessorOnCancelRecur was added in 5.27 - remove method_exists once minVer is 5.27
    if (method_exists($propertyBag, 'setIsNotifyProcessorOnCancelRecur')) {
      if (!$propertyBag->has('isNotifyProcessorOnCancelRecur')) {
        // If isNotifyProcessorOnCancelRecur is NOT set then we set our default
        $propertyBag->setIsNotifyProcessorOnCancelRecur(TRUE);
      }
      $notifyProcessor = $propertyBag->getIsNotifyProcessorOnCancelRecur();
    }
    else {
      // CiviCRM < 5.27
      $notifyProcessor = (boolean) CRM_Utils_Request::retrieveValue('send_cancel_request', 'Boolean', TRUE, FALSE, 'POST');
    }

    if (!$notifyProcessor) {
      return ['message' => ts('Successfully cancelled the subscription in CiviCRM ONLY.')];
    }

    $log_message = __FUNCTION__ . ": "
      . json_encode(['subscription_id' => $propertyBag->getRecurProcessorID(), 'contribution_recur_id' => $propertyBag->getContributionRecurID()])
      . ' ';
    try {
      $gc_api = $this->getGoCardlessApi();
      // The GoCardless subscription ID is stored in processor_id
      $gc_api->subscriptions()->cancel($propertyBag->getRecurProcessorID());
      Civi::log('GoCardless')->info("$log_message SUCCESS");
    }
    catch (\GoCardlessPro\Core\Exception\ApiException $e) {
      // Api request failed / record couldn't be created.
      $this->logGoCardlessException("$log_message FAILED", $e);
      // repackage as PaymentProcessorException
      throw new PaymentProcessorException($e->getMessage());
    }
    catch (\GoCardlessPro\Core\Exception\MalformedResponseException $e) {
      // Unexpected non-JSON response
      $this->logGoCardlessException("$log_message FAILED", $e);
      throw new PaymentProcessorException('Unexpected response type from GoCardless');
    }
    catch (\GoCardlessPro\Core\Exception\ApiConnectionException $e) {
      // Network error
      $this->logGoCardlessException("$log_message FAILED", $e);
      throw new PaymentProcessorException('Network error, please try later.');
    }

    return ['message' => ts('Successfully cancelled the subscription at GoCardless.')];
  }

  /**
   * Attempt to cancel the subscription at GoCardless.
   * @deprecated Remove when min CiviCRM version is 5.25
   *
   * @see supportsCancelRecurring()
   *
   * @param string $message
   * @param array|\Civi\Payment\PropertyBag $params
   *
   * @return bool
   * @throws \CiviCRM_API3_Exception
   * @throws \Civi\Payment\Exception\PaymentProcessorException
   */
  public function cancelSubscription(&$message = '', $params = []) {
    $propertyBag = \Civi\Payment\PropertyBag::cast($params);
    if (!$propertyBag->has('recurProcessorID')) {
      throw new \Civi\Payment\Exception\PaymentProcessorException("cancelSubscription requires the recurProcessorID");
    }

    // contributionRecurID is set when doCancelRecurring is called directly (from 5.25)
    if (!$propertyBag->has('contributionRecurID')) {
      $contrib_recur = civicrm_api3('ContributionRecur', 'getsingle', ['processor_id' => $propertyBag->getRecurProcessorID()]);
      $propertyBag->setContributionRecurID($contrib_recur['id']);
    }

    $message = $this->doCancelRecurring($propertyBag)['message'];
    return TRUE;
  }

  /**
   * Attempt to change the subscription at GoCardless.
   *
   * Note there are some limitations here:
   *
   * - We can only make 10 changes before we need to cancel the subscription
   *   and create a new one. The latter is not implemented.
   * - We can only change the amount (not frequency etc)
   *
   * @param string $message
   * @param array $params
   *
   * - id: ContributionRecur ID
   * - amount: new amount
   *
   * The following may be received from CiviCRM's
   * CRM_Contribute_Form_UpdateSubscription but are **not required/used**.
   *
   * - subscriptionId: This is the value of ContributionRecur.processor_id
   *   which (as of v1.9) relates to the GoCardless subscription ID.
   * - is_notify: 0|1 whether to notify the user (not implemented here)
   * - installments: new No. installments (can be blank)
   * - (campaign_id): only present if we support that
   * - (financial_type_id): only present if we support changing that
   * - custom ContributionRecur data fields
   * - ? entityID, if it's there it belongs to ContributionRecur
   * - Plus any fields defined in $editableScheduleFields.
   *
   * @return array|bool|object
   * @throws \Civi\Payment\Exception\PaymentProcessorException
   */
  public function changeSubscriptionAmount(&$message = '', $params = []) {
    // Get the GoCardless subscription ID, stored in processor_id
    $contrib_recur = civicrm_api3('ContributionRecur', 'getsingle', ['id' => $params['id']]);
    $subscription_id = $contrib_recur['processor_id'] ?? NULL;
    if (!$subscription_id) {
      throw new PaymentProcessorException("Missing GoCardless subscription ID in ContributionRecur processor_id field. Cannot process changing subscription amount.");
    }

    if (empty($params['amount']) || ((float) $params['amount']) < 0.01) {
      throw new PaymentProcessorException("Missing/invalid amount");
    }
    if ($params['amount'] == $contrib_recur['amount']) {
      // Don't attempt to update the subscription at GoCardless if the amount hasn't changed
      // but allow CiviCRM to continue processing any other changes
      $message = "Amount unchanged";
      return TRUE;
    }

    if (!empty($params['installments'])) {
      throw new PaymentProcessorException("This processor does not support changing the number of installments.");
    }

    // Make sure a template contribution exists.
    CRM_Contribute_BAO_ContributionRecur::ensureTemplateContributionExists($contrib_recur['id']);

    $subscription_updates = [
      'params' => [
        // Convert to pennies.
        'amount' => (int) (100 * $params['amount']),
      ],
    ];

    $logger = Civi::log('GoCardless');
    $logger->info('', ['=start' => ["subscription=$subscription_id", "cr_id=$params[id]"]]);
    try {
      $gc_api = $this->getGoCardlessApi();
      $gc_api->subscriptions()->update($subscription_id, $subscription_updates);
      $logger->info("changeSubscriptionAmount SUCCESS", ['=pop' => 2]);
    }
    catch (\GoCardlessPro\Core\Exception\ApiException $e) {
      // Api request failed / record couldn't be created.
      $this->logGoCardlessException("FAILED", $e);
      $logger->info('', ['=pop' => 2]);
      // repackage as PaymentProcessorException
      throw new PaymentProcessorException($e->getMessage());

    }
    catch (\GoCardlessPro\Core\Exception\MalformedResponseException $e) {
      // Unexpected non-JSON response
      $this->logGoCardlessException("FAILED", $e);
      $logger->info('', ['=pop' => 2]);
      throw new PaymentProcessorException('Unexpected response type from GoCardless');

    }
    catch (\GoCardlessPro\Core\Exception\ApiConnectionException $e) {
      // Network error
      $this->logGoCardlessException("FAILED", $e);
      $logger->info('', ['=pop' => 2]);
      throw new PaymentProcessorException('Network error, please try later.');
    }

    $message = "Successfully updated regular amount to $params[amount].";
    return TRUE;
  }

  /**
   * Sends user off to Gocardless.
   *
   * Note: the guts of this function are in getBillingRequestFlowURL() so that
   * can be tested without issuing a redirect.
   *
   * This is called by civicrm_api3_contribution_transact calling doPayment on the payment processor
   * which happens as part of the CiviContribute Contribution pages flow.
   *
   * @param array|PropertyBag $params
   * @param string $component
   * @return void A redirect is called; this function never returns.
   *
   * @throws \Civi\Payment\Exception\PaymentProcessorException
   */
  public function doPayment(&$params, $component = 'contribute') {
    // Not sure what the point of this next line is.
    $this->_component = $component;

    if (is_array($params)) {
      $params = $this->chooseBestContactDetails($params);
    }

    $propertyBag = PropertyBag::cast($params);
    $statuses = CRM_Contribute_BAO_Contribution::buildOptions('contribution_status_id', 'validate');

    // If we have a $0 amount, skip call to processor and set payment_status to Completed.
    if ($propertyBag->getAmount() == 0) {
      return [
        'payment_status' => 'Completed',
        'payment_status_id' => array_search('Completed', $statuses),
      ];
    }

    // Added via javascript so "removed" by quickform after submission
    $dayOfMonth = CRM_Utils_Request::retrieveValue('day_of_month', 'Integer', NULL, FALSE, 'POST');
    if ($dayOfMonth) {
      $propertyBag->setCustomProperty('day_of_month', $dayOfMonth);
    }

    // Ensure entryURL is set. It’s the page to come back to if things go wrong.
    if (empty($propertyBag->getter('entryURL', TRUE))) {
      $protocol = (
        preg_match('/^(?:on|1)$/i', $_SERVER['HTTPS'] ?? '')
        || (($_SERVER['HTTP_X_FORWARDED_PROTO'] ?? '') === 'https')
      ) ? 'https' : 'http';
      $propertyBag->setCustomProperty('entryURL', "$protocol://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]");
    }

    $url = $this->getBillingRequestFlowURL($propertyBag, $component);
    CRM_Utils_System::redirect($url);
  }

  /**
   *
   * Address, phone, email parameters provided by profiles have names like:
   *
   * - email-5 (e.g. 5 is the LocationType ID)
   * - email-Primary (Primary email was selected)
   *
   * We try to pick the billing location types if possible, after that we look
   * for Primary, after that we go with any given.
   *
   */
  public function chooseBestContactDetails(array $params): array {

    // We remove all addresses, emails from the $params array, and then re-insert the one we want to use.
    $addresses = [];
    $emails = [];
    $phones = [];
    foreach ($params as $inputKey => $value) {
      if (preg_match('/^(phone|email|street_address|supplemental_address_1|supplemental_address_2|city|postal_code|country|state_province)-(\d+|\w+)$/', $inputKey, $matches)) {

        [, $fieldname, $locType] = $matches;
        if ($fieldname === 'email') {
          $emails[$locType] = $value;
        }
        elseif ($fieldname === 'phone') {
          $phones[$locType] = $value;
        }
        else {
          // Index addresses by location type, field.
          $addresses[$locType][$fieldname] = $value;
        }
        unset($params[$inputKey]);
      }
    }

    $selectedAddress = [];
    // First preference is dedicated billing location types
    $billingLocTypeID = CRM_Core_BAO_LocationType::getBilling();
    if ($billingLocTypeID && isset($addresses[$billingLocTypeID])) {
      // Use this one.
      $selectedAddress = $addresses[$billingLocTypeID];
    }
    elseif (isset($addresses['Primary'])) {
      $selectedAddress = $addresses['Primary'];
    }
    elseif ($addresses) {
      $selectedAddress = reset($addresses);
    }

    foreach ([
      "street_address"         => 'billingStreetAddress',
      "supplemental_address_1" => 'billingSupplementalAddress1',
      "supplemental_address_2" => 'billingSupplementalAddress2',
      "supplemental_address_3" => 'billingSupplementalAddress3',
      "city"                   => 'billingCity',
      "postal_code"            => 'billingPostalCode',
      "country"                => 'billingCountryCode',
      "state_province"         => 'billingStateProvince',
    ] as $arrayKey => $propertyBagProp) {
      if (!empty($selectedAddress[$arrayKey])) {
        $params[$propertyBagProp] = $selectedAddress[$arrayKey];
      }
    }

    if ($billingLocTypeID && !empty($emails[$billingLocTypeID])) {
      $params['email'] = $emails[$billingLocTypeID];
    }
    elseif (!empty($emails['Primary'])) {
      $params['email'] = $emails['Primary'];
    }
    elseif ($emails) {
      $params['email'] = reset($emails);
    }

    if ($billingLocTypeID && !empty($phones[$billingLocTypeID])) {
      $params['phone'] = $phones[$billingLocTypeID];
    }
    elseif (!empty($phones['Primary'])) {
      $params['phone'] = $phones['Primary'];
    }
    elseif ($phones) {
      $params['phone'] = reset($phones);
    }

    return $params;
  }

  /**
   * Called when a billing request has completed.
   *
   * It is called by the buildForm hook for the contribution page thank you class
   * but may also be called by other code wishing to implement payments.
   */
  public function handleSuccessfulBillingRequest(string $billingRequestID, ?array $cr = NULL): array {
    // Validate input.
    if (empty($billingRequestID)) {
      throw new PaymentProcessorException("Missing billingRequestID, could not setup your Direct Debit.");
    }

    $worker = new \Civi\GoCardless\Events\BillingRequestsFulfilled($this, $billingRequestID);
    $worker->cr = $cr;

    // Catch the case that the BRQ is not yet fulfilled, but GoCardless are sorta giving the goahead.
    if (in_array($worker->brq->status, ['ready_to_fulfil', 'fulfilling'])) {
      $worker->ensureContributionRecurLoaded();
      $msg = "Billing request $billingRequestID is not 'fulfilled' yet, but GoCardless is saying it should be fine. Should complete async in a few minutes via webhook. No subscription set up yet.";
      // Update recur to In Progress because it looks like it will complete? Maybe not.
      // ContributionRecur::update(FALSE)
      //   ->addWhere('id', '=', $worker->cr['id'])
      //   ->addValue('contribution_status_id:name', 'In Progress')
      //   ->execute();
      $return = [
        'billingRequestsFulfilledWorker' => $worker,
        'billingRequest' => $worker->brq,
        'payment' => NULL,
        'subscription' => NULL,
        'contactID' => $worker->cr['contact_id'],
        'contributionRecurID' => $worker->cr['id'],
        'contributionID' => $worker->cnID,
      ];
    }
    else {
      try {
        $msg = $worker->process();
        // Try to return as many useful things as poss.
        $return = [
          'billingRequestsFulfilledWorker' => $worker,
          'billingRequest' => $worker->brq,
          'payment' => $worker->payment,
          'subscription' => $worker->subscription,
          'contactID' => $worker->cr['contact_id'],
          'contributionRecurID' => $worker->cr['id'],
          'contributionID' => $worker->cnID,
        ];
      }
      catch (WebhookException $e) {
        // We don't expect this.
        throw new PaymentProcessorException($e->getMessage());
      }
    }

    Civi::log('GoCardless')->info($msg, [
      'billingRequestID' => $billingRequestID,
      'subscriptionID' => $return['subscription'] ? $return['subscription']->id : NULL,
      'contactID' => $return['contactID'],
      'contributionRecurID' => $return['contributionRecurID'],
      'contributionID' => $return['contributionID'],
    ]);
    return $return;
  }

  /**
   * Processes the contribution page submission for doTransferCheckout.
   *
   * This is used by Core's QuickForm Contribution/Event Pages UX.
   *
   * @param \Civi\Payment\PropertyBag $params
   * @param string $component
   * @return string
   *   URL to redirect to.
   */
  public function getBillingRequestFlowURL(PropertyBag $params, string $component): string {
    try {
      // Create the billing request, store its ID on the ContributionRecur.
      $brq = $this->createBillingRequest($params);
      // By default set cycle_day to 0.
      $crUpdates = ['processor_id' => $brq->id, 'cycle_day' => 0];

      if ($params->has('day_of_month')) {
        // This is a Javascript hack added in to allow the user to select a cycle_day.
        // (I'm not sure why it's not a core thing, cycle day exists...)
        // Store this on the CR.
        $cycleDay = (int) $params->getCustomProperty('day_of_month');
        if ($cycleDay === -1) {
          // This means Last Day of Month. We can't store -1 in cycle_day though, so we store a special value.
          $cycleDay = self::CYCLE_DAY_FOR_LAST_DAY_OF_MONTH;
        }
        elseif ($cycleDay < 0 || $cycleDay > 28) {
          throw new PaymentProcessorException("Invalid day of month given: $cycleDay");
        }
        $crUpdates['cycle_day'] = $cycleDay;
      }

      // CiviCRM sets up 'weekly' memberships by using a 7 day interval, but GoCardless does not allow
      // daily-specified intervals.
      if ($params->getRecurFrequencyUnit() === 'day') {
        $interval = $params->getRecurFrequencyInterval();
        // Convert daily intervals to weekly if poss.
        if (($interval % 7) !== 0) {
          Civi::log('GoCardless')->error("Attempt to setup a GoCardless recur with $interval day interval. GoCardless does not support day-based intervals and this number is not divisible by 7 so cannot be converted to weeks.");
          throw new PaymentProcessorException("If using day intervals the frequency must be divisible by 7 (i.e. only full weeks supported.)");
        }
        // Convert to weeks.
        $interval /= 7;
        $params->setRecurFrequencyInterval($interval)->setRecurFrequencyUnit('week');
        $crUpdates['frequency_interval'] = $interval;
        $crUpdates['frequency_unit'] = 'week';
      }

      ContributionRecur::update(FALSE)
        ->setValues($crUpdates)
        ->addWhere('id', '=', $params->getContributionRecurID())
        ->execute();

      // The user should come back to the form's thank you page on our site after completing
      // the GoCardless offsite process.
      // If we have successUrl, cancelUrl set, these take precedence.
      if (!empty($this->successUrl)) {
        $firstSep = str_contains($this->successUrl, '?') ? '&' : '?';
        $redirect_uri = $this->successUrl . $firstSep . http_build_query([
          'cid' => $params->getContactID(),
          'brqID' => $brq->id,
          'crID' => $params->getContributionRecurID(),
        ]);
      }
      else {
        $redirect_uri = CRM_Utils_System::url(
          ($component == 'event') ? 'civicrm/event/register' : 'civicrm/contribute/transact',
          [
            '_qf_ThankYou_display' => 1,
            'qfKey' => $params->getCustomProperty('qfKey'),
            'cid' => $params->getContactID(),
            'brqID' => $brq->id,
            'crID' => $params->getContributionRecurID(),
          ],
          TRUE, NULL, FALSE);
      }

      $exit_uri = !empty($this->cancelUrl) ? $this->cancelUrl : htmlspecialchars_decode($params->getCustomProperty('entryURL'));

      $brf = $this->createBillingRequestFlow($params, $brq, compact('exit_uri', 'redirect_uri'));
      return $brf->authorisation_url;
    }
    catch (\Exception $e) {
      CRM_Core_Session::setStatus(E::ts('Sorry, there was an error contacting the payment processor GoCardless.'), E::ts("Error"), "error");
      Civi::log('GoCardless')->error(__METHOD__ . ' exception: ' . $e->getMessage() . "\n\n" . $e->getTraceAsString());
      return $params->getCustomProperty('entryURL');
    }
  }

  /**
   * Sets up a Billing Request with GoCardless.
   *
   * Note that withPayment is never used by Contribution Forms.
   *
   * @param \Civi\Payment\PropertyBag $params
   * @param int $withPayment
   *
   *
   * @return \GoCardlessPro\Resources\BillingRequest
   */
  public function createBillingRequest(PropertyBag $params, int $withPayment = self::NO_INSTANT_PAYMENT): BillingRequest {
    $params->require($withPayment > self::NO_INSTANT_PAYMENT ? ['description', 'amount'] : ['description']);

    // Create the billing request.
    // metadata:
    // > Key-value store of custom data. Up to 3 keys are permitted,
    // > with key names up to 50 characters and values up to 500 characters.
    // Note: values MUST be strings
    $billingRequestParams = [
      "mandate_request" => [
        "description" => $params->getDescription(),
        "currency" => "GBP",
      ],
      "metadata" => [
        'contactID' => (string) $params->getContactID(),
        'contributionRecurID' => (string) $params->getContributionRecurID(),
      ],
    ];
    if ($params->has('contributionID')) {
      $billingRequestParams['metadata']['contributionID'] = (string) $params->getContributionID();
    }

    if ($withPayment > self::NO_INSTANT_PAYMENT) {
      // Get amount in pennies and check it's positive.
      $amountInMinorUnits = Money::of($params->getAmount(), 'GBP')->getMinorAmount()->toInt();
      if (!($amountInMinorUnits > 0)) {
        throw new InvalidArgumentException("Zero amount in CRM_Core_Payment_GoCardless::createBillingRequest.");
      }
      if ($withPayment === self::INSTANT_PAYMENT_WITH_FALLBACK) {
        // "this billing request can fallback from instant payment to direct debit. Should not be set if GoCardless payment intelligence feature is used."
        $billingRequestParams["fallback_enabled"] = TRUE;
      }
      $billingRequestParams["payment_request"] = [
        "description" => $params->getDescription(),
        "amount" => $amountInMinorUnits,
        "currency" => "GBP",
      ];
    }

    // Now create the brq
    $gcApi = $this->getGoCardlessApi();
    $brq = $gcApi->billingRequests()->create(['params' => $billingRequestParams]);

    return $brq;
  }

  /**
   * Create a GoCardless BillingRequestFlow object using a BillingRequest, form values, etc.
   */
  public function createBillingRequestFlow(PropertyBag $params, BillingRequest $brq, array $brfParams = []): BillingRequestFlow {
    $brfParams["links"]["billing_request"] = $brq->id;
    foreach ([
      ['given_name', 'firstName', NULL],
      ['family_name', 'lastName', NULL],
      ['email', 'email', NULL],
      ['address_line1', 'billingStreetAddress', NULL],
      ['city', 'billingCity', NULL],
      ['postal_code', 'billingPostalCode', NULL],
      ['country_code', 'billingCountryCode', 'GB'],
    ] as [$gcKey, $ourKey, $default]) {
      $brfParams['prefilled_customer'][$gcKey] = $params->getter($ourKey, TRUE, $default);
    }
    // Combine our three optional supplemental address fields into GC address line 2.
    $brfParams['prefilled_customer']['address_line2'] = implode(", ", array_filter([
      $params->getter('billingSupplementalAddress1', TRUE),
      $params->getter('billingSupplementalAddress2', TRUE),
      $params->getter('billingSupplementalAddress3', TRUE),
    ]));
    $brfParams['prefilled_customer'] = array_filter($brfParams['prefilled_customer']);
    $gcApi = $this->getGoCardlessApi();
    $brf = $gcApi->billingRequestFlows()->create(['params' => $brfParams]);
    return $brf;
  }

  /**
   * This will be called by CiviCRM Core's IPN handler.
   *
   * Nb. Separation of concerns: this method deals with reading the http
   * request and emitting headers; the main logic is abstracted and does not
   * depend on an http environment, making it more accessible for tests.
   */
  public function handlePaymentNotification() {
    // We need to check the input against the test and live payment processors.
    $body = file_get_contents('php://input');
    if (!function_exists('getallheaders')) {
      // https://lab.civicrm.org/extensions/gocardless/-/issues/23
      // Some server configs do not provide getallheaders().
      // We only care about the Webhook-Signature header so try to extract that from $_SERVER.
      $headers = [];
      if (isset($_SERVER['HTTP_WEBHOOK_SIGNATURE'])) {
        $headers['Webhook-Signature'] = $_SERVER['HTTP_WEBHOOK_SIGNATURE'];
      }
    }
    else {
      $headers = getallheaders();
    }

    $handler = new CRM_Core_Payment_GoCardlessIPN($this);
    http_response_code($handler->handleRequest($headers, $body));
  }

  /**
   * Called by mjwshared extension's queue processor api3 Job.process_paymentprocessor_webhooks
   *
   * The array parameter contains a row of PaymentprocessorWebhook data, which represents a single GC event
   *
   * Return TRUE for success, FALSE if there's a problemh
   */
  public function processWebhookEvent(array $webhookEvent) :bool {
    // If there is another copy of this event in the table with a lower ID, then
    // this is a duplicate that should be ignored. We do not worry if there is one with a higher ID
    // because that means that while there are duplicates, we'll only process the one with the lowest ID.
    $duplicates = PaymentprocessorWebhook::get(FALSE)
    // Remove line when minversion>=5.29
      ->setCheckPermissions(FALSE)
      ->selectRowCount()
      ->addWhere('event_id', '=', $webhookEvent['event_id'])
      ->addWhere('id', '<', $webhookEvent['id'])
      ->execute()->count();
    if ($duplicates) {
      PaymentprocessorWebhook::update(FALSE)
      // Remove line when minversion>=5.29
        ->setCheckPermissions(FALSE)
        ->addWhere('id', '=', $webhookEvent['id'])
        ->addValue('status', 'error')
        ->addValue('message', 'Refusing to process this event as it is a duplicate.')
        ->execute();
      return FALSE;
    }

    $handler = new CRM_Core_Payment_GoCardlessIPN($this);
    $result = $handler->processQueuedWebhookEvent($webhookEvent);

    return $result;
  }

  /**
   * Returns a GoCardless API object for this payment processor.
   *
   * These are cached not because they are expensive to create, but to allow
   * testing.  Nb. this may be injected by setGoCardlessApi() for testing.
   *
   * @return \GoCardlessPro\Client
   */
  public function getGoCardlessApi() {
    $pp = $this->getPaymentProcessor();
    if (!isset(static::$gocardless_api[$pp['id']])) {
      $access_token = $pp['user_name'];
      $environment  = $pp['is_test'] ? \GoCardlessPro\Environment::SANDBOX : \GoCardlessPro\Environment::LIVE;
      // Allow forcing use of sandbox api endpoint. Only for development/testing.
      if (defined('CIVI_GOCARDLESS_FORCE_SANDBOX_ENVIRONMENT') && constant('CIVI_GOCARDLESS_FORCE_SANDBOX_ENVIRONMENT')) {
        $environment = \GoCardlessPro\Environment::SANDBOX;
      }
      CRM_GoCardlessUtils::loadLibraries();
      static::$gocardless_api[$pp['id']] = new \GoCardlessPro\Client(compact('access_token', 'environment'));
    }
    return static::$gocardless_api[$pp['id']];
  }

  /**
   * Returns a GoCardless API object for this payment processor.
   *
   * @param \GoCardlessPro\Client|null $mocked_api pass NULL to remove cache.
   */
  public function setGoCardlessApi($mocked_api) {
    $pp = $this->getPaymentProcessor();
    if ($mocked_api === NULL) {
      unset(static::$gocardless_api[$pp['id']]);
    }
    else {
      static::$gocardless_api[$pp['id']] = $mocked_api;
    }
  }

  /**
   * Shared code to handle extracting info from a gocardless exception.
   * @see https://github.com/gocardless/gocardless-pro-php/
   */
  protected function logGoCardlessException($prefix, $e) {
    Civi::log('GoCardless')->error("$prefix", [
      'message' => $e->getMessage(),
      'type' => $e->getType(),
      'errors' => $e->getErrors(),
      'requestId' => $e->getRequestId(),
      'documentationUrl' => $e->getDocumentationUrl(),
    ]);
  }

  /**
   * Are back office payments supported. No.
   *
   * @inheritdoc
   */
  protected function supportsBackOffice() {
    return FALSE;
  }

  /**
   * Can more than one transaction be processed at once? No.
   *
   * @inheritdoc
   */
  protected function supportsMultipleConcurrentPayments() {
    return FALSE;
  }

  /**
   * We do not need any fields because we send people off-site.
   *
   * @inheritdoc
   */
  public function getPaymentFormFields() {
    return [];
  }

  /**
   * Does this processor support cancelling recurring contributions through code.
   *
   * Since v1.9 we do because we now use processor_id instead of trxn_id to
   * match subscription_id. For a while we were waiting on
   * https://github.com/civicrm/civicrm-core/pull/15673
   * but decided just to start using processor_id anyway.
   *
   * @return bool
   */
  protected function supportsCancelRecurring() {
    return TRUE;
  }

  /**
   * Checks if back-office recurring edit is allowed
   *
   * @return bool
   */
  public function supportsEditRecurringContribution() {
    return TRUE;
  }

  /**
   * Does the processor support the user having a choice as to whether to cancel the recurring with the processor?
   *
   * If this returns TRUE then there will be an option to send a cancellation request in the cancellation form.
   *
   * @return bool
   */
  protected function supportsCancelRecurringNotifyOptional() {
    return TRUE;
  }

  /**
   * Shorthand method to determine if this processor is a test one.
   */
  public function isTestMode() {
    $pp = $this->getPaymentProcessor();
    return (bool) $pp['is_test'];
  }

  public function getText($context, $params) {
    $text = parent::getText($context, $params);

    switch ($context) {
      case 'cancelRecurDetailText':
        // $params['selfService'] added via https://github.com/civicrm/civicrm-core/pull/17687
        $params['selfService'] = $params['selfService'] ?? TRUE;
        if ($params['selfService']) {
          $text .= ' <br/><strong>' . E::ts('GoCardless will be automatically notified and the subscription will be cancelled.') . '</strong>';
        }
        else {
          $text .= ' <br/><strong>' . E::ts("If you select 'Send cancellation request..' then GoCardless will be automatically notified and the subscription will be cancelled.") . '</strong>';
        }
    }
    return $text;
  }

  /**
   * For GoCardless, our policy is that the Contribution's receive_date should be
   * the date the Payment is completed. Civi 5.29+ does not update the Contribution
   * receive_date when a payment comes in, which makes sense, but not what we want.
   * Calling Contribution.create to change the date has had some nasty side
   * effects in the wild, so to be safer we do a quick and dirty SQL update.
   *
   * This is called from PaymentConfirmed and a phpunit test.
   */
  public function setContributionReceiveDateBySQL(int $cnID, string $date) {
    $time = strtotime($date);
    if (!$time) {
      throw new \InvalidArgumentException("Cannot parse date: " . json_encode($date));
    }
    $sql = 'UPDATE civicrm_contribution SET receive_date="%2" WHERE id=%1';
    $sqlParams = [
      1 => [$cnID, 'Positive'],
      2 => [date('YmdHis', $time), 'Date'],
    ];
    CRM_Core_DAO::executeQuery($sql, $sqlParams);
  }

}
