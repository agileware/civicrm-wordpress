<?php
/**
 * Utils class for GoCardless integration.
 * @author Rich Lott / Artful Robot.
 */

use CRM_GoCardless_ExtensionUtil as E;
use Civi\Api4\ContributionRecur;
use Civi\Payment\Exception\PaymentProcessorException;

// In composer based installs, the vendor dir might not be installed here and our dependencies
// may be already met.
$gcVendorAutoload = dirname(__DIR__) . DIRECTORY_SEPARATOR . 'vendor' . DIRECTORY_SEPARATOR . 'autoload.php';
if (file_exists($gcVendorAutoload)) {
  require_once $gcVendorAutoload;
}
unset($gcVendorAutoload);

/**
 * Class CRM_GoCardlessUtils
 */
class CRM_GoCardlessUtils {
  const GC_TEST_SORT_CODE = '200000';
  const GC_TEST_ACCOUNT   = '55779911';
  /**
   * @var \GoCardlessPro\Clientormockwithtestcredentials*/
  protected static $api_test;
  /**
   * @var \GoCardlessPro\Clientormockwithlivecredentials*/
  protected static $api_live;

  /**
   * Returns a GoCardless API object.
   *
   * There's a singleton pattern here for each of live/test.
   *
   * @deprecated Please use the CRM_Core_Payment_GoCardless->getGoCardlessApi() method.
   *
   * @param bool $test Sandbox or Live API?
   * @return \GoCardlessPro\Client
   */
  public static function getApi($test = FALSE) {
    trigger_error("Calling CRM_GoCardlessUtils::getApi is deprecated as of v1.8. Instead you should load the CRM_Core_Payment_GoCardless payment processor object and call its getGoCardlessApi() method.", E_USER_DEPRECATED);
    if ($test && isset(static::$api_test)) {
      return static::$api_test;
    }
    if (!$test && isset(static::$api_live)) {
      return static::$api_live;
    }

    $pp = CRM_GoCardlessUtils::getPaymentProcessor($test);
    $access_token = $pp['user_name'];

    $client = new \GoCardlessPro\Client(array(
      'access_token' => $access_token,
      'environment'  => $test ? \GoCardlessPro\Environment::SANDBOX : \GoCardlessPro\Environment::LIVE,
    ));

    if ($test) {
      static::$api_test = $client;
    }
    else {
      static::$api_live = $client;
    }
    return $client;
  }

  /**
   * Do a PaymentProcessor:getsingle for the GoCardless processor type.
   *
   * @deprecated This pattern only works for sites that only have one GoCardless payment processor.
   *
   * @param bool $test Whether to find a test processor or a live one.
   */
  public static function getPaymentProcessor($test = FALSE) {
    trigger_error("Calling CRM_GoCardlessUtils::getPaymentProcessor is deprecated as of v1.8. Instead you should load the CRM_Core_Payment_GoCardless payment processor object using other CiviCRM native methods.", E_USER_DEPRECATED);
    // Find the credentials.
    $result = civicrm_api3('PaymentProcessor', 'getsingle',
      ['payment_processor_type_id' => "GoCardless", 'is_test' => (int) $test]);
    return $result;
  }

  /**
   * Sets the live or test GoCardlessPro API.
   *
   * This is useful so you can mock it for tests.
   *
   * @param bool $test
   * @param \GoCardlessPro\Client $api like object.
   * @return void
   */
  public static function setApi($test, \GoCardlessPro\Client $api) {
    if (!($api instanceof \GoCardlessPro\Client)) {
      throw new InvalidArgumentException("Object passed to CRM_GoCardlessUtils::setApi does not look like a GoCardlessPro\\Client");
    }
    if ($test) {
      static::$api_test = $api;
    }
    else {
      static::$api_live = $api;
    }
  }

  /**
   * Placeholder for sugar. Calling this will require this file and thereby the GC libraries.
   */
  public static function loadLibraries() {}

  /**
   * Get our specific settings.
   *
   * @return Array
   */
  public static function getSettings() {
    $json = Civi::settings()->get('gocardless', NULL);
    if ($json) {
      $storedSettings = json_decode($json, TRUE);
    }
    if (empty($storedSettings)) {
      $storedSettings = [];
    }

    // Validate stored settings, copy to $settings - see docs/reference/settings.md
    $settings = [];
    $warnings = [];

    // Check sendReceiptsForCustomPayments
    if (!preg_match('/^(never|always|defer)$/', $storedSettings['sendReceiptsForCustomPayments'] ?? '')) {
      $warnings[] = "Applying default sendReceiptsForCustomPayments policy (defer) as no valid value found.";
      $settings['sendReceiptsForCustomPayments'] = 'defer';
    }
    else {
      $settings['sendReceiptsForCustomPayments'] = $storedSettings['sendReceiptsForCustomPayments'];
    }

    // Check forceRecurring
    if (!is_bool($storedSettings['forceRecurring'] ?? NULL)) {
      $warnings[] = "Applying default forceRecurring policy (false) as no valid value found.";
      $settings['forceRecurring'] = FALSE;
    }
    else {
      $settings['forceRecurring'] = $storedSettings['forceRecurring'];
    }

    // Check daysOfMonth - we just check it's an array with at least one value.
    if (!is_array($storedSettings['daysOfMonth'] ?? NULL) || count($storedSettings['daysOfMonth']) === 0) {
      $warnings[] = "Applying default daysOfMonth policy (['0'] meaning any) as no valid value found.";
      $settings['daysOfMonth'] = ['0'];
    }
    else {
      $settings['daysOfMonth'] = $storedSettings['daysOfMonth'];
    }

    // Remove settings we've handled.
    foreach ($storedSettings as $key => $value) {
      if (!in_array($key, ['sendReceiptsForCustomPayments', 'forceRecurring', 'daysOfMonth'])) {
        $warnings[] = "Removed settings crud, key '$key'";
      }
    }

    if ($warnings) {
      Civi::log('GoCardless')->info(
        "CRM_GoCardlessUtils::getSettings has cleaned up the stored settings:\n" . json_encode([
          'oldStoredSettings' => $storedSettings,
          'newSettings' => $settings,
          'warnings' => $warnings,
        ], JSON_PRETTY_PRINT));

      Civi::settings()->set('gocardless', json_encode($settings));
    }

    return $settings;
  }

  /**
   * The thank you page is the page the billing request flow redirects to.
   *
   * We validate some stuff we need then pass on to the payment procesor.
   *
   * - call GC API to complete the mandate.
   * - find details of the contribution: how much, how often, day of month, 'name'
   * - set up a GC Subscription.
   * - set processor_id to the subscription ID in the contribution table.
   * - set status to "In Progress",
   * - set start_date
   * - if membership: set membership end date to start date + interval.
   *
   * This is called when we have a brqID on _GET
   */
  public static function handleContributeFormThanks() {
    $cr = ContributionRecur::get(FALSE)
      ->addWhere('id', '=', (int) ($_GET['crID'] ?? 0))
      ->execute()->single();

    // In order to trust the crID provided in the URL query, it must match the
    // brqID which is stored in the processor_id field.
    if ($cr['processor_id'] !== $_GET['brqID']) {
      // Either: spammer, or potentially a race condition if a webhook
      // processed this before we did? @todo
      Civi::log('GoCardless')->notice("handleContributeFormThanks did not find billing request {brqID} under recur {crID}", [
        ['brqID' => $_GET['brqID'], 'crID' => $_GET['crID'] ?? NULL],
      ]);
      return;
    }

    // Complete the billing flow with GC.
    try {
      /** @var \CRM_Core_Payment_GoCardless $pp */
      $pp = \Civi\Payment\System::singleton()->getById($cr['payment_processor_id']);
      $pp->handleSuccessfulBillingRequest($cr['processor_id'], $cr);
    }
    catch (Exception $e) {
      Civi::log()->error("Error completing GoCardless redirect flow. " . $e->getMessage(), ['exception' => $e]);
      throw new PaymentProcessorException('Sorry there was an error setting up your Direct Debit. Please contact us so we can look into what went wrong.');
    }
  }

  /**
   * Do our forceRecurring magic, if configured to do so.
   */
  public static function handleContributeFormHacks() {
    $settings = CRM_GoCardlessUtils::getSettings();
    if ($settings['forceRecurring']) {
      // Get GC processor IDs.
      $paymentProcessorsIDs = \Civi\Api4\PaymentProcessor::get(FALSE)
        ->addSelect('id')
        ->addWhere('payment_processor_type_id:name', '=', 'GoCardless')
        ->addWhere('is_active', '=', TRUE)
        ->addWhere('is_test', 'IS NOT NULL')
        ->execute()
        ->column('id');

      $js = file_get_contents(E::path('js/gcform.js'));
      $js = str_replace([
        'var goCardlessProcessorIDs = [];',
        'var dayOfMonthOptions = {};',
      ],
        [
          'var goCardlessProcessorIDs = ' . json_encode($paymentProcessorsIDs) . ';',
          'var dayOfMonthOptions = ' . json_encode(self::getDayOfMonthOptions()) . ';',
        ],
        $js
      );
      CRM_Core_Region::instance('page-body')->add(['markup' => "<script>$js</script>"]);
    }
  }

  /**
   * Ensure that if gocardless settings are fetched by api4, they are up-to-date.
   *
   */
  public static function hookApiPrepare($event) {
    static $preventLoop = FALSE;
    if ($preventLoop) {
      return;
    }
    if ('4.setting.get' !== $event->getApiRequestSig()) {
      return;
    }
    $apiRequest = $event->getApiRequest();
    $select = $apiRequest->getSelect();
    // No select means fetch ALL settings.
    if (empty($select) || in_array('gocardless', $select)) {
      // Ensure settings exist and are up-to-date.
      // Our getSettings() may call the api too, and in which case we do not
      // want an infinite loop.
      $preventLoop = TRUE;
      static::getSettings();
      $preventLoop = FALSE;
    }
  }

  /**
   * Get the formatted options for daysOfMonth eg. [1 => '1st']
   * This is used for frontend presentation and backend settings
   *
   * @param bool $all
   *
   * @return array
   */
  public static function getDayOfMonthOptions($all = FALSE) {
    if ($all) {
      $options = [0];
      for ($i = 1; $i <= 28; $i++) {
        // Add days 1 to 28 (29-31 are excluded because don't exist for some months)
        $options[] = $i;
      }
      $options[] = -1;
    }
    else {
      $options = static::getSettings()['daysOfMonth'];
    }

    foreach ($options as $option) {
      switch ($option) {
        case 0:
          $dayOfMonthOptions[$option] = 'auto';
          break;

        case -1:
          $dayOfMonthOptions[$option] = 'last';
          break;

        default:
          $dayOfMonthOptions[$option] = self::formatPreferredCollectionDay($option);
      }
    }
    return $dayOfMonthOptions;
  }

  /**
   * Format collection day like 1st, 2nd, 3rd, 4th etc.
   *
   * @param $collectionDay
   *
   * @return string
   */
  public static function formatPreferredCollectionDay($collectionDay) {
    $ends = ['th', 'st', 'nd', 'rd', 'th', 'th', 'th', 'th', 'th', 'th'];
    if ((($collectionDay % 100) >= 11) && (($collectionDay % 100) <= 13)) {
      $abbreviation = $collectionDay . 'th';
    }
    else {
      $abbreviation = $collectionDay . $ends[$collectionDay % 10];
    }

    return $abbreviation;
  }

  public static function getCache() {
    return CRM_Utils_Cache::create(['type' => ['SqlGroup'], 'name' => 'gocardless']);
  }

  public static function recordLegacyIPN() {
    static::getCache()->set('legacyIPN', date('Y-m-d'));
  }

  public static function getLegacyIPNInUse() {
    return static::getCache()->get('legacyIPN', NULL);
  }

}
