<?php

use Civi\Test\HeadlessInterface;
use Civi\Test\TransactionalInterface;
use Civi\GoCardless\Exceptions\Base as WebhookException;
use Civi\Api4\PaymentprocessorWebhook;
use Civi\Api4\ContributionRecur;
use Civi\Api4\Contribution;
use Civi\Api4\Note;
use Civi\Payment\PropertyBag;
use CRM_GoCardless_ExtensionUtil as E;

/**
 * Tests the GoCardless direct debit extension.
 *
 * Tips:
 *  - With HookInterface, you may implement CiviCRM hooks directly in the test class.
 *    Simply create corresponding functions (e.g. "hook_civicrm_post(...)" or similar).
 *  - With TransactionalInterface, any data changes made by setUp() or test****() functions will
 *    rollback automatically -- as long as you don't manipulate schema or truncate tables.
 *    If this test needs to manipulate schema or truncate tables, then either:
 *       a. Do all that using setupHeadless() and Civi\Test.
 *       b. Disable TransactionalInterface, and handle all setup/teardown yourself.
 *
 * @group headless
 */
class GoCardlessTest extends PHPUnit\Framework\TestCase implements HeadlessInterface, TransactionalInterface {

  protected $defaultDate = '2021-01-01';
  protected $defaultDateSubscriptionStarts = '2021-01-08';

  /**
   * Holds a map of name to value for contributionrecur statuses
   *
   * @var array
   */

  protected $contribution_recur_status_map;

  /**
   * Holds a map of name to value for contribution statuses
   * @var array
   */
  protected $contribution_status_map;

  /**
   * Holds a map of name to value for membership statuses
   * @var array
   */
  protected $membership_status_map;

  protected $membership_status_map_flip;

  protected $hookWasCalled = FALSE;
  protected $contactID = NULL;

  /**
   * Holds test mode payment processor.
   * @var array
   */
  public $test_mode_payment_processor;

  public function setUpHeadless() {
    // Civi\Test has many helpers, like install(), uninstall(), sql(), and sqlFile().
    // See: https://github.com/civicrm/org.civicrm.testapalooza/blob/master/civi-test.md
    // confusingly, unset the following 'reInstallOnce' var, so the install only happens once.
    // Set it to TRUE if you DON'T want to reinstall
    static $reInstallOnce = TRUE;

    $reInstall = FALSE;
    if (!isset($reInstallOnce)) {
      $reInstallOnce = TRUE;
      $reInstall = TRUE;
    }
    return \Civi\Test::headless()
      ->install(['taggedlog', 'mjwshared'])
      ->installMe(__DIR__)
      ->apply($reInstall);
  }

  public function setUp() :void {
    parent::setUp();

    // Set up a Payment Processor that uses GC.
    $this->test_mode_payment_processor = \Civi\Api4\PaymentProcessor::create(FALSE)
      ->setValues([
        'payment_processor_type_id:name' => "GoCardless",
        'name' => "GoCardless",
        'description' => "Set up by test script",
        'signature' => "mock_webhook_key",
        'is_active' => 1,
        'is_test' => 1,
        'url_api' => 'https://api-sandbox.gocardless.com/',
        'user_name' => "fake_test_api_key",
        'payment_instrument_id:name' => "direct_debit_gc",
        'domain_id' => 1,
      ])
      ->execute()->first();

    // We need a live one, too, even though we don't use it.
    \Civi\Api4\PaymentProcessor::create(FALSE)
      ->setValues([
        'payment_processor_type_id:name' => "GoCardless",
        'name' => "GoCardless",
        'signature' => "this is no the webhook key you are looking fo",
        'description' => "Set up by test script",
        'is_active' => 1,
        'url_api' => 'https://api.gocardless.com/',
        'is_test' => 0,
        'user_name' => "fake_live_api_key",
        'payment_instrument_id:name' => "direct_debit_gc",
        'domain_id' => 1,
      ])->execute();

    // Map contribution statuses to values.
    $this->contribution_recur_status_map = array_flip(CRM_Contribute_BAO_ContributionRecur::buildOptions('contribution_status_id', 'validate'));
    $this->contribution_status_map = array_flip(CRM_Contribute_BAO_Contribution::buildOptions('contribution_status_id', 'validate'));

    // Create a membership type
    $result = civicrm_api3('MembershipType', 'create', [
      'member_of_contact_id' => 1,
      'financial_type_id' => "Member Dues",
      'duration_unit' => "year",
      'duration_interval' => 1,
      'period_type' => "rolling",
      'name' => "MyMembershipType",
      'minimum_fee' => 1,
      'auto_renew' => 1,
    ]);

    $this->membership_status_map_flip = CRM_Member_PseudoConstant::membershipstatus();
    $this->membership_status_map = array_flip($this->membership_status_map_flip);

  }

  /**
   * Test 2 historical quirks in Civi
   *
   * 5.26: Payment.create did not set trxn_id on Contrib.
   * 5.27: Payment.create DID set trxn_id on Contrib. but receive_date is wrong.
   * 5.28.4: Payment.create sets both receive_date and trxn_id :-)
   * 5.29.1+: Payment.create sets trxn_id on Contrib, but does not update receive_date.
   *
   * Payment.create did not used to set the trxn_id apparently.
   */
  public function testPaymentCreate() {
    $contactID = $this->createTestContact();

    // Crete a Pending contrib.
    $contribution = [
      'total_amount'           => '1',
      'receive_date'           => '2021-01-01 09:09:09',
      'financial_type_id'      => 1,
      //'trxn_id'                => 'orig_trxn_id',
      'contact_id'             => $contactID,
      'contribution_status_id' => 'Pending',
      'is_email_receipt'       => FALSE,
    ];
    $createdID = civicrm_api3('Contribution', 'create', $contribution)['id'];

    // Record payment
    civicrm_api3('Payment', 'create', [
      'contribution_id'                   => $createdID,
      'total_amount'                      => '1',
      'is_send_contribution_notification' => FALSE,
      // Updated date
      'trxn_date'                         => '2021-02-02 10:10:10',
      // Updated ID
      'trxn_id'                           => 'my_trxn_id',
    ]);

    // We expect that the receive_date has not changed after adding the payment
    // (but it has done so in older versions of Civi, so we're guarding against that changing again!)
    $contrib = civicrm_api3('Contribution', 'get', ['id' => $createdID])['values'][$createdID];
    $this->assertEquals('2021-01-01 09:09:09', $contrib['receive_date'], 'Payment changed the receive_date, which is unexpected');

    $pp = $this->getPaymentProcessorObject();
    $pp->setContributionReceiveDateBySQL($createdID, '2021-02-02T10:10:10Z');

    // Fetch the contrib again.
    $contrib = civicrm_api3('Contribution', 'get', ['id' => $createdID])['values'][$createdID];
    // Test that Civi updated trxn_id - it should from 5.27, so this tests for regressions.
    $this->assertEquals('my_trxn_id', $contrib['trxn_id'] ?? NULL);
    // Test that the SQL updated the date correctly. Bit superfluous really.
    $this->assertEquals('2021-02-02 10:10:10', $contrib['receive_date'], 'SQL failed to change the receive_date');

  }

  /**
   * Test that the GC Apis are used to create the billing request + flow
   * and that the BRQ ID is stored on the cr.processor_id. Also tests our
   * bespoke use of cycle_day.
   */
  public function testGetBillingRequestFlowURL() {
    list($contactID, $recur, $contrib, $membership) = $this->createFixture([], FALSE);

    $this->mockGoCardlessApiForGetBRFUrl();

    // Call the thing we're testing with test params.
    $pb = new PropertyBag();
    // $pb->setCustomProperty('day_of_month', 1);
    $pb->setCustomProperty('qfKey', 'mockKey');
    $pb->setCustomProperty('entryURL', 'https://example.org/');
    $pb->setDescription('Support CiviCRM');
    $pb->setRecurFrequencyInterval(1);
    $pb->setRecurFrequencyUnit('month');
    $pb->setContributionRecurID($recur['id']);
    $pb->setContributionID($contrib['id']);
    $pb->setContactID($contactID);

    $pp = $this->getPaymentProcessorObject();
    $url = $pp->getBillingRequestFlowURL($pb, 'contribute');
    $this->assertEquals('https://brq.example.com', $url);

    // Test that the recur was updated.
    $recurNow = ContributionRecur::get(FALSE)
      ->addWhere('id', '=', $recur['id'])
      ->addSelect('processor_id', 'cycle_day', 'contribution_status_id:name')
      ->execute()->single();
    $this->assertEquals(
      [
        'id' => $recur['id'],
        'processor_id' => 'BRQ00001',
        'cycle_day' => 0,
        'contribution_status_id:name' => 'Pending',
      ],
      $recurNow
    );
  }

  /**
   * Test that the various APIs are used to create subscription
   * after a successful billing request flow.
   *
   * The cr should have the subscription ID, be In Progress,
   * and to have a new start date provided by the API
   *
   * @dataProvider dataForTestHandleSuccessfulBillingRequest
   */
  public function testHandleSuccessfulBillingRequest(bool $withNewMembership) {
    // Create the fixtures
    // $dateFormSubmitted = '2021-01-01';
    $dateSubscriptionStarts = date('Y-m-d', strtotime('today + 7 days'));
    $fixtureOverrides = [
      'recur' => ['processor_id' => 'BRQ00001', 'cycle_day' => 0],
      // 'order' => ['receive_date' => $dateFormSubmitted],
    ];
    if ($withNewMembership) {
      $fixtureOverrides['membership'] = [];
    }
    list($contactID, $recur, $contrib, $membership) = $this->createFixture($fixtureOverrides, FALSE);

    // Mock API.
    $this->mockGoCardlessApiForSuccessfulBRF($dateSubscriptionStarts, $contactID, $recur, $contrib['id']);

    // Thing we want to test.
    $pp = $this->getPaymentProcessorObject();
    $pp->handleSuccessfulBillingRequest('BRQ00001', $recur);

    // We expect the recur to have the subscription ID, and to be In Progress, and to have the new start date.
    $recurNow = ContributionRecur::get(FALSE)
      ->addSelect('DATE(start_date) AS start_date_1', 'processor_id', 'contribution_status_id:name', 'cycle_day')
      ->addWhere('id', '=', $recur['id'])->execute()->first();
    $this->assertEquals([
      'id' => $recur['id'],
      'start_date_1' => "$dateSubscriptionStarts",
      'cycle_day' => (int) substr($dateSubscriptionStarts, -2),
      'processor_id' => 'SUBSCRIPTION_ID',
      'contribution_status_id:name' => 'In Progress',
    ], $recurNow);

    // We expect the cn to be the same except for its receive_date
    $cnNow = Contribution::get(FALSE)
      ->addSelect('contribution_status_id:name', 'DATE(receive_date) AS date')
      ->addWhere('id', '=', $contrib['id'])
      ->execute()->single();
    $this->assertEquals([
      'id' => $contrib['id'],
      'contribution_status_id:name' => 'Pending',
      'date' => $dateSubscriptionStarts,
    ], $cnNow);

    if ($withNewMembership) {
      $result = civicrm_api3('Membership', 'getsingle', ['id' => $membership['id']]);
      // status should be still be Pending
      $this->assertEquals($this->membership_status_map["Pending"], $result['status_id']);
      // Dates should be unchanged
      foreach (['start_date', 'end_date', 'join_date'] as $date) {
        $this->assertEquals($membership[$date] ?? NULL, $result[$date] ?? NULL);
      }
    }
  }

  /**
   * Test that a billing request flow that ends successful but with a brq not yet fulfilled
   * does what we want.
   *
   * The cr should have the BRQID and be Pending still
   *
   * @dataProvider dataForTestHandleSuccessfulBillingRequestNotFulfilled
   */
  public function testHandleSuccessfulBillingRequestNotFulfilled(string $status) {
    // Create the fixtures
    // $dateFormSubmitted = '2021-01-01';
    $dateSubscriptionStarts = date('Y-m-d', strtotime('today + 7 days'));
    $fixtureOverrides = ['recur' => ['processor_id' => 'BRQ00001', 'cycle_day' => 0]];
    list($contactID, $recur, $contrib, $membership) = $this->createFixture($fixtureOverrides, FALSE);

    // Mock API.
    $this->mockGoCardlessApiForSuccessfulBRF($dateSubscriptionStarts, $contactID, $recur, $contrib['id'], $status);

    // Thing we want to test.
    $pp = $this->getPaymentProcessorObject();
    $pp->handleSuccessfulBillingRequest('BRQ00001', $recur);

    // We expect the recur to have the brq ID, and to be Pending, and to have the new start date.
    $recurNow = ContributionRecur::get(FALSE)
      ->addSelect('DATE(start_date) AS start_date_1', 'processor_id', 'contribution_status_id:name')
      ->addWhere('id', '=', $recur['id'])->execute()->first();
    $this->assertEquals([
      'id' => $recur['id'],
      'start_date_1' => $this->defaultDate,
      'processor_id' => 'BRQ00001',
      'contribution_status_id:name' => 'Pending',
    ], $recurNow);

    // We expect the cn to be the same except for its receive_date
    $cnNow = Contribution::get(FALSE)
      ->addSelect('contribution_status_id:name', 'DATE(receive_date) AS date')
      ->addWhere('id', '=', $contrib['id'])
      ->execute()->single();
    $this->assertEquals([
      'id' => $contrib['id'],
      'contribution_status_id:name' => 'Pending',
      'date' => $this->defaultDate,
    ], $cnNow);
  }

  public function dataForTestHandleSuccessfulBillingRequestNotFulfilled(): array {
    return [['ready_to_fulfil'], ['fulfilling']];
  }

  public function dataForTestHandleSuccessfulBillingRequest(): array {
    return [[FALSE], [TRUE]];
  }

  /**
   * Setting up a new cr to pay for an existing membership
   * should not affect the membership dates or status since
   * no payment has yet come in. The only change should be
   * that the membership now points to the new recur.
   *
   * @dataProvider dataForTestRenewingExistingMembership
   */
  public function testRenewingExistingMembership(string $type) {

    // Setup fixture: create contact and existing membership
    $contactID = $this->createTestContact();
    $dt = new DateTimeImmutable();
    if ($type === 'Grace') {
      // Renewing after the end of a membership.
      $membershipParamsOverride = [
        'join_date'  => $dt->modify("-25 months")->format("Y-m-d"),
        'start_date' => $dt->modify("-13 months")->format("Y-m-d"),
        'end_date'   => $dt->modify("-1 months")->format("Y-m-d"),
      ];
      list($contrib, $membership) = $this->createExistingMembershipNotPaidByGC($contactID, $membershipParamsOverride, $type);
      // Force the status to grace with this hack. (I don't know why the dates don't cause it to be grace already)
      civicrm_api3('Membership', 'create', [
        'id' => $membership['id'],
        'status_id' => 'Grace',
        'skipStatusCal' => 1,
      ]);
      $membership = civicrm_api3('Membership', 'getsingle', ['id' => $membership['id']]);
      $this->assertMembershipStatus($membership, 'Grace');
    }
    else {
      // Renewing a membership that is current (New).
      $membershipParamsOverride = [
        'join_date'  => $dt->modify("-1 months")->format("Y-m-d"),
        'start_date' => $dt->modify("-1 months")->format("Y-m-d"),
        'end_date'   => $dt->modify("-1 months + 1 year - 1 day")->format("Y-m-d"),
      ];
      list($contrib, $membership) = $this->createExistingMembershipNotPaidByGC($contactID, $membershipParamsOverride, $type);
      $this->assertMembershipStatus($membership, 'New');
    }
    $origMembership = $membership;

    // Setup fixture: Simulate a contribution page used to renew the membership using GC having been submitted,
    // but before the user has returned from the Billing Request flow.
    $dateFormSubmitted = '2021-01-01';
    $fixtureOverrides = [
      'recur' => [
        'start_date' => $dateFormSubmitted,
        'processor_id' => 'BRQ00001',
        'cycle_day' => 0,
      ],
      'order' => ['receive_date' => $dateFormSubmitted],
      // Point to existing membership by providing the 'id' field in the entity params.
      // Ensure we are not setting dates or statuses (the NULLs are removed by the createFixture call)
      'membership' => [
        'id'         => $membership['id'],
        'status_id'  => NULL,
        'join_date'  => NULL,
        'start_date' => NULL,
        'end_date'   => NULL,
      ],
    ];
    list($contactID, $recur, $contrib, $membership) = $this->createFixture($fixtureOverrides, FALSE);
    $this->assertEquals($origMembership['id'], $membership['id'], 'Expected membership ID to be the same, but looks like a new membership was created.');

    // Membership status should be still be as it was
    $this->assertMembershipStatus($membership, $type);

    // Mock the GC API to return a billing request flow ID and a URL.
    $this->mockGoCardlessApiForSuccessfulBRF($membershipParamsOverride['start_date'], $contactID, $recur, $contrib['id']);

    // ---------------
    // Call the thing we want to test
    $pp = $this->getPaymentProcessorObject();
    $pp->handleSuccessfulBillingRequest('BRQ00001', $recur);

    // -------------
    // Test it
    $membershipUpdated = civicrm_api3('Membership', 'getsingle', ['id' => $membership['id']]);

    // Dates should be unchanged
    foreach (['start_date', 'end_date', 'join_date'] as $date) {
      $this->assertEquals($membership[$date], $membershipUpdated[$date]);
    }

    // Membership status should be still be what it was
    $this->assertMembershipStatus($membershipUpdated, $type);
  }

  /**
   * Rather heavily overladen DRY code.
   *
   * $overrides may contain keys 'recur', 'order', 'membership' with array
   * values of overrides to the defaults below.
   *
   * If the overrides contains a 'membership' key at all then a membership is included.
   *
   * If $completePayment is TRUE then a payment will be added, which should
   * bump the statuses (Contrib, Membership) accordingly.
   *
   * @return array with the following things in order:
   *   - contactID
   *   - recur
   *   - contrib
   *   - membership
   */
  protected function createFixture(?Array $overrides = NULL, ?bool $completePayment = FALSE) {
    $overrides = $overrides ?? [];
    $handlingMembership = (array_key_exists('membership', $overrides));
    foreach ($overrides as $key => $val) {
      if (!in_array($key, ['order', 'recur', 'membership'])) {
        $this->assertTrue(FALSE, "FIX THIS, got key $key");
      }
    }

    $return = [
      'contactID' => $this->createTestContact(),
      'recur' => [],
      'contrib' => [],
      'membership' => [],
    ];
    $contactID = $return['contactID'];

    $return['recur'] = $this->createContribRecur($overrides['recur'] ?? []);
    // Reload with getsingle for consistency.
    $return['recur'] = civicrm_api3('ContributionRecur', 'getsingle', ['id' => $return['recur']['id']]);

    if ($handlingMembership) {
      // array_filter is used so you can remove default items via override,
      // e.g. passing [status_id] => NULL will remove that.
      $entityParams = array_filter(($overrides['membership'] ?? []) + [
        'membership_type_id' => 'MyMembershipType',
        'contact_id' => $contactID,
        'contribution_recur_id' => $return['recur']['id'],
        'join_date' => $this->defaultDate,
        'start_date' => $this->defaultDate,

        // Needed to override default status calculation
        'skipStatusCal' => 1,
        'status_id' => 'Pending',
      ]);
      $lineItemOverrides = [
        // Nb. the Order API seems to look for this being in the line items, but surely it should be in the params?
        // Duplicating it here in case it's needed.
        'membership_type_id' => 'MyMembershipType',
        'entity_table' => 'civicrm_membership',
      ];
    }
    else {
      $lineItemOverrides = $entityParams = [];
    }

    $orderCreateParams = ($overrides['order'] ?? []) + [
      'sequential' => 1,
        // Donation
      'financial_type_id' => 1,
      'total_amount' => 1,
      'source' => 'SOURCE_1',
      'receive_date' => $this->defaultDate,
      'contact_id' => $contactID,
      'contribution_recur_id' => $return['recur']['id'],
      'contribution_status_id' => "Pending",
      'is_test' => 1,
      'line_items' => [
          [
            'params' => $entityParams,
            'line_item' => [$lineItemOverrides + [
              'line_total' => 1,
              'unit_price' => 1,
              "price_field_id" => 1,
    // xxx ?
              "price_field_value_id" => 1,
              'financial_type_id' => 1, /* 'Donation' */
              'qty' => 1,
            ],
            ],
          ],
      ],
    ];
    $contrib = civicrm_api3('Order', 'create', $orderCreateParams);
    //print json_encode($orderCreateParams, JSON_PRETTY_PRINT);
    $return['contrib'] = civicrm_api3('Order', 'getsingle', ['id' => $contrib['id']]);
    //print json_encode($return['contrib'], JSON_PRETTY_PRINT);

    // Assert receive_date
    $this->assertEquals($orderCreateParams['receive_date'], substr($return['contrib']['receive_date'], 0, 10),
      "receive_date we got back was not the receive date we passed to order.create");

    if ($handlingMembership) {
      // Fetch the membership
      $return['membership'] = civicrm_api3('Membership', 'getsingle', ['id' => $return['contrib']['line_items'][0]['entity_id']]);
      // Assert start_date
      if (!empty($orderCreateParams['line_items'][0]['params']['start_date'])) {
        $this->assertEquals($orderCreateParams['line_items'][0]['params']['start_date'],
          $return['membership']['start_date'],
          "membership start_date we got back was not the date we passed to order.create's line items params");
      }
    }

    if ($completePayment) {
      // We need to complete the payment.
      civicrm_api3('Payment', 'create', [
        'contribution_id' => $return['contrib']['id'],
        'total_amount' => $return['contrib']['total_amount'],
        'trxn_date' => $return['contrib']['receive_date'],
        'trxn_id' => 'PAYMENT_ID',
        'is_send_contribution_notification' => 0,
      ]);

      // Reload.
      $return['contrib'] = civicrm_api3('Order', 'getsingle', ['id' => $contrib['id']]);
      if ($handlingMembership) {
        $return['membership'] = civicrm_api3('Membership', 'getsingle', ['id' => $return['membership']['id']]);
      }
    }

    return array_values($return);
  }

  /**
   * Calculates hmac signature and prepares a mock GC API to return the payment details.
   *
   * @return string the calculated signature
   */
  protected function mockGoCardlessApiForWebhook(string $webhookBodyJSON, string $paymentGetJSON, string $paymentID = 'PAYMENT_ID') :string {

    // Mock the GC API.
    $payment = $this->createMock(\GoCardlessPro\Resources\Payment::class);
    $tuples = [];
    foreach (json_decode($paymentGetJSON, TRUE) as $k => $v) {
      $tuples[] = [$k, is_array($v) ? (object) $v : $v];
    }
    $payment->method('__get')->willReturnMap($tuples);

    $paymentsService = $this->createMock(\GoCardlessPro\Services\PaymentsService::class);
    $paymentsService
      ->expects($this->once())
      ->method('get')
      ->with($paymentID)
      ->willReturn($payment);

    $apiMock = $this->createMock(\GoCardlessPro\Client::class);
    $apiMock->method('payments')->willReturn($paymentsService);

    $this->mockGoCardlessApiForTestPaymentProcessor($apiMock);

    $calculatedSignature = hash_hmac("sha256", $webhookBodyJSON, 'mock_webhook_key');
    return $calculatedSignature;
  }

  /**
   * Calculates hmac signature and prepares a mock GC API to return the subscription details.
   *
   * @return string the calculated signature
   */
  protected function mockGoCardlessApiForWebhookSubscription(string $webhookBodyJSON, string $subscriptionGetJSON) :string {

    $subscriptionService = $this->createMock(\GoCardlessPro\Services\SubscriptionsService::class);
    $subscriptionService
      ->expects($this->once())
      ->method('get')
      ->with('SUBSCRIPTION_ID')
      ->willReturn(json_decode($subscriptionGetJSON));

    $apiMock = $this->createMock(\GoCardlessPro\Client::class);
    $apiMock->method('subscriptions')->willReturn($subscriptionService);

    $this->mockGoCardlessApiForTestPaymentProcessor($apiMock);

    $calculatedSignature = hash_hmac("sha256", $webhookBodyJSON, 'mock_webhook_key');
    return $calculatedSignature;
  }

  public function dataForTestRenewingExistingMembership() {
    return [['New'], ['Grace']];
  }

  /**
   * Check missing signature throws InvalidArgumentException.
   */
  public function testWebhookMissingSignature() {
    $this->expectException(InvalidArgumentException::class);
    $this->expectExceptionMessage("Unsigned API request.");
    $controller = new CRM_Core_Payment_GoCardlessIPN();
    $controller->parseWebhookRequest([], '');
  }

  /**
   * Check wrong signature throws InvalidArgumentException.
   *
   */
  public function testWebhookWrongSignature() {
    $this->expectException(InvalidArgumentException::class);
    $this->expectExceptionMessage("Invalid signature in request.");
    $controller = new CRM_Core_Payment_GoCardlessIPN();
    $controller->parseWebhookRequest(["Webhook-Signature" => 'foo'], 'bar');
  }

  /**
   * Check events extracted from webhook.
   *
   */
  public function testWebhookParse() {
    $controller = new CRM_Core_Payment_GoCardlessIPN();
    $body = '{"events":[
      {"id":"EV1","resource_type":"payments","action":"confirmed"},
      {"id":"EV2","resource_type":"payments","action":"failed"},
      {"id":"EV3","resource_type":"payments","action":"something we do not handle"},
      {"id":"EV4","resource_type":"subscriptions","action":"cancelled"},
      {"id":"EV5","resource_type":"subscriptions","action":"finished"},
      {"id":"EV6","resource_type":"subscriptions","action":"something we do not handle"},
      {"id":"EV7","resource_type":"unhandled_resource","action":"foo"}
      ]}';
    $calculated_signature = hash_hmac("sha256", $body, 'mock_webhook_key');
    $controller->parseWebhookRequest(["Webhook-Signature" => $calculated_signature], $body);

    $this->assertIsArray($controller->events);
    foreach (['EV1', 'EV2', 'EV4', 'EV5'] as $event_id) {
      $this->assertArrayHasKey($event_id, $controller->events);
    }
    $this->assertCount(4, $controller->events);
  }

  /**
   * A payment confirmation should update the initial Pending Contribution.
   *
   * @dataProvider withOrWithoutTemplate
   */
  public function testWebhookPaymentConfirmationFirst(bool $withTemplateContribution) {
    // when webhook called
    $dt = new DateTimeImmutable();
    $today = $dt->format("Y-m-d");
    $charge_date = $dt->modify("-2 days")->format("Y-m-d");

    $fixtureOverrides = [
      'recur' => [
        'start_date' => $charge_date,
        'contribution_status_id' => "In Progress",
        'processor_id' => 'SUBSCRIPTION_ID', /* simulate a complete flow */
      ],
      'order' => ['receive_date' => $charge_date],
      'membership' => [
        'status_id' => NULL,
        'start_date' => NULL,
        'end_date' => NULL,
        'join_date' => NULL,
      ],
    ];
    list($contactID, $recur, $contrib, $membership) = $this->createFixture($fixtureOverrides, FALSE);

    if ($withTemplateContribution) {
      CRM_Contribute_BAO_ContributionRecur::ensureTemplateContributionExists($recur['id']);
    }

    // Mock webhook input data.
    $body = '{"events":[
      {"id":"EV1","resource_type":"payments","action":"confirmed","links":{"payment":"PAYMENT_ID"}}
      ]}';
    $calculatedSignature = $this->mockGoCardlessApiForWebhook($body, <<<JSON
      {
        "id":"PAYMENT_ID",
        "status":"confirmed",
        "charge_date":"$charge_date",
        "amount":100,
        "links":{"subscription":"SUBSCRIPTION_ID"}
      }
      JSON);

    // Now trigger webhook.
    $controller = new CRM_Core_Payment_GoCardlessIPN();
    $controller->parseWebhookRequest(["Webhook-Signature" => $calculatedSignature], $body);
    $controller->processWebhookEvents(TRUE);

    // Now check the changes have been made.
    $contrib = civicrm_api3('Contribution', 'getsingle', ['id' => $contrib['id']]);
    $this->assertEquals($charge_date . ' 00:00:00', $contrib['receive_date']);
    $this->assertEquals('PAYMENT_ID', $contrib['trxn_id']);
    $this->assertContributionStatus($contrib, 'Completed');
    // Nb. this is an edge case:
    $this->assertEquals(1, $contrib['total_amount']);

    $membershipUpdated = civicrm_api3('Membership', 'getsingle', ['id' => $membership['id']]);
    // status should be updated to New
    $this->assertEquals($this->membership_status_map["New"], $membershipUpdated['status_id'], "Wanted status New, got status " . CRM_Member_PseudoConstant::membershipstatus()[$membership['status_id']]);
    // join_date should be unchanged
    $this->assertEquals($membership['join_date'] ?? NULL, $membershipUpdated['join_date'] ?? NULL);

    // start_date set to setup date (i.e. probably? unchanged)
    // The original test expected this to be today's date, but while testing with 5.19.1
    // this was not the case. As those changes happen outside of this payment processor
    // I decided to go with what core now does...
    // 5.40: now it seems to be today's date. This assertion is only here so I can track
    // changes in what core does, so I have updated the assertion to today.
    $this->assertEquals($today, $membershipUpdated['start_date']);
    // end_date updated
    $this->assertEquals((new DateTimeImmutable($today))->modify("+1 year")->modify("-1 day")->format("Y-m-d"), $membershipUpdated['end_date']);

    // Check the recur record has been updated.
    $recur = civicrm_api3('ContributionRecur', 'getsingle', ['id' => $recur['id']]);
    $this->assertContributionRecurStatus($recur, 'In Progress');
  }

  public function withOrWithoutTemplate() {
    return [
      'with template' => [TRUE],
      'without template' => [FALSE],
    ];
  }

  /**
   * A payment confirmation should create a new contribution.
   *
   * Fixture:
   * - Create a recurring payment with 1 *completed* payment 1 year ago
   *
   * @dataProvider withOrWithoutTemplate
   */
  public function testWebhookPaymentConfirmationSubsequent(bool $withTemplateContribution) {
    // when webhook called
    $dt = new DateTimeImmutable();
    $first_date_string = $dt->modify('-1 year')->format('Y-m-d');
    $second_charge_date = $dt->format('Y-m-d');

    list($contactID, $recur, $contrib, $membership) = $this->createFixture([
      'recur' => [
        'contribution_status_id' => "In Progress",
        'start_date' => $first_date_string,
        'processor_id' => 'SUBSCRIPTION_ID',
      ],
      'order' => [
        'receive_date' => $first_date_string,
      ],
      'membership' => [
        'start_date' => $first_date_string,
        'join_date' => $first_date_string,
      ],
    ], TRUE);

    // Check membership start_date
    $this->assertEquals($first_date_string, $membership['start_date'],
      "The membership start_date is not what we expected; not the date we passed in.");
    // Check end date is as expected.
    $this->assertEquals((new DateTimeImmutable($first_date_string))->modify("+1 year")->modify("-1 day")->format("Y-m-d"), $membership['end_date'],
      "The membership end_date is not as expected"
    );

    // Mock webhook input data.
    $body = '{"events":[
      {"id":"EV1","resource_type":"payments","action":"confirmed","links":{"payment":"PAYMENT_ID_2"}}
      ]}';
    $calculatedSignature = $this->mockGoCardlessApiForWebhook($body, '{
        "id":"PAYMENT_ID_2",
        "status":"confirmed",
        "charge_date": "' . $second_charge_date . '",
        "amount":100,
        "links":{"subscription":"SUBSCRIPTION_ID"}
        }', 'PAYMENT_ID_2');

    if ($withTemplateContribution) {
      CRM_Contribute_BAO_ContributionRecur::ensureTemplateContributionExists($recur['id']);
    }

    //
    // Mocks, done, now onto the code we are testing: trigger webhook.
    //
    $controller = new CRM_Core_Payment_GoCardlessIPN();
    $controller->parseWebhookRequest(["Webhook-Signature" => $calculatedSignature], $body);
    $controller->processWebhookEvents(TRUE);

    //
    // Now check the changes have been made.
    //
    $result = civicrm_api3('Contribution', 'get', [
      'contribution_recur_id' => $recur['id'],
      'is_test' => 1,
    ]);
    // Should be 2 records now.
    $this->assertEquals(2, $result['count']);
    // Ensure we have the first one.
    $this->assertArrayHasKey($contrib['id'], $result['values']);
    // Now we can get rid of it.
    unset($result['values'][$contrib['id']]);
    // And the remaining one should be our new one.
    $contrib = reset($result['values']);

    $this->assertEquals("$second_charge_date 00:00:00", $contrib['receive_date']);
    $this->assertEquals(1, $contrib['total_amount']);
    $this->assertEquals('PAYMENT_ID_2', $contrib['trxn_id']);
    $this->assertArrayHasKey('contribution_source', $contrib, 'Expected to find `contribution_source` in subsequent contribution, but didn’t.');
    $this->assertEquals('SOURCE_1', $contrib['contribution_source']);
    $this->assertEquals($this->contribution_status_map['Completed'], $contrib['contribution_status_id']);

    // Check membership
    $result = civicrm_api3('Membership', 'getsingle', ['id' => $membership['id']]);
    $this->assertEquals($this->membership_status_map["Current"], $result['status_id']);
    // join_date and start_date are unchanged
    $this->assertEquals($membership['join_date'], $result['join_date']);
    $this->assertEquals($membership['start_date'], $result['start_date']);
    // end_date is 12 months later
    $end_dt = new DateTimeImmutable($membership['end_date']);
    $this->assertEquals($end_dt->modify("+12 months")->format("Y-m-d"), $result['end_date']);
  }

  /**
   * A payment confirmation should not change a recur status from Cancelled to In Progress.
   * See Issue 54
   *
   */
  public function testWebhookPaymentConfirmationDoesNotMarkCancelledAsInProgress() {

    // First, create a contact who has had a membership with one completed
    // payment, that is now cancelled (meaning: the ContributionRecur is cancelled).

    $dt = new DateTimeImmutable();
    $first_date_string = $dt->modify('-1 year')->format('Y-m-d');
    $second_charge_date = $dt->format('Y-m-d');
    list($contactID, $recur, $contrib, $membership) = $this->createFixture([
      'recur' => [
        'start_date' => $first_date_string,
        'contribution_status_id' => "Cancelled", /* Look! */
        'processor_id' => 'SUBSCRIPTION_ID',
      ],
      'order' => [
        'receive_date' => $first_date_string,
      ],
      'membership' => [
        'start_date' => $dt->modify("-11 months")->format("Y-m-d"),
        'join_date' => $dt->modify("-23 months")->format("Y-m-d"),
      ],
    ], TRUE /* complete the contribution */);
    $this->assertContributionRecurStatus($recur, 'Cancelled');

    // Force membership onto Current, then reload it.
    civicrm_api3('Membership', 'create', [
      'id' => $membership['id'],
      'status_id' => "Current",
      'skipStatusCal' => 1,
    ]);
    $membership = civicrm_api3('Membership', 'getsingle', ['id' => $membership['id']]);

    // Mock webhook input data.
    $body = '{"events":[
      {"id":"EV1","resource_type":"payments","action":"confirmed","links":{"payment":"PAYMENT_ID_2"}}
      ]}';
    $calculatedSignature = $this->mockGoCardlessApiForWebhook($body, '{
        "id":"PAYMENT_ID_2",
        "status":"confirmed",
        "charge_date": "' . $second_charge_date . '",
        "amount":100,
        "links":{"subscription":"SUBSCRIPTION_ID"}
        }', 'PAYMENT_ID_2');

    //
    // Mocks, done, now onto the code we are testing: trigger webhook.
    //
    $controller = new CRM_Core_Payment_GoCardlessIPN();
    $controller->parseWebhookRequest(["Webhook-Signature" => $calculatedSignature], $body);
    $controller->processWebhookEvents(TRUE);

    //
    // Now check the changes have been made.
    //
    $result = civicrm_api3('Contribution', 'get', [
      'contribution_recur_id' => $recur['id'],
      'is_test' => 1,
    ]);
    // Should be 2 records now.
    $this->assertEquals(2, $result['count']);
    // Ensure we have the first one.
    $this->assertArrayHasKey($contrib['id'], $result['values']);
    // Now we can get rid of it.
    unset($result['values'][$contrib['id']]);
    // And the remaining one should be our new one.
    $contrib = reset($result['values']);

    // Check the recur status is still cancelled.
    $result = civicrm_api3('ContributionRecur', 'getsingle', ['id' => $contrib['contribution_recur_id']]);
    $contrib_recur_statuses = array_flip($this->contribution_recur_status_map);
    $this->assertEquals($contrib_recur_statuses[$result['contribution_status_id']], 'Cancelled',
      'Expected the contrib recur record to STILL have status Cancelled after a successful contribution received.');
  }

  /**
   * A payment failed should update the initial Pending Contribution.
   *
   * Also, if a successful payment comes in at a later date, that should work normally.
   */
  public function testWebhookPaymentFailedFirst() {

    $fixtureOverrides = [
      'recur' => [
        'processor_id' => 'SUBSCRIPTION_ID', /* simulate a complete flow */
        'contribution_status_id' => 'In Progress',
      ],
      'order' => [],
    ];
    list($contactID, $recur, $contrib, $membership) = $this->createFixture($fixtureOverrides, FALSE);

    // Mock webhook input data.
    $body = '{"events":[
      {"id":"EV1","resource_type":"payments","action":"failed","links":{"payment":"PAYMENT_ID"}}
      ]}';
    $calculatedSignature = $this->mockGoCardlessApiForWebhook($body, '{
        "id":"PAYMENT_ID",
          "status":"failed",
          "charge_date":"2016-10-02",
          "amount":100,
          "links":{"subscription":"SUBSCRIPTION_ID"}
        }'
    );

    // -------------------------------------------------------
    // What we're testing: Trigger webhook.
    // -------------------------------------------------------
    $controller = new CRM_Core_Payment_GoCardlessIPN();
    $controller->parseWebhookRequest(["Webhook-Signature" => $calculatedSignature], $body);
    $controller->processWebhookEvents(TRUE);

    // -------------------------------------------------------
    // Test: the initial contrib should show as failed.
    $contrib = civicrm_api3('Contribution', 'getsingle', ['id' => $contrib['id']]);
    $this->assertEquals('2016-10-02 00:00:00', $contrib['receive_date']);
    $this->assertEquals(1, $contrib['total_amount']);
    $this->assertEquals('PAYMENT_ID', $contrib['trxn_id']);
    $this->assertContributionStatus($contrib, 'Failed');

    // Test: the contrib recur should be 'overdue'
    $recur = civicrm_api3('ContributionRecur', 'getsingle', ['id' => $recur['id']]);
    $this->assertContributionRecurStatus($recur, 'Overdue');

    // -------------------------------------------------------
    // Mock a second, failed contribution.
    // -------------------------------------------------------
    $body = '{"events":[
      {"id":"EV2","resource_type":"payments","action":"failed","links":{"payment":"PAYMENT_ID_2"}}
      ]}';
    $calculatedSignature = $this->mockGoCardlessApiForWebhook($body, '{
        "id":"PAYMENT_ID_2",
          "status":"failed",
          "charge_date":"2016-11-02",
          "amount":100,
          "links":{"subscription":"SUBSCRIPTION_ID"}
        }', 'PAYMENT_ID_2');

    // -------------------------------------------------------
    // What we're testing: Trigger webhook.
    // -------------------------------------------------------
    $controller = new CRM_Core_Payment_GoCardlessIPN();
    $controller->parseWebhookRequest(["Webhook-Signature" => $calculatedSignature], $body);
    $controller->processWebhookEvents(TRUE);

    // -------------------------------------------------------
    // Test: Original (now failed) payment is unaffected
    $contrib = civicrm_api3('Contribution', 'getsingle', ['id' => $contrib['id']]);
    $this->assertEquals('2016-10-02 00:00:00', $contrib['receive_date']);
    $this->assertEquals(1, $contrib['total_amount']);
    $this->assertEquals('PAYMENT_ID', $contrib['trxn_id']);
    $this->assertContributionStatus($contrib, 'Failed');

    // -------------------------------------------------------
    // Test: Should be a new, failed payment
    $contrib = $this->getLatestContribution($recur['id']);
    $this->assertEquals('2016-11-02 00:00:00', $contrib['receive_date']);
    $this->assertEquals(1, $contrib['total_amount']);
    $this->assertEquals('PAYMENT_ID_2', $contrib['trxn_id']);
    $this->assertContributionStatus($contrib, 'Failed');

    // -------------------------------------------------------
    // Mock a third, and our first successful ('confirmed'), contribution.
    // @see https://lab.civicrm.org/extensions/gocardless/-/issues/82
    // -------------------------------------------------------
    $body = '{"events":[
      {"id":"EV3","resource_type":"payments","action":"confirmed","links":{"payment":"PAYMENT_ID_3"}}
      ]}';
    $calculatedSignature = $this->mockGoCardlessApiForWebhook($body, '{
        "id":"PAYMENT_ID_3",
        "status":"confirmed",
        "charge_date":"2016-12-02",
        "amount":100,
        "links":{"subscription":"SUBSCRIPTION_ID"}
      }', 'PAYMENT_ID_3');

    // -------------------------------------------------------
    // What we're testing: Trigger webhook.
    // -------------------------------------------------------
    $controller = new CRM_Core_Payment_GoCardlessIPN();
    $controller->parseWebhookRequest(["Webhook-Signature" => $calculatedSignature], $body);
    $controller->processWebhookEvents(TRUE);

    // -------------------------------------------------------
    // Test: Should be a new, successful payment
    $contrib = $this->getLatestContribution($recur['id']);
    $this->assertEquals('2016-12-02 00:00:00', $contrib['receive_date']);
    $this->assertEquals(1, $contrib['total_amount']);
    $this->assertEquals('PAYMENT_ID_3', $contrib['trxn_id']);
    $this->assertContributionStatus($contrib, 'Completed');

    // -------------------------------------------------------
    // Test: the contrib recur should be In Progress again.
    $recur = civicrm_api3('ContributionRecur', 'getsingle', ['id' => $recur['id']]);
    $this->assertContributionRecurStatus($recur, 'In Progress');

  }

  /**
   * Failed payment on cancelled subscription
   */
  public function testWebhookPaymentFailedOnCancelledSubscription() {

    $fixtureOverrides = [
      'recur' => [
        'processor_id' => 'SUBSCRIPTION_ID', /* simulate a complete flow */
        'contribution_status_id' => "Cancelled", /* Look! */
        'end_date' => 'now',
        'cancel_date' => 'now',
      ],
      'order' => [],
    ];
    list($contactID, $recur, $contrib, $membership) = $this->createFixture($fixtureOverrides, FALSE);

    // Mock webhook input data.
    $body = '{"events":[
      {"id":"EV1","resource_type":"payments","action":"failed","links":{"payment":"PAYMENT_ID"}}
      ]}';
    $calculatedSignature = $this->mockGoCardlessApiForWebhook($body, '{
        "id":"PAYMENT_ID",
          "status":"failed",
          "charge_date":"2016-10-02",
          "amount":100,
          "links":{"subscription":"SUBSCRIPTION_ID"}
        }'
    );

    // -------------------------------------------------------
    // What we're testing: Trigger webhook.
    // -------------------------------------------------------
    $controller = new CRM_Core_Payment_GoCardlessIPN();
    $controller->parseWebhookRequest(["Webhook-Signature" => $calculatedSignature], $body);
    $controller->processWebhookEvents(TRUE);

    // -------------------------------------------------------
    // Test: the initial contrib should show as failed.
    $contrib = civicrm_api3('Contribution', 'getsingle', ['id' => $contrib['id']]);
    $this->assertEquals('2016-10-02 00:00:00', $contrib['receive_date']);
    $this->assertEquals(1, $contrib['total_amount']);
    $this->assertEquals('PAYMENT_ID', $contrib['trxn_id']);
    $this->assertContributionStatus($contrib, 'Failed');

    // Test: the contrib recur should (still) be Cancelled
    $recur = civicrm_api3('ContributionRecur', 'getsingle', ['id' => $recur['id']]);
    $this->assertContributionRecurStatus($recur, 'Cancelled');
  }

  /**
   * A payment confirmation should create a new contribution.
   *
   * @dataProvider withOrWithoutTemplate
   */
  public function testWebhookPaymentFailedSubsequent($withTemplateContribution) {

    // Fixture: completed flow with one payment complete
    $fixtureOverrides = [
      'recur' => [
        'processor_id' => 'SUBSCRIPTION_ID',
        'contribution_status_id' => "In Progress",
      ],
    ];
    list($contactID, $recur, $contrib, $membership) = $this->createFixture($fixtureOverrides, TRUE);
    if ($withTemplateContribution) {
      CRM_Contribute_BAO_ContributionRecur::ensureTemplateContributionExists($recur['id']);
    }

    // Mock webhook input data.
    $body = '{"events":[
      {"id":"EV1","resource_type":"payments","action":"failed","links":{"payment":"PAYMENT_ID_2"}}
      ]}';
    $calculatedSignature = $this->mockGoCardlessApiForWebhook($body, '{
        "id":"PAYMENT_ID_2",
        "status":"failed",
        "charge_date":"2016-10-02",
        "amount":100,
        "links":{"subscription":"SUBSCRIPTION_ID"}
      }', 'PAYMENT_ID_2');

    // Now trigger webhook.
    $controller = new CRM_Core_Payment_GoCardlessIPN();
    $controller->parseWebhookRequest(["Webhook-Signature" => $calculatedSignature], $body);
    $controller->processWebhookEvents(TRUE);

    // Now check the changes have been made.
    $result = civicrm_api3('Contribution', 'get', [
      'contribution_recur_id' => $recur['id'],
      'is_test' => 1,
    ]);
    // Should be 2 records now.
    $this->assertEquals(2, $result['count']);
    // Ensure we have the first one.
    $this->assertArrayHasKey($contrib['id'], $result['values']);
    // Now we can get rid of it.
    unset($result['values'][$contrib['id']]);
    // And the remaining one should be our new one.
    $contrib = reset($result['values']);

    $this->assertEquals('2016-10-02 00:00:00', $contrib['receive_date']);
    $this->assertEquals(1, $contrib['total_amount']);
    $this->assertEquals('PAYMENT_ID_2', $contrib['trxn_id']);
    $this->assertContributionStatus($contrib, 'Failed');

  }

  /**
   * Late Payments.
   * See Issue 55
   */
  public function testWebhookPaymentFailedLate() {

    // Fixture: completed flow with one payment complete
    $fixtureOverrides = [
      'recur' => [
        'processor_id' => 'SUBSCRIPTION_ID',
        'contribution_status_id' => "In Progress",
      ],
    ];
    list($contactID, $recur, $contrib, $membership) = $this->createFixture($fixtureOverrides, TRUE);

    // Now force the CR to cancelled.
    $recur = civicrm_api3('ContributionRecur', 'create', array(
      'id' => $recur['id'],
      'contribution_status_id' => "Cancelled",
    ));

    // Mock webhook input data.
    $body = '{"events":[
      {"id":"EV1","resource_type":"payments","action":"failed","links":{"payment":"PAYMENT_ID"}}
      ]}';
    $calculatedSignature = $this->mockGoCardlessApiForWebhook($body, '{
        "id":"PAYMENT_ID",
        "status":"failed",
        "charge_date":"' . $this->defaultDateSubscriptionStarts . '",
        "amount":100,
        "links":{"subscription":"SUBSCRIPTION_ID"}
        }');

    // Now trigger webhook.
    $controller = new CRM_Core_Payment_GoCardlessIPN();
    $controller->parseWebhookRequest(["Webhook-Signature" => $calculatedSignature], $body);
    $controller->processWebhookEvents(TRUE);

    // Now check the changes have been made.
    $contrib = civicrm_api3('Contribution', 'getsingle', ['id' => $contrib['id']]);
    $this->assertContributionStatus($contrib, 'Refunded');
    $this->assertEquals('PAYMENT_ID', $contrib['trxn_id']);
  }

  /**
   * A payment confirmation webhook that is out of date.
   *
   */
  public function testWebhookOutOfDate() {

    $this->expectException(WebhookException::class);
    $this->expectExceptionMessage("OK: Webhook out of date, expected status confirmed, got 'cancelled'");

    $this->mockGoCardlessApiForWebhook('', '{
        "id":"PAYMENT_ID",
        "status":"cancelled",
        "charge_date":"2016-10-02",
        "amount":100,
        "links":{"subscription":"SUBSCRIPTION_ID"}
      }');

    // Now trigger webhook.
    $event = json_decode(json_encode(['links' => ['payment' => 'PAYMENT_ID']]));
    // Calling with different status to that which will be fetched from API.
    $controller = $this->getWebhookControllerForTestProcessor();
    $controller->getAndCheckGoCardlessPayment($event, ['confirmed']);
  }

  /**
   * A subscription cancelled webhook that is out of date.
   *
   */
  public function testWebhookOutOfDateSubscription() {

    $this->expectException(WebhookException::class);
    $this->expectExceptionMessage("OK: Skipping this event as it is out of date, expected subscription status 'complete', got 'cancelled'");

    $this->mockGoCardlessApiForWebhookSubscription('',
     '{"status": "cancelled", "id": "SUBSCRIPTION_ID"}'
    );

    $event = json_decode('{"links":{"subscription":"SUBSCRIPTION_ID"}}');
    $controller = $this->getWebhookControllerForTestProcessor();
    // Calling with different status to that which will be fetched from API.
    $controller->getAndCheckSubscription($event, 'complete');
  }

  /**
   * A payment confirmation webhook event that does not have a subscription
   * should be ignored.
   *
   * Note that coverage wise, this test also tests that exceptions are handled in the processWebhookEvents, processWebhookEvent methods.
   */
  public function testWebhookPaymentWithoutSubscriptionIgnored() {

    $this->mockGoCardlessApiForWebhook('', '{
        "id":"PAYMENT_ID",
          "status":"confirmed",
          "charge_date":"2016-10-02",
          "amount":100,
          "links":{}
        }');

    // Now trigger webhook.
    $event = json_decode(json_encode(['id' => 'EV1', 'links' => ['payment' => 'PAYMENT_ID'], 'resource_type' => 'payments', 'action' => 'confirmed']));
    // Calling with different status to that which will be fetched from API.
    $controller = $this->getWebhookControllerForTestProcessor();
    $controller->events = [$event];
    // Test the inner method
    $result = $controller->processWebhookEvent($event);
    $this->assertEquals('OK: Ignoring non-subscription payment that we don\'t know.', $result->message);
    $this->assertEquals(TRUE, $result->ok);
    $this->assertInstanceOf(WebhookException::class, $result->exception);
    // Test the loop method, no throw (execution should continue)
    $this->mockGoCardlessApiForWebhook('', '{
        "id":"PAYMENT_ID",
          "status":"confirmed",
          "charge_date":"2016-10-02",
          "amount":100,
          "links":{}
        }');
    $controller->processWebhookEvents(FALSE);
    $this->assertTrue(TRUE);
    // Test the throw way
    $this->mockGoCardlessApiForWebhook('', '{
        "id":"PAYMENT_ID",
          "status":"confirmed",
          "charge_date":"2016-10-02",
          "amount":100,
          "links":{}
        }');
    $this->expectException(WebhookException::class);
    $this->expectExceptionMessage("OK: Ignoring non-subscription payment that we don't know.");
    $controller->processWebhookEvents(TRUE);
  }

  /**
   * A subscription cancelled should update the recurring contribution record
   * and a Pending Contribution.
   *
   * @dataProvider dataForTestWebhookSubscriptionEnded
   *
   */
  public function testWebhookSubscriptionEnded($event, $expectedStatus) {

    $finishDate = date('Y-m-d', strtotime($this->defaultDateSubscriptionStarts . ' + 3 month'));

    $fixtureOverrides = [
      'recur' => [
        'processor_id' => 'SUBSCRIPTION_ID', /* simulate a complete flow */
        'contribution_status_id' => 'In Progress',
      ],
      'order' => [],
    ];
    list($contactID, $recur, $contrib, $membership) = $this->createFixture($fixtureOverrides, FALSE);

    // Mock webhook input data.
    $body = '{"events":[
      {"id":"EV1","resource_type":"subscriptions","action":"' . $event . '","links":{"subscription":"SUBSCRIPTION_ID"}}
      ]}';
    $calculatedSignature = $this->mockGoCardlessApiForWebhookSubscription($body, '{
        "id":"SUBSCRIPTION_ID",
        "status":"' . $event . '",
        "end_date":"' . $finishDate . '"
      }');

    // Now trigger webhook.
    $controller = new CRM_Core_Payment_GoCardlessIPN();
    $controller->parseWebhookRequest(["Webhook-Signature" => $calculatedSignature], $body);
    $controller->processWebhookEvents(TRUE);

    // Now check the changes have been made to the original contribution.
    $contrib = civicrm_api3('Contribution', 'getsingle', [
      'contribution_recur_id' => $recur['id'],
      'is_test' => 1,
    ]);
    $this->assertContributionStatus($contrib, 'Cancelled');

    // Now check the changes have been made to the recurring contribution.
    $recur = civicrm_api3('ContributionRecur', 'getsingle', ['id' => $recur['id']]);
    // The end_date always gets set.
    $this->assertEquals("$finishDate 00:00:00", $recur['end_date']);
    // The cancel date should only be set for cancellations.
    if ($event === 'cancelled') {
      $this->assertEquals("$finishDate 00:00:00", $recur['cancel_date']);
    }
    else {
      $this->assertNull($recur['cancel_date'] ?? NULL);
    }

    $this->assertContributionRecurStatus($recur, $expectedStatus);
  }

  public function dataForTestWebhookSubscriptionEnded() {
    return [
      ['cancelled', 'Cancelled'],
      ['finished', 'Completed'],
    ];
  }

  /**
   * A payment confirmation should update the initial Pending Contribution.
   *
   * This is the same test as testWebhookPaymentConfirmationFirst except that it
   * uses the queue.
   */
  public function testWebhookQueueSupport() {
    // when webhook called
    $dt = new DateTimeImmutable();
    $today = $dt->format("Y-m-d");
    $charge_date = $dt->modify("-2 days")->format("Y-m-d");

    $fixtureOverrides = [
      'recur' => [
        'start_date' => $charge_date,
        'processor_id' => 'SUBSCRIPTION_ID', /* simulate a complete flow */
        'contribution_status_id' => 'In Progress',
      ],
      'order' => ['receive_date' => $charge_date],
      'membership' => [
        'status_id' => NULL,
        'start_date' => NULL,
        'end_date' => NULL,
        'join_date' => NULL,
      ],
    ];
    list($contactID, $recur, $contrib, $membership) = $this->createFixture($fixtureOverrides, FALSE);

    // Mock webhook input data.
    $body = '{"meta": {"webhook_id": "WEBHOOK_ID"}, "events":[
      {"id":"EV1","resource_type":"payments","action":"confirmed","links":{"payment":"PAYMENT_ID"}}
      ]}';
    $calculatedSignature = $this->mockGoCardlessApiForWebhook($body, '{
        "id":"PAYMENT_ID",
        "status":"confirmed",
        "charge_date":"' . $charge_date . '",
        "amount":100,
        "links":{"subscription":"SUBSCRIPTION_ID"}
      }');

    // Now trigger webhook via handleRequest().
    $controller = $this->getWebhookControllerForTestProcessor();
    $result = $controller->handleRequest(["Webhook-Signature" => $calculatedSignature], $body);
    $this->assertEquals(204, $result);

    // Load the queue and inspect.
    $queueItems = PaymentprocessorWebhook::get(FALSE)->execute()->getArrayCopy();
    $this->assertEquals(1, count($queueItems), "Expected that the event was queued as PaymentprocessorWebhook");

    // Re-send the webhook so we have a duplicate.
    $controller = $this->getWebhookControllerForTestProcessor();
    $result = $controller->handleRequest(["Webhook-Signature" => $calculatedSignature], $body);
    $this->assertEquals(204, $result);

    // Now run the queue
    $result = civicrm_api3('Job', 'process_paymentprocessor_webhooks', []);

    // Load the queue and inspect.
    $queueItems = PaymentprocessorWebhook::get(FALSE)->addOrderBy('id')->execute()->getArrayCopy();
    $this->assertEquals(2, count($queueItems), "Expected 2 queue items - the original and the duplicate.");
    $this->assertEquals('success', $queueItems[0]['status'], "Expected first queue item to have succeeded.");
    $this->assertStringStartsWith("OK: PaymentsConfirmed completed. Recur:", $queueItems[0]['message'], "Expected second queue item to have errored.");
    $this->assertEquals('error', $queueItems[1]['status'], "Expected second queue item to have errored.");
    $this->assertEquals('Refusing to process this event as it is a duplicate.', $queueItems[1]['message'], "Expected second queue item to have errored.");

    // Now check the changes have been made.
    $contrib = civicrm_api3('Contribution', 'getsingle', ['id' => $contrib['id']]);
    $this->assertEquals($charge_date . ' 00:00:00', $contrib['receive_date']);
    $this->assertEquals('PAYMENT_ID', $contrib['trxn_id']);
    $this->assertContributionStatus($contrib, 'Completed');
    // Nb. this is an edge case:
    $this->assertEquals(1, $contrib['total_amount']);

    $membershipUpdated = civicrm_api3('Membership', 'getsingle', ['id' => $membership['id']]);
    // status should be updated to New
    $this->assertEquals($this->membership_status_map["New"], $membershipUpdated['status_id'], "Wanted status New, got status " . CRM_Member_PseudoConstant::membershipstatus()[$membership['status_id']]);
    // join_date should be unchanged
    $this->assertEquals($membership['join_date'] ?? NULL, $membershipUpdated['join_date'] ?? NULL);

    // start_date set to setup date (i.e. probably? unchanged)
    // The original test expected this to be today's date, but while testing with 5.19.1
    // this was not the case. As those changes happen outside of this payment processor
    // I decided to go with what core now does...
    // 5.40: now it seems to be today's date. This assertion is only here so I can track
    // changes in what core does, so I have updated the assertion to today.
    $this->assertEquals($today, $membershipUpdated['start_date']);
    // end_date updated
    $this->assertEquals((new DateTimeImmutable($today))->modify("+1 year")->modify("-1 day")->format("Y-m-d"), $membershipUpdated['end_date']);

    // Check the recur record has been updated.
    $recur = civicrm_api3('ContributionRecur', 'getsingle', ['id' => $recur['id']]);
    $this->assertContributionRecurStatus($recur, 'In Progress');
  }

  /**
   * Test some weird cases.
   */
  public function testGetOriginalContribution() {
    /** @var \Civi\TaggedLog\Logger */
    $logger = Civi::log('GoCardless');

    file_put_contents($logger->logFile, '');
    $this->createTestContact();
    // Test broken recurs.
    // Create a recur with no contributions.
    $recur = \Civi\Api4\ContributionRecur::create(FALSE)
      ->setValues([
        'amount' => 1,
        'financial_type_id' => 1,
        'contact_id' => $this->contactID,
      ])->execute()->first();

    $controller = $this->getWebhookControllerForTestProcessor();
    $result = $controller->getOriginalContribution($recur, FALSE);
    $this->assertEquals(['_was' => 'not_found'], $result);
    $this->assertLogContains('FAILED: Did not find any Contributions at all for ContributionRecur');
    // clear log
    file_put_contents($logger->logFile, '');

    // Test that getOriginalContribution fails if a recur with one failed contrib
    // exists and we tell it not to create a template.
    // Create a failed contrib.
    $failedContrib = \Civi\Api4\Contribution::create(FALSE)
      ->setValues([
        'contribution_recur_id' => $recur['id'],
        'contribution_status_id:name' => 'Failed',
        'receive_date' => 'today',
        'total_amount' => 1,
        'financial_type_id' => 1,
        'contact_id' => $this->contactID,
      ])->execute()->first();
    $result = $controller->getOriginalContribution($recur, FALSE);
    $this->assertEquals(['_was' => 'not_found'], $result);
    $this->assertLogContains('UNEXPECTED: Did not find any completed or template Contributions to use.');
    // clear log
    file_put_contents($logger->logFile, '');

    // Test that getOriginalContribution returns template.
    $result = $controller->getOriginalContribution($recur, TRUE);
    $this->assertEquals('found_template', $result['_was']);
    $this->assertNotEquals($failedContrib['id'], $result['id'], "Expect template contribution to have different ID to failed Contribution");
    $this->assertLogContains('Did not find any completed or template Contributions but found a non-completed Contribution. Trying to create a template now for ContributionRecur');
    $this->assertLogContains('Template created');
  }

  public function testGetContributionRecurFromSubscriptionId() {
    $controller = $this->getWebhookControllerForTestProcessor();
    try {
      $controller->getContributionRecurFromSubscriptionId('');
      $this->fail("Expected WebhookException");
    }
    catch (WebhookException $e) {
      $this->assertEquals('ERROR: No subscription_id data passed into getContributionRecurFromSubscriptionId', $e->getMessage());
    }
    try {
      $controller->getContributionRecurFromSubscriptionId('doesnotexist');
      $this->fail("Expected WebhookException");
    }
    catch (WebhookException $e) {
      $this->assertEquals('ERROR: No matching recurring contribution record for processor_id doesnotexist', $e->getMessage());
    }
  }

  /**
   * According to issue 59, CiviCRM sets up 'weekly' memberships by passing
   * in a 7 day interval, which GC does not allow.
   *
   * This creates a contact with a contribution and a ContributionRecur in the
   * same way that CiviCRM's core Contribution Pages form does, then, having
   * mocked the GC API, it calls getBillingRequestFlowURL()
   * and checks that the result is updated contribution and ContributionRecur records.
   *
   */
  public function testIssue59() {
    // We need to mimick what the contribution page does, which AFAICS does:
    list($contactID, $recur, $contrib, $membership) = $this->createFixture([
      'recur' => [
        // Issue59 specific settings
        'frequency_unit' => "day",
        'frequency_interval' => 7,
      ],
      'membership' => [],
    ]);

    $this->mockGoCardlessApiForGetBRFUrl();

    // Call the thing we're testing with test params.
    $pb = new PropertyBag();
    // $pb->setCustomProperty('day_of_month', 1);
    $pb->setCustomProperty('qfKey', 'mockKey');
    $pb->setCustomProperty('entryURL', 'https://example.org/');
    $pb->setDescription('Support CiviCRM');
    $pb->setContributionRecurID($recur['id']);
    $pb->setContactID($contactID);
    $pb->setRecurFrequencyUnit('day'); /* issue59 */
    $pb->setRecurFrequencyInterval(7);/* issue59 */

    $pp = $this->getPaymentProcessorObject();
    $url = $pp->getBillingRequestFlowURL($pb, 'contribute');
    $this->assertEquals('https://brq.example.com', $url);

    // Test that the recur was updated.
    $recurNow = ContributionRecur::get(FALSE)
      ->addWhere('id', '=', $recur['id'])
      ->addSelect('processor_id', 'cycle_day', 'contribution_status_id:name', 'frequency_unit', 'frequency_interval')
      ->execute()->single();
    $this->assertEquals(
      [
        'id' => (int) $recur['id'],
        'processor_id' => 'BRQ00001',
        'cycle_day' => 0,
        'contribution_status_id:name' => 'Pending',
        'frequency_unit' => 'week', /* issue59 */
        'frequency_interval' => 1, /* issue59 */
      ],
      $recurNow
    );
  }

  public function testUpgrade0002() {
    $payment_instrument = civicrm_api3('OptionValue', 'getsingle', ['option_group_id' => "payment_instrument", 'name' => "direct_debit_gc"])['value'];
    $processor_type = civicrm_api3('PaymentProcessorType', 'getsingle', ['name' => 'GoCardless', 'options' => ['limit' => 0]])['id'];

    // After an install, our processor should be correctly set up.
    $proc = civicrm_api3('PaymentProcessor', 'getsingle', ['id' => $this->test_mode_payment_processor['id']]);
    $this->assertEquals($payment_instrument, $proc['payment_instrument_id']);
    $this->assertEquals(CRM_Core_Payment::PAYMENT_TYPE_DIRECT_DEBIT, $proc['payment_type']);

    // Now bodge this backwards.
    CRM_Core_DAO::executeQuery("UPDATE civicrm_payment_processor SET payment_type=1, payment_instrument_id=1 WHERE id = %1", [
      1 => [$this->test_mode_payment_processor['id'], 'Integer'],
    ]);

    // Now run the upgrade
    $up = new CRM_GoCardless_Upgrader(E::LONG_NAME, E::path());
    $up->upgrade_0002();

    // And re-test
    $proc = civicrm_api3('PaymentProcessor', 'getsingle', ['id' => $this->test_mode_payment_processor['id']]);
    $this->assertEquals($payment_instrument, $proc['payment_instrument_id']);
    $this->assertEquals(CRM_Core_Payment::PAYMENT_TYPE_DIRECT_DEBIT, $proc['payment_type']);

  }

  /**
   * check sending receipts.
   *
   * Variables: contrib recur, contrib API, sendReceiptsForCustomPayments
   *
   * Note: there are several places where sending receipts is controlled.
   *
   * is_send_contribution_notification
   * is_email_receipt
   *
   * @dataProvider dataForTestSendReceipts
   */
  public function testSendReceipts($policy, $recur_is_email_receipt, $expected) {
    $mut = new CiviMailUtils($this, TRUE);

    $settings = CRM_GoCardlessUtils::getSettings();
    $settings['sendReceiptsForCustomPayments'] = $policy;
    Civi::settings()->set('gocardless', json_encode($settings));

    $fixtureOverrides = [
      'recur' => [
        'processor_id' => 'SUBSCRIPTION_ID', /* simulate a complete flow */
        'contribution_status_id' => 'In Progress',
        'is_email_receipt' => $recur_is_email_receipt,
      ],
      'order' => [],
    ];
    list($contactID, $recur, $contrib, $membership) = $this->createFixture($fixtureOverrides, FALSE);

    // Mock webhook input data.
    $body = '{"events":[
      {"id":"EV1","resource_type":"payments","action":"confirmed","links":{"payment":"PAYMENT_ID"}}
      ]}';
    $calculatedSignature = $this->mockGoCardlessApiForWebhook($body, '{
        "id":"PAYMENT_ID",
        "status":"confirmed",
        "charge_date": "2016-10-02",
        "amount":100,
        "links":{"subscription":"SUBSCRIPTION_ID"}
      }'
    );

    // Trigger webhook
    $controller = new CRM_Core_Payment_GoCardlessIPN();
    $controller->parseWebhookRequest(["Webhook-Signature" => $calculatedSignature], $body);
    $controller->processWebhookEvents(TRUE);

    $contrib = civicrm_api3('Contribution', 'getsingle', ['id' => $contrib['id']]);
    $this->assertContributionStatus($contrib, 'Completed');
    $messagesSent = count($mut->getAllMessages());

    if ($expected) {
      // Check that a receipt WAS sent.
      $this->assertNotEmpty($contrib['receipt_date'] ?? NULL);
      $this->assertEquals(1, $messagesSent, "Expected 1 email to be sent when first payment completed. (receipt date: {$contrib['receipt_date']})");
      $mut->checkMailLog(['Contribution Information']);
    }
    else {
      // Check it was NOT sent.
      $this->assertEmpty($contrib['receipt_date'] ?? NULL);
      $this->assertEquals(0, $messagesSent, "Expected no emails to be sent when first payment completed");
      $mut->checkMailLog([], ['Contribution Information']);
    }

    // Subsequent payments
    //
    // Reset mail log
    $mut->clearMessages();

    // Create 2nd contrib.
    // Mock webhook input data.
    $body = '{"events":[
      {"id":"EV2","resource_type":"payments","action":"confirmed","links":{"payment":"PAYMENT_ID_2"}}
      ]}';
    $calculatedSignature = $this->mockGoCardlessApiForWebhook($body, '{
        "id":"PAYMENT_ID_2",
        "status":"confirmed",
        "charge_date": "2016-11-02",
        "amount":100,
        "links":{"subscription":"SUBSCRIPTION_ID"}
      }', 'PAYMENT_ID_2');

    // Trigger webhook
    $controller = new CRM_Core_Payment_GoCardlessIPN();
    $controller->parseWebhookRequest(["Webhook-Signature" => $calculatedSignature], $body);
    $controller->processWebhookEvents(TRUE);

    // Find the latest contribution.
    $contribution2 = civicrm_api3('Contribution', 'get', [
      'contribution_recur_id' => $recur['id'],
      'return' => ['receipt_date', 'receive_date'],
      'sequential' => 1,
      'is_test' => 1,
      'options' => ['limit' => 1, 'sort' => "id DESC"],
    ])['values'][0] ?? NULL;
    // Check that this is a different contrib to the other one.
    $this->assertNotNull($contribution2, "Expected to find a contribution but got none!");
    $this->assertNotEquals($contrib['id'], $contribution2['id'], 'Expected for a new contribution to have been added but seems the last one is the first one we made.');
    $this->assertEquals('2016-11-02 00:00:00', $contribution2['receive_date'], 'Wrong receive_date on followup contribution');
    $receipt_date = $contribution2['receipt_date'] ?? NULL;
    $messagesSent = count($mut->getAllMessages());
    if ($expected) {
      // Check that a receipt WAS sent.
      $this->assertNotEmpty($receipt_date, 'Expected a receipt_date to have been set for subsequent payment');
      $this->assertEquals(1, $messagesSent, "Expected one email to be sent for subsequent payments");
      $mut->checkMailLog(['Contribution Information', 'PAYMENT_ID_2']);
      // We only expect one message
      $this->assertEquals(1, $messagesSent);
    }
    else {
      // Check it was NOT sent.
      $this->assertEmpty($receipt_date, 'Expected a receipt_date NOT to have been set for subsequent payment');
      $mut->checkMailLog([], ['Contribution Information', 'PAYMENT_ID_2']);
      // We don't expect any messages
      $this->assertEquals(0, $messagesSent, "Expected no emails to be sent for subsequent payments");
    }
    $mut->stop();
    $mut->clearMessages();

  }

  /**
   * Data provider for testSendReceipts
   */
  public function dataForTestSendReceipts() {
    return [
      ['defer', 1, 1],
      ['defer', 0, 0],
      ['always', 0, 1],
      ['always', 1, 1],
      ['never', 0, 0],
      ['never', 1, 0],
    ];
  }

  /**
   * Tests the cronjob that fails abandoned cr, cns.
   */
  public function testGocardlessfailabandoned() {
    $dt = new DateTimeImmutable();
    // Create an old contribution date.
    $receive_date = $dt->modify("-1 day")->format("Y-m-d");
    // The processor_id doesn't matter, but at this stage it would be a BRQ...
    $fixtureOverrides = [
      'recur' => [
        'processor_id' => 'BRQ00001',
        'amount' => 1,
        'start_date' => $receive_date,
      ],
      'order' => [
        'receive_date' => $receive_date,
      ],
    ];
    list($contactID, $recur, $contrib, $membership) = $this->createFixture($fixtureOverrides, FALSE);

    civicrm_api3('Job', 'Gocardlessfailabandoned', ['contribution_recur_id' => $recur['id']]);

    // We expect that the contrib recur is now Failed
    // We expect that the contrib is now Cancelled
    // We expect that there is now a note on the cn.
    $recur = civicrm_api3('ContributionRecur', 'getsingle', ['id' => $recur['id']]);
    $this->assertContributionRecurStatus($recur, 'Failed');

    $contrib = civicrm_api3('Contribution', 'getsingle', ['id' => $contrib['id']]);
    $this->assertContributionStatus($contrib, 'Cancelled');

    $note = Note::get(FALSE)
      ->addWhere('entity_table', '=', 'civicrm_contribution')
      ->addWhere('entity_id', '=', $contrib['id'])
      ->execute()->single();
    $this->assertEquals($note['note'], 'Mandate setup abandoned or failed.');

  }

  protected function createExistingMembershipNotPaidByGC($contactID, ?array $membershipParamsOverride, $expectedStatus = 'New') {

    $dt = new DateTimeImmutable();

    // Create a membership not using GC
    $contrib = civicrm_api3('Order', 'create',
      [
        'sequential' => 1,
        'financial_type_id' => 1,
        'total_amount' => 1,
        'receive_date' => $membershipParamsOverride['start_date'],
        'contact_id' => $contactID,
        'contribution_status_id' => "Pending",
        'is_test' => 1,
        'line_items' => [[
          'params' => $membershipParamsOverride + [
            'membership_type_id' => 'MyMembershipType',
            'contact_id' => $contactID,

            // Needed to override default status calculation *deprecated*
            'skipStatusCal' => 1,
          ],
          'line_item' => [[
            'entity_table' => 'civicrm_membership',
            'line_total' => 1,
            'unit_price' => 1,
            "price_field_id" => 1,
          // xxx ?
            "price_field_value_id" => 1,
            'financial_type_id' => 1, /* 'Donation' */
            'qty' => 1,
          ],
          ],
        ],
        ],
      ]);
    $contrib = civicrm_api3('Order', 'getsingle', ['id' => $contrib['id']]);
    $membership = civicrm_api3('Membership', 'getsingle', ['id' => $contrib['line_items'][0]['entity_id']]);

    //print json_encode(['input' => $membershipParamsOverride, 'output' => $membership ], JSON_PRETTY_PRINT);

    // Now get that membership started with a payment.
    civicrm_api3('Payment', 'create', [
      'contribution_id' => $contrib['id'],
      'total_amount' => $contrib['total_amount'],
      'trxn_date' => $contrib['receive_date'],
      'trxn_id' => 'NON_GC_PAYMENT_ID',
      'is_send_contribution_notification' => 0,
    ]);
    // Reload membership and contrib
    $contrib = civicrm_api3('Order', 'getsingle', ['id' => $contrib['id']]);
    $membership = civicrm_api3('Membership', 'getsingle', ['id' => $membership['id']]);
    // Check it's what we expect - this is a sanity check, it's not actually checking GC stuff.
    $this->assertMembershipStatus($membership, $expectedStatus);
    $this->assertContributionStatus($contrib, 'Completed');

    //print json_encode(['input' => $membershipParamsOverride, 'output' => $membership ], JSON_PRETTY_PRINT);

    return [$contrib, $membership];
  }

  /**
   * Return a fake GoCardless IPN processor.
   *
   * Helper function for other tests.
   */
  protected function getWebhookControllerForTestProcessor() :CRM_Core_Payment_GoCardlessIPN {
    $pp_config = $this->test_mode_payment_processor;
    $pp = Civi\Payment\System::singleton()->getByProcessor($pp_config);
    $controller = new CRM_Core_Payment_GoCardlessIPN($pp);
    return $controller;
  }

  /**
   * Helper
   */
  protected function mockGoCardlessApiForTestPaymentProcessor($mock) {
    $obj = new CRM_Core_Payment_GoCardless('test', $this->test_mode_payment_processor);
    $obj->setGoCardlessApi($mock);
  }

  /**
   * DRY code.
   *
   * @param string $paymentJSON
   */
  protected function mockPaymentGet($paymentJSON) {
    $payment = json_decode($paymentJSON);
    if (!$payment) {
      throw new \InvalidArgumentException("test code error: \$paymentJSON must be valid json but wasn't");
    }
    $this->mockGoCardlessApiForWebhook('', $paymentJSON);
  }

  /**
   * Fetch the last contribution (by ID) for the given recurID.
   *
   * @param int $recurID
   *
   * @return array
   */
  protected function getLatestContribution($recurID) {
    $r = civicrm_api3('Contribution', 'get', [
      'contribution_recur_id' => $recurID,
      'sequential' => 1,
      'is_test' => 1,
      'options' => ['limit' => 0, 'sort' => "id ASC"],
    ])['values'] ?? [];
    if ($r) {
      return end($r);
    }
  }

  protected function createTestContact():int {
    if (empty($this->contactID)) {
      $contact = civicrm_api3('Contact', 'create', [
        'sequential' => 1,
        'contact_type' => "Individual",
        'first_name' => "Wilma",
        'last_name' => "Flintstone",
        'email' => 'wilma@example.org',
      ]);
      $this->contactID = (int) $contact['id'];
    }
    return $this->contactID;
  }

  protected function assertMembershipStatus(array $membership, string $statusName) {
    $expectedStatusID = $this->membership_status_map[$statusName];
    $gotStatusName = $this->membership_status_map_flip[$membership['status_id']];
    $this->assertEquals($expectedStatusID, $membership['status_id'],
      "Expected memberships status '$statusName' but got '$gotStatusName'"
    );
  }

  protected function assertContributionStatus(array $contrib, string $statusName) {

    $expectedStatusID = $this->contribution_status_map[$statusName];
    $gotStatusID = $contrib['contribution_status_id'];
    $gotStatusName = array_flip($this->contribution_status_map)[$gotStatusID];
    $this->assertEquals($expectedStatusID, $gotStatusID,
      "Expected contribution status '$statusName' but got '$gotStatusName'"
    );
  }

  protected function assertContributionRecurStatus(array $recur, string $statusName) {

    $expectedStatusID = $this->contribution_recur_status_map[$statusName];
    $gotStatusID = $recur['contribution_status_id'];
    $gotStatusName = array_flip($this->contribution_recur_status_map)[$gotStatusID];
    $this->assertEquals($expectedStatusID, $gotStatusID,
      "Expected contribution recur status '$statusName' but got '$gotStatusName'"
    );
  }

  /**
   * Creates a recurring contribution in the way one would be created before
   * the user is sent off-site to GC.
   */
  protected function createContribRecur($overrides = []) {
    return civicrm_api3('ContributionRecur', 'create',
      $overrides + [
        'sequential' => 1,
        'contact_id' => $this->createTestContact(),
        'frequency_interval' => 1,
        'payment_processor_id' => $this->test_mode_payment_processor['id'],
        'amount' => 1,
        'frequency_unit' => "month",
        'start_date' => $this->defaultDate,
        'financial_type_id' => 1,
        'is_test' => 1,
        'contribution_status_id' => "Pending",
      ])['values'][0];
  }

  public function assertLogContains($patterns) {
    $patterns = (array) $patterns;
    $logs = \Civi\Api4\TaggedLog::get(FALSE)
      ->setFacility('GoCardless')
      ->setPatterns($patterns)
      ->execute()->countFetched();
    $this->assertGreaterThan(0, $logs, "Expected log to contain " . json_encode($patterns));
  }

  protected function getPaymentProcessorObject(): CRM_Core_Payment_GoCardless {
    return \Civi\Payment\System::singleton()->getByProcessor($this->test_mode_payment_processor);
  }

  protected function mockGoCardlessApiForSuccessfulBRF(string $dateSubscriptionStarts, int $contactID, array $recur, int $contributionID, string $brqStatus = 'fulfilled') {

    // Always mock the billingRequest get call.
    $billingRequest = $this->createMock(\GoCardlessPro\Resources\BillingRequest::class);
    $billingRequest->method('__get')->willReturnMap([
      ['status', $brqStatus],
      ['id', 'BRQ00001'],
      ['mandate_request', (object) ['description' => 'test contribution']],
      ['metadata', (object) ['contactID' => (string) $contactID, 'contributionRecurID' => (string) $recur['id'], 'contributionID' => $contributionID]],
      ['links', (object) ['mandate_request_mandate' => 'MANDATE_ID']],
    ]);
    $billingRequestsService = $this->createMock(\GoCardlessPro\Services\BillingRequestsService::class);
    $billingRequestsService
      ->expects($this->once())
      ->method('get')
      ->with('BRQ00001')
      ->willReturn($billingRequest);

    $apiMock = $this->createMock(\GoCardlessPro\Client::class);
    $apiMock->method('billingRequests')->willReturn($billingRequestsService);

    if ($brqStatus === 'fulfilled') {
      $mandate = $this->createMock(\GoCardlessPro\Resources\Mandate::class);
      $mandate->method('__get')->with('next_possible_charge_date')->willReturn($dateSubscriptionStarts);
      $mandatesService = $this->createMock(\GoCardlessPro\Services\MandatesService::class);
      $mandatesService
        ->expects($this->once())
        ->method('get')
        ->with('MANDATE_ID')
        ->willReturn($mandate);

      $expectedSubscriptionCreateParams = [
        'amount'        => 100,
        'currency'      => 'GBP',
        'interval'      => 1,
        'name'          => 'test contribution',
        'interval_unit' => 'monthly',
        'links'         => ['mandate' => 'MANDATE_ID'],
        'metadata'      => ['civicrm' => json_encode(['contactID' => (int) $contactID, 'contributionRecurID' => (int) $recur['id']])],
      ];
      // if ($installments) {
      //   $expectedSubscriptionCreateParams['count'] = $installments;
      // }
      $subscription = $this->createMock(\GoCardlessPro\Resources\Subscription::class);
      $fakeDateOfNextPayment = date('Y-m-d\TH:i:s\Z', strtotime($dateSubscriptionStarts . ' + 1 month'));
      $subscription->method('__get')->willReturnMap([
      ['start_date', $dateSubscriptionStarts],
      ['id', 'SUBSCRIPTION_ID'],
      ['upcoming_payments', [(object) ['charge_date' => $fakeDateOfNextPayment]]],
      ]);

      $subscriptionService = $this->createMock(\GoCardlessPro\Services\SubscriptionsService::class);
      $subscriptionService
        ->expects($this->once())
        ->method('create')
        ->with(['params' => $expectedSubscriptionCreateParams])
        ->willReturn($subscription);

      $apiMock->method('subscriptions')->willReturn($subscriptionService);
      $apiMock->method('mandates')->willReturn($mandatesService);
    }

    $this->mockGoCardlessApiForTestPaymentProcessor($apiMock);
  }

  protected function mockGoCardlessApiForGetBRFUrl() {

    // Mock the GC API
    $billingRequest = $this->createMock(\GoCardlessPro\Resources\BillingRequest::class);
    $billingRequest->method('__get')->with('id')->willReturn('BRQ00001');
    $billingRequestsService = $this->createMock(\GoCardlessPro\Services\BillingRequestsService::class);
    $billingRequestsService
      ->expects($this->once())
      ->method('create')
      ->with($this->callback(function ($arg) {
        $p = $arg['params'];
        return ($p['mandate_request'] === ['description' => 'Support CiviCRM', 'currency' => 'GBP'])
          && array_diff(array_keys($p['metadata']), ['contactID', 'contributionRecurID', 'contributionID']) === [];
      }))
      ->willReturn($billingRequest);
    $billingRequestFlow = $this->createMock(\GoCardlessPro\Resources\BillingRequestFlow::class);
    $billingRequestFlow->method('__get')->with('authorisation_url')->willReturn('https://brq.example.com');
    $billingRequestFlowsService = $this->createMock(\GoCardlessPro\Services\BillingRequestFlowsService::class);
    $billingRequestFlowsService
      ->expects($this->once())
      ->method('create')
      ->with($this->callback(function ($arg) {
        return (($arg['params']['links']['billing_request'] ?? NULL) == 'BRQ00001')
          && strpos(($arg['params']['redirect_uri'] ?? NULL), 'BRQ00001') > 0;
      }))
      ->willReturn($billingRequestFlow);
    $apiMock = $this->createMock(\GoCardlessPro\Client::class);
    $apiMock->method('billingRequests')->willReturn($billingRequestsService);
    $apiMock->method('billingRequestFlows')->willReturn($billingRequestFlowsService);
    $this->mockGoCardlessApiForTestPaymentProcessor($apiMock);
  }

  public function testChooseBestContactDetails() {
    $gcpp = $this->getPaymentProcessorObject();
    $billingLocTypeID = CRM_Core_BAO_LocationType::getBilling();
    $this->assertGreaterThan(0, $billingLocTypeID);
    $this->assertLessThan(100, $billingLocTypeID);

    // Check it picks the billing when we have lots others available.
    $params = [
      "phone-Primary" => '456',
      "email-Primary" => '456@example.org',
      "street_address-Primary" => '456 The Road',
      "city-Primary" => 'Some City',
      "phone-$billingLocTypeID" => '123',
      "email-$billingLocTypeID" => '123@example.org',
      "street_address-$billingLocTypeID" => '123 The Road',
      "phone-100" => '987',
      "email-100" => '987@example.org',
      "street_address-100" => '987 The Road',
    ];
    $munged = $gcpp->chooseBestContactDetails($params);
    $this->assertEquals([
      "phone" => '123',
      "email" => '123@example.org',
      "billingStreetAddress" => '123 The Road',
    ], $munged);

    // Check it picks primary when billing not there.
    $params = [
      "phone-Primary" => '456',
      "email-Primary" => '456@example.org',
      "street_address-Primary" => '456 The Road',
      "phone-100" => '987',
      "email-100" => '987@example.org',
      "street_address-100" => '987 The Road',
    ];
    $munged = $gcpp->chooseBestContactDetails($params);
    $this->assertEquals([
      "phone" => '456',
      "email" => '456@example.org',
      "billingStreetAddress" => '456 The Road',
    ], $munged);


    // Check it picks any address if neither Billing nor primary
    $params = [
      "phone-100" => '987',
      "email-100" => '987@example.org',
      "street_address-100" => '987 The Road',
    ];
    $munged = $gcpp->chooseBestContactDetails($params);
    $this->assertEquals([
      "phone" => '987',
      "email" => '987@example.org',
      "billingStreetAddress" => '987 The Road',
    ], $munged);

    // Check it picks nothing if nothing
    $params = [];
    $munged = $gcpp->chooseBestContactDetails($params);
    $this->assertEquals([], $munged);

    // Mixed email/billing
    $params = [
      "email-$billingLocTypeID" => '123@example.org',
      "email-Primary" => '456@example.org',
      "street_address-Primary" => '456 The Road',
    ];
    $munged = $gcpp->chooseBestContactDetails($params);
    $this->assertEquals([
      "email" => '123@example.org',
      "billingStreetAddress" => '456 The Road',
    ], $munged);
  }

}
