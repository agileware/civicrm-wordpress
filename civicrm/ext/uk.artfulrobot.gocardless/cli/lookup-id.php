<?php
if (PHP_SAPI !== 'cli') {
  http_response_code(404);
  echo "404 Not Found.";
  exit;
}

$id = $argv[1] ?? NULL;
if (!$id) {
  echo <<<TXT
  Usage: cv scr $argv[0] <GOCARDLESS_ID>

  Looks up a GoCardless object from the given ID.

  TXT;
}

/**
 * @var CRM_Core_Payment_GoCardless $pp */
$pp = \Civi\Payment\System::singleton()->getByName('GoCardless', FALSE);
$gc = $pp->getGoCardlessApi();

if (preg_match('/^BRQ[a-zA-Z0-9]+$/', $id)) {
  $obj = $gc->billingRequests()->get($id);
}
elseif (preg_match('/^SUB[a-zA-Z0-9]+$/', $id)) {
  $obj = $gc->subscriptions()->get($id);
}
elseif (preg_match('/^PM[a-zA-Z0-9]+$/', $id)) {
  $obj = $gc->payments()->get($id);
}
elseif (preg_match('/^MD[a-zA-Z0-9]+$/', $id)) {
  $obj = $gc->mandates()->get($id);
}
elseif (preg_match('/^CU[a-zA-Z0-9]+$/', $id)) {
  $obj = $gc->customers()->get($id);
}
else {
  fwrite(STDERR, "Unrecognised id\n");
  exit(1);
}

function extractGcData(\GoCardlessPro\Resources\BaseResource $obj) {
  $o = [];
  $r = new ReflectionObject($obj);
  while ($r->name !== \GoCardlessPro\Resources\BaseResource::class) {
    $r = $r->getParentClass();
  }
  $prop = $r->getProperty('data');
  $prop->setAccessible(TRUE);
  $d = json_decode(json_encode($prop->getValue($obj)), TRUE);
  echo myJson($d);
}

function myJson(array &$d, $depth = 0) {
  // depth first.
  $big = count($d) > 2;
  foreach ($d as $k => $v) {
    if (is_array($v)) {
      $d[$k] = myJson($v, $depth + 1);
    }
    else {
      // Simple thing
      $d[$k] = json_encode($v);
      if (strpos($d[$k], "\n") !== FALSE) {
        $big = TRUE;
      }
    }
  }
  $indent = str_repeat(' ', $depth);
  $indent2 = str_repeat(' ', $depth + 1);
  $s = '';
  if ($big) {
    $parts = [];
    foreach ($d as $k => $v) {
      $parts[] = "$indent$k: $v";
    }
    $s = "{\n$indent2" . implode(",\n$indent2", $parts);
    $s .= "$indent}";
  }
  else {
    $parts = [];
    foreach ($d as $k => $v) {
      $parts[] = "$k: $v";
    }
    $s = "{ " . implode(", ", $parts);
    $s .= "}";
  }
  return $s;
}

// $d = ['x' => 123, 'y' => '567']; print myJson($d);
extractGcData($obj);
