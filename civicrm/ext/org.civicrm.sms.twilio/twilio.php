<?php

require_once 'twilio.civix.php';

/**
 * Implementation of hook_civicrm_config
 */
function twilio_civicrm_config(&$config) {
  _twilio_civix_civicrm_config($config);
}

/**
 * Implementation of hook_civicrm_install
 */
function twilio_civicrm_install() {
  $groupID = CRM_Core_DAO::getFieldValue('CRM_Core_DAO_OptionGroup','sms_provider_name','id','name');

  civicrm_api3('OptionValue', 'create', [
    'option_group_id' => $groupID,
    'label' => 'Twilio',
    'value' => 'org.civicrm.sms.twilio',
    'name'  => 'twilio',
    'is_default' => 1,
    'is_active'  => 1,
  ]);

  return _twilio_civix_civicrm_install();
}

/**
 * Implementation of hook_civicrm_uninstall
 */
function twilio_civicrm_uninstall() {
  $optionID = CRM_Core_DAO::getFieldValue('CRM_Core_DAO_OptionValue','twilio','id','name');

  if ($optionID) {
    CRM_Core_BAO_OptionValue::del($optionID); 
  }
  
  $filter =  ['name'  => 'org.civicrm.sms.twilio'];
  $Providers = CRM_SMS_BAO_Provider::getProviders(FALSE, $filter, FALSE);
  if ($Providers) {
    foreach($Providers as $key => $value) {
      CRM_SMS_BAO_Provider::del($value['id']);
    }
  }
  return;
}

/**
 * Implementation of hook_civicrm_enable
 */
function twilio_civicrm_enable() {
  $optionID = CRM_Core_DAO::getFieldValue('CRM_Core_DAO_OptionValue','twilio' ,'id','name');

  if ($optionID) {
    CRM_Core_BAO_OptionValue::setIsActive($optionID, TRUE); 
  }
  
  $filter = ['name' => 'org.civicrm.sms.twilio'];
  $Providers = CRM_SMS_BAO_Provider::getProviders(FALSE, $filter, FALSE);
  if ($Providers) {
    foreach ($Providers as $key => $value) {
      CRM_SMS_BAO_Provider::setIsActive($value['id'], TRUE); 
    }
  }
  return _twilio_civix_civicrm_enable();
}

/**
 * Implementation of hook_civicrm_disable
 */
function twilio_civicrm_disable() {
  $optionID = CRM_Core_DAO::getFieldValue('CRM_Core_DAO_OptionValue','twilio','id','name');

  if ($optionID) {
    CRM_Core_BAO_OptionValue::setIsActive($optionID, FALSE);
  }
  
  $filter = ['name' => 'org.civicrm.sms.twilio'];
  $Providers = CRM_SMS_BAO_Provider::getProviders(FALSE, $filter, FALSE);
  if ($Providers) {
    foreach ($Providers as $key => $value) {
      CRM_SMS_BAO_Provider::setIsActive($value['id'], FALSE); 
    }
  }
  return;
}
