# Twilio SMS Provider

Twilio and CiviCRM integration allows delivering single and mass short message service (SMS) messages through its Twilio Gateway to mobile phone users.

Installation instructions: https://docs.civicrm.org/user/en/latest/sms-text-messaging/set-up/
