<?php

class CRM_Recurringbuttons_Contribute_Form_ContributionPage_Amount {

  /**
   * @see recurringbuttons_civicrm_buildForm().
   *
   * Adds a widget for the setting. The JS positions it in the right place.
   */
  public static function buildForm(&$form) {
    $form->addYesNo('contribution_recurringbuttons_default', ts('Select recurring donations by default?'), FALSE);

    $form_id = $form->get('id');
    $current_value = Civi::settings()->get('recurringbuttons_default_' . $form_id);

    if (empty($current_value)) {
      $current_value = 0;
    }

    $form->setDefaults([
      'contribution_recurringbuttons_default' => $current_value,
    ]);

    CRM_Core_Region::instance('form-body')->add(array(
      'template' => 'CRM/Recurringbuttons/Contribute/Form/ContributePage/Amount.tpl',
    ));
  }

  /**
   *
   */
  public static function postProcess(&$form) {
    $form_id = $form->get('id');
    $values = $form->exportValues();
    $default = CRM_Utils_Array::value('contribution_recurringbuttons_default', $values);

    Civi::settings()->set('recurringbuttons_default_' . $form_id, $default);
  }

}
