<table class="crm-form-block crm-contribution-recurringbuttons">
  <tr>
    <td class="label">{$form.contribution_recurringbuttons_default.label}</td>
    <td class="content">
      {$form.contribution_recurringbuttons_default.html}
    </td>
  </tr>
</table>

{literal}
<script>
  CRM.$(function($) {
    // Move the fields to the right place
    $('#recurFields > td > table tr:last').after($('.crm-contribution-recurringbuttons tr'));
    $('.crm-contribution-contributionbuttons').remove();
  });
</script>
{/literal}
