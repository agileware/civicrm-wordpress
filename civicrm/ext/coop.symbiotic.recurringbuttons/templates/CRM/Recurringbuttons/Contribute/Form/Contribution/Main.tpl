<div class="crm-public-form-item crm-section is_recur_radio-section">
  <div class="label">{$form.is_recur_radio.label}</div>
  <div class="content">
    {$form.is_recur_radio.html}
  </div>
  <div class="clear"></div>
</div>
