<?php

require_once 'radiobuttons.civix.php';
use CRM_Radiobuttons_ExtensionUtil as E;

/**
 * Implements hook_civicrm_config().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_config
 */
function radiobuttons_civicrm_config(&$config) {
  _radiobuttons_civix_civicrm_config($config);
}

/**
 * Implements hook_civicrm_xmlMenu().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_xmlMenu
 */
function radiobuttons_civicrm_xmlMenu(&$files) {
  _radiobuttons_civix_civicrm_xmlMenu($files);
}

/**
 * Implements hook_civicrm_install().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_install
 */
function radiobuttons_civicrm_install() {
  _radiobuttons_civix_civicrm_install();
}

/**
 * Implements hook_civicrm_postInstall().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_postInstall
 */
function radiobuttons_civicrm_postInstall() {
  _radiobuttons_civix_civicrm_postInstall();
}

/**
 * Implements hook_civicrm_uninstall().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_uninstall
 */
function radiobuttons_civicrm_uninstall() {
  _radiobuttons_civix_civicrm_uninstall();
}

/**
 * Implements hook_civicrm_enable().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_enable
 */
function radiobuttons_civicrm_enable() {
  _radiobuttons_civix_civicrm_enable();
}

/**
 * Implements hook_civicrm_disable().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_disable
 */
function radiobuttons_civicrm_disable() {
  _radiobuttons_civix_civicrm_disable();
}

/**
 * Implements hook_civicrm_upgrade().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_upgrade
 */
function radiobuttons_civicrm_upgrade($op, CRM_Queue_Queue $queue = NULL) {
  return _radiobuttons_civix_civicrm_upgrade($op, $queue);
}

/**
 * Implements hook_civicrm_angularModules().
 *
 * Generate a list of Angular modules.
 *
 * Note: This hook only runs in CiviCRM 4.5+. It may
 * use features only available in v4.6+.
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_angularModules
 */
function radiobuttons_civicrm_angularModules(&$angularModules) {
  _radiobuttons_civix_civicrm_angularModules($angularModules);
}

/**
 * Implements hook_civicrm_alterSettingsFolders().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_alterSettingsFolders
 */
function radiobuttons_civicrm_alterSettingsFolders(&$metaDataFolders = NULL) {
  _radiobuttons_civix_civicrm_alterSettingsFolders($metaDataFolders);
}

/**
 * Helper functions that can be called from other extensions.
 */
function radiobuttons_convert_element($selector, Array $options) {
  if (!isset(Civi::$statics[__FUNCTION__])) {
    Civi::resources()
      ->addStyleFile('radiobuttons', 'radiobuttons.css')
      ->addScriptFile('radiobuttons', 'radiobuttons.js');

    Civi::$statics[__FUNCTION__] = 1;
  }

  $options_json = json_encode($options);

  Civi::resources()
    ->addScript('CRM.radiobuttonsFormRadiosAsButtons("' . $selector . '", ' . $options_json . ');', ['weight' => 10]);
}

/**
 * Implements hook_civicrm_buildForm().
 */
function radiobuttons_civicrm_buildForm($formName, &$form) {
  $check_forms = [
    'CRM_Contribute_Form_Contribution_Main',
    'CRM_Event_Form_Registration_Register',
  ];

  if (in_array($formName, $check_forms)) {
    CRM_Radiobuttons_Contribute_Form_Contribution_Main::buildForm($form);
  }
}

/**
 * Implements hook_civicrm_navigationMenu().
 */
function radiobuttons_civicrm_navigationMenu(&$menu) {
  _radiobuttons_civix_insert_navigation_menu($menu, 'Administer/Customize Data and Screens', [
    'label' => E::ts('Radiobuttons Settings'),
    'name' => 'radiobuttons_settings',
    'url' => 'civicrm/admin/setting/radiobuttons',
    'permission' => 'administer CiviCRM',
    'operator' => 'OR',
    'separator' => 0,
  ]);
  _radiobuttons_civix_navigationMenu($menu);
}
