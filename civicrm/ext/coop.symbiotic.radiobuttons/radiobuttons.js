(function($, _, ts) {

  /**
   * Make radio buttons more pretty.
   */
  CRM.radiobuttonsFormRadiosAsButtons = function(selector, options) {
    options = typeof options !== 'undefined' ? options : {};

    var button_width = options.button_width || '';
    var button_width_class = (button_width ? 'crm-radio-wrapper-' + button_width + 'px' : '');
    var hide_zero_value = options.hide_zero_value || false;
    var mandatory_field = options.mandatory_field || false;
    var preserve_class = options.preserve_class || 'content';

    $(document).on('crmLoad', function(e) {
      if ($('.crm-form-radio', selector).size()) {
        var $new = $('<div>', {'class': preserve_class + ' radiobuttons-form-radios-as-buttons'});

        $('.crm-form-radio', selector).each(function() {
          var $this = $(this);
          var $oldparent = $(this).parent();
          var $label = $('label[for=' + $(this).attr('id') + ']');
          var id = 'crm-radio-' + $this.attr('name') + '-wrapper';
          var $div = $('<div>', {id: id, 'class': 'crm-radio-wrapper ' + button_width_class});

          // On the Payment Type field "prop" did not work. Using "is" is said to be
          // slower, but should work everywhere.
          if ($this.is(':checked')) {
            $div.addClass('selected');
          }

          $this.removeClass('crm-form-radio');
          $div.append($this);
          $div.append($label);
          $new.append($div);

          if (hide_zero_value && $(this).val() == 0) {
            $div.hide();
          }

          // Hide empty dom elements
          if ($oldparent.is(':empty')) {
            $oldparent.remove();
          }
        });

        // Weird leftover from messing with the DOM (oldparent seems to get flattened)
        // Hide any leftover empty price-set-row (notably if using 'quick' pricesets)
        $('.price-set-row', selector).each(function() {
          if ($(this).is(':empty') || $(this).children().length == 0) {
            console.log('empty - removing');
            $(this).remove();
          }
        });

        // Check for other non-radio divs, such as recurHelp
        $('*:not(.crm-form-radio)', selector).appendTo($new);

        // This is to make sure we do not have stray nbsps.
        $(selector).replaceWith($new);
      }

      // Add a 'selected' class on clicked labels
      $('.crm-radio-wrapper > label', selector).on('click', function(e) {
        $this = $(this);
        $tbody = $this.closest(selector);
        var already_selected = $this.parent().hasClass('selected');

        $('.crm-radio-wrapper', $tbody).removeClass('selected');

        // Allow deselecting
        if (already_selected && !mandatory_field) {
          $this.parent().find('input.crm-form-radio').prop('checked', false);
          e.preventDefault();
        }
        else {
          $this.parent().addClass('selected');
        }
      });

      // Add a class on the initial selector, for easier CSS theming, but do it only once
      $(selector).addClass('radiobuttons-form-radios-as-buttons');
    });
  };

})(CRM.$, CRM._, CRM.ts('radiobuttons'));
