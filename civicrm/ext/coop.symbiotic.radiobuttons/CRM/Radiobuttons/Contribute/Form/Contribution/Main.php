<?php

use CRM_Radiobuttons_ExtensionUtil as E;

class CRM_Radiobuttons_Contribute_Form_Contribution_Main {

  /**
   * Code copied from the pricesetvisibility extension
   *
   * @see pricesetvisibility_civicrm_buildForm().
   */
  public static function buildForm(&$form) {
    $price_set_id = $form->_priceSetId;

    // Ex: Free events, no pricing buttons
    if (!$price_set_id) {
      return;
    }

    $priceset = civicrm_api3('PriceSet', 'getsingle', [
      'id' => $price_set_id,
    ]);

    // Convert the Amounts options
    if (Civi::settings()->get('radiobuttons_amounts')) {
      // Selectors for full PriceSets
      $jquery_selector = '#priceset div.content.amount-content';
      $original_class = 'content amount-content';

      if ($priceset['is_quick_config']) {
        $jquery_selector = '#priceset div.content.contribution_amount-content';
        $original_class = 'content contribution_amount-content';
      }

      radiobuttons_convert_element($jquery_selector, [
        'mandatory_field' => true,
        'button_width' => 400,
        'preserve_class' => $original_class,
      ]);
    }

    // Convert the Payment Processor options
    if (Civi::settings()->get('radiobuttons_pp')) {
      radiobuttons_convert_element('.payment_processor-section .content', [
        'mandatory_field' => true,
        'button_width' => 400,
      ]);
    }

    // Fix the Other Amounts field
    if (Civi::settings()->get('radiobuttons_fix_other_amount')) {
      $pf_params = [
        'price_set_id' => $price_set_id,
        'is_active' => 1,
        'sort' => [
          'weight ASC',
        ],
      ];

      // If viewing a ContributionPage as admin, admin-only fields will be visible
      // This has an impact on whether we hide the Total Amount section or not.
      if (!CRM_Core_Permission::check('edit contributions')) {
        // Show only public fields
        $pf_params['visibility_id'] = 1;
      }

      $fields = civicrm_api3('PriceField', 'get', $pf_params)['values'];
      self::improveOtherAmountField($form, $priceset, $fields);
    }
  }

  /**
   * Hides the "Total Amount" if this is a non-quick priceset with only an
   * amount + other field. This mimics how the 'quick' priceset works.
   * Also shows the currency in the Other Amount field.
   */
  private static function improveOtherAmountField($form, $priceset, $fields) {
    // Expect the first field to be a radio button
    $first_field = array_shift($fields);
    if ($first_field['html_type'] != 'Radio') {
      return;
    }

    // Expect the second field to be a text field and has a label "Other Amount"
    // or "Other" (using core translation on purpose).
    $second_field = array_shift($fields);

    if (!($second_field['html_type'] == 'Text' && ($second_field['label'] == ts('Other Amount') || $second_field['label'] == ts('Other') || $second_field['name'] == 'other_amount'))) {
      return;
    }

    // Hide the Total Amount if there are only 2 fields (Amount and Other Amount)
    // We check for empty because we have been shifting the array above.
    if (empty($fields)) {
      Civi::resources()->addStyle('#pricesetTotal {display: none;}');
    }

    $first_id = 'price_' . $first_field['id'];
    $other_id = 'price_' . $second_field['id'];

    // When Other Amount is clicked, unselect radio amounts
    Civi::resources()->addScript('CRM.$("#' . $other_id . '").focus(function() {
      if (CRM.$("input[name=' . $first_id . ']").attr("type") == "radio") {
        // Removing "selected" class on the parent for radiobuttons compatibility.
        CRM.$("input[name=' . $first_id . ']").prop("checked", false).trigger("change").parent().removeClass("selected");
        // This is horrible, but required to reset the line_raw_total on the input
        // otherwise calculateAmount() will still think this is selected.
        calculateRadioLineItemValue("input[name=' . $first_id . ']");
      }
    });');

    // When radio amounts are clicked, remove the Other Amount
    Civi::resources()->addScript('CRM.$("input[name=' . $first_id . ']").click(function() {CRM.$("#' . $other_id . '").val("").trigger("change"); });');

    Civi::resources()->addScript('
      // Disable autocomplete/autofill
      CRM.$("#' . $other_id . '").attr("autocomplete", "off");

      // Convert to a type=number if inputmask is enabled
      if (typeof CRM.inputmasksNumericOnlyApply != "undefined") {
        CRM.inputmasksNumericOnlyApply("#' . $other_id . '");
      }
    ');

    // Remove the "- none -" from the first field.
    Civi::resources()->addStyle("#priceset .{$first_field['name']}-content .price-set-row:last-child {display: none;}");
    // If radio buttons are enabled
    Civi::resources()->addStyle("#priceset .{$first_field['name']}-content .crm-radio-wrapper:last-child {display: none;}");
  }

}
