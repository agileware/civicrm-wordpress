# Radio Buttons

![Screenshot](/images/screenshot.png)

Provides an alternative visual to CiviCRM radio buttons, such as on Event or
Contribution Pages. It can also be called from other extensions, such as
[recurringbuttons](https://lab.civicrm.org/extensions/recurringbuttons).

Once enabled, go to Administer > Customize Data and Screens > Radiobuttons
to enable the various features:

- Enhance the Contribution or Event page "amounts"
- Enhance the Payment Processor selection (on Contribution/Events pages)
- Fix the "Other Amount" field when using a Price Set on Contribution Pages (ex: if you have two fields: Amount and Other Amount, and the user first selects an amount, but then enters an amount in the Other Amount field, then the initial amount will be deselected).

The extension is licensed under [AGPL-3.0](LICENSE.txt).

## Requirements

* PHP v7.3+
* CiviCRM 5.x

## Installation

Install as a regular CiviCRM extension.

## Usage

The extension provides a default colour for buttons. You can override it in CSS.

It adds a new settings menu under Administer > Customize Data and Screens > Radiobutton Setings.

## Extending

You can enable radio elements to be replaced by buttons in your extension too.

1. Note the Form name of the form on which you want to make the change (e.g.
   `CRM_Contribute_Form_Contribution_Main`).

2. View the HTML code. Identify the immediate parent element of the set of
   radio elements.

3. Design a [jQuery selector](https://api.jquery.com/category/selectors/) to
   uniquely identify this exact element.

4. Use
   [`hook_civicrm_buildForm`](https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_buildForm/).
   Trigger only on your form, and call, for example:

```

// Match on the div element with class set to "content
// contribution_amount-content" that is a child of the
// div with the id "priceset".

$jquery_selector = '#priceset div.content.contribution_amount-content';
$original_class = 'content contribution_amount-content';
radiobuttons_convert_element($jquery_selector, [
  'mandatory_field' => true,
  'button_width' => 400,
  'preserve_class' => $original_class,
]);
```

## Known Issues

https://lab.civicrm.org/extensions/radiobuttons/-/issues

## Roadmap

* Provide admin setting for the button colours.
* Provide admin setting for the width/style of the buttons

## Support

Please post bug reports in the issue tracker of this project on CiviCRM's Gitlab:  
https://lab.civicrm.org/extensions/radiobuttons/-/issues

While we do our best to provide free community support for this extension,
please consider financially contributing to support or development of this
extension.

Commercial support is available through Coop SymbioTIC:  
https://www.symbiotic.coop/en

Coop Symbiotic is a worker-owned co-operative based in Canada. We have a strong
experience working with non-profits and CiviCRM. We provide affordable, fast,
turn-key hosting with regular upgrades and proactive monitoring, as well as custom
development and training.
