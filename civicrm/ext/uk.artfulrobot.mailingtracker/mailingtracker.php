<?php

require_once 'mailingtracker.civix.php';
// phpcs:disable
use CRM_Mailingtracker_ExtensionUtil as E;
// phpcs:enable

/**
 * Implements hook_civicrm_config().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_config/
 */
function mailingtracker_civicrm_config(&$config) {
  _mailingtracker_civix_civicrm_config($config);
}

/**
 * Implements hook_civicrm_install().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_install
 */
function mailingtracker_civicrm_install() {
  _mailingtracker_civix_civicrm_install();
}

/**
 * Implements hook_civicrm_enable().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_enable
 */
function mailingtracker_civicrm_enable() {
  _mailingtracker_civix_civicrm_enable();
}

// --- Functions below this ship commented out. Uncomment as required. ---

/**
 * Implements hook_civicrm_preProcess().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_preProcess
 */
//function mailingtracker_civicrm_preProcess($formName, &$form) {
//
//}

/**
 * Implements hook_civicrm_navigationMenu().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_navigationMenu
 */
function mailingtracker_civicrm_navigationMenu(&$menu) {
  _mailingtracker_civix_insert_navigation_menu($menu, 'Reports', [
    'label' => E::ts('Mailing tracker'),
    'name' => 'mailingtracker',
    'url' => 'civicrm/dataviz/MailingTracker',
    'permission' => 'access CiviMail',
    'operator' => 'OR',
    'separator' => 0,
  ]);
  _mailingtracker_civix_navigationMenu($menu);
}
