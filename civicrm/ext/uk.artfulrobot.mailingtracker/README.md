# Mailing Tracker

![Screenshot](/images/screenshot.png)

Graph and list your CiviMail mailing performance statistics: deliveries, bounces, open/click rate, unsubscribes. View aggregate stats by day/week/month and optionally for a set of mailings (filtering on subject/mailing name).

The extension is licensed under [AGPL-3.0](LICENSE.txt).

## Requirements

* PHP v7.4+
* CiviCRM 5.35
* Civisualize

## Installation (Web UI)

Learn more about installing CiviCRM extensions in the [CiviCRM Sysadmin Guide](https://docs.civicrm.org/sysadmin/en/latest/customize/extensions/).

## Installation (CLI, Zip)

Sysadmins and developers may download the `.zip` file for this extension and
install it with the command-line tool [cv](https://github.com/civicrm/cv).

```bash
cd <extension-dir>
cv dl mailingtracker@https://github.com/FIXME/mailingtracker/archive/master.zip
```

## Installation (CLI, Git)

Sysadmins and developers may clone the [Git](https://en.wikipedia.org/wiki/Git) repo for this extension and
install it with the command-line tool [cv](https://github.com/civicrm/cv).

```bash
git clone https://github.com/FIXME/mailingtracker.git
cv en mailingtracker
```

## Getting Started

Once installed, find the report at **Reports » Mailing Tracker**.

However, it won't have much/any data yet. You can click the link to try generating the data for the past 6 months’ mailings via your browser. You can generate stats for as far-back as you like using a command line API call, e.g. `cv api4 CachedMailingStats.syncStats since='today - 24 months'`

This is a one-off thing; it should stay up-to-date with current and future mailings automatically (it runs with every cron run, typically every 15 mins or so).

Notes on filtering mailings

- The Since cut-off is fairly self-explanatory.

- The search text is a case-insensitve sub-string match on title or subject. If you prefix with <code>!</code> it means all mailings <em>except</em> those that match.

- Clicking on a row in the table will select it, and mean that the chart only shows the selected mailing(s).

## Changes

### 1.3

- 🐞 Fix bug that happens sometimes when a SQL query returns "" as a `mailing_id`
  value, which then causes a php 8 rejection due to expecting an `int`
