<?php
return [
  'mailingtracker' => [
    'name' => 'mailingtracker',
    'title' => ts('Internal Mailing Tracker cache'),
    'description' => ts('You should not need to alter this. You can safely delete it. It is here to improve performance.'),
    'group_name' => 'domain',
    'type' => 'String',
    'default' => '',
    'is_domain' => 1,
    'is_contact' => 0,
  ],
];
