{crmTitle string="Mailings Tracker"}
{literal}<style>
div.dc-chart {
  float:none !important;
}
#mailing-tracker-chart-container {
  background:white;
  padding: 1rem;
  border-radius: 8px;
  margin-bottom: 2rem;
}
#mailing-tracker-chart {
}
form#mailing-tracker-filter {
  background:white;
  padding: 1rem;
  border-radius: 8px;
  margin-bottom: 2rem;
  display: flex;
  justify-content: space-between;
  flex-wrap: wrap;
}
form#mailing-tracker-filter>div {
  flex: 0 0 auto;
}
.mailing-subject {
  font-size: 0.8em;
}
#mailing-tracker-granularity-container label {
  margin-left: 2ch;
}

</style>
{/literal}
<div style="overflow:hidden">
  <div id="mailing-tracker-chart-container">
    <div id="mailing-tracker-chart"></div>
  </div>
  <form id="mailing-tracker-filter" >
    <div>
      <label style="display: block;" for="text-filter">Filter mailings by name or subject</label>
      <input id="text-filter" />
    </div>
    <div>
      <label style="display: block;" for="recipients-filter">Only mailings to more than</label>
      <input id="recipients-filter" type=number step=1 />
    </div>
    <div>
      <label style="display: block;" for="since-filter">Mailings in last</label>
      <select id="since-filter" value="6">
        <option value=1>1 month</option>
        <option value=3>3 months</option>
        <option value=6>6 months</option>
        <option value=12>12 months</option>
        <option value="">All data</option>
      </select>
    </div>
    <div id="mailing-tracker-granularity-container">
      <label><input name="granularity" type=radio checked value="month"/> Month</label>
      <label><input name="granularity" type=radio value="week"/> Week</label>
      <label><input name="granularity" type=radio value="day"/> Day</label>
    </div>
  </form>
  <table id="mailing-tracker-table">
    <tfoot>
      <tr><th colspan=2>Averages</th>
      <th><span id=avg-delivered></span><br>Delivered</th>
      <th><span id=avg-bounced></span>%<br>Bounces</th>
      <th><span id=avg-opened></span>%<br>Opens</th>
      <th><span id=avg-clicked></span>%<br>Clicks</th>
      <th><span id=avg-unsubscribed></span>%<br>Unsubscribes</th>
    </tfoot>
  </table>

  <div style="margin-top: 3rem; opacity: 0.8;">
    <p><strong>Not seeing any data?</strong> If you have just installed the extension you need to generate the stats for previous mailings first.
    This can take a long time if you have done a lot of big mailings. If you have access to the command line you can run:</p>
    <pre>cv api4 CachedMailingStats.syncStats since='today - 6 months'</pre>
    <p>Otherwise we can try to run it from your browser: <a href id="syncStatsNow">Generate stats now</a>. There's a chance that this will time out on you, but there's no harm in trying. Depending on your server set-up it may still work even if you do see a timeout error.</p>
  </div>
  <div class="help">
    <p>Note: you can click rows to show only those mailings in the chart. You
    can prefix the search text with <code>!</code> to negate it, e.g.
    <code>!welcome mailing</code> would <em>remove</em> all that match
    <code>welcome mailing</code>.</p>
  </div>
</div>
<script>
{literal}
var mailings=[], loaded=false;
  // We do everything in an anonymous function so we are free to use any
  // variable names we need without clobbering those in the global scope.
  (function() {
    // Define the function that will make your charts etc.

    let chartData = [];

    let ndx,
      ts = CRM.ts(),
      mailingFilter,
      tableData,
      mailingsIdx = {},
      percentagesGroup,
      lineDims,
      lineDim,
      granularity = 'month',
      // DOM nodes
      chartContainerNode,
      tableContainerNode,
      textInput = document.getElementById('text-filter'),
      countInput = document.getElementById('recipients-filter'),
      sinceInput = document.getElementById('since-filter'),
      statsNow = document.getElementById('syncStatsNow'),
      statsNowN = 6,
      statsNowRunning = false,
      lineChart,
      resizeTimeout,
      // Local versions:
      d3, dc, crossfilter;
    ;

    //[].forEach.call(document.querySelectorAll('input[name="granularity"]:not(*[value="month"])'), el => el.checked = false);
    document.querySelector('input[name="granularity"][value="month"]').checked = true;
    textInput.value = '';
    sinceInput.value = '6';

    let el = document.createElement('div');
    function htmlSafe(text) {
      el.textContent = text;
      return el.innerHTML;
    }

    function bootViz() {
      // Ensure we're using the up-to-date libraries, not the CiviCRM core ones.
      d3 = CRM.civisualize.d3;
      dc = CRM.civisualize.dc;
      crossfilter = CRM.civisualize.crossfilter;
      filtersUI();
      loadData();
      window.addEventListener('resize', e => {
        clearTimeout(resizeTimeout);
        if (lineChart) {
          resizeTimeout = setTimeout(() => {
            lineChart.width(chartContainerNode.clientWidth);
            lineChart.redraw();
          }, 300);
        }
      });
    }

    function matchesTextFilter(subject) {
      if (!filterText || filterText === '!') return true;
      if (!subject) {
        return false;
      }
      let invert = (filterText[0] === '!')
        needle = invert ? filterText.substr(1) : filterText,
        match = subject.indexOf(needle) > -1;
      match = invert ? !match: match;

      return match;
    }

    function matchesCountFilter(recipients) {
      if (filterCount < 0) {
        return (-filterCount >= recipients);
      }
      else {
        return (filterCount <= recipients);
      }
    }

    function loadData() {
      /* Use this for testing.
      return Promise.resolve([
        { mailing_id: 1, scheduled_date: '2021-01-01', mailing: '#A', subject: 'subject A', bounces: 2, delivered: 98, unsubscribes: 0, unique_opens: 20, unique_clicks: 10 },
        { mailing_id: 2, scheduled_date: '2021-02-01', mailing: '#B', subject: 'subject B', bounces: 5, delivered: 95, unsubscribes: 0, unique_opens: 30, unique_clicks: 20 },
        { mailing_id: 3, scheduled_date: '2021-03-01', mailing: '#C', subject: 'subject C', bounces: 0, delivered: 200, unsubscribes: 0, unique_opens: 60, unique_clicks: 30 }
      ])
      */
      return CRM.api4('CachedMailingStats', 'get', { forReport: true })
      .then(r => {
        if (r.length === 0) {
          return;
        }
        // Import the data.
        chartData = r;

        // parse/prepare data
        chartData.forEach(row => {
          // Safari cannot parse Y-m-d format dates.
          row.scheduled_date = new Date(row.scheduled_date.replace(/-/g, "/"));
          row.humanDate = row.scheduled_date.toLocaleString(undefined, {day: 'numeric', month: 'short', year: 'numeric'});
          row.month = d3.timeMonth(row.scheduled_date);
          row.day = d3.timeDay(row.scheduled_date);
          row.week = d3.timeMonday(row.scheduled_date);
          row.filterText = (row.mailing + ' ' + row.subject).toLowerCase();
          row.mailing = htmlSafe(row.mailing);
          row.subject = htmlSafe(row.subject);
          row.bounceRate = row.bounces * 100 / (row.bounces + row.delivered);
          if (row.delivered > 0) {
            row.openRate = row.unique_opens * 100 / row.delivered;
            row.clickRate = row.unique_clicks * 100 / row.delivered;
            row.unsubscribeRate = row.unsubscribes * 100 / row.delivered;
          }
          else {
            row.openRate = 0;
            row.clickRate = 0;
            row.unsubscribeRate = 0;
          }
          mailingsIdx[row.mailing_id] = row;
        });

        // Create crossfilter
        ndx = crossfilter(chartData);

        if (mailingFilter) {
          mailingFilter.dispose();
        }
        mailingFilter = ndx.dimension(d => d.mailing_id);
        // The table can't be a dimension because that would filter out items
        // that are not selected.
        // So we create a fake dimension that contains the data filtered only by
        // the textfilter and since.
        tableData = {
          top: () => {
            let since = sinceDate();
            return chartData.filter(row => {
              // console.log("erm", row, since);
              if (since && since > row.scheduled_date) {
                // Always rule out if exceeds cut-off.
                return false;
              }
              return matchesTextFilter(row.filterText) && matchesCountFilter(row.delivered);
            });
          },
          // Not sure this is used.
          bottom: () => {
            return tableData.top().reverse();
          },
        };


        // Group by date
        if (lineDims) {
          lineDims.month.dispose();
          lineDims.day.dispose();
          lineDims.week.dispose();
        }
        lineDims = {
          month: ndx.dimension(d => d.month),
          day  : ndx.dimension(d => d.day),
          week  : ndx.dimension(d => d.week),
        };

        // Find DOM nodes
        chartContainerNode = document.getElementById('mailing-tracker-chart');
        tableContainerNode = document.getElementById('mailing-tracker-table');

        // Create the charts if first call.
        createLineChart();
        createTable();
        createAvgs();
        loaded = true;
        // (re)render charts.
        dc.renderAll();

      });
    }

    function createLineChart() {
      chartContainerNode.style.float = 'none';
      // Select the dimension
      lineDim = lineDims[granularity];
      // Remove previous chart.
      if (lineChart) {
        chartContainerNode.innerHTML = '';
      }
      lineChart = dc.compositeChart(chartContainerNode);

      var min = sinceDate() || d3.timeDay.offset(d3.min(chartData, (d) => d[granularity]), 0);
      var max = d3.timeDay.offset(d3.max(chartData, (d) => d[granularity]), 0);

      var cMax = d3.max(chartData, (d) => d.delivered),
          o = 10 ** (cMax.toString().length -1);
      cMax = Math.ceil(cMax / o) * o;

      const updateCalcs = p => {
        p.bounces = p.bouncesTotal / (p.deliveredTotal + p.bouncesTotal) * 100;
        if (p.deliveredTotal > 0) {
          p.unique_clicks = p.unique_clicksTotal / p.deliveredTotal * 100;
          p.unique_opens = p.unique_opensTotal / p.deliveredTotal * 100;
          p.unsubscribes = p.unsubscribesTotal / p.deliveredTotal * 100;
        }
        else {
          p.unique_clicks = null;
          p.unique_opens = null;
          p.unsubscribes = null;
        }
      };
      percentagesGroup = lineDim.group().reduce(
        (p, v) => {
          p.deliveredTotal += v.delivered;
          p.bouncesTotal += v.bounces;
          p.unique_clicksTotal += v.unique_clicks;
          p.unique_opensTotal += v.unique_opens;
          p.unsubscribesTotal += v.unsubscribes;
          updateCalcs(p);
          // bounces as a percent.
          return p;
        },
        (p, v) => {
          p.deliveredTotal -= v.delivered;
          p.bouncesTotal -= v.bounces;
          p.unique_clicksTotal -= v.unique_clicks;
          p.unique_opensTotal -= v.unique_opens;
          p.unsubscribesTotal -= v.unsubscribes;
          // bounces as a percent.
          updateCalcs(p);
          return p;
        },
        () => ({
          deliveredTotal: 0,
          bouncesTotal: 0,
          unique_clicksTotal: 0,
          unique_opensTotal: 0,
          unsubscribesTotal: 0,
          bounces: 0,
          unsubscribes: 0,
        })
      );
    console.log({percentagesGroup});

      function label(k) {
        return d => (d.value[k] || 0).toFixed(2) + "%";
      };

      lineChart
      //.width(container.clientWidth)
      .width(chartContainerNode.clientWidth)
      .height(300)
      .margins({top: 10, right: 80, bottom: 30, left: 40})
      .dimension(lineDim)
      .group(percentagesGroup)
      .x(d3.scaleTime().domain([min, max]))
      .y(d3.scalePow().domain([0, 100]).exponent(0.4))
      // This is the padding
      .yAxisPadding('10%')
      .legend(dc.legend().x(70).y(10).itemHeight(13).gap(5).horizontal(true).autoItemWidth(true))
      // Without this next line, the .title() calls are ignored.
      .shareTitle(false)
      //.xUnits(d3.timeDays)
      //.round(d3.timeDay.round)
      .compose([
        new dc.lineChart(lineChart)
          .group(percentagesGroup, "Delivered")
          .valueAccessor(d => d.value.deliveredTotal)
          .ordinalColors(["#d8d8d8"])
          .useRightYAxis(true)
          .renderArea(true)
          .title(d => d.value.deliveredTotal.toLocaleString())
          .interpolate("cardinal")
          ,
        new dc.lineChart(lineChart)
          .group(percentagesGroup, "Bounce rate")
          .valueAccessor(d => d.value.bounces)
          .ordinalColors(["#aa2929"])
          .title(label('bounces'))
          .interpolate("cardinal")
          ,
        new dc.lineChart(lineChart)
          .group(percentagesGroup, "Open rate")
          .valueAccessor(d => d.value.unique_opens || null)
          .renderTitle(true)
          .title(label('unique_opens'))
          .ordinalColors(["#35c"])
          .interpolate("cardinal")
          ,
        new dc.lineChart(lineChart)
          .group(percentagesGroup, "Click rate")
          .valueAccessor(d => d.value.unique_clicks ||null)
          .ordinalColors(["#3c5"])
          .title(label('unique_clicks'))
          .interpolate("cardinal")
          ,
        new dc.lineChart(lineChart)
          .group(percentagesGroup, "Unsubscribe rate")
          .valueAccessor(d => d.value.unsubscribes || null)
          .ordinalColors(["#ee0d0d"])
          .title(label('unsubscribes'))
          .interpolate("cardinal")
          ,
      ])
      .yAxisLabel('Rate (%)')
      .renderHorizontalGridLines(true)
      .rightYAxisLabel('Delivered')
      .brushOn(false)
      //.mouseZoomable(true)
      //.elasticY(true)
      ;
      lineChart.rightYAxis().tickFormat(d => {
        let magnitude = Math.min(2, parseInt(d.toString().length/3));
        let unit = ['', 'k', 'M'][magnitude];
        let denominator = [1, 1000, 1000000][magnitude];
        return (d/denominator).toFixed(1) + unit + '     ';
      })
      .tickPadding(10);
      lineChart.yAxis().tickValues([0, 0.5, 1, 2, 3, 5, 7, 10, 20, 40, 60, 80, 100])
      .tickFormat(d => {
        if (d < 1 && d > 0) {
          return d.toFixed(1);
        }
        return d;
      });

    }

    let selectedMailings = [], filterText = '', filterCount = 0;

    function sinceDate() {
      if (sinceInput.value) {
        since = new Date();
        since.setMonth(since.getMonth() - sinceInput.value);
        return since;
      }
    }

    function applyMailingFilter() {
      let since = sinceDate();

      mailingFilter.filter(mailing_id => {
        let mailing = mailingsIdx[mailing_id];

        if (since && since > mailing.scheduled_date) {
          // Always rule out if exceeds cut-off.
          return false;
        }

        // Remove if we have a selection, and this isn't in it.
        if (selectedMailings.length > 0
          && !selectedMailings.includes(mailing_id)) {
          console.log("Mailing ", mailing_id, " is not in selected mailings: ", selectedMailings);
          return false;
        }

        return matchesTextFilter(mailing.filterText) && matchesCountFilter(mailing.delivered);
      });

      dc.redrawAll();
    }

    let mfdeb;
    function applyMailingFilterDebounced() {
      if (mfdeb) {
        clearTimeout(mfdeb);
      }
      mfdeb = setTimeout(applyMailingFilter, 400);
    }

    function filtersUI() {
      // create filter.
      textInput.addEventListener('input', e => {
        filterText = textInput.value.toLowerCase();
        applyMailingFilterDebounced();
      });

      countInput.addEventListener('change', e => {
        filterCount = countInput.value;
        applyMailingFilterDebounced();
      });

      sinceInput.addEventListener('change', e => {
        if (sinceInput.value == 1 && granularity === 'month') {
          d3.select('input[name="granularity"][value="week"]').each((x, y, z) => {
            z[0].checked = true;
          });
          granularity = 'week';
        }
        createLineChart();
        lineChart.render();
      });


      let granularityInputs = d3.selectAll('input[name="granularity"]')
        .on('change', function() {
          granularity = event.target.value;
          createLineChart();
          lineChart.render();
        });
    }
    let table;
    function createTable() {
      table = new dc.dataTable(tableContainerNode);
      let tableSort = ['scheduled_date', 'descending'];
      table._doColumnHeaderFormat = h => {
        return '<a href="#" data-sort-key="' + h.sortKey + '" >' + htmlSafe(h.label) + '</a>';
      };
      table.on('renderlet', c => {
        c.selectAll('a[data-sort-key]').on('click', () => {
          event.preventDefault();
          event.stopPropagation();
          let sortKey = event.target.dataset.sortKey;
          if (tableSort[0] === sortKey) {
            if (tableSort[1] === 'descending') {
              tableSort[1] = 'ascending';
            }
            else {
              tableSort[1] = 'descending';
            }
          }
          else {
            tableSort = [sortKey, (sortKey === 'mailing') ? 'ascending' : 'descending'];
          }
          table.order(d3[tableSort[1]]);
          table.redraw();
        });
        c.selectAll('td:nth-child(3), td:nth-child(4), td:nth-child(5), td:nth-child(6), td:nth-child(7)').style('text-align', 'right');
        c.selectAll('tbody>tr')
        .classed('selected', (d) => {
          return selectedMailings.indexOf(d.mailing_id) > -1;
        })
        .on('click', (d) => {
          if (d.mailing_id) {
            let idx = selectedMailings.indexOf(d.mailing_id);
            if (idx === -1) {
              selectedMailings.push(d.mailing_id);
            }
            else {
              selectedMailings.splice(idx, 1);
            }
            table.redraw();
            applyMailingFilterDebounced();
          }
        });


      });
      table
        .dimension(tableData)
        //.group(mailingFilter.group().all())
        .sortBy(d => d[tableSort[0]])
        .order(d3[tableSort[1]])
        //.section(d => '')
        .showSections(false)
        .size(Infinity)
        .columns([
          {
            label: 'Sent',
            sortKey: 'scheduled_date',
            format: d => d.humanDate,
          },
          {
            label: 'Mailing',
            sortKey: 'mailing',
            format: d => '<div class="mailing-name"><a href="'
            + CRM.url('civicrm/mailing/report', {reset:1, mid: d.mailing_id})
            + '" target="_blank" title="View Mailing Report">'
            + htmlSafe(d.mailing) + '</a></div><div class="mailing-subject">' + htmlSafe(d.subject) + '</div>'
          },
          {
            label: 'Delivered',
            sortKey: 'delivered',
            format: d => d.delivered.toLocaleString()
          },
          {
            label: 'Bounced',
            sortKey: 'bounceRate',
            format: d => '<span title="' + d.bounces.toLocaleString() + '">' + d.bounceRate.toFixed(1) + '%</span>',
          },
          {
            label: 'Opened',
            sortKey: 'openRate',
            format: d => '<span title="' + d.unique_opens.toLocaleString() + '">' + d.openRate.toFixed(1) + '%</span>',
          },
          {
            label: 'Clicked',
            sortKey: 'clickRate',
            format: d => '<span title="' + d.unique_clicks.toLocaleString() + '">' + d.clickRate.toFixed(1) + '%</span>',
          },
          {
            label: 'Unsubscribes',
            sortKey: 'unsubscribeRate',
            format: d => '<span title="' + d.unsubscribes.toLocaleString() + '">' + d.unsubscribeRate.toFixed(1) + '%</span>',
          },
        ])
      ;
      loaded = true;
    };

    function createAvgs() {
      (new dc.numberDisplay('#avg-delivered'))
      .group({value: () => {
          const rows = tableData.top();
          return (rows.length > 0)
          ? rows.reduce(
              (acc, newVal) => { return acc + (newVal.delivered ?? 0);}, 0
              ) / rows.length
          : '';
          }}
          )
      .formatNumber(d3.format(',.0f'))
      .valueAccessor(d => d);

      (new dc.numberDisplay('#avg-bounced'))
      .group({value: () => {
          const rows = tableData.top();
          return (rows.length > 0)
          ? rows.reduce((acc, newVal) => { return acc + (isNaN(newVal.bounceRate) ? 0 : newVal.bounceRate);}, 0) / rows.length
          : '';
          }}
          )
      .formatNumber(d3.format(',.1f'))
      .valueAccessor(d => d);


      (new dc.numberDisplay('#avg-opened'))
      .group({value: () => {
          const rows = tableData.top();
          return (rows.length > 0)
          ? rows.reduce(
              (acc, newVal) => { return acc + (newVal.openRate ?? 0);}, 0
              ) / rows.length
          : '';
          }}
          )
      .formatNumber(d3.format(',.1f'))
      .valueAccessor(d => d);

      (new dc.numberDisplay('#avg-clicked'))
      .group({value: () => {
          const rows = tableData.top();
          return (rows.length > 0)
          ? rows.reduce(
              (acc, newVal) => { return acc + (newVal.clickRate ?? 0);}, 0
              ) / rows.length
          : '';
          }}
          )
      .formatNumber(d3.format(',.1f'))
      .valueAccessor(d => d);

      (new dc.numberDisplay('#avg-unsubscribed'))
      .group({value: () => {
          const rows = tableData.top();
          return (rows.length > 0)
          ? rows.reduce(
              (acc, newVal) => { return acc + (newVal.unsubscribeRate ?? 0);}, 0
              ) / rows.length
          : '';
          }}
          )
      .formatNumber(d3.format(',.1f'))
      .valueAccessor(d => d);
    }

    // Boot our script as soon as ready.
    CRM.civisualizeQueue = CRM.civisualizeQueue || [];
    CRM.civisualizeQueue.push(bootViz);
  })(); // Immediately call our anonymous function.
{/literal}
</script>

