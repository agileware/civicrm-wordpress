<?php
use CRM_Mailingtracker_ExtensionUtil as E;

/**
 * Job.Mailingtrackersync API specification (optional)
 * This is used for documentation and validation.
 *
 * @param array $spec description of fields supported by this API call
 *
 * @see https://docs.civicrm.org/dev/en/latest/framework/api-architecture/
 */
function _civicrm_api3_job_Mailingtrackersync_spec(&$spec) {
}

/**
 * Job.Mailingtrackersync API
 *
 * @param array $params
 *
 * @return array
 *   API result descriptor
 *
 * @see civicrm_api3_create_success
 *
 * @throws API_Exception
 */
function civicrm_api3_job_Mailingtrackersync($params) {
  $result = \Civi\Api4\CachedMailingStats::syncStats(FALSE)->execute()->getArrayCopy();
  return civicrm_api3_create_success($result, $params, 'Job', 'Mailingtrackersync');
}
