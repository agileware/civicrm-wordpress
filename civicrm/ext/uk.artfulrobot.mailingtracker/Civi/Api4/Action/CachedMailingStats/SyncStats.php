<?php
namespace Civi\Api4\Action\CachedMailingStats;

use Civi\Api4\Generic\Result;
use Civi\Api4\CachedMailingStats;
use CRM_Core_DAO;
use Civi;

/**
 *
 */
class SyncStats extends \Civi\Api4\Generic\AbstractAction {

  /**
   * Process mailings with events since...
   * @param string
   */
  protected $since;

  /**
   * @param int mailingID
   */
  protected $mailingID;

  /**
   *
   */
  public function _run(Result $result) {

    $maxIDs = $this->getEventTableLatestIDs();

    if (!empty($this->since)) {
      $this->setMailingIDsFromSince();
    }
    elseif (empty($this->since) && empty($this->mailingID)) {
      $meta = Civi::settings()->get('mailingtracker');
      $meta = $meta ? json_decode($meta, TRUE) : NULL;
      if ($meta) {
        // OK, we can use most efficient method.
        $this->setMailingIDsFromEventTables($meta);
      }
      else {
        // Not available. Do last week's mailings.
        $this->setMailingIDsFromSince();
      }
    }
    if (!empty($this->mailingID)) {
      if (!is_array($this->mailingID)) {
        $this->mailingID = [$this->mailingID];
      }

      $result['mailings'] = [];
      foreach ($this->mailingID as $mailingID) {
        $result['mailings'][] = $this->syncStatsForMailing($mailingID);
      }
    }

    // Civi::log()->info("mailingtracker: storing ids: " . $maxIDs);
    Civi::settings()->set('mailingtracker', $maxIDs);

    return $result;
  }

  protected function setMailingIDsFromEventTables(array $meta) {
    $startTime = microtime(TRUE);
    $uniqueMailingIDs = [];
    foreach ([
      'civicrm_mailing_event_bounce',
      'civicrm_mailing_event_delivered',
      'civicrm_mailing_event_opened',
      'civicrm_mailing_event_trackable_url_open',
      'civicrm_mailing_event_unsubscribe',
    ] as $tableName) {

      $id = (int) $meta[$tableName];

      // Since 5.67, mailing_id is available on the event queue.
      $sql = "SELECT qu.mailing_id
        FROM $tableName ev
        INNER JOIN civicrm_mailing_event_queue qu ON qu.id = ev.event_queue_id
        WHERE ev.id > $id
        GROUP BY qu.mailing_id
      ";
      $dao = CRM_Core_DAO::executeQuery($sql);
      while ($dao->fetch()) {
        if ($dao->mailing_id) {
          // Haven't found out what returns "" as a mailing ID, but something does, and I don't want it!
          $uniqueMailingIDs[$dao->mailing_id] = 1;
        }
      }
    }
    $this->mailingID = array_keys($uniqueMailingIDs);
    // Civi::log()->info("mailingtracker: took " . (microtime(TRUE) - $startTime) . "s to check for mailings we need to update");
  }

  protected function setMailingIDsFromSince() {
    $since = strtotime(empty($this->since) ? 'today - 1 week' : $this->since);
    if (!$since) {
      throw new \InvalidArgumentException("Invalid 'since' parameter.");
    }
    // SQL-safe date
    $since = date('Ymd', $since);

    // Get the list of mailings we care to sync
    $sql = <<<SQL
      SELECT DISTINCT job.mailing_id
      FROM civicrm_mailing_job job
      WHERE job.start_date > $since
      SQL;
    $dao = CRM_Core_DAO::executeQuery($sql);
    $this->mailingID = [];
    while ($dao->fetch()) {
      $this->mailingID[] = $dao->mailing_id;
    }
  }

  /**
   *
   */
  protected function syncStatsForMailing(int $mailingID) {
    // Civi::log()->info("Starting CachedMailingStats.syncStatsForMailing", ['=start' => "mailing$mailingID", '=timed' => 1]);

    $record = [
      'mailing_id' => $mailingID,
      'updated_date' => date('YmdHis'),
    ];
    // Find existing.
    $cacheID = CachedMailingStats::get(FALSE)
      ->addWhere('mailing_id', '=', $mailingID)
      ->execute()->first()['id'] ?? NULL;

    if ($cacheID) {
      $record['id'] = (int) $cacheID;
    }

    // Table name to column name.
    $map = [
      'civicrm_mailing_event_bounce' => 'bounces',
      //'civicrm_mailing_event_delivered' => 'delivered',
      'civicrm_mailing_event_opened' => 'unique_opens',
      'civicrm_mailing_event_trackable_url_open' => 'unique_clicks',
      'civicrm_mailing_event_unsubscribe' => 'unsubscribes',
    ];

    // Q. is this any faster than doing all in one query like Civi does?
    foreach ($map as $tableName => $column) {
      $sql = "SELECT COUNT(DISTINCT qu.contact_id)
        FROM $tableName ev
        INNER JOIN civicrm_mailing_event_queue qu ON qu.id = ev.event_queue_id AND qu.mailing_id = $mailingID
      ";
      // Civi::log()->debug("Count table $tableName for mailing $mailingID", ['=' => 'start,timed', 'sql' => $sql]);
      $record[$column] = (int) CRM_Core_DAO::singleValueQuery($sql);
      // Civi::log()->debug("", ['=' => 'pop']);
    }

    $sql = "SELECT COUNT(DISTINCT qu.contact_id)
      FROM civicrm_mailing_event_delivered ev
      INNER JOIN civicrm_mailing_event_queue qu ON qu.id = ev.event_queue_id AND qu.mailing_id = $mailingID
      LEFT JOIN civicrm_mailing_event_bounce bounce ON qu.id = bounce.event_queue_id
      WHERE bounce.id IS NULL
    ";
    // Civi::log()->debug("Count delivered for mailing $mailingID", ['=' => 'start,timed', 'sql' => $sql]);
    $record['delivered'] = (int) CRM_Core_DAO::singleValueQuery($sql);
    // Civi::log()->debug("", ['=' => 'pop']);

    CachedMailingStats::save(FALSE)
      ->setRecords([$record])
      ->execute();;

    // Civi::log()->info("", ['=' => 'pop']);
    return $record;
  }

  protected function getEventTableLatestIDs() {
    $ids = [];
    foreach ([
      'civicrm_mailing_event_bounce',
      'civicrm_mailing_event_delivered',
      'civicrm_mailing_event_opened',
      'civicrm_mailing_event_trackable_url_open',
      'civicrm_mailing_event_unsubscribe',
    ] as $tableName) {
      $sql = "SELECT id FROM $tableName ev ORDER BY id DESC LIMIT 1";
      $ids[$tableName] = (int) CRM_Core_DAO::singleValueQuery($sql);
    }
    return json_encode($ids);
  }

}
