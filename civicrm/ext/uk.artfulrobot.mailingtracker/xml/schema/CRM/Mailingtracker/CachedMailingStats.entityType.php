<?php
// This file declares a new entity type. For more details, see "hook_civicrm_entityTypes" at:
// https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_entityTypes
return [
  [
    'name' => 'CachedMailingStats',
    'class' => 'CRM_Mailingtracker_DAO_CachedMailingStats',
    'table' => 'civicrm_cached_mailing_stats',
  ],
];
