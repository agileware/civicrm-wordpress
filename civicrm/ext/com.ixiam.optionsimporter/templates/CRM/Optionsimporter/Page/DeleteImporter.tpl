{crmScope extensionKey='optionsimporter'}
<h3>{ts}Options deleted{/ts} ({$number_elements_deleted})</h3>
<div id="crm-main-content-wrapper">
  <table class="crm-option-selector dataTable no-footer">
    <thead>
      <tr class="columnheader">
        <th class="crm-custom_option-label"> Value </th>
        <th class="crm-custom_option-label"> Label </th>
      </tr>
    </thead>
   {foreach key=key item=item from=$value_deleted}
    <tr>
      <td class="crm-optionsimporter-deletedvalue">{$key}</td>
      <td class="crm-optionsimporter-deletedlabel">{$item}</td>
    </tr>
    {/foreach}
  </table>
  <br />
  <h3>{ts}Options not deleted{/ts} ({$number_elements_not_deleted})</h3>
  <table class="crm-option-selector dataTable no-footer">
    <thead>
      <tr class="columnheader">
        <th class="crm-custom_option-label"> Value </th>
        <th class="crm-custom_option-label"> Label </th>
      </tr>
    </thead>
    {foreach key=key item=item from=$value_not_deleted}
    <tr>
      <td class="crm-optionsimporter-deletedvalue">{$key}</td>
      <td class="crm-optionsimporter-deletedlabel">{$item}</td>
    </tr>
    {/foreach}
  </table>
</div>
{/crmScope}
