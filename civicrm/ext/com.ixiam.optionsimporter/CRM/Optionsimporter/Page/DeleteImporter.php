<?php

class CRM_Optionsimporter_Page_DeleteImporter extends CRM_Core_Page {
  protected $_gid;
  protected $_ogid;
  protected $_fid;

  public function run() {
    $this->_fid = CRM_Utils_Request::retrieve('fid', 'Positive', $this);
    $this->_gid = CRM_Utils_Request::retrieve('gid', 'Positive', $this);

    $results = civicrm_api3("CustomField", "getsingle", ['id' => $this->_fid]);

    if (!isset($results["is_error"])) {
      $this->_ogid = $results["option_group_id"];
    }
    else {
      CRM_Core_Error::fatal(ts('Wrong Custom Field!!'));
      return;
    }

    CRM_Utils_System::setTitle(ts('Deleted Values'));

    $values_field = civicrm_api3("OptionValue", "get", [
      'sequential' => '1',
      'option_group_id' => $results["option_group_id"],
      'option.limit' => 0,
    ]);
    $number_elements_deleted     = 0;
    $number_elements_not_deleted = 0;
    $value_deleted               = [];
    $value_not_deleted           = [];

    foreach ($values_field['values'] as $key => $value) {
      $results_error = civicrm_api3("OptionValue", "delete", [
        'sequential' => '1',
        'id' => $value['id'],
      ]);
      if ($results_error['is_error'] == 0) {
        $number_elements_deleted++;
        $value_deleted[$value['label']] = $value['value'];
      }
      else {
        $number_elements_not_deleted++;
        $value_not_deleted[$value['label']] = $value['value'];
      }
    }
    $this->assign('gid', $this->_gid);
    $this->assign('value_deleted', $value_deleted);
    $this->assign('value_not_deleted', $value_not_deleted);
    $this->assign('number_elements_deleted', $number_elements_deleted);
    $this->assign('number_elements_not_deleted', $number_elements_not_deleted);

    $statusMsg = "Options Deleted:" . $number_elements_deleted . ", Options Not Deleted:" . $number_elements_not_deleted;
    CRM_Core_Session::setStatus($statusMsg, FALSE);
    parent::run();
  }

}
