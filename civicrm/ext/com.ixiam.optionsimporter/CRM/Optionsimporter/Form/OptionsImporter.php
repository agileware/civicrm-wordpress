<?php

use Civi\Api4\OptionValue;
use CRM_Optionsimporter_ExtensionUtil as E;

class CRM_Optionsimporter_Form_OptionsImporter extends CRM_Core_Form {
  const
    VALUE_LABEL = 1,
    LABEL_VALUE = 2,
    VALUE = 3,
    SKIP_OPTION = 2,
    OVERWRITE_LABEL = 3;

  protected $_gid;
  protected $_ogid;
  protected $_fid;

  /**
   * Build the form
   *
   * @access public
   * @return void
   */
  function buildQuickForm() {
    $this->_fid = CRM_Utils_Request::retrieve('fid', 'Positive', $this);
    $this->_gid = CRM_Utils_Request::retrieve('gid', 'Positive', $this);

    //Get group
    $field_name = "";
    $results = civicrm_api3("CustomField", "getsingle", ['id' => $this->_fid]);

    if (!isset($results["is_error"])) {
      $this->_ogid = $results["option_group_id"];
    }
    else {
      throw new Exception('Wrong Custom Field!!');
    }
    // Get Custom Field Label
    $result = civicrm_api3("CustomField", "getsingle", ['sequential' => '1', 'id' => $this->_fid]);
    if (!isset($result['is_error'])) {
      $field_name = $result["label"];
    }
    else {
      throw new Exception('Wrong Custom Field!!');
    }

    CRM_Utils_System::setTitle("$field_name - " . E::ts('Import Option Values'));

    //Setting Upload File Size
    $config = CRM_Core_Config::singleton();
    $maxFileSize = $config->maxFileSize;
    $this->assign('uploadSize', $maxFileSize);

    $this->add('file', 'uploadFile', E::ts('Import Data File'), ['size' => 30, 'maxlength' => 255], TRUE);

    $this->addRule('uploadFile', E::ts('A valid file must be uploaded.'), 'uploadedfile');
    $this->addRule('uploadFile', E::ts('File size should be less than %1 MByte(s)',
        [1 => $maxFileSize]
      ),
      'maxfilesize',
      $maxFileSize * 1024 * 1024
    );
    $this->setMaxFileSize($maxFileSize * 1024 * 1024);
    $this->addRule('uploadFile', E::ts('Input file must be in CSV format'), 'utf8File');

    $this->addElement('checkbox', 'skipColumnHeader', E::ts('First row contains column headers'));

    $this->addElement('text', 'fieldSeparator', E::ts('Import Field Separator'), [
      'size' => 2,
      'maxlength' => 1
    ]);
    $this->addElement('text', 'textEnclosure', E::ts('Field Text Enclosure'), [
      'size' => 1,
      'maxlength' => 1
    ]);

    $overrideOptions[self::SKIP_OPTION] = $this->createElement('radio', NULL, E::ts('Skip Option'), "Skip Option", self::SKIP_OPTION);
    $overrideOptions[self::OVERWRITE_LABEL] = $this->createElement('radio', NULL, E::ts('Overwrite Label'), "Overwrite Label", self::OVERWRITE_LABEL);

    $this->addGroup($overrideOptions, 'overrideimport', E::ts('If Option Value Exists'));

    $colOrder = [
      self::VALUE_LABEL => E::ts("2 columns (value, label)"),
      self::LABEL_VALUE => E::ts("2 columns (label, value)"),
      self::VALUE => E::ts("only 1 column (label will be same as value)")
    ];
    $this->add('select', 'colOrder', E::ts('Columns Order'), $colOrder, TRUE);

    $this->addButtons([
      [
        'type' => 'upload',
        'name' => E::ts('Import >>'),
        'spacing' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
        'isDefault' => TRUE
      ],
      [
        'type' => 'cancel',
        'name' => E::ts('Cancel')
      ]
    ]);

  }

  function setDefaultValues() {
    $config = CRM_Core_Config::singleton();
    $defaults = [
      'fieldSeparator' => $config->fieldSeparator,
      'textEnclosure' => "'",
      'overrideimport' => self::SKIP_OPTION,
    ];

    return $defaults;
  }

  /**
   * @param string $value
   * @param string $label
   *
   * @throws \CiviCRM_API3_Exception
   */
  public function insertValue_customfield(string $value, string $label) {
    try {
      OptionValue::create(FALSE)
        ->addValue('option_group_id', $this->_ogid)
        ->addValue('label', $label)
        ->addValue('value', $value)
        ->execute();
    }
    catch (Exception $e) {
      \Civi::log()->error($e->getMessage() . "; value=$value; label=$label. Skipping!");
    }
  }

  /**
   * process the form after the input has been submitted and validated
   */
  public function postProcess() {
    $skipColumnHeader = $this->controller->exportValue($this->_name, 'skipColumnHeader');
    $separator        = $this->controller->exportValue($this->_name, 'fieldSeparator');
    $colOrder         = $this->controller->exportValue($this->_name, 'colOrder');
    $fileName         = $this->controller->exportValue($this->_name, 'uploadFile');
    $textEnclosure    = $this->controller->exportValue($this->_name, 'textEnclosure');
    $override_import  = $this->controller->exportValue($this->_name, 'overrideimport');
    $lineCount        = 0;
    $lineUpdate       = 0;

    if (empty($separator)) {
      $config = CRM_Core_Config::singleton();
      $separator = $config->fieldSeparator;
    }

    if (!is_array($fileName)) {
      throw new Exception('$fileName is not array');
    }
    $fileName = $fileName['name'];

    $fd = fopen($fileName, "r");
    if (!$fd) {
      throw new Exception('File could not be opened');
    }

    if ($skipColumnHeader) {
      fgets($fd);
    }

    while (($values = fgetcsv($fd, 8192, $separator)) !== FALSE) {
      if (CRM_Utils_System::isNull($values)) {
        continue;
      }

      $label = $value = "";
      if ($colOrder == self::VALUE_LABEL) {
        $value = $this->_encloseAndTrim($values[0], $textEnclosure);
        $label = $this->_encloseAndTrim($values[1], $textEnclosure);
      }
      elseif ($colOrder == self::LABEL_VALUE) {
        $label = $this->_encloseAndTrim($values[0], $textEnclosure);
        $value = $this->_encloseAndTrim($values[1], $textEnclosure);
      }
      elseif ($colOrder == self::VALUE) {
        $label = $value = $this->_encloseAndTrim($values[0], $textEnclosure);
      }

      switch ($override_import) {
        case self::SKIP_OPTION:
          $values_custom_field = OptionValue::get(FALSE)
            ->addWhere('option_group_id', '=', $this->_ogid)
            ->execute();
          $notexist = TRUE;
          foreach ($values_custom_field as $value_comp) {
            if ($value_comp['value'] == $value) {
              $notexist = FALSE;
              break;
            }
          }
          if ($notexist) {
            $this->insertValue_customfield($value, $label);
            $lineCount++;
          }
          break;

        case self::OVERWRITE_LABEL:
          $values_custom_field = OptionValue::get(FALSE)
            ->addWhere('option_group_id', '=', $this->_ogid)
            ->execute();
          $notexist = TRUE;
          foreach ($values_custom_field as $value_comp) {
            if ($value_comp['value'] == $value) {
              civicrm_api3("OptionValue", "create", [
                'id' => $value_comp['id'],
                'label' => $label
              ]);
              OptionValue::update(FALSE)
                ->addWhere('id', '=', $value_comp['id'])
                ->addValue('label', $label)
                ->execute();
              $notexist = FALSE;
              $lineUpdate++;
              break;
            }
          }
          if ($notexist) {
            $this->insertValue_customfield($value, $label);
            $lineCount++;
          }
      }

    }
    fclose($fd);

    $statusMsg = "Options Inserted:" . $lineCount . "\n Options Updated:" . $lineUpdate;
    CRM_Core_Session::setStatus($statusMsg, FALSE);
    CRM_Utils_System::redirect(CRM_Utils_System::url('civicrm/admin/custom/group/field/option', "reset=1&action=browse&gid=" . $this->_gid . "&fid=" . $this->_fid));
  }

  private function _encloseAndTrim($value, $enclosure = "'") {
    if (empty($value)) {
      return;
    }
    // Replace enclosures and trim blank chars
    return preg_replace("/^$enclosure(.*)$enclosure$/", '$1', trim($value, " \t\r\n"));
  }

}
