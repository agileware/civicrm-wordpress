<?php

require_once 'optionsimporter.civix.php';
use CRM_Optionsimporter_ExtensionUtil as E;

/**
 * Implements hook_civicrm_pageRun().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_pageRun/
 */
function optionsimporter_civicrm_pageRun(&$page) {
  $pageName = $page->getVar('_name');
  if ($pageName == 'CRM_Custom_Page_Option') {
    $page->assign('gid', $page->getVar('_gid'));
    $page->assign('fid', $page->getVar('_fid'));

    CRM_Core_Region::instance('page-body')->add(array(
      'template' => E::path("templates/CRM/Optionsimporter/Page/OptionButtons.tpl")
    ));
  }
}

/**
 * Implements hook_civicrm_config().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_config
 */
function optionsimporter_civicrm_config(&$config) {
  _optionsimporter_civix_civicrm_config($config);
}

/**
 * Implements hook_civicrm_install().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_install
 */
function optionsimporter_civicrm_install() {
  _optionsimporter_civix_civicrm_install();
}

/**
 * Implements hook_civicrm_enable().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_enable
 */
function optionsimporter_civicrm_enable() {
  _optionsimporter_civix_civicrm_enable();
}
