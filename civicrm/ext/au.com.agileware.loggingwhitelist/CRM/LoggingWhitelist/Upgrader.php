<?php
use CRM_LoggingWhitelist_ExtensionUtil as E;

/**
 * Collection of upgrade steps.
 */
class CRM_LoggingWhitelist_Upgrader extends CRM_LoggingWhitelist_Upgrader_Base {

  public function install() {
    \CRM_LoggingWhitelist::install();
  }

  public function enable() {
    \CRM_LoggingWhitelist::install();
  }

  /**
   * Run installer on upgrade to 1.2 version as well to remove unused tables.
   *
   * @return TRUE on success
   * @throws Exception
   */
  public function upgrade_1200() {
    $this->ctx->log->info('Forcing rebuild of triggers and removing unused log tables.');

    \CRM_LoggingWhitelist::install();

    return TRUE;
  }

    /**
     * Run installer on upgrade to 1.6 version as well to remove unused tables.
     *
     * @return TRUE on success
     * @throws Exception
     */
    public function upgrade_1600() {
        $this->ctx->log->info('Forcing rebuild of triggers and removing unused log tables.');

        \CRM_LoggingWhitelist::install();

        return TRUE;
    }
}
