<?php

use CRM_Loggingwhitelist_ExtensionUtil as E;

class CRM_LoggingWhitelist {
  private static $dropTables = [];

  static function alterLogTables(&$logTableSpec) {
    $whitelist = json_decode( file_get_contents( E::path( 'json/whitelist.json' ) ), TRUE );

    foreach ( array_keys( $logTableSpec ) as $tableName ) {
      if ( in_array( $tableName, $whitelist ) || strpos( $tableName, 'civicrm_value_') === 0) {
        continue;
      }

      // Add to our list of droppables
      self::$dropTables[] = 'log_' . $tableName;

      // And remove from the trigger specs.
      unset( $logTableSpec[ $tableName ] );
    }
  }

  static function dropLogTables() {
    // truncate log table if exists
    foreach (self::$dropTables as $tableName) {
      \CRM_Core_DAO::executeQuery( 'DROP TABLE IF EXISTS ' . $tableName );
    }
  }

  // Symfony listener for install / enable phase.
  static function logTablesListener($event) {
    self::alterLogTables($event->logTableSpec);
  }

  // shared install / enable callback.
  static function install() {
    // Listen for alter log tables event.  Because the module is not "enabled"
    // yet, we have to register the listener ourselves.
    \Civi::dispatcher()->addListener('hook_civicrm_alterLogTables', ['CRM_LoggingWhitelist', 'logTablesListener']);

    // Rebuild the triggers.  This will allow us to alter the table list, so
    // that the triggers we don't want are removed.
    \CRM_Core_DAO::triggerRebuild();

    // Remove the listener to avoid calling it extra times after the module has
    // finished being enabled.
    \Civi::dispatcher()->removeListener('hook_civicrm_alterLogTables', ['CRM_LoggingWhitelist', 'logTablesListener']);

    // Drop the log tables.  Safe to do so now single the triggers are not there
    // to depend on them
    self::dropLogTables();
  }
}
