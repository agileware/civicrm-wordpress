<?php
use CRM_SearchKitReports_ExtensionUtil as E;

return [
  [
    'name' => 'SavedSearch_Attendee_List',
    'entity' => 'SavedSearch',
    'cleanup' => 'unused',
    'update' => 'unmodified',
    'params' => [
      'version' => 4,
      'values' => [
        'name' => 'Attendee_List',
        'label' => E::ts('Attendee List'),
        'form_values' => NULL,
        'is_template' => TRUE,
        'search_custom_id' => NULL,
        'api_entity' => 'Event',
        'api_params' => [
          'version' => 4,
          'select' => [
            'Event_Participant_event_id_01.contact_id.display_name',
            'title',
            'Event_Participant_event_id_01.status_id:label',
            'Event_Participant_event_id_01.role_id:label',
          ],
          'orderBy' => [],
          'where' => [
            [
              'Event_Participant_event_id_01.is_test',
              '=',
              FALSE,
            ],
            [
              'Event_Participant_event_id_01_Participant_Contact_contact_id_01.is_deceased',
              '=',
              FALSE,
            ],
          ],
          'groupBy' => [],
          'join' => [
            [
              'Participant AS Event_Participant_event_id_01',
              'LEFT',
              [
                'id',
                '=',
                'Event_Participant_event_id_01.event_id',
              ],
            ],
            [
              'Contact AS Event_Participant_event_id_01_Participant_Contact_contact_id_01',
              'LEFT',
              [
                'Event_Participant_event_id_01.contact_id',
                '=',
                'Event_Participant_event_id_01_Participant_Contact_contact_id_01.id',
              ],
            ],
          ],
          'having' => [],
        ],
        'expires_date' => NULL,
        'description' => NULL,
      ],
    ],
  ],
];
