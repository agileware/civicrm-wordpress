<?php
use CRM_SearchKitReports_ExtensionUtil as E;

// Check CiviContribute is enabled
$entity = \Civi\Api4\Entity::get(FALSE)
  ->addWhere('name', '=', 'Contribution')->execute();
if (!$entity->count()) {
  return [];
}

return [
  [
    'name' => 'SavedSearch_Personal_Campaign_Page_Summary',
    'entity' => 'SavedSearch',
    'cleanup' => 'unused',
    'update' => 'unmodified',
    'params' => [
      'version' => 4,
      'values' => [
        'name' => 'Personal_Campaign_Page_Summary',
        'label' => E::ts('Personal Campaign Page Summary'),
        'form_values' => NULL,
        'is_template' => TRUE,
        'search_custom_id' => NULL,
        'api_entity' => 'Contribution',
        'api_params' => [
          'version' => 4,
          'select' => [
            'Contribution_ContributionSoft_contribution_id_01_ContributionSoft_PCP_pcp_id_01.contact_id.display_name',
            'Contribution_ContributionSoft_contribution_id_01_ContributionSoft_PCP_pcp_id_01.title',
            'GROUP_CONCAT(DISTINCT campaign_id.title) AS GROUP_CONCAT_campaign_id_title',
            'Contribution_ContributionSoft_contribution_id_01_ContributionSoft_PCP_pcp_id_01.goal_amount',
            'SUM(Contribution_ContributionSoft_contribution_id_01.amount) AS SUM_Contribution_ContributionSoft_contribution_id_01_amount',
            'COUNT(Contribution_ContributionSoft_contribution_id_01.id) AS COUNT_Contribution_ContributionSoft_contribution_id_01_id',
          ],
          'orderBy' => [],
          'where' => [],
          'groupBy' => [
            'Contribution_ContributionSoft_contribution_id_01_ContributionSoft_PCP_pcp_id_01.id',
          ],
          'join' => [
            [
              'ContributionSoft AS Contribution_ContributionSoft_contribution_id_01',
              'INNER',
              [
                'id',
                '=',
                'Contribution_ContributionSoft_contribution_id_01.contribution_id',
              ],
            ],
            [
              'PCP AS Contribution_ContributionSoft_contribution_id_01_ContributionSoft_PCP_pcp_id_01',
              'INNER',
              [
                'Contribution_ContributionSoft_contribution_id_01.pcp_id',
                '=',
                'Contribution_ContributionSoft_contribution_id_01_ContributionSoft_PCP_pcp_id_01.id',
              ],
            ],
            [
              'Campaign AS Contribution_Campaign_campaign_id_01',
              'LEFT',
              [
                'campaign_id',
                '=',
                'Contribution_Campaign_campaign_id_01.id',
              ],
            ],
          ],
          'having' => [],
        ],
        'expires_date' => NULL,
        'description' => NULL,
      ],
    ],
  ],
];
