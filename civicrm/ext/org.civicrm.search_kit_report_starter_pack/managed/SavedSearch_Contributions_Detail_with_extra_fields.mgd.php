<?php
use CRM_SearchKitReports_ExtensionUtil as E;

// Check CiviContribute is enabled
$entity = \Civi\Api4\Entity::get(FALSE)
  ->addWhere('name', '=', 'Contribution')->execute();
if (!$entity->count()) {
  return [];
}

return [
  [
    'name' => 'SavedSearch_Contributions_Detail_with_extra_fields',
    'entity' => 'SavedSearch',
    'cleanup' => 'unused',
    'update' => 'unmodified',
    'params' => [
      'version' => 4,
      'values' => [
        'name' => 'Contributions_Detail_with_extra_fields',
        'label' => E::ts('Contributions Detail with extra fields'),
        'form_values' => NULL,
        'is_template' => TRUE,
        'search_custom_id' => NULL,
        'api_entity' => 'Contact',
        'api_params' => [
          'version' => 4,
          'select' => [
            'GROUP_CONCAT(DISTINCT display_name) AS GROUP_CONCAT_display_name',
            'COUNT(id) AS COUNT_id',
            'Contact_Contribution_contact_id_01.id',
            'Contact_Contribution_contact_id_01.financial_type_id:label',
            'Contact_Contribution_contact_id_01.receive_date',
            'Contact_Contribution_contact_id_01.total_amount',
            'Contact_Contribution_contact_id_01.contribution_status_id:label',
          ],
          'orderBy' => [],
          'where' => [
            [
              'Contact_Contribution_contact_id_01.contribution_status_id:name',
              '=',
              'Completed',
            ],
            [
              'Contact_Contribution_contact_id_01.is_test',
              '=',
              FALSE,
            ],
          ],
          'groupBy' => [
            'Contact_Contribution_contact_id_01.id',
          ],
          'join' => [
            [
              'Contribution AS Contact_Contribution_contact_id_01',
              'LEFT',
              [
                'id',
                '=',
                'Contact_Contribution_contact_id_01.contact_id',
              ],
            ],
          ],
          'having' => [],
        ],
        'expires_date' => NULL,
        'description' => NULL,
      ],
    ],
  ],
];
