<?php
use CRM_SearchKitReports_ExtensionUtil as E;

// Check CiviContribute is enabled
$entity = \Civi\Api4\Entity::get(FALSE)
  ->addWhere('name', '=', 'Contribution')->execute();
if (!$entity->count()) {
  return [];
}

return [
  [
    'name' => 'SavedSearch_Recurring_Contributions',
    'entity' => 'SavedSearch',
    'cleanup' => 'unused',
    'update' => 'unmodified',
    'params' => [
      'version' => 4,
      'values' => [
        'name' => 'Recurring_Contributions',
        'label' => E::ts('Recurring Contributions'),
        'form_values' => NULL,
        'is_template' => TRUE,
        'search_custom_id' => NULL,
        'api_entity' => 'Contribution',
        'api_params' => [
          'version' => 4,
          'select' => [
            'Contribution_Contact_contact_id_01.display_name',
            'contribution_status_id:label',
            'Contribution_Contact_contact_id_01_Contact_ContributionRecur_contact_id_01.frequency_interval',
            'Contribution_Contact_contact_id_01_Contact_ContributionRecur_contact_id_01.frequency_unit:label',
            'Contribution_Contact_contact_id_01_Contact_ContributionRecur_contact_id_01.installments',
            'Contribution_Contact_contact_id_01_Contact_ContributionRecur_contact_id_01.amount',
            'Contribution_Contact_contact_id_01_Contact_ContributionRecur_contact_id_01.end_date',
          ],
          'orderBy' => [],
          'where' => [],
          'groupBy' => [],
          'join' => [
            [
              'Contact AS Contribution_Contact_contact_id_01',
              'LEFT',
              [
                'contact_id',
                '=',
                'Contribution_Contact_contact_id_01.id',
              ],
            ],
            [
              'ContributionRecur AS Contribution_Contact_contact_id_01_Contact_ContributionRecur_contact_id_01',
              'LEFT',
              [
                'Contribution_Contact_contact_id_01.id',
                '=',
                'Contribution_Contact_contact_id_01_Contact_ContributionRecur_contact_id_01.contact_id',
              ],
            ],
          ],
          'having' => [],
        ],
        'expires_date' => NULL,
        'description' => NULL,
      ],
    ],
  ],
];
