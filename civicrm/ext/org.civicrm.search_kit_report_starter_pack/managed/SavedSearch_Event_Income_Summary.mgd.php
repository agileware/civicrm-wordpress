<?php
use CRM_SearchKitReports_ExtensionUtil as E;

// Check CiviEvent is enabled
$entity = \Civi\Api4\Entity::get(FALSE)
  ->addWhere('name', '=', 'Event')->execute();
if (!$entity->count()) {
  return [];
}

return [
  [
    'name' => 'SavedSearch_Event_Income_Summary',
    'entity' => 'SavedSearch',
    'cleanup' => 'unused',
    'update' => 'unmodified',
    'params' => [
      'version' => 4,
      'values' => [
        'name' => 'Event_Income_Summary',
        'label' => E::ts('Event Income Summary'),
        'form_values' => NULL,
        'is_template' => TRUE,
        'search_custom_id' => NULL,
        'api_entity' => 'Event',
        'api_params' => [
          'version' => 4,
          'select' => [
            'title',
            'event_type_id:label',
            'COUNT(Event_Participant_event_id_01.id) AS COUNT_Event_Participant_event_id_01_id',
            'SUM(Event_Participant_event_id_01_Participant_LineItem_entity_id_01.line_total) AS SUM_Event_Participant_event_id_01_Participant_LineItem_entity_id_01_line_total',
          ],
          'orderBy' => [],
          'where' => [
            [
              'Event_Participant_event_id_01_Participant_Contact_contact_id_01.is_deleted',
              '=',
              FALSE,
            ],
          ],
          'groupBy' => [
            'id',
          ],
          'join' => [
            [
              'Participant AS Event_Participant_event_id_01',
              'LEFT',
              [
                'id',
                '=',
                'Event_Participant_event_id_01.event_id',
              ],
            ],
            [
              'Contact AS Event_Participant_event_id_01_Participant_Contact_contact_id_01',
              'INNER',
              [
                'Event_Participant_event_id_01.contact_id',
                '=',
                'Event_Participant_event_id_01_Participant_Contact_contact_id_01.id',
              ],
              [
                'Event_Participant_event_id_01.status_id:name',
                'IN',
                [
                  'Attended',
                  'Registered',
                  'Pending from pay later',
                ],
              ],
            ],
            [
              'LineItem AS Event_Participant_event_id_01_Participant_LineItem_entity_id_01',
              'LEFT',
              [
                'Event_Participant_event_id_01.id',
                '=',
                'Event_Participant_event_id_01_Participant_LineItem_entity_id_01.entity_id',
              ],
              [
                'Event_Participant_event_id_01_Participant_LineItem_entity_id_01.entity_table',
                '=',
                "'civicrm_participant'",
              ],
            ],
          ],
          'having' => [],
        ],
        'expires_date' => NULL,
        'description' => NULL,
      ],
    ],
  ],
];
