<?php
use CRM_SearchKitReports_ExtensionUtil as E;

// Check CiviEvent is enabled
$entity = \Civi\Api4\Entity::get(FALSE)
  ->addWhere('name', '=', 'Event')->execute();
if (!$entity->count()) {
  return [];
}

return [
  [
    'name' => 'SavedSearch_Event_Overview',
    'entity' => 'SavedSearch',
    'cleanup' => 'unused',
    'update' => 'unmodified',
    'params' => [
      'version' => 4,
      'values' => [
        'name' => 'Event_Overview',
        'label' => E::ts('Event Overview'),
        'form_values' => NULL,
        'is_template' => TRUE,
        'search_custom_id' => NULL,
        'api_entity' => 'Event',
        'api_params' => [
          'version' => 4,
          'select' => [
            'title',
            'GROUP_CONCAT(DISTINCT start_date) AS GROUP_CONCAT_start_date',
            'COUNT(Event_Participant_event_id_01.contact_id) AS COUNT_Event_Participant_event_id_01_contact_id',
            'SUM(Event_Participant_event_id_01.fee_amount) AS SUM_Event_Participant_event_id_01_fee_amount',
          ],
          'orderBy' => [],
          'where' => [
            [
              'Event_Participant_event_id_01.status_id:name',
              'IN',
              [
                'Registered',
                'Attended',
              ],
            ],
          ],
          'groupBy' => [
            'title',
            'financial_type_id',
          ],
          'join' => [
            [
              'Participant AS Event_Participant_event_id_01',
              'LEFT',
              [
                'id',
                '=',
                'Event_Participant_event_id_01.event_id',
              ],
            ],
            [
              'Contact AS Event_Participant_event_id_01_Participant_Contact_contact_id_01',
              'LEFT',
              [
                'Event_Participant_event_id_01.contact_id',
                '=',
                'Event_Participant_event_id_01_Participant_Contact_contact_id_01.id',
              ],
            ],
          ],
          'having' => [],
        ],
        'expires_date' => NULL,
        'description' => NULL,
      ],
    ],
  ],
];
