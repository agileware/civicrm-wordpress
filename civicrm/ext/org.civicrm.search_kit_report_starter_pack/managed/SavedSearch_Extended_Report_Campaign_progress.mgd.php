<?php
use CRM_SearchKitReports_ExtensionUtil as E;

// Check CiviCampaign is enabled
$entity = \Civi\Api4\Entity::get(FALSE)
  ->addWhere('name', '=', 'Campaign')->execute();
if (!$entity->count()) {
  return [];
}

return [
  [
    'name' => 'SavedSearch_Extended_Report_Campaign_progress',
    'entity' => 'SavedSearch',
    'cleanup' => 'unused',
    'update' => 'unmodified',
    'params' => [
      'version' => 4,
      'values' => [
        'name' => 'Extended_Report_Campaign_progress',
        'label' => E::ts('Extended Report - Campaign progress'),
        'form_values' => NULL,
        'is_template' => TRUE,
        'search_custom_id' => NULL,
        'api_entity' => 'Campaign',
        'api_params' => [
          'version' => 4,
          'select' => [
            'title',
            'GROUP_CONCAT(DISTINCT campaign_type_id:label) AS GROUP_CONCAT_campaign_type_id_label',
            'goal_revenue',
            'GROUP_CONCAT(DISTINCT Campaign_Contribution_campaign_id_01.financial_type_id:label) AS GROUP_CONCAT_Campaign_Contribution_campaign_id_01_financial_type_id_label',
            'GROUP_CONCAT(DISTINCT status_id:label) AS GROUP_CONCAT_status_id_label',
            'SUM(Campaign_Contribution_campaign_id_01.total_amount) AS SUM_Campaign_Contribution_campaign_id_01_total_amount',
            'GROUP_CONCAT(DISTINCT Campaign_Contribution_campaign_id_01_Contribution_Contact_contact_id_01.display_name) AS GROUP_CONCAT_Campaign_Contribution_campaign_id_01_Contribution_Contact_contact_id_01_display_name',
          ],
          'orderBy' => [],
          'where' => [],
          'groupBy' => [
            'title',
          ],
          'join' => [
            [
              'Contribution AS Campaign_Contribution_campaign_id_01',
              'LEFT',
              [
                'id',
                '=',
                'Campaign_Contribution_campaign_id_01.campaign_id',
              ],
            ],
            [
              'FinancialType AS Campaign_Contribution_campaign_id_01_Contribution_FinancialType_financial_type_id_01',
              'LEFT',
              [
                'Campaign_Contribution_campaign_id_01.financial_type_id',
                '=',
                'Campaign_Contribution_campaign_id_01_Contribution_FinancialType_financial_type_id_01.id',
              ],
            ],
            [
              'Contact AS Campaign_Contribution_campaign_id_01_Contribution_Contact_contact_id_01',
              'LEFT',
              [
                'Campaign_Contribution_campaign_id_01.contact_id',
                '=',
                'Campaign_Contribution_campaign_id_01_Contribution_Contact_contact_id_01.id',
              ],
            ],
          ],
          'having' => [],
        ],
        'expires_date' => NULL,
        'description' => NULL,
      ],
    ],
  ],
];
