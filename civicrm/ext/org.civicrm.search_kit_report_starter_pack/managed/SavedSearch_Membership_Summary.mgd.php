<?php
use CRM_SearchKitReports_ExtensionUtil as E;

// Check CiviMember is enabled
$entity = \Civi\Api4\Entity::get(FALSE)
  ->addWhere('name', '=', 'Membership')->execute();
if (!$entity->count()) {
  return [];
}

return [
  [
    'name' => 'SavedSearch_Membership_Summary',
    'entity' => 'SavedSearch',
    'cleanup' => 'unused',
    'update' => 'unmodified',
    'params' => [
      'version' => 4,
      'values' => [
        'name' => 'Membership_Summary',
        'label' => E::ts('Membership Summary'),
        'form_values' => NULL,
        'is_template' => TRUE,
        'search_custom_id' => NULL,
        'api_entity' => 'Membership',
        'api_params' => [
          'version' => 4,
          'select' => [
            'membership_type_id:label',
            'SUM(id) AS SUM_id',
            'SUM(Membership_LineItem_entity_id_01.line_total) AS SUM_Membership_LineItem_entity_id_01_line_total',
          ],
          'orderBy' => [],
          'where' => [],
          'groupBy' => [
            'membership_type_id',
          ],
          'join' => [
            [
              'LineItem AS Membership_LineItem_entity_id_01',
              'LEFT',
              [
                'id',
                '=',
                'Membership_LineItem_entity_id_01.entity_id',
              ],
              [
                'Membership_LineItem_entity_id_01.entity_table',
                '=',
                "'civicrm_membership'",
              ],
            ],
          ],
          'having' => [],
        ],
        'expires_date' => NULL,
        'description' => NULL,
      ],
    ],
  ],
];
