<?php
use CRM_SearchKitReports_ExtensionUtil as E;

return [
  [
    'name' => 'SavedSearch_Current_Employers',
    'entity' => 'SavedSearch',
    'cleanup' => 'unused',
    'update' => 'unmodified',
    'params' => [
      'version' => 4,
      'values' => [
        'name' => 'Current_Employers',
        'label' => E::ts('Current Employers'),
        'form_values' => NULL,
        'is_template' => TRUE,
        'search_custom_id' => NULL,
        'api_entity' => 'Contact',
        'api_params' => [
          'version' => 4,
          'select' => [
            'display_name',
            'Contact_RelationshipCache_Contact_01.display_name',
            'Contact_RelationshipCache_Contact_01.job_title',
            'Contact_RelationshipCache_Contact_01.start_date',
            'Contact_RelationshipCache_Contact_01.email_primary.email',
            'Contact_RelationshipCache_Contact_01.is_active',
          ],
          'orderBy' => [],
          'where' => [
            [
              'is_deceased',
              '=',
              FALSE,
            ],
          ],
          'groupBy' => [],
          'join' => [
            [
              'Contact AS Contact_RelationshipCache_Contact_01',
              'INNER',
              'RelationshipCache',
              [
                'id',
                '=',
                'Contact_RelationshipCache_Contact_01.far_contact_id',
              ],
              [
                'Contact_RelationshipCache_Contact_01.near_relation:name',
                '=',
                '"Employee of"',
              ],
            ],
          ],
          'having' => [],
        ],
        'expires_date' => NULL,
        'description' => NULL,
      ],
    ],
  ],
];
