<?php
use CRM_SearchKitReports_ExtensionUtil as E;

// Check CiviMember is enabled
$entity = \Civi\Api4\Entity::get(FALSE)
  ->addWhere('name', '=', 'Membership')->execute();
if (!$entity->count()) {
  return [];
}

return [
  [
    'name' => 'SavedSearch_Membership_Details',
    'entity' => 'SavedSearch',
    'cleanup' => 'unused',
    'update' => 'unmodified',
    'params' => [
      'version' => 4,
      'values' => [
        'name' => 'Membership_Details',
        'label' => E::ts('Membership Details'),
        'form_values' => NULL,
        'is_template' => TRUE,
        'search_custom_id' => NULL,
        'api_entity' => 'Membership',
        'api_params' => [
          'version' => 4,
          'select' => [
            'Membership_Contact_contact_id_01.display_name',
            'membership_type_id:label',
            'start_date',
            'end_date',
            'status_id:label',
          ],
          'orderBy' => [],
          'where' => [
            [
              'Membership_Contact_contact_id_01.is_deleted',
              '=',
              FALSE,
            ],
          ],
          'groupBy' => [],
          'join' => [
            [
              'Contact AS Membership_Contact_contact_id_01',
              'LEFT',
              [
                'contact_id',
                '=',
                'Membership_Contact_contact_id_01.id',
              ],
            ],
          ],
          'having' => [],
        ],
        'expires_date' => NULL,
        'description' => NULL,
      ],
    ],
  ],
];
