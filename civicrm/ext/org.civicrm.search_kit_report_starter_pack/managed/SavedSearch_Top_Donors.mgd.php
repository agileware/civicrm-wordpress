<?php
use CRM_SearchKitReports_ExtensionUtil as E;

// Check CiviContribute is enabled
$entity = \Civi\Api4\Entity::get(FALSE)
  ->addWhere('name', '=', 'Contribution')->execute();
if (!$entity->count()) {
  return [];
}

return [
  [
    'name' => 'SavedSearch_Top_Donors',
    'entity' => 'SavedSearch',
    'cleanup' => 'unused',
    'update' => 'unmodified',
    'params' => [
      'version' => 4,
      'values' => [
        'name' => 'Top_Donors',
        'label' => E::ts('Top Donors'),
        'form_values' => NULL,
        'is_template' => TRUE,
        'search_custom_id' => NULL,
        'api_entity' => 'Contribution',
        'api_params' => [
          'version' => 4,
          'select' => [
            'GROUP_CONCAT(DISTINCT Contribution_Contact_contact_id_01.display_name) AS GROUP_CONCAT_Contribution_Contact_contact_id_01_display_name',
            'SUM(total_amount) AS SUM_total_amount',
            'COUNT(id) AS COUNT_id',
            'AVG(total_amount) AS AVG_total_amount',
          ],
          'orderBy' => [],
          'where' => [
            [
              'contribution_status_id:name',
              '=',
              'Completed',
            ],
          ],
          'groupBy' => [
            'contact_id.display_name',
          ],
          'join' => [
            [
              'Contact AS Contribution_Contact_contact_id_01',
              'LEFT',
              [
                'contact_id',
                '=',
                'Contribution_Contact_contact_id_01.id',
              ],
            ],
          ],
          'having' => [],
        ],
        'expires_date' => NULL,
        'description' => NULL,
      ],
    ],
  ],
];
