# SearchKit Report Starter Pack

SearchKit is a great tool, but it can be daunting to build a complex search from scratch.
This starter pack contains pre-built searches to jump-start your search building experience.

The extension is licensed under [AGPL-3.0](LICENSE.txt).
