# Agileware Improvements to the CiviCRM User Experience (au.com.agileware.ux)

CiviCRM extension developed by Agileware to improve the overall user experience when using CiviCRM. This extension hides options which users find confusing, sets default options where the user would normally expect them to be set, hides pop-ups and other messages which add no value and more!

The extension is licensed under [AGPL-3.0](LICENSE.txt).

# About the Authors

This CiviCRM extension was developed by the team at [Agileware](https://agileware.com.au).

[Agileware](https://agileware.com.au) provide a range of CiviCRM services including:

  * CiviCRM migration
  * CiviCRM integration
  * CiviCRM extension development
  * CiviCRM support
  * CiviCRM hosting
  * CiviCRM remote training services

Support your Australian [CiviCRM](https://civicrm.org) developers, [contact Agileware](https://agileware.com.au/contact) today!


![Agileware](logo/agileware-logo.png)
