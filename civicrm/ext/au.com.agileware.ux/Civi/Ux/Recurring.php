<?php

namespace Civi\Ux;

use Civi\Api4\Membership;
use Civi\Core\Event\PostEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class Recurring implements EventSubscriberInterface {

	/**
	 * @inheritDoc
	 */
	public static function getSubscribedEvents() {
    return [
      'hook_civicrm_post::ContributionRecur' => 'post',
    ];
  }

  /**
   * PostEvent hook for recurring contributions
   *
   * @param \Civi\Core\Event\PostEvent $event
   *
   * @return void
   * @throws \CRM_Core_Exception
   * @throws \Civi\API\Exception\UnauthorizedException
   */
  public function post(PostEvent $event) {
    // Guard against incorrect entities firing
    if($event->entity !== 'ContributionRecur') {
      return;
    }

    /** @var \CRM_Contribute_BAO_ContributionRecur $contribution_recur */
    $contribution_recur = $event->object;
    $contribution_recur->find(TRUE);

    $cancelledId = \CRM_Core_PseudoConstant::getKey('CRM_Contribute_BAO_ContributionRecur', 'contribution_status_id', 'Cancelled');

    // When the ContributionRecur is cancelled, disconnect any linked memberships
    if($contribution_recur->contribution_status_id == $cancelledId) {
      Membership::update(FALSE)
        ->addValue('contribution_recur_id', NULL)
        ->addWhere('contribution_recur_id', '=', $event->id)
        ->execute();
    }
  }
}
