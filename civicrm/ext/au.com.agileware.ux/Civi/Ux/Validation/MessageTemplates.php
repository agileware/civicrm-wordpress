<?php

namespace Civi\Ux\Validation;

use Civi\Core\Event\GenericHookEvent;
use Civi\Ux\Utils;

class MessageTemplates {

  public static function buildForm(GenericHookEvent $event) {
    $form = $event->form;
    $form->addFormRule([self::class, 'validateChecksumTokens']);
  }

  /**
   * Implement validation for incorrect checksum usage.
   * Prevent the form from saving if checksum and contact ID are being used, instead of their respective CiviCRM tokens.
   */
  public static function validateChecksumTokens($params, $files, $form) {
    $errors = [];

    foreach (['msg_html', 'msg_text'] as $field) {
      $problems = [];

      $str = isset($params[$field]) ? $params[$field] : '';
      if (empty($str)) {
        continue;
      }
      $problems = Utils::validateChecksumTokens($str, $field);

      if (!empty($problems)) {
        $message = '<ul>';
        foreach ($problems as $problem => $count) {
          $message .= "<li>{$problem}" . ($count > 1 ? " <strong>x{$count}</strong>" : "") . "</li>";
        }
        $message .= '</ul>';

        $errors[$field] = ts(
          '<p>Page cannot be saved. The following URLs contain errors:</p>
            %1
          <p>Please change URLs to use proper CiviCRM checksum tokens: <strong>https://yourdomain.com/path-to-your-page/?{contact.checksum}&cid={contact.contact_id}</strong></p>',
          [1 => $message]);
      }
    }

    return empty($errors) ? TRUE : $errors;
  }
}
