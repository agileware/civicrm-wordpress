<?php

class CRM_Ux_APIWrapperGetQuick implements API_Wrapper {
  private $includeWildCardInName = 1;

  /**
   * Alter the parameters of the api request
   */
	public function fromApiInput( $apiRequest ) {
		$formName      = 'CRM_Contact_Search_QuickForm';
		$wildcardForms = Civi::settings()
		                     ->get( 'ux_remove_automatic_wildcard' );
		$emailForms    = Civi::settings()->get( 'ux_dont_include_email' );

		if ( is_array( $wildcardForms ) ) {
			if ( in_array( $formName, $wildcardForms ) ) {
				$this->includeWildCardInName = Civi::settings()
				                                   ->get( 'includeWildCardInName' );
				if ( $this->includeWildCardInName ) {
					Civi::settings()
					    ->loadMandatory( [ 'includeWildCardInName' => '0' ] );
					$apiRequest['updated_wildcard_setting'] = 1;
				}
			}
		}

		if ( is_array( $wildcardForms ) ) {
			if ( in_array( $formName, $emailForms ) ) {
				$config                     = CRM_Core_Config::singleton();
				$config->includeEmailInName = FALSE;
			}
		}

		$apiRequest['params']['description_field'] = [];

		return $apiRequest;
	}

	/**
   * Alter the result before returning it to the caller.
   */
	public function toApiOutput( $apiRequest, $result ) {

		$formName      = 'CRM_Contact_Search_QuickForm';
		$wildcardForms = Civi::settings()
		                     ->get( 'ux_remove_automatic_wildcard' );
		$emailForms    = Civi::settings()->get( 'ux_dont_include_email' );

		if ( is_array( $wildcardForms ) ) {
			if ( in_array( $formName, $wildcardForms ) && 1 == ( $apiRequest['updated_wildcard_setting'] ?? NULL ) ) {
				Civi::settings()
				    ->loadMandatory( [ 'includeWildCardInName' => $this->includeWildCardInName ] );
			}
		}

		if ( is_array( $emailForms ) ) {
			if ( in_array( $formName, $emailForms ) ) {
				$config                     = CRM_Core_Config::singleton();
				$settingsValue              = Civi::settings()
				                                  ->get( 'includeEmailInName' );
				$config->includeEmailInName = $settingsValue;
			}
		}

		return $result;
	}

}
