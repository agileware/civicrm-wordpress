<?php

class CRM_Ux_APIWrapper implements API_Wrapper {
  static $ux_opened_form;

  /**
   * Alter the parameters of the api request
   */
  public function fromApiInput($apiRequest) {
    $wildcardForms = Civi::settings()->get('ux_remove_automatic_wildcard');
    $emailForms = Civi::settings()->get('ux_dont_include_email');

    if (!empty($wildcardForms) && in_array(self::$ux_opened_form, $wildcardForms)) {
        $apiRequest['params']['add_wildcard'] = 0;
    }

    if (!empty($emailForms) && in_array(self::$ux_opened_form, $emailForms)) {
        $config = CRM_Core_Config::singleton();
        $config->includeEmailInName = FALSE;
    }

    return $apiRequest;
  }

  /**
   * Alter the result before returning it to the caller.
   */
  public function toApiOutput($apiRequest, $result) {
    $emailForms = Civi::settings()->get('ux_dont_include_email');

    if (!empty($emailForms) &&in_array(self::$ux_opened_form, $emailForms)) {
        $config = CRM_Core_Config::singleton();
        $config->includeEmailInName = Civi::settings()->get('includeEmailInName');
    }

    return $result;
  }

}
