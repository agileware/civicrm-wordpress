<?php

use Civi\Api4\MembershipStatus;
use CRM_Ux_ExtensionUtil as E;

/**
 * Collection of upgrade steps.
 */
class CRM_Ux_Upgrader extends CRM_Extension_Upgrader_Base {

  /**
   * Update 100 function
   *
   * @return TRUE on success
   * @throws Exception
   */
  public function upgrade_100() {
    $this->ctx->log->info('Applying update 100');
    // CIVIUX-144 Disable the "Receipt" Activity Type in the Activities menu as it serves no purpose for users to create this type of Activity
    CRM_Core_DAO::executeQuery("UPDATE `civicrm_option_value` SET `filter` = '1' WHERE `civicrm_option_value`.`name` = 'ReceiptActivity'");

    return TRUE;
  }

  /**
   * @return TRUE on success
   */
	public function upgrade_10500() {
		if ( \CRM_Extension_System::singleton()
		                          ->getManager()
		                          ->getStatus( 'civi_member' ) === 'installed' ) {
			$this->ctx->log->info( 'Disconnecting Memberships from cancelled Recurring Contributions' );

			$contributionCancelled = \CRM_Core_PseudoConstant::getKey( 'CRM_Contribute_BAO_ContributionRecur', 'contribution_status_id', 'Cancelled' );
			$membershipCancelled   = MembershipStatus::get( FALSE )
			                                         ->addSelect( 'id' )
			                                         ->addWhere( 'name', 'IN', [
				                                         'Cancelled',
				                                         'Deceased',
			                                         ] )
			                                         ->execute()
			                                         ->column( 'id' );

			// CIVIUX-171 For existing Recurring Contributions that are cancelled, disconnect referring Memberships
			CRM_Core_DAO::executeQuery( <<<'sql'
UPDATE civicrm_membership m
   SET contribution_recur_id = NULL
 WHERE EXISTS (SELECT 1 FROM `civicrm_contribution_recur` `cr` WHERE `m`.`contribution_recur_id` = `cr`.`id` AND `contribution_status_id` = %1)
   AND (`m`.`status_id` NOT IN (%2)) AND (`m`.`is_test` = 0);
sql
				,
				[
					'1' => [ $contributionCancelled, 'Integer' ],
					'2' => [
						implode( ',', $membershipCancelled ),
						'CommaSeparatedIntegers',
					],
				] );
		}

		return TRUE;
	}

}
