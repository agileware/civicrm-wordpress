# Membership Logs

Lists logs for the membership under membership tab of contact summary and expander to membership search results.

## Requirements

* PHP v7.1+
* CiviCRM 5.19+

## Installation

See: https://docs.civicrm.org/sysadmin/en/latest/customize/extensions/#installing-a-new-extension

## Usage

![Screenshot](/images/Logs.png)
