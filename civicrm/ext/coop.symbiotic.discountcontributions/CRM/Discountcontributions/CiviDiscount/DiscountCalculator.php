<?php

class CRM_Discountcontributions_CiviDiscount_DiscountCalculator extends CRM_CiviDiscount_DiscountCalculator {

  /**
   * Check if discount is applicable.
   *
   * This overrides the built-in cividiscount function. It basically always returns TRUE,
   * unless a discount has been applied to a specific Membership/Event page.
   *
   * @param array $discount
   * @param string $entity
   * @param int $id entity id
   * @param string $type 'filters' or autodiscount
   * @param array $additionalFilter e.g array('contact_id' => x) when looking at memberships
   *
   * @return bool
   */
  protected function checkDiscountsByEntity(array $discount, string $entity, ?int $id, string $type, array $additionalFilter = []): bool {
  
    if (!empty($discount[$type][$entity])) {
      return FALSE;
    }

    if (!empty($discount['autodiscount'])) {
      return FALSE;
    }

    return TRUE;
  }

}
