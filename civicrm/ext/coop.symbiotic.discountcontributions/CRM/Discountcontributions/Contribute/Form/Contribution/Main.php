<?php

use CRM_Discountcontributions_ExtensionUtil as E;

class CRM_Discountcontributions_Contribute_Form_Contribution_Main {

  /**
   * @param $form \CRM_Core_Form
   *
   * @throws \CRM_Core_Exception
   * @see discountcontributions_civicrm_buildForm().
   */
  static function buildForm(&$form) {
    // don't render the code field if no discount code set
    if (!$form->_values['fee'] || !self::hasDiscountFor($form->_values['fee'])) {
      return;
    }

    // Force CiviDiscount to kick-in on the payment form.
    // Based on cividiscount_civicrm_buildForm().
    _cividiscount_add_discount_textfield($form);
    $code = trim(CRM_Utils_Request::retrieveValue('discountcode', 'String') ?? '');

    if ($code) {
      $form->setDefaults([
        'discountcode' => $code,
      ]);
    }
  }

  /**
   * Check if there is a discount code set for the price sets
   * @param $fee the fee value stored in the form object
   *
   * @return bool
   */
  static private function hasDiscountFor($fee) {
    // get a list of price field value ids
    $priceFieldValueID = [];
    foreach ($fee as $price) {
      $ids = array_keys($price['options']);
      $priceFieldValueID = array_merge($priceFieldValueID, $ids);
    }

    // api3 cannot do the filtering we want, so doing it ourselves
    try{
      $code = civicrm_api3('DiscountCode', 'get', [
        'options' => ['limit' => 0],
        // 'pricesets' => SOMETHING not work here
      ]);
    } catch (CiviCRM_API3_Exception $e) {
      return FALSE;
    }
    foreach ($code['values'] as $value) {
      // no price set set means it applies to all price set - any
      if (empty($value['pricesets'])) {
        return TRUE;
      }
      // any one price field value have discount code -> yes
      if (!empty(array_intersect($priceFieldValueID, $value['pricesets']))) {
        return TRUE;
      }
    }
    return FALSE;
  }

  /**
   * @see discountcontributions_civicrm_buildAmount().
   */
  static function buildAmount($pageType, &$form, &$amounts) {
    // This is a simplification of the conditions from cividiscount buildAmount,
    // but where we look only for pageType == contribution.
    if (!(($form->getVar('_action') & CRM_Core_Action::PREVIEW)
          || !($form->getVar('_action') & CRM_Core_Action::ADD)
          || !($form->getVar('_action') & CRM_Core_Action::UPDATE)
        )) {
      return;
    }

    if (empty($amounts) || !is_array($amounts) || $pageType != 'contribution') {
      return;
    }

    $contact_id = $form->getContactID();
    $autodiscount = FALSE;
    $psid = $form->get('priceSetId');
    $ps = $form->get('priceSet');
    $v = $form->getVar('_values');

    $code = trim(CRM_Utils_Request::retrieve('discountcode', 'String', $form, FALSE, NULL, 'REQUEST') ?? '');

    if (!empty($v['currency'])) {
      $currency = $v['currency'];
    }
    elseif (!empty($v['event']['currency'])) {
      $currency = $v['event']['currency'];
    }
    else {
      $currency = CRM_Core_Config::singleton()->defaultCurrency;
    }

    // [ML] We do not apply any auto-discounts for now.
    if (empty($code)) {
      return;
    }

    $applyToAllLineItems = FALSE;

    $form->set('_discountInfo', NULL);
    $discountEntity = 'PriceSet'; // [ML] this does not really make any difference
    $discountCalculator = new CRM_Discountcontributions_CiviDiscount_DiscountCalculator($discountEntity, $psid, $contact_id, $code, FALSE, $psid);

    $discounts = $discountCalculator->getDiscounts();

    if (!empty($code) && empty($discounts)) {
      $form->set( 'discountCodeErrorMsg', ts('The discount code you entered is invalid.', ['domain' => 'org.civicrm.module.cividiscount']));
    }

    // [ML] Below this point is pretty much a trimmed version of upstream cividiscount.

    // here we check if discount is configured for events or for membership types.
    // There are two scenarios:
    // 1. Discount is configure for the event or membership type, in that case we should apply discount only
    //    if default fee / membership type is configured. ( i.e price set with quick config true )
    // 2. Discount is configure at price field level, in this case discount should be applied only for
    //    that particular price set field.

    $keys = array_keys($discounts);
    $key = array_shift($keys);

    // in this case discount is specified for event id or membership type id, so we need to get info of
    // associated price set fields. For events discount we already have the list, but for memberships we
    // need to filter at membership type level

    // Retrieve price set field associated with this priceset
    $priceSetInfo = CRM_CiviDiscount_Utils::getPriceSetsInfo($psid);
    $originalAmounts = $amounts;

    foreach ($discounts as $discountID => $discount) {
      $discountApplied = FALSE;
      if (!empty($discount['events']) || !empty($discount['memberships'])) {
        // this discount is for events/memberships and should not be used here
        // FIXME show error?
        continue;
      }
      $priceFields = isset($discount['pricesets']) ? $discount['pricesets'] : [];

      foreach ($amounts as $fee_id => &$fee) {
        if (!is_array($fee['options'])) {
          continue;
        }

        foreach ($fee['options'] as $option_id => &$option) {
          if (!empty($applyToAllLineItems) || CRM_Utils_Array::value($option['id'], $priceFields)) {
            $originalLabel = $originalAmounts[$fee_id]['options'][$option_id]['label'];
            $originalAmount = CRM_Utils_Rule::cleanMoney($originalAmounts[$fee_id]['options'][$option_id]['amount']);
            list($amount, $label) = _cividiscount_calc_discount($originalAmount, $originalLabel, $discount, $autodiscount, $currency);
            $discountAmount = $originalAmounts[$fee_id]['options'][$option_id]['amount'] - $amount;

            if($discountAmount > CRM_Utils_Array::value('discount_applied', $option)) {
              $option['amount'] = $amount;
              $option['label'] = $label;
              $option['discount_applied'] = $discountAmount;
              /*
              * Priyanka Karan @ Veda NFP Consulting Ltd
              * Re-calculate VAT/Sales TAX on discounted amount.
              */
              $recalculateTaxAmount = array();
              if (array_key_exists('tax_amount', $originalAmounts[$fee_id]['options'][$option_id]) &&
                  array_key_exists('tax_rate', $originalAmounts[$fee_id]['options'][$option_id])) {
                $recalculateTaxAmount = CRM_Contribute_BAO_Contribution_Utils::calculateTaxAmount($amount, $originalAmounts[$fee_id]['options'][$option_id]['tax_rate']);
                if (!empty($recalculateTaxAmount)) {
                  $option['tax_amount'] = round($recalculateTaxAmount['tax_amount'], 2);
                }
              }
            }
            $appliedDiscountID = $discountID;
            $discountApplied = TRUE;
          }
        }
      }
    }

    if (isset($discountApplied) && $discountApplied && !empty($discounts[$appliedDiscountID])) {
      if (!empty($ps['fields'])) {
        $ps['fields'] = $amounts;
        $form->setVar('_priceSet', $ps);
      }

      $form->set('_discountInfo', array(
        'discount' => $discounts[$appliedDiscountID],
        'autodiscount' => $autodiscount,
        'contact_id' => $contact_id,
      ));
    }
  }

}
