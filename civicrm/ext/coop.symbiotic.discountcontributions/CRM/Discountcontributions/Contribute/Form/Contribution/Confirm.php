<?php

use Civi\FormHelper;

class CRM_Discountcontributions_Contribute_Form_Contribution_Confirm {

  public static function postProcess($form) {
    /* @var CRM_Contribute_Form_Contribution_Confirm $form */
    $discountInfo = $form->get('_discountInfo');
    if (!$discountInfo) {
      return;
    }

    $formHelper = new FormHelper($form);

    // CiviDiscount has a postProcess for 'CRM_Contribute_Form_Contribution_Confirm' to handle
    // memberships.  Make sure we don't process forms with memberships.
    if ($formHelper->getMembershipTypes()) {
      return;
    }
    $discountParams = [
      'item_id' => $discountInfo['discount']['id'],
      'contribution_id' => $formHelper->getContributionID(),
      'contact_id' => $form->getContactID(),
      'entity_table:label' => 'Contribution',
      'entity_id' => $formHelper->getContributionID(),
      'used_date' => 'now',
    ];
    civicrm_api4('DiscountTrack', 'create', ['values' => $discountParams]);
  }

}
