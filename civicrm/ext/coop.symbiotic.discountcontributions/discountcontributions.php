<?php

require_once 'discountcontributions.civix.php';
use CRM_Discountcontributions_ExtensionUtil as E;

/**
 * Implements hook_civicrm_config().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_config
 */
function discountcontributions_civicrm_config(&$config) {
  _discountcontributions_civix_civicrm_config($config);
}

/**
 * Implements hook_civicrm_install().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_install
 */
function discountcontributions_civicrm_install() {
  _discountcontributions_civix_civicrm_install();
}

/**
 * Implements hook_civicrm_enable().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_enable
 */
function discountcontributions_civicrm_enable() {
  _discountcontributions_civix_civicrm_enable();
}

/**
 * Implements hook_civicrm_buildForm().
 */
function discountcontributions_civicrm_buildForm($formName, &$form) {
  if ($formName == 'CRM_Contribute_Form_Contribution_Main') {
    CRM_Discountcontributions_Contribute_Form_Contribution_Main::buildForm($form);
  }
}

/**
 * Implements hook_civicrm_buildAmount().
 */
function discountcontributions_civicrm_buildAmount($pageType, &$form, &$amounts) {
  CRM_Discountcontributions_Contribute_Form_Contribution_Main::buildAmount($pageType, $form, $amounts);
}

/**
 * Implements hook_civicrm_postProcess().
 */
function discountcontributions_civicrm_postProcess($class, $form) {
  if ($class == 'CRM_Contribute_Form_Contribution_Confirm') {
    CRM_Discountcontributions_Contribute_Form_Contribution_Confirm::postProcess($form);
  }
}
