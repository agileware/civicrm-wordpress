# Discount Contributions

Allow discounts on contributions pages (payments).

For some background, see:  
https://civicrm.stackexchange.com/questions/7401/can-cividiscount-be-used-on-a-contribution-page-without-membership/7428

The extension is licensed under [AGPL-3.0](LICENSE.txt).

## Requirements

* PHP v7.2+
* CiviCRM 5.latest
* Latest stable version of CiviDiscount

## Installation

Install as a regular extension.

## Usage

Set up discounts as per the [CiviDiscount documentation](https://docs.civicrm.org/discount/en/latest/).  At present you can use the "Price Field Options" or "Automatic Discounts" sections of a CiviDiscount, but can not specify a particular contribution page.  Make a price set unique to a page, then discount the price field options to limit discounts to that page.

This has been mostly tested with a CiviDiscount that applies to Price Field Options. If there are no conditions set, the Contribution Page will respond with "unknown discount".

## Support

Please post bug reports in the issue tracker of this project:  
https://lab.civicrm.org/extensions/discountcontributions/issues

Commercial support is available through Coop SymbioTIC:  
https://www.symbiotic.coop/en

Coop Symbiotic is a worker-owned co-operative based in Canada. We have a strong
experience working with non-profits and CiviCRM. We provide fast, affordable,
turn-key hosting with regular upgrades and proactive monitoring, as well as
custom development and training.

For developers/implementors: We offer a collaborative environment that lets
you have control on your work while being part of a team. For CiviCRM
providers or solo freelancers, we also provide white-label hosting and
second-level support.
