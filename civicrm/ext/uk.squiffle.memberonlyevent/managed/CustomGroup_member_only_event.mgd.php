<?php
use CRM_Memberonlyevent_ExtensionUtil as E;

return [
  [
    'name' => 'CustomGroup_member_only_event',
    'entity' => 'CustomGroup',
    'cleanup' => 'unused',
    'update' => 'unmodified',
    'params' => [
      'version' => 4,
      'values' => [
        'name' => 'member_only_event',
        'title' => E::ts('Member-Only event'),
        'extends' => 'Event',
        'style' => 'Inline',
        'collapse_display' => TRUE,
        'help_pre' => E::ts('If selected, only users who are logged in and have a Membership with one of the set Membership Statuses and Types (Administer > CiviEvent > Member-Only Event Settings) will be able to register for this event.'),
        'weight' => 4,
        'is_public' => FALSE,
      ],
      'match' => [
        'name',
      ],
    ],
  ],
  [
    'name' => 'OptionGroup_Member_only_event_Is_this_a_member_only_event_',
    'entity' => 'OptionGroup',
    'cleanup' => 'unused',
    'update' => 'unmodified',
    'params' => [
      'version' => 4,
      'values' => [
        'name' => 'Member_only_event_Is_this_a_member_only_event_',
        'title' => E::ts('Member-Only event :: Is this a member-only event?'),
        'data_type' => 'String',
        'is_reserved' => FALSE,
        'option_value_fields' => [
          'name',
          'label',
        ],
      ],
      'match' => [
        'name',
      ],
    ],
  ],
  [
    'name' => 'OptionGroup_Member_only_event_Is_this_a_member_only_event_OptionValue_Yes',
    'entity' => 'OptionValue',
    'cleanup' => 'unused',
    'update' => 'unmodified',
    'params' => [
      'version' => 4,
      'values' => [
        'option_group_id.name' => 'Member_only_event_Is_this_a_member_only_event_',
        'label' => E::ts('Yes'),
        'value' => '1',
        'name' => 'Yes',
      ],
      'match' => [
        'option_group_id',
        'name',
        'value',
      ],
    ],
  ],
  [
    'name' => 'CustomGroup_member_only_event_CustomField_is_member_only_event',
    'entity' => 'CustomField',
    'cleanup' => 'unused',
    'update' => 'unmodified',
    'params' => [
      'version' => 4,
      'values' => [
        'custom_group_id.name' => 'member_only_event',
        'name' => 'is_member_only_event',
        'label' => E::ts('Is this a member-only event?'),
        'html_type' => 'CheckBox',
        'option_group_id.name' => 'Member_only_event_Is_this_a_member_only_event_',
        'serialize' => 1,
      ],
      'match' => [
        'name',
        'custom_group_id',
      ],
    ],
  ],
];
