<?php

use CRM_Memberonlyevent_ExtensionUtil as E;

return [
  'memberonlyevent_statuses' => [
    'name' => 'memberonlyevent_statuses',
    'type' => 'Array',
    'title' => E::ts('Qualifying membership statuses'),
    'description' => E::ts('If "Member-Only Event" is selected in the event configuration, ' .
      'users need a membership with one of these statuses to be eligible to book.'),
    'html_type' => 'select',
    'html_attributes' => [
      'multiple' => TRUE,
      'class' => 'crm-select2',
    ],
    // New & Current
    'default' => ["1", "2"],
    'pseudoconstant' => ['callback' => 'CRM_Memberonlyevent::getMembershipStatuses'],
    'settings_pages' => [
      'memberonlyevent' => [
        'weight' => 10,
      ],
    ],
  ],

  'memberonlyevent_types' => [
    'name' => 'memberonlyevent_types',
    'type' => 'Array',
    'title' => E::ts('Qualifying membership types'),
    'description' => E::ts('If "Member-Only Event" is selected in the event configuration, ' .
      'users need a membership with one of these types to be eligible to book.' .
      'If no types are selected here than any type is permitted to book.'),
    'html_type' => 'select',
    'html_attributes' => [
      'multiple' => TRUE,
      'class' => 'crm-select2',
    ],
    'pseudoconstant' => ['callback' => 'CRM_Memberonlyevent::getMembershipTypes'],
    'settings_pages' => [
      'memberonlyevent' => [
        'weight' => 10,
      ],
    ],
  ],

  'memberonlyevent_message' => [
    'name' => 'memberonlyevent_message',
    'type' => 'String',
    'title' => E::ts('Message if not eligible'),
    'description' => E::ts('This message is displayed if the user is not eligible to book'),
    'html_type' => 'wysiwyg',
    'default' => '<p>' . E::ts('This event is only open to members. To register, you need to be logged in and have a current membership.') . '</p>',
    'settings_pages' => [
      'memberonlyevent' => [
        'weight' => 10,
      ],
    ],
  ],

];
