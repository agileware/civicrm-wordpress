# Member-Only Events

This extension enables you to restrict registration to certain events to logged
in members only but still have other events open to the public.

Users who are not logged in or do not have the right memberships will be
redirected from the Event Registration page to the Event Information page.  The
Information page will not display the Registration buttons.

It creates custom fields on the Event Configuration page to mark whether the
event is restricted to members.

Settings allow the administrator to configure the acceptable membership types
and statuses and to define the message displayed to those not eligible to book.

This extension was originally developed by vakeesan@millertech.co.uk
https://github.com/MillerTech-CRM/uk.co.nfpservices.module.memberonlyevent

V2 by Aidan Saunders, (hello@squiffle.uk)

![Member-Only Event Settings](/images/MemberOnlyEventSettings.png "Member-Only Event Settings")

![Member-Only Event Configuration Fields](/images/MemberOnlyEventConfigurationFields.png "Member-Only Event Configuration Fields")

![Event Information page example](/images/MemberOnlyEventInfoPage.png "Event Information page example")


## Installation

Install as a regular CiviCRM extension.

If you are upgrading from v1.x, see ChangeLog.md

## Usage

Review the settings at Administration > CiviEvent > Member-Only Event Settings
 - Choose the Membership Statuses that you allow to register.  You probably want New and Current, and maybe Grace.
 - Choose the Membership Types that you allow to register.
 - Configure the message to be shown when a user accesses the Event Information page
when they are not logged in or not eligible.

Once enabled, the extension adds Custom Fields to the Event Configuration page.

Ticking the box will restrict access based on the settings.

## Notes

The restrictions apply to the user accessing the Event Information and
Registration pages.  If you allow users to register other participants
then the membership requirements DO NOT apply to them.

## Issues

See https://lab.civicrm.org/extensions/memberonlyevent/issues
