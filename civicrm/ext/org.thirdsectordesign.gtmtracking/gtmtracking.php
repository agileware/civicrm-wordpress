<?php

require_once 'gtmtracking.civix.php';
// phpcs:disable
use CRM_Gtmtracking_ExtensionUtil as E;
// phpcs:enable

/**
 * Implements hook_civicrm_config().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_config/
 */
function gtmtracking_civicrm_config(&$config): void {
  _gtmtracking_civix_civicrm_config($config);
}

/**
 * Implements hook_civicrm_install().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_install
 */
function gtmtracking_civicrm_install(): void {
  _gtmtracking_civix_civicrm_install();
}

/**
 * Implements hook_civicrm_enable().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_enable
 */
function gtmtracking_civicrm_enable(): void {
  _gtmtracking_civix_civicrm_enable();
}

// --- Functions below this ship commented out. Uncomment as required. ---

/**
 * Implements hook_civicrm_preProcess().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_preProcess
 */
//function gtmtracking_civicrm_preProcess($formName, &$form): void {
//
//}

/**
 * Implements hook_civicrm_navigationMenu().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_navigationMenu
 */
function gtmtracking_civicrm_navigationMenu(&$menu): void {
  _gtmtracking_civix_insert_navigation_menu($menu, 'Administer/System Settings', [
    'label' => E::ts('Google Tag Manager Settings'),
    'name' => 'gtmtracking_settings',
    'url' => 'civicrm/admin/setting/gtmtracking',
    'permission' => 'administer CiviCRM',
    'operator' => 'OR',
    'separator' => 0,
  ]);
  _gtmtracking_civix_navigationMenu($menu);
}

/**
 * Implements hook_civicrm_navigationMenu().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_buildForm
 */
function gtmtracking_civicrm_buildForm($formName, &$form): void {
  // handle contribution forms
  Civi\Gtmtracking\ContributionPages::buildForm($formName, $form);

  //handle events
  Civi\Gtmtracking\Events::buildForm($formName, $form);
}
