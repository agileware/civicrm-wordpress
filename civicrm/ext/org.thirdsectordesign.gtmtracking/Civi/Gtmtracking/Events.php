<?php

namespace Civi\Gtmtracking;

class Events {

  public static function buildForm($formName, &$form) {
    // check if we are on contribution pages
    $validForms = [
      'CRM_Event_Form_Registration_Register',
      'CRM_Event_Form_Registration_Confirm',
      'CRM_Event_Form_Registration_ThankYou',
    ];

    if (!in_array($formName, $validForms)) {
      return;
    }

    $gtmCode = \Civi\Gtmtracking\Form::verify($form, 'gtmtracking_event_ids');
    if (empty($gtmCode)) {
      return;
    }

    $amount = 0;
    $frequency = 'Once';
    $currency = $form->_values['event']['currency'];

    // update based on user selection
    $params = $form->getVar('_params');
    if (!empty($params)) {
      $amount = $params[0]['amount'];
      if (!empty($params[0]['currencyID'])) {
        $currency = $params[0]['currencyID'];
      }
    }

    $dataLayerProperties = [
      'title' => $form->_values['event']['title'],
      'amount' => $amount,
      'currency' => $currency,
      'frequency' => $frequency,
      'financial_type_id' => $form->_values['event']['financial_type_id'],
      'campaign_id' => $form->_values['event']['campaign_id'],
    ];

    \Civi\Gtmtracking\Form::embedGTMCode($gtmCode, $dataLayerProperties);
  }

}
