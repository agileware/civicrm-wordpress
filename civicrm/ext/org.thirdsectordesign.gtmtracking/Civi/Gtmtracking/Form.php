<?php

namespace Civi\Gtmtracking;

class Form {

  public static function verify($form, $entitySettingsField) {
    // return early if GTM code is missing
    $gtmCode = \Civi::settings()->get('gtmtracking_gtm_id');

    if (empty($gtmCode)) {
      return;
    }

    // check if we need to handle test mode
    $isTestMode = ($form->_mode === 'test');

    $allowTestModeTracking = \Civi::settings()->get('gtmtracking_enable_test');
    if (empty($allowTestModeTracking) && $isTestMode) {
      return;
    }

    $entityId = $form->getVar('_id');

    // get the configured entities: contribution pages or events
    $entityIDs = \Civi::settings()->get($entitySettingsField);

    if (!empty($entityIDs) && !in_array($entityId, $entityIDs)) {
      return;
    }

    return $gtmCode;
  }

  public static function embedGTMCode($gtmCode, &$dataLayerProperties) {
    $financialType = $campaign = '';

    if (!empty($dataLayerProperties['financial_type_id'])) {
      $financialTypes = \Civi\Api4\FinancialType::get(FALSE)
        ->addSelect('name')
        ->addWhere('id', '=', $dataLayerProperties['financial_type_id'])
        ->execute()
        ->first();

      $financialType = $financialTypes['name'];
    }

    if (!empty($dataLayerProperties['campaign_id'])) {
      $campaigns = \Civi\Api4\Campaign::get(FALSE)
        ->addSelect('title')
        ->addWhere('id', '=', $dataLayerProperties['campaign_id'])
        ->execute()
        ->first();

      $campaign = $campaigns['title'];
    }

    // add Google Tag Manager snippets
    $hearderSnippetCode = "
      <script>
      window.dataLayer = window.dataLayer || [];
      dataLayer.push({
        'title': '" . $dataLayerProperties['title'] . "',
        'amount': '" . $dataLayerProperties['amount'] . "',
        'currency': '" . $dataLayerProperties['currency'] . "',
        'frequency': '" . $dataLayerProperties['frequency'] . "',
        'financial_type': '" . $financialType . "',
        'campaign': '" . $campaign . "',
      });
      </script>
      <!-- Google Tag Manager -->
      <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
      new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
      j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
      'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
      })(window,document,'script','dataLayer','" . $gtmCode . "');</script>
      <!-- End Google Tag Manager -->
      ";

    \CRM_Core_Region::instance('html-header')->add([
      'markup' => $hearderSnippetCode,
    ]);

    $bodySnippetCode = '
      <!-- Google Tag Manager (noscript) -->
      <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=' . $gtmCode . '"
      height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
      <!-- End Google Tag Manager (noscript) -->
      ';

    \CRM_Core_Region::instance('page-body')->add([
      'markup' => $bodySnippetCode,
    ]);

  }

}
