<?php

namespace Civi\Gtmtracking;

class ContributionPages {

  public static function buildForm($formName, $form) {
    // check if we are on contribution pages
    $validForms = [
      'CRM_Contribute_Form_Contribution_Main',
      'CRM_Contribute_Form_Contribution_Confirm',
      'CRM_Contribute_Form_Contribution_ThankYou',
    ];

    if (!in_array($formName, $validForms)) {
      return;
    }

    $gtmCode = \Civi\Gtmtracking\Form::verify($form, 'gtmtracking_contribution_page_ids');
    if (empty($gtmCode)) {
      return;
    }

    $amount = 0;
    $frequency = 'Once';
    $currency = $form->_values['currency'];

    // update based on user selection
    $params = $form->getVar('_params');
    if (!empty($params)) {
      $amount = $form->getVar('_amount');
      if (!empty($params['amount'])) {
        $amount = $params['amount'];
      }

      if (!empty($params['is_recur'])) {
        $frequency = 'subscription - ' . $params['frequency_unit'];
      }
      if (!empty($params['currencyID'])) {
        $currency = $params['currencyID'];
      }
    }

    $dataLayerProperties = [
      'title' => $form->_values['title'],
      'amount' => $amount,
      'currency' => $currency,
      'frequency' => $frequency,
      'financial_type_id' => $form->_values['financial_type_id'],
      'campaign_id' => $form->_values['campaign_id'],
    ];

    \Civi\Gtmtracking\Form::embedGTMCode($gtmCode, $dataLayerProperties);
  }

}
