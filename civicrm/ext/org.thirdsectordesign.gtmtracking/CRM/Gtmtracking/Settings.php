<?php

class CRM_Gtmtracking_Settings {

  public static function getContributionPages() {
    return \Civi\Api4\ContributionPage::get(FALSE)
      ->addSelect('id', 'title')
      ->addWhere('is_active', '=', TRUE)
      ->addOrderBy('title', 'ASC')
      ->execute()
      ->indexBy('id')
      ->column('title');
  }

  public static function getEvents() {
    return \Civi\Api4\Event::get(FALSE)
      ->addSelect('id', 'title')
      ->addWhere('is_active', '=', TRUE)
      ->addOrderBy('title', 'ASC')
      ->execute()
      ->indexBy('id')
      ->column('title');
  }

}
