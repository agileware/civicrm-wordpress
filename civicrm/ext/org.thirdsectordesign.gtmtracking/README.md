# Google Tag Manager for CiviCRM

Simple conversion tracking for your contribution and event pages

## Getting Started

The idea is that information from CiviCRM thank you pages should be made available when a transaction has been successfully completed.

The kind of data we are interested in includes:

- Financial value
- Currency
- Transaction type: e.g., one-off donation, monthly subscription sign-up, annual subscription sign-up.

### Setting up Google Tag Manager

This assumes you already have a Google Tag Manager account, a Google Analytics 4 property and have set up a GA4 configuration tag within Google Tag Manager to fire on every page view.

#### Setting up the data layer variables

To receive the data from CiviCRM when a transaction event occurs, some user-defined data layer variables need to be set up within Google Tag Manager.

To set these up, within Tag Manager, go to Variables/User-Defined Variables and click New. In the configuration screen, choose “Data Layer Variable” for each of the following. You don’t necessarily need all of these - only the ones that are appropriate in your situation.

| Variable name | Data Layer Variable Name | Description |
|---------------|--------------------------|-------------|
| Civi Transaction Amount | amount | The financial value |
| Civi Transaction Currency | currency | Currency: e.g GBP, USD |
|Civi Transaction Financial Type | financial_type | Type of transaction: e.g., one-off donation |
| Civi Transaction Frequency | frequency | Frequency of repetition |
| Civi Transaction Title | title | Title of transaction |
| Civi Transaction Campaign | campaign |Campaign from which the transaction should be attributed (if any)|

#### Setting up the GA4 trigger

Now it’s time to configure a trigger to fire when the user reaches the CiviCRM transaction thankyou page. You can do this by using a simple regular expression which recognises the URL of the success page.

So go to triggers, click “New” and add a new trigger. Call it “CiviCRM Thankyou Page”. Configure it like this:

- Trigger type “Page View - DOM Ready” (choose DOM Ready)
- Trigger fires on: Some DOM Ready Events.
- Fire this trigger when: an Event occurs and all of these conditions are true:
  - Page URL
  - matches RegEx
  - transact.*_qf_ThankYou_display=1

#### Setting up the GA4 tag

Once your variables and triggers are set up, it’s time to do something with the data now available to Tag Manager. We send it to Google Analytics 4 as events, using a GA4 event tag in Tag Manager.

This assumes you already have a GA4 configuration tag that fires on every page view.

Within Tag Manager, click Tags/New and configure a tag like this:

- Tag Type: Google Analytics: GA4 Event
- Event Name: civi_transaction

Add event parameters as follows:

| Parameter Name | Value |
|----------------|-------|
| amount | {{Civi Transaction Amount}} |
| campaign | {{Civi Transaction Campaign}} |
| currency | {{Civi Transaction Currency}} |
| financial_type | {{Civi Transaction Financial Type}} |
| frequency | {{Civi Transaction Frequency}} |
| title | {{Civi Transaction Title}} |
| value|{{Civi Transaction Amount}} |

The “value” parameter is needed because Google Analytics recognises event parameters with the name of “value” and treats them as financial transactions.

Save all the above work, and don’t forget to Submit this so that the changes to your Tag Manager container are pushed to your website.

### Analysing the data in Google Analytics 4

If everything is working correctly, when users make financial transactions the basic data is sent to Google Analytics 4 as an event. We wanted to understand which channels drove the most transactions, so we set up a report to analyse this.

We found that whilst the CiviCRM transaction events could be seen in the real time events view in Google Analytics 4, they did not appear in the standard reports. This is because there are a relatively low number of such events, and allowing them to be viewed in the standard reports could allow GA4 users to link the events to individual users, giving rise to privacy concerns. Therefore GA4 was thresholding our data and not showing all the Civi transaction events to us.

However, the events can be seen using custom explorations within GA4.

Before GA4 can include this data in reports, we need to set up custom dimensions and metrics matching the event parameters we’re sending to Analytics.

Go to Admin and choose Custom Definitions. Create a custom dimension with the following configuration:

- Dimension name: “financial_type”,
- Scope: “Event”
- Event Parameter “financial_type”.

Also add a Custom metric with the following configuration:

- Metric name: “amount”
- Scope: “Event”
- Description “Amount of financial transaction”
- Event parameter: “amount”
- Unit of measurement: “Currency”
- Data type: “Revenue data”

You can set up the other dimensions and variables in a similar way if you need them in your reports.

Add a custom exploration and import the following Dimensions:

- Event name
- Session default channel group
- financial_type

Import the following metrics:

- Event count
- amount

Choose a date range and add “Event name” and “Session default channel group” as rows and “Event count” and “amount” as values. You’ll then see how many transactions took place via Civi in your date range, and the channel which drove those users to your website. Here’s a sample result:

![Sample Report](images/sample_report.png)

## Help

If you have any questions regarding this extension that are not answered in this README or the documentation, please post a question on [CiviCRM Stackexchange](http://civicrm.stackexchange.com). Alternatively, feel free to contact info@thirdsectordesign.org.

## Contributing

Contributions to this repository are very welcome. For small changes, feel free to submit a pull request. For larger changes, please create an issue first so we can talk about your ideas.

## Credits

Thank you to our client [Population Matters](https://populationmatters.org) for their support to make this extension possible and [Dotwise](https://dotwise.uk) for sharing their Google Analytics and GTM knowledge with us to ensure the data coming in can be used to establish meaningful reports.

It is mantained by [Kurund Jalmi](https://twitter.com/kurund) from [Third Sector Design](https://thirdsectordesign.org/) who you can [contact](https://thirdsectordesign.org/contact) for help, support and further development.

## License

This extension is licensed under [AGPL-3.0](LICENSE.txt).
