<?php
use CRM_Gtmtracking_ExtensionUtil as E;

return [
  'gtmtracking_gtm_id' => [
    'name' => 'gtmtracking_gtm_id',
    'type' => 'String',
    'html_type' => 'text',
    'default' => '',
    'is_domain' => 1,
    'is_contact' => 0,
    'title' => E::ts('Google Tag Manager ID'),
    'description' => E::ts('Enter your Google Tag Manager ID here. Use comma without space (,) to enter multiple IDs.'),
    'html_attributes' => [
      'size' => 40,
    ],
    'settings_pages' => [
      'gtmtracking' => [
        'weight' => 1,
      ],
    ],
  ],

  'gtmtracking_enable_test' => [
    'name' => 'gtmtracking_enable_test',
    'type' => 'Boolean',
    'html_type' => 'checkbox',
    'default' => 0,
    'is_domain' => 1,
    'is_contact' => 0,
    'title' => E::ts('Enable tracking for test forms'),
    'description' => E::ts('Enable for test forms (eg. test contribution pages)'),
    'html_attributes' => [],
    'settings_pages' => [
      'gtmtracking' => [
        'weight' => 2,
      ],
    ],
  ],

  'gtmtracking_contribution_page_ids' => [
    'name' => 'gtmtracking_contribution_page_ids',
    'type' => 'Array',
    'html_type' => 'select',
    'html_attributes' => [
      'class' => 'crm-select2 huge',
      'multiple' => TRUE,
      'placeholder' => E::ts('- select -'),
    ],
    'pseudoconstant' => [
      'callback' => ['CRM_Gtmtracking_Settings', 'getContributionPages'],
    ],
    'default' => [],
    'is_domain' => 1,
    'title' => E::ts('Enable for specific Contribution Pages'),
    'description' => E::ts('Leave blank to enable for all contribution pages'),
    'settings_pages' => [
      'gtmtracking' => [
        'weight' => 3,
      ],
    ],
  ],

  'gtmtracking_event_ids' => [
    'name' => 'gtmtracking_event_ids',
    'type' => 'Array',
    'html_type' => 'select',
    'html_attributes' => [
      'class' => 'crm-select2 huge',
      'multiple' => TRUE,
      'placeholder' => E::ts('- select -'),
    ],
    'pseudoconstant' => [
      'callback' => ['CRM_Gtmtracking_Settings', 'getEvents'],
    ],
    'default' => [],
    'is_domain' => 1,
    'title' => E::ts('Enable for specific Event registration forms'),
    'description' => E::ts('Leave blank to enable for all event registration forms'),
    'settings_pages' => [
      'gtmtracking' => [
        'weight' => 4,
      ],
    ],
  ],

];
