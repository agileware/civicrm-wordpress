<?php return array(
    'root' => array(
        'pretty_version' => '3.3',
        'version' => '3.3.0.0',
        'type' => 'library',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(),
        'reference' => '4d82a84a94a914d2645b594bf45f1365090d1cee',
        'name' => 'civipkg/uk.co.vedaconsulting.mosaico',
        'dev' => true,
    ),
    'versions' => array(
        'civicrm/composer-downloads-plugin' => array(
            'pretty_version' => 'v3.0.1',
            'version' => '3.0.1.0',
            'type' => 'composer-plugin',
            'install_path' => __DIR__ . '/../civicrm/composer-downloads-plugin',
            'aliases' => array(),
            'reference' => '3aabb6d259a86158d01829fc2c62a2afb9618877',
            'dev_requirement' => false,
        ),
        'civipkg/uk.co.vedaconsulting.mosaico' => array(
            'pretty_version' => '3.3',
            'version' => '3.3.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(),
            'reference' => '4d82a84a94a914d2645b594bf45f1365090d1cee',
            'dev_requirement' => false,
        ),
        'guzzlehttp/guzzle' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '^6.5',
            ),
        ),
        'guzzlehttp/psr7' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '^1.8.5',
            ),
        ),
        'intervention/image' => array(
            'pretty_version' => '2.7.2',
            'version' => '2.7.2.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../intervention/image',
            'aliases' => array(),
            'reference' => '04be355f8d6734c826045d02a1079ad658322dad',
            'dev_requirement' => false,
        ),
        'togos/gitignore' => array(
            'pretty_version' => '1.1.1',
            'version' => '1.1.1.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../togos/gitignore',
            'aliases' => array(),
            'reference' => '32bc0830e4123f670adcbf5ddda5bef362f4f4d4',
            'dev_requirement' => false,
        ),
    ),
);
