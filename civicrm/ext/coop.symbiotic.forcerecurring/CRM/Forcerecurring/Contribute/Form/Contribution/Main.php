<?php

class CRM_Forcerecurring_Contribute_Form_Contribution_Main {

  /**
   * @see forcerecurring_civicrm_buildForm().
   *
   * Applies the setting to force recurring contributions.
   */
  public static function buildForm(&$form) {
    $form_id = $form->get('id');
    $forcerecurring = Civi::settings()->get('forcerecurring_' . $form_id);

    if ($forcerecurring && $form->elementExists('is_recur')) {
      $form->setDefaults([
        'is_recur' => 1,
      ]);

      $e = $form->getElement('is_recur');
      $e->freeze();
    }
  }

}
