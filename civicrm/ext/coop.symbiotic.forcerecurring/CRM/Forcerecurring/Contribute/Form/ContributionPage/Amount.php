<?php

class CRM_Forcerecurring_Contribute_Form_ContributionPage_Amount {

  /**
   * @see forcerecurring_civicrm_buildForm().
   *
   * Adds a radio button for the setting. The JS positions it in the right place.
   */
  public static function buildForm(&$form) {
    $form->addYesNo('contribution_force_recurring', ts('Only accept recurring contributions?'), FALSE);

    $form_id = $form->get('id');
    $current_value = Civi::settings()->get('forcerecurring_' . $form_id);

    $form->setDefaults([
      'contribution_force_recurring' => $current_value,
    ]);

    CRM_Core_Region::instance('form-body')->add(array(
      'template' => 'CRM/Forcerecurring/Contribute/Form/ContributePage/Amount.tpl',
    ));
  }

  /**
   *
   */
  public static function postProcess(&$form) {
    $form_id = $form->get('id');
    $values = $form->exportValues();
    $forcerecurring = CRM_Utils_Array::value('contribution_force_recurring', $values);

    Civi::settings()->set('forcerecurring_' . $form_id, $forcerecurring);
  }

}
