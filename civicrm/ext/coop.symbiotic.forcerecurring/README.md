# Force Recurring

![Screenshot](/images/screenshot.png)

Adds an option to force recurring donations on contribution pages.

The extension is licensed under [AGPL-3.0](LICENSE.txt).

If you are looking for more recurring contribution options, please see:  
https://github.com/adixon/ca.civicrm.contributionrecur

## Requirements

* PHP v7.0+
* CiviCRM 5.latest

## Installation

Install as a regular CiviCRM extension.

## Usage

Once enabled, you will see a new setting under the 'Amount' tab on contribution page settings.

## Known Issues

When creating new Contribution Pages, if the setting does not save, you mean need to clear the CiviCRM cache.
