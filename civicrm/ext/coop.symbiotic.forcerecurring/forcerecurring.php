<?php

require_once 'forcerecurring.civix.php';
use CRM_Forcerecurring_ExtensionUtil as E;

/**
 * Implements hook_civicrm_config().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_config/ 
 */
function forcerecurring_civicrm_config(&$config) {
  _forcerecurring_civix_civicrm_config($config);
}

/**
 * Implements hook_civicrm_install().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_install
 */
function forcerecurring_civicrm_install() {
  _forcerecurring_civix_civicrm_install();
}

/**
 * Implements hook_civicrm_enable().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_enable
 */
function forcerecurring_civicrm_enable() {
  _forcerecurring_civix_civicrm_enable();
}

/**
 * Implementation of hook_civicrm_buildForm()
 */
function forcerecurring_civicrm_buildForm($formName, &$form) {
  if ($formName == 'CRM_Contribute_Form_ContributionPage_Amount') {
    CRM_Forcerecurring_Contribute_Form_ContributionPage_Amount::buildForm($form);
  }
  elseif ($formName == 'CRM_Contribute_Form_Contribution_Main') {
    CRM_Forcerecurring_Contribute_Form_Contribution_Main::buildForm($form);
  }
}

/**
 * Implementation of hook_civicrm_postProcess().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_postProcess
 */
function forcerecurring_civicrm_postProcess($formName, &$form) {
  if ($formName == 'CRM_Contribute_Form_ContributionPage_Amount') {
    CRM_Forcerecurring_Contribute_Form_ContributionPage_Amount::postProcess($form);
  }
}
