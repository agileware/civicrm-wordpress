<?php

use CRM_Forcerecurring_ExtensionUtil as E;

$settings = [];
$weight = 0;

$contribution_pages = civicrm_api3('ContributionPage', 'get', [
  'option.limit' => 0,
]);

foreach ($contribution_pages['values'] as $id => $p) {
  $settings['forcerecurring_' . $id] = [
    'name' => 'forcerecurring_' . $id,
    'type' => 'String',
    'default' => null,
    'html_type' => 'yesno',
    'add' => '1.0',
    'title' => E::ts('Force recurring contributions for: %1', [1 => $p['title']]),
    'is_domain' => 1,
    'is_contact' => 0,
    'settings_pages' => [
      'forcerecurring' => [
        'weight' => $weight++,
      ],
    ],
  ];
}

return $settings;
