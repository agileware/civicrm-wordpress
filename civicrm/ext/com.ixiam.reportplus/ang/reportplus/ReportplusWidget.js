(function(angular, $, _) {

  angular.module('reportplus').component('reportplusWidget', {
    bindings: {
      instanceId: '<',
      contactId: '<'
    },
    templateUrl: '~/reportplus/ReportplusWidget.html',
    controller: function ($scope, $element) {
      var ts = $scope.ts = CRM.ts();
      var ctrl = this;

      function reload()  {
        url = "civicrm/report/instance/" + ctrl.instanceId
        query = {force: "1", context: "widget", snippet: "json", id_value: ctrl.contactId};
        CRM.loadPage(CRM.url(url, query), {target: $('.reportplus-widget-content', $element)});
      }

      this.$onInit = function() {
        // Add ChartJS script for contact summary widgets
        chartjs_url = CRM.resourceUrls["com.ixiam.modules.reportplus"] + "/js/chart.min.js";
        CRM.loadScript(chartjs_url);

        reload();
      };
    }
  });

})(angular, CRM.$, CRM._);
