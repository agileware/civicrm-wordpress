<?php
return [
  'js' => [
    'ang/reportplus.js',
    'ang/reportplus/*.js',
    'ang/reportplus/*/*.js',
  ],
  'css' => ['ang/reportplus.css'],
  'partials' => ['ang/reportplus'],
  'requires' => ['crmUi', 'crmUtil', 'ngRoute'],
  'settings' => [],
];
