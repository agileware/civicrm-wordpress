# ReportPlus

![Screenshot](images/screenshot1.png)

ReportPlus extends the default CiviCRM Form's Framework adding new features

## Features

- Drag & Drop display column selection
- Collapsible filters grouped by Entity
- CiviCRM Basic Report enhanced (as Contribution Detail/Summary, Membership Detail/Summary, etc)
- All Entity fields is available as column, order by and filter in basic reports
- Permissions for access and administer
- Extra CSV export features as encoding selection, separator, enclose, etc
- Export to Excel out-of-the-box (until 2.3.3, then it will depend on [civiexportexcel](https://lab.civicrm.org/extensions/civiexportexcel) extension)
- [ChartJS](https://www.chartjs.org/) library included to render graphs
- New type of Report Template called **Matrix**, where you can select 2 dimensions (columns, rows) and stats for several entities
- Integration with extension [Contact Layout](https://github.com/civicrm/org.civicrm.contactlayout) to include Reportplus results (table & charts) as part of Contact Summary Page
- Drill-down reports from Matrix Reports to expand grouped results in Detail Reports
- And some more...

The extension is licensed under [AGPL-3.0](LICENSE.txt).

## Requirements

* **CiviCRM 5.69.x w/ Smarty3**: Use releases **2.8.x+** / PHP v8.1+
* **CiviCRM 5.63.x**: Use releases **2.7.x+** / PHP v8.1+
* **CiviCRM 5.57.x**: Use releases **2.6.x+** / PHP v8.x
* **CiviCRM 5.45.x**: Use releases **2.5.x+** / PHP v7.4

## Caveats

From `ReportPlus 2.6.0`, Matrix reports create temporary tables to cache results to fasten the pager and avoid to recalculate the entire resulset when moving between pages
(This feature can be disabled selecting option `Do Not Cache MySQL results table when Paging`).
This temporary tables need to be removed manually, so there is a new api `reportplus.cleanup()` that takes care of it. Add this api call to a new Scheduled Job, to periodically drop those temp tables.

From `ReportPlus 2.8.0`, **Smarty 3** is recommended. Please check this [reference](https://lab.civicrm.org/dev/core/-/issues/4146) for more information

### Disclaimer

**ReportPlus** extends from the CiviCRM core Report framework, which means that any change on this framework and its related classes (specially `CRM/Report/*.php` files) from the new
CiviCRM monthly releases, a revision on **ReportPlus** compatibility must be performed. And this involves a big continuous effort we cannot commit monthly basis.

**ReportPlus** releases will guarantee compatibility with the [CiviCRM ESR](https://civicrm.org/esr) bi-annual releases. Please follow the *Requirements* section to know which version to install
on each [CiviCRM ESR](https://civicrm.org/esr) version


## Usage

1. Install the extensions
2. Set Drupal permissions as needed
3. Access to *Administer > CiviReport > Create New Report from Template* and at the end of the page you will see a new group of templates grouped by **ReportPlus Templates**


## Support and Maintenance

This extension is supported and maintained by:

[![iXiam Global Solutions](images/ixiam-logo.png)](https://www.ixiam.com)

The extension is licensed under [AGPL-3.0](LICENSE.txt).
