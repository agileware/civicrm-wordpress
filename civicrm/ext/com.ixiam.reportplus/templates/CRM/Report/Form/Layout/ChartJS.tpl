{if !empty($chartJSEnabled)}
<div id="canvas-holder" style="margin: auto;width:90%;height: 40vh;max-height: 400px;padding-bottom: 50px;">
  <canvas id="chartJS_{$uniqueChartJSId}"></canvas>
</div>

{literal}
<script>
CRM.$(document).ready( function () {
  var chartId = {/literal}"chartJS_{$uniqueChartJSId}";{literal}
  const ctx = document.getElementById(chartId);
  const myChart = new Chart(ctx, {
    type: {/literal}'{$chartJSType}'{literal},
    data: loadData(),
    options: loadOptions()
  });

  function loadData() {
    var chartJSData = {/literal}{$chartJSData}{literal};
    return chartJSData;
  }

  function loadOptions() {
    var chartJSOptions = {/literal}{$chartJSOptions}{literal};
    return chartJSOptions;
  }
});
</script>
{/literal}
{/if}
