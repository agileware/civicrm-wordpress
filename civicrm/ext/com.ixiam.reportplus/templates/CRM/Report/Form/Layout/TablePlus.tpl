{if (!empty($resultTableEnabled) && $rows)}
    {if $pager and $pager->_response and $pager->_response.numPages > 1}
        <div class="report-pager">
            {include file="CRM/common/pager.tpl" location="top"}
        </div>
    {/if}
    <table class="report-layout display">
        {capture assign="tableHeader"}
            <th>&nbsp;</th>
            {foreach from=$columnHeaders item=header key=field}
                {assign var=class value=""}
                {if $header.type eq 1024 OR $header.type eq 1 OR $header.type eq 512}
                {assign var=class value="class='reports-header-right'"}
                {else}
                    {assign var=class value="class='reports-header'"}
                {/if}
                {if !$skip}
                   {if $header.colspan}
                       <th colspan={$header.colspan}>{$header.title|escape}</th>
                      {assign var=skip value=true}
                      {assign var=skipCount value=$header.colspan}
                      {assign var=skipMade  value=1}
                   {else}
                       <th {$class}>{$header.title|escape}</th>
                   {assign var=skip value=false}
                   {/if}
                {else} {* for skip case *}
                   {assign var=skipMade value=$skipMade+1}
                   {if $skipMade >= $skipCount}{assign var=skip value=false}{/if}
                {/if}
            {/foreach}
        {/capture}

        {if !$sections} {* section headers and sticky headers aren't playing nice yet *}
            <thead class="sticky">
            <tr>
              {$tableHeader|smarty:nodefaults}
            </tr>
        </thead>
        {/if}

        {foreach from=$rows item=row key=rowid}
            <tr class="{cycle values="odd-row,even-row"} {$row.class} crm-report" id="crm-report_{$rowid}">
                <th class="crm-report-row">{$rowHeaders[$row.row].title}</th>
                {foreach from=$columnHeaders item=header key=field}
                    <td class="crm-report-{$field}{if $header.type eq 1024 OR $header.type eq 1 OR $header.type eq 512} report-contents-right{elseif $row.$field eq 'Subtotal'} report-label{/if}">
                        {if (!empty($rowDecorators)  && !isset($rowDecorators.$rowid.$field.url))}
                            {assign var=fieldLink value=$rowDecorators.$rowid.$field.url}
                            {if !empty($fieldLink)}
                                <a title="" href="{$fieldLink}">
                            {/if}
                        {/if}

                      {if array_key_exists($field, $row) && is_array($row.$field)}
                          {foreach from=$row.$field item=fieldrow key=fieldid}
                              <div class="crm-report-{$field}-row-{$fieldid}">{$fieldrow}</div>
                          {/foreach}
                      {elseif array_key_exists($field, $row) && $row.$field eq 'Subtotal'}
                          {$row.$field}
                      {elseif $header.type & 4 OR $header.type & 256}
                          {if $header.group_by eq 'MONTH' or $header.group_by eq 'QUARTER'}
                              {$row.$field|crmDate:$config->dateformatPartial}
                          {elseif $header.group_by eq 'YEAR'}
                              {$row.$field|crmDate:$config->dateformatYear}
                          {else}
                              {if $header.type == 4}
                                 {$row.$field|truncate:10:''|crmDate}
                              {else}
                                 {$row.$field|crmDate}
                              {/if}
                          {/if}
                      {elseif $header.type eq 1024}
                          {if $currencyColumn}
                              <span class="nowrap">{$row.$field|crmMoney:$row.$currencyColumn}</span>
                          {else}
                              <span class="nowrap">{$row.$field|crmMoney}</span>
                         {/if}
                      {elseif array_key_exists($field, $row)}
                          {$row.$field|smarty:nodefaults|purify}
                      {/if}

                      {if !empty($fieldLink)}</a>{/if}
                  </td>
              {/foreach}
          </tr>
        {/foreach}

        {if $grandStat}
            {* foreach from=$grandStat item=row*}
            <tr class="total-row">
                {foreach from=$columnHeaders item=header key=field}
                    <td class="report-label">
                        {if $header.type eq 1024}
                            {if $currencyColumn}
                                {$grandStat.$field|crmMoney:$row.$currencyColumn}
                            {else}
                                {$grandStat.$field|crmMoney}
                            {/if}
                        {elseif array_key_exists($field, $grandStat)}
                            {$grandStat.$field}
                        {/if}
                    </td>
                {/foreach}
            </tr>
            {* /foreach*}
        {/if}
    </table>
    {if $pager and $pager->_response and $pager->_response.numPages > 1}
        <div class="report-pager">
            {include file="CRM/common/pager.tpl" location="bottom"}
        </div>
    {/if}
{/if}
