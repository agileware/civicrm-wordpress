<div id="report-tab-chart-js" class="civireport-criteria">
  <table class="report-layout">
    <tr class="crm-report crm-report-criteria-aggregate">
      <td>
        <div id='crm-chartjs_enabled'>
          <label>{$form.chartjs_enabled.label}</label>&nbsp;&nbsp;{$form.chartjs_enabled.html}
        </div>
      </td>
    </tr>
    <tr class="crm-report crm-report-criteria-aggregate">
      <td>
        <div id='crm-resultTable_hide'>
          <label>{$form.resultTable_hide.label}</label>&nbsp;&nbsp;{$form.resultTable_hide.html}
        </div>
      </td>
    </tr>
    <tr class="crm-report crm-report-criteria-aggregate">
      <td>
        <div id='crm-charts'>
          <label>{$form.charts.label}</label>&nbsp;&nbsp;{$form.charts.html}
        </div>
      </td>
    </tr>
    <tr class="crm-report crm-report-criteria-aggregate">
      <td>
        <h2>Line Chart options</h2>
        <div id='crm-chartjs_line'>
          <label>{$form.chartjs_line_fill.label}</label>&nbsp;&nbsp;{$form.chartjs_line_fill.html} <br />
          <label>{$form.chartjs_line_smooth.label}</label>&nbsp;&nbsp;{$form.chartjs_line_smooth.html} <br />
        </div>
      </td>
    </tr>
  </table>
</div>
