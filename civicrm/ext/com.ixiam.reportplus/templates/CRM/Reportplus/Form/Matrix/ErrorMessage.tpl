{if empty($rows)}
  <div class="messages status no-popup">
    {icon icon="fa-info-circle"}{/icon} {ts}None found.{/ts}
  </div>
{/if}
