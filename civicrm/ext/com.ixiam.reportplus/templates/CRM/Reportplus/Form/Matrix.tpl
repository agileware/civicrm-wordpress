{if ($context == "widget") OR ($context == "dashlet")}
  <div class="crm-block crm-content-block crm-report-layoutGraph-form-block">
    {include file="CRM/Report/Form/Layout/ChartJS.tpl"}
  </div>
  <div class="crm-block crm-content-block crm-report-layoutTable-form-block">
    {include file="CRM/Report/Form/Layout/TablePlus.tpl"}
  </div>
{else}
  {if $section eq 1}
    <div class="crm-block crm-content-block crm-report-layoutGraph-form-block">
      {*include the graph*}
      {include file="CRM/Report/Form/Layout/ChartJS.tpl"}
    </div>
  {elseif $section eq 2}
    <div class="crm-block crm-content-block crm-report-layoutTable-form-block">
      {*include the table layout*}
      {include file="CRM/Report/Form/Layout/TablePlus.tpl"}
    </div>
  {else}
    {if $criteriaForm OR $instanceForm OR $instanceFormError}
      <div class="crm-block crm-form-block crm-report-field-form-block">
        {include file="CRM/Report/Form/Fields.tpl"}
      </div>
    {/if}

    <div class="crm-block crm-content-block crm-report-form-block">
      {*include actions*}
      {include file="CRM/Report/Form/Actions.tpl"}

      {*Statistics at the Top of the page*}
      {include file="CRM/Reportplus/Form/Matrix/Statistics.tpl" top=true bottom=false}

      {*include the graph*}
      {include file="CRM/Report/Form/Layout/ChartJS.tpl"}

      {*include the table layout*}
      {include file="CRM/Report/Form/Layout/TablePlus.tpl"}
      <br />
      {*Statistics at the bottom of the page*}
      {include file="CRM/Reportplus/Form/Matrix/Statistics.tpl" top=false bottom=true}

      {include file="CRM/Reportplus/Form/Matrix/ErrorMessage.tpl"}
    </div>
  {/if}
  {if !empty($outputMode) && $outputMode == 'print'}
    <script type="text/javascript">
      window.print();
    </script>
  {/if}
{/if}
