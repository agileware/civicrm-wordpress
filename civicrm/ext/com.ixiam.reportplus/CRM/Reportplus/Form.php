<?php
use CRM_Reportplus_ExtensionUtil as E;

/**
 * Class CRM_Reportplus_Form
 */
class CRM_Reportplus_Form extends CRM_Report_Form {

  public $_csvEncoding      = NULL;
  public $_csvFilename      = 'Report';
  public $_csvFileExtension = 'csv';
  public $_csvShowHeaders   = TRUE;
  public $_csvSeparator     = NULL;
  public $_csvEnclose       = TRUE;

  protected $_newColumnsTab = FALSE;
  protected $_colPositions  = NULL;

  protected $_campaignEnabled = FALSE;
  protected $_activeCampaigns = NULL;
  protected $_disabledCampaigns = [];

  protected $_chartJSEnabled = FALSE;
  protected $_chartJSType;

  protected $_context;

  /**
   * Property to be overriden by every report to match the contact_id filter name
   *
   * @var string
   */
  protected $contactIDField = "id";


  /**
   * Array of extra buttons
   *
   * E.g we define the tab title, the tpl and the tab-specific part of the css or  html link.
   *
   *  $this->_extraActions[report_instance.<action_name>] = array(
   *    'title' => ts('Create Report'),
   *    'data' => array(
   *       'is_confirm' => TRUE,
   *       'confirm_title' => ts('Create Report'),
   *       'confirm_refresh_fields' => json_encode(array(
   *         'title' => array('selector' => '.crm-report-instanceForm-form-block-title', 'prepend' => ''),
   *         'description' => array('selector' => '.crm-report-instanceForm-form-block-description', 'prepend' => ''),
   *       )),
   *     ),
   *  );
   *
   * @var array
   */
  protected $_extraActions = [];

  public function __construct() {
    $this->_options['csvEncoding'] = [
      'type'      => 'select',
      'title'     => E::ts('CSV Encoding'),
      'options'   => [
        ""              => "-",
        "utf-8"         => "UTF-8",
        "windows-1252"  => "Windows-1252",
        "iso-8859-1"    => "iso-8859-1",
      ],
    ];
    $this->_options['limit'] = [
      'type'      => 'select',
      'title'     => ts('Limit'),
      'options'   => [
        ""    => "-",
        "5"   => "5",
        "10"  => "10",
        "25"  => "25",
        "50"  => "50",
        "100" => "100",
        "200" => "200",
      ],
    ];

    parent::__construct();

    // set collapsable group title
    $groupTitle = [
      'civicrm_membership' => ts('Membership'),
      'civicrm_membership_status' => ts('Membership Status'),
      'civicrm_contribution' => ts('Contribution'),
      'civicrm_address' => ts('Address'),
      'civicrm_contact' => ts('Contact'),
      'civicrm_tag' => ts('Tag'),
      'civicrm_group' => ts('Group'),
      'civicrm_relationship' => ts('Relationship'),
      'civicrm_participant' => ts('Participant'),
      'civicrm_contribution_soft' => ts('Soft Credit'),
      'civicrm_note' => ts('Notes'),
      'civicrm_contribution_ordinality' => ts('Contribution Ordinality'),
      'civicrm_email' => ts('Email'),
      'civicrm_phone' => ts('Phone'),
      'civicrm_financial_trxn' => ts('Financial Transaction'),
      'civicrm_financial_type' => ts('Financial Type'),
      'civicrm_activity' => ts('Activity'),
      'civicrm_pledge_payment' => ts('Pledge Payment'),
      'civicrm_contribution_recur' => ts('Recurring Contributions'),
      'civicrm_event' => ts('Event'),
      'civicrm_participant' => ts('Participant'),
      'civicrm_line_item' => ts('Price Set'),
    ];
    foreach ($groupTitle as $key => $title) {
      if (isset($this->_columns[$key])) {
        $this->_columns[$key]['group_title'] = $title;
      }
    }

    $config = CRM_Core_Config::singleton();
    $this->_campaignEnabled = in_array("CiviCampaign", $config->enableComponents);
    if ($this->_campaignEnabled) {
      $getCampaigns = CRM_Campaign_BAO_Campaign::getPermissionedCampaigns(NULL, NULL, TRUE, FALSE, TRUE);
      $this->_activeCampaigns = $getCampaigns['campaigns'];
      asort($this->_activeCampaigns);

      $result = civicrm_api3('Campaign', 'get', ['is_active' => 0]);
      foreach ($result["values"] as $key => $value) {
        $this->_disabledCampaigns[$key] = $value["title"];
      }
      asort($this->_disabledCampaigns);
    }
  }

  /**
   * End post processing.
   *
   * @param array|null $rows
   */
  public function endPostProcess(&$rows = NULL) {
    if ($this->_outputMode == 'csv') {
      if (!empty($this->_params['csvEncoding'])) {
        $this->_csvEncoding = $this->_params['csvEncoding'];
      }
      CRM_Reportplus_Utils_Report::export2csv($this, $rows);
    }
    else {
      parent::endPostProcess($rows);
    }
  }

  /**
   * Get the actions for this report instance.
   *
   * @param int $instanceId
   *
   * @return array
   */
  protected function getActions($instanceId) {
    $actions = parent::getActions($instanceId);
    // Add extra Actions
    $actions += $this->_extraActions;

    return $actions;
  }

  public function addColumns() {
    if (!$this->_newColumnsTab) {
      parent::addColumns();
    }
    else {
      $options = [];
      $colGroups = NULL;
      foreach ($this->_columns as $tableName => $table) {
        if (array_key_exists('fields', $table)) {
          foreach ($table['fields'] as $fieldName => $field) {
            $groupTitle = '';
            if (empty($field['no_display'])) {
              foreach (['table', 'field'] as $var) {
                if (!empty(${$var}['grouping'])) {
                  if (!is_array(${$var}['grouping'])) {
                    $tableName = ${$var}['grouping'];
                  }
                  else {
                    $tableName = array_keys(${$var}['grouping']);
                    $tableName = $tableName[0];
                    $groupTitle = array_values(${$var}['grouping']);
                    $groupTitle = $groupTitle[0];
                  }
                }
              }

              if (!$groupTitle && isset($table['group_title'])) {
                $groupTitle = $table['group_title'];
              }

              $colGroups[$tableName]['fields'][$fieldName] = CRM_Utils_Array::value('title', $field);
              if ($groupTitle && empty($colGroups[$tableName]['group_title'])) {
                $colGroups[$tableName]['group_title'] = $groupTitle;
              }
              $options[$fieldName] = CRM_Utils_Array::value('title', $field);
              $this->add('text', "position[{$fieldName}]", NULL, NULL);
            }
          }
        }
      }

      $this->addCheckBox("fields", ts('Select Columns'), $options, NULL,
        NULL, NULL, NULL, $this->_fourColumnAttribute, TRUE
      );

      if (!empty($colGroups)) {
        $this->tabs['FieldSelection'] = [
          'title' => ts('Columns'),
          'tpl' => 'FieldSelectionPlus',
          'div_label' => 'col-groups',
        ];

        // Note this assignment is only really required in buildForm. It is being 'over-called'
        // to reduce risk of being missed due to overridden functions.
        $this->assign('tabs', $this->tabs);
      }

      $this->assign('colGroups', $colGroups);
    }
  }

  public function addAddressFields($groupBy = TRUE, $orderBy = FALSE, $filters = TRUE, $defaults = ['country_id' => TRUE], $fields = TRUE) {
    $addressFields = parent::addAddressFields($groupBy, $orderBy, $filters, $defaults);
    if (!$fields) {
      $addressFields['civicrm_address']['fields'] = [];
    }
    $addressFields['civicrm_address']['grouping'] = 'location-fields';

    return $addressFields;
  }

  public function addCampaignFields($entityTable = 'civicrm_contribution', $groupBy = FALSE, $orderBy = FALSE, $filters = TRUE, $fields = TRUE) {
    parent::addCampaignFields($entityTable, $groupBy, $orderBy, $filters);
    if (!$fields) {
      unset($this->_columns[$entityTable]['fields']['campaign_id']);
    }
  }

  public function addAddressTable() {
    $tables = $this->selectedTables();
    if (isset($tables) && !$this->isTableSelected('civicrm_address')) {
      foreach ($tables as $table) {
        if (substr($table, 0, 14) === "civicrm_value_") {
          $result = civicrm_api3('CustomGroup', 'get', [
            'sequential' => 1,
            'return' => ["extends"],
            'table_name' => $table,
          ]);
          if ($result['values'][0]['extends'] == 'Address') {
            $this->_selectedTables[] = 'civicrm_address';
            break;
          }
        }
      }
    }
  }

  /**
   * Join address table from custom address table
   */
  public function joinAddressFromContact($prefix = '', $extra = []) {
    $this->addAddressTable();
    parent::joinAddressFromContact($prefix, $extra);
  }

  public function preProcessCommon() {
    parent::preProcessCommon();

    $this->_context = CRM_Utils_Request::retrieve('context', 'String');
    $this->assign('context', $this->_context);

    if ($this->_newColumnsTab) {
      if (!$this->_id) {
        foreach ($this->_columns as $tableName => $table) {
          if (isset($table['fields'])) {
            foreach ($table['fields'] as $key => $field) {
              if (isset($field['position']) && !empty($field['position'])) {
                $this->_colPositions[$key] = $field['position'];
              }
            }
          }
        }
      }
    }

    if (isset($this->_colPositions)) {
      uasort($this->_colPositions, ['CRM_Reportplus_Form', '_positionSort']);
    }
    $this->assign('colPositions', $this->_colPositions);

    if ($this->_chartJSEnabled) {
      $this->addChartJS();
    }
  }

  public function addChartJS() {
    $this->tabs['Charts'] = [
      'title' => E::ts('Charts'),
      'tpl' => 'ChartJS',
      'div_label' => 'chart-js',
    ];

    $chartTypes = [
      'line' => 'Line Chart',
      'bar' => 'Bar Chart',
      'horizontalBar' => 'Horizontal Bar Chart',
      //'radar' => 'Radar Chart',
      'pie' => 'Pie Chart',
      'doughnut' => 'Doughnut Chart',
      //'polarArea' => 'Polar Area Chart',
      //'bubble' => 'Bubble Chart',
      //'scatter' => 'Scatter Chart',

    ];

    $this->addElement('checkbox', 'chartjs_enabled', E::ts('Chart Enabled'));
    $this->addElement('checkbox', 'resultTable_hide', E::ts('Show chart only'));
    $this->addElement('select', 'charts', E::ts('Chart Type'), ['' => E::ts('-- Select --')] + $chartTypes, ['class' => 'crm-select2 huge']);

    $this->addElement('checkbox', 'chartjs_line_fill', E::ts('Fill Enabled'));
    $this->addElement('checkbox', 'chartjs_line_smooth', E::ts('Smooth lines'));
  }

  /**
   * Format display output.
   *
   * @param array $rows
   * @param bool $pager
   */
  public function formatDisplay(&$rows, $pager = TRUE) {
    parent::formatDisplay($rows, $pager);

    if (!empty($this->_params['chartjs_enabled']) && !empty($rows)) {
      $this->buildChartJS($rows);
      $this->assign('chartJSEnabled', TRUE);
    }
    if (!empty($this->_params['resultTable_hide']) && !empty($rows)) {
      $this->assign('resultTableEnabled', FALSE);
    }
    else {
      $this->assign('resultTableEnabled', TRUE);
    }
  }

  public function buildChartJS(&$rows) {
    // override this method for building Chart JS.
  }

  public function setDefaultValues($freeze = TRUE) {
    parent::setDefaultValues($freeze);

    if ($this->_newColumnsTab) {
      if (!isset($this->_defaults['position'])) {
        $this->_defaults['position'] = $this->_colPositions;
      }
      else {
        $this->_colPositions = isset($this->_colPositions) ? array_merge($this->_colPositions, $this->_defaults['position']) : $this->_defaults['position'];
        uasort($this->_colPositions, ['CRM_Reportplus_Form', '_positionSort']);
        $this->assign('colPositions', $this->_colPositions);
      }
    }

    $contact_id = $this->getContactIdFilterFromURL();
    if ($contact_id) {
      $this->_defaults[$this->contactIDField . '_value'] = $contact_id;
      $this->_defaults[$this->contactIDField . '_op'] = 'in';
    }

    return $this->_defaults;
  }

  public function beginPostProcessCommon() {
    if ($this->_newColumnsTab) {
      $this->_colPositions = $this->_params['position'] ?? [];
      uasort($this->_colPositions, ['CRM_Reportplus_Form', '_positionSort']);
      $this->assign('colPositions', $this->_colPositions);
    }
  }

  public function modifyColumnHeaders() {
    if ($this->_newColumnsTab) {
      foreach ($this->_columns as $tableName => $table) {
        if (array_key_exists('fields', $table)) {
          foreach ($table['fields'] as $fieldName => $field) {
            if (!empty($field['statistics'])) {
              foreach ($field['statistics'] as $stat => $label) {
                $alias = "{$tableName}_{$fieldName}_{$stat}";
                switch (strtolower($stat)) {
                  case 'max':
                  case 'sum':
                    $this->_columnHeaders["{$tableName}_{$fieldName}_{$stat}"]['position'] = 97;
                    break;

                  case 'count':
                    $this->_columnHeaders["{$tableName}_{$fieldName}_{$stat}"]['position'] = 98;
                    break;

                  case 'count_distinct':
                    $this->_columnHeaders["{$tableName}_{$fieldName}_{$stat}"]['position'] = 99;
                    break;

                  case 'avg':
                    $this->_columnHeaders["{$tableName}_{$fieldName}_{$stat}"]['position'] = 100;
                    break;
                }
              }
            }
            else {
              if (!empty($this->_colPositions[$fieldName])) {
                $this->_columnHeaders["{$tableName}_{$fieldName}"]['position'] = $this->_colPositions[$fieldName];
              }
            }
          }
        }
      }
    }
    uasort($this->_columnHeaders, ['CRM_Reportplus_Form', '_columnSort']);
  }

  /**
   * Filter statistics.
   * Override parent method to display proper qill for operators
   * ['ept', 'nept', 'nll', 'nnll', 'eptnll', 'neptnnll']
   *
   * @param array $statistics
   */
  public function filterStat(&$statistics) {
    foreach ($this->_columns as $tableName => $table) {
      if (array_key_exists('filters', $table)) {
        foreach ($table['filters'] as $fieldName => $field) {
          if ((CRM_Utils_Array::value('type', $field) & CRM_Utils_Type::T_DATE ||
              CRM_Utils_Array::value('type', $field) & CRM_Utils_Type::T_TIME) &&
            CRM_Utils_Array::value('operatorType', $field) !=
            CRM_Report_Form::OP_MONTH
          ) {
            $from = $this->_params["{$fieldName}_from"] ?? NULL;
            $to = $this->_params["{$fieldName}_to"] ?? NULL;
            if (!empty($this->_params["{$fieldName}_relative"])) {
              list($from, $to) = CRM_Utils_Date::getFromTo($this->_params["{$fieldName}_relative"], NULL, NULL);
            }
            if (strlen($to) === 10) {
              // If we just have the date we assume the end of that day.
              $to .= ' 23:59:59';
            }
            if ($from || $to) {
              if ($from) {
                $from = date('l j F Y, g:iA', strtotime($from));
              }
              if ($to) {
                $to = date('l j F Y, g:iA', strtotime($to));
              }
              $statistics['filters'][] = [
                'title' => $field['title'],
                'value' => ts("Between %1 and %2", [1 => $from, 2 => $to]),
              ];
            }
            elseif (in_array($rel = CRM_Utils_Array::value("{$fieldName}_relative", $this->_params),
              array_keys($this->getOperationPair(CRM_Report_Form::OP_DATE))
            )) {
              $pair = $this->getOperationPair(CRM_Report_Form::OP_DATE);
              $statistics['filters'][] = [
                'title' => $field['title'],
                'value' => $pair[$rel],
              ];
            }
          }
          else {
            $op = $this->_params["{$fieldName}_op"] ?? NULL;
            $value = NULL;
            if ($op) {
              $pair = $this->getOperationPair(
                CRM_Utils_Array::value('operatorType', $field),
                $fieldName
              );
              $min = $this->_params["{$fieldName}_min"] ?? NULL;
              $max = $this->_params["{$fieldName}_max"] ?? NULL;
              $val = $this->_params["{$fieldName}_value"] ?? NULL;
              if (in_array($op, ['bw', 'nbw']) && ($min || $max)) {
                $value = "{$pair[$op]} $min " . ts('and') . " $max";
              }
              elseif ($val && CRM_Utils_Array::value('operatorType', $field) & self::OP_ENTITYREF) {
                $this->setEntityRefDefaults($field, $tableName);
                $result = civicrm_api3($field['attributes']['entity'], 'getlist',
                  ['id' => $val] +
                  CRM_Utils_Array::value('api', $field['attributes'], []));
                $values = [];
                foreach ($result['values'] as $v) {
                  $values[] = $v['label'];
                }
                $value = "{$pair[$op]} " . implode(', ', $values);
              }
              elseif (in_array($op, ['ept', 'nept', 'nll', 'nnll', 'eptnll', 'neptnnll'])) {
                $value = $pair[$op];
              }
              elseif (is_array($val) && (!empty($val))) {
                $options = CRM_Utils_Array::value('options', $field, []);
                foreach ($val as $key => $valIds) {
                  if (isset($options[$valIds])) {
                    $val[$key] = $options[$valIds];
                  }
                }
                $pair[$op] = (count($val) == 1) ? (($op == 'notin' || $op ==
                  'mnot') ? ts('Is Not') : ts('Is')) : CRM_Utils_Array::value($op, $pair);
                $val = implode(', ', $val);
                $value = "{$pair[$op]} " . $val;
              }
              elseif (!is_array($val) && (!empty($val) || $val == '0') &&
                isset($field['options']) &&
                is_array($field['options']) && !empty($field['options'])
              ) {
                $value = CRM_Utils_Array::value($op, $pair) . " " .
                  CRM_Utils_Array::value($val, $field['options'], $val);
              }
              elseif ($val) {
                $value = CRM_Utils_Array::value($op, $pair) . " " . $val;
              }
            }
            if ($value && empty($field['no_display'])) {
              $statistics['filters'][] = [
                'title' => $field['title'] ?? NULL,
                'value' => CRM_Utils_String::htmlToText($value),
              ];
            }
          }
        }
      }
    }
  }

  public function whereClause(&$field, $op, $value, $min, $max) {

    $type = CRM_Utils_Type::typeToString(CRM_Utils_Array::value('type', $field));
    $clause = NULL;

    switch ($op) {
      case 'ept':
      case 'nept':
        $sqlOP = $this->getSQLOperator($op);
        $clause = "( {$field['dbAlias']} $sqlOP )";
        break;

      case 'eptnll':
      case 'neptnnll':
        $sqlOP = $this->getSQLOperator($op);
        $clause = str_replace("%field%", "{$field['dbAlias']}", $sqlOP);
        break;

      default:
        $clause = parent::whereClause($field, $op, $value, $min, $max);
        break;
    }

    return $clause;
  }

  public function getBasicContactFields() {
    $result = parent::getBasicContactFields() + [
      'contact_source' => [
        'title' => ts('Source of Contact Data'),
      ],
      'is_deceased' => [
        'title' => ts('Contact is Deceased'),
      ],
      'deceased_date' => [
        'title' => ts('Deceased Date'),
      ],
      'created_date' => [
        'title' => ts('Created Date'),
      ],
    ];

    return $result;
  }

  public function getBasicContactFilters($defaults = []) {
    $result = array_merge(parent::getBasicContactFilters($defaults), [
      'age' => [
        'title' => ts('Age'),
        'type' => CRM_Utils_Type::T_INT,
        'dbAlias' => "DATE_FORMAT(FROM_DAYS((TO_DAYS(NOW())+1)-TO_DAYS(birth_date)), '%Y')+0",
      ],
      'deceased_date' => [
        'title' => ts('Deceased Date'),
        'operatorType' => CRM_Report_Form::OP_DATE,
      ],
      'created_date' => [
        'title' => ts('Created Date'),
        'operatorType' => CRM_Report_Form::OP_DATE,
        'type' => CRM_Utils_Type::T_DATE,
      ],
    ]);

    unset($result['is_deleted']['no_display']);

    $result['is_deleted']['title'] = ts('Deleted');

    return $result;
  }

  /**
   * Create a temporary table.
   *
   * This function creates a table AND adds the details to the developer tab & $this->>temporary tables.
   *
   * @param string $identifier
   *   This is the key that will be used for the table in the temporaryTables property.
   * @param string $sql
   *   Sql select statement or column description (the latter requires the columns flag)
   * @param bool $isColumns
   *   Is the sql describing columns to create (rather than using a select query).
   * @param bool $isMemory
   *   Create a memory table rather than a normal INNODB table.
   *
   * @return string
   */
  public function createTemporaryTable($identifier, $sql, $isColumns = FALSE, $isMemory = FALSE) {
    $tempTable = CRM_Utils_SQL_TempTable::build()->setCategory('reportplus');
    if ($isMemory) {
      $tempTable->setMemory();
    }
    else {
      $tempTable->setDurable();
    }
    if ($isColumns) {
      $tempTable->createWithColumns($sql);
    }
    else {
      $tempTable->createWithQuery($sql);
    }
    $name = $tempTable->getName();
    // Developers may force tables to be durable to assist in debugging so lets check.
    $isNotTrueTemporary = $tempTable->isDurable();
    $this->addToDeveloperTab($tempTable->getCreateSql());
    $this->temporaryTables[$identifier] = ['temporary' => !$isNotTrueTemporary, 'name' => $name];
    return $name;
  }

  public function getBasicContactOrderBys() {
    return [
      'sort_name' => [
        'title' => ts('Last Name, First Name'),
        'default' => '1',
        'default_weight' => '0',
        'default_order' => 'ASC',
      ],
      'first_name' => [
        'name' => 'first_name',
        'title' => ts('First Name'),
      ],
      'gender_id' => [
        'name' => 'gender_id',
        'title' => ts('Gender'),
      ],
      'birth_date' => [
        'name' => 'birth_date',
        'title' => ts('Birth Date'),
      ],
      'contact_type' => [
        'title' => ts('Contact Type'),
      ],
      'contact_sub_type' => [
        'title' => ts('Contact Subtype'),
      ],
    ];
  }

  public function getOperationPair($type = "string", $fieldName = NULL) {
    if (empty($type) || $type == "string") {
      $result = [
        'has' => ts('Contains'),
        'sw' => ts('Starts with'),
        'ew' => ts('Ends with'),
        'nhas' => ts('Does not contain'),
        'eq' => ts('Is equal to'),
        'neq' => ts('Is not equal to'),
        'ept' => E::ts('Is empty'),
        'nept' => E::ts('Is not empty'),
        'nll' => E::ts('Is Null'),
        'nnll' => E::ts('Is not Null'),
        'eptnll' => E::ts('Is empty or Null'),
        'neptnnll' => E::ts('Is not empty and Null'),
      ];
      return $result;
    }
    else {
      return parent::getOperationPair($type, $fieldName);
    }
  }

  public function getSQLOperator($operator = "like") {
    switch ($operator) {
      case 'ept':
        return '= \'\'';

      case 'nept':
        return '!= \'\'';

      case 'eptnll':
        return ' ( %field% IS NULL OR %field% = \'\' ) ';

      case 'neptnnll':
        return ' ( %field% IS NOT NULL AND %field% != \'\' ) ';

      default:
        // type is string
        return parent::getSQLOperator($operator);
    }
  }

  /**
   * Parse contact_id/cid from the url or api input params.
   *
   * @return int|null
   * @throws \CRM_Core_Exception
   */
  protected function getContactIdFilterFromURL() {
    if (empty($this->contactIDField)) {
      return NULL;
    }
    else {
      if (CRM_Utils_Array::value('contact_id', $this->_params)) {
        return $this->_params['contact_id'];
      }
      return CRM_Utils_Request::retrieveValue('cid', 'Int', CRM_Utils_Request::retrieveValue('contact_id', 'Int'));
    }
    return NULL;
  }

  /**
   * Override to set limit is 10
   * @param int $rowCount
   */
  public function limit($rowCount = self::ROW_COUNT_LIMIT) {
    if (isset($this->_params['limit']) && $this->_params['limit']) {
      $rowCount = $this->_params['limit'];
    }
    parent::limit($rowCount);
  }

  /**
   * Alter display of rows.
   *
   * Iterate through the rows retrieved via SQL and make changes for display purposes,
   * such as rendering contacts as links.
   *
   * @param array $rows
   *   Rows generated by SQL, with an array for each row.
   */
  public function alterDisplay(&$rows) {
    $entryFound = FALSE;
    foreach ($rows as $rowNum => $row) {
      // If using campaigns, convert campaign_id to campaign title
      if (array_key_exists('civicrm_contribution_campaign_id', $row)) {
        if ($value = $row['civicrm_contribution_campaign_id']) {
          if (array_key_exists($value, $this->_disabledCampaigns)) {
            $rows[$rowNum]['civicrm_contribution_campaign_id'] = '<font color="#FF0000">' . $this->_disabledCampaigns[$value] . '</font>';
          }
          else {
            $rows[$rowNum]['civicrm_contribution_campaign_id'] = $this->_activeCampaigns[$value];
          }
        }
        $entryFound = TRUE;
      }
      // skip looking further in rows, if first row itself doesn't
      // have the column we need
      if (!$entryFound) {
        break;
      }
    }
  }

  /**
   * Override to set pager with limit is 10
   * @param int $rowCount
   */
  public function setPager($rowCount = self::ROW_COUNT_LIMIT) {
    if (isset($this->_params['limit']) && $this->_params['limit']) {
      $rowCount = $this->_params['limit'];
    }
    parent::setPager($rowCount);
  }

  private function _columnSort($a, $b) {
    return $this->_positionSort(($a['position'] ?? -1), ($b['position'] ?? -1));
  }

  private function _positionSort($a, $b) {
    if (empty($a)) {
      return 1;
    }
    if (empty($b)) {
      return -1;
    }
    if ($a == $b) {
      return 0;
    }

    return ($a > $b) ? +1 : -1;
  }

}
