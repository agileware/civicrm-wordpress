<?php

/**
 * Build various graphs using Chart JS library.
 * http://www.chartjs.org/
 *
 */
class CRM_Reportplus_Utils_ChartJS {

  /**
   * Colours.
   * @var array
   */
  private static $_colours = [
    '#4dc9f6',
    '#f67019',
    '#f53794',
    '#537bc4',
    '#acc236',
    '#166a8f',
    '#00a950',
    '#58595b',
    '#8549ba',
    '#F0E68C',
    '#CD5C5C',
    '#4169E1',
    '#98FB98',
    '#F4A460',
    '#FFC0CB',
    '#008080',
  ];

  /**
   * Total Colours.
   * @var string
   */
  private static $_totalColour = '#2E2929';

  /**
   * @param $rows
   * @param $type
   * @param $xLabels
   * @param $yLabels
   * @param $params
   */
  public static function chart($rows, $type, $xLabels, $yLabels, $params = []) {
    $data = self::getData($rows, $type, $xLabels, $yLabels, $params);
    $options = self::getOptions($type, $params);
    $uniqueChartJSId = md5(uniqid(rand(), TRUE));

    // assign chart data to template
    $template = CRM_Core_Smarty::singleton();
    $template->assign('chartJSData', json_encode($data));
    $template->assign('chartJSOptions', json_encode($options));
    $template->assign('uniqueChartJSId', $uniqueChartJSId);
  }

  public static function getOptions($type, $params = []) {
    $options = [
      'responsive' => TRUE,
      'maintainAspectRatio' => FALSE,
      'legend' => [
        'position' => 'top',
      ],
      'title' => [
        'display' => TRUE,
        'text' => !empty($params['title']) ? $params['title'] : $params['description'],
      ]
    ];

    // https://www.chartjs.org/docs/latest/charts/bar.html#horizontal-bar-chart
    if ($type == 'horizontalBar') {
      $options['indexAxis'] = 'y';
    }

    return $options;
  }

  public static function getData($rows, $type, $xLabels, $yLabels, $params = []) {
    // remove last column from rows id showTotal
    if (!empty($params['showTotals'])) {
      foreach ($rows as $keyA => $valueA) {
        unset($rows[$keyA]['col_total']);
      }
      unset($xLabels['col_total']);
    }

    $data = [];
    $data['labels'] = array_column($xLabels, 'title');
    switch ($type) {
      case 'doughnut':
      case 'pie':
        $data['datasets'] = self::pie($rows, $yLabels, $params);
        break;

      case 'bar':
      case 'horizontalBar':
        $data['datasets'] = self::bar($rows, $yLabels, $params);
        break;

      case 'line':
        $data['datasets'] = self::line($rows, $yLabels, $params);
        break;
    }

    return $data;
  }

  private static function line($rows, $yLabels, $params = []) {
    $datasets = self::bar($rows, $yLabels);

    foreach ($datasets as $key => $dataset) {
      $datasets[$key]['fill'] = !empty($params['chartjs_line_fill']);
      $datasets[$key]['lineTension'] = !empty($params['chartjs_line_smooth']) ? 0.4 : 0;
      if ($dataset['label'] == 'Total') {
        $datasets[$key]['borderWidth'] = 4;
      }
    }

    return $datasets;
  }

  private static function bar($rows, $yLabels, $params = []) {
    $i = 0;
    $datasets = [];

    foreach ($rows as $yLabel => $row) {
      if ($row['rowx'] == 'total') {
        $color = self::$_totalColour;
      }
      else {
        $color = self::$_colours[$i % (count(self::$_colours))];
      }
      $data = self::filterRowData($row);
      $datasets[] = [
        'label' => self::searchTitle($row['rowx'], $yLabels),
      //color(window.chartColors.red).alpha(0.5).rgbString(),
        'backgroundColor' => $color,
      //window.chartColors.red,
        'borderColor' => $color,
        'borderWidth' => 1,
        'data' => $data,
      ];
      $i++;
    }

    return $datasets;
  }

  private static function pie($rows, $yLabels, $params = []) {
    $i = 0;
    $colors = [];
    $datasets = [];

    foreach ($rows as $yLabel => $row) {
      $data = self::filterRowData($row);
      foreach ($row as $value) {
        $colors[] = self::$_colours[$i];
        $i = ($i == count(self::$_colours) - 1) ? 0 : ($i + 1);
      }
      if ($colors[0] == $colors[count($colors) - 1]) {
        $colors[count($colors) - 1] = self::$_colours[1];
      }
      $datasets[] = [
        'label' => self::searchTitle($row['rowx'], $yLabels),
        'backgroundColor' => $colors,
        'data' => $data,
      ];
    }

    return $datasets;
  }

  private static function filterRowData($row) {
    return array_values(array_filter($row, function($k) {
      return preg_match('/col_\d+/', $k);
    }, ARRAY_FILTER_USE_KEY));
  }

  private static function searchTitle($value, $haystack) {
    $key = array_filter($haystack, function ($x) use ($value) {
      return $x['value'] == $value;
    });

    if (is_array($key)) {
      return reset($key)['title'] ?? $value;
    }
    return $value;
  }

}
