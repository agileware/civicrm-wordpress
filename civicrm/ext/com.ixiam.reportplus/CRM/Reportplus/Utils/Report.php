<?php

/**
 * Class CRM_Reportplus_Form
 */
class CRM_Reportplus_Utils_Report extends CRM_Report_Utils_Report {

  /**
   * @param CRM_Core_Form $form
   * @param $form
   * @param $rows
   */
  public static function export2csv(&$form, &$rows) {
    //Mark as a CSV file.
    if (isset($form->_csvEncoding)) {
      header('Content-Type: text/csv; charset=' . $form->_csvEncoding);
    }
    else {
      header('Content-Type: text/csv');
    }

    //Force a download and name the file using the current timestamp.
    $datetime = date('Ymd-Gi', $_SERVER['REQUEST_TIME']);
    header('Content-Disposition: attachment; filename=' . $form->_csvFilename . '_' . $datetime . '.' . $form->_csvFileExtension);

    $csv = self::makeCsv($form, $rows);
    if (isset($form->_csvEncoding)) {
      $csv = mb_convert_encoding($csv, $form->_csvEncoding, "UTF-8");
    }

    echo $csv;
    CRM_Utils_System::civiExit();
  }

  /**
   * Utility function for export2csv and CRM_Report_Form::endPostProcess
   * - make CSV file content and return as string.
   */
  public static function makeCsv(&$form, &$rows) {
    $config = CRM_Core_Config::singleton();
    $csv = '';
    if (!isset($form->_csvSeparator)) {
      $form->_csvSeparator = $config->fieldSeparator;
    }

    // Add headers if this is the first row.
    $columnHeaders = array_keys($form->_columnHeaders);

    // Replace internal header names with friendly ones, where available.
    if ($form->_csvShowHeaders) {
      foreach ($columnHeaders as $header) {
        if (isset($form->_columnHeaders[$header])) {
          $headers[] = '"' . html_entity_decode(strip_tags($form->_columnHeaders[$header]['title'])) . '"';
        }
      }
      // Add the headers.
      $csv .= implode($form->_csvSeparator, $headers) . "\r\n";
    }

    $displayRows = [];
    $value = NULL;
    foreach ($rows as $row) {
      foreach ($columnHeaders as $k => $v) {
        $value = CRM_Utils_Array::value($v, $row);
        if (isset($value)) {
          // Remove HTML, unencode entities, and escape quotation marks.
          if ($form->_csvEnclose) {
            $value = str_replace('"', '""', html_entity_decode(strip_tags($value)));
          }
          else {
            $value = html_entity_decode(strip_tags($value));
          }

          if (CRM_Utils_Array::value('type', $form->_columnHeaders[$v]) & 4) {
            if (CRM_Utils_Array::value('group_by', $form->_columnHeaders[$v]) == 'MONTH' ||
              CRM_Utils_Array::value('group_by', $form->_columnHeaders[$v]) == 'QUARTER'
            ) {
              $value = CRM_Utils_Date::customFormat($value, $config->dateformatPartial);
            }
            elseif (CRM_Utils_Array::value('group_by', $form->_columnHeaders[$v]) == 'YEAR') {
              $value = CRM_Utils_Date::customFormat($value, $config->dateformatYear);
            }
            elseif ($form->_columnHeaders[$v]['type'] == 12) {
              // This is a datetime format
              $value = CRM_Utils_Date::customFormat($value, '%Y-%m-%d %H:%i');
            }
            else {
              $value = CRM_Utils_Date::customFormat($value, '%Y-%m-%d');
            }
          }
          elseif (CRM_Utils_Array::value('type', $form->_columnHeaders[$v]) == 1024) {
            $value = CRM_Utils_Money::format($value, $row['civicrm_contribution_currency']);
          }

          if ($form->_csvEnclose) {
            $displayRows[$v] = '"' . $value . '"';
          }
          else {
            $displayRows[$v] = $value;
          }
        }
        else {
          $displayRows[$v] = "";
        }
      }
      // Add the data row.
      $csv .= implode($form->_csvSeparator, $displayRows) . "\r\n";
    }

    return $csv;
  }

  /**
   * Get action links.
   *
   * @param int $instanceID
   * @param string $className
   *
   * @return array
   */
  public static function getActionLinks($instanceID, $className) {
    $urlCommon = 'civicrm/report/instance/' . $instanceID;
    $actions = [
      'copy' => [
        'url' => CRM_Utils_System::url($urlCommon, 'reset=1&output=copy'),
        'label' => ts('Save a Copy'),
      ],
      'pdf' => [
        'url' => CRM_Utils_System::url($urlCommon, 'reset=1&force=1&output=pdf'),
        'label' => ts('View as pdf'),
      ],
      'print' => [
        'url' => CRM_Utils_System::url($urlCommon, 'reset=1&force=1&output=print'),
        'label' => ts('Print report'),
      ],
    ];
    if (CRM_Core_Permission::check('administer Reports')) {
      $actions['delete'] = [
        'url' => CRM_Utils_System::url($urlCommon, 'reset=1&action=delete'),
        'label' => ts('Delete report'),
        'confirm_message' => ts('Are you sure you want delete this report? This action cannot be undone.'),
      ];
    }

    return $actions;
  }

  /**
   * Build a URL query string containing all report filter criteria that are
   * stipulated in $_GET or in a report Preview, but which haven't yet been
   * saved in the report instance.
   *
   * @param array $defaults
   *   The report criteria that aren't coming in as submitted form values, as in CRM_Report_Form::_defaults.
   * @param array $params
   *   All effective report criteria, as in CRM_Report_Form::_params.
   *
   * @return string
   *   URL query string
   */
  public static function getPreviewCriteriaQueryParams($defaults = [], $params = []) {
    static $query_string;
    if (!isset($query_string)) {
      if (!empty($params)) {
        $url_params = $op_values = $string_values = $process_params = [];

        // We'll only use $params that are different from what's in $default.
        foreach ($params as $field_name => $field_value) {
          if (!array_key_exists($field_name, $defaults) || $defaults[$field_name] != $field_value) {
            $process_params[$field_name] = $field_value;
          }
        }
        // Criteria stipulated in $_GET will be in $defaults even if they're not
        // saved, so we can't easily tell if they're saved or not. So just include them.
        $process_params += $_GET;

        // All $process_params should be passed on if they have an effective value
        // (in other words, there's no point in propagating blank filters).
        foreach ($process_params as $field_name => $field_value) {
          $suffix_position = strrpos($field_name, '_');
          $suffix = substr($field_name, $suffix_position);
          $basename = substr($field_name, 0, $suffix_position);
          if ($suffix == '_min' || $suffix == '_max' ||
            $suffix == '_from' || $suffix == '_to' ||
            $suffix == '_relative'
          ) {
            // For these types, we only keep them if they have a value.
            if (!empty($field_value)) {
              // Normalize date format without dashes to send in querystring
              if ($suffix == '_from' || $suffix == '_to') {
                $url_params[$field_name] = str_replace("-", "", $field_value);
              }
              else {
                $url_params[$field_name] = $field_value;
              }
            }
          }
          elseif ($suffix == '_value') {
            // These filters can have an effect even without a value
            // (e.g., values for 'nll' and 'nnll' ops are blank),
            // so store them temporarily and examine below.
            $string_values[$basename] = $field_value;
            $op_values[$basename] = $params["{$basename}_op"] ?? NULL;
          }
          elseif ($suffix == '_op') {
            // These filters can have an effect even without a value
            // (e.g., values for 'nll' and 'nnll' ops are blank),
            // so store them temporarily and examine below.
            $op_values[$basename] = $field_value;
            $string_values[$basename] = $params["{$basename}_value"];
          }
        }

        // Check the *_value and *_op criteria and include them if
        // they'll have an effective value.
        foreach ($op_values as $basename => $field_value) {
          if ($field_value == 'nll' || $field_value == 'nnll') {
            // 'nll' and 'nnll' filters should be included even with empty values.
            $url_params["{$basename}_op"] = $field_value;
          }
          elseif ($string_values[$basename]) {
            // Other filters are only included if they have a value.
            $url_params["{$basename}_op"] = $field_value;
            $url_params["{$basename}_value"] = (is_array($string_values[$basename]) ? implode(',', $string_values[$basename]) : $string_values[$basename]);
          }
        }
        $query_string = http_build_query($url_params);
      }
      else {
        $query_string = '';
      }
    }
    return $query_string;
  }

  /**
   * Clear leftover temporary tables.
   */
  public static function clearTempTables() {
    $dao = new CRM_Core_DAO();
    $query = "
      SELECT TABLE_NAME as tableName
      FROM   INFORMATION_SCHEMA.TABLES
      WHERE  TABLE_SCHEMA = %1
        AND TABLE_NAME LIKE 'civicrm_tmp_d_reportplus%'
    ";

    $tableDAO = CRM_Core_DAO::executeQuery($query, [1 => [$dao->database(), 'String']]);
    while ($tableDAO->fetch()) {
      CRM_Core_DAO::executeQuery("DROP TABLE " . $tableDAO->tableName);
    }
  }

}
