<?php
return [
  [
    'name' => 'Cron:Reportplus.cleanup',
    'entity' => 'Job',
    'params' => [
      'version' => 3,
      'name' => 'ReportPlus Clean-Up Task',
      'description' => 'Removes temporary tables created by ReportPlus Matrix reports',
      'run_frequency' => 'Daily',
      'api_entity' => 'Reportplus',
      'api_action' => 'cleanup',
      'parameters' => '',
      'is_active' => FALSE,
    ],
  ],
];
