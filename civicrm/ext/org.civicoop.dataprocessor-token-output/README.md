# Data Processor Token Output

With this extension extension you can easily configure custom tokens through a data processor.

This is done by adding a Token type output to your data processor.   

The extension is licensed under [AGPL-3.0](LICENSE.txt).

## Requirements

* CiviCRM
* [Data Processor](https://lab.civicrm.org/extensions/dataprocessor) version 1.1
