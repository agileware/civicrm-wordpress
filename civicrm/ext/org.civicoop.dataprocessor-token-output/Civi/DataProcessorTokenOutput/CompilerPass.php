<?php
/**
 * @author Jaap Jansma <jaap.jansma@civicoop.org>
 * @license AGPL-3.0
 */

namespace Civi\DataProcessorTokenOutput;

use CRM_DataprocessorTokenOutput_ExtensionUtil as E;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Civi\DataProcessorTokenOutput\Symfony\Component\DependencyInjection\DefinitionAdapter;

class CompilerPass implements CompilerPassInterface {

  /**
   * You can modify the container here before it is dumped to PHP code.
   */
  public function process(ContainerBuilder $container) {
    if (!$container->hasDefinition('data_processor_factory')) {
      return;
    }

    $factoryDefinition = $container->getDefinition('data_processor_factory');
    $tokenFactoryDefinition = DefinitionAdapter::createDefinitionClass('Civi\DataProcessorTokenOutput\TokenFactory');

    $factoryDefinition->addMethodCall('addOutput', array(
      'token', 'Civi\DataProcessorTokenOutput\Output\ContactToken', E::ts('Tokens')
    ));
    $tokenFactoryDefinition->addMethodCall('addTokenOutputType', ['token']);

    $factoryDefinition->addMethodCall('addOutput', array(
      'household_token', 'Civi\DataProcessorTokenOutput\Output\HouseholdToken', E::ts('Household Tokens')
    ));
    $tokenFactoryDefinition->addMethodCall('addTokenOutputType', ['household_token']);

    $factoryDefinition->addMethodCall('addOutput', array(
      'participant_token', 'Civi\DataProcessorTokenOutput\Output\ParticipantToken', E::ts('Participants Tokens')
    ));
    $tokenFactoryDefinition->addMethodCall('addTokenOutputType', ['participant_token']);

    $factoryDefinition->addMethodCall('addOutput', array(
      'case_token', 'Civi\DataProcessorTokenOutput\Output\CaseToken', E::ts('Case Tokens')
    ));
    $tokenFactoryDefinition->addMethodCall('addTokenOutputType', ['case_token']);

    $factoryDefinition->addMethodCall('addOutput', array(
      'event_token', 'Civi\DataProcessorTokenOutput\Output\EventToken', E::ts('Event Tokens')
    ));
    $tokenFactoryDefinition->addMethodCall('addTokenOutputType', ['event_token']);

    $container->setDefinition('data_processor_token_factory', $tokenFactoryDefinition);
  }


}
