<?php
/**
 * @author Jaap Jansma <jaap.jansma@civicoop.org>
 * @license AGPL-3.0
 */

namespace Civi\DataProcessorTokenOutput\Output;

use Civi\DataProcessor\DataFlow\EndOfFlowException;
use Civi\DataProcessor\DataFlow\Sort\SortCompareFactory;
use Civi\DataProcessor\DataFlow\SqlDataFlow;
use Civi\DataProcessor\Exception\DataSourceNotFoundException;
use Civi\DataProcessor\Exception\FieldNotFoundException;
use Civi\DataProcessor\Output\OutputInterface;

use Civi\DataProcessor\ProcessorType\AbstractProcessorType;
use CRM_DataprocessorTokenOutput_ExtensionUtil as E;

class ParticipantToken extends AbstractToken {

  /**
   * Array containing all the related participant ids
   *
   * This property is set when executed from an participant search action.
   *
   * @var array
   */
  private static $_participantIds = null;

  /**
   * @return string
   */
  protected function getTokenProcessorIdFieldName() {
    return 'participantId';
  }

  /**
   * Function to set a different contact ID to be used in the data processor.
   * This function is here so child classes can override it.
   *
   * @param $contactId
   * @param $configuration
   *
   * @return mixed
   */
  protected function getIdForToken($contactId, $values, $configuration) {
    if (isset($values['extra_data']['participant']['id'])) {
      // Coming from CiviRules
      return $values['extra_data']['participant']['id'];
    } elseif (is_array(self::$_participantIds) && count(self::$_participantIds)) {
      return array_shift(self::$_participantIds);
    }
    return false;
  }

  /**
   * Returns the title of the ID field.
   *
   * @return String
   */
  protected function getIdFieldTitle() {
    return E::ts('Participant ID Field');
  }

  /**
   * @return string
   */
  protected function getIdFieldName() {
    return 'participant_id_field';
  }

  /**
   * Extract the event ids from the event task forms.
   * We need this as soon as the task is a send letter or send e-mail from the action
   * after a search for participants.
   *
   * We can then use those event ids during the processing of the tokens.
   *
   * @param $form
   */
  public static function extractParticipantIdsFromEventFormTasks(\CRM_Core_Form $form) {
    if ($form instanceof \CRM_Event_Form_Task) {
      self::$_participantIds = $form->getVar('_participantIds');
    }
  }


}
