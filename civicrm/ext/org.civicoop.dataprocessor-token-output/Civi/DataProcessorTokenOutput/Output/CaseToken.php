<?php
/**
 * @author Jaap Jansma <jaap.jansma@civicoop.org>
 * @license AGPL-3.0
 */

namespace Civi\DataProcessorTokenOutput\Output;

use CRM_DataprocessorTokenOutput_ExtensionUtil as E;

class CaseToken extends AbstractToken {

  /**
   * Array containing all the related case ids
   *
   * This property is set when executed from a case action.
   *
   * @var array
   */
  private static $_caseIds = null;

  /**
   * @return string
   */
  protected function getTokenProcessorIdFieldName() {
    return 'caseId';
  }

  /**
   * Function to set a different contact ID to be used in the data processor.
   * This function is here so child classes can override it.
   *
   * @param $contactId
   * @param $configuration
   *
   * @return mixed
   */
  protected function getIdForToken($contactId, $values, $configuration) {
    if (isset($values['caseId'])) {
      return $values['caseId'];
    } elseif (isset($values['case_id'])) {
        return $values['case_id'];
    } elseif (isset($values['extra_data']['case']['id'])) {
      // Coming from CiviRules
      return $values['extra_data']['case']['id'];
    } elseif (is_array(self::$_caseIds) && count(self::$_caseIds)) {
      return array_shift(self::$_caseIds);
    }  elseif (self::$_caseIds) {
      return self::$_caseIds;
    }
    return false;
  }

  /**
   * Returns the title of the ID field.
   *
   * @return String
   */
  protected function getIdFieldTitle() {
    return E::ts('Case ID Field');
  }

  /**
   * @return string
   */
  protected function getIdFieldName() {
    return 'case_id_field';
  }

  /**
   * Extract the event ids from the event task forms.
   * We need this as soon as the task is a send letter or send e-mail from the action
   * after a search for participants.
   *
   * We can then use those event ids during the processing of the tokens.
   *
   * @param $form
   */
  public static function extractCaseIdsFromManageCaseForms(\CRM_Core_Form $form) {
    if ($form instanceof \CRM_Contact_Form_Task_Email) {
      self::$_caseIds = $form->getVar('_caseId');
    } elseif ($form instanceof \CRM_Contact_Form_Task_PDF) {
      self::$_caseIds = $form->getVar('_caseId');
    }
  }


}
