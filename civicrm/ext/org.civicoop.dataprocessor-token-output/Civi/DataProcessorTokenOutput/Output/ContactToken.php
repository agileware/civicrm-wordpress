<?php
/**
 * @author Jaap Jansma <jaap.jansma@civicoop.org>
 * @license AGPL-3.0
 */

namespace Civi\DataProcessorTokenOutput\Output;

use Civi\DataProcessor\DataFlow\EndOfFlowException;
use Civi\DataProcessor\DataFlow\Sort\SortCompareFactory;
use Civi\DataProcessor\DataFlow\SqlDataFlow;
use Civi\DataProcessor\Exception\DataSourceNotFoundException;
use Civi\DataProcessor\Exception\FieldNotFoundException;
use Civi\DataProcessor\Output\OutputInterface;

use Civi\DataProcessor\ProcessorType\AbstractProcessorType;
use CRM_DataprocessorTokenOutput_ExtensionUtil as E;

class ContactToken extends AbstractToken {

  /**
   * Returns the title of the ID field.
   *
   * @return String
   */
  protected function getIdFieldTitle() {
    return E::ts('Contact ID Field');
  }

  /**
   * @return string
   */
  protected function getIdFieldName() {
    return 'contact_id_field';
  }

  /**
   * @return string
   */
  protected function getTokenProcessorIdFieldName() {
    return 'contactId';
  }

  /**
   * Function to set a different contact ID to be used in the data processor.
   * This function is here so child classes can override it.
   *
   * @param int $contactId
   * @param array $values
   * @param array $configuration
   *
   * @return int|null
   */
  protected function getIdForToken($contactId, $values, $configuration) {
    return $contactId;
  }


}
