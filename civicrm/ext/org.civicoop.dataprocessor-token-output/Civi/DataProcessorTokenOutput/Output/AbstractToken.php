<?php
/**
 * @author Jaap Jansma <jaap.jansma@civicoop.org>
 * @license AGPL-3.0
 */

namespace Civi\DataProcessorTokenOutput\Output;

use Civi\DataProcessor\DataFlow\CombinedDataFlow\CombinedSqlDataFlow;
use Civi\DataProcessor\DataFlow\EndOfFlowException;
use Civi\DataProcessor\DataFlow\InvalidFlowException;
use Civi\DataProcessor\DataFlow\Sort\SortCompareFactory;
use Civi\DataProcessor\DataFlow\SqlDataFlow;
use Civi\DataProcessor\DataFlow\SqlTableDataFlow;
use Civi\DataProcessor\Exception\DataFlowException;
use Civi\DataProcessor\Exception\DataSourceNotFoundException;
use Civi\DataProcessor\Exception\FieldNotFoundException;
use Civi\DataProcessor\Output\OutputInterface;

use Civi\DataProcessor\ProcessorType\AbstractProcessorType;
use CRM_DataprocessorTokenOutput_ExtensionUtil as E;

abstract class AbstractToken implements OutputInterface, TokenOutput {

  /**
   * Returns the title of the ID field.
   *
   * @return String
   */
  abstract protected function getIdFieldTitle();

  /**
   * @return string
   */
  abstract protected function getTokenProcessorIdFieldName();

  /**
   * Checks whether the output is available for this evaluateTokenEvent.
   *
   * @param \Civi\Token\Event\TokenValueEvent $evaluateTokenEvent
   *
   * @return bool
   */
  public function availableForEvaluateToken($evaluateTokenEvent) {
    $ids = $evaluateTokenEvent->getTokenProcessor()->getContextValues($this->getTokenProcessorIdFieldName());
    if (count($ids)) {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Function to set a different contact ID to be used in the data processor.
   * This function is here so child classes can override it.
   *
   * @param \Civi\Token\TokenRow $tokenRow
   * @param $configuration
   *
   * @return int|null
   */
  protected function getIdForTokenFromTokenRow($tokenRow, $configuration) {
    if (isset($tokenRow->context[$this->getTokenProcessorIdFieldName()])) {
      return $tokenRow->context[$this->getTokenProcessorIdFieldName()];
    }
    return null;
  }

  /**
   * Return all available fields for the tokens.
   *
   * @param \Civi\DataProcessor\ProcessorType\AbstractProcessorType $dataProcessorClass
   * @param $configuration
   *
   * @return array|void
   */
  protected function getAvailableFields(AbstractProcessorType $dataProcessorClass, $configuration) {
    $hiddenFields = array();
    if (isset($configuration['hidden_fields']) && is_array($configuration['hidden_fields'])) {
      $hiddenFields = $configuration['hidden_fields'];
    }

    try {
      $allFields = $dataProcessorClass->getDataFlow()->getOutputFieldHandlers();
    } catch (\Exception $e) {
      return; // No fields available.
    }

    $availableFields = [];
    foreach ($allFields as $outputFieldHandler) {
      $field = $outputFieldHandler->getOutputFieldSpecification();
      if (!in_array($field->alias, $hiddenFields)) {
        $availableFields[] = $field;
      }
    }
    return $availableFields;
  }

  /**
   * Evaluate tokens
   *
   * @param \Civi\Token\Event\TokenValueEvent $evaluateTokenEvent
   * @param \Civi\DataProcessor\ProcessorType\AbstractProcessorType $dataProcessorClass
   * @param String $name
   * @param array $configuration
   */
  public function evaluateTokens($evaluateTokenEvent, AbstractProcessorType $dataProcessorClass, $name, $configuration) {
    $availableFields = $this->getAvailableFields($dataProcessorClass, $configuration);
    foreach($evaluateTokenEvent->getRows() as $row) {
      $idToUse = $this->getIdForTokenFromTokenRow($row, $configuration);
      if (!$idToUse) {
        return;
      }
      try {
        $this->initializeDataProcessorClass($idToUse, $dataProcessorClass, $configuration);
      } catch (InvalidFlowException|DataSourceNotFoundException|FieldNotFoundException $e) {
        return;
      }
      $record = false;
      $allDataSetting = isset($configuration['all_data_setting']) ? $configuration['all_data_setting'] : false;
      if ($allDataSetting) {
        try {
          $allRecords = $dataProcessorClass->getDataFlow()->allRecords();
          if ($allRecords) {
            $this->processAllDataForTokenProcessor($allDataSetting, $allRecords, $row, $availableFields, $name, $configuration);
            $record = reset($allRecords);
          }
        } catch (EndOfFlowException|InvalidFlowException|DataFlowException $e) {
        }
      } else {
        try {
          $record = $dataProcessorClass->getDataFlow()->nextRecord();
        } catch (EndOfFlowException|InvalidFlowException|DataFlowException $e) {
        }
      }
      if ($record) {
        try {
          $this->processForTokenProcessor($record, $row, $availableFields, $name, $configuration);
        } catch (EndOfFlowException|InvalidFlowException|DataFlowException $e) {
        }
      }
    }
  }


  /**
   * @return string
   */
  protected function getIdFieldName() {
    return 'id_field';
  }

  protected function getHelpText() {
    return '';
  }

  protected function allDataOptions() {
    /*
     * @ToDo Implement hook
     * Make this hookable so that other extension can provide additional layouts for
     * all data tokens, such as lists etc..
     */
    return [
      'table' => [
        'title' => E::ts('Render table with all data'),
        'callback' => ['Civi\DataProcessorTokenOutput\AllData\Table', 'render'],
        'token_name' => 'table_all_data',
        'token_title' => 'Table with all data'
      ],
      'list' => [
        'title' => E::ts('Render list with all data'),
        'callback' => ['Civi\DataProcessorTokenOutput\AllData\UnorderedList', 'render'],
        'token_name' => 'list_all_data',
        'token_title' => 'List with all data'
      ],
    ];
  }

  /**
   * Returns true when this output has additional configuration
   *
   * @return bool
   */
  public function hasConfiguration() {
    return true;
  }

  /**
   * When this output type has additional configuration you can add
   * the fields on the form with this function.
   *
   * @param \CRM_Core_Form $form
   * @param array $output
   */
  public function buildConfigurationForm(\CRM_Core_Form $form, $output = []) {
    $allDataOptions = [];
    foreach($this->allDataOptions() as $allDataOption => $allDataOptionSettings) {
      $allDataOptions[$allDataOption] = $allDataOptionSettings['title'];
    }

    $fieldSelect = \CRM_Dataprocessor_Utils_DataSourceFields::getAvailableFilterFieldsInDataSources($output['data_processor_id']);
    $dataProcessor = civicrm_api3('DataProcessor', 'getsingle', array('id' => $output['data_processor_id']));
    $dataProcessorClass = \CRM_Dataprocessor_BAO_DataProcessor::dataProcessorToClass($dataProcessor);
    $fields = array();
    $sortFields = array();
    foreach($dataProcessorClass->getDataFlow()->getOutputFieldHandlers() as $outputFieldHandler) {
      $field = $outputFieldHandler->getOutputFieldSpecification();
      $fields[$field->alias] = $field->title;
      $sortFields['asc_'.$field->alias] = E::ts('Ascending sort %1', array(1=>$field->title));
      $sortFields['desc_'.$field->alias] = E::ts('Descending sort %1', array(1=>$field->title));
    }

    $form->assign('id_field_name', $this->getIdFieldName());
    $form->add('select', $this->getIdFieldName(), $this->getIdFieldTitle(), $fieldSelect, true, array(
      'style' => 'min-width:250px',
      'class' => 'crm-select2 huge',
      'placeholder' => E::ts('- select -'),
    ));

    $form->add('select', 'hidden_fields', E::ts('Hidden fields'), $fields, false, array(
      'style' => 'min-width:250px',
      'class' => 'crm-select2 huge',
      'multiple' => true,
      'placeholder' => E::ts('- select -'),
    ));

    $form->add('select', 'sortfield', E::ts('Sort'), $sortFields, false, array(
      'style' => 'min-width:250px',
      'class' => 'crm-select2 huge',
      'multiple' => false,
      'placeholder' => E::ts('- select -'),
    ));
    $form->add('select', 'all_data_setting', E::ts('Process all records'), $allDataOptions, false, array(
      'style' => 'min-width:250px',
      'class' => 'crm-select2 huge',
      'multiple' => false,
      'placeholder' => E::ts('- select -'),
    ));
    $form->assign('help_text', $this->getHelpText());

    $form->add('textarea', 'pre_text', E::ts('Pre text'), array('class' => 'huge'), false);
    $form->add('textarea', 'post_text', E::ts('Post text'), array('class' => 'huge'), false);

    $defaults = array();
    if ($output) {
      if (isset($output['configuration']) && is_array($output['configuration'])) {
        if (isset($output['configuration']['help_text'])) {
          $defaults['help_text'] = $output['configuration']['help_text'];
        }
        if (isset($output['configuration'][$this->getIdFieldName()])) {
          $defaults[$this->getIdFieldName()] = $output['configuration'][$this->getIdFieldName()];
        }
        if (isset($output['configuration']['hidden_fields'])) {
          $defaults['hidden_fields'] = $output['configuration']['hidden_fields'];
        }
        if (isset($output['configuration']['sort'])) {
          $defaults['sortfield'] = $output['configuration']['sort'];
        }
        if (isset($output['configuration']['all_data_setting'])) {
          $defaults['all_data_setting'] = $output['configuration']['all_data_setting'];
        }
        if (isset($output['configuration']['pre_text'])) {
          $defaults['pre_text'] = $output['configuration']['pre_text'];
        }
        if (isset($output['configuration']['post_text'])) {
          $defaults['post_text'] = $output['configuration']['post_text'];
        }
      }
    }
    if (!isset($defaults['default_limit'])) {
      $sortFieldNames = array_keys($sortFields);
      $defaults['sort'] = reset($sortFieldNames); // Get the first field.
    }
    $form->setDefaults($defaults);
  }

  /**
   * When this output type has configuration specify the template file name
   * for the configuration form.
   *
   * @return false|string
   */
  public function getConfigurationTemplateFileName() {
    return "CRM/DataprocessorTokenOutput/Form/TokenConfiguration.tpl";
  }

  /**
   * Process the submitted values and create a configuration array
   *
   * @param $submittedValues
   * @param array $output
   *
   * @return array $output
   */
  public function processConfiguration($submittedValues, &$output) {
    $configuration[$this->getIdFieldName()] = $submittedValues[$this->getIdFieldName()];
    $configuration['hidden_fields'] = $submittedValues['hidden_fields'];
    $configuration['sort'] = $submittedValues['sortfield'];
    $configuration['all_data_setting'] = $submittedValues['all_data_setting'];
    $configuration['pre_text'] = $submittedValues['pre_text'];
    $configuration['post_text'] = $submittedValues['post_text'];
    return $configuration;
  }

  /**
   * This function is called prior to removing an output
   *
   * @param array $output
   *
   * @return void
   */
  public function deleteOutput($output) {
    // Do nothing.
  }


  /**
   * Sets the tokens for this output.
   *
   * @param $tokens
   * @param \Civi\DataProcessor\ProcessorType\AbstractProcessorType $dataProcessorClass
   * @param String $title
   * @param String $name
   * @param array $configuration
   */
  public function tokens(&$tokens, AbstractProcessorType $dataProcessorClass, $title, $name, $configuration) {
    // Retrieve the tokens for this output.
    $hiddenFields = array();
    if (isset($configuration['hidden_fields']) && is_array($configuration['hidden_fields'])) {
      $hiddenFields = $configuration['hidden_fields'];
    }

    try {
      $availableFields = $dataProcessorClass->getDataFlow()->getOutputFieldHandlers();
    } catch (\Exception $e) {
      return; // No fields available.
    }

    foreach($availableFields as $outputFieldHandler) {
      $field = $outputFieldHandler->getOutputFieldSpecification();
      if (!in_array($field->alias, $hiddenFields)) {
        $tokens[$name][$name.'.'.$field->alias] = $field->title.' :: '.$title;
      }
    }

    $allDataOptions = $this->allDataOptions();
    $allData = isset($configuration['all_data_setting']) ? $configuration['all_data_setting'] : false;
    if (isset($allDataOptions[$allData])) {
      $tokens[$name][$name.'.'.$allDataOptions[$allData]['token_name']] = $allDataOptions[$allData]['token_title'].' :: '.$title;
    }
  }

  /**
   * Sets the tokens for this output.
   *
   * @param int $contact_id
   * @param $values
   * @param \Civi\DataProcessor\ProcessorType\AbstractProcessorType $dataProcessorClass
   * @param String $name
   * @param array $configuration
   */
  public function tokenValues($contact_id, &$values, AbstractProcessorType $dataProcessorClass, $name, $configuration) {
    $availableFields = $this->getAvailableFields($dataProcessorClass, $configuration);

    $idToUse = $this->getIdForToken($contact_id, $values, $configuration);
    if (!$idToUse) {
      return;
    }
    try {
      $this->initializeDataProcessorClass($idToUse, $dataProcessorClass, $configuration);
    } catch (InvalidFlowException|FieldNotFoundException|DataSourceNotFoundException $e) {
      return;
    }
    $record = false;
    $allDataSetting = isset($configuration['all_data_setting']) ? $configuration['all_data_setting'] : false;
    if ($allDataSetting) {
      try {
        $allRecords = $dataProcessorClass->getDataFlow()->allRecords();
        if ($allRecords) {
          $this->processAllData($allDataSetting, $allRecords, $values, $availableFields, $name, $configuration);
          $record = reset($allRecords);
        }
      } catch (EndOfFlowException|InvalidFlowException|DataFlowException $e) {
      }
    } else {
      try {
        $record = $dataProcessorClass->getDataFlow()->nextRecord();
      } catch (EndOfFlowException|InvalidFlowException|DataFlowException $e) {
      }
    }
    if ($record) {
      try {
        $this->process($record, $values, $availableFields, $name, $configuration);
      } catch (EndOfFlowException|InvalidFlowException|DataFlowException $e) {
      }
    }
  }

  /**
   * Convert the data from the data processor to tokens.
   *
   * @param \Civi\DataProcessor\ProcessorType\AbstractProcessorType $dataProcessorClass
   * @param array $values
   * @param \Civi\DataProcessor\DataSpecification\FieldSpecification[] $availableFields
   * @param string $name
   * @param array $configuration
   *
   * @throws \Civi\DataProcessor\DataFlow\InvalidFlowException
   */
  protected function process($record, &$values, $availableFields, $name, $configuration) {
    $pre_text = isset($configuration['pre_text']) ? html_entity_decode($configuration['pre_text']) : '';
    $post_text = isset($configuration['post_text']) ? html_entity_decode($configuration['post_text']) : '';
    foreach($availableFields as $field) {
      $values[$name.'.'.$field->alias] = $pre_text.$record[$field->alias]->formattedValue.$post_text;
    }
  }

  /**
   * Convert the data from the data processor to tokens.
   *
   * @param \Civi\DataProcessor\ProcessorType\AbstractProcessorType $dataProcessorClass
   * @param \Civi\Token\TokenRow $tokenRow
   * @param \Civi\DataProcessor\DataSpecification\FieldSpecification[] $availableFields
   * @param string $name
   * @param array $configuration
   *
   * @throws \Civi\DataProcessor\DataFlow\InvalidFlowException
   */
  protected function processForTokenProcessor($record, &$tokenRow, $availableFields, $name, $configuration) {
    $pre_text = isset($configuration['pre_text']) ? html_entity_decode($configuration['pre_text']) : '';
    $post_text = isset($configuration['post_text']) ? html_entity_decode($configuration['post_text']) : '';
    foreach($availableFields as $field) {
      $tokenRow->format('text/html')->tokens($name, $field->alias, $pre_text.$record[$field->alias]->formattedValue . $post_text);
    }
  }

  /**
   * Convert the data from the data processor to tokens.
   *
   * @param string $allDataSetting
   * @param array $records
   * @param array $values
   * @param \Civi\DataProcessor\DataSpecification\FieldSpecification[] $availableFields
   * @param string $name
   * @param array $configuration
   *
   * @throws \Civi\DataProcessor\DataFlow\InvalidFlowException
   */
  protected function processAllData($allDataSetting, $records, $values, $availableFields, $name, $configuration) {
    $pre_text = isset($configuration['pre_text']) ? html_entity_decode($configuration['pre_text']) : '';
    $post_text = isset($configuration['post_text']) ? html_entity_decode($configuration['post_text']) : '';
    $allDataOptions = $this->allDataOptions();
    if (isset($allDataOptions[$allDataSetting]) && isset($allDataOptions[$allDataSetting]['callback'])) {
      $values[$name.'.'.$allDataOptions[$allDataSetting]['token_name']] = $pre_text.call_user_func($allDataOptions[$allDataSetting]['callback'], $records, $availableFields).$post_text;
    }
  }

  /**
   * Convert the data from the data processor to tokens.
   *
   * @param string $allDataSetting
   * @param array $records
   * @param \Civi\Token\TokenRow $tokenRow
   * @param \Civi\DataProcessor\DataSpecification\FieldSpecification[] $availableFields
   * @param string $name
   * @param array $configuration
   *
   * @throws \Civi\DataProcessor\DataFlow\InvalidFlowException
   */
  protected function processAllDataForTokenProcessor($allDataSetting, $records, &$tokenRow, $availableFields, $name, $configuration) {
    $pre_text = isset($configuration['pre_text']) ? html_entity_decode($configuration['pre_text']) : '';
    $post_text = isset($configuration['post_text']) ? html_entity_decode($configuration['post_text']) : '';
    $allDataOptions = $this->allDataOptions();
    if (isset($allDataOptions[$allDataSetting]) && isset($allDataOptions[$allDataSetting]['callback'])) {
      $tokenRow->format('text/html')->tokens($name, $allDataOptions[$allDataSetting]['token_name'], $pre_text.call_user_func($allDataOptions[$allDataSetting]['callback'], $records, $availableFields).$post_text);
    }
  }

  /**
   * Initialize the data processor.
   *
   * @param int|array $idsToUse
   * @param \Civi\DataProcessor\ProcessorType\AbstractProcessorType $dataProcessorClass
   * @param array $configuration
   *
   * @throws \Civi\DataProcessor\DataFlow\InvalidFlowException
   * @throws \Civi\DataProcessor\Exception\DataSourceNotFoundException
   * @throws \Civi\DataProcessor\Exception\FieldNotFoundException
   */
  protected function initializeDataProcessorClass($idsToUse, AbstractProcessorType $dataProcessorClass, $configuration) {
    [$datasource_name, $field_name] = explode('::', $configuration[$this->getIdFieldName()], 2);
    $dataSource = $dataProcessorClass->getDataSourceByName($datasource_name);
    if (!$dataSource) {
      throw new DataSourceNotFoundException(E::ts("Requires data source '%1' which could not be found. Did you rename or deleted the data source?", [1 => $datasource_name]));
    }
    $fieldSpecification = $dataSource->getAvailableFilterFields()
      ->getFieldSpecificationByAlias($field_name);
    if (!$fieldSpecification) {
      throw new FieldNotFoundException(E::ts("Requires a field with the name '%1' in the data source '%2'. Did you change the data source type?", [
        1 => $field_name,
        2 => $datasource_name
      ]));
    }

    $dataFlow = $dataSource->ensureField($fieldSpecification);
    if ($dataFlow && $dataFlow instanceof SqlDataFlow) {
      $operator = '=';
      if (is_array($idsToUse)) {
        $operator = 'IN';
      }
      $tableAlias = $this->getTableAlias($dataFlow);
      $whereClause = new SqlDataFlow\SimpleWhereClause($tableAlias, $fieldSpecification->name, $operator, $idsToUse, $fieldSpecification->type, FALSE);
      $dataFlow->addWhereClause($whereClause);
    }

    $sortField = substr($configuration['sort'], 5);
    $direction = 'desc';
    if (stripos($configuration['sort'], 'asc_') === 0) {
      $sortField = substr($configuration['sort'], 4);
      $direction = 'asc';
    }
    $dataProcessorClass->getDataFlow()->addSort($sortField, $direction);
    $dataProcessorClass->getDataFlow()->setLimit(false);
    $dataProcessorClass->getDataFlow()->setOffset(0);
  }

  /**
   * Returns the table alias of a sql data flow.
   *
   * @param \Civi\DataProcessor\DataFlow\SqlDataFlow $dataFlow
   * @return string|null
   */
  protected function getTableAlias(SqlDataFlow $dataFlow) {
    $tableAlias = $dataFlow->getName();
    if ($dataFlow instanceof SqlTableDataFlow) {
      $tableAlias = $dataFlow->getTableAlias();
    } elseif ($dataFlow instanceof CombinedSqlDataFlow) {
      $tableAlias = $dataFlow->getPrimaryTableAlias();
    }
    return $tableAlias;
  }


}
