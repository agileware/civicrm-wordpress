<?php
/**
 * @author Jaap Jansma <jaap.jansma@civicoop.org>
 * @license AGPL-3.0
 */

namespace Civi\DataProcessorTokenOutput\Output;

use Civi\DataProcessor\DataFlow\EndOfFlowException;
use Civi\DataProcessor\DataFlow\Sort\SortCompareFactory;
use Civi\DataProcessor\DataFlow\SqlDataFlow;
use Civi\DataProcessor\Exception\DataSourceNotFoundException;
use Civi\DataProcessor\Exception\FieldNotFoundException;
use Civi\DataProcessor\Output\OutputInterface;

use Civi\DataProcessor\ProcessorType\AbstractProcessorType;
use CRM_DataprocessorTokenOutput_ExtensionUtil as E;

interface TokenOutput {

  /**
   * Sets the tokens for this output.
   *
   * @param $tokens
   * @param \Civi\DataProcessor\ProcessorType\AbstractProcessorType $dataProcessorClass
   * @param String $title
   * @param String $name
   * @param array $configuration
   */
  public function tokens(&$tokens, AbstractProcessorType $dataProcessorClass, $title, $name, $configuration);

  /**
   * Sets the tokens for this output.
   *
   * @param int $contact_id
   * @param $values
   * @param \Civi\DataProcessor\ProcessorType\AbstractProcessorType $dataProcessorClass
   * @param String $name
   * @param array $configuration
   */
  public function tokenValues($contact_id, &$values, AbstractProcessorType $dataProcessorClass, $name, $configuration);

  /**
   * Evaluate tokens
   *
   * @param \Civi\Token\Event\TokenValueEvent $evaluateTokenEvent
   * @param \Civi\DataProcessor\ProcessorType\AbstractProcessorType $dataProcessorClass
   * @param String $name
   * @param array $configuration
   */
  public function evaluateTokens($evaluateTokenEvent, AbstractProcessorType $dataProcessorClass, $name, $configuration);

  /**
   * Checks whether the output is available for this evaluateTokenEvent.
   *
   * @param \Civi\Token\Event\TokenValueEvent $evaluateTokenEvent
   * @return bool
   */
  public function availableForEvaluateToken($evaluateTokenEvent);

}
