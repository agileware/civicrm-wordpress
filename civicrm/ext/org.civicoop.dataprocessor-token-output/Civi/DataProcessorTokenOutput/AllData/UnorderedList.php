<?php
/**
 * @author Jaap Jansma <jaap.jansma@civicoop.org>
 * @license AGPL-3.0
 */

namespace Civi\DataProcessorTokenOutput\AllData;

use Civi\DataProcessor\FieldOutputHandler\HTMLFieldOutput;

class UnorderedList {

  public static function render($records, $availableFields) {
    $output = '<ul>';
    foreach($records as $record) {
      $data = array();
      foreach($availableFields as $field) {
        if ($record[$field->alias] instanceof HTMLFieldOutput) {
          $data[] = $record[$field->alias]->getMarkupOut();
        } else {
          $data[] = $record[$field->alias]->formattedValue;
        }
      }
      $output .= '<li>'.implode(", ", $data).'</li>';
    }
    $output .= '</ul>';
    return $output;
  }

}
