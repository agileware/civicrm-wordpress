{crmScope extensionKey='dataprocessor-token-output'}
  {if ($help_text)}
  <div class="help">
    {$help_text}
  </div>
  {/if}
  <div class="crm-section">
    <div class="label">{$form.$id_field_name.label}</div>
    <div class="content">{$form.$id_field_name.html}</div>
    <div class="clear"></div>
  </div>
  <div class="crm-section">
    <div class="label">{$form.hidden_fields.label}</div>
    <div class="content">{$form.hidden_fields.html}</div>
    <div class="clear"></div>
  </div>
  <div class="crm-section">
    <div class="label">{$form.sortfield.label}</div>
    <div class="content">{$form.sortfield.html}</div>
    <div class="clear"></div>
  </div>
  <div class="crm-section">
    <div class="label">{$form.all_data_setting.label}</div>
    <div class="content">{$form.all_data_setting.html}</div>
    <div class="clear"></div>
  </div>
  <div class="crm-section">
    <div class="label">{$form.pre_text.label}</div>
    <div class="content">{$form.pre_text.html}
      <p class="description">{ts}The pre text is shown just before the rendered value of the token.{/ts}</p>
    </div>
    <div class="clear"></div>
  </div>
  <div class="crm-section">
    <div class="label">{$form.post_text.label}</div>
    <div class="content">{$form.post_text.html}
      <p class="description">{ts}The post text is shown just after the rendered value of the token.{/ts}</p>
    </div>
    <div class="clear"></div>
  </div>

{/crmScope}
