{crmScope extensionKey='dataprocessor-token-output'}
  {include file="CRM/DataprocessorTokenOutput/Form/TokenConfiguration.tpl"}
  <div class="crm-section">
    <div class="label">{$form.relationship_types.label}</div>
    <div class="content">{$form.relationship_types.html}</div>
    <div class="clear"></div>
  </div>

{/crmScope}
