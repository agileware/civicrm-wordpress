<?php

require_once 'dataprocessor_token_output.civix.php';
use CRM_DataprocessorTokenOutput_ExtensionUtil as E;
use Symfony\Component\DependencyInjection\ContainerBuilder;

/**
 * Implements hook_civicrm_container()
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_container/
 */
function dataprocessor_token_output_civicrm_container(ContainerBuilder $container) {
  $container->addCompilerPass(new Civi\DataProcessorTokenOutput\CompilerPass());
  $container->findDefinition('dispatcher')->addMethodCall('addListener',
    array('civi.token.eval', 'dataprocessor_token_output_evaluate_tokens')
  );
}

/**
 * @param \Civi\Token\Event\TokenValueEvent $event
 *
 * @return void
 */
function dataprocessor_token_output_evaluate_tokens($event) {
  $factory = dataprocessor_get_factory();
  // Check whether the factory exists. Usually just after
  // installation the factory does not exists but then no
  // outputs exists either. So we can safely return this function.
  if (!$factory) {
    return;
  }

  $dao = _dataprocessor_token_output_get_dao();
  while($dao->fetch()) {
    try {
      $outputClass = $factory->getOutputByName($dao->type);
      if (!$outputClass instanceof Civi\DataprocessorTokenOutput\Output\TokenOutput) {
        continue;
      }
      if (!$outputClass->availableForEvaluateToken($event)) {
        continue;
      }

      // Find data processors for this output.
      $dataprocessor = civicrm_api3('DataProcessor', 'getsingle', ['id' => $dao->data_processor_id]);
      $configuration = json_decode($dao->configuration, TRUE);
      $dataProcessorClass = CRM_Dataprocessor_BAO_DataProcessor::dataProcessorToClass($dataprocessor);

      $outputClass->evaluateTokens($event, $dataProcessorClass, $dataprocessor['name'], $configuration);
    } catch (\Exception $e) {
      // Do nothing.
    }
  }
}

/**
 * Implement hook_civicrm_tokens()
 *
 * @param $tokens
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_tokens/
 */
function dataprocessor_token_output_civicrm_tokens(&$tokens) {
  $factory = dataprocessor_get_factory();
  // Check whether the factory exists. Usually just after
  // installation the factory does not exists but then no
  // outputs exists either. So we can safely return this function.
  if (!$factory) {
    return;
  }

  $dao = _dataprocessor_token_output_get_dao();
  while($dao->fetch()) {
    try {
      $outputClass = $factory->getOutputByName($dao->type);
      if (!$outputClass instanceof Civi\DataprocessorTokenOutput\Output\TokenOutput) {
        continue;
      }
      $dataprocessor = civicrm_api3('DataProcessor', 'getsingle', ['id' => $dao->data_processor_id]);
      $configuration = json_decode($dao->configuration, TRUE);
      $dataProcessorClass = CRM_Dataprocessor_BAO_DataProcessor::dataProcessorToClass($dataprocessor);

      $outputClass->tokens($tokens, $dataProcessorClass, $dataprocessor['title'], $dataprocessor['name'], $configuration);
    } catch (\Exception $e) {
      // Do nothing.
    }
  }
}

/**
 * Implementation of hook_civicrm_tokenValues()
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_tokenValues/
 */
function dataprocessor_token_output_civicrm_tokenValues(&$values, $cids, $job = null, $tokens = [], $context = null) {
  $factory = dataprocessor_get_factory();
  // Check whether the factory exists. Usually just after
  // installation the factory does not exists but then no
  // outputs exists either. So we can safely return this function.
  if (!$factory) {
    return;
  }

  $dao = _dataprocessor_token_output_get_dao($tokens);
  while($dao->fetch()) {
    $outputClass = $factory->getOutputByName($dao->type);
    if (!$outputClass instanceof Civi\DataprocessorTokenOutput\Output\TokenOutput) {
      continue;
    }
    $dataprocessor = civicrm_api3('DataProcessor', 'getsingle', ['id' => $dao->data_processor_id]);
    $configuration = json_decode($dao->configuration, true);
    foreach($cids as $cid) {
      $dataProcessorClass = CRM_Dataprocessor_BAO_DataProcessor::dataProcessorToClass($dataprocessor);
      $value =& $values[$cid];
      $outputClass->tokenValues($cid, $value, $dataProcessorClass, $dataprocessor['name'], $configuration);
    }
  }
}

/**
 * Returns the DAO object for the query to retrieve active data processor with a token output.
 *
 * @param null|array $names
 * @return \CRM_Core_DAO|object
 */
function _dataprocessor_token_output_get_dao($names=null) {
  $tokenFactory = dataprocessor_token_output_get_token_factory();
  $typeClauses = [];
  foreach($tokenFactory->getTokenOutputTypes() as $type) {
    $typeClauses[] = "`o`.`type` = '".\CRM_Utils_Type::escape($type, 'String')."'";
  }


  $sql = "SELECT o.*, d.name as data_processor_name, o.configuration
            FROM civicrm_data_processor d
            INNER JOIN civicrm_data_processor_output o ON d.id = o.data_processor_id
            WHERE d.is_active = 1 AND (".implode(" OR ", $typeClauses).")";
  if (is_array($names) && count($names)) {
    $nameClauses = array();
    foreach($names as $name => $tokens) {
      $nameClauses[] = "d.name = '" . CRM_Utils_Type::escape($name, 'String') . "'";
    }
    $sql .= " AND (".implode(" OR ", $nameClauses).")";
  }
  $dao = CRM_Core_DAO::executeQuery($sql);
  return $dao;
}

/**
 * @return Civi\DataProcessorTokenOutput\TokenFactory
 */
function dataprocessor_token_output_get_token_factory() {
  $container = \Civi::container();
  if ($container->has('data_processor_token_factory')) {
    return \Civi::service('data_processor_token_factory');
  }
  return null;
}

/**
 * Implements hook_civicrm_buildForm().
 */
function dataprocessor_token_output_civicrm_buildForm($formName, &$form) {
  \Civi\DataProcessorTokenOutput\Output\EventToken::extractParticipantIdsFromEventFormTasks($form);
  \Civi\DataProcessorTokenOutput\Output\ParticipantToken::extractParticipantIdsFromEventFormTasks($form);
  \Civi\DataProcessorTokenOutput\Output\CaseToken::extractCaseIdsFromManageCaseForms($form);
}


/**
 * Implementation of hook_civicrm_pageRun()
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_pageRun/
 */
function dataprocessor_token_output_civicrm_pageRun(&$page) {
  if ($page instanceof CRM_Admin_Page_Extensions) {
    $unmet = CRM_DataprocessorTokenOutput_Upgrader::checkExtensionDependencies();
    CRM_DataprocessorTokenOutput_Upgrader::displayDependencyErrors($unmet);
  }
}

/**
 * Implements hook_civicrm_config().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_config/
 */
function dataprocessor_token_output_civicrm_config(&$config) {
  _dataprocessor_token_output_civix_civicrm_config($config);
}

/**
 * Implements hook_civicrm_xmlMenu().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_xmlMenu
 */
function dataprocessor_token_output_civicrm_xmlMenu(&$files) {
  _dataprocessor_token_output_civix_civicrm_xmlMenu($files);
}

/**
 * Implements hook_civicrm_install().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_install
 */
function dataprocessor_token_output_civicrm_install() {
  _dataprocessor_token_output_civix_civicrm_install();
}

/**
 * Implements hook_civicrm_postInstall().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_postInstall
 */
function dataprocessor_token_output_civicrm_postInstall() {
  _dataprocessor_token_output_civix_civicrm_postInstall();
}

/**
 * Implements hook_civicrm_uninstall().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_uninstall
 */
function dataprocessor_token_output_civicrm_uninstall() {
  _dataprocessor_token_output_civix_civicrm_uninstall();
}

/**
 * Implements hook_civicrm_enable().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_enable
 */
function dataprocessor_token_output_civicrm_enable() {
  _dataprocessor_token_output_civix_civicrm_enable();
}

/**
 * Implements hook_civicrm_disable().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_disable
 */
function dataprocessor_token_output_civicrm_disable() {
  _dataprocessor_token_output_civix_civicrm_disable();
}

/**
 * Implements hook_civicrm_upgrade().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_upgrade
 */
function dataprocessor_token_output_civicrm_upgrade($op, CRM_Queue_Queue $queue = NULL) {
  return _dataprocessor_token_output_civix_civicrm_upgrade($op, $queue);
}

/**
 * Implements hook_civicrm_managed().
 *
 * Generate a list of entities to create/deactivate/delete when this module
 * is installed, disabled, uninstalled.
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_managed
 */
function dataprocessor_token_output_civicrm_managed(&$entities) {
  _dataprocessor_token_output_civix_civicrm_managed($entities);
}

/**
 * Implements hook_civicrm_caseTypes().
 *
 * Generate a list of case-types.
 *
 * Note: This hook only runs in CiviCRM 4.4+.
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_caseTypes
 */
function dataprocessor_token_output_civicrm_caseTypes(&$caseTypes) {
  _dataprocessor_token_output_civix_civicrm_caseTypes($caseTypes);
}

/**
 * Implements hook_civicrm_angularModules().
 *
 * Generate a list of Angular modules.
 *
 * Note: This hook only runs in CiviCRM 4.5+. It may
 * use features only available in v4.6+.
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_angularModules
 */
function dataprocessor_token_output_civicrm_angularModules(&$angularModules) {
  _dataprocessor_token_output_civix_civicrm_angularModules($angularModules);
}

/**
 * Implements hook_civicrm_alterSettingsFolders().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_alterSettingsFolders
 */
function dataprocessor_token_output_civicrm_alterSettingsFolders(&$metaDataFolders = NULL) {
  _dataprocessor_token_output_civix_civicrm_alterSettingsFolders($metaDataFolders);
}

/**
 * Implements hook_civicrm_entityTypes().
 *
 * Declare entity types provided by this module.
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_entityTypes
 */
function dataprocessor_token_output_civicrm_entityTypes(&$entityTypes) {
  _dataprocessor_token_output_civix_civicrm_entityTypes($entityTypes);
}

/**
 * Implements hook_civicrm_thems().
 */
function dataprocessor_token_output_civicrm_themes(&$themes) {
  _dataprocessor_token_output_civix_civicrm_themes($themes);
}

// --- Functions below this ship commented out. Uncomment as required. ---

/**
 * Implements hook_civicrm_preProcess().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_preProcess
 *
function dataprocessor_token_output_civicrm_preProcess($formName, &$form) {

} // */

/**
 * Implements hook_civicrm_navigationMenu().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_navigationMenu
 *
function dataprocessor_token_output_civicrm_navigationMenu(&$menu) {
  _dataprocessor_token_output_civix_insert_navigation_menu($menu, 'Mailings', array(
    'label' => E::ts('New subliminal message'),
    'name' => 'mailing_subliminal_message',
    'url' => 'civicrm/mailing/subliminal',
    'permission' => 'access CiviMail',
    'operator' => 'OR',
    'separator' => 0,
  ));
  _dataprocessor_token_output_civix_navigationMenu($menu);
} // */
