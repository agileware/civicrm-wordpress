<?php

namespace Civi\Api4\Action\DiscountCode;

use Civi\Api4\Generic\Result;

/**
 * Verify action can be used to check, whether a discount code is existing and valid.
 *
 * Validation covers the following scenarios:
 *
 * - discount code is active
 *
 * - maximum usage count
 *
 * - within allowed date range
 *
 * - eligible for the given event (entity type "event" and entityId of event must be submitted within the api call)
 *
 * If verification fails for one of the above reasons, it is stated in the reply (e.g. for displaying it on forms)
 *
 * If Verification is successful discountetAmount is given back, calculated on basis of the settings of the individual discount code (percentage or absolute discount).
 *
 */
class Verify extends \Civi\Api4\Generic\AbstractAction {

    /**
     * @required
     * @var string
     */
    protected $discountCode;
    /**
     * @var string
     */
    protected $entityType;
    /**
     * @var int
     */
    protected $entityId;
    /**
     * @required
     * @var string
     */
    protected $amount;

    public function calculateDiscount($discountAmount, $discountType, $discountReduction) {
            //amount type 1=percentage, 2=absolute
      if ($discountType == '1') {
        $discountreturn = $discountAmount-($discountAmount*($discountReduction/100));
      }
      elseif ($discountType == '2') {
        $discountreturn = $discountAmount-$discountReduction;
      }
      return($discountreturn);
    }


    /**
     * Every action must define a _run function to perform the work and place results in the Result object.
     *
     * When using the set of Basic actions, they define _run for you and you just need to provide a getter/setter function.
     *
     * @param Result $result
     */
    public function _run(Result $result) {

        $acceptedEntities = array(1 => 'event');

        //TO DO:
        ////add support for memberships and price sets

        $discountcodeDetails = \Civi\Api4\DiscountCode::get(TRUE)
            ->addWhere('code', '=', $this->discountCode)
            ->setLimit(25)
            ->execute();


        //return error, if no discount code found
        if ($discountcodeDetails[0] == []) {
            //negative result
            $result[] = [
                'discountCode' => $this->discountCode,
                'verify' => 'failed',
                'reason' => 'discount code not existing',
            ];
        }
        //check whether entityType is set and within allowed options
        elseif ($this->entityType != '' && in_array($this->entityType, $acceptedEntities) == false) {
          //negative result
          $result[] = [
            'discountCode' => $this->discountCode,
            'verify' => 'failed',
            'reason' => 'entityType not allowed',
          ];
        }
        //check whether discount code is active
        elseif ($discountcodeDetails[0]["is_active"] == false) {
            //negative result
            $result[] = [
                'discountCode' => $this->discountCode,
                'verify' => 'failed',
                'reason' => 'discount code not active',
            ];
        }
        //check whether maximum usage count is reached
        elseif ($discountcodeDetails[0]["count_use"] == $discountcodeDetails[0]["count_max"]) {
            //negative result
            $result[] = [
                'discountCode' => $this->discountCode,
                'verify' => 'failed',
                'reason' => 'discount code maximum usage count reached',
            ];
        }
        //check whether discount code eligible for given event
        elseif ($this->entityType == "event" && $discountcodeDetails[0]["events"][0] != "0" && in_array($this->entityId,$discountcodeDetails[0]["events"]) == false) {
                //negative result
                $result[] = [
                    'discountCode' => $this->discountCode,
                    'verify' => 'failed',
                    'reason' => 'discount code not valid for this event',
                ];
            }
        //check whether usage of discount code within the allowed date range
        elseif ((strtotime($discountcodeDetails[0]["active_on"]) >= time() && strtotime($discountcodeDetails[0]["expire_on"]) <= time()) OR ($discountcodeDetails[0]["active_on"] == false && strtotime($discountcodeDetails[0]["expire_on"]) <= time()) OR (strtotime($discountcodeDetails[0]["active_on"]) >= time() && $discountcodeDetails[0]["expire_on"] == false)) {
          //negative result
          $result[] = [
            'discountCode' => $this->discountCode,
            'verify' => 'failed',
            'reason' => 'discount code date validation failed',
          ];
        }
        else {
            //positive result
            $result[] = [
                'discountCode' => $this->discountCode,
                'verify' => 'success',
                'discountedAmount' => $this->calculateDiscount($this->amount,$discountcodeDetails[0]["amount_type"],$discountcodeDetails[0]["amount"]),

            ];
        }
    }

    /**
     * Declare ad-hoc field list for this action.
     *
     * Some actions return entirely different data to the entity's "regular" fields.
     *
     * This is a convenient alternative to adding special logic to our GetFields function to handle this action.
     *
     * @return array
     */
    public static function fields() {
        return [
            ['name' => 'discountCode'],
            ['name' => 'entityType'],
            ['name' => 'entityId', 'data_type' => 'Integer'],
            ['name' => 'amount', 'data_type' => 'Float'],
        ];
    }
}

?>
