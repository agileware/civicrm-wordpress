<?php

use Civi\Api4\PriceFieldValue;

/**
 * @package CiviDiscount
 */
class CRM_CiviDiscount_DiscountCalculator {
  protected string $entity;
  protected ?int $entity_id;
  protected array $discounts = [];
  protected ?int $contact_id;
  protected ?string $code;
  protected array $entityDiscounts;

  protected array $priceSetDiscounts;
  protected bool $is_display_field_mode;
  /**
   * Applicable automatic discounts.
   *
   * @var array
   */
  public array $autoDiscounts = [];

  /**
   * @var mixed
   */
  private int $price_set_id;

  /**
   * Constructor.
   *
   * @param string $entity
   * @param int|null $entity_id
   * @param int|null $contact_id
   * @param string|null $code
   * @param bool $is_display_field_mode - ie are we trying to calculate whether it would be possible to find a discount cod
   * @param int $priceSetID
   */
  public function __construct(string $entity, ?int $entity_id, ?int $contact_id, ?string $code, bool $is_display_field_mode, int $priceSetID) {
    if (empty($code) && !$contact_id && !$is_display_field_mode) {
      $this->discounts = [];
    }
    else {
      $this->discounts = CRM_CiviDiscount_BAO_Item::getValidDiscounts();
    }
    $this->entity = $entity;
    $this->contact_id = $contact_id;
    $this->entity_id = $entity_id;
    $this->code = ($code) ? trim($code) : $code;
    $this->is_display_field_mode = $is_display_field_mode;
    $this->price_set_id = $priceSetID;
  }

  /**
   * Get discounts that apply in this instance.
   */
  public function getDiscounts(): array {
    if (!empty($this->code)) {
      $this->filterDiscountByCode();
    }
    $this->filterDiscountByEntity();
    if (!$this->is_display_field_mode) {
      $this->filterDiscountsByContact();
    }
    return $this->entityDiscounts + $this->priceSetDiscounts;
  }

  /**
   * Filter this discounts according to entity.
   */
  protected function filterDiscountByEntity(): void {
    $this->entityDiscounts = $this->priceSetDiscounts = [];
    foreach ($this->discounts as $discount_id => $discount) {
      // WARNING! The previous attempted to improve performance in deciding when
      // the autoDiscount field should be displayed resulted in breakage.
      // See https://github.com/dlobo/org.civicrm.module.cividiscount/issues/145 before
      // attempting.
      if ($this->checkDiscountsByEntity($discount, $this->entity, $this->entity_id, 'filters')) {
        $this->entityDiscounts[$discount_id] = $discount;
      }
      if ($this->checkDiscountsByEntity($discount, 'price_set', $this->price_set_id, 'filters')) {
        $this->priceSetDiscounts[$discount_id] = $discount;
      }
    }
  }

  /**
   * Filter discounts by auto-discount criteria.
   *
   * If any one of the criteria is not met for this contact then the discount
   * does not apply
   *
   * We can assume that the no-contact id situation is dealt with in that
   * our scenarios are
   * - no contact id but code - in which case we will already be filtered down to code
   * - no contact id, no code & 'is_display_field_mode' - ie. anonymous mode so we don't need to filter by contact
   * - no contact id, no code & is not is_display_field_mode' - ie we won't have populated discounts in construct
   * (saves a query)
   */
  protected function filterDiscountsByContact(): void {
    if (empty($this->contact_id)) {
      return;
    }
    $this->autoDiscounts = $this->entityDiscounts + $this->priceSetDiscounts;
    foreach ($this->autoDiscounts as $discount_id => $discount) {
      if (empty($discount['autodiscount'])) {
        unset($this->autoDiscounts[$discount_id]);
      }
      else {
        foreach (array_keys($discount['autodiscount']) as $entity) {
          $additionalParams = ['contact_id' => $this->contact_id];
          $id = ($entity === 'contact') ? $this->contact_id : NULL;
          if (!$this->checkDiscountsByEntity($discount, $entity, $id, 'autodiscount', $additionalParams)) {
            unset($this->autoDiscounts[$discount_id]);
          }
        }
      }
    }
  }

  /**
   * Check if the entity has applicable discounts.
   */
  protected function getEntityHasDiscounts(): bool {
    $this->getDiscounts();
    if (!empty($this->entityDiscounts)) {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Check if the discount field should be shown.
   */
  public function isShowDiscountCodeField(): bool {
    if (!$this->getEntityHasDiscounts()) {
      return FALSE;
    }
    if (!empty($this->entityDiscounts)) {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Check if discount is applicable.
   *
   * We check the 'filters' to see if
   * 1) there are any filters for this entity type - no filter means NO
   * 2) there is an empty filter for this entity type - means 'any'
   * 3) the only filter is on id (in which case we will do a direct comparison
   * 4) there is an api filter
   *
   * @param array $discount
   * @param string $entity
   * @param int|null $id entity id
   * @param string $type 'filters' or autodiscount
   * @param array $additionalFilter e.g array('contact_id' => x) when looking at memberships
   *
   * @return bool
   */
  protected function checkDiscountsByEntity(array $discount, string $entity, ?int $id, string $type, array $additionalFilter = []): bool {
    try {
      if (!isset($discount[$type][$entity])) {
        return FALSE;
      }
      if (empty($discount[$type][$entity])) {
        return TRUE;
      }
      $entityDiscountDetail = $discount[$type][$entity];
      if (count($entityDiscountDetail) === 1 && !empty($entityDiscountDetail['id'])) {
        if (array_key_first($entityDiscountDetail['id']) === 'IS NOT NULL') {
          return TRUE;
        }
        // If this discount is only limited by specific entity (say, a specific
        // event and not an event type), we have the IDs already and don't need
        // to make an API call. Store the IDs in a structure like they would
        // have as the result of an API call.
        $ids = $discount[$type][$entity]['id']['IN'];
      }
      else {
        $params = $discount[$type][$entity] + array_merge([
            'options' => ['limit' => 999999999],
            'return' => 'id',
          ], $additionalFilter);
        $ids = array_keys(civicrm_api3($entity, 'get', $params)['values']);
      }
      if ($entity === 'price_set') {
        return (bool) PriceFieldValue::get(FALSE)
          ->addWhere('id', 'IN', $ids)
          ->addWhere('price_field_id.price_set_id', '=', $this->price_set_id)
          ->execute()->count();
      }

      if ($id) {
        return in_array($id, $ids, FALSE);
      }
      return !empty($ids);
    }
    catch (CRM_Core_Exception $e) {
      return FALSE;
    }
  }

  /**
   * If a code is passed in we are going to unset any filters that don't match the code.
   *
   * @todo cividiscount ignore case is always true - it's obviously preparatory to allowing
   * case sensitive.
   *
   * @return array
   */
  protected function filterDiscountByCode(): array {
    if (_cividiscount_ignore_case()) {
      foreach ($this->discounts as $id => $discount) {
        if (strcasecmp($this->code, $discount['code']) !== 0) {
          unset($this->discounts[$id]);
        }
      }
    }
    return $this->discounts;
  }

}
