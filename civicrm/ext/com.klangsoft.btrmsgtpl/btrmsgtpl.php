<?php

require_once 'btrmsgtpl.civix.php';
// phpcs:disable
use CRM_Btrmsgtpl_ExtensionUtil as E;
// phpcs:enable

function btrmsgtpl_civicrm_alterContent(  &$content, $context, $tplName, &$object ) {
  if ($tplName == 'CRM/Btrmsgtpl/Page/MessageTemplates.tpl') {
    $smarty = CRM_Core_Smarty::singleton();
    $html = $smarty->fetch(E::path('templates/CRM/Btrmsgtpl/Page/Standalone.tpl'));

    ob_end_clean();
    echo $html;
    exit(0);
  }
}

function btrmsgtpl_getLanguages() {
  $langs = [];
  $optionValues = \Civi\Api4\OptionValue::get(TRUE)
    ->addSelect('label', 'name')
    ->addWhere('option_group_id:label', '=', 'Languages')
    ->addWhere('is_active', '=', TRUE)
    ->execute();
  foreach ($optionValues as $optionValue) {
    $langs[$optionValue['name']] = $optionValue['label'];
  }
  return $langs;
}

function btrmsgtpl_getMessageTemplates() {
  $api = civicrm_api3('MessageTemplate', 'get', [
    'is_reserved' => 0,
    'return' => ['id', 'msg_title', 'workflow_name'],
    'options' => [
      'sort' => 'msg_title ASC',
      'limit' => 0
    ]
  ]);
  $templates = $api['values'];

  $dao = CRM_Core_DAO::executeQuery('SELECT msg_template_id, COUNT(*) AS num_samples
    FROM civicrm_msg_template_data
    GROUP BY msg_template_id
  ');
  while ($dao->fetch()) {
    $templates[$dao->msg_template_id]['num_samples'] = $dao->num_samples;
  }
  $dao = CRM_Core_DAO::executeQuery('SELECT diverted.id
    FROM civicrm_msg_template diverted
    JOIN civicrm_msg_template orig ON (diverted.workflow_name = orig.workflow_name
      AND orig.is_reserved = 1
      AND (
        diverted.msg_subject != orig.msg_subject
        OR diverted.msg_text    != orig.msg_text
        OR diverted.msg_html    != orig.msg_html
      )
    )
  ');
  while ($dao->fetch()) {
    $templates[$dao->id]['is_modified'] = 1;
  }

  $langs = btrmsgtpl_getLanguages();

  foreach ($templates as &$tpl) {
    $tpl['lang'] = [];
  }

  $translations = \Civi\Api4\Translation::get(TRUE)
    ->addSelect('entity_id', 'language')
    ->addWhere('entity_table', '=', 'civicrm_msg_template')
    ->addWhere('entity_field', '=', 'msg_html')
    ->execute();
  foreach ($translations as $translation) {
    if (empty($templates[$translation['entity_id']]['lang'][$translation['language']])) {
      $templates[$translation['entity_id']]['lang'][$translation['language']] = $langs[$translation['language']];
    }
  }

  foreach ($templates as &$tpl) {
    $tpl['num_samples'] = $tpl['num_samples'] ?? 0;
    $tpl['is_modified'] = $tpl['is_modified'] ?? 0;
    $tpl['workflow_name'] = $tpl['workflow_name'] ?? FALSE;
    $tpl['lang'] = array_values($tpl['lang']);

    if ($tpl['is_modified'] || !empty($tpl['num_samples']) || count($tpl['lang']) > 0) {
      $tpl['important'] =
        ($tpl['is_modified'] ? E::ts('Modified') : E::ts('System Default')) . 
        (!empty($tpl['num_samples']) ? E::ts(', %1 Samples', [ 1 => $tpl['num_samples'] ]) : '') .
        (count($tpl['lang']) > 0 ? ', ' . implode(', ', $tpl['lang']) : '');
    }
    else {
      $tpl['important'] = FALSE;
    }
  }
  return $templates;
}

function btrmsgtpl_getDefaults($msg_template_id) {
  $fmt = Civi::settings()->get('dateformatDatetime');
  $defaults = [];

  $query = 'SELECT id, rev_date, git_hash, notes
    FROM civicrm_msg_template_revisions
    WHERE msg_template_id = %1 AND git_hash IS NOT NULL
    ORDER BY rev_date DESC
  ';
  $dao = CRM_Core_DAO::executeQuery($query, [
    1 => [$msg_template_id, 'Integer']
  ]);
  while ($dao->fetch()) {
    $ts = strtotime($dao->rev_date);
    $defaults[$dao->git_hash] = [
      'id' => $dao->id,
      'name' => E::ts('System Default ') . date('Y-m-d', $ts),
      'alias' => CRM_Utils_Date::customFormat($dao->rev_date, $fmt),
      'ts' => $ts,
      'hash' => $dao->git_hash,
      'notes' => $dao->notes
    ];
  }
  return $defaults;
}

function btrmsgtpl_getRevisions($msg_template_id, $lang = '') {
  $fmt = Civi::settings()->get('dateformatDatetime');
  $revisions = [];

  $query = 'SELECT id, rev_alias, rev_date, rev_user_id, language, notes
    FROM civicrm_msg_template_revisions
    WHERE msg_template_id = %1 AND git_hash IS NULL' .
    ($lang ? ' AND language = %2' : '') . '
    ORDER BY rev_date DESC, rev_alias
  ';
  $dao = CRM_Core_DAO::executeQuery($query, [
    1 => [$msg_template_id, 'Integer'],
    2 => [$lang, 'String']
  ]);
  while ($dao->fetch()) {
    if (empty($revisions[$dao->language])) {
      $revisions[$dao->language] = [];
    }
    $date = CRM_Utils_Date::customFormat($dao->rev_date, $fmt);
    $name = E::ts('%1 by %2', [
      1 => $dao->rev_alias ? $dao->rev_alias : $date,
      2 => btrmsgtpl_revisionAuthor($dao->rev_user_id)
    ]);

    $revisions[$dao->language][] = [
      'id' => $dao->id,
      'name' => $name,
      'alias' => $dao->rev_alias ? $date : FALSE,
      'ts' => strtotime($dao->rev_date),
      'hash' => false,
      'notes' => ''
    ];
  }
  if ($lang) {
    return $revisions[$lang];
  }
  return $revisions;
}

function btrmsgtpl_revisionAuthor($contact_id) {
  static $authors;

  if (!$authors) {
    $authors = [];
  }
  if (empty($authors[$contact_id])) {
    try {
      $authors[$contact_id] = civicrm_api3('Contact', 'getvalue', [
        'id' => $contact_id,
        'return' => 'display_name'
      ]);
    }
    catch (\Exception $e) {
      $authors[$contact_id] = E::ts('Deleted Contact') . " ($contact_id)";
    }
  }
  return $authors[$contact_id];
}

/**
 * add revision support for system workflow message templates
 *
 * @return bool
 */
function btrmsgtpl_add_revision_support() {
  try {
    CRM_Core_DAO::executeQuery("DROP TABLE IF EXISTS `civicrm_msg_template_revisions`");
    CRM_Core_DAO::executeQuery("CREATE TABLE `civicrm_msg_template_revisions` LIKE `civicrm_msg_template`");

    CRM_Core_DAO::executeQuery("ALTER TABLE `civicrm_msg_template_revisions`
      ADD COLUMN `msg_template_id` int unsigned NOT NULL COMMENT 'Message Template ID' AFTER `id`,
      CHANGE COLUMN `msg_title` `rev_alias` varchar(255) DEFAULT NULL COMMENT 'Descriptive title of message',
      DROP COLUMN `is_active`,
      DROP COLUMN `workflow_id`,
      DROP COLUMN `workflow_name`,
      DROP COLUMN `is_default`,
      DROP COLUMN `is_reserved`,
      DROP COLUMN `is_sms`,
      DROP COLUMN `pdf_format_id`,
      ADD COLUMN `rev_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
      ADD COLUMN `rev_user_id` int DEFAULT NULL,
      ADD COLUMN `language` varchar(5) NOT NULL DEFAULT '_std_' COMMENT 'Locale of the revision',
      ADD COLUMN `git_hash` varchar(40) DEFAULT NULL COMMENT 'Commit ID default template changed',
      ADD COLUMN `notes` varchar(2048) DEFAULT NULL COMMENT 'Notes/commit message for the revision',
      ADD INDEX (`msg_template_id`),
      ADD INDEX (`language`)
    ");

    if (Civi::settings()->get('logging')) {
      $dsn = DB::parseDSN(CRM_Utils_SQL::autoSwitchDSN(defined('CIVICRM_LOGGING_DSN') ? CIVICRM_LOGGING_DSN : CIVICRM_DSN));
      $db = $dsn['database'];

      CRM_Core_DAO::executeQuery("INSERT INTO `civicrm_msg_template_revisions` (`msg_template_id`, `msg_subject`, `msg_text`, `msg_html`, `rev_date`, `rev_user_id`)
        SELECT `id`, `msg_subject`, `msg_text`, `msg_html`, `log_date`, `log_user_id`
        FROM {$db}.log_civicrm_msg_template
        WHERE `workflow_id` IS NOT NULL
          AND `is_default` = 1
      ");
    }
    else {
      $contact_id = CRM_Core_Session::getLoggedInContactID();

      CRM_Core_DAO::executeQuery("INSERT INTO `civicrm_msg_template_revisions` (`msg_template_id`, `msg_subject`, `msg_text`, `msg_html`, `rev_user_id`)
        SELECT `id`, `msg_subject`, `msg_text`, `msg_html`, $contact_id
        FROM civicrm_msg_template
        WHERE `workflow_id` IS NOT NULL
          AND `is_default` = 1
      ");
    }
    return TRUE;
  }
  catch (\Exception $e) {}

  return FALSE;
}

/**
 * get active message headers/footers for tokens
 *
 * @param boolean $withContent Whether to include message content.
 * @return array List of headers keyed on munged token name.
 */
function btrmsgtpl_headers_footers($withContent = FALSE) {
  $hfs = [
    'Header' => [],
    'Footer' => []
  ];
  $params = [
    'component_type' => [
      'IN' => ['header', 'footer']
    ],
    'is_active' => 1,
    'return' => ['component_type', 'name', 'is_default'],
    'options' => [
      'limit' => 0,
      'sort' => 'name ASC'
    ]
  ];
  if ($withContent) {
    $params['return'] = array_merge($params['return'], ['body_html', 'body_text']);
  }
  $api = civicrm_api3('MailingComponent', 'get', $params);
  foreach ($api['values'] as $hf) {
    $hfs[$hf['component_type']][CRM_Utils_String::munge(strtolower($hf['name']))] = $hf;
  }
  return [
    'header' => $hfs['Header'],
    'footer' => $hfs['Footer']
  ];
}

/**
 * register header/footer tokens
 *
 * @param \Civi\Token\Event\TokenRegisterEvent $e
 * @return void
 */
function btrmsgtpl_register_tokens(\Civi\Token\Event\TokenRegisterEvent $e) {
  $hfs = btrmsgtpl_headers_footers();
  
  foreach ($hfs as $type => $tokens) {
    if (!empty($tokens)) {
      $entity = $e->entity($type);
      $entity->register('_default', E::ts('Default Message %1', [ 1 => ucwords($type) ]));

      foreach ($tokens as $token => $hf) {
        $entity->register($token, $hf['name']);
      }
    }
  }
}

/**
 * evaluate header/footer tokens
 *
 * @param \Civi\Token\Event\TokenValueEvent $e
 * @return void
 */
function btrmsgtpl_evaluate_tokens(\Civi\Token\Event\TokenValueEvent $e) {
  $hfs = btrmsgtpl_headers_footers(TRUE);
  $used = $e->getTokenProcessor()->getMessageTokens();

  foreach ($hfs as $type => $tokens) {
    if (empty($used[$type])) {
      continue;
    }
    $need_default = in_array('_default', $used[$type]);

    foreach ($e->getRows() as $row) {
      $row->format('text/html');
      foreach ($tokens as $token => &$hf) {
        if (in_array($token, $used[$type])) {
          btrmsgtpl_evaluate_hf($token, $hf);
          $row->tokens($type, $token, $hf['body_html']);
        }
        if ($need_default && $hf['is_default']) {
          btrmsgtpl_evaluate_hf($token, $hf);
          $row->tokens($type, '_default', $hf['body_html']);
        }
      }
      $row->format('text/plain');
      foreach ($tokens as $token => &$hf) {
        if (in_array($token, $used[$type])) {
          btrmsgtpl_evaluate_hf($token, $hf);
          $row->tokens($type, $token, $hf['body_text']);
        }
        if ($need_default && $hf['is_default']) {
          btrmsgtpl_evaluate_hf($token, $hf);
          $row->tokens($type, '_default', $hf['body_text']);
        }
      }
    }
  }
}

/**
 * evaluate header/footer token (because might contain other tokens)
 * uses context of document being rendered
 *
 * @param [string] $token
 * @param [object] $hf
 * @return void
 */
function btrmsgtpl_evaluate_hf($token, &$hf) {
  if (empty($hf['evaluated'])) {
    $tp = new \Civi\Token\TokenProcessor(\Civi::dispatcher(), [
      'controller' => __FILE__,
      'smarty' => FALSE,
    ]);
    $tp->addMessage('body_html', $hf['body_html'], 'text/html');
    $tp->addMessage('body_text', $hf['body_text'], 'text/plain');
    $tp->addRow(\Civi::cache('short')->get('tokenContext'));
    $tp->evaluate();
    foreach ($tp->getRows() as $row) {
      $hf['body_html'] = $row->render('body_html');
      $hf['body_text'] = $row->render('body_text');
    }
    $hf['evaluated'] = TRUE;
  }
}

/**
 * Implements hook_civicrm_pageRun()
 * 
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_pageRun/
 * @param object $page
 * @return void
 */
function btrmsgtpl_civicrm_pageRun(&$page) {
  $pageName = $page->getVar('_name');

  // add a Better Message Templates suggestion to the base CiciCRM message template pages
  if (in_array($pageName, ['CRM_Admin_Page_MessageTemplates', 'CRM_MessageAdmin_Page_MsgtplBasePage'])) {

    CRM_Core_Region::instance('page-footer')->add([
      'jquery' => '
        setTimeout(() => {
          const url = CRM.url("civicrm/admin/btrmsgtpl", { reset: 1 });
          const msg = ts("For an improved System Workflow experience, use <a href=\"%1\">Better Message Templates</a>", { 1: url });
          CRM.alert(msg, ts("Better Message Templates"), "info", { expires: 20000 });
        }, 3000);
      '
    ]);
  }
}

/**
 * Implements hook_civicrm_alterMailContent().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_alterMailContent
 */
function btrmsgtpl_civicrm_alterMailContent(&$content) {
  if (!empty($content['workflow_name'])) {
    $header = Civi::settings()->get('btrmsgtpl_inject_header');
    if ($header) {
      if (!empty($content['html'])) {
        $content['html'] = preg_replace('/(<!-- BEGIN HEADER -->).*(<!-- END HEADER -->)/ms', '$1{header.' . $header . '}$2', $content['html']);
      }
      if (!empty($content['text'])) {
        $content['text'] = '{header.' . $header . '}' . $content['text'];
      }
    }
    $footer = Civi::settings()->get('btrmsgtpl_inject_footer');
    if ($footer) {
      if (!empty($content['html'])) {
        $content['html'] = preg_replace('/(<\/body>)/m', '{footer.' . $footer . '}$1', $content['html']);
      }
      if (!empty($content['text'])) {
        $content['text'] = $content['text'] . '{footer.' . $footer . '}';
      }
    }
  }
}

/**
 * Implements hook_civicrm_alterMailParams(). - via addListener()
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_alterMailParams
 */
function btrmsgtpl_listen_alterMailParams($event) {
  static $samples;

  $params = $event->params;

  if (!empty($params['tokenContext'])) {
    \Civi::cache('short')->set('tokenContext', $params['tokenContext']);
  }

  if ($event->context == 'messageTemplate' && !empty($params['workflow']) && empty($params['tplParams']['_btrmsgtpl_preview_'])) {

    // determine sample name
    try {
      if (!empty($params['contactId'])) {
        $name = civicrm_api3('Contact', 'getvalue', [
          'id' => $params['contactId'],
          'return' => 'display_name'
        ]);
      }
      else {
        // TODO - make sure this is right
        // have only seen anonymous samples when capturing preview from message_admin
        // we don't want samples for that
        return;
        $name = E::ts('Anonymous');
      }
      $now = date('Ymd.His');

      $sample_name = "$name $now";

      if (!$samples) {
        $samples = [$sample_name];
      }
      elseif (array_search($sample_name, $samples) === FALSE) {
        $samples[] = $sample_name;
      }
      else {
        // we've already captured this
        return;
      }
    }
    catch (Exception $e) {
      \Civi::log()->error(E::ts('btrmsgtpl: unable to determine sample name: ') . $e->getMessage());
      return;
    }

    // capture sample data for the template
    $vars = [];
    $tplParams = $params['tplParams'];

    $alreadyAssigned = CRM_Core_Smarty::singleton()->get_template_vars();
    foreach ($alreadyAssigned as $key => $val) {
      if (!empty($val) && empty($tplParams[$key]) && !is_array($val) && !is_object($val)) {
        $tplParams[$key] = $val;
      }
    }

    foreach($tplParams as $key => $val) {
      if (is_array($val)) {
        $type = 'structured';
        $val = var_export($val, TRUE);
      }
      elseif (is_bool($val)) {
        $type = 'bool';
      }
      else {
        $type = 'simple';
        $val = (string) $val;
      }
      if (!is_null($val)) {
        $vars[] = [
          'name' => "\$$key",
          'type' => $type,
          'value' => $val
        ];
      }
    }

    if (!empty($params['tokenContext'])) {
      $vars[] = array_merge(['name' => 'tokenContext'], $params['tokenContext']);
    }

    try {
      if (!empty($params['messageTemplateID'])) {
        $tplID = $params['messageTemplateID'];
      }
      else {
        $tplID = civicrm_api3('MessageTemplate', 'getvalue', [
          'workflow_name' => $params['workflow'],
          'is_default' => 1,
          'return' => 'id'
        ]);
      }

      civicrm_api3('MessageTemplateData', 'create', [
        'msg_template_id' => $tplID,
        'msg_variables' => json_encode($vars),
        'sample_name' => $sample_name
      ]);

      // limit sample captures to 25
      $created_date = CRM_Core_DAO::singleValueQuery('SELECT created_date
        FROM civicrm_msg_template_data
        WHERE msg_template_id = %1 AND is_capture = 1
        ORDER BY created_date DESC
        LIMIT 25, 1
      ', [
        1 => [$tplID, 'Int']
      ]);
      if ($created_date) {
        CRM_Core_DAO::executeQuery('DELETE FROM civicrm_msg_template_data
          WHERE msg_template_id = %1 AND is_capture = 1 AND created_date <= %2
        ', [
          1 => [$tplID, 'Int'],
          2 => [$created_date, 'String']
        ]);
      }
    }
    catch (Exception $e) {
      \Civi::log()->error(E::ts('btrmsgtpl: unable to create sample: ') . $e->getMessage());
    }
  }
}

/**
 * Implements hook_civicrm_post().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_post
 */
function btrmsgtpl_civicrm_post(string $op, string $objectName, int $objectId, &$objectRef) {
  static $trans;

  try {
    // save template revision
    if ($op == 'edit' && $objectName == 'MessageTemplate') {
      CRM_Core_DAO::executeQuery('INSERT INTO civicrm_msg_template_revisions
        (msg_template_id, msg_html, msg_subject, msg_text, rev_user_id)
        SELECT %1, msg_html, msg_subject, msg_text, %2
          FROM civicrm_msg_template
          WHERE id = %1
            AND workflow_id IS NOT NULL
            AND is_active = 1
            AND is_default = 1
      ', [
        1 => [$objectId, 'Integer'],
        2 => [CRM_Core_Session::getLoggedInContactID(), 'Integer']
      ]);
    }
    // save translation revision
    if (
      in_array($op, ['create', 'edit']) &&
      $objectName == 'Translation' &&
      $objectRef->entity_table = 'civicrm_msg_template' && 
      $objectRef->status_id == 1
    ) {
      if (empty($trans[$objectRef->entity_id])) {
        $trans[$objectRef->entity_id] = [ 'language' => $objectRef->language ];
      }
      $trans[$objectRef->entity_id][$objectRef->entity_field] = html_entity_decode($objectRef->string);
  
      if (count($trans[$objectRef->entity_id]) == 4) {
        CRM_Core_DAO::executeQuery('INSERT INTO civicrm_msg_template_revisions
          (msg_template_id, msg_html, msg_subject, msg_text, language, rev_user_id)
          VALUES (%1, %2, %3, %4, %5, %6)
        ', [
          1 => [$objectRef->entity_id, 'Integer'],
          2 => [$trans[$objectRef->entity_id]['msg_html'], 'String'],
          3 => [$trans[$objectRef->entity_id]['msg_subject'], 'String'],
          4 => [$trans[$objectRef->entity_id]['msg_text'], 'String'],
          5 => [$trans[$objectRef->entity_id]['language'], 'String'],
          6 => [CRM_Core_Session::getLoggedInContactID(), 'String']
        ]);
        unset($trans[$objectRef->entity_id]);
      }
    }
  }
  catch (\Exception $e) {
    \Civi::log()->info(E::ts('btrmsgtpl: failed saving revision: ') . $e->getMessage());
  }
}

/**
 * Implements hook_civicrm_install().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_install
 */
function btrmsgtpl_civicrm_install() {
  _btrmsgtpl_civix_civicrm_install();
  btrmsgtpl_add_revision_support();
}

/**
 * Implements hook_civicrm_config().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_config/
 */
function btrmsgtpl_civicrm_config(&$config) {
  _btrmsgtpl_civix_civicrm_config($config);

  if (isset(Civi::$statics[__FUNCTION__])) {
    return;
  }
  Civi::$statics[__FUNCTION__] = 1;
}

/**
 * Implements hook_civicrm_container().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_container
 */
function btrmsgtpl_civicrm_container(\Symfony\Component\DependencyInjection\ContainerBuilder $container) {
  $container->addResource(new \Symfony\Component\Config\Resource\FileResource(__FILE__));
  $dispatcher = $container->findDefinition('dispatcher');
  $dispatcher->addMethodCall('addListener',
    ['civi.token.list', 'btrmsgtpl_register_tokens']
  )->setPublic(TRUE);
  $dispatcher->addMethodCall('addListener',
    ['civi.token.eval', 'btrmsgtpl_evaluate_tokens']
  )->setPublic(TRUE);

  // used to be in hook_civicrm_config as Civi::dispatcher()->addListener('hook_civicrm_alterMailParams', 'btrmsgtpl_listen_alterMailParams', 0 - PHP_INT_MAX) but that stopped working for some reason
  $dispatcher->addMethodCall('addListener', ['hook_civicrm_alterMailParams', 'btrmsgtpl_listen_alterMailParams', 0 - PHP_INT_MAX]);
}

/**
 * Implements hook_civicrm_enable().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_enable
 */
function btrmsgtpl_civicrm_enable() {
  _btrmsgtpl_civix_civicrm_enable();
}

// --- Functions below this ship commented out. Uncomment as required. ---

/**
 * Implements hook_civicrm_preProcess().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_preProcess
 */
//function btrmsgtpl_civicrm_preProcess($formName, &$form) {
//
//}

/**
 * Implements hook_civicrm_navigationMenu().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_navigationMenu
 */
function btrmsgtpl_civicrm_navigationMenu(&$menu) {
  $items = [];

  $scan = function($path, $sub) use (&$items, &$scan) {
    foreach ($sub as $item) {
      if ($item['attributes']['name'] == 'Message Templates') {
        $items[trim($path, '/')] = $item['attributes'];
      }
      if (!empty($item['child'])) {
        $scan("$path/{$item['attributes']['name']}", $item['child']);
      }
    }
  };
  $scan('', $menu);

  // create a btrmsgtpl menu item after every messge templates menu item
  foreach ($items as $path => $item) {
    _btrmsgtpl_civix_insert_navigation_menu($menu, $path, [
      'name' => E::ts('Better Message Templates'),
      'url' => 'civicrm/admin/btrmsgtpl?reset=1',
      'permission' => $item['permission'],
      'operator' => $item['operator'],
      'weight' => $item['weight'],
    ]);
  }
  _btrmsgtpl_civix_navigationMenu($menu);
}
