<?php

// phpcs:disable
use CRM_Btrmsgtpl_ExtensionUtil as E;
// phpcs:enable

use Civi\Token\TokenProcessor;

class CRM_Btrmsgtpl_Page_MessageTemplates extends CRM_Core_Page {

  function run() {
    $this->baseUrl = CRM_Utils_System::url('civicrm/admin/btrmsgtpl', 'reset=1');

    $id = CRM_Utils_Request::retrieve('id', 'Positive', $this);
    $vars = $id ? $this->editor($id) : $this->index();

    $vars['restApi'] = CRM_Utils_System::url('civicrm/ajax/rest');

    // for ts()
    $strings = [];
    $region = CRM_Core_Region::instance('html-header', FALSE);
    if ($region) {
      $strings = $region->getSettings()['strings'] ?? $strings;
    }
    $vars['strings'] = $strings;

    $this->assign('mtid', $id);
    $this->assign('vars', base64_encode(json_encode($vars)));
    $this->assign('resourceCacheCode', Civi::resources()->getCacheCode());
    $this->assign('js', E::url('js'));
    $this->assign('css', E::url('css'));

    parent::run();
  }

  protected function index() {
    $vars = [];

    $vars['return_url'] = CRM_Utils_System::url('civicrm');

    $messages = (new CRM_Utils_Check_Component_Tokens())->checkTokens();
    $warning = count($messages) > 0 ? $messages[0]->toArray() : ['is_visible' => FALSE];

    // token warnings
    if ($warning['is_visible']) {
      $workflows = array_keys(CRM_Utils_Token::getTokenDeprecations()['WorkFlowMessageTemplates']);
      sort($workflows);
      $titles = [];

      $api = civicrm_api3('MessageTemplate', 'get', [
        'workflow_name' => [
          'IN' => $workflows
        ],
        'is_reserved' => 0,
        'options' => [
          'limit' => 0,
          'sort' => 'workflow_name ASC'
        ],
        'return' => 'msg_title'
      ]);
      foreach ($api['values'] as $tpl) {
        $titles[] = "<em>{$tpl['msg_title']}</em>";
      }
      $warning['message'] = str_replace($workflows, $titles, $warning['message']);
    }
    $this->assign('warning', $warning);

    $this->assign('allTemplates', btrmsgtpl_getMessageTemplates());

    $this->assign('tour', E::url('docs/btrmsgtpl/visual-tour.html'));

    $hfs = btrmsgtpl_headers_footers();
    $this->assign('headers', $hfs['header']);
    $this->assign('injectHeader', Civi::settings()->get('btrmsgtpl_inject_header'));
    $this->assign('footers', $hfs['footer']);
    $this->assign('injectFooter', Civi::settings()->get('btrmsgtpl_inject_footer'));

    $vars['user_driven'] = CRM_Utils_System::url('civicrm/admin/messageTemplates/add', 'reset=1&action=update&id=id');
    $vars['system_workflow'] = CRM_Utils_System::url('civicrm/admin/btrmsgtpl', 'reset=1&id=id');
     
    return $vars;
  }

  protected function editor($id) {
    $vars = [];

    $vars['return_url'] = $this->baseUrl;

    try {
      $msg_tpl = civicrm_api3('MessageTemplate', 'getsingle', [
        'id' => $id,
        'is_reserved' => 0,
        'return' => ['id', 'msg_subject', 'msg_text', 'msg_html', 'msg_title', 'workflow_name']
      ]);
      $reserved = civicrm_api3('MessageTemplate', 'getsingle', [
        'workflow_name' => $msg_tpl['workflow_name'],
        'is_reserved' => 1,
        'return' => ['msg_html', 'msg_text', 'msg_subject']
      ]);
      $msg_tpl['msg_text'] = $msg_tpl['msg_text'] ?? '';
      $reserved['msg_text'] = $reserved['msg_text'] ?? '';
    }
    catch (CiviCRM_API3_Exception $e) {
      CRM_Utils_System::redirect($this->baseUrl);
    }

    $this->assign('title', $msg_tpl['msg_title']);

    $revisions = btrmsgtpl_getRevisions($id);

    $lang = '_std_';
    $langs = btrmsgtpl_getLanguages();
    $this->assign('lang', $lang);
    $this->assign('langs', $langs);

    $msg_tpl['content'] = [
      '_std_' => [
        'ids' => FALSE,
        'lang' => '_std_',
        'language' => E::ts('Standard'),
        'revisions' => $revisions['_std_'] ?? [],
        'modified' => FALSE,
        'msg_html' => $msg_tpl['msg_html'],
        'msg_text' => $msg_tpl['msg_text'],
        'msg_subject' => $msg_tpl['msg_subject']
      ]
    ];
    unset($msg_tpl['msg_html'], $msg_tpl['msg_text'], $msg_tpl['msg_subject']);

    $translations = \Civi\Api4\Translation::get(TRUE)
      ->addSelect('id', 'entity_field', 'language', 'string', 'status_id:name')
      ->addWhere('entity_table', '=', 'civicrm_msg_template')
      ->addWhere('entity_id', '=', $id)
      ->execute();
    foreach ($translations as $translation) {
      if ($translation['status_id:name'] == 'draft') {
        $name = $langs[$translation['language']];
        $translation['language'] .= '_draft';
        $langs[$translation['language']] = "$name (Draft)";
      }
      if (empty($msg_tpl['content'][$translation['language']])) {
        $msg_tpl['content'][$translation['language']] = [
          'ids' => [],
          'lang' => $translation['language'],
          'language' => $langs[$translation['language']],
          'revisions' => $revisions[substr($translation['language'], 0, 5)] ?? [ $msg_tpl['content']['_std_']['revisions'][0]],
          'modified' => FALSE
        ];
      }
      if ($translation['entity_field'] == 'msg_html') {
        $translation['string'] = str_replace('</body>', '<endbody>', $translation['string']);
      }
      $msg_tpl['content'][$translation['language']][$translation['entity_field']] = $translation['string'];
      $msg_tpl['content'][$translation['language']]['ids'][$translation['entity_field']] = $translation['id'];
    }
    uasort($msg_tpl['content'], function($a, $b) {
      return strcmp($a['language'], $b['language']);
    });

    $msg_tpl['defaults'] = [];

    $tokenProcessor = new TokenProcessor(Civi::dispatcher(), ['schema' => ['contactId']]);
    $tokens = CRM_Utils_Token::formatTokensForDisplay($tokenProcessor->listTokens());
    
    $vars['reserved'] = $reserved;
    $vars['tpl'] = $msg_tpl;
    $vars['warnings'] = CRM_Utils_Token::getTokenDeprecations()['WorkFlowMessageTemplates'];

    $this->assign('tokens', $tokens);
    $this->assign('vs', E::url('js/monaco-editor/min/vs'));

    return $vars;
  }


}