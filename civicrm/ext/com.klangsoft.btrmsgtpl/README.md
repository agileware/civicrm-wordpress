# Better Message Templates (btrmsgtpl)

This is an extension that provides an alternative user interface for working with system workflow message templates. It provides tools to help visualize, edit, and troubleshoot these templates, by...

* Replacing the plain textarea control for editing with the Monaco code editor.
* Providing a side by side, contextually highlighted outline.
  * Smarty code is enclosed in collapsible blocks or highlighted inline.
  * {ts} is highlighted to help you find your strings.
  * Variables and tokens are colorized.
  * Clicking a variable or token will select all other references to it.
  * Clicking a section of text will highlight it in the text editor, and scroll it into view.
* Detecting missing/unexpected Smarty tags.
* Warning of outdated token use.
* Detecting all variables and tokens used in the template.
  * All references to the selected variable are highlighted in the outline.
* Capturing template data during normal workflow, to be used as sample data.
* Providing preview of the rendered Smarty template.
* Sending the rendered Smarty templates via email, to see how it looks there too.
* Message template revisions.
* Comparing current message to system default or revision.
  * Easily apply/revert individual changes.
  * Revert entire message.
* Language translations.
* Drafts.

The extension is licensed under [AGPL-3.0](LICENSE.txt).

## Requirements

* PHP 7.2+
* CiviCRM 5.46+

## Installation (Web UI)

Learn more about installing CiviCRM extensions in the [CiviCRM Sysadmin Guide](https://docs.civicrm.org/sysadmin/en/latest/customize/extensions/).

## Installation (CLI, Zip)

Sysadmins and developers may download the `.zip` file for this extension and
install it with the command-line tool [cv](https://github.com/civicrm/cv).

```bash
cd <extension-dir>
cv dl btrmsgtpl@https://lab.civicrm.org/extensions/btrmsgtpl/-/archive/master/btrmsgtpl-master.zip
```

## Installation (CLI, Git)

Sysadmins and developers may clone the [Git](https://en.wikipedia.org/wiki/Git) repo for this extension and
install it with the command-line tool [cv](https://github.com/civicrm/cv).

```bash
git clone https://lab.civicrm.org/extensions/btrmsgtpl.git
cv en btrmsgtpl
```

