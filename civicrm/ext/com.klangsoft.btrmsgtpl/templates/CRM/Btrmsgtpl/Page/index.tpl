{if $warning.is_visible}
  <div class="p-3 bg-warning-subtle">
    {capture assign=infoTitle}{$warning.title}{/capture}
    {capture assign=infoMessage}{$warning.message}{/capture}
    {include file="CRM/common/info.tpl" infoType="no-popup"}
  </div>
{/if}

<div class="row">
  <div class="col m-2">
    <div id="search-a-roni">
      <input id="btrmsgtpl-search" class="form-control form-text" type="text" placeholder="{ts}search message content for...{/ts}" />
      <div class="form-text" data-style="margin-top:8px;font-size:smaller">{ts 1='<span class="key">ENTER</span>' 2='<span class="key">ESC</span>'}Press %1 to execute search, %2 to clear.{/ts}</div>
    </div>

    <div class="mt-3">
      <select id="btrmsgtpl-id" class="form-select">
        <option></option>
        <optgroup label="{ts}User-driven{/ts}">
          {foreach from=$allTemplates item=tpl}
            {if !$tpl.workflow_name}
              <option value="{$tpl.id}" class="user important">{$tpl.msg_title}</option>
            {/if}
          {/foreach}
        </optgroup>
        <optgroup label="{ts}System Workflow{/ts}">
          {foreach from=$allTemplates item=tpl}
            {if $tpl.workflow_name}
              <option value="{$tpl.id}"{if $tpl.important} class="important" data-important="{$tpl.important}"{/if}>{$tpl.msg_title}</option>
            {/if}
          {/foreach}
        </optgroup>
      </select>
    </div>

    <hr />

    <div class="card mt-2" id="-btrmsgtpl-settings">
      <div class="card-header">
        <i class="fa fa-globe me-2"></i>{ts}Global Settings{/ts}
      </div>
      <div class="card-body btrmsgtpl-setting">
        <div class="form-text mb-3">{ts}These settings affect general operation for all users.{/ts}</div>

        <label class="form-label" for="setting-inject-header">{ts}Automatically Inject Header{/ts}</label>
        <select class="form-select" id="setting-inject-header">
          <option value="">{ts}- none -{/ts}</option>
          <option value="_default"{if $injectHeader == '_default'} selected="selected"{/if}>{ts}Default Message Header{/ts}</option>
          {foreach from=$headers key=token item=header}
            <option value="{$token}"{if $token == $injectHeader} selected="selected"{/if}>{$header.name}</option>
          {/foreach}
        </select>
        <div class="form-text mb-3">The selected <code>{ldelim}header{rdelim}</code> token will be placed between the <code>&#x3C;!-- BEGIN HEADER --&#x3E;</code> and <code>&#x3C;!-- END HEADER --&#x3E;</code> comments in the template, for HTML content. For plain-text, the <code>{ldelim}header{rdelim}</code> token is placed before all other content.</div>

        <label class="form-label" for="setting-inject-footer">{ts}Automatically Inject Footer{/ts}</label>
        <select class="form-select" id="setting-inject-footer">
          <option value="">{ts}- none -{/ts}</option>
          <option value="_default"{if $injectFooter == '_default'} selected="selected"{/if}>{ts}Default Message Footer{/ts}</option>
          {foreach from=$footers key=token item=footer}
            <option value="{$token}"{if $token == $injectFooter} selected="selected"{/if}>{$footer.name}</option>
          {/foreach}
        </select>
        <div class="form-text">The selected <code>{ldelim}footer{rdelim}</code> token will be placed directly before <code>&#x3C;/body&#x3E;</code>, for HTML content. For plain-text, the <code>{ldelim}footer{rdelim}</code> token is placed after all other content.</div>
      </div>
    </div>

    <div class="card mt-2" id="-btrmsgtpl-prefs">
      <div class="card-header">
        <i class="fa-solid fa-user me-2"></i>{ts}Personal Preferences{/ts}
      </div>
      <div class="card-body">
        <div class="form-text mb-3">{ts}These settings apply to this browser on this device.{/ts}</div>
        <div class="form-check mb-3 btrmsgtpl-pref">
          <input class="form-check-input" type="checkbox" id="pref-important" />
          <label class="form-check-label" for="pref-important">{ts}Only list important templates (System Workflow){/ts}</label>
          <div class="form-text">{ts}Important templates are those that have been modified and/or have a translation and/or have samples available. In other words, templates being used by the site.{/ts}</div>
        </div>
        <div class="mb-3 btrmsgtpl-pref">
          <label class="form-label" for="pref-font-size">{ts}Font Size{/ts}</label>
          <input class="form-control" type="number" min="8" max="96" id="pref-font-size" />
          <div class="form-text">{ts}Font size to be used by the Monaco code editors and the outline tool.{/ts}</div>
        </div>
        <div class="mb-3 btrmsgtpl-pref">
          <label class="form-label" for="pref-tool-side">{ts}Tool Side{/ts}</label>
          <select class="form-select" id="pref-tool-side">
            <option value="right">{ts}Right{/ts}</option>
            <option value="left">{ts}Left{/ts}</option>
          </select>
          <div class="form-text">{ts}Which side of the editor should display the selected tool?{/ts}</div>
        </div>
        <div class="mb-3 btrmsgtpl-pref">
          <label class="form-label" for="pref-wordwrap">{ts}Wordwrap{/ts}</label>
          <select class="form-select" id="pref-wordwrap">
            <option value="">{ts}Off{/ts}</option>
            <option value="same">{ts}On{/ts}</option>
            <option value="indent">{ts}Indent{/ts}</option>
            <option value="deepIndent">{ts}Deep Indent{/ts}</option>
          </select>
          <div class="form-text">{ts}Wordwrap mode to be used by the Monaco code editor.{/ts}</div>
        </div>
      </div>
    </div>
  </div>

  <div class="col m-2">
    <h3>User Guide</h3>
    <p>Check out the <a href="{$tour}" target="_blank">Visual Tour <i class="fa-solid fa-external-link"></i></a> - tons of images, and a few words, covering every aspect of <em>Better Message Templates</em>. Don't miss the part about <a href="{$tour}#curating" target="_blank">Curating Quality Reference Samples <i class="fa-solid fa-external-link"></i></a>.</p>
    <p><strong>NOTE:</strong> The <em>Visual Tour</em> is for a previous version. Things look different now, but a lot of the functionality/explanation still applies.</p>

    <h3>Support</h3>
    <p>Want to provide some feedback? Did you find an issue that needs addressed? Do you have a feature request or suggestion for the documentation? Please check out the <a href="https://lab.civicrm.org/extensions/btrmsgtpl/-/issues" target="_blank">issue queue <i class="fa-solid fa-external-link"></i></a> to create a new issue or chime in on an existing one.</p>

    <p>Are you a developer looking to help out with some solid code? Please check out the <a href="https://lab.civicrm.org/extensions/btrmsgtpl/-/issues" target="_blank">issue queue <i class="fa-solid fa-external-link"></i></a>, work up a merge request, and submit for consideration. All help is appreciated.</p>

    <p>Are you an organization or CiviCRM Solution Provider with some funds to spare and would like to aid the continued development of this and other quality CiviCRM extensions? Please consider contributing at <a href="https://paypal.me/klangsoft" target="_blank">https://paypal.me/klangsoft <i class="fa-solid fa-external-link"></i></a>. Any amount is both helpful and appreciated. Thanks!</p>
  </div>
</div>
