<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <title>Better Message Templates{if !empty($title)} - {$title}{/if}</title>

    <script type="text/javascript">
      const btrmsgtpl = {ldelim}{rdelim};
      {if $mtid}
        var require = {ldelim} paths: {ldelim} vs: '{$vs}' {rdelim} {rdelim};
      {/if}
    </script>

    <!-- jQuery -->
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/jquery@3.7.1/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL" crossorigin="anonymous"></script>
    {if !$mtid}
      <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/select2-bootstrap-5-theme@1.3.0/dist/select2-bootstrap-5-theme.min.css" />
    {/if}
    <!-- Fontawesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.5.1/css/all.min.css" integrity="sha512-DTOQO9RWCH3ppGqcWaEA1BIZOC6xxalwEsw9c2QQeAIftl+Vegovlnee1c9QX4TctnWMn13TZye+giMm8e2LwA==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <!-- Select2 -->
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    
    {if $mtid}
      <!-- monaco -->
      <script type="text/javascript" src="{$vs}/loader.js"></script>
      <script type="text/javascript" src="{$vs}/editor/editor.main.nls.js"></script>
      <script type="text/javascript" src="{$vs}/editor/editor.main.js"></script>
    {/if}
  </head>
  <body>
    <!-- the toaster -->
    <div id="toaster" class="toast-container position-fixed top-0 end-0 p-3"></div>
    <!-- the prompter -->
    <div id="prompter" class="modal" tabindex="-1">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header bg-body-secondary">
            <h1 class="modal-title h5"></h1>
          </div>
          <div class="modal-body">
            <label for="prompter-value" class="form-label"></label>
            <input type="text" class="form-control" id="prompter-value" />
          </div>
          <div class="modal-footer bg-body-tertiary">
            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal"></button>
            <button type="button" class="btn btn-primary"></button>
          </div>
        </div>
      </div>
    </div>
    {if $mtid}
      <!-- translator -->
      <div id="translator" class="modal" tabindex="-1">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header bg-body-secondary">
              <h1 class="modal-title h5">{ts}Add Translation{/ts}</h1>
            </div>
            <div class="modal-body">
              <label for="languages">{ts}Locale{/ts}</label>
              <select id="languages" class="form-select" tabindex="0">
                <option value="">{ts} - select -{/ts}</option>
                {foreach from=$langs key=_lang item=_name}
                  <option value="{$_lang}">{$_name}</option>
                {/foreach}
              </select>
            </div>
            <div class="modal-footer bg-body-tertiary">
              <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">{ts}Cancel{/ts}</button>
              <button type="button" class="btn btn-primary">{ts}Okay{/ts}</button>
            </div>
          </div>
        </div>
      </div>
      <!-- tokenator -->
      <div id="tokenator" class="modal" tabindex="-1">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header bg-body-secondary">
              <h1 class="modal-title h5">{ts}Insert Token{/ts}</h1>
            </div>
            <div class="modal-body">
              <form id="insert-token" class="bg-body-secondary p-2 rounded border border-secondary">
                <select id="tokens" class="form-select d-none">
                  <option value="" data-icon="fa-code" class="-d-none">{ts}Tokens{/ts}</option>
                  {foreach from=$tokens item=token}
                    <optgroup label="{$token.text}">
                      {foreach from=$token.children item=field}
                        <option value="{$field.id}">{$field.text}</option>
                      {/foreach}
                    </optgroup>
                  {/foreach}
                </select>
              </form>
            </div>
          </div>
        </div>
      </div>
    {/if}
    <!-- the editor -->
    <div class="container-fluid position-fixed">
      <div id="btrmsgtpl" class="card" style="height:100vh">
        <div class="card-header text-bg-secondary">
          Better Message Templates
          {if $mtid}
            <span class="mx-2">|</span> {ts}System Workflow:{/ts} { $title }
          {/if}
          <span id="closer" class="float-end ms-5" data-bs-toggle="tooltip" data-bs-trigger="hover" data-bs-title="{if $mtid}{ts}Close Editor{/ts}{else}{ts}Return To CiviCRM{/ts}{/if}">
            <i class="fa-solid fa-times"></i>
          </span>
          <span id="is-modified" class="float-end">
            <i class="fa-solid fa-exclamation-triangle mx-3"></i>
            {ts}MODIFIED{/ts}
            <i class="fa-solid fa-exclamation-triangle mx-3"></i>
          </span>
        </div>
        <div class="card-body p-0 bg-body-secondary">
          {if $mtid}
            {include file='CRM/Btrmsgtpl/Page/editor.tpl'}
          {else}
            {include file='CRM/Btrmsgtpl/Page/index.tpl'}
          {/if}
        </div>
      </div>
    </div>
    <!-- our vars -->
    <div id="vars" class="d-none">{$vars}</div>
    <!-- our scripts -->
    <script type="text/javascript" src="{$js}/common.js?r={$resourceCacheCode}"></script>
    {if $mtid}
      <script type="text/javascript" src="{$js}/monaco.js?r={$resourceCacheCode}"></script>
      <script type="text/javascript" src="{$js}/language.js?r={$resourceCacheCode}"></script>
      <script type="text/javascript" src="{$js}/cymbal.js?r={$resourceCacheCode}"></script>
      <script type="text/javascript" src="{$js}/outline.js?r={$resourceCacheCode}"></script>
      <script type="text/javascript" src="{$js}/preview.js?r={$resourceCacheCode}"></script>
      <script type="text/javascript" src="{$js}/compare.js?r={$resourceCacheCode}"></script>
      <script type="text/javascript" src="{$js}/init.js?r={$resourceCacheCode}"></script>
    {else}
      <script type="text/javascript" src="{$js}/index.js?r={$resourceCacheCode}"></script>
    {/if}
    <!-- our styles -->
    <link href="{$css}/so_btrmsgtpl.css?r={$resourceCacheCode}" rel="stylesheet" />

  </body>
</html>
