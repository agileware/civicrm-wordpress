(function(_event, _language) {

  // import needed vars
  const { tpl, warnings } = btrmsgtpl.vars;

  // outline actions
  const OA = btrmsgtpl.actions('outline-actions');

  const _cymbal = {};

  _cymbal.$ = $('#symbols');
  _cymbal.symbols = [];

  _cymbal._defaulting = false;
  _cymbal.defaulting = function(val) {
    _cymbal._defaulting = val;
  }

  _cymbal._id =  1;

  _cymbal._parsing = false;
  _cymbal.parsing = function(val) {
    _cymbal._parsing = val;
  }

  _cymbal.addSymbol = function(name, smarty) {
    const type = name[0] === '$' ? 'var' : (name[0] === '#' ? 'prop' : 'token');

    let sym = this.find('name', name);

    if (!sym) {
      const id = this._id++;
      sym = new Proxy({
        $: $('<option>').text(name).val(id),
        id,
        name,
        type: (
          type === 'token' ? 'token' : (
            smarty || name === '$smarty' ? 'smarty' : (
              name === '$config' ? 'known' : 'var'
            )
          )
        ),
        isRef: false,
        isDef: false
      }, {
        set: function (target, key, value) {
          setTimeout(() => _cymbal.$.trigger('change.select2'), 100);
          target[key] = value;
          return true;
        }
      });
      sym.$.data('symbol', sym);
      this.symbols.push(sym);
      this.symbols.sort((a, b) => {
        const an = a.name[0] === '$' ? a.name.substr(1) : a.name;
        const bn = b.name[0] === '$' ? b.name.substr(1) : b.name;
        return an.localeCompare(bn);
      });

      const i = this.symbols.findIndex((_sym) => _sym === sym);
      if (i === 0) {
        sym.$.prependTo(this.$);
      }
      else {
        sym.$.insertAfter(this.$.find(`option:nth-child(${i})`));
      }
      sym.isRef = this._parsing;
      sym.isDef = this._defaulting;

      if (this._parsing && warnings.hasOwnProperty(tpl.workflow_name) && warnings[tpl.workflow_name].hasOwnProperty(sym.name)) {
        btrmsgtpl.alert(ts('The token <code>%1</code> has been replaced by <code>%2</code>.', {
          1: sym.name,
          2: warnings[tpl.workflow_name][sym.name]
        }), ts('Outdated Token Warning'));
      }
    }
    else {
      if (!sym.isRef && this._parsing) {
        sym.isRef = true;
      }
    }
    return sym;
  };

  _cymbal.empty = function() {
    this.$.empty().append($('<option>'));
    this.symbols = [];
  }

  _cymbal.find = function(field, val) {
    const sym = this.symbols.find((_sym) => _sym[field] === val);
    return sym ? sym : false;
  }

  _cymbal.list = function() {
    return this.symbols.map((sym) => sym.name);
  }

  _cymbal.remove = function(sym) {
    if (sym === this.val()) {
      this.val(null, true);
    }
    const i = this.symbols.findIndex((_sym) => _sym === sym);
    if (i !== -1) {
      this.symbols.splice(i, 1);
      sym.$.remove();
    }
  }

  _cymbal.removeUnreferenced = function() {
    this.symbols = this.symbols.filter((_sym) => {
      if (!_sym.isRef) {
        _sym.$.remove();
        return false;
      }
      return true;
    });
  }

  _cymbal.val = function(val, trigger) {
    if (typeof val !== 'undefined') {
      this.$.val(val);
      if (trigger) {
        this.$.trigger('change');
      }
    }
    else {
      return this.find('id', parseInt(this.$.val()));
    }
  }

  _cymbal.$.select2({
    allowClear: true,
    dropdownAutoWidth: true,
    placeholder: '{ $ }',
    width: 'unset',
    templateResult: function(state) {
      const $el = $(state.element);
      const sym = $el.data('symbol');

      if (sym) {
        const $div = $('<div>').text(state.text);
        $ind = $('<div>').addClass('float-end').appendTo($div);
        if (sym.type === 'smarty') {
          $('<i>').addClass('fa-solid fa-gear ms-1').appendTo($ind);
        }
        if (!sym.isRef) {
          $('<i>').addClass('fa-solid fa-eye-slash ms-1').appendTo($ind);
        }
        if (!sym.isDef && sym.type !== 'token') {
          $('<i>').addClass('fa-solid fa-exclamation-triangle ms-1').appendTo($ind);
        }
        return $div;
      }
      return state.text;
    },
    templateSelection: function(state) {
      const $el = $(state.element);
      const sym = $el.data('symbol');

      if (sym) {
        const $div = $('<div>').text(state.text);
        $ind = $('<div>').addClass('ms-3 float-end').appendTo($div);
        if (sym.type === 'smarty') {
          new bootstrap.Tooltip(
            $('<i>')
              .addClass('fa-solid fa-gear ms-1')
              .attr({
                'data-bs-toggle': 'tooltip',
                'data-bs-trigger': 'hover',
                'data-bs-title': ts('Assigned by Smarty')
              })
              .appendTo($ind)
          );
        }
        if (!sym.isRef) {
          new bootstrap.Tooltip(
            $('<i>')
              .addClass('fa-solid fa-eye-slash ms-1')
              .attr({
                'data-bs-toggle': 'tooltip',
                'data-bs-trigger': 'hover',
                'data-bs-title': ts('Not referenced by the template')
              })
              .appendTo($ind)
          );
        }
        if (!sym.isDef && sym.type !== 'token') {
          new bootstrap.Tooltip(
            $('<i>')
              .addClass('fa-solid fa-exclamation-triangle ms-1')
              .css('color', 'firebrick')
              .attr({
                'data-bs-toggle': 'tooltip',
                'data-bs-trigger': 'hover',
                'data-bs-title': ts('Likely deprecated (should not use)')
              })
              .appendTo($ind)
          );
        }
        return $div;
      }
      return state.text;
    }
  })
    .on('change', (evt) => {
      const sym = _cymbal.val();

      if (sym === false) {
        bootstrap.Tooltip.getInstance(_cymbal.$.parent()).hide();
      }
      _event.trigger('symbol_select', [ sym ]);
    })
    .on('change.select2', (evt) => {
      $('#select2-symbols-container').removeAttr('title');
    });

  btrmsgtpl._cymbal = _cymbal;
    
}(btrmsgtpl._event, btrmsgtpl._language));