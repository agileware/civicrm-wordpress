(function() {

  $('#btrmsgtpl').find('.card-body').first().css('overflow', 'clip auto');

  $('[data-bs-toggle="tooltip"]').each(function() {
    new bootstrap.Tooltip(this);
  });

  $('.crm-i').removeClass('.crm-i').addClass('fa-solid'); // need for token warning

  $('#closer').on('click', (evt) => { window.location = btrmsgtpl.vars.return_url });
  
  let search = false;

  const $msgTemplateId = $('#btrmsgtpl-id')
    .select2({
      theme: 'bootstrap-5',
      placeholder: '',
      matcher: function(params, data) {
        if (!data.hasOwnProperty('children')) {
          return data;
        }
        const filtered = data.children.filter((item) => {
          if (params.term) {
            if (item.text.toLowerCase().indexOf(params.term.toLowerCase()) === -1) {
              return false;
            }
          }
          return $(item.element).is('.d-none') ? false : true;
        });
        if (filtered.length === data.children.length) {
          return data;
        }
        const newData = $.extend({}, data, true);
        newData.children = filtered;

        return newData;
      },
      templateSelection: function(state) {
        if (state.id === '') {
          if (search === false) {
            return ts('...or just select a message to edit');
          }
          else {
            return ts('search results (%1)', { 1: search });
          }
        }
        return state.text;
      },
      templateResult: function(state) {
        if (state.hasOwnProperty('children')) {
          return $(`<span>${state.text}</span>`).css({
            'font-weight': 'bold',
            color: 'black'
          });
        }
        const $el = $(state.element);
        if ($el.is('.user') || !$el.is('.important')) {
          return state.text;
        }
        return $(`<span class="important" style="white-space:nowrap;">${state.text}</span><div class="description important">${$el.data('important')}</div>`);
      }
    })
    .change((evt) => {
      let url = btrmsgtpl.vars.system_workflow;
      const $opt = $msgTemplateId.find(':selected');
      if ($opt.is('.user')) {
        url = btrmsgtpl.vars.user_driven;
      }
      url = url.replaceAll('&amp;', '&').replace('id=id', `id=${$opt.val()}`);
      if ($opt.is('.user')) {
        window.open(url);
        $msgTemplateId.val(null).trigger('change.select2');
      }
      else {
        window.location = url;
      }
    })
    .on('change.select2', (evt) => {
      $('#select2-btrmsgtpl-id-container').removeAttr('title');
    })
    .trigger('change.select2')
    .on('select2:opening', () => {
      // start off showing all
      $msgTemplateOptions.removeClass('d-none');
      // filter search results
      if (search !== false) {
        $msgTemplateOptions.not('.search-result').addClass('d-none');
      }
      // filter important
      if (btrmsgtpl.pref('important', false)) {
        $msgTemplateOptions.not('.important').addClass('d-none');
      }
    });
  const $msgTemplateOptions = $msgTemplateId.find('option');

  const $search = $('#btrmsgtpl-search')
    .keydown((evt) => {
      if (evt.keyCode === 13) {
        btrmsgtpl.stopPrevent(evt);
        const $busy = btrmsgtpl.alert(ts('Searching...'), '', 'indeterminate');

        btrmsgtpl.api3('Btrmsgtpl', 'search', {
          needle : $search.val().replace(/(%|_)/g, '\\$1')
        })
          .then((result) => {
            $busy.remove();
            search = result.ids.length;

            $msgTemplateOptions.removeClass('search-result').each(function() {
              const $opt = $(this);
              if (result.ids.indexOf(parseInt($opt.val())) !== -1) {
                $opt.addClass('search-result');
              }
            });
            $msgTemplateId.trigger('change.select2').select2('open');
          });
      }
      if (evt.keyCode === 27) {
        btrmsgtpl.stopPrevent(evt);

        search = false;
        $search.val('');
        $msgTemplateOptions.removeClass('.search-result');
        $msgTemplateId.trigger('change.select2');
      }
    });


  $('#pref-important')
    .prop('checked', btrmsgtpl.pref('important'))
    .change((evt) => btrmsgtpl.pref('important', $(evt.target).prop('checked'), true));
  $('#pref-font-size')
    .val(btrmsgtpl.pref('font-size') || 12)
    .change((evt) => btrmsgtpl.pref('font-size', $(evt.target).val(), true));
  $('#pref-default-tool')
    .val(btrmsgtpl.pref('default-tool', 'outline'))
    .change((evt) => btrmsgtpl.pref('default-tool', $(evt.target).val(), true));
  $('#pref-tool-side')
    .val(btrmsgtpl.pref('tool-side', 'right'))
    .change((evt) => btrmsgtpl.pref('tool-side', $(evt.target).val(), true));
  $('#pref-wordwrap')
    .val(btrmsgtpl.pref('wordwrap') || '')
    .change((evt) => btrmsgtpl.pref('wordwrap', $(evt.target).val(), true));

    $('.btrmsgtpl-setting select')
      .change((evt) => {
        const setting = $(evt.target);
        const params = {};
        params['btrmsgtpl_' + setting.attr('id').substr(8).replaceAll('-', '_')] = setting.val();
        btrmsgtpl.api3('setting', 'create', params)
          .then((result) => {
            if (result.is_error === 1) {
              btrmsgtpl.alert(result.error_message, ts('Danger, Will Robinson!'), 'error');
            }
          });
      });

}());