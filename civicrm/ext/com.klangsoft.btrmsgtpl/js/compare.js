(function(_event, _language) {

  let __lang = null;
  let __format = null;

  const { tpl } = btrmsgtpl.vars;

  // compare actions
  const CA = btrmsgtpl.actions('compare-actions');

  // show info about the highlighted revision
  const $defaultInfo = $('#default-info').addClass('d-none');
  
  // current revision
  const $revision = $('#revision');
  let _revision = null;

  // container for the monaco editor
  const $compare = $('#compare');

  // monaco instance to the diff editor
  let _monaco = null;

  let _didModifyThisTime = false;
  
  function initDiffEditor() {
    if (!_monaco) {
      btrmsgtpl._differ = _monaco = monaco.editor.createDiffEditor($compare[0], {
        fontSize: btrmsgtpl.pref('font-size', 12),
        lineNumbersMinsChars: 3,
        renderSideBySide: false
      });
      _monaco.onDidUpdateDiff(() => {
        const changes = _monaco.getLineChanges();
        if (changes) {
          $('#diff-count').text(`${changes.length} differences`);
          $diffPrev.prop('disabled', changes.length === 0);
          $diffNext.prop('disabled', changes.length === 0);
        }
      });
    }
  }

  function loadSystemDefaults() {
    if (!_haveLoadedDefaults) {
      const $busy = CA._busy(ts('Loading system defaults...'));

      btrmsgtpl.api3('btrmsgtpl', 'revision', {
        msg_template_id: tpl.id,
        action: 'defaults'
      })
        .then((result) => {
          CA._busy($busy);
          _haveLoadedDefaults = true;

          if (result.is_error === 0) {
            if (result.is_info === 1) {
              btrmsgtpl.alert(result.info_message, ts('System Defaults'), 'info');
            }
            if (result.defaults.length > tpl.defaults.length) {
              tpl.defaults = result.defaults;

              const { content } = _language.val();
              let { rev } = revision();
              if (!rev) {
                rev = result.defaults[0];
              }
              if (result.is_info !== 1) {
                btrmsgtpl.alert(ts('System defaults loaded.'), '', 'info', { expires : 3000 });
              }
              revisions(content.revisions, rev.id);
            }
          }
          else {
            btrmsgtpl.error(result.error_message);
          }
        });
    }
  }

  /**
   * Get currently selected revision.
   * 
   * @returns {object} Revision information.
   */
  function revision() {
    const $opt = $revision.find('option:selected');
    return {
      rid: $revision.val(),
      rev: $opt.data('revision'),
      $opt
    };
  }

  /**
   * Build revisions list and make a selection.
   * 
   * @param {array} revs List of revisions.
   * @param {string} val Value of the revision to select. If not specified, first will be selected.
   */
  function revisions(revs, val) {
    let revisions = [];
    if (revs) {
      revisions = revs.concat(tpl.defaults);
      revisions.sort((a, b) => b.ts - a.ts);

      let $dopt = null;
      let notes = '';
      
      $revision.empty();
      revisions.forEach((rev) => {
        if ($dopt) { // previous rev was a system default
          if (rev.hash === false) { // transition from system default to revision
            $dopt.attr('data-diff-notes', notes.trim().replaceAll('\n', '<br />'));
            $dopt = notes = null;
          }
          else { // consecutive system defaults, combine notes
            notes += `${rev.alias}<hr />${rev.notes}\n\n`;
          }
        }
        if (!$dopt) {
          const $opt = $('<option>')
            .val(rev.id)
            .text(rev.name)
            .attr('data-git-hash', rev.hash)
            .appendTo($revision)
            .data('revision', rev);

          if (rev.hash !== false) {
            $dopt = $opt;
            notes = `${rev.alias}<hr />${rev.notes}\n\n`;
          }
          else if (rev.alias) {
            $opt.attr('data-diff-notes', `${rev.alias}<hr />${rev.name}`);
          }
          else {
            $opt.attr('data-diff-notes', rev.name);
          }
        }
      });
      if ($dopt) {
        $dopt.attr('data-diff-notes', notes.trim().replaceAll('\n', '<br />'));
      }
    }

    if (!$revision.hasClass('select2-hidden-accessible')) {
      $revision.select2({
        minimumResultsForSearch: 8,
        dropdownAutoWidth: true,
        dropdownCssClass: 'bg-body-tertiary',
        width: 'unset',
        templateResult: function(state) {
          const $el = $(state.element);
          const hash = $el.data('git-hash');
          const notes = $el.data('diff-notes');
  
          let t = state.text;
          if (hash) {
            t = `<strong>${t}</strong>`;
          }
          return $('<div>')
            .html(t)
            .data('diff-notes', notes)
            .on('mouseenter', (evt) => {
              $defaultInfo.html($(evt.currentTarget).data('diff-notes'));
            })
          ;
        }
      })
        .on('select2:open', (evt) => {
          // without the timeout, things don't line up correctly after first dropdown
          setTimeout(() => {
            const $results = $('.select2-results');
            const roff = $results.offset();
            const rw = $results.width();
            const rh = $results.height();
  
            $defaultInfo.removeClass('d-none')
              .css('left', '0px')
              .css('width',(roff.left - 14) + 'px')
              .css('height', (rh + 44) + 'px');
          }, 100);
        })
        .on('select2:close', () => {
          $defaultInfo.addClass('d-none');
        });
    }

    if (!val && revisions.length > 0) {
      val = revisions[0].id;
    }
    if (val) {
      $revision.val(val).trigger('change');
    }
  }

  // side-by-side
  const $sbs = $('#side-by-side').change(() => _monaco.updateOptions({ renderSideBySide: $sbs.prop('checked') }));

  // prev/next difference
  const $diffPrev = $('#diff-prev').click(() => _monaco.goToDiff('previous'));
  const $diffNext = $('#diff-next').click(() => _monaco.goToDiff('next'));

  // lazy load system defaults
  let _haveLoadedDefaults = false;

  /******************
   * EVENT HANDLERS *
   ******************/

  /**
   * action_revision_delete
   */
  _event.on('action_revision_delete', (evt, actions) => {
    const { rev, $opt } = revision();
    const i = rev.name.lastIndexOf(' by ');
    const before = rev.name.substr(0, i) + (rev.alias ? ` (${rev.alias})` : '');

    btrmsgtpl.confirm(ts('Delete Revision?'), `<code>${before}</code>`, {
      primary: ts('Yes'),
      secondary: ts('No')
    })
      .then((confirm) => {
        if (confirm) {
          const $busy = CA._busy(ts('Deleting revision...'));

          btrmsgtpl.api3('btrmsgtpl', 'revision', {
            action: 'delete',
            revision_id: rev.id
          })
            .then((result) => {
              CA._busy($busy);

              if (result.is_error === 0) {
                $opt.remove();
                $revision.trigger('change');

                btrmsgtpl.success(ts('Revision has been deleted.'));
              }
              else {
                btrmsgtpl.error(result.error_message);
              }
            });
        }
      });
  });
  /**
   * action_revision_refresh
   */
  _event.on('action_revision_refresh', (evt, actions) => {
    const $busy = CA._busy(ts('Refreshing revisions...'));

    const { lang, content } = _language.val();
    
    btrmsgtpl.api3('btrmsgtpl', 'revision', {
      action: 'refresh',
      msg_template_id: tpl.id,
      lang: lang.substr(0, 5)
    })
      .then((result) => {
        CA._busy($busy);

        if (result.is_error === 0) {
          if (result.revisions.length > content.revisions.length) {
            const { rev } = revision();
            content.revisions = result.revisions;
            revisions(result.revisions, rev.id);

            btrmsgtpl.success(ts('Revisions have been updated.'));
          }
          else {
            btrmsgtpl.success(ts('Revisions are up to date.'));
          }
        }
        else {
          btrmsgtpl.error(result.error_message);
        }
      });
  });
  /**
   * action_revision_rename
   */
  _event.on('action_revision_rename', (evt, actions) => {
    const { rev } = revision();
    const i = rev.name.lastIndexOf(' by ');
    const before = rev.name.substr(0, i);

    btrmsgtpl.prompt(ts('Rename Revision'), ts('Enter a new name for the revision'), before)
      .then((alias) => {
        if (alias && alias.localeCompare(before) !== 0) {
          const $busy = CA._busy(ts('Renaming revision...'));
          
          btrmsgtpl.api3('btrmsgtpl', 'revision', {
            action: 'rename',
            revision_id: rev.id,
            alias
          })
            .then((result) => {
              CA._busy($busy);

              if (result.is_error === 0) {
                if (rev.alias === false) {
                  rev.alias = before;
                }
                rev.name = alias.concat(rev.name.substr(i));

                const { content } = _language.val();
                revisions(content.revisions, rev.id);

                btrmsgtpl.success(ts('Revision has been renamed.'));
              }
              else {
                btrmsgtpl.error(result.error_message);
              }
            });
        }
      });
  });
  /**
   * action_revision_revert
   */
  _event.on('action_revision_revert', (evt, actions) => {
    if (_revision) {
      const { content } = _language.val();
      content.msg_html.setValue(_revision.msg_html.getValue());
      content.msg_text.setValue(_revision.msg_text.getValue());
      content.msg_subject.setValue(_revision.msg_subject.getValue());

      const { rev } = revision();

      btrmsgtpl.success(ts('<code>%1</code> has been modified to match <code>%2</code>.', {
        1: content.language,
        2: rev.name
      }));
    }
  });
  /**
   * btrmsgtpl_resize
   */
  _event.on('btrmsgtpl_resize', (evt, height) => {
    $compare.height(height - $compare.position().top - 16);
    if (_monaco) {
      _monaco.layout();
    }
  });
  /**
   * format_select
   */
  _event.on('format_select', (evt, format, model) => {
    if (btrmsgtpl.tool() !== 'compare' || !_monaco || !_revision || format === __format) {
      return;
    }
    __format = format;

    _monaco.setModel({
      original: _revision[format],
      modified: model
    });
  });
  /**
   * language_select
   */
  _event.on('language_select', (evt, lang, content) => {
    if (btrmsgtpl.tool() !== 'compare' || !lang || lang === __lang) {
      return;
    }
    __lang = lang;
    __format = null;

    revisions(content.revisions);
  });
  /**
   * monaco_modified
   */
  _event.on('monaco_modified', (evt, editor) => {
    _didModifyThisTime = true;
  });
  /**
   * $revision.change
   */
  $revision.on('change', (evt) => {
    if (btrmsgtpl.tool() !== 'compare') {
      return;
    }

    const { rid } = revision();
    if (_revision !== null) {
      if (_revision.rid === rid) {
        return;
      }
      _monaco.setModel(null);
      _revision.msg_html.dispose();
      _revision.msg_text.dispose();
      _revision.msg_subject.dispose();
      _revision = null;
    }

    if (rid) {
      const $busy = CA._busy(ts('Loading revision...'));
  
      btrmsgtpl.api3('btrmsgtpl', 'revision', {
        msg_template_id: tpl.id,
        revision_id: rid
      })
        .then((result) => {
          CA._busy($busy);
  
          if (result.is_error === '0') {
            if (rid === result.rid) {
              _revision = {
                rid,
                git_hash: result.git_hash,
                msg_html: monaco.editor.createModel(result.msg_html, 'html'),
                msg_text: monaco.editor.createModel(result.msg_text, 'text'),
                msg_subject: monaco.editor.createModel(result.msg_subject, 'text'),
              };
              __format = null;
              btrmsgtpl.$format.trigger('change');
  
              CA._disable('revision_delete', _revision.git_hash.length > 0)
                ._disable('revision_rename', _revision.git_hash.length > 0);
            }
          }
          else {
            btrmsgtpl.error(result.error_message);
          }
          $('#select2-revision-container').removeAttr('title');
        });
    }
  });
  /**
   * template_saved
   */
  _event.on('template_saved', (evt, content) => {
    const { rid } = revision();
    revisions(content.revisions, rid);
  });
  /**
   * tool_deselect
   */
  _event.on('tool_deselect', (evt, tool) => {
    if (tool === 'compare') {
      _event.trigger('tool_select', ['monaco']);

      if (_didModifyThisTime) {
        _event.trigger('monaco_refresh', [ btrmsgtpl._editor ]);
      }
    }
  });
  /**
   * tool_select
   */
  _event.on('tool_select', (evt, tool) => {
    if (tool === 'compare') {
      // hide message pane
      _event.trigger('tool_deselect', ['monaco']);
      _didModifyThisTime = false;

      // these things happen first time around
      initDiffEditor();
      loadSystemDefaults();
      if (!_revision) {
        $revision.trigger('change');
      }
    }
  });

}(btrmsgtpl._event, btrmsgtpl._language));