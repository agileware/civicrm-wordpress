(function() {

  /**
   * Queen Bee
   * @namespace btrmsgtpl
   */

  /*** vars passed in from server are based64+json encoded ***/
  const $_vars = $('#vars');
  /**
   * @memberof btrmsgtpl
   * @property {object} vars Like CRM.vars, our variables passed from server.
   */
  btrmsgtpl.vars = JSON.parse(atob($_vars.text()));

  const _event = {
    $: $_vars.text(''), // re-purpose empty node for comms
    _queue: []
  };
  _event.on = function(event, handler) {
    this.$.on(event, handler);
  }
  _event.trigger = function(event, params, immediate) {
    if (immediate) {
      this.$.trigger(event, params);
    }
    else {
      // clear out an existing event in the queue
      for (let i = 0; i < this._queue.length; i++) {
        if (this._queue[i].event === event) {
          this._queue.splice(i, 1);
          break; 
        }
      }
      // then add new event to end of queue
      this._queue.push({ event, params });

      // if there is more than one event queued, it's already being processed
      if (this._queue.length > 1) {
        return;
      }
      // process all events in the queue
      while (this._queue.length > 0) {
        this.$.trigger(this._queue[0].event, this._queue[0].params);
        this._queue.shift();
      }
    }
  }
  /**
   * @memberof btrmsgtpl
   * @property {jQuery} _event Event bus.
   */
  btrmsgtpl._event = _event;
  
  /******
   * js *
   ******/
  /**
   * TWEAKED FROM Common.js
   * 
   * Short-named function for string translation, defined in global scope so it's available everywhere.
   *
   * @param text string for translating
   * @param params object key:value of additional parameters
   *
   * @return string
   */
  this.ts = function(text, params) {
    if (btrmsgtpl.vars.strings.hasOwnProperty(text)) {
      text = btrmsgtpl.vars.strings[text];
    }

    if (typeof(params) === 'object') {
      for (var i in params) {
        if (typeof(params[i]) === 'string' || typeof(params[i]) === 'number') {
          // sprintf emulation: escape % characters in the replacements to avoid conflicts
          text = text.replace(new RegExp('%' + i, 'g'), String(params[i]).replace(/%/g, '%-crmescaped-'));
        }
      }
      return text.replace(/%-crmescaped-/g, '%');
    }
    return text;
  }

  /***********
   * actions *
   ***********/
  /**
   * @memberof btrmsgtpl
   * @private
   * @property {object} _actions Collection of action controls.
   */
  btrmsgtpl._actions = {};
  /**
   * Result/selection formatter for actions.
   * 
   * @memberof btrmsgtpl
   * @private
   * @see {@link https://select2.org/dropdown}
   * @see {@link https://select2.org/selections}
   */
  btrmsgtpl._actionFormat = function(state, hr, placeholder) {
    if (hr && state.text === '-') {
      return $('<hr class="separator" />');
    }
    const $el = $(state.element);
    if (!placeholder && $el.val() === '') {
      return null;
    }
    const icon = $el.data('icon');
    const mod = $el.hasClass('modified');

    return $(`<span class="${mod ? 'modified' : ''}"><i class="me-1 ${icon}"></i> ${state.text}</span>`);
  };
  /**
   * Create or get action control.
   * 
   * @memberof btrmsgtpl
   * @param {string} id DOM ID of the action control.
   * @returns {object} The requested action control.
   * @see _actions
   */
  btrmsgtpl.actions = function(id) {
    if (!this._actions.hasOwnProperty(id)) {
      /**
       * Actions control as returned by <em>btrmsgtpl.actions(id)</em>,
       * used to manipulate items of the control.
       * 
       * @namespace actions
       */
      const actions = {
        /**
         * @memberof actions
         * @property {string} id DOM ID of the action control.
         */
        id,
        /**
         * @memberof actions
         * @private
         * @property {jQuery} $ Action control.
         */
        $: $(`select#${id}`),
        /**
         * @memberof actions
         * @private
         * @property {jQuery} $alert Alert in progress.
         */
        $alert: null,
        /**
         * Display a message while busy working/loading/whatever.
         * 
         * @memberof actions
         * @param {string|jQuery} message Message to display while busy, or return from previous call to dismiss.
         * @returns {jQuery|void} Busy alert. Pass back in when done.
         */
        _busy: function(message) {
          if (typeof message === 'string') {
            const $busy = btrmsgtpl.alert(message, '', 'indeterminate');
            this.$.data('brc', (this.$.data('brc') || 0) + 1)
              .prop('disabled', true);

            return $busy;
          }
          else {
            message.remove();
            const brc = (this.$.data('brc') || 1) - 1;
            this.$.data('brc', brc).prop('disabled', brc > 0);
          }
        },
        /**
         * Disable an action.
         * 
         * @memberof actions
         * @param {string} action Action to affect.
         * @param {bool} disabled Whether this action should be disabled.
         * @returns {object} this
         */
        _disable: function(action, disabled) {
          if (this.hasOwnProperty(action)) {
            this[action].prop('disabled', disabled);
          }
          return this;
        },
        /**
         * Mark action modified.
         * While any action is modified, the whole action control is marked as such.
         * 
         * @memberof actions
         * @param {string} action Action to affect.
         * @param {bool} modified Whether this action should be marked modified.
         * @returns {object} this
         */
        _modify: function(action, modified) {
          if (this.hasOwnProperty(action)) {
            this[action][modified ? 'addClass' : 'removeClass']('modified');
            this.$ph[this.$.find('.action.modified').length > 0 ? 'addClass' : 'removeClass']('modified');
            this.$.trigger('change.select2');
            $(`#select2-${this.id}-container`).removeAttr('title');
          }
          return this;
        },
      };
      actions.$.find('option').each(function() {
        const $opt = $(this);
        const val = $opt.val();
        if (val === '-') {
          $opt.prop('disabled', true);
          return;
        }
        $opt.addClass('action');
        actions[val] = $opt;
      });

      const ph = actions.$.attr('placeholder');
      /**
       * @memberof actions
       * @private
       * @property {jQuery} $ph Placeholder option.
       */
      let $ph = actions.$ph = $('<option>')
        .data('icon', 'fa-solid fa-bolt-lightning')
        .text(ph ? ph : ts('Action'))
        .val('')
        .attr('selected', true)
        .prependTo(actions.$);

      actions.$.select2({
        dropdownAutoWidth: true,
        dropdownCssClass: ':all:',
        minimumResultsForSearch: -1,
        templateResult: function(state) {
          return btrmsgtpl._actionFormat(state, true);
        },
        templateSelection: function(state) {
          return btrmsgtpl._actionFormat(state, false, true);
        }
      })
        .on('change', actions, (evt) => {
          const a = evt.data;
          const v = a.$.val();
          if (v) {
            console.log('triggering', v);
            /**
             * An action was selected from an action control.
             * 
             * @event action
             * @param {object} actions Action control initiated from.
             * @param {string} action Action to take. e.g. <em>template_save</em>.
             */
            this._event.trigger('action', [ a, v ]);
            /**
             * [action] was selected from an action control. e.g. <em>action_template_save</em>.
             * 
             * @event action_[action]
             * @param {object} actions Action control initiated from.
             */
            this._event.trigger(`action_${v}`, [ a ]);
            a.$.val('').trigger('change');
          }
          $(`#select2-${a.id}-container`).removeAttr('title');
        });
      actions.$.trigger('change');
      
      this._actions[id] = actions;
    }
    return this._actions[id];
  }

  // initialize all actions
  $('select.actions').each(function() {
    btrmsgtpl.actions($(this).attr('id'));
  });

  /*********
   * alert *
   *********/
  /**
   * @memberof btrmsgtpl
   * @private
   * @property {jQuery} $toaster Alert (toast) container.
   */
  btrmsgtpl.$toaster = $('#toaster');
  /**
   * @memberof btrmsgtpl
   * @private
   * @property {int} _toastId Next ID to assign.
   */
  btrmsgtpl._toastId = 1;
  /**
   * Replacement for CRM.alert.
   * <em>options.unique</em> ignored. No default for <em>options.expires</em>.
   * 
   * @memberof btrmsgtpl
   * @param {string} message The message to display.
   * @param {string} title Title for the alert.
   * @param {string} type alert | error | info | success
   * @param {object} options <em>options.expires</em> - Auto-dismiss in ms. 0 to leave open.
   * @returns {jquery} $ to the created alert. Can be used to remove an indeterminate alert.
   * @see {@link https://docs.civicrm.org/dev/en/latest/framework/ui/#popup-notifications}
   */
  btrmsgtpl.alert = function(message, title, type, options) {
    const ic = {
      alert: { icon: 'triangle-exclamation', color: 'orange' },
      error: { icon: 'bomb', color: 'firebrick' },
      indeterminate: { icon: 'gear', color: 'royalblue'},
      info: { icon: 'circle-info', color: 'royalblue' },
      success: { icon: 'check', color: 'forestgreen' }
    };
    title = title || '';
    type = type && ic.hasOwnProperty(type) ? type : 'alert';
    options = options || {};

    // remove dismissed alerts from the dom
    btrmsgtpl.$toaster.find('.toast.hide').remove();

    // build a new alert
    const $toast = $('<div>')
      .addClass('toast')
      .attr('id', `toast-${btrmsgtpl._toastId++}`)
      .appendTo(btrmsgtpl.$toaster);
    
    if (type !== 'indeterminate' && options.hasOwnProperty('expires') && options.expires > 0) {
      $toast.attr('data-bs-delay', options.expires);
    }
    else {
      $toast.attr('data-bs-autohide', 'false');
    }

    if (title.length > 0) {
      $header = $('<div>')
        .addClass('toast-header bg-body-tertiary')
        .html(`<span class="me-auto">${title}<span>`)
        .appendTo($toast);
      $('<i>')
        .addClass(`me-2 fa-solid fa-${ic[type].icon}`)
        .css('color', ic[type].color)
        .prependTo($header);
      if (type !== 'indeterminate') {
        $('<button>')
          .addClass('btn-close')
          .attr({
            type: 'button',
            'data-bs-dismiss': 'toast'
          })
          .appendTo($header);
      }
    }
    const $body = $('<div>')
      .addClass('toast-body')
      .html(message)
      .appendTo($toast);

    if (type === 'indeterminate') {
      $('<i>')
        .addClass(`me-2 fa-solid fa-spin fa-${ic[type].icon}`)
        .css('color', ic[type].color)
        .prependTo($body);
    }
    else if (title.length === 0) {
      $('<i>')
        .addClass(`me-2 fa-solid fa-${ic[type].icon}`)
        .css('color', ic[type].color)
        .prependTo($body);
    }

    // display it
    bootstrap.Toast.getOrCreateInstance($toast[0]).show();

    return $toast;
  }

  /********
   * api3 *
   ********/
  /**
   * Replacement for CRM.api3.
   * 
   * @memberof btrmsgtpl
   * @param {string} entity CiviCRM entity.
   * @param {string} action API action.
   * @param {object} params Parameters appropriate for call.
   * @returns {object} Result of API call.
   */
  btrmsgtpl.api3 = function(entity, action, params) {
    params = {
      entity,
      action: action.toLowerCase(),
      json: JSON.stringify(params || {})
    };
    return $.ajax({
      url: btrmsgtpl.vars.restApi,
      dataType: 'json',
      data: params,
      method: params.action.indexOf('get') === 0 ? 'GET' : 'POST'
    });
  };

  /***********
   * confirm *
   ***********/
  /**
   * Pose a question, receive affirmation.
   *
   * @param {string} title Title for prompt.
   * @param {string} message Message to display.
   * @param {object} buttons <em>primary</em> and <em>secondary</em> to specify button text. Defaults to <em>Okay</em> and <em>Cancel</em>.
   * @returns {bool} Provided answer.
   */
  btrmsgtpl.confirm = function(title, message, buttons) {
    if (this._prompter.promise !== null) {
      return;
    }
    this._prompter.promise = new Promise((resolve, reject) => {
      this._prompter.$title.text(title);
      this._prompter.$label.html(message);
      this._prompter.$value.val('confirm').addClass('d-none');
      this._prompter.$primary.text(buttons && buttons.hasOwnProperty('primary') ? buttons.primary : ts('Okay'));
      this._prompter.$secondary.text(buttons && buttons.hasOwnProperty('secondary') ? buttons.secondary : ts('Cancel'));
      this._prompter.value = false;

      this.$prompter.one('hidden.bs.modal', () => {
        this._prompter.promise = null;
        resolve(this._prompter.value === 'confirm');
      });
      this._prompter.instance.show();
    });
    return this._prompter.promise;
  };

  /**
   * Display an error message.
   * 
   * @memberof btrmsgtpl
   * @param {string} msg Error message to display.
   * @returns {void}
   */
  btrmsgtpl.error = function(msg) {
    this.alert(msg, ts('Danger, Will Robinson!'), 'error');
  };

  /**
   * Display an succes message.
   * 
   * @memberof btrmsgtpl
   * @param {string} msg Successful message to display.
   * @returns {void}
   */
  btrmsgtpl.success = function(msg) {
    this.alert(msg, ts('Success'), 'success', { expires: 5000 });
  };

  /**********
   * format *
   **********/
  /**
   * @memberof btrmsgtpl
   * @private
   * @property {jQuery} $format Template format selector.
   */
  btrmsgtpl.$format = $('#format')
    .select2({
      dropdownAutoWidth: true,
      minimumResultsForSearch: -1,
      width: 'unset'
    })
    .on('change', () => {
      $('#select2-format-container').removeAttr('title');
      const { lang, content } = btrmsgtpl._language.val();
      const format = btrmsgtpl.$format.val();
      /**
       * Message format has been selected.
       *
       * @event format_select
       * @param {string} format Selected format.
       * @param {string} model Model for the selected format.
       */
      btrmsgtpl._event.trigger('format_select', [ format, content[format] ]);
    });
  /**
   * @memberof btrmsgtpl
   * @returns {string} Selected template format. One of <em>msg_html</em>, <em>msg_text</em>, or <em>msg_subject</em>.
   */
  btrmsgtpl.format = function() {
    return this.$format.val();
  }

  /**
   * @memberof btrmsgtpl
   * @property {jQuery} $isModified Modified indicator in title bar.
   */
  btrmsgtpl.$isModified = $('#is-modified').addClass('d-none');

  /********
   * pref *
   ********/
  btrmsgtpl._prefs = JSON.parse(localStorage.getItem('btrmsgtpl-prefs')) || {};
  btrmsgtpl.pref = function(pref, def, set) {
    if (set) {
      this._prefs[pref] = def;
      localStorage.setItem('btrmsgtpl-prefs', JSON.stringify(this._prefs));
    }
    else {
      return this._prefs[pref] || def;
    }
  }

  /**********
   * prompt *
   **********/
  /**
   * @memberof btrmsgtpl
   * @private
   * @property {jQuery} $prompter Prompt dialog.
   */
  btrmsgtpl.$prompter = $('#prompter')
    // this doesn't actually work, even if you try in dev console with modal showing
    // gets called, just doesn't focus the control, wonder why???
    .on('shown.bs.modal', () => btrmsgtpl._prompter.$value.focus());
  /**
   * @memberof btrmsgtpl
   * @private
   * @property {object} _prompter Modal instance, and stuff.
   */
  btrmsgtpl._prompter = {
    $title: btrmsgtpl.$prompter.find('.modal-title'),
    $label: btrmsgtpl.$prompter.find('.form-label'),
    $value: btrmsgtpl.$prompter.find('input'),
    $primary: btrmsgtpl.$prompter.find('.btn-primary')
      .click(() => {
        btrmsgtpl._prompter.value = btrmsgtpl._prompter.$value.val();
        btrmsgtpl._prompter.instance.hide();
      }),
    $secondary: btrmsgtpl.$prompter.find('.btn-secondary'),
    instance: new bootstrap.Modal(btrmsgtpl.$prompter[0]),
    promise: null,
    value: null
  };
  /**
   * Ask a question, get an answer.
   *
   * @param {string} title Title for the prompt.
   * @param {string} message Message to display.
   * @param {string} def Default value.
   * @param {object} buttons <em>primary</em> and <em>secondary</em> to specify button text. Defaults to <em>Okay</em> and <em>Cancel</em>.
   * @returns {string|bool} The entered value, or FALSE on cancel.
   */
  btrmsgtpl.prompt = function(title, message, def, buttons) {
    if (this._prompter.promise !== null) {
      return;
    }
    this._prompter.promise = new Promise((resolve, reject) => {
      this._prompter.$title.text(title);
      this._prompter.$label.html(message);
      this._prompter.$value.val(def).removeClass('d-none');
      this._prompter.$primary.text(buttons && buttons.hasOwnProperty('primary') ? buttons.primary : ts('Okay'));
      this._prompter.$secondary.text(buttons && buttons.hasOwnProperty('secondary') ? buttons.secondary : ts('Cancel'));
      this._prompter.value = null;

      this.$prompter.one('hidden.bs.modal', () => {
        this._prompter.promise = null;
        resolve(this._prompter.value === null ? false : this._prompter.value);
      });
      this._prompter.instance.show();
    });
    return this._prompter.promise;
  };

  /**********
   * resize *
   **********/
  /**
   * @memberof btrmsgtpl
   * @private
   * @property {jQuery} $btrmsgtpl Main UI container.
   */
  btrmsgtpl.$btrmsgtpl = $('#btrmsgtpl');
  /**
   * Trigger resize event.
   * 
   * @memberof btrmsgtpl
   * @private
   * @returns {void}
   */
  btrmsgtpl._resize = function() {
    /**
     * UI has resized, either by toggle of variables or window resize.
     * 
     * @event btrmsgtpl_resize
     * @param {int} height Height of the editor.
     */
    btrmsgtpl._event.trigger('btrmsgtpl_resize', [ btrmsgtpl.$btrmsgtpl.height() ]);
  }
  $(window).on('resize', () => btrmsgtpl._resize());
  btrmsgtpl._resize(); // initial sizing

  btrmsgtpl.stopPrevent = function(evt) {
    if (evt) {
      evt.preventDefault();
      evt.stopPropagation();
    }
  }

  /********
   * tool *
   ********/
  /**
   * Result/selection formatter for tools.
   * 
   * @memberof btrmsgtpl
   * @private
   * @see {@link https://select2.org/dropdown}
   * @see {@link https://select2.org/selections}
   */
  btrmsgtpl._toolFormat = function(state) {
    const $el = $(state.element);
    const icon = $el.data('icon');
    return $(`<span><i class="me-1 ${icon}"></i> ${state.text}</span>`);
  };
  /**
   * @memberof btrmsgtpl
   * @private
   * @property {jQuery} $tool Tool selector.
   */
  btrmsgtpl.$tool = $('#tool');
  btrmsgtpl.$tool.select2({
      dropdownAutoWidth: true,
      minimumResultsForSearch: -1,
      templateResult: btrmsgtpl._toolFormat,
      templateSelection: btrmsgtpl._toolFormat,
      width: 'unset'
    })
      .on('select2:selecting', (evt) => {
        /**
         * A tool was deselected, as a result of another being selected. This is called first.
         * 
         * @event tool_deselect
         * @param {string} tool Deselected tool.
         */
        btrmsgtpl._event.trigger('tool_deselect', [ btrmsgtpl.$tool.val() ]);
        $('[data-tool]').addClass('d-none'); // hide all tools
      })
      .on('change', (evt) => {
        $('#select2-tool-container').removeAttr('title');

        const v = btrmsgtpl.$tool.val();
        $(`[data-tool="${v}"]`).removeClass('d-none'); // show selected tool
        /**
         * A tool was selected.
         * 
         * @event tool_select
         * @param {string} tool Selected tool.
         */
        btrmsgtpl._event.trigger('tool_select', [ v ]);
        btrmsgtpl._resize();
        btrmsgtpl._language._select();
      })
      .trigger('select2:selecting');
      //.trigger('change');

  /**
   * Get selected tool.
   * @returns {string} Selected tool.
   */
  btrmsgtpl.tool = function() {
    return this.$tool.val();
  }

  /******************
   * EVENT HANDLERS *
   ******************/



}());