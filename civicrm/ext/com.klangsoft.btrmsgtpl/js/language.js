(function(_event) {

  const TA = btrmsgtpl.actions('template-actions');

  const { tpl } = btrmsgtpl.vars;

  /**
   * Language
   * @namespace _language
   */
  const _language = {};
  /**
   * @memberof _language
   * @private
   * @property {jQuery} $ Language selector.
   */
  _language.$ = $('#language');
  /**
   * @memberof _language
   * @private
   * @property {object} _content All the variations for the template.
   */
  _language._content = tpl.content;

  // create options for existing variations
  for (let lang in _language._content) {
    if (lang !== '_std_') { // Standard is always first, and pre-created
      $('<option>').val(lang).text(_language._content[lang].language).appendTo(_language.$);
    }
  }

  /**
   * Result/selection formatter.
   * 
   * @memberof _language
   * @private
   * @see {@link https://select2.org/dropdown}
   * @see {@link https://select2.org/selections}
   */
  _language._format = function(state) {
    if (state.text === '-') {
      return $('<hr class="separator" />');
    }
    const content = _language._content[state.id];
    if (content && content.modified) {
      return $(`<span class="modified">${state.text}<i class="fa-solid fa-exclamation-triangle ms-2"></i></span>`);
    }
    return state.text;
  };

  // create the language control
  _language.$.select2({
    dropdownAutoWidth: true,
    templateResult: _language._format,
    templateSelection: _language._format,
    minimumResultsForSearch: 8,
    width: 'unset'
  });

  /**
   * Add a variation.
   * 
   * @fires language_add
   * @memberof _language
   * @param {string} lang Language code.
   * @returns {object} Content for the added variation.
   */
  _language.add = function(lang, silent) {
    if (!this.has(lang)) {
      let name = lang.indexOf('_std_') === 0 ? ts('Standard') : this._translator.$languages.find(`option[value="${lang.substr(0, 5)}"]`).text();
      if (lang.length > 5) {
        name += ts(' (Draft)');
      }
      const opt = $('<option>')
        .val(lang)
        .text(name);

      let inserted = false;
      this.$.find('option').each(function(i) {
        if (!i) {
          return;
        }
        const $opt = $(this);
        if (name.localeCompare($opt.text()) < 0) {
          opt.insertBefore($opt);
          inserted = true;
          return false;
        }
      });
      if (!inserted) {
        opt.appendTo(this.$);
      }

      /**
       * Represents a single template variation.
       * 
       * @namespace content
       */
      this._content[lang] = {
        /**
         * @memberof content
         * @property {object} ids IDs to translation records, keyed by format.
         */
        ids: {
          msg_html: 0,
          msg_text: 0,
          msg_subject: 0
        },
        /**
         * @memberof content
         * @property {string} lang Language code.
         */
        lang,
        /**
         * @memberof content
         * @property {string} language Language name.
         */
        language: name,
        /**
         * @memberof content
         * @property {array} revisions Revisions for the variation.
         */
        revisions: [],
        /**
         * @memberof content
         * @property {bool} modified Whether this content has been modified.
         */
        modified: true,
        /**
         * @memberof content
         * @property {ITextModel} msg_html HTML content.
         */
        msg_html: monaco.editor.createModel('', 'html'),
        /**
         * @memberof content
         * @property {ITextModel} msg_text Plain-text content.
         */
        msg_text: monaco.editor.createModel('', 'text'),
        /**
         * @memberof content
         * @property {ITextModel} msg_subject Message subject.
         */
        msg_subject: monaco.editor.createModel('', 'text')
      };
      if (lang.length > 5) {
        this._content[lang].revisions = this._content[lang.substr(0, 5)].revisions;
      }
      if (!silent) {
        /**
         * A new language variation has been added.
         * 
         * @event language_add
         * @param {string} lang Language code.
         * @param {object} content Content for the variation.
         */      
        _event.trigger('language_add', [ lang, this._content[lang] ]);
      }
    }
    return this._content[lang];
  };
  /**
   * Get content for a variation.
   * 
   * @memberof _language
   * @param {string} lang Language code.
   * @param {bool} createModels Whether to create Monaco models for the content formats.
   * @returns {object|null} Content for the variation, or NULL if not found.
   */
  _language.get = function(lang, createModels) {
    if (this.has(lang)) {
      const content = this._content[lang];
      if (createModels && typeof content.msg_html === 'string') {
        content.msg_html = monaco.editor.createModel(content.msg_html, 'html');
        content.msg_text = monaco.editor.createModel(content.msg_text, 'text');
        content.msg_subject = monaco.editor.createModel(content.msg_subject, 'text');
      }
      return content;
    }
    return null;
  };
  /**
   * Check if variation exists.
   * 
   * @memberof _language
   * @param {string} lang Language code.
   * @returns {bool} Whether the variation exists.
   */
  _language.has = function(lang) {
    return this._content.hasOwnProperty(lang);
  };
  /**
   * Check if modified.
   * 
   * @memberof _language
   * @returns {bool} Whether any variation is modified.
   */
  _language.modified = function() {
    let mod = false;
    for (let lang in this._content) {
      mod = mod || this._content[lang].modified;
    }
    return mod;
  };
  /**
   * Get modified variations.
   * 
   * @memberof _language
   * @returns {array} List of all modified variation names.
   */
  _language.modifiedLanguages = function() {
    let mod = [];
    for (let lang in this._content) {
      if (this._content[lang].modified) {
        mod.push(this._content[lang].language);
      }        
    }
    return mod;
  };
  /**
   * Change modified status for the current variation.
   * 
   * @fires language_modify
   * @memberof _language
   * @param {bool} mod Modified status.
   * @returns {void}
   */
  _language.modify = function(mod, force) {
    const { lang, content } = this.val();
    if (force || (content && content.modified !== mod)) {
      content.modified = mod;
      this.$.trigger('change.select2');
      /**
       * A language variation has been modified.
       * 
       * @event language_modify
       * @param {string} lang Language code.
       * @param {object} content Content for the variation.
       * @param {bool} modified Whether any variation is currently modified.
       */
      _event.trigger('language_modify', [ lang, content, this.modified() ]);
    }
  };
  /**
   * Remove a variation. Will also remove a corresponding draft.
   * 
   * @fires language_remove
   * @memberof _language
   * @param {string} lang Language code.
   * @returns {void}
   */
  _language.remove = function(lang) {
    const content = this.get(lang);
    if (content) {
      if (typeof content.msg_html !== 'string') {
        content.msg_html.dispose();
        content.msg_text.dispose();
        content.msg_subject.dispose();
      }  
      delete this._content[lang];
      this.$.find(`option[value="${lang}"]`).remove();
      /**
       * A language variation has been removed.
       * 
       * @event language_remove
       * @param {string} lang Language code.
       */
      _event.trigger('language_remove', [ lang ]);

      // @todo should draft auto delete with translation?
      /* if (lang.length === 5) {
        _language.remove(lang + '_draft');
      } */
    }
  };
  /**
   * Variation selected.
   * 
   * @fires language_select
   * @private
   * @memberof _language
   * @returns {void}
   */
  _language._select = function() {
    const { lang, content } = this.val();
    /**
     * @event language_select
     * @param {string} lang Language code.
     * @param {object} content Content for the variation.
     */
    _event.trigger('language_select', [ lang, content ]);
  };
  /**
   * Variation deselected.
   * 
   * @fires language_deselect
   * @private
   * @memberof _language
   * @returns {void}
   */
  _language._deselect = function() {
    const { lang, content } = this.val();
    /**
     * @event language_deselect
     * @param {string} lang Language code.
     * @param {object} content Content for the variation.
     */
    _event.trigger('language_deselect', [ lang, content ]);
  };
  /**
   * Get/set current language variation.
   * 
   * @memberof _language
   * @param {string} lang Language code. Set only.
   * @param {bool} trigger Whether to trigger a change event. Set only.
   * @returns {object|void} For get, returns an object containing <em>lang</em> and <em>content</em>.
   */
  _language.val = function(lang, trigger) {
    if (lang) {
      if (lang !== this.$.val()) {
        this._deselect();
      }
      this.$.val(lang);
      if (trigger) {
        this.$.trigger('change');
      }
    }
    else {
      lang = this.$.val();
      return {
        lang,
        content : this.get(lang)
      };
    }
  };

  /**************
   * translator *
   **************/
  /**
   * @memberof _language
   * @private
   * @property {jQuery} $translator Translator modal.
   */
  _language.$translator = $('#translator');
  /**
   * @memberof _language
   * @private
   * @property {object} _translator Modal instance and stuff.
   */
  _language._translator = {
    $languages: _language.$translator.find('#languages'),
    $okay: _language.$translator.find('.btn-primary')
      .click(() => {
        _language._translator.value = _language._translator.$languages.val();
        _language._translator.instance.hide();
      }),
    instance: null,
    promise: null,
    value: null
  };
  /**
   * Display and process <em>Add Translation</em> modal.
   * 
   * @memberof _language
   * @returns {Promise}
   */
  _language.translate = function() {
    if (this._translator.promise !== null) {
      return;
    }
    this._translator.promise = new Promise((resolve, reject) => {
      this._translator.$languages.val('');
      this._translator.value = null;

      this.$translator.one('hidden.bs.modal', () => {
        resolve(this._translator.value === null ? false : this._translator.value);
        this._translator.instance.dispose();
        this._translator.instance = null;
        this._translator.promise = null;
      });

      this._translator.instance = new bootstrap.Modal(this.$translator[0]);
      this._translator.instance.show();
    });
    return this._translator.promise;
  };

  _language.$.on('change', () => {
    $('#select2-language-container').removeAttr('title');
    _language._select();
  });
  _language.$.trigger('change');

  _language.$.on('select2-selecting', () => {
    _language._deselect();
  });

  /**
   * Language control.
   * 
   * @memberof btrmsgtpl
   * @property {object} _language
   * @see _language
   */
  btrmsgtpl._language = _language;


  $('#closer').on('click', (evt) => {
    const mods = _language.modifiedLanguages();

    if (mods.length === 0) {
      window.location = btrmsgtpl.vars.return_url;
    }
    else {
      btrmsgtpl.confirm(ts('Close Editor?'), ts('The following variation(s) have been modified:<br /><br /><code>%1</code>', {
        1: mods.join('</code><br /><code>')
      }), {
        primary: ts('Yes'),
        secondary: ts('No')
      })
        .then((confirm) => {
          if (confirm) {
            window.location = btrmsgtpl.vars.return_url;
          }
        });
    }
  });

  /******************
   * EVENT HANDLERS *
   ******************/

  /**
   * action_draft_activate
   */
  _event.on('action_draft_activate', (evt, actions) => {
    const { lang, content: draft } = _language.val();
    
    const base = lang.substr(0, 5);
    let content = _language.get(base, true);

    if (!content) {
      content = _language.add(base, true);
    }

    if (draft && content) {
      const $busy = TA._busy(ts('Activating draft...'));

      content.msg_html.setValue(draft.msg_html.getValue());
      content.msg_text.setValue(draft.msg_text.getValue());
      content.msg_subject.setValue(draft.msg_subject.getValue());

      _event.trigger('action_template_delete', [ actions, true ]);

      /* _language.val(content.lang, true);
      _language.modify(true, true);

      _event.trigger('action_template_save', [ actions ]); */

      TA._busy($busy);

      btrmsgtpl.success(ts('<code>%1</code> has been activated.', { 1: content.language }));
    }
  });
  /**
   * action_draft_create
   */
  _event.on('action_draft_create', (evt, actions) => {
    const { lang, content } = _language.val();
    if (lang.length === 5) {
      const name = lang + '_draft';
      const draft = _language.get(name, true);

      if (draft) {
        _language.val(name);
      }
      else {
        _language.add(name);
      }
    }
  });
  /**
   * action_language_add
   */
  _event.on('action_language_add', (evt, actions) => {
    _language.translate()
      .then((lang) => {
        if (lang !== false) {
          if (_language.has(lang)) {
            _language.val(lang, true);
          }
          else {
            _language.add(lang);
          }
        }
      });
  });
  /**
   * action_template_delete
   */
  // todo document force for the event
  _event.on('action_template_delete', (evt, actions, force) => {
    const { lang, content } = _language.val();

    if (force) {
      prom = Promise.resolve(true);
    }
    else {
      prom = btrmsgtpl.confirm(ts('Delete Variation?'), `<code>${content.language}</code>`, {
        primary: ts('Yes'),
        secondary: ts('No')
      });
    }
    prom.then((confirm) => {
      if (confirm) {
        const $busy = TA._busy(ts('Deleting %1...', {
          1: lang.length === 5 ? ts('translation') : ts('draft')
        }));
        
        let prom2 = null;
        if (content.ids.msg_html === 0) {
          prom2 = Promise.resolve({ is_error: 0 });
        }
        else {
          prom2 = btrmsgtpl.api3('Btrmsgtpl', 'delete', { id: tpl.id, lang });
        }
        prom2.then((result) => {
          TA._busy($busy);

          if (result.is_error === 0) {
            let sel = '_std_';
            if (lang.length > 5) {
              const base = lang.substr(0, 5);
              if (_language.has(base)) {
                sel = base;
              }
            }
            _language.remove(lang);
            _language.val(sel, true);
            _language.modify(_language.get(sel).modified, true);

            btrmsgtpl.success(ts('%1 has been deleted.', {
              1: `<code>${content.language}</code>`
            }));
          }
          else {
            btrmsgtpl.error(result.error_message);
          }
        });
      }
    });
  });
  /**
   * action_template_save
   */
  _event.on('action_template_save', (evt, actions) => {
    const { lang, content } = _language.val();
    const params = {
      id: tpl.id,
      lang, ids: content.ids,
      msg_html: content.msg_html.getValue(),
      msg_text: content.msg_text.getValue(),
      msg_subject: content.msg_subject.getValue()
    };

    const $busy = TA._busy(ts('Saving %1...', {
      1: lang.length === 5 ? ts('translation') : ts('draft')
    }));

    btrmsgtpl.api3('Btrmsgtpl', 'save', params)
      .then((result) => {
        TA._busy($busy);
        if (result.is_error === 0) {

          if (result.hasOwnProperty('ids')) {
            content.ids = result.ids;
          }
          content.revisions = result.revisions;

          _language.modify(false);
          
          btrmsgtpl.success(ts('%1 has been saved.', {
            1: `<code>${content.language}</code>`
          }));
          _event.trigger('template_saved', [ content ]);
        }
        else {
          btrmsgtpl.error(result.error_message);
        }
      });
  });
  /**
   * language_add
   */
  _event.on('language_add', (evt, lang, content) => {
    const current = _language.val();

    content.msg_html.setValue(current.content.msg_html.getValue());
    content.msg_text.setValue(current.content.msg_text.getValue());
    content.msg_subject.setValue(current.content.msg_subject.getValue());

    _language.val(lang, true);
    _language.modify(true, true);
  });
  /**
   * language_modify
   */
  _event.on('language_modify', (evt, lang, content, modified) => {
    btrmsgtpl.$isModified[modified ? 'removeClass': 'addClass']('d-none');
    btrmsgtpl.$btrmsgtpl.parent().css('background-color', modified ? 'firebrick' : 'transparent');
    TA._modify('template_save', content.modified)
      ._disable('template_save', !content.modified);
  });
  /**
   * language_select
   */
  _event.on('language_select', (evt, lang, content) => {
    if (!lang) {
      return;
    }
    const draft = lang.length > 5;

    TA._disable('draft_create', draft)
      ._disable('template_save', !content.modified)
      ._disable('draft_activate', !draft)
      ._disable('draft_delete', !draft)
      ._disable('template_delete', lang === '_std_')
      ._modify('template_save', content.modified);

    if (typeof content.msg_html === 'string') {
      content.msg_html = monaco.editor.createModel(content.msg_html, 'html');
      content.msg_text = monaco.editor.createModel(content.msg_text, 'text');
      content.msg_subject = monaco.editor.createModel(content.msg_subject, 'text');
    }
    btrmsgtpl.$format.trigger('change');
  });

}(btrmsgtpl._event));