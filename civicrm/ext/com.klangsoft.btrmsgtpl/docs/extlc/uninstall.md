# Better Message Templates (btrmsgtpl)

## Uninstall?

The following database tables will be dropped...

* `civicrm_msg_template_data` - recent captures and reference samples.
* `civicrm_msg_template_revisions` - message template revisions.

If you are uninstalling with the intent to reinstall at a later time and do not wish to lose this data, backup or rename these tables before proceeding.

---

[README](../../README.md)