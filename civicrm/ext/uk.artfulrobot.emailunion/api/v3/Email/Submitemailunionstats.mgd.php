<?php
// This file declares a managed database record of type "Job".
// The record will be automatically inserted, updated, or deleted from the
// database as appropriate. For more details, see "hook_civicrm_managed" at:
// http://wiki.civicrm.org/confluence/display/CRMDOC42/Hook+Reference
return array (
  0 =>
  array (
    'name' => 'Cron:EmailUnionSubmit',
    'entity' => 'Job',
    'params' =>
    array (
      'version' => 3,
      'name' => 'Email Union submission',
      'description' => 'Submit anonymous, aggregate data of popular domains, if enabled from within Email Union settings',
      'run_frequency' => 'Daily',
      'api_entity' => 'Email',
      'api_action' => 'Submitemailunionstats',
      'parameters' => 'check_auto_setting=1',
    ),
  ),
);
