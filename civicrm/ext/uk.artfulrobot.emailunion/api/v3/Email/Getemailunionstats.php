<?php
use CRM_Emailunion_ExtensionUtil as E;

/**
 * Email.Getemailunionstats API specification (optional)
 * This is used for documentation and validation.
 *
 * @param array $spec description of fields supported by this API call
 *
 * @see https://docs.civicrm.org/dev/en/latest/framework/api-architecture/
 */
function _civicrm_api3_email_Getemailunionstats_spec(&$spec) {
  $spec['use_cache'] = [
    'description' => 'Use cache',
    'type'        => 'boolean',
    'api.default' => 1,
  ];
  $spec['for_export'] = [
    'description' => 'Gets data to submit',
    'type'        => 'boolean',
    'api.default' => 0,
  ];
}

/**
 * Email.Getemailunionstats API
 *
 * @param array $params
 *
 * @return array
 *   API result descriptor
 *
 * @see civicrm_api3_create_success
 *
 * @throws API_Exception
 */
function civicrm_api3_email_Getemailunionstats($params) {

  // Prepare a cache for this data, we don't need to recalc every time.
  $cache = CRM_Utils_Cache::create(['type' => ['SqlGroup'], 'name' => 'emailunion']);

  // Load our provider name
  $provider = Civi::settings()->get('emailunion_provider');

  $dontUseCache = !$params['use_cache'] || $params['for_export'];

  $cutOff = $params['for_export'] ? 500 : 200;

  $localStats = $cache->get('localStats', NULL);
  if ($dontUseCache || !$localStats) {

    // on_hold = 1 for bounce, 2 for opt out.
    // We ignore opt outs.
    $sql = "
      SELECT MID(email, LOCATE('@', email) + 1, 100) domain, SUM(on_hold = 1) onHold, COUNT(*) total
      FROM civicrm_email
      WHERE on_hold = 0 OR (on_hold = 1 AND hold_date > CURRENT_DATE - INTERVAL 1 YEAR)
      GROUP BY domain
      HAVING COUNT(*) > $cutOff
      ORDER BY COUNT(*) DESC
    ";
    $localStats = CRM_Core_DAO::executeQuery($sql)->fetchAll();


    $totalRow = ['domain' => 'average', 'onHold' => 0, 'total' => 0];
    foreach ($localStats as &$row) {
      $row['onHold'] = (int) $row['onHold'];
      $row['total'] = (int) $row['total'];
      $totalRow['onHold'] += $row['onHold'];
      $totalRow['total'] += $row['total'];
    }
    unset($row);

    if ($params['for_export']) {
      if (!$provider) {
        throw new API_Exception('Cannot submit without specifying your provider.');
      }

      $datasetID = Civi::settings()->get('emailunion_dataset_id');
      if (!$datasetID) {
        // Generate a dataset ID now. 128 bits should be enough.
        $datasetID = bin2hex(random_bytes(16));
        Civi::settings()->set('emailunion_dataset_id', $datasetID);
      }
      $stats['datasetID'] = $datasetID;

      // Remove hidden domains.
      $hideDomains = Civi::settings()->get('emailunion_hide_domains');
      foreach (preg_split('/[\r\n]+/', trim($hideDomains)) as $d) {
        if ($d) {
          $d = '/' . str_replace('%', '.*', $d) . '/i';
          foreach ($localStats as $i => $row) {
            if (preg_match($d, $row['domain'])) {
              unset($localStats[$i]);
            }
          }
        }
      }

      return civicrm_api3_create_success([
        'local'         => $localStats,
        'provider'      => $provider,
        'datasetID'     => $datasetID,
      ], $params, 'Email', 'Getemailunionstats');

    }

    // Insert the average at the start.
    array_unshift($localStats, $totalRow);
    // Cache for 1 hour
    $cache->set('localStats', $localStats, 60*60);
  }

  // Load Union stats
  $unionStats = $cache->get('unionStats', NULL);
  if ($dontUseCache || !$unionStats) {
    $unionStats = json_decode(file_get_contents(EMAILUNION_FETCH_URL), TRUE);
    if ($unionStats) {
      // Success. Cache it for 1 day.
      $cache->set('unionStats', $unionStats, 60*60*24);
    }
  }
  $unionStats = $unionStats ?: [];

  $lastSubmitted = Civi::settings()->get('emailunion_last_sent');
  return civicrm_api3_create_success([
    'local'         => $localStats,
    'union'         => $unionStats,
    'provider'      => $provider,
    'lastSubmitted' => $lastSubmitted,
    'hideDomains'   => Civi::settings()->get('emailunion_hide_domains') ?? '',
    'autoSubmit'    => Civi::settings()->get('emailunion_auto_submit') ?? FALSE,
  ], $params, 'Email', 'Getemailunionstats');
}
