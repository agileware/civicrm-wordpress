<?php
use CRM_Emailunion_ExtensionUtil as E;
use GuzzleHttp\Psr7\Uri;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\TransferException;

/**
 * Email.Submitemailunionstats API specification (optional)
 * This is used for documentation and validation.
 *
 * @param array $spec description of fields supported by this API call
 *
 * @see https://docs.civicrm.org/dev/en/latest/framework/api-architecture/
 */
function _civicrm_api3_email_Submitemailunionstats_spec(&$spec) {
  $spec['delete_data'] = [
    'type' => 'boolean',
    'api.default' => 0,
    'description' => E::ts('If set, this will delete data from this site from the online Email Union database.'),
  ];
  $spec['check_auto_setting'] = [
    'type' => 'boolean',
    'api.default' => 0,
    'description' => E::ts('Used to identify run as scheduled job. If set it will check whether auto-submission is configured in the emailunion_auto_submit setting before submitting anything.'),
  ];
}

/**
 * Email.Submitemailunionstats API
 *
 * @param array $params
 *
 * @return array
 *   API result descriptor
 *
 * @see civicrm_api3_create_success
 *
 * @throws API_Exception
 */
function civicrm_api3_email_Submitemailunionstats($params) {

  if ($params['check_auto_setting']) {
    // Only proceed if configured to.
    if (!(Civi::settings()->get('emailunion_auto_submit') ?? FALSE)) {
      // Stop.
      return civicrm_api3_create_success(['info' => 'Did NOT submit any stats because auto submit is not enabled in Email Union settings.'], $params, 'Email', 'Submitemailunionstats');
    }
  }

  $stats = civicrm_api3('Email', 'getemailunionstats', ['for_export' => 1])['values'] ?? NULL;
  if (!$stats) {
    throw new API_Exception(/*error_message*/ 'Something went wrong calculating your stats. Could not submit');
  }

  if ($params['delete_data']) {
    $stats['local'] = [];
  }

  $returnValues = [];

  // Make POST request.
  $guzzleClient = new Client([
    'http_errors' => TRUE, // Get exceptions from guzzle requests.
  ]);
  try {
    $response = $guzzleClient->request('POST', EMAILUNION_SUBMIT_URL, ['json' => $stats]);
    $body = json_decode($response->getBody()->getContents(), TRUE);
    if (!$body) {
      throw new API_Exception('Unexecpted response received.');
    }
    $lastSubmitted = date('Y-m-d H:i:s');
    $returnValues = $body + ['lastSubmitted' => $lastSubmitted];
    // Success.
    Civi::settings()->set('emailunion_last_sent', $lastSubmitted);
  }
  catch (ClientException $e) {
    // 4xx errors
    $body = json_decode($e->getResponse()->getBody()->getContents(), TRUE);
    throw new API_Exception($body['error'] ?? 'Unknown error', $e->getCode());
  }
  catch (TransferException $e) {
    // Everything else
    throw new API_Exception('Server error', $e->getCode());
  }

  // Regenerate the cache
  civicrm_api3('Email', 'getemailunionstats', ['use_cache' => 0]);

  return civicrm_api3_create_success($returnValues, $params, 'Email', 'Submitemailunionstats');
}
