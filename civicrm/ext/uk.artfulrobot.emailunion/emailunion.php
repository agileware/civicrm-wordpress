<?php

require_once 'emailunion.civix.php';
// phpcs:disable
use CRM_Emailunion_ExtensionUtil as E;
// phpcs:enable

define('EMAILUNION_SUBMIT_URL', 'https://artfulrobot.uk/lil/emailunion/submit/');
define('EMAILUNION_FETCH_URL', 'https://artfulrobot.uk/lil/emailunion/data.json');
/**
 * Implements hook_civicrm_config().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_config/
 */
function emailunion_civicrm_config(&$config) {
  _emailunion_civix_civicrm_config($config);
}

/**
 * Implements hook_civicrm_install().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_install
 */
function emailunion_civicrm_install() {
  _emailunion_civix_civicrm_install();
}

/**
 * Implements hook_civicrm_enable().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_enable
 */
function emailunion_civicrm_enable() {
  _emailunion_civix_civicrm_enable();
}

// --- Functions below this ship commented out. Uncomment as required. ---

/**
 * Implements hook_civicrm_preProcess().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_preProcess
 */
//function emailunion_civicrm_preProcess($formName, &$form) {
//
//}

/**
 * Implements hook_civicrm_navigationMenu().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_navigationMenu
 */
function emailunion_civicrm_navigationMenu(&$menu) {
  _emailunion_civix_insert_navigation_menu($menu, 'Mailings', array(
    'label' => E::ts('Email Union Stats'),
    'name' => 'emailunion_stats',
    'url' => 'civicrm/a#/emailunion',
    'permission' => 'access CiviMail',
    'operator' => 'OR',
    'separator' => 0,
  ));
  _emailunion_civix_navigationMenu($menu);
}
