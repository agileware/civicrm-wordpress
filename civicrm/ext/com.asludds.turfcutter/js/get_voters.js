function get_voter_markers_from_civi(){
  var listOfMarkers = [];
  var defaultMarker = L.ExtraMarkers.icon({
    icon: 'fa-circle',
    iconColor: "white",
    markerColor: 'blue',
    shape: 'circle',
    prefix: 'fa'
  });
  CRM.$.ajaxSetup({async : false});
  CRM.api3('Contact', 'get', {
    "sequential": 1,
    "return": ["geo_code_1","geo_code_2","first_name","last_name","id"],
    "contact_type": "Individual",
    "options": {"limit":0} //no limit
    //TODO : Remove volunteers from this
  }).done(function(result) {
    // do something
    CRM.$.each(result.values, function(k,v){
      var location = [v.geo_code_1,v.geo_code_2];
      var marker = L.marker(location, {
        "id" : v.id,
        icon : defaultMarker
      });
      marker.bindPopup("<div style='text-align: center; margin-left: auto; margin-right: auto;'>"+ v.first_name + " " + v.last_name +"</div>", {maxWidth: '400'});
      marker.ID = v.id;
      listOfMarkers.push(marker);
    });
  });
  return listOfMarkers;
}

function get_volunteer_markers_from_civi(){
  var listOfMarkers = [];
  CRM.$.ajaxSetup({async : false});
  CRM.api3('Contact', 'get', {
    "sequential": 1,
    "return": ["geo_code_1","geo_code_2","first_name","last_name"],
    "contact_type": "Individual",
    "options": {"limit":0}, //no limit
    "contact_sub_type": "Volunteer"
  }).done(function(result) {
    // do something
    CRM.$.each(result.values, function(k,v){
      var location = [v.geo_code_1,v.geo_code_2];
      var redMarker = L.ExtraMarkers.icon({
        icon: 'fa-home',
        markerColor: 'red',
        shape: 'square',
        prefix: 'fa'
      });
      var marker = L.marker(location,{
        icon: redMarker
      });
      listOfMarkers.push(marker.bindPopup("<div style='text-align: center; margin-left: auto; margin-right: auto;'>"+ v.first_name + " " + v.last_name +"</div>", {maxWidth: '400'}));
    });
  });
  return L.layerGroup(listOfMarkers);
}

function add_voters_to_survey(surveyID,interviewerID,respondentID,new_group_name_string){
  var date = getDateTime();
  if(new_group_name_string == ""){
    var new_group_name_string =  "Group created at time" + "_" + date.replace(/ /g,"_");
  }
  CRM.$.ajaxSetup({async : false});
  CRM.api3('SurveyRespondant', 'create', {
    "survey_id": surveyID,
    "interviewer_id": interviewerID,
    "respondent_ids": respondentID,
    "new_group_name" : new_group_name_string
  });
  return new_group_name_string;
}

// Returns the voterIDs that are within all of the currently active drawnItems
function find_voters_within_drawnItems(polylineBool){
  var setOfVoterIDsWithinLayers = new Set([]);
  var arrayOfVoterLatLngs = [];
  var votersGeoJSON = voters.toGeoJSON();
  drawnItems.eachLayer(function(layer) {
    if(typeof layer._rings != 'undefined'){
      //Case where we have either a polygon or a rectangle
      var latlongminmax = find_maxminlatlong_of_rectangle_or_polygon(layer);
      window.latlongminmax = latlongminmax;
      var newgeojson = L.geoJson(votersGeoJSON, {
        //We are going to filter for markers which fall within bounds which is the largest rectangle surrounding this polygon
        filter : function(feature,layer){
          latlongminmax = window.latlongminmax;
          var minlat = latlongminmax[0];
          var maxlat = latlongminmax[1];
          var minlong = latlongminmax[2];
          var maxlong = latlongminmax[3];
          var lat = feature.geometry.coordinates[1];
          var long = feature.geometry.coordinates[0];
          if(minlat < lat && lat < maxlat && minlong < long && long < maxlong){
            return true;
          }
          else {
            return false;
          }
        }
      });
      delete window.latlongminmax;
      voters_heuristic_geojson = convert_geojson_to_feature(newgeojson);
      var found = find_voters_within_polygon_or_rectangle(voters_heuristic_geojson,layer.toGeoJSON());
      arrayOfVoterLatLngs = arrayOfVoterLatLngs.concat(found);
      //In the case that we can draw the shortest path polyline we do
      if(polylineBool){
        polyline_handler(found);
      }
    }
    else{
      //Case where we are dealing with a circle
      var found = find_voters_within_circle(votersGeoJSON,layer._latlng.lat,layer._latlng.lng,layer._mRadius);
      arrayOfVoterLatLngs = arrayOfVoterLatLngs.concat(found);
    }
  });
  // We now have a lat long for each voter, we just have to go through all of the voterGeoJSON and get the IDs.
  for(var i in voters._featureGroup._layers){
    var layerOfInterest = voters._featureGroup._layers[i];
    for(var j=0; j<arrayOfVoterLatLngs.length;j++){
      if(latLngArraysEqual([layerOfInterest._latlng.lat,layerOfInterest._latlng.lng],arrayOfVoterLatLngs[j])){
        setOfVoterIDsWithinLayers.add(layerOfInterest.ID);
      }
    }
  }
  return Array.from(setOfVoterIDsWithinLayers);
}

function convert_geojson_to_feature(newgeojson){
  var listOfMarkers = [];
  newgeojson.eachLayer(function(layer){
    var marker = L.marker([layer._latlng.lat,layer._latlng.lng]);
    listOfMarkers.push(marker);
  });
  return L.layerGroup(listOfMarkers).toGeoJSON();
}

function find_voters_within_polygon_or_rectangle(votersGeoJSON,layerGeoJSON){
  var toReturn = [];
  var thePoints = turf.within(votersGeoJSON,layerGeoJSON);
  for(var i=0; i<thePoints.features.length; i++){
    toReturn.push([thePoints.features[i].geometry.coordinates[1],thePoints.features[i].geometry.coordinates[0]]);
  }
  return toReturn;
}

function find_voters_within_circle(votersGeoJSON,lat,lng,mRadius){
  var toReturn = [];
  var centerlat = lat;
  var centerlng = lng;
  var radiusInMeters = mRadius;
  for(var i=0; i<votersGeoJSON.features.length; i++){
    var voterlat = votersGeoJSON.features[i].geometry.coordinates[1]; //reversed because geojson
    var voterlng = votersGeoJSON.features[i].geometry.coordinates[0];
    var measuredDistanceInMeters = measure(centerlat,centerlng,voterlat,voterlng);
    if(radiusInMeters>measuredDistanceInMeters){
      toReturn.push([voterlat,voterlng]);
    }
  }
  return toReturn;
}

function voters_within_circle_heuristic(votersGeoJSON,minLat,maxLat,minLong,maxLong){

}

function polyline_handler(found){
  console.log(found);
}


function latLngArraysEqual(array1,array2){
  if(array1[0]==array2[0]&&array1[1]==array2[1]){
    return true;
  }
  else{
    return false;
  }
}

function measure(lat1, lon1, lat2, lon2){  // generally used geo measurement function
  var R = 6378.137; // Radius of earth in KM
  var dLat = lat2 * Math.PI / 180 - lat1 * Math.PI / 180;
  var dLon = lon2 * Math.PI / 180 - lon1 * Math.PI / 180;
  var a = Math.sin(dLat/2) * Math.sin(dLat/2) +
      Math.cos(lat1 * Math.PI / 180) * Math.cos(lat2 * Math.PI / 180) *
      Math.sin(dLon/2) * Math.sin(dLon/2);
  var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
  var d = R * c;
  return d * 1000; // meters
}


function find_maxminlatlong_of_rectangle_or_polygon(layer){
  //initialize to be a very high value
  latitudes = [];
  longitudes = [];
  for(var i = 0; i <layer._latlngs[0].length; i++){
    var lat = layer._latlngs[0][i].lat;
    var lng = layer._latlngs[0][i].lng;
    latitudes.push(lat);
    longitudes.push(lng);
  }
  minLat = Math.min(...latitudes);
  maxLat = Math.max(...latitudes);
  minLong = Math.min(...longitudes);
  maxLong = Math.max(...longitudes);
  return [minLat,maxLat,minLong,maxLong];
}

function getDateTime() {
  var now     = new Date();
  var year    = now.getFullYear();
  var month   = now.getMonth()+1;
  var day     = now.getDate();
  var hour    = now.getHours();
  var minute  = now.getMinutes();
  var second  = now.getSeconds();
  if(month.toString().length == 1) {
    month = '0'+month;
  }
  if(day.toString().length == 1) {
    day = '0'+day;
  }
  if(hour.toString().length == 1) {
    hour = '0'+hour;
  }
  if(minute.toString().length == 1) {
    minute = '0'+minute;
  }
  if(second.toString().length == 1) {
    second = '0'+second;
  }
  var dateTime = year+'/'+month+'/'+day+' '+hour+':'+minute+':'+second;
  return dateTime;
}
