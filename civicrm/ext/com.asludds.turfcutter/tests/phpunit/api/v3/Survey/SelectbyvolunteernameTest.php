<?php

use Civi\Test\HeadlessInterface;
use Civi\Test\HookInterface;
use Civi\Test\TransactionalInterface;

/**
 * Survey.Selectbyvolunteername API Test Case
 * This is a generic test class implemented with PHPUnit.
 * @group headless
 */
class api_v3_Survey_SelectbyvolunteernameTest extends \PHPUnit_Framework_TestCase implements HeadlessInterface, HookInterface, TransactionalInterface {

  /**
   * Civi\Test has many helpers, like install(), uninstall(), sql(), and sqlFile().
   * See: https://github.com/civicrm/org.civicrm.testapalooza/blob/master/civi-test.md
   */
  public function setUpHeadless() {
    return \Civi\Test::headless()
      ->installMe(__DIR__)
      ->apply();
  }

  /**
   * The setup() method is executed before the test is executed (optional).
   */
  public function setUp() {
    parent::setUp();
    //Create volunteer

    //Create contact for test_contact_with_one_activity
    $result_one = civicrm_api3('Contact', 'create', [
                'contact_type' => "Individual",
                'first_name' => "Test",
                'last_name' => 1,
              ]);
    //Create address for contact
    $result_one_address = civicrm_api3('Address', 'create', [
                'contact_id' => $result_one["id"],
                'location_type_id' => "Home",
                'street_address' => "Test 1",
                'postal_code' => 22222,
                'city' => "testcity",
              ]);
    //Create activity for contact
    $result_one_activity = civicrm_api3('Activity', 'create', [
                'source_contact_id' => $volunteer["id"],
                'target_id' => $result_one["id"],
              ]);

    //Create contact for test_activity_with_no_voter
    $result_no_voter = civicrm_api3('Contact', 'create', [
                     'contact_type' => "Individual",
                     'first_name' => "Test",
                     'last_name' => 3,
                   ]);
    //Create address for contact
    $result_no_voter_address = civicrm_api3('Address', 'create', [
                'contact_id' => $result_no_voter['id'],
                'location_type_id' => "Home",
                'street_address' => "Test 3",
                'postal_code' => 22222,
                'city' => "testcity",
              ]);

    $result_no_voter_activity = civicrm_api3('Activity', 'create', [
                             'source_contact_id' => $volunteer["id"]
                           ]);
    }

  /**
   * The tearDown() method is executed after the test was executed (optional)
   * This can be used for cleanup.
   */
  public function tearDown() {
    parent::tearDown();
    //Teardown for test_contact_with_one_activity
    $result = civicrm_api3('Contact', 'delete', [
                'id' => $result_one["id"]
              ]);
    $result = civicrm_api3('Address', 'delete', [
                'id' => $result_one_address["id"]
              ]);
    $result = civicrm_api3('Activity', 'delete', [
                'id' => $result_one_activity["id"],
              ]);
    //Teardown for test_activity_with_no_voter
    $result = civicrm_api3('Contact', 'delete', [
                'id' => $result_no_voter["id"],
              ]);
    $result = civicrm_api3('Address', 'delete', [
                'id' => $result_no_voter_address["id"]
              ]);
    $result = civicrm_api3('Activity', 'delete', [
                'id' => $result_no_voter_activity["id"],
              ]);
  }

  /**
   * Simple example test case.
   *
   * Note how the function name begins with the word "test".
   */

  public function test_contact_with_one_activity(){
    $result = civicrm_api3('Survey','Selectbyvolunteername',array('volunteer_id',$volunteer['id']));
    $this->assertEquals($result["id"],$result_one["id"]);
  }

  public function test_activity_with_no_voter(){
    $result = civicrm_api3('Survey','Selectbyvolunteername',array('volunteer_id',$volunteer['id']));
    $this->assertEquals("",$result_no_voter["id"]);
  }


}
