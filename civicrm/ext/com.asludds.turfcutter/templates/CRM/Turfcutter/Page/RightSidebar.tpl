<!-- Nav tabs -->
        <div id="sidebar-right" class="leaflet-sidebar collapsed">
            <div class="leaflet-sidebar-tabs">
                <ul role="tablist">
                    <li><a href="#righthome" role="tab"><i class="fa fa-bars active"><p class="icon-center-rightsidebar">{ts}Filter{/ts}</p></i></a></li>
                    <li><a href="#rightprofile" role="tab"><i class="fa fa-user"><p class="icon-center-rightsidebar">{ts}V Info{/ts}</p></i></a></li>
                    <!-- <li><a href="#rightautocut" role="tab"><i class="fa fa-scissors"><p class="icon-center-rightsidebar">{ts}Auto{/ts}</p></i></a></li> -->
                </ul>
            </div>
            <!-- Tab panes -->
            <div class="leaflet-sidebar-content">
                <div class="leaflet-sidebar-pane" id="righthome">
                    <!-- Content of the right home -->
                    <h1 class="leaflet-sidebar-header">
                        {ts}RIGHTHOME{/ts}
                        <span class="leaflet-sidebar-close"><i class="fa fa-caret-right"></i></span>
                    </h1>
                    <h4 class="lorem"><center>{ts}Filter{/ts}</center></h4>
                    <p>{ts}This tab helps to explain how to make more sophisticated queries about your data using smart groups.{/ts}</p>
                    <p>{ts}Smart groups are a way of creating groups which meet a specific criteria for a query. For example, suppose I want to find all people within your instance of CiviCRM who are under 30 and also in Brooklyn. We can do this using a smart group. Hover over Search on the top menu bar, then click Advanced Search. This will load a form that can be filled out in order to create a group that meets your criteria. For this example there are a few ways of doing this. Firstly we want to find all people under 30. If we already have a group of people who are under 30 we can select it from Groups under Basic Criteria. If we don't have a group created we can go to Demographics and then choose the age range that we are interested in. For choosing the city of Brooklyn we have a few options. If we have a group of all people who are inside of the city of Brooklyn then we can use this in Groups under Basic Criteria. If we don't have a group already created we can go to Address Fields and filter by city.{/ts}</p> 

                </div>
                <div class="leaflet-sidebar-pane" id="rightprofile">
                    <!-- Content of the right profile -->
                    <h1 class="leaflet-sidebar-header">
                        {ts}VOLUNTEER INFORMATION{/ts}
                        <span class="leaflet-sidebar-close"><i class="fa fa-caret-right"></i></span>
                    </h1>
                    <p class="lorem">{ts}Here are a list of volunteers and their status for turf assignment{/ts}</p>
                    <nav>
                        <ul id="volunteer_info_volunteer_list" class="volunteer_info_volunteer_list">
                        </ul>
                        <script>populate_volunteer_list()</script>
                    </nav>
                </div>
                <div class="leaflet-sidebar-pane" id="rightautocut">
                  <!-- Content of right auto cut -->
                  <h1 class="leaflet-sidebar-header">
                        {ts}Auto-Cut Turf{/ts}
                        <span class="leaflet-sidebar-close"><i class="fa fa-caret-right"></i></span>
                    </h1>
                    <p class="lorem">{ts}This is a turf auto-cutter. The purpose is to help you be able to cut turf automatically.{/ts}</p>
                    <nav>
                        <ul id="turf_autocut" class="turf_autocut">
                        </ul>
                        <script>autocut()</script>
                    </nav>
                </div>
            </div>

        </div>
