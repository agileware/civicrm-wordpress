<div id="sidebar-left" class="leaflet-sidebar collapsed">
            <!-- Nav tabs -->
            <div class="leaflet-sidebar-tabs">
                <ul role="tablist">
                    <li><a href="#lefthome" role="tab"><i class="fa fa-bars active"><p class="icon-center">Home</p></i></a></li>
                    <li><a href="#lefttutorial" role="tab"><i class="fa fa-graduation-cap"><p class="icon-center">Learn</p></i></a></li>
                    <li><a href="#leftdocumentation" role="tab"><i class="fa fa-book"><p class="icon-center">Docs</p></i></a></li>
                    <li><a href="#leftaboutus" role="tab"><i class="fa fa-user"><p class="icon-center">About</p></i></a></li>
                    <li><a href="#leftlicence" role="tab"><i class="fa fa-gavel"><p class="icon-center">Licence</p></i></a></li>
                </ul>
                <!-- Link to gitlab of project -->
                <ul role="tablist">
                    <li><a href="https://gitlab.com/asludds/civicrm-turfcutter"><i class="fa fa-gitlab"><p  class="icon-center">Gitlab</p></i></a></li>
                </ul>
            </div>

            <!-- Tab panes -->
            <div class="leaflet-sidebar-content">
                <div class="leaflet-sidebar-pane" id="lefthome">
                    <!-- Content of left home goes here -->
                    <h1 class="leaflet-sidebar-header">
                        HOME
                        <span class="leaflet-sidebar-close"><i class="fa fa-caret-left"></i></span>
                    </h1>
                    <h1 class="lorem"><center>{ts}CiviCRM Turfcutter{/ts}</center></h1>
                    <h3>First thing, please press the fullscreen button below the plus and minus buttons here ----></h3>
                    <h4>{ts}What is turfcutting?{/ts}</h4>
                    <p>{ts}Turfcutting is when a small group of contacts is created from a larger group of contacts using geospatial information. For example, in political campaigns each person who canvasses must receive a group of people to canvass{/ts}</p>
                    <h4>{ts}What is a turf?{/ts}</h4>
                    <p>{ts}A turf is a group of people who were selected based on geospatial information.{/ts}</p>
                    <h4>{ts}How do I use this turfcutter?{/ts}</h4>
                    <p>{ts}There is a tutorial located below (check the graduation cap){/ts}</p>
                    <h4>{ts}What features does this turfcutter have?{/ts}</h4>
                    <p>{ts}This turfcutter includes the ability to filter turfs by language, time since last canvassed and custom fields.{/ts}</p>
                    <h4>{ts}Who made this turfcutter?{/ts}</h4>
                    <p>{ts}Check the about-us tab below (the person shaped icon).{/ts}</p>
                    <h4>{ts} How can I contribute to this project? {/ts}</h4>
                    <p>{ts}Click the github icon below{/ts}</p>
                    <h4>{ts}What type of licence does this software use?{/ts}</h4>
                    <p>{ts}This software uses an GNU AGPL 3 licence. For more information, click the gavel icon below {/ts}</p>
                </div>
                <div class="leaflet-sidebar-pane" id="lefttutorial">
                    <!-- Content of left tutorial goes here -->
                    <h1 class="leaflet-sidebar-header">
                        {ts}TUTORIAL{/ts}
                        <span class="leaflet-sidebar-close"><i class="fa fa-caret-left"></i></span>
                    </h1>
                    <p class="lorem">{ts}This application is a turfcutter, the purpose of which is to assist in creating groups of individuals from their locations.{/ts}</p>
                    <h2>{ts}How to create a turf{/ts}</h2>
                    <p class="lorem">{ts}Step 1:Choose a shape which you want to use to create your turf from the left shape menu.{/ts}</p>
                    <p class="lorem">{ts}Step 2: Draw a shape around some markers! This represents a turf. A popup will appear.{/ts}</p>
                    <p class="lorem">{ts}Step 3: This popup has several options including selecting which volunteer should be assigned to the turf, which primary language the voters should have to be assigned to the volunteer, how long the volunteer should be assigned to the turf for because they are removed, and which survey the volunteer should be added to. After selecting the parameters you want, you can click submit to add this information to the civicrm database.{/ts}</p>
                    <p class="lorem">{ts}There are several tools to assist in helping create a turf. The main tool are shapes, located on the left side of the map. These tools are a polygon, rectangle, and circle tool.{/ts}</p>
                    <p class="lorem">{ts}Another tool that is available are groups for markers. You can view these by hovering on the menu icon in the top right of the map. You will see three options labelled "voter", "volunteer", and "heat map". The voter option allows you to view the voters within your civiCRM instance. Volunteer allows you to see the home and work locations of your volunteers, which is useful for helping to figure out which volunteer to assign to a turf. Heat map is a stastical query tool. The parameters for the heat map can be modified by the first tab in the right menu.{/ts}</p>
                    <p class="lorem">{ts}The base map that is used can be changed from the menu icon in the top right of the map. There are currectly seventeen options.{/ts}</p>
                </div>
                <div class="leaflet-sidebar-pane" id="leftdocumentation">
                    <!-- Content of left documentation goes here -->
                    <h1 class="leaflet-sidebar-header">
                        {ts}DOCUMENTATION{/ts}
                        <span class="leaflet-sidebar-close"><i class="fa fa-caret-left"></i></span>
                    </h1>
                    <p class="lorem">{ts}This map is based upon <a href="https://leafletjs.com/">leaflet</a>, an open source javascript maps library.{/ts}</p>
                    <p class="lorem">{ts}Within leaflet each object (marker, polygon, rectangle etc) is stored as a "layer". A collection of layers is a "layer group".{/ts}</p>
                    <p class="lorem">{ts}There are three layer groups that are used for the turfcutter. A voter group which stores voter markers, a volunteer group, which stores volunteer homes and work, and a heat map group, which allows for statistical queries on voter and volunteer data.{/ts}</p>
                    <p class="lorem">{ts}CiviCRM is extended with several new API calls. The SurveyRespondant Entity has a create option and the Survey API is extended and now allows to alllow for adding voters to surveys.{/ts}</p>
                </div>
                <div class="leaflet-sidebar-pane" id="leftaboutus">
                    <!-- Content of left about us goes here -->
                    <h1 class="leaflet-sidebar-header">
                        {ts}ABOUT-US{/ts}
                        <span class="leaflet-sidebar-close"><i class="fa fa-caret-left"></i></span>
                    </h1>
                    <p class="lorem">{ts}Much of the software was designed by Alex Sludds (alexsludds@protonmail.com){/ts}</p>
                    <p class="lorem">{ts}Special thanks to Monish, Joe Murray, and Joe McLaughlin for their help and insight.{/ts}</p>
                </div>
                <div class="leaflet-sidebar-pane" id="leftlicence">
                    <!-- Content of left licence goes here -->
                    <h1 class="leaflet-sidebar-header">
                        {ts}LICENCE{/ts}
                        <span class="leaflet-sidebar-close"><i class="fa fa-caret-left"></i></span>
                    </h1>
                    <p>
    The GNU Affero General Public License is a free, copyleft license for software and other kinds of works, specifically designed to ensure cooperation with the community in the case of network server software.</p>
                    <p>
The licenses for most software and other practical works are designed to take away your freedom to share and change the works. By contrast, our General Public Licenses are intended to guarantee your freedom to share and change all versions of a program--to make sure it remains free software for all its users.
                    </p>
                    <p>
When we speak of free software, we are referring to freedom, not price. Our General Public Licenses are designed to make sure that you have the freedom to distribute copies of free software (and charge for them if you wish), that you receive source code or can get it if you want it, that you can change the software or use pieces of it in new free programs, and that you know you can do these things.</p>
                   <p>
Developers that use our General Public Licenses protect your rights with two steps: (1) assert copyright on the software, and (2) offer you this License which gives you legal permission to copy, distribute and/or modify the software.</p>
                    <p>
A secondary benefit of defending all users' freedom is that improvements made in alternate versions of the program, if they receive widespread use, become available for other developers to incorporate. Many developers of free software are heartened and encouraged by the resulting cooperation. However, in the case of software used on network servers, this result may fail to come about. The GNU General Public License permits making a modified version and letting the public access it on a server without ever releasing its source code to the public.</p>
                    <p>
The GNU Affero General Public License is designed specifically to ensure that, in such cases, the modified source code becomes available to the community. It requires the operator of a network server to provide the source code of the modified version running there to the users of that server. Therefore, public use of a modified version, on a publicly accessible server, gives the public access to the source code of the modified version.</p>
                    <p>
An older license, called the Affero General Public License and published by Affero, was designed to accomplish similar goals. This is a different license, not a version of the Affero GPL, but Affero has released a new version of the Affero GPL which permits relicensing under this license.
                  </p>
                  <a href="https://www.gnu.org/licenses/agpl-3.0.html">For more technical details on the licence see this link</a>
                </div>
            </div>
        </div>
