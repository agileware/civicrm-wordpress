<!DOCTYPE html>
<html>
    <head>
        <title>CiviCRM Turfcutter</title>

        <meta http-equiv="content-type" content="text/html; charset=utf-8" />

        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

        {literal}
        <style>
            html,  body {
                 padding: 0;
                 margin: 0;
             }

             html, body, #map {
                 height: 500px;
                  width: 800px;
                 font: 10pt "Helvetica Neue", Arial, Helvetica, sans-serif;
             }

             .lorem {
                 font-style: italic;
                 text-align: justify;
                 color: #AAA;
             }
             ul,
             .block ul,
             ol {
                margin: 0 0 0 0 !important;
                padding : 0 0 0 0 !important;
             }

             ol li,
             ul li,
             ul.menu li,
             .item-list ul li,
             li.leaf {
                margin: 0 0 0 0  !important; /* LTR */
                padding-bottom: 0 !important;
             }

             h2,
             #center h1{
                line-height: 180% !important;
                font-size: 160% !important;
             }
            </style>
            {/literal}
    </head>
    <body>
      {include file='CRM/Turfcutter/Page/LeftSidebar.tpl'}
      {include file='CRM/Turfcutter/Page/RightSidebar.tpl'}
      <div id="map"></div>

       {literal}
      <script>
             var voters =  L.markerClusterGroup({maxClusterRadius:function(e){return maxClusterRadius(e)},showCoverageOnHover:true,
                                                 disableClusteringAtZoom:16
                                                });
         var volunteers = get_volunteer_markers_from_civi();

             var popupGroup = L.layerGroup([]);

             //This function defines how much clustering will occur. Note that at all zooms 16 and greater than clustering is disabled
             function maxClusterRadius(zoomlevel){
               var startingValue = 10;
               var endValue = 0;
               var maxZoomLevel = 20;
               return startingValue + (startingValue-endValue)/maxZoomLevel * zoomlevel;
             }

         var defaultZoom = 11;

             var map = L.map('map', {
               fullscreenControl: true,
               center: map_initial_center(), //Note this function is located inside of volunteerinfo.js
           zoom: defaultZoom,
           maxZoom : 20,
             layers: [voters, googleStreets]
             });

         /*
          All of the base maps are defined within the tilelayer.js file
         */
         var baseMaps = {
             "Google Street map": googleStreets,
             "Open Street Maps": openStreetMaps,
             "Open Street Maps BW": openStreetMaps_BW,
             "Google Hybrid Map": googleHybrid,
             "Google Satellite Map": googleSat,
             "Google Terrain Map": googleTerrain,
             "ArcGis": arcGis,
             "Open Topological Map" : openTopoMap,
             "Open Roads Maps" : OpenMapSurfer_Roads,
             "Open Roads Greyscale" : OpenMapSurfer_Grayscale,
             "Stamen Toner" : Stamen_Toner,
             "Stamen Tonerlite" : Stamen_TonerLite,
             "Esri World Street Map" : Esri_WorldStreetMap,
             "Esri National Geographic World Map" : Esri_NatGeoWorldMap,
             "Carto" : CartoDB_Voyager,
             "HikeBike" : HikeBike_HikeBike,
             "Wikimedia" : Wikimedia,
             "Open PT Map" : OpenPtMap,
             "Open Railway Map" : OpenRailwayMap
         };

         var overlayMaps = {
             "Voter View": voters,
             "Volunteer View": volunteers
         };

         map.addControl(new L.Control.Scale());

         L.control.layers(baseMaps, overlayMaps).addTo(map);

         var drawnItems = new L.FeatureGroup();

         var options = {
             position: 'topleft',
             editable : true,
             draw: {
                 polyline: false,
                 polygon: {
                     allowIntersection: false, // Restricts shapes to simple polygons
                     drawError: {
                         color: '#f4355f', // Color the shape will turn when intersects
                         message: '<strong>Oh snap!<strong> you can\'t draw that!' // Message that will show when intersect
                     },
                     shapeOptions: {
                         color: '#f4355f'
                     }
                 },
                 circle: true,
                 marker : false,
                 rectangle: {
                     shapeOptions: {
                         clickable: false
                     }
                 },
                 circlemarker : false,
             },
             edit: {
                 featureGroup: drawnItems
             }
         };
         map.addLayer(popupGroup);
         map.addLayer(drawnItems);

         var drawControl = new L.Control.Draw(options);
         map.addControl(drawControl);

         drawControlEdit = new L.Control.Draw(options);

         map.on('draw:created', function(e) {
             var type = e.layerType,
                 layer = e.layer;
              drawnItems.addLayer(layer);
         });

         map.on('draw:deleted', function (e) {
             drawControlEdit.remove();
             drawControl.addTo(map);
         });

         map.on(L.Draw.Event.CREATED, function (e) {
             var type = e.layerType,
                 layer = e.layer;

             if (type === "polygon"){
                 var latlng = layer.getBounds().getCenter();
                 cornerLatLngs = layer.getLatLngs();
                 var points = voters.getLayers();
                 var votersGeoJSON = voters.toGeoJSON();
                 var voterIDsWithinPolygon = find_voters_within_drawnItems(true);
                 var popupcontentPolygon = get_popupcontentPolygon(latlng,voterIDsWithinPolygon);
                 layer.on('click', function() {
                     var newlayer = L.popup().setLatLng([latlng.lat,latlng.lng]).bindPopup(popupcontentPolygon, {
                     keepInView: true,
                     autoPan: true,
                     closeButton: true,
                     maxWidth : 1000,
                     maxHeight : 1000
                     });
                    popupGroup.addLayer(newlayer);
                    newlayer.openPopup();
                 });
                 var popup = L.popup().setLatLng([latlng.lat,latlng.lng]).bindPopup(popupcontentPolygon, {
                     keepInView: true,
                     autoPan: true,
                     closeButton: true,
                     maxWidth : 1000,
                     maxHeight : 1000
                 });
                 popupGroup.addLayer(popup);
                 popup.openPopup();
             }

             if(type == "rectangle"){
                 var centerlatlng = layer.getBounds().getCenter();
                 var points = voters.getLayers();
                 var votersGeoJSON = voters.toGeoJSON();
                 var voterIDsWithinRectangle = find_voters_within_drawnItems();
                 console.log(voterIDsWithinRectangle);
                 popupcontentRectangle = get_popupcontentRectangle(centerlatlng,voterIDsWithinRectangle);
                 var newlayer = L.popup().setLatLng([centerlatlng.lat,centerlatlng.lng]).bindPopup(popupcontentRectangle, {
                     keepInView: true,
                     closeButton: true
                 });
                 popupGroup.addLayer(newlayer);
                 newlayer.openPopup();
                 layer.on('click', function() {
                     var newlayer = L.popup().setLatLng([centerlatlng.lat,centerlatlng.lng]).bindPopup(popupcontentRectangle, {
                     keepInView: true,
                     autoPan: true,
                     closeButton: true
                     });
                    popupGroup.addLayer(newlayer);
                    newlayer.openPopup();
                 });
             }
             if(type == 'circle'){
                 var centerlatlng = layer._latlng;
                 var points = voters.getLayers();
                 var votersGeoJSON= voters.toGeoJSON();
                 var voterIDsWithinCircle = find_voters_within_drawnItems();
                 popupcontentCircle = get_popupcontentCircle(centerlatlng,voterIDsWithinCircle);
                 var newlayer = L.popup().setLatLng([centerlatlng.lat,centerlatlng.lng]).bindPopup(popupcontentCircle, {
                     keepInView: true,
                     closeButton: true
                 });
                 popupGroup.addLayer(newlayer);
                 newlayer.openPopup();
                 layer.on('click', function() {
                     var newlayer = L.popup().setLatLng([centerlatlng.lat,centerlatlng.lng]).bindPopup(popupcontentCircle, {
                     keepInView: true,
                     autoPan: true,
                     closeButton: true
                     });
                    popupGroup.addLayer(newlayer);
                    newlayer.openPopup();
                 });

             }
         });

         var leftsidebar = L.control.sidebar({ container: 'sidebar-left', position : 'left' })
                            .addTo(map)
                            .open('lefthome');

         var rightsidebar = L.control.sidebar({ container: 'sidebar-right', position : 'right' })
                             .addTo(map);
        //generate_voter_group_list
        var legend = L.control({position: 'topright'});
             legend.onAdd = function (map) {
               var div = L.DomUtil.create('div', 'info legend');
               div.innerHTML = generate_voter_group_list(1);
               div.setAttribute("id","info_legend");
        return div;
             };
             legend.addTo(map);
        </script>
        {/literal}
    </body>
</html>
