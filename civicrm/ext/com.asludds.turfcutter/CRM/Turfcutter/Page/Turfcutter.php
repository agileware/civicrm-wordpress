<?php
use CRM_Turfcutter_ExtensionUtil as E;

class CRM_Turfcutter_Page_Turfcutter extends CRM_Core_Page {
  public function run() {
    // CRM_Utils_System::setTitle(E::ts('Turfcutter'));
    //leaflet is top priority
    Civi::resources()->addStyleFile('com.asludds.turfcutter','css/leaflet_1.2.0.css',0,'html-header');
    Civi::resources()->addScriptFile('com.asludds.turfcutter','node_modules/leaflet/dist/leaflet.js',0,'html-header');

    //javascript files
    Civi::resources()->addScriptFile('com.asludds.turfcutter','js/leaflet.extra-markers.min.js',1,'html-header');
    Civi::resources()->addScriptFile('com.asludds.turfcutter', 'node_modules/leaflet-draw/dist/leaflet.draw.js', 1, 'html-header');
    Civi::resources()->addScriptFile('com.asludds.turfcutter', 'node_modules/leaflet-sidebar-v2/js/leaflet-sidebar.js', 2, 'html-header');
    Civi::resources()->addScriptFile('com.asludds.turfcutter', 'node_modules/@turf/turf/turf.min.js', 1, 'html-header');
    Civi::resources()->addScriptFile('com.asludds.turfcutter', 'js/heatmap.js', 1, 'html-header');
    Civi::resources()->addScriptFile('com.asludds.turfcutter', 'js/popupcontent.js', 1, 'html-header');
    Civi::resources()->addScriptFile('com.asludds.turfcutter', 'js/volunteerinfo.js', 1, 'html-header');
    Civi::resources()->addScriptFile('com.asludds.turfcutter','js/get_voters.js',1,'html-header');
    Civi::resources()->addScriptFile('com.asludds.turfcutter','js/tilelayer.js',1,'html-header');
    Civi::resources()->addScriptFile('com.asludds.turfcutter','js/displayed_voter_group_controller.js',1,'html-header');
    Civi::resources()->addScriptFile('com.asludds.turfcutter','js/autocut.js',1,'html-header');
    Civi::resources()->addScriptFile('com.asludds.turfcutter','js/leaflet.markercluster-src.js',1,'html-header');
    Civi::resources()->addScriptFile('com.asludds.turfcutter','node_modules/leaflet.fullscreen/Control.FullScreen.js',1,'html-header');

    //css files
    Civi::resources()->addStyleFile('com.asludds.turfcutter', 'css/leaflet.extra-markers.min.css',1,'html-header');
    Civi::resources()->addStyleFile('com.asludds.turfcutter', 'node_modules/leaflet-draw/dist/leaflet.draw.css', 1, 'html-header');
    Civi::resources()->addStyleFile('com.asludds.turfcutter', 'node_modules/leaflet-sidebar-v2/css/leaflet-sidebar.css', 1, 'html-header');
    Civi::resources()->addStyleFile('com.asludds.turfcutter', 'css/heatmap.css', 1, 'html-header');
    Civi::resources()->addStyleFile('com.asludds.turfcutter', 'css/volunteerinfo.css', 1, 'html-header');
    Civi::resources()->addStyleFile('com.asludds.turfcutter','css/leftsidebar.css',1,'html-header');
    Civi::resources()->addStyleFile('com.asludds.turfcutter','css/display_voter_group.css',1,'html-header');
    Civi::resources()->addStyleFile('com.asludds.turfcutter','css/MarkerCluster.css',1,'html-header');
    Civi::resources()->addStyleFile('com.asludds.turfcutter','css/MarkerCluster.Default.css',1,'html-header');
    Civi::resources()->addStyleFile('com.asludds.turfcutter','node_modules/leaflet.fullscreen/Control.FullScreen.css',1,'html-header');

    parent::run();
  }

}
?>
