{crmScope extensionKey='invoiceaddressapi'}
  <div class="crm-block crm-form-block">
    <div class="help-block" id="help">
      {ts}In this form you can select relationship types that will be used to collect invoice addresses for contacts using the InvoiceAddress Get API. This means that for each of the relationships of the selected type, the billing address(es) of the related contact will be returned as possible invoice addresses.{/ts}
      {ts}You can also select the fields that will be returned for eacht address.{/ts}
    </div>

    <div class="crm-section">
      <div class="label">{$form.inv_adr_relationship_type.label}</div>
      <div class="content crm-select-container" id="inv_adr_relationship_type_block">
        {$form.inv_adr_relationship_type.html}
      </div>
      <div class="clear"></div>
    </div>
    <div class="crm-section">
      <div class="label">{$form.inv_adr_columns.label}</div>
      <div class="content crm-select-container" id="inv_adr_columns_block">
        {$form.inv_adr_columns.html}
      </div>
      <div class="clear"></div>
    </div>
    <div class="crm-section">
      <div class="label">{$form.inv_adr_billing_loc_type.label}</div>
      <div class="content crm-select-container" id="inv_adr_billing_loc_type_block">
        {$form.inv_adr_billing_loc_type.html}
      </div>
      <div class="clear"></div>
    </div>
    <div class="crm-section">
      <div class="label">{$form.inv_adr_billing_check.label}</div>
      <div class="content crm-select-container" id="inv_adr_billing_check_block">
        {$form.inv_adr_billing_check.html}
      </div>
      <div class="clear"></div>
    </div>

    <div class="crm-submit-buttons">
      {include file="CRM/common/formButtons.tpl"}
    </div>
  </div>
{/crmScope}
