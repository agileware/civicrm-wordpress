<?php
use CRM_Invoiceaddressapi_ExtensionUtil as E;

/**
 * InvoiceAddress.Get API specification (optional)
 * This is used for documentation and validation.
 *
 * @param array $spec description of fields supported by this API call
 * @return void
 * @see http://wiki.civicrm.org/confluence/display/CRMDOC/API+Architecture+Standards
 */
function _civicrm_api3_invoice_address_Get_spec(&$spec) {
  $spec['contact_id'] = array(
    'name' => 'contact_id',
    'title' => 'contact_id',
    'description' => 'ContactID to get Invoice Addresses for',
    'api.required' => 1,
    'type' => CRM_Utils_Type::T_INT,
  );
}

/**
 * InvoiceAddress.Get API
 *
 * @param array $params
 * @return array API result descriptor
 * @see civicrm_api3_create_success
 * @see civicrm_api3_create_error
 * @throws API_Exception
 */
function civicrm_api3_invoice_address_Get($params) {
  $invoiceAddress = new CRM_Invoiceaddressapi_InvoiceAddress();
  $result = $invoiceAddress->getByContactId($params['contact_id']);
  if (!empty($result)) {
    return civicrm_api3_create_success($result, $params, 'InvoiceAddress', 'Get');
  }
  else {
    return civicrm_api3_create_error('No invoice addresses found');
  }
}
