<?php
use CRM_Invoiceaddressapi_ExtensionUtil as E;
/**
 * InvoiceAddress processing
 *
 * @author Erik Hommel (CiviCooP) <erik.hommel@civicoop.org>
 * @date 21 May 2018
 * @license AGPL-3.0
 */
class CRM_Invoiceaddressapi_InvoiceAddress {

  private $_fields = [];
  private $_billingLocationTypeId = NULL;
  private $_invoiceAddresses = [];
  private $_validRelationshipTypeIds = [];
  private $_inclBillingLocationType = FALSE;
  private $_inclBillingCheck = FALSE;

  /**
   * CRM_Invoiceaddresses_InvoiceAddress constructor.
   */
  public function __construct() {
    $this->_fields = Civi::settings()->get('invAdrApiColumns');
    $this->_inclBillingLocationType = Civi::settings()->get('invAdrApiBillingLocType');
    $this->_inclBillingCheck = Civi::settings()->get('invAdrApiBillingCheck');
    $this->_validRelationshipTypeIds = Civi::settings()->get('invAdrApiRelTypes');
    try {
      $locationType = \Civi\Api4\LocationType::get(FALSE)
        ->addSelect('id')
        ->addWhere('name', '=', 'Billing')
        ->execute()->first();
      if (isset($locationType['id'])) {
        $this->_billingLocationTypeId = $locationType['id'];
      }
    }
    catch (\API_Exception $ex) {
    }
  }

  /**
   * Method to get invoice address for a specific contact
   *
   * @param int|NULL $contactId
   * @return array|bool
   */
  public function getByContactId(?int $contactId) {
    if (empty($contactId)) {
      CRM_Core_Error::createError(E::ts('Empty contact id received in ') . __METHOD__);
    }
    // first add contacts on billing address
    $this->getBillingAddress($contactId);
    // get all relevant relationships for contact
    try {
      $relationships = civicrm_api3('Relationship', 'get', [
        'contact_id' => $contactId,
        'relationship_type_id' => ['IN' => $this->_validRelationshipTypeIds],
        'options' => ['limit' => '0'],
      ]);
      foreach ($relationships['values'] as $relationshipId => $relationship) {
        // get address for other contact
        if ($relationship['contact_id_a'] == $contactId) {
          $addressContactId = $relationship['contact_id_b'];
        }
        else {
          $addressContactId = $relationship['contact_id_a'];
        }
        $this->getBillingAddress($addressContactId);
      }
    }
    catch (CiviCRM_API3_Exception $ex) {
    }
    if ($this->_invoiceAddresses) {
      return $this->_invoiceAddresses;
    }
    else {
      return FALSE;
    }
  }

  /**
   * Method to get billing address(es) for contact
   *
   * @param int $contactId
   */
  private function getBillingAddress($contactId) {
    try {
      // first get the billing location type address if set and if setting is yes
      if ($this->_billingLocationTypeId && $this->_inclBillingLocationType) {
        $address = \Civi\Api4\Address::get(FALSE)
          ->addWhere('location_type_id', '=', $this->_billingLocationTypeId)
          ->addWhere('contact_id', '=', $contactId)
          ->setLimit(1)->execute()->first();
        $this->addAddress($address);
      }
    }
    catch (\API_Exception $ex) {
    }
    // then get all addresses checked as billing for contact if setting is yes
    if ($this->_inclBillingCheck) {
      try {
        $addresses = \Civi\Api4\Address::get(FALSE)
          ->addWhere('contact_id', '=', $contactId)
          ->addWhere('is_billing', '=', TRUE)
          ->execute();
        foreach ($addresses['values'] as $address) {
          $this->addAddress($address);
        }
      }
      catch (\API_Exception $ex) {
      }
    }
  }

  /**
   * Method to add addresses if not already in
   *
   * @param $address
   */
  private function addAddress($address) {
    $found = [];
    // if the same address is not already in the list
    if (!isset($this->_invoiceAddresses[$address['id']])) {
      $found['id'] = $address['id'];
      $found['address_contact_id'] = $address['contact_id'];
      try {
        $contact = \Civi\Api4\Contact::get(FALSE)
          ->addSelect('display_name')
          ->addWhere('contact_id', '=', $address['contact_id'])
          ->setLimit(1)->execute()->first();
        if (isset($contact['display_name'])) {
          $found['name'] = $contact['display_name'];
        }
      }
      catch (\API_Exception $ex) {
        $found['name'] = '';
      }
      foreach ($this->_fields as $field) {
        if (isset($address[$field])) {
          $found[$field] = $address[$field];
          if ($field == 'country_id') {
            try {
              $country = \Civi\Api4\Country::get(FALSE)
                ->addSelect('iso_code')
                ->addWhere('id', '=', $address[$field])
                ->setLimit(1)->execute()->first();
              if (isset($country['iso_code'])) {
                $found['country_iso'] = $country['iso_code'];
              }
            }
            catch (\API_Exception $ex) {
              $found['country_iso'] = '';
            }
          }
        }
      }
      $this->_invoiceAddresses[] = $found;
    }
  }

}
