# Clone Event Registrations.

![Screenshot](/images/screenshot.gif)

This extension clones an existing event registration and registers the contact for the new event. 
All data will be copied from the original event registration.

The extension is licensed under [AGPL-3.0](LICENSE.txt).

## Requirements

* PHP v5.6+
* CiviCRM

## Installation (Web UI)

This extension has not yet been published for installation via the web UI.

## Installation (CLI, Zip)

Sysadmins and developers may download the `.zip` file for this extension and
install it with the command-line tool [cv](https://github.com/civicrm/cv).

```bash
cd <extension-dir>
cv dl cloneeventregistration@https://lab.civicrm.org/civicoop/cloneeventregistration/-/archive/master/anonymousaccessbyhash-master.zip
```

## Installation (CLI, Git)

Sysadmins and developers may clone the [Git](https://en.wikipedia.org/wiki/Git) repo for this extension and
install it with the command-line tool [cv](https://github.com/civicrm/cv).

```bash
git clone https://lab.civicrm.org/civicoop/cloneeventregistration.git
cv en cloneeventregistration
```

## Usage

You can find the clone tool under Events --> Search Participants and then search for participants and the clone tool is one of the actions in the search result screen.

## Known Issues

(* FIXME *)
