{if $showFeeBlock}
    {if $priceSet}
        <div id='validate_pricefield' class='messages crm-error hiddenElement'></div>
    {/if}

{include file="CRM/Event/Form/EventFees.tpl"}

{* Main event form template *}
{else}


{crmScope extensionKey='cloneeventregistration'}
<div class="crm-block crm-form-block crm-contact-task-cloneeventregistration-form-block">
    <div class="help">
        <p>{ts}Clones the participant registration(s) to a given event.{/ts}</p>
        <p>{ts}Click <strong>Clone Participant(s)</strong> below to save all your changes.{/ts}</p>
    </div>

    <table class="form-layout">
        <tr><td>{include file="CRM/Event/Form/Task.tpl"}</td></tr>
    </table>

    <table class="form-layout">
        <tr class="crm-contact-task-cloneeventregistration-form-block-event">
            <td class="label">{$form.event_id.label}</td>
            <td>{$form.event_id.html}</td>
        <tr>

        {include file="CRM/Campaign/Form/addCampaignToComponent.tpl" campaignTrClass="crm-participant-form-block-campaign_id"}

        <tr class="crm-contact-task-cloneeventregistration-form-block-status">
            <td class="label">{$form.status_id.label}</td>
            <td>{$form.status_id.html}</td>
        <tr>
    </table>

    {* Fee block (EventFees.tpl) is injected here when an event is selected. *}
    <div class="crm-event-form-fee-block"></div>

    <div class="crm-submit-buttons">{include file="CRM/common/formButtons.tpl" location="bottom"}</div>
</div>

{if $participantId and $hasPayment}
    {include file="CRM/Contribute/Page/PaymentInfo.tpl" show='payments'}
{/if}

<script type="text/javascript">
{literal}
CRM.$(function($) {
  var $form = $('form.CRM_Event_Form_Task_CloneEventRegistration');

  $('#event_id').change(function() {
    var eventId = $(this).val();
    if (!eventId) {
      return;
    }

    buildFeeBlock();
  });

  buildFeeBlock();

  //build discount block
  if ($('#discount_id', $form).val()) {
    buildFeeBlock($('#discount_id', $form).val());
  }
  $($form).on('change', 'discount_id', function() {
    buildFeeBlock($(this).val());
  });

  function buildFeeBlock(discountId)  {
    var dataUrl = {/literal}"{crmURL p=$urlPath h=0 q="snippet=4&qfKey=$qfKey"}";
    dataUrl += '&' + '{$urlPathVar}';

    {literal}
    var eventId = $('[name=event_id], #event_id').val();

    if (eventId) {
      dataUrl += '&eventId=' + eventId;
    } else {
      return;
    }

    if (discountId) {
      dataUrl += '&discountId=' + discountId;
    }

    $.ajax({
      url: dataUrl,
      success: function ( html ) {
        $(".crm-event-form-fee-block").html( html ).trigger('crmLoad');
      }
    });
  }
});
{/literal}
</script>
{/crmScope}
{/if}
