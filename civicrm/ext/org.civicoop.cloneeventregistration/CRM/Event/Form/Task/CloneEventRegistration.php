<?php
/**
 * @author Jaap Jansma <jaap.jansma@civicoop.org>
 * @license AGPL-3.0
 */

use CRM_Cloneeventregistration_ExtensionUtil as E;

class CRM_Event_Form_Task_CloneEventRegistration extends CRM_Event_Form_Task {

  public $_showFeeBlock = FALSE;

  /**
   * Line Item for Price Set.
   */
  public $_lineItem = NULL;

  /**
   * Contribution mode for event registration for offline mode.
   *
   * @deprecated
   */
  public $_contributeMode = 'direct';

  public $_online;

  public $_isPaidEvent;

  /**
   * The values for the contribution db object.
   *
   * @var array
   */
  public $_values;

  /**
   * The values for the quickconfig for priceset.
   *
   * @var boolean
   */
  public $_quickConfig = NULL;

  /**
   * Price Set ID, if the new price set method is used
   *
   * @var int
   */
  public $_priceSetId;

  /**
   * Array of fields for the price set.
   *
   * @var array
   */
  public $_priceSet;

  /**
   * Event id.
   */
  public $_eventId = NULL;

  public $_context = 'standalone';

  public function preProcess() {
    parent::preProcess();
    $this->_showFeeBlock = CRM_Utils_Array::value('eventId', $_GET);
    $this->assign('showFeeBlock', FALSE);
    $this->assign('feeBlockPaid', FALSE);

    $this->_action = CRM_Utils_Request::retrieve('action', 'String', $this, FALSE, 'add');
    $this->_mode = empty($this->_mode) ? CRM_Utils_Request::retrieve('mode', 'String', $this) : $this->_mode;

    if ($this->_mode) {
      $this->assign('participantMode', $this->_mode);
    }

    $this->assign('accessContribution', FALSE);
    if (CRM_Core_Permission::access('CiviContribute')) {
      $this->assign('accessContribution', TRUE);
    }

    if ($this->_showFeeBlock) {
      $this->assign('showFeeBlock', TRUE);
      $isMonetary = CRM_Core_DAO::getFieldValue('CRM_Event_DAO_Event', $this->_showFeeBlock, 'is_monetary');
      if ($isMonetary) {
        $this->assign('feeBlockPaid', TRUE);
      }
      return CRM_Event_Form_EventFees::preProcess($this);
    }

    // when fee amount is included in form
    if (!empty($_POST['hidden_feeblock']) || !empty($_POST['send_receipt'])) {
      CRM_Event_Form_EventFees::preProcess($this);
      CRM_Event_Form_EventFees::buildQuickForm($this);
      CRM_Event_Form_EventFees::setDefaultValues($this);
    }

    $urlString = CRM_Utils_System::currentPath();
    $this->assign('urlPath', $urlString);
    $this->assign('urlPathVar', "_qf_CloneEventRegistration_display=true&qfKey={$this->controller->_key}");
  }

  public function setDefaultValues() {
    if ($this->_showFeeBlock) {
      return CRM_Event_Form_EventFees::setDefaultValues($this);
    }
  }

  public function buildQuickForm() {
    if ($this->_showFeeBlock) {
      return CRM_Event_Form_EventFees::buildQuickForm($this);
    }

    $eventFieldParams = array(
      'entity' => 'event',
      'select' => array('minimumInputLength' => 0),
      'api' => array(
        'extra' => array('campaign_id', 'default_role_id', 'event_type_id'),
      ),
    );

    $this->addEntityRef('event_id', ts('Event'), $eventFieldParams, TRUE);

    $campaignId = null;
    CRM_Campaign_BAO_Campaign::addCampaign($this, $campaignId);

    $statusOptions = CRM_Event_BAO_Participant::buildOptions('status_id', 'create');
    $this->addSelect('status_id', array(
        'entity' => 'Participant',
        'context' => 'create',
        'options' => $statusOptions,
        'option_url' => 'civicrm/admin/participant_status',
      ), TRUE);

    $this->addDefaultButtons(ts('Clone Participant(s)'));
  }

  public function postProcess() {
    parent::postProcess();

    $submitValues = $this->controller->exportValues($this->_name);
    if (isset($submitValues['total_amount'])) {
      $submitValues['total_amount'] = CRM_Utils_Rule::cleanMoney($submitValues['total_amount']);
    }

    $queue = CRM_Queue_Service::singleton()->create(array(
      'type' => 'Sql',
      'name' => 'cloneeventregistration',
      'reset' => false, //do not flush queue upon creation
    ));

    foreach($this->_participantIds as $id) {
      $task = new CRM_Queue_Task(
        array('CRM_Event_Form_Task_CloneEventRegistration', 'cloneEventRegistration'), //call back method
        array($id, $submitValues['event_id'], $submitValues['status_id'], $submitValues['campaign_id'], $submitValues, $this->_values, $this->_lineItem, $this->_priceSetId) //parameters
      );
      //now add this task to the queue
      $queue->createItem($task);
    }

    $session = CRM_Core_Session::singleton();
    $url = str_replace("&amp;", "&", $session->popUserContext());
    $runner = new CRM_Queue_Runner(array(
      'title' => E::ts('Clone event registration(s)'), //title fo the queue
      'queue' => $queue, //the queue object
      'errorMode'=> CRM_Queue_Runner::ERROR_ABORT, //abort upon error and keep task in queue
      'onEnd' => array('CRM_Event_Form_Task_CloneEventRegistration', 'onEnd'), //method which is called as soon as the queue is finished
      'onEndUrl' => $url,
    ));

    $runner->runAllViaWeb(); // does not return
  }

  public static function cloneEventRegistration(CRM_Queue_TaskContext $ctx, $participant_id, $event_id, $status_id, $campaign_id, $submitValues, $formValues, $lineItem, $priceSetId) {
    try {
      $config = CRM_Core_Config::singleton();

      $fieldsToSkip = array('status', 'status_id', 'role', 'id', 'register_date', 'source', 'registered_by_id', 'campaign_id', 'fee_level');
      $participantRecord = civicrm_api3('Participant', 'getsingle', array('id' => $participant_id));
      $cloneParams = array();
      foreach($participantRecord as $field => $value) {
        if (stripos($field, 'participant_') === 0) {
          $field = substr($field, 12);
          if (!in_array($field, $fieldsToSkip)) {
            $cloneParams[$field] = $value;
          }
        } elseif (stripos($field, 'custom_')===0) {
          $customFieldId = substr($field, 7);
          // Skik custom fields in the format of custom_5_2751
          // Only process a custom field when the name is custom_5.
          if (stripos($customFieldId, '_') > 0) {
            continue;
          }

          if (is_array($value) && isset($value['data'])) {
            // This a file custom field.
            // We pass the file ID.
            // But only if it is not empty.
            if (isset($value['fid']) && !empty($value['fid'])) {
              $cloneParams[$field] = $value['fid'];
            }
          } else {
            $cloneParams[$field] = $value;
          }
        }
      }
      $cloneParams['contact_id'] = $participantRecord['contact_id'];
      $cloneParams['event_id'] = $event_id;
      $cloneParams['status_id'] = $status_id;
      $cloneParams['campaign_id'] = $campaign_id;
      $cloneParams['source'] = E::ts('Cloned from %event', array('%event' => $participantRecord['event_title']));
      $participantApiResult = civicrm_api3('Participant', 'create', $cloneParams);

      $contribution = self::addContribution($participantRecord['contact_id'],$event_id, $participantApiResult['id'], $submitValues, $formValues, $lineItem, $priceSetId);

      $session = CRM_Core_Session::singleton();
      $count = $session->get('cloneeventregistration_count');
      $count ++;
      $session->set('cloneeventregistration_count', $count);

    } catch (Exception $e) {
      // Do nothing.
      throw $e;
      throw new Exception('Error during cloning of event registration: '.$e->getMessage());
    }

    return true;
  }

  public static function onEnd() {
    $session = CRM_Core_Session::singleton();
    $count = $session->get('cloneeventregistration_count');
    $msg = E::ts('%1 event registration cloned.', array(1 => $count, 'plural' => '%1 event registration cloned.'));
    CRM_Core_Session::setStatus($msg, E::ts('Event registrations cloned '), 'success');

    $session = CRM_Core_Session::singleton();
  }

  protected static function addContribution($contactID, $eventID, $participant_id, $submitValues, $formValues, $lineItem, $priceSetId) {
    $config = CRM_Core_Config::singleton();
    $participantStatus = CRM_Event_PseudoConstant::participantStatus();
    $now = date('YmdHis');
    $receiptDate = NULL;

    $eventTitle = CRM_Core_DAO::getFieldValue('CRM_Event_DAO_Event', $eventID, 'title');
    $userName = CRM_Core_Session::singleton()->getLoggedInContactDisplayName();

    if (!empty($formValues['event']['is_email_confirm'])) {
      $receiptDate = $now;
    }

    $lineItem = array();
    $lineItem[0] = array();

    CRM_Price_BAO_PriceSet::processAmount($formValues['fee'], $submitValues, $lineItem[0]);
    //CRM-11529 for quick config backoffice transactions
    //when financial_type_id is passed in form, update the
    //lineitems with the financial type selected in form
    $submittedFinancialType = CRM_Utils_Array::value('financial_type_id', $submitValues);
    $isPaymentRecorded = CRM_Utils_Array::value('record_contribution', $submitValues);
    if ($isPaymentRecorded && $submittedFinancialType) {
      foreach ($lineItem[0] as &$values) {
        $values['financial_type_id'] = $submittedFinancialType;
      }
    }

    $contribParams = array(
      'contact_id' => $contactID,
      'financial_type_id' => !empty($formValues['event']['financial_type_id']) ? $formValues['event']['financial_type_id'] : $submitValues['financial_type_id'],
      'receive_date' => $now,
      'total_amount' => $submitValues['total_amount'],
      'tax_amount' => $submitValues['tax_amount'],
      'amount_level' => $submitValues['amount_level'],
      'invoice_id' => $submitValues['invoiceID'],
      'currency' => $submitValues['currencyID'],
      'source' => !empty($submitValues['participant_source']) ? $submitValues['participant_source'] : $submitValues['description'],
      'is_pay_later' => CRM_Utils_Array::value('is_pay_later', $submitValues, 0),
      'campaign_id' => CRM_Utils_Array::value('campaign_id', $submitValues),
      'card_type_id' => CRM_Utils_Array::value('card_type_id', $submitValues),
      'pan_truncation' => CRM_Utils_Array::value('pan_truncation', $submitValues),
      'contribution_status_id' => $submitValues['contribution_status_id'],
    );

    $submitValues['fee_level'] = $submitValues['amount_level'];
    $contribParams['total_amount'] = $submitValues['amount'];
    if (!empty($submitValues['total_amount']) && $submitValues['status_id'] != array_search('Partially paid', $participantStatus)
    ) {
      $submitValues['fee_amount'] = $submitValues['total_amount'];
    }
    else {
      //fix for CRM-3086
      $submitValues['fee_amount'] = $submitValues['amount'];
    }

    $amountOwed = NULL;
    if (isset($params['amount'])) {
      $amountOwed = $submitValues['amount'];
      unset($submitValues['amount']);
    }

    // overwrite actual payment amount if entered
    if (!empty($submitValues['total_amount'])) {
      $contribParams['total_amount'] = CRM_Utils_Array::value('total_amount', $submitValues);
    }

    if (CRM_Contribute_BAO_Contribution::checkContributeSettings('deferred_revenue_enabled')) {
      $eventStartDate = CRM_Core_DAO::getFieldValue('CRM_Event_DAO_Event', $eventID, 'start_date');
      if (strtotime($eventStartDate) > strtotime(date('Ymt'))) {
        $contribParams['revenue_recognition_date'] = date('Ymd', strtotime($eventStartDate));
      }
    }

    //build contribution params
    if (empty($submitValues['source'])) {
      $contribParams['source'] = ts('%1 : Offline registration (by %2)', array(
        1 => $eventTitle,
        2 => $userName,
      ));
    }
    else {
      $contribParams['source'] = $submitValues['source'];
    }

    $contribParams['currency'] = $config->defaultCurrency;
    $contribParams['non_deductible_amount'] = 'null';
    $contribParams['receipt_date'] = !empty($submitValues['send_receipt']) ? CRM_Utils_Date::processDate(CRM_Utils_Array::value('receive_date', $submitValues)) : 'null';
    $contribParams['contact_id'] = $contactID;
    // @todo change receive_date to datepicker field. Strip out all wrangling.
    $contribParams['receive_date'] = CRM_Utils_Date::processDate($submitValues['receive_date']);
    $recordContribution = array(
      'financial_type_id',
      'payment_instrument_id',
      'trxn_id',
      'contribution_status_id',
      'check_number',
      'campaign_id',
      'pan_truncation',
      'card_type_id',
    );

    foreach ($recordContribution as $f) {
      $contribParams[$f] = CRM_Utils_Array::value($f, $submitValues);
    }

    //insert financial type name in receipt.
    $contribParams['skipLineItem'] = 1;
    // Set is_pay_later flag for back-office offline Pending status contributions
    if ($contribParams['contribution_status_id'] == CRM_Core_PseudoConstant::getKey('CRM_Contribute_DAO_Contribution', 'contribution_status_id', 'Pending')) {
      $contribParams['is_pay_later'] = 1;
    }
    elseif ($contribParams['contribution_status_id'] == CRM_Core_PseudoConstant::getKey('CRM_Contribute_DAO_Contribution', 'contribution_status_id', 'Completed')) {
      $contribParams['is_pay_later'] = 0;
    }

    if ($submitValues['status_id'] == array_search('Partially paid', $participantStatus)) {
      // CRM-13964 partial_payment_total
      if ($amountOwed > $submitValues['total_amount']) {
        // the owed amount
        $contribParams['partial_payment_total'] = $amountOwed;
        // the actual amount paid
        $contribParams['partial_amount_to_pay'] = $submitValues['total_amount'];
      }
    }

    if (CRM_Utils_Array::value('tax_amount', $submitValues)) {
      $contribParams['tax_amount'] = $submitValues['tax_amount'];
    }

    $contribution = CRM_Contribute_BAO_Contribution::create($contribParams);

    $ppDAO = new CRM_Event_DAO_ParticipantPayment();
    $ppDAO->participant_id = $participant_id;
    $ppDAO->contribution_id = $contribution->id;
    $ppDAO->save();

    if ($lineItem) {
      foreach ($lineItem as $key => $value) {
        if (is_array($value) && $value != 'skip') {
          foreach ($value as $lineKey => $line) {
            //10117 update the line items for participants if contribution amount is recorded
            if (!empty($submitValues['total_amount']) && ($submitValues['status_id'] != array_search('Partially paid', $participantStatus))) {
              $line['unit_price'] = $line['line_total'] = $submitValues['total_amount'];
              if (!empty($submitValues['tax_amount'])) {
                $line['unit_price'] = $line['unit_price'] - $submitValues['tax_amount'];
                $line['line_total'] = $line['line_total'] - $submitValues['tax_amount'];
              }
            }
            $lineItem[$priceSetId][$lineKey] = $line;
          }
          CRM_Price_BAO_LineItem::processPriceSet($participant_id, $lineItem, $contribution, 'civicrm_participant');
          CRM_Contribute_BAO_Contribution::addPayments(array($contribution));
        }
      }
    }

    return $contribution;
  }



}