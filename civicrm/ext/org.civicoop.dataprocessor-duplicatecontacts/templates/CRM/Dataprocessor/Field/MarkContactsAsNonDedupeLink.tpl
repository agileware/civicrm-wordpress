{literal}
<script type="text/javascript">
function process_non_dedupe_{/literal}{$id}{literal}() {
  CRM.confirm({"message": '{/literal}{$confirm_text|escape}{literal}'}).on('crmConfirm:yes', function () {
    CRM.api3('Exception', 'create', {"contact_id1": {/literal}{$cid1}{literal}, "contact_id2": {/literal}{$cid2}{literal}}).then(function(result) {
      CRM.$('#{/literal}{$id}{literal}').html('{/literal}{$marked_text|escape}{literal}');
    });
  });
}

</script>
{/literal}

<span id="{$id}">
<a href="javascript:process_non_dedupe_{$id}();">
  {$link_text}
</a>
</span>
