{crmScope extensionKey='dataprocessor-duplicatecontacts'}
  <div class="crm-section">
      <div class="label">{$form.contact_id_1_field.label}</div>
      <div class="content">{$form.contact_id_1_field.html}</div>
      <div class="clear"></div>
  </div>
  <div class="crm-section">
    <div class="label">{$form.contact_id_2_field.label}</div>
    <div class="content">{$form.contact_id_2_field.html}</div>
    <div class="clear"></div>
  </div>
{/crmScope}
