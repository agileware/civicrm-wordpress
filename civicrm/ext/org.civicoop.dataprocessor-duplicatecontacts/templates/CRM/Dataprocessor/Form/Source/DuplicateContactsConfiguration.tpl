{crmScope extensionKey='dataprocessor-duplicatecontacts'}
<div class="crm-accordion-wrapper">
  <div class="crm-accordion-header">{ts}Configuration{/ts}</div>
  <div class="crm-accordion-body">
    <div class="help-block" id="help">
      <p>{ts}Be carefull with the duplicate contacts source as it will first search for all duplicates and load them into memory.{/ts}</p>
    </div>

    <div class="crm-section">
      <div class="label">{$form.rule_group_id.label}</div>
      <div class="content">{$form.rule_group_id.html}</div>
      <div class="clear"></div>
    </div>
  </div>
</div>
{/crmScope}
