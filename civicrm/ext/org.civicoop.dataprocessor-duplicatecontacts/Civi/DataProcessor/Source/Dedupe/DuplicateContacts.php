<?php
/**
 * @author Jaap Jansma <jaap.jansma@civicoop.org>
 * @license AGPL-3.0
 */

namespace Civi\DataProcessor\Source\Dedupe;

use Civi\DataProcessor\Source\AbstractSource;
use Civi\DataProcessor\DataFlow\InMemoryDataFlow;
use Civi\DataProcessor\DataSpecification\DataSpecification;
use Civi\DataProcessor\DataSpecification\FieldSpecification;

use CRM_DataprocessorDuplicatecontacts_ExtensionUtil as E;

class DuplicateContacts extends AbstractSource {

  /**
   * @var \Civi\DataProcessor\DataSpecification\DataSpecification
   */
  protected $availableFields;

  protected $rules = [];

  /**
   * @var \CRM_Utils_Cache_Interface
   */
  protected $cache;

  /**
   * @var int
   */
  protected $cacheTtl = 300; // Cache timout in seconds


  /**
   * Initialize the join
   *
   * @return void
   */
  public function initialize() {
    if (empty($this->cache)) {
      $this->cache = \Civi::cache('long');
    }
    if ($this->dataFlow) {
      return;
    }
    $this->dataFlow = new InMemoryDataFlow\CallbackDataFlow([$this, 'retrieveData']);
  }

  public function retrieveData(InMemoryDataFlow\CallbackDataFlow $dataFlow) {
    $cids = [];
    foreach($dataFlow->getFilters() as $filter) {
      if ($filter instanceof InMemoryDataFlow\SimpleFilter && $filter->getField() == 'contact_id_1' && ($filter->getOperator() == '=' || $filter->getOperator() == 'IN')) {
        $value = $filter->getValue();
        if (is_array($value)) {
          $cids = $value;
        } else {
          $cids[] = $value;
        }
        $dataFlow->removeFilter($filter);
      }
    }
    $checkPermissions = FALSE;

    $data = [];
    if (count($cids)) {
      $cacheKey = $this->sourceName . '_'.md5(implode('_', $cids));
      $cachedData = $this->cache->get($cacheKey);
      if ($cachedData !== null) {
        return $cachedData;
      }
      foreach ($this->configuration['rule_group_id'] as $rule_group_name) {
        $rule_group_id = civicrm_api3('RuleGroup', 'getvalue', ['return' => 'id', 'name' => $rule_group_name]);
        $rule_title = civicrm_api3('RuleGroup', 'getvalue', ['return' => 'title', 'name' => $rule_group_name]);
        $duplicateContacts = \CRM_Dedupe_Finder::dupes($rule_group_id, $cids, $checkPermissions);
        foreach ($duplicateContacts as $duplicateContact) {
          $cid1 = $duplicateContact[0];
          $cid2 = $duplicateContact[1];
          if (in_array($cid2, $cids)) {
            // Switch around the duplicates as we have
            // been searching for a duplicates of a specific contact set.
            $cid1 = $duplicateContact[1];
            $cid2 = $duplicateContact[0];
          }
          $data[] = [
            'contact_id_1' => $cid1,
            'contact_id_2' => $cid2,
            'weight' => $duplicateContact[2],
            'rule_group_id' => $rule_group_id,
            'rule_title' => $rule_title,
          ];
        }
      }
      $this->cache->set($cacheKey, $data, $this->cacheTtl);
    }
    return $data;
  }

  /**
   * @return \Civi\DataProcessor\DataSpecification\DataSpecification
   */
  public function getAvailableFields() {
    if (!$this->availableFields) {
      $this->availableFields = new DataSpecification();
      $this->availableFields->addFieldSpecification('contact_id_1', new FieldSpecification('contact_id_1', 'Int', E::ts('Duplicate Contact ID 1')));
      $this->availableFields->addFieldSpecification('contact_id_2', new FieldSpecification('contact_id_2', 'Int', E::ts('Duplicate Contact ID 2')));
      $this->availableFields->addFieldSpecification('weight', new FieldSpecification('weight', 'Int', E::ts('Weight')));
      $this->availableFields->addFieldSpecification('rule_group_id', new FieldSpecification('rule_group_id', 'Int', E::ts('Dedupe Rule ID'), self::getRuleGroups()));
      $this->availableFields->addFieldSpecification('rule_title', new FieldSpecification('rule_title', 'String', E::ts('Dedupe Rule Title')));
    }
    return $this->availableFields;
  }

  /**
   * Returns an array with the names of required configuration filters.
   * Those filters are displayed as required to the user
   *
   * @return array
   */
  protected function requiredConfigurationFilters() {
    return array('rule_group_id');
  }

  /**
   * Get a list fo dedupe rules.
   *
   * @return array
   * @throws \CiviCRM_API3_Exception
   */
  protected static function getRuleGroups() {
    static $rules;
    if (!$rules) {
      $rules = [];
      $ruleGroupsApi = civicrm_api3('RuleGroup', 'get', ['options' => ['limit' => 0]]);
      foreach ($ruleGroupsApi['values'] as $ruleGroup) {
        $rules[$ruleGroup['name']] = $ruleGroup['title']. ' ('.$ruleGroup['contact_type'].')';
      }
    }
    return $rules;
  }

  /**
   * Returns true when this source has additional configuration
   *
   * @return bool
   */
  public function hasConfiguration() {
    return TRUE;
  }

  /**
   * When this source has additional configuration you can add
   * the fields on the form with this function.
   *
   * @param \CRM_Core_Form $form
   * @param array $source
   */
  public function buildConfigurationForm(\CRM_Core_Form $form, $source=array()) {

    $form->add('select', "rule_group_id", E::ts('Dedupe Rule'), self::getRuleGroups(), true, array(
      'style' => 'min-width:250px',
      'class' => 'crm-select2 huge',
      'multiple' => 'multiple',
      'placeholder' => E::ts('- select -'),
    ));

    $defaults = array();
    if (isset($source['configuration']['rule_group_id'])) {
      $defaults['rule_group_id'] = $source['configuration']['rule_group_id'];
    }
    $form->setDefaults($defaults);
  }

  /**
   * Process the submitted values and create a configuration array
   *
   * @param $submittedValues
   * @return array
   */
  public function processConfiguration($submittedValues) {
    $configuration = array();
    $configuration['rule_group_id'] = $submittedValues['rule_group_id'];
    return $configuration;
  }

  /**
   * When this source has configuration specify the template file name
   * for the configuration form.
   *
   * @return false|string
   */
  public function getConfigurationTemplateFileName() {
    return "CRM/Dataprocessor/Form/Source/DuplicateContactsConfiguration.tpl";
  }


}
