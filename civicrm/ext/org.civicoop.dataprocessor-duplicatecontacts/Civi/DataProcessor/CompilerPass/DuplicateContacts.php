<?php
/**
 * @author Jaap Jansma <jaap.jansma@civicoop.org>
 * @license AGPL-3.0
 */

namespace Civi\DataProcessor\CompilerPass;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;

use CRM_DataprocessorDuplicatecontacts_ExtensionUtil as E;

class DuplicateContacts implements CompilerPassInterface {

  /**
   * You can modify the container here before it is dumped to PHP code.
   */
  public function process(ContainerBuilder $container) {
    if (!$container->hasDefinition('data_processor_factory')) {
      return;
    }

    $factoryDefinition = $container->getDefinition('data_processor_factory');
    $factoryDefinition->addMethodCall('addDataSource', array('duplicate_contacts', 'Civi\DataProcessor\Source\Dedupe\DuplicateContacts', E::ts('Duplicate contacts')));
    $factoryDefinition->addMethodCall('addOutputHandler', array('merge_contact_link', 'Civi\DataProcessor\FieldOutputHandler\MergeContactLink', E::ts('Merge Contacts Link')));
    $factoryDefinition->addMethodCall('addOutputHandler', array('mark_contacts_as_non_dedupe_link', 'Civi\DataProcessor\FieldOutputHandler\MarkContactsAsNonDedupeLink', E::ts('Mark Contacts as non-duplicate Link')));
  }


}
