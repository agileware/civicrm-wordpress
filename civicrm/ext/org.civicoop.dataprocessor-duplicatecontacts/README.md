# DataProcessor Duplicate Contacts

This extension contains the following addition to the data processor extension:
* A source for **Duplicate contacts**.
* A field with a link to merge two contacts.
* A field for marking two contacts as not a duplicate.

Below an example:

![Screenshot](images/screenshot.png)

The extension is licensed under [AGPL-3.0](LICENSE.txt).

## Requirements

* PHP v7.0+
* CiviCRM
* DataProcessor (version 1.7.0)
