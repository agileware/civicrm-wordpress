<?php
/**
 * Copyright (C) 2021  Jaap Jansma (jaap.jansma@civicoop.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace Civi\Documents\DataProcessor\FieldOutputHandler;

use Civi\DataProcessor\FieldOutputHandler\AbstractSimpleFieldOutputHandler;
use Civi\DataProcessor\FieldOutputHandler\FieldOutput;
use Civi\DataProcessor\FieldOutputHandler\HTMLFieldOutput;
use CRM_Documents_ExtensionUtil as E;

class CaseDocuments extends AbstractSimpleFieldOutputHandler {

  /**
   * @var bool
   */
  protected $returnUrl = false;

  /**
   * @var bool
   */
  protected $public = false;

  /**
   * @var array
   */
  protected $typeIds = [];

  /**
   * @var array
   */
  protected $statusIds = [];

  /**
   * Returns the label of the field for selecting a field.
   *
   * This could be override in a child class.
   *
   * @return string
   */
  protected function getFieldTitle() {
    return E::ts('Case ID Field');
  }

  /**
   * Returns the data type of this field
   *
   * @return String
   */
  protected function getType() {
    return 'String';
  }

  /**
   * Returns the formatted value
   *
   * @param $rawRecord
   * @param $formattedRecord
   *
   * @return \Civi\DataProcessor\FieldOutputHandler\FieldOutput
   */
  public function formatField($rawRecord, $formattedRecord) {
    $caseId = $rawRecord[$this->inputFieldSpec->alias];
    $repo = \CRM_Documents_Entity_DocumentRepository::singleton();
    $documents = $repo->getDocumentsByCaseId($caseId);
    $raw = '';
    $html = '';
    foreach($documents as $document) {
      if (count($this->typeIds) && !in_array($document->getTypeId(), $this->typeIds)) {
        continue;
      }
      if (count($this->statusIds) && !in_array($document->getStatusId(), $this->statusIds)) {
        continue;
      }
      $url = $document->getCurrentVersion()->getAttachment()->url;
      if ($this->public && $url) {
        // Generate a frontend link.
        $fileEntityId = $document->getCurrentVersion()->getId();
        $fileId = $document->getCurrentVersion()->getAttachment()->fileID;
        $fileHash = \CRM_Core_BAO_File::generateFileHash($fileEntityId, $fileId);
        $url = \CRM_Utils_System::url('civicrm/file', "reset=1&id={$fileId}&eid={$fileEntityId}&fcs={$fileHash}", TRUE, NULL, TRUE, TRUE);
      }

      if ($url) {
        if (strlen($raw)) {
          $raw .= "\r\n";
        }
        $raw .= $url;
        if (!$this->returnUrl) {
          if (strlen($html)) {
            $html .= '<br>';
          }
          $html .= '<a href="' . $url . '" title="' . $document->getCurrentVersion()->getAttachment()->cleanname . '"><i class="crm-i ' . $document->getIcon() . '">&nbsp;</i>' . $document->getSubject() . '</a>';
        }
      }
    }

    if ($this->returnUrl) {
      $output = new FieldOutput($raw);
    }
    else {
      $output = new HTMLFieldOutput($raw);
      $output->formattedValue = $html;
      $output->setHtmlOutput($html);
    }

    return $output;
  }

  /**
   * Initialize the processor
   *
   * @param String $alias
   * @param String $title
   * @param array $configuration
   * @param \Civi\DataProcessor\ProcessorType\AbstractProcessorType $processorType
   */
  public function initialize($alias, $title, $configuration) {
    parent::initialize($alias, $title, $configuration);
    $this->returnUrl = isset($configuration['return_url']) ? $configuration['return_url'] : false;
    $this->public = isset($configuration['public']) ? $configuration['public'] : false;
    $this->typeIds = isset($configuration['type_id']) ? $configuration['type_id'] : [];
    $this->statusIds = isset($configuration['status_id']) ? $configuration['status_id'] : [];
  }

  /**
   * When this handler has additional configuration you can add
   * the fields on the form with this function.
   *
   * @param \CRM_Core_Form $form
   * @param array $field
   */
  public function buildConfigurationForm(\CRM_Core_Form $form, $field=array()) {
    parent::buildConfigurationForm($form, $field);
    $form->add('checkbox', 'return_url', E::ts('Only return URL'));
    $form->add('checkbox', 'public', E::ts('Public link'));

    $types = \CRM_Core_OptionGroup::values('document_type');
    $form->add('select', 'type_id', E::ts('Type'), $types, FALSE, ['class' => 'huge crm-select2', 'multiple' => TRUE]);

    $statuses = \CRM_Core_OptionGroup::values('document_status');
    $form->add('select', 'status_id', E::ts('Status'), $statuses, FALSE, ['class' => 'huge crm-select2', 'multiple' => TRUE]);

    if (isset($field['configuration'])) {
      $configuration = $field['configuration'];
      $defaults = array();
      if (isset($configuration['return_url'])) {
        $defaults['return_url'] = $configuration['return_url'];
      }
      if (isset($configuration['public'])) {
        $defaults['public'] = $configuration['public'];
      }
      if (isset($configuration['type_id'])) {
        $defaults['type_id'] = $configuration['type_id'];
      }
      if (isset($configuration['status_id'])) {
        $defaults['status_id'] = $configuration['status_id'];
      }
      $form->setDefaults($defaults);
    }
  }

  /**
   * When this handler has configuration specify the template file name
   * for the configuration form.
   *
   * @return false|string
   */
  public function getConfigurationTemplateFileName() {
    return "CRM/Documents/DataProcessor/FieldoutputHandler/Configuration/CaseDocuments.tpl";
  }


  /**
   * Process the submitted values and create a configuration array
   *
   * @param $submittedValues
   * @return array
   */
  public function processConfiguration($submittedValues) {
    $configuration = parent::processConfiguration($submittedValues);
    $configuration['return_url'] = isset($submittedValues['return_url']) ? $submittedValues['return_url'] : false;
    $configuration['public'] = isset($submittedValues['public']) ? $submittedValues['public'] : false;
    $configuration['type_id'] = isset($submittedValues['type_id']) ? $submittedValues['type_id']: [];
    $configuration['status_id'] = isset($submittedValues['status_id']) ? $submittedValues['status_id']: [];
    return $configuration;
  }

}
