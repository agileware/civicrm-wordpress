<?php
/**
 * Copyright (C) 2022  Jaap Jansma (jaap.jansma@civicoop.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace Civi\CaseArchive\Documents\Entity;

use Civi\CaseArchive\ArchiveFile;
use Civi\CaseArchive\Documents\Section\DocumentSection;
use Civi\CaseArchive\Documents\Section\DocumentVersionsSection;
use Civi\CaseArchive\Entity\AbstractEntity;
use CRM_Documents_ExtensionUtil as E;

class Document extends AbstractEntity {

  /**
   * Returns the title
   *
   * @return string
   */
  public function getTitle(): string {
    return E::ts('Document');
  }

  /**
   * Returns the Name
   *
   * @return string
   */
  public function getName(): string {
    return 'document_'.$this->getEntityId();
  }

  /**
   * Returns the entity, e.g. Case or Contact.
   *
   * @return string
   */
  public function getEntityType(): string {
    return 'Document';
  }

  /**
   * Method called when an archive file is prepared
   *
   * @param \Civi\CaseArchive\ArchiveFile $archiveFile
   *
   * @return void
   */
  public function onPrepare(ArchiveFile $archiveFile) {
    $parentHtmlFile = $this->getHtmlFile();
    $this->htmlFile->setTitle($this->getTitle());
    $archiveFile->addSection(new DocumentSection($this));
    $archiveFile->addSections($this->customData->getSections());
    $archiveFile->addSection(new DocumentVersionsSection($this));
  }

}


