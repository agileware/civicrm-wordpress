<?php
/**
 * Copyright (C) 2022  Jaap Jansma (jaap.jansma@civicoop.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace Civi\CaseArchive\Documents\Entity;

use Civi\CaseArchive\ArchiveFile;
use Civi\CaseArchive\ArchiveFile\SubHtmlFile;
use Civi\CaseArchive\Documents\Section\CaseDocumentSection;
use Civi\CaseArchive\Entity\CaseEntity;
use CRM_Documents_Entity_DocumentRepository;

class CaseDocuments {

  /**
   * @var \Civi\CaseArchive\Entity\CaseEntity
   */
  private $entity;


  private $documents;

  public function __construct(CaseEntity $entity) {
    $this->entity = $entity;
  }

  /**
   * Method called when an archive file is prepared
   *
   * @param \Civi\CaseArchive\ArchiveFile $archiveFile
   *
   * @return void
   */
  public function onPrepare(ArchiveFile $archiveFile) {
    $parentHtmlFile = $this->entity->getHtmlFile();
    $archivePath = $parentHtmlFile->getArchivePath() . "/documents";
    $relativePath = $parentHtmlFile->getRootPath() . $parentHtmlFile->getArchivePath() . "/documents/";
    $archiveFile->addSection(new CaseDocumentSection($this->entity, $this, $relativePath));
    foreach($this->getDocuments() as $document) {
      /** @var int $docId */
      $docId = $document->getId();
      $documentFile = new SubHtmlFile($this->getFileName($docId), $archivePath, $parentHtmlFile);
      $documentFile->setAttachmentPath($archivePath.'/document_'.$docId);
      $documenEntity = new Document($docId, $documentFile);
      $documenEntity->onPrepare($archiveFile);
      $archiveFile->addHtmlFile($documentFile);
    }
  }

  /**
   * @return \CRM_Documents_Entity_Document[]
   */
  public function getDocuments(): array {
    if (!$this->documents) {
      $documentRepo = CRM_Documents_Entity_DocumentRepository::singleton();
      $this->documents = $documentRepo->getDocumentsByCaseId($this->entity->getEntityId());
    }
    return $this->documents;
  }

  /**
   * @param int $documentId
   * @return string
   */
  public function getFileName(int $documentId): string {
    return 'document_' . $documentId . '.html';
  }

}


