<?php
/**
 * Copyright (C) 2022  Jaap Jansma (jaap.jansma@civicoop.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace Civi\CaseArchive\Documents\Section;

use Civi\CaseArchive\ArchiveFile;
use Civi\CaseArchive\Section\AbstractSection;
use Civi\CaseArchive\ArchiveFile\FileAttachment;
use Civi\CaseArchive\Documents\Entity\Document;
use CRM_Documents_ExtensionUtil as E;
use CRM_Documents_Entity_Document;
use CRM_Documents_Entity_DocumentRepository;
use Exception;

class DocumentVersionsSection extends AbstractSection {

  /**
   * @var CRM_Documents_Entity_Document
   */
  private $document;

  protected $templateFileName = 'CRM/CaseArchive/Documents/DocumentVersionsSection.tpl';


  /**
   * Set the template file for this section.
   *
   * @param string $templateFileName
   *
   * @return void
   */
  public function setTemplateFileName(string $templateFileName) {
    $this->templateFileName = $templateFileName;
  }

  /**
   * @return string
   */
  public function getSectionTitle(): string {
    return E::ts('Versions');
  }

  /**
   * Method called when an archive file is build
   *
   * @param \Civi\CaseArchive\ArchiveFile $archiveFile
   *
   * @return void
   */
  public function onBuild(ArchiveFile $archiveFile) {
    $rootPath = $this->entity->getHtmlFile()->getRootPath();
    $document = $this->getDocument();
    if ($document) {
      $versions = [];
      foreach($document->getVersions() as $version) {
        $archiveAttachmentPath = $this->entity->getHtmlFile()->getAttachmentPath() . '/version_'.$version->getVersion();
        $attachment = new FileAttachment($version->getAttachment()->fileID, $version->getAttachment()->filename, $archiveAttachmentPath);
        $archiveFile->addAttachment($attachment);
        $versions[] = [
          'link' => $rootPath.$attachment->getRelativePath(),
          'filename' => $version->getAttachment()->cleanname,
          'description' => $version->getDescription(),
          'version' => $version->getVersion(),
          'updated_by' => $version->getFormattedUpdatedBy(FALSE),
          'updated_at' => $version->getFormattedDateUpdated(),
        ];
      }

      $vars = [
        'versions' => $versions,
      ];
      $this->content = $this->fetchTemplate($vars, $this->templateFileName);
      $name = 'documentVersions_' . $this->entity->getEntityId();
      $this->entity->getHtmlFile()
        ->addContentSection($this->getSectionTitle(), $this->content, $name);
    }
  }

  /**
   * @return \CRM_Documents_Entity_Document
   */
  protected function getDocument(): ?CRM_Documents_Entity_Document {
    if (!$this->document && $this->entity instanceof Document) {
      try {
        $docId = $this->entity->getEntityId();
        $documentRepo = CRM_Documents_Entity_DocumentRepository::singleton();
        $this->document = $documentRepo->getDocumentById($docId);
      } catch (Exception $e) {
      }
    }
    return $this->document;
  }


}
