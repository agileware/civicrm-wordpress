<?php
/**
 * Copyright (C) 2022  Jaap Jansma (jaap.jansma@civicoop.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace Civi\CaseArchive\Documents;

use \CRM_Documents_Entity_Document;
use CRM_Documents_Utils_Formatter;

class DocumentFacade {

  /**
   * @var \CRM_Documents_Entity_Document
   */
  private $document;

  /**
   * @var string
   */
  private $attachmentLink;

  /**
   * @var string
   */
  private $detailUrl;

  /**
   * @param \CRM_Documents_Entity_Document $document
   * @param string $attachmentLink
   * @param string $detailUrl
   */
  public function __construct(CRM_Documents_Entity_Document $document, string $attachmentLink, string $detailUrl='') {
    $this->document = $document;
    $this->attachmentLink = $attachmentLink;
    $this->detailUrl = $detailUrl;
  }

  /**
   * @return \CRM_Documents_Entity_Document
   */
  public function getDocument(): CRM_Documents_Entity_Document {
    return $this->document;
  }

  /**
   * @return string
   */
  public function getSubject(): string {
    return $this->document->getSubject();
  }

  /**
   * @return string
   */
  public function getFormattedContacts(): string {
    $formatter = CRM_Documents_Utils_Formatter::singleton();
    $contacts = '';
    foreach($this->document->getContactIds() as $cid) {
      if (strlen($contacts)) {
        $contacts .= ', ';
      }
      $contacts .= $formatter->formatContact($cid, FALSE);
    }
    return $contacts;
  }

  /**
   * @return string
   */
  public function getFormattedTypeId(): string {
    return $this->document->getFormattedTypeId();
  }

  /**
   * @return string
   */
  public function getFormattedStatusId(): string {
    return $this->document->getFormattedStatusId();
  }

  /**
   * @return string
   */
  public function getFormattedDateUpdated(): string {
    return $this->document->getFormattedDateUpdated();
  }

  /**
   * @return string
   */
  public function getFormattedUpdatedBy(): string {
    return $this->document->getFormattedUpdatedBy(FALSE);
  }

  /**
   * @return string
   */
  public function getLink(): string {
    return '<a href="'.$this->attachmentLink.'">' . $this->document->getCurrentVersion()->getAttachment()->cleanname.'</a>';
  }

  /**
   * @return string
   */
  public function getDetailUrl(): string {
    return $this->detailUrl;
  }

}
