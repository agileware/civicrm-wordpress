<?php
/**
 * @author Jaap Jansma (CiviCooP) <jaap.jansma@civicoop.org>
 * @license http://www.gnu.org/licenses/agpl-3.0.html
 */
class CRM_Documents_Utils_HookInvoker {

  private static $singleton;

  private function __construct() {

  }

  /**
   * @return \CRM_Documents_Utils_HookInvoker
   */
  public static function singleton() {
    if (!self::$singleton) {
      self::$singleton = new CRM_Documents_Utils_HookInvoker();
    }
    return self::$singleton;
  }

  /**
   * Returns an array with instances of classes which implements the interface CRM_Documents_Interface_EntityRefSpec
   *
   * @return array
   */
  public function hook_civicrm_documents_entity_ref_spec() {
    $null = NULL;
    return CRM_Utils_Hook::singleton()->invoke([], $null, $null, $null, $null, $null, $null, 'civicrm_documents_entity_ref_spec');
  }

  /**
   * This hook is called the retrieve the status of a document.
   *
   * A status indicates if a document is (un)used and therefore should be kept or could be deleted
   *
   * @param $doc The document
   * @param $status 0 is unused document. 1 is document is in use.
   * @return mixed
   */
  public function hook_civicrm_documents_get_status($doc, &$status) {
    $null = NULL;
    return CRM_Utils_Hook::singleton()->invoke(['doc', 'status'], $doc, $status, $null, $null, $null, $null, 'civicrm_documents_get_status');
  }

}
