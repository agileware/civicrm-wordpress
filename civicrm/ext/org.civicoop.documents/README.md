# Document storage in CiviCRM

## Functionality

* Store Documents on the Contact Card
* A Document can be linked to more than one Contact
* Version management with a Document
* Custom search to find Documents

## Technical background

There is an entity **CRM_Documents_Entity_Document** which contains 
all the information for a Document. E.g. the linked Contact ID's. 
Every Document contains one or more **CRM_Documents_Entity_DocumentVersion** 
for a version of the document. A Document version contains a link to the file
which is a **civicrm_entity_file** item.

## Hooks

See [available hooks](doc/hooks.md) for the documentation of the hooks in this extension

## Drupal modules

This extension includes two Drupal modules
* views_civicrm_documents
* webform_civicrm_documents

## Roadmap

### Next beta release

* Search Document by case type
* Store the user context upon search (e.g. for going back on a edit form)
* Removing Document on a merge of duplicate Contacts

### Future (dreaming)

* Add file type icons (such as pdf/doc etc...)
* Add tagging to to a Document
* Search with Tags
* Add hooks for linking Documents to custum entities (e.g. campaigns)
* Add a connection with ownCloud for interacting with Documents
* Add functionality to work together on a Document with the webODF functionality





