{include file="CRM/Dataprocessor/Form/Field/Configuration/SimpleFieldOutputHandler.tpl"}
{crmScope extensionKey='org.civicoop.documents'}
  <div class="crm-section">
    <div class="label">{$form.type_id.label}</div>
    <div class="content">{$form.type_id.html}</div>
    <div class="clear"></div>
  </div>
  <div class="crm-section">
    <div class="label">{$form.status_id.label}</div>
    <div class="content">{$form.status_id.html}</div>
    <div class="clear"></div>
  </div>
  <div class="crm-section">
    <div class="label">{$form.public.label}</div>
    <div class="content">{$form.public.html}</div>
    <div class="clear"></div>
  </div>
  <div class="crm-section">
    <div class="label">{$form.return_url.label}</div>
    <div class="content">{$form.return_url.html}</div>
    <div class="clear"></div>
  </div>
{/crmScope}
