<?php

use CRM_Msgtplblocker_ExtensionUtil as E;

return [
  'msgtplblocker_types' => [
    'name' => 'msgtplblocker_types',
    'type' => 'Array',
    'default' => [],
    'html_type' => 'select',
    'html_attributes' => [
      'multiple' => 1,
      'class' => 'crm-select2',
    ],
    'is_required' => false,
    'add' => '1.0',
    'title' => E::ts('Message template types to block'),
    'description' => E::ts('This will completely block these workflow messages from being sent. For example, it does not currently make a distinction between a contribution receipt, and the receipt from a recurring contribution receipt (ex: the one sent each month). If you would like to sponsor this development, please contact us (info@symbiotic.coop).'),
    'pseudoconstant' => [
      'callback' => 'CRM_Msgtplblocker_Utils::getOptionsForTemplateList',
    ],
    'is_domain' => 1,
    'is_contact' => 0,
    'settings_pages' => [
      'msgtplblocker' => [
        'weight' => 10,
      ]
    ],
  ],
  'msgtplblocker_blockall' => [
    'name' => 'msgtplblocker_blockall',
    'type' => 'Boolean',
    'html_type' => 'checkbox',
    'default' => 0,
    'add' => '5.13',
    'is_domain' => 1,
    'is_contact' => 0,
    'title' => E::ts('Block all workflow message templates?'),
    'description' => E::ts('Block sending of all workflow message templates (an activity will be recorded with status "Cancelled" and the message will not be emailed out).'),
    'html_attributes' => [],
    'settings_pages' => [
      'msgtplblocker' => [
        'weight' => 5,
      ]
    ],
  ],
  'msgtplblocker_createactivity' => [
    'name' => 'msgtplblocker_createactivity',
    'type' => 'Boolean',
    'html_type' => 'checkbox',
    'default' => 0,
    'add' => '5.13',
    'is_domain' => 1,
    'is_contact' => 0,
    'title' => E::ts('Create an "Email" activity when CiviCRM sends a message template?'),
    'description' => E::ts('Create an "Email" activity for all message templates sent (status="Cancelled" if blocked, "Completed" otherwise).
If an "Email" activity has already been created another one will not be created so you should not end up with duplicates.'),
    'html_attributes' => [],
    'settings_pages' => [
      'msgtplblocker' => [
        'weight' => 15,
      ]
    ],
  ],
];
