# msgtplblocker

Blocks a configurable list of message templates emails. For example, if you do not want
donation receipts to be sent out, you can disable the receipts on the contribution form,
but the recurring payments may still send a receipt depending on the payment gateway.

This extension makes it possible to filter out certain type of message templates (email
notifications) by type and record an "Email" activity when the message template is sent.

Another option (recommended by Eileen) could be to add a `is_active` field to
the `civicrm_msg_template` table and expose an option to enable/disable from the
Workflow / Message Templates screen.

The extension is licensed under [AGPL-3.0](LICENSE.txt).

## Installation

Install as a regular extension.

## Usage

Once enabled, go to /civicrm/admin/setting/msgtplblocker to configure:
* a list of message templates to block.
* Whether to block all message templates.
* Whether to record an activity when CiviCRM sends a message template (status="Cancelled" if blocked, "Completed" otherwise).

## Known Issues

The configuration options of this extension are currently pretty limited. It's a trivial extension,
but we figured we may as well create it as a generic extension, rather than a client-specific extension.
With time, as people request features, it might provide a more fine-grained set of options.

## Support

https://www.symbiotic.coop/en
