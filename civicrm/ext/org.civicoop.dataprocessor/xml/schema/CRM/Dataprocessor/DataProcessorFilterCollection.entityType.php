<?php
// This file declares a new entity type. For more details, see "hook_civicrm_entityTypes" at:
// http://wiki.civicrm.org/confluence/display/CRMDOC/Hook+Reference
return array (
  0 =>
  array (
    'name' => 'DataProcessorFilterCollection',
    'class' => 'CRM_Dataprocessor_DAO_DataProcessorFilterCollection',
    'table' => 'civicrm_data_processor_filter_collection',
  ),
);
