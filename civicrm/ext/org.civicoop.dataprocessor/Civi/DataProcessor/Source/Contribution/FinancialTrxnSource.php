<?php
/**
 * @author Klaas Eikelboom <klaas.eikelboom@civicoop.org>
 * @license AGPL-3.0
 */

namespace Civi\DataProcessor\Source\Contribution;

use Civi\DataProcessor\Source\AbstractCivicrmEntitySource;
use CRM_Dataprocessor_ExtensionUtil as E;

class FinancialTrxnSource extends AbstractCivicrmEntitySource{

  /**
   * Returns the entity name
   *
   * @return String
   */
  protected function getEntity() {
    return 'FinancialTrxn';
  }

  /**
   * Returns the table name of this entity
   *
   * @return String
   */
  protected function getTable() {
    return 'civicrm_financial_trxn';
  }
}
