<?php
/**
 * Copyright (C) 2023  Jaap Jansma (jaap.jansma@civicoop.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace Civi\DataProcessor\Source\Contact;

use Civi\DataProcessor\DataFlow\SqlDataFlow\AndClause;
use Civi\DataProcessor\DataFlow\SqlDataFlow\InTableWhereClause;
use Civi\DataProcessor\DataFlow\SqlDataFlow\OrClause;
use Civi\DataProcessor\DataFlow\SqlDataFlow\SimpleWhereClause;
use Civi\DataProcessor\Source\AbstractCivicrmEntitySource;

abstract class AbstractContactSource extends AbstractCivicrmEntitySource {

  /**
   * Add a sub query filter for the contact type.
   *
   * The subquery performance better than a direct where statement on the contact_type
   * field.
   *
   * @param array $contactTypes
   * @param string $op
   * @param bool $isJoinClause
   *
   * @return void
   * @throws \Exception
   */
  protected function addContactTypeFilter(array $contactTypes, string $op = 'IN', bool $isJoinClause = false) {
    switch ($op) {
      case '=':
        $op = 'IN';
        break;
      case '!=':
        $op = 'NOT IN';
        break;
    }
    if ($op != 'IN' && $op != 'NOT IN') {
      return;
    }
    if (count($contactTypes)) {
      $this->ensureEntity();
      $tableAlias = $this->getEntityTableAlias();
      if (count($contactTypes) == 1) {
        $contactTypeFilter = new SimpleWhereClause($tableAlias, 'contact_type', '=', reset($contactTypes), 'String', $isJoinClause);
      } else {
        $contactTypeFilter = new SimpleWhereClause($tableAlias, 'contact_type', 'IN', $contactTypes, 'String', $isJoinClause);
      }
      $this->entityDataFlow->addWhereClause($contactTypeFilter);
    }
  }

  /**
   * Adds an inidvidual filter to the data source
   *
   * @param $filter_field_alias
   * @param $op
   * @param $values
   *
   * @throws \Exception
   */
  protected function addFilter($filter_field_alias, $op, $values) {
    if ($filter_field_alias == 'contact_type' && !in_array($op, ['IS NULL', 'IS NOT NULL'])) {
      $contactTypes = $values;
      if (!is_array($contactTypes)) {
        $contactTypes = [$contactTypes];
      }
      $this->addContactTypeFilter($contactTypes, $op, TRUE);
    } elseif ($filter_field_alias == 'contact_sub_type' && $op == 'IN') {
      $contactTypeClauses = [];
      foreach ($values as $value) {
        $contactTypeSearchName = '%' . \CRM_Core_DAO::VALUE_SEPARATOR . $value . \CRM_Core_DAO::VALUE_SEPARATOR . '%';
        $contactTypeClauses[] = new SimpleWhereClause($this->getSourceName(), 'contact_sub_type', 'LIKE', $contactTypeSearchName, 'String', TRUE);
      }
      if (count($contactTypeClauses)) {
        $contactTypeClause = new OrClause($contactTypeClauses, TRUE);
        $entityDataFlow = $this->ensureEntity();
        $entityDataFlow->addWhereClause($contactTypeClause);
      }
    } elseif ($filter_field_alias == 'contact_sub_type' && $op == 'NOT IN') {
      $contactTypeClauses = [];
      foreach($values as $value) {
        $contactTypeSearchName = '%'.\CRM_Core_DAO::VALUE_SEPARATOR.$value.\CRM_Core_DAO::VALUE_SEPARATOR.'%';
        $contactTypeClauses[] = new SimpleWhereClause($this->getSourceName(), 'contact_sub_type', 'NOT LIKE', $contactTypeSearchName, 'String',TRUE);
      }
      if (count($contactTypeClauses)) {
        $contactTypeClause = new AndClause($contactTypeClauses, TRUE);
        $entityDataFlow = $this->ensureEntity();
        $entityDataFlow->addWhereClause($contactTypeClause);
      }
    } else {
      parent::addFilter($filter_field_alias, $op, $values);
    }
  }

}
