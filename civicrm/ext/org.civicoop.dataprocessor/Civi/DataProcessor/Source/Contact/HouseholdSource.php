<?php
/**
 * @author Jaap Jansma <jaap.jansma@civicoop.org>
 * @license AGPL-3.0
 */

namespace Civi\DataProcessor\Source\Contact;

use Civi\DataProcessor\DataSpecification\DataSpecification;

class HouseholdSource extends AbstractContactSource {

  protected $skipFields = array(
    'first_name',
    'middle_name',
    'last_name',
    'formal_title',
    'job_title',
    'gender_id',
    'prefix_id',
    'suffix_id',
    'birth_date',
    'organization_name',
    'legal_name',
    'legal_identifier',
    'sic_code',
    'home_URL',
    'is_deceased',
    'deceased_date',
  );

  protected $skipFilterFields = array(
    'contact_type',
    'first_name',
    'middle_name',
    'last_name',
    'formal_title',
    'job_title',
    'gender_id',
    'prefix_id',
    'suffix_id',
    'birth_date',
    'organization_name',
    'legal_name',
    'legal_identifier',
    'sic_code',
    'home_URL',
    'is_deceased',
    'deceased_date',
  );

  /**
   * Returns the entity name
   *
   * @return String
   */
  protected function getEntity(): string {
    return 'Contact';
  }

  /**
   * Returns the table name of this entity
   *
   * @return String
   */
  protected function getTable(): string {
    return 'civicrm_contact';
  }

  /**
   * Returns the default configuration for this data source
   *
   * @return array
   */
  public function getDefaultConfiguration(): array {
    return array(
      'filter' => array(
        'is_deleted' => array (
          'op' => '=',
          'value' => '0',
        )
      )
    );
  }

  /**
   * @return \Civi\DataProcessor\DataSpecification\DataSpecification
   * @throws \Exception
   */
  public function getAvailableFilterFields(): DataSpecification {
    if (!$this->availableFilterFields) {
      $this->availableFilterFields = new DataSpecification();
      $this->loadFields($this->availableFilterFields, $this->skipFilterFields);
      $this->loadCustomGroupsAndFields($this->availableFilterFields, true, 'Household');
    }
    return $this->availableFilterFields;
  }

  /**
   * @return \Civi\DataProcessor\DataSpecification\DataSpecification
   * @throws \Exception
   */
  public function getAvailableFields(): DataSpecification {
    if (!$this->availableFields) {
      $this->availableFields = new DataSpecification();
      $this->loadFields($this->availableFields, $this->skipFields);
      $this->loadCustomGroupsAndFields($this->availableFields, false, 'Household');
    }
    return $this->availableFields;
  }

  /**
   * Add the filters to the where clause of the data flow
   *
   * @param $configuration
   * @throws \Exception
   */
  protected function addFilters($configuration) {
    parent::addFilters($configuration);
    $this->addFilter('contact_type', '=', 'Household');
  }


}
