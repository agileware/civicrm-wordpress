<?php
/**
 * @author Jaap Jansma <jaap.jansma@civicoop.org>
 * @license AGPL-3.0
 */

namespace Civi\DataProcessor\Source\Contact;

use Civi\DataProcessor\DataSpecification\DataSpecification;

class ContactSource extends AbstractContactSource {

  /**
   * Returns the entity name
   *
   * @return String
   */
  protected function getEntity(): string {
    return 'Contact';
  }

  /**
   * Returns the table name of this entity
   *
   * @return String
   */
  protected function getTable(): string {
    return 'civicrm_contact';
  }

  /**
   * @return \Civi\DataProcessor\DataSpecification\DataSpecification
   * @throws \Exception
   */
  public function getAvailableFilterFields(): DataSpecification {
    if (!$this->availableFilterFields) {
      $this->availableFilterFields = new DataSpecification();
      $this->loadFields($this->availableFilterFields);
      $this->loadCustomGroupsAndFields($this->availableFilterFields, true);
      $this->loadCustomGroupsAndFields($this->availableFilterFields, true, 'Individual');
      $this->loadCustomGroupsAndFields($this->availableFilterFields, true, 'Household');
      $this->loadCustomGroupsAndFields($this->availableFilterFields, true, 'Organization');
    }
    return $this->availableFilterFields;
  }

  /**
   * @return \Civi\DataProcessor\DataSpecification\DataSpecification
   * @throws \Exception
   */
  public function getAvailableFields(): DataSpecification {
    if (!$this->availableFields) {
      $this->availableFields = new DataSpecification();
      $this->loadFields($this->availableFields);
      $this->loadCustomGroupsAndFields($this->availableFields, false);
      $this->loadCustomGroupsAndFields($this->availableFields, false, 'Individual');
      $this->loadCustomGroupsAndFields($this->availableFields, false, 'Household');
      $this->loadCustomGroupsAndFields($this->availableFields, false, 'Organization');
    }
    return $this->availableFields;
  }


  /**
   * Returns the default configuration for this data source
   *
   * @return array
   */
  public function getDefaultConfiguration(): array {
    return array(
      'filter' => array(
        'is_deleted' => array (
          'op' => '=',
          'value' => '0',
        )
      )
    );
  }

}
