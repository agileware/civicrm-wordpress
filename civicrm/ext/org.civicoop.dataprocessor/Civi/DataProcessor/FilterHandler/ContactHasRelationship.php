<?php
/**
 * Copyright (C) 2022  Jaap Jansma (jaap.jansma@civicoop.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace Civi\DataProcessor\FilterHandler;

use Civi\DataProcessor\DataFlow\SqlDataFlow;
use Civi\DataProcessor\DataSpecification\FieldSpecification;
use Civi\DataProcessor\Exception\InvalidConfigurationException;
use CiviCRM_API3_Exception;
use CRM_Core_Exception;
use CRM_Core_Form;
use CRM_Dataprocessor_ExtensionUtil as E;
use CRM_Dataprocessor_Utils_DataSourceFields;
use Exception;

class ContactHasRelationship extends AbstractFieldFilterHandler {

  /**
   * @var array
   */
  private $relationshipTypeOptions = null;

  /**
   * @var array
   */
  private $relationshipTypeToId = null;

  protected function getRelationshipTypeOptions(): array {
    if (empty($this->relationshipTypeOptions)) {
      $this->relationshipTypeOptions = [];
      try {
        $relationshipTypeApi = civicrm_api3('RelationshipType', 'get', [
          'options' => ['limit' => 0],
        ]);
        foreach($relationshipTypeApi['values'] as $relationshipType) {
          $this->relationshipTypeOptions['a_b_'.$relationshipType['name_a_b']] = E::ts('Contact A - %1', [1=>$relationshipType['label_a_b']]);
          $this->relationshipTypeOptions['b_a_'.$relationshipType['name_b_a']] = E::ts('Contact B - %1', [1=>$relationshipType['label_b_a']]);
          $this->relationshipTypeToId['a_b_'.$relationshipType['name_a_b']] = $relationshipType['id'];
          $this->relationshipTypeToId['b_a_'.$relationshipType['name_b_a']] = $relationshipType['id'];
        }
      } catch (CiviCRM_API3_Exception $e) {
      }
    }
    return $this->relationshipTypeOptions;
  }

  protected function getRelationshipTypeId($name):? int {
    $this->getRelationshipTypeOptions();
    if (isset($this->relationshipTypeToId[$name])) {
      return $this->relationshipTypeToId[$name];
    }
    return null;
  }


  /**
   * Initialize the filter
   *
   * @throws \Civi\DataProcessor\Exception\DataSourceNotFoundException
   * @throws \Civi\DataProcessor\Exception\InvalidConfigurationException
   * @throws \Civi\DataProcessor\Exception\FieldNotFoundException
   */
  protected function doInitialization() {
    if (!isset($this->configuration['datasource']) || !isset($this->configuration['field'])) {
      throw new InvalidConfigurationException(E::ts("Filter %1 requires a field to filter on. None given.", array(1=>$this->title)));
    }
    $this->initializeField($this->configuration['datasource'], $this->configuration['field']);
  }

  /**
   * Returns true when this filter has additional configuration
   *
   * @return bool
   */
  public function hasConfiguration(): bool {
    return true;
  }

  /**
   * When this filter type has additional configuration you can add
   * the fields on the form with this function.
   *
   * @param \CRM_Core_Form $form
   * @param array $filter
   */
  public function buildConfigurationForm(CRM_Core_Form $form, $filter=array()) {
    $fieldSelect = [];
    try {
      $fieldSelect = CRM_Dataprocessor_Utils_DataSourceFields::getAvailableFilterFieldsInDataSources($filter['data_processor_id']);
    } catch (Exception $e) {
    }

    try {
      $form->add('select', 'contact_id_field', E::ts('Contact ID Field'), $fieldSelect, TRUE, [
        'style' => 'min-width:250px',
        'class' => 'crm-select2 huge',
        'placeholder' => E::ts('- select -'),
      ]);
    } catch (CRM_Core_Exception $e) {
    }

    if (isset($filter['configuration'])) {
      $configuration = $filter['configuration'];
      $defaults = array();
      if (isset($configuration['field']) && isset($configuration['datasource'])) {
        try {
          $defaults['contact_id_field'] = CRM_Dataprocessor_Utils_DataSourceFields::getSelectedFieldValue($filter['data_processor_id'], $configuration['datasource'], $configuration['field']);
        } catch (CiviCRM_API3_Exception $e) {
        }
      }
      $form->setDefaults($defaults);
    }
  }

  /**
   * When this filter type has configuration specify the template file name
   * for the configuration form.
   *
   * @return false|string
   */
  public function getConfigurationTemplateFileName():? string {
    return "CRM/Dataprocessor/Form/Filter/Configuration/ContactHasRelationship.tpl";
  }


  /**
   * Process the submitted values and create a configuration array
   *
   * @param $submittedValues
   * @return array
   */
  public function processConfiguration($submittedValues): array {
    [$datasource, $field] = explode('::', $submittedValues['contact_id_field'], 2);
    $configuration['field'] = $field;
    $configuration['datasource'] = $datasource;
    return $configuration;
  }

  protected function getOperatorOptions(FieldSpecification $fieldSpec): array {
    return array(
      'IN' => E::ts('Does have a relationship'),
      'NOT IN' => E::ts('Does not have a relationship'),
    );
  }

  /**
   * Add the elements to the filter form.
   *
   * @param \CRM_Core_Form $form
   * @param array $defaultFilterValue
   * @param string $size
   *   Possible values: full or compact
   * @return array
   *   Return variables belonging to this filter.
   */
  public function addToFilterForm(CRM_Core_Form $form, $defaultFilterValue, $size='full'): array {
    $fieldSpec = $this->getFieldSpecification();
    $operations = $this->getOperatorOptions($fieldSpec);
    $defaults = array();

    $title = $fieldSpec->title;
    $alias = $fieldSpec->alias;
    if ($this->isRequired()) {
      $title .= ' <span class="crm-marker">*</span>';
    }

    $sizeClass = 'huge';
    $minWidth = 'min-width: 250px;';
    if ($size =='compact') {
      $sizeClass = 'medium';
      $minWidth = '';
    }

    try {
      $form->add('select', "{$alias}_op", E::ts('Operator:'), $operations, TRUE, [
        'style' => $minWidth,
        'class' => 'crm-select2 ' . $sizeClass,
        'multiple' => FALSE,
        'placeholder' => E::ts('- select -'),
      ]);
      $form->add('select', "{$alias}_value", null, $this->getRelationshipTypeOptions(), false, [
        'style' => $minWidth,
        'class' => 'crm-select2 '.$sizeClass,
        'multiple' => TRUE,
        'placeholder' => E::ts('- Any relationship -'),
      ]);
    } catch (CRM_Core_Exception $e) {
    }

    if (isset($defaultFilterValue['op'])) {
      $defaults[$alias . '_op'] = $defaultFilterValue['op'];
    } else {
      $defaults[$alias . '_op'] = 'IN';
    }
    if (isset($defaultFilterValue['value'])) {
      $defaults[$alias.'_value'] = $defaultFilterValue['value'];
    }

    $filter['type'] = $fieldSpec->type;
    $filter['alias'] = $fieldSpec->alias;
    $filter['title'] = $title;
    $filter['size'] = $size;

    if (count($defaults)) {
      $form->setDefaults($defaults);
    }

    return $filter;
  }

  /**
   * @param array $filter
   *   The filter settings
   */
  public function setFilter($filter) {
    $dataFlow = null;
    try {
      $this->resetFilter();
    } catch (Exception $e) {
    }
    try {
      $dataFlow = $this->dataSource->ensureField($this->inputFieldSpecification);
    } catch (Exception $e) {
    }
    if ($dataFlow instanceof SqlDataFlow) {
      $tableAlias = $this->getTableAlias($dataFlow);
      $fieldName = $this->inputFieldSpecification->getName();
      $fieldAlias = $this->inputFieldSpecification->alias;

      if (empty($filter['value'])) {
        $sqlStatement = "`$tableAlias`.`$fieldName` {$filter['op']} (
          SELECT `contact_id_a`
          FROM `civicrm_relationship` `r_c_a_$fieldAlias`
          WHERE `is_active` = '1'
          UNION SELECT `contact_id_b`
          FROM `civicrm_relationship` `r_c_b_$fieldAlias`
          WHERE `is_active` = '1'
          ";
        $this->whereClause = new SqlDataFlow\PureSqlStatementClause($sqlStatement, FALSE);
        $this->filterCollection->addWhere($this->whereClause);
      }
      else {
        $relationshipTypeA = [];
        $relationshipTypeB = [];
        $sqlStatements = [];
        foreach ($filter['value'] as $relationshipType) {
          if (stripos($relationshipType, 'a_b_')===0) {
            $relationshipTypeA[] = $this->getRelationshipTypeId($relationshipType);
          }
          else {
            $relationshipTypeB[] = $this->getRelationshipTypeId($relationshipType);
          }
        }
        if (count($relationshipTypeA)) {
          $sqlStatements[] = "SELECT `contact_id_a` FROM `civicrm_relationship` `r_c_a_$fieldAlias` WHERE `is_active` = '1' AND `relationship_type_id` IN (" . implode(",", $relationshipTypeA) . ")";
        }
        if (count($relationshipTypeB)) {
          $sqlStatements[] = "SELECT `contact_id_b` FROM `civicrm_relationship` `r_c_b_$fieldAlias` WHERE `is_active` = '1' AND `relationship_type_id` IN (" . implode(",", $relationshipTypeB) . ")";
        }
        if (count($sqlStatements)) {
          $sqlStatement = "`$tableAlias`.`$fieldName` {$filter['op']} (" . implode(" UNION ", $sqlStatements) . ")";
          $this->whereClause = new SqlDataFlow\PureSqlStatementClause($sqlStatement, FALSE);
          $this->filterCollection->addWhere($this->whereClause);
        }
      }
    }
  }

}
