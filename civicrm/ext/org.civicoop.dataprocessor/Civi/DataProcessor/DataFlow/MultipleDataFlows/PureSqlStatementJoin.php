<?php

namespace Civi\DataProcessor\DataFlow\MultipleDataFlows;

class PureSqlStatementJoin implements SqlJoinInterface {

  /**
   * @var string
   */
  protected $sql;

  public function __construct($sql) {
    $this->sql = $sql;
  }

  public function getJoinClause(DataFlowDescription $sourceDataFlowDescription): string {
    return $this->sql;
  }

  public function setType($type) {
    // Does not do anything.
  }
}
