<?php
/**
 * Copyright (C) 2023  Jaap Jansma (jaap.jansma@civicoop.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace Civi\DataProcessor\DataFlow\SqlDataFlow;

class AbstractWhereClause {

  /**
   * @var bool
   */
  protected $isJoinClause = FALSE;

  protected $isOr = FALSE;

  /**
   * Returns whether this is an OR clause.
   * Meaning this clause and the previous clause are put together in an OR statement.
   *
   * Return false by default. Which make this is an AND statement;
   *
   * @return bool
   */
  public function isOR(): bool {
    return $this->isOr;
  }

  /**
   * Sets whether this statement is an OR statement
   *
   * @param bool $isOr
   *
   * @return void
   */
  public function setIsOR(bool $isOr) {
    $this->isOr = $isOr;
  }

  /**
   * Returns true when this where clause can be added to the
   * join or whether this clause should be propagated to the where part of the query
   *
   * @return bool
   */
  public function isJoinClause(): bool {
    return $this->isJoinClause;
  }

  /**
   * Sets the join clause property
   *
   * @param bool $isJoinClause
   */
  public function setJoinClause(bool $isJoinClause) {
    $this->isJoinClause = $isJoinClause;
  }

}
