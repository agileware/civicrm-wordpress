<?php
/**
 * @author Jaap Jansma <jaap.jansma@civicoop.org>
 * @license AGPL-3.0
 */

namespace Civi\DataProcessor\DataFlow\SqlDataFlow;

class PureSqlStatementClause extends AbstractWhereClause implements WhereClauseInterface {

  /**
   * @var string
   */
  protected $where;

  public function __construct(string $pureSqlStatement, bool $isJoinClause = FALSE) {
    $this->where = $pureSqlStatement;
    $this->isJoinClause = $isJoinClause;
  }

  /**
   * Returns the where clause
   * E.g. contact_type = 'Individual'
   *
   * @return string
   */
  public function getWhereClause(): string {
    return $this->where;
  }


}
