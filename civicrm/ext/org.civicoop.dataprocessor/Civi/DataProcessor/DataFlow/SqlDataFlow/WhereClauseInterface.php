<?php
/**
 * @author Jaap Jansma <jaap.jansma@civicoop.org>
 * @license AGPL-3.0
 */

namespace Civi\DataProcessor\DataFlow\SqlDataFlow;

interface WhereClauseInterface {

  /**
   * Returns the where clause
   * E.g. contact_type = 'Individual'
   *
   * @return string
   */
  public function getWhereClause();

  /**
   * Returns true when this where clause can be added to the
   * join or whether this clause should be propagated to the where part of the query
   *
   * @return bool
   */
  public function isJoinClause();

  /**
   * Sets the join clause property
   *
   * @param bool $isJoinClause
   */
  public function setJoinClause(bool $isJoinClause);

  /**
   * Returns whether this is an OR clause.
   * Meaning this clause and the previous clause are put together in an OR statement.
   *
   * Return false by default. Which make this is an AND statement;
   *
   * @return bool
   */
  public function isOR(): bool;

  /**
   * Sets whether this statement is an OR statement
   *
   * @param bool $isOr
   *
   * @return void
   */
  public function setIsOR(bool $isOr);

}
