<?php
/**
 * Copyright (C) 2023  Jaap Jansma (jaap.jansma@civicoop.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace Civi\DataProcessor\FileFormat;

use Civi\DataProcessor\DataSpecification\DataSpecification;
use CRM_Core_Form;

interface Fileformat {

  /**
   * @return string
   */
  public function getMimetype(): string;

  /**
   * @return string
   */
  public function getFileExtension(): string;

  /**
   * @return string
   */
  public function getExportFileIcon(): string;

  /**
   * Returns true when this configuration has additional configuration.
   *
   * @return bool
   */
  public function hasConfiguration(): bool;

  /**
   * When this file format has additional configuration you can add
   * the fields on the form with this function.
   *
   * @param \CRM_Core_Form $form
   * @param array $configuration
   */
  public function buildConfigurationForm(CRM_Core_Form $form, array $configuration=array());

  /**
   * When this configuration type has configuration specify the template file name
   * for the configuration form.
   *
   * @return false|string
   */
  public function getConfigurationTemplateFileName():? string;


  /**
   * Process the submitted values into the configuration array.
   *
   * @param $submittedValues
   * @param array $configuration
   */
  public function processConfiguration($submittedValues, array &$configuration);

  /**
   * Create a header
   *
   * @param \Civi\DataProcessor\DataSpecification\DataSpecification $fieldSpecification
   * @param string $filename
   * @param array $configuration
   */
  public function createHeader(DataSpecification $fieldSpecification, string $filename, array $configuration);

  /**
   * Create a footer
   *
   * @param \Civi\DataProcessor\DataSpecification\DataSpecification $fieldSpecification
   * @param string $filename
   * @param array $configuration
   */
  public function createFooter(DataSpecification $fieldSpecification, string $filename, array $configuration);

  /**
   * Add a record to the file
   *
   * @param \Civi\DataProcessor\DataSpecification\DataSpecification $fieldSpecification
   * @param string $filename
   * @param array $record
   * @param array $configuration
   */
  public function addRecord(DataSpecification $fieldSpecification, string $filename, array $record, array $configuration);

  /**
   * Close a file
   *
   * @param \Civi\DataProcessor\DataSpecification\DataSpecification $fieldSpecification
   * @param string $filename
   * @param array $configuration
   */
  public function closeFile(DataSpecification $fieldSpecification, string $filename, array $configuration);

}
