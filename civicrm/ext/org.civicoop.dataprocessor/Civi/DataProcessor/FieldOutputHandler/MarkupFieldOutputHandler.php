<?php
/**
 * @author Jaap Jansma <jaap.jansma@civicoop.org>
 * @license AGPL-3.0
 */

namespace Civi\DataProcessor\FieldOutputHandler;

use Civi\DataProcessor\Exception\DataSourceNotFoundException;
use Civi\DataProcessor\Exception\FieldNotFoundException;
use CRM_Dataprocessor_ExtensionUtil as E;
use Civi\DataProcessor\Source\SourceInterface;
use Civi\DataProcessor\DataSpecification\FieldSpecification;

class MarkupFieldOutputHandler extends AbstractSimpleSortableFieldOutputHandler {

  /**
   * Returns the formatted value
   *
   * @param $rawRecord
   * @param $formattedRecord
   *
   * @return \Civi\DataProcessor\FieldOutputHandler\FieldOutput
   */
  public function formatField($rawRecord, $formattedRecord) {
    // stripAlternatives is needed for an email activity details field where sometimes an email
    // is stored in html and text as well
    $formattedValue = \CRM_Utils_String::stripAlternatives($rawRecord[$this->inputFieldSpec->alias]);
    $output = new HTMLFieldOutput($formattedValue);
    $output->setHtmlOutput($formattedValue);
    return $output;
  }

}
