<?php
/**
 * @author Gerhard Weber <gerhard.weber@civiservice.de>
 * @license AGPL-3.0
 */

namespace Civi\DataProcessor\FieldOutputHandler;

use CRM_Dataprocessor_ExtensionUtil as E;
use Civi\DataProcessor\Source\SourceInterface;
use Civi\DataProcessor\DataSpecification\FieldSpecification;

class DateRangeEndMidnightFieldOutputHandler extends DateFieldOutputHandler {

  /**
   * Field for end date.
   *
   * @var FieldSpecification
   */
  protected $dateStart;

  /**
   * Source for end date.
   *
   * @var SourceInterface
   */
  protected $dataSourceDateStart;

  /**
   * Additional optional format used if time is not set (default time '00:00:00').
   */
  protected $formatTime = NULL;

  /**
   * Initialize the processor
   *
   * @param String $alias
   * @param String $title
   * @param array $configuration
   */
  public function initialize($alias, $title, $configuration) {
    parent::initialize($alias, $title, $configuration);

    [$this->dataSourceDateStart, $this->dateStart] = $this->initializeField($configuration['date_start'], $configuration['date_start_datasource'], $alias . '_date_end');
    $this->formatTime = isset($configuration['format_time']) ? $configuration['format_time'] : false;
  }

  /**
   * Add additional configuration for start date and time format.
   *
   * @param \CRM_Core_Form $form
   * @param array $field
   */
  public function buildConfigurationForm(\CRM_Core_Form $form, $field=array()) {
    parent::buildConfigurationForm($form, $field);

    // @nice-to-have get directly from form's first datasource?
    $fieldSelect = \CRM_Dataprocessor_Utils_DataSourceFields::getAvailableFieldsInDataSources($field['data_processor_id'],
      array($this, 'isFieldValid')
    );
    $form->add('select', 'date_start', E::ts('Start date'), $fieldSelect, true, array(
      'style' => 'min-width:250px',
      'class' => 'crm-select2 huge data-processor-field-for-name',
      'placeholder' => E::ts('- select -'),
    ));

    $form->add('text', 'format_time', E::ts('Time format'), array(
      'style' => 'min-width:250px',
      'class' => 'huge',
    ));

    if (isset($field['configuration'])) {
      $configuration = $field['configuration'];
      $defaults = array();

      if (isset($configuration['date_start']) && isset($configuration['date_start_datasource'])) {
        $defaults['date_start'] = \CRM_Dataprocessor_Utils_DataSourceFields::getSelectedFieldValue($field['data_processor_id'],
          $configuration['date_start_datasource'], $configuration['date_start']);
      }

      if (isset($configuration['format_time'])) {
        $defaults['format_time'] = $configuration['format_time'];
      }

      $form->setDefaults($defaults);
    }

  }

  /**
   * Returns template file name for the configuration form.
   *
   * @return false|string
   */
  public function getConfigurationTemplateFileName() {
    return "CRM/Dataprocessor/Form/Field/Configuration/DateRangeEndMidnightFieldOutputHandler.tpl";
  }

  /**
   * Process the submitted values and create a configuration array
   *
   * @param $submittedValues
   * @return array
   */
  public function processConfiguration($submittedValues) {
    $configuration = parent::processConfiguration($submittedValues);

    [$datasource, $field] = explode('::', $submittedValues['date_start'], 2);
    $configuration['date_start'] = $field;
    $configuration['date_start_datasource'] = $datasource;

    $configuration['format_time'] = isset($submittedValues['format_time']) ? $submittedValues['format_time'] : false;

    return $configuration;
  }

  /**
   * Returns the formatted value.
   *
   * @param $rawRecord
   * @param $formattedRecord
   *
   * @return \Civi\DataProcessor\FieldOutputHandler\FieldOutput
   */
  public function formatField($rawRecord, $formattedRecord) {
    // Get formated end date part (same as parent function but format should not include time formatting!)
    $rawValue = $rawRecord[$this->inputFieldSpec->alias];
    $output = new FieldOutput($rawRecord[$this->inputFieldSpec->alias]);
    if ($this->format && $rawValue) {
      $date = new \DateTime($rawValue);
      $format = $this->format;

      // Add time part.
      if ($this->formatTime) {
        $format .= ' ' . $this->formatTime;

        if (str_ends_with($rawValue, ' 23:59:00')) {
          // End date has time 23:59, so we are checking the start date.
          $rawValue2 = $rawRecord[$this->dateStart->alias];
          if (str_ends_with($rawValue2, ' 00:00:00')) {
            // Start date has time 00:00, so we will use time 00:00 for the formatting.
            $date->setTime(0, 0);
          }
        }
      }

      $output->formattedValue = $date->format($format);
    }

    return $output;
  }

}
