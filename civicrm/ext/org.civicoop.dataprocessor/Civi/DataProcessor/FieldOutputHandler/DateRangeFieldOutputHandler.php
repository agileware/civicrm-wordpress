<?php
/**
 * @author Gerhard Weber <gerhard.weber@civiservice.de>
 * @license AGPL-3.0
 */

namespace Civi\DataProcessor\FieldOutputHandler;

use CRM_Dataprocessor_ExtensionUtil as E;
use Civi\DataProcessor\Source\SourceInterface;
use Civi\DataProcessor\DataSpecification\FieldSpecification;

class DateRangeFieldOutputHandler extends DateFieldOutputHandler {

  /**
   * Field for end date.
   *
   * @var FieldSpecification
   */
  protected $dateEnd;

  /**
   * Source for end date.
   *
   * @var SourceInterface
   */
  protected $dataSourceDateEnd;

  /**
   * Additional optional format used if time is not set (default time '00:00:00').
   */
  protected $formatDateOnly = NULL;

  /**
   * Separates start and end date.
   */
  protected $separator;

  /**
   * Initialize the processor
   *
   * @param String $alias
   * @param String $title
   * @param array $configuration
   */
  public function initialize($alias, $title, $configuration) {
    parent::initialize($alias, $title, $configuration);

    [$this->dataSourceDateEnd, $this->dateEnd] = $this->initializeField($configuration['date_end'], $configuration['date_end_datasource'], $alias . '_date_end');
    $this->formatDateOnly = isset($configuration['format_date_only']) ? $configuration['format_date_only'] : false;
    $this->separator = isset($configuration['separator']) ? $configuration['separator'] : false;
  }

  /**
   * Add additional configuration for end date, date format and separator.
   *
   * @param \CRM_Core_Form $form
   * @param array $field
   */
  public function buildConfigurationForm(\CRM_Core_Form $form, $field=array()) {
    parent::buildConfigurationForm($form, $field);

    // @nice-to-have get directly from form's first datasource?
    $fieldSelect = \CRM_Dataprocessor_Utils_DataSourceFields::getAvailableFieldsInDataSources($field['data_processor_id'],
      array($this, 'isFieldValid')
    );
    $form->add('select', 'date_end', E::ts('End date'), $fieldSelect, true, array(
      'style' => 'min-width:250px',
      'class' => 'crm-select2 huge data-processor-field-for-name',
      'placeholder' => E::ts('- select -'),
    ));

    $form->add('text', 'format_date_only', E::ts('Format date only'), array(
      'style' => 'min-width:250px',
      'class' => 'huge',
    ));

    $form->add('text', 'separator', E::ts('Separator'),
      array('class' => 'medium'),
      true
      // @nice-to-have add description here (only possible in .tpl file?)
    );

    if (isset($field['configuration'])) {
      $configuration = $field['configuration'];
      $defaults = array();

      if (isset($configuration['date_end']) && isset($configuration['date_end_datasource'])) {
        $defaults['date_end'] = \CRM_Dataprocessor_Utils_DataSourceFields::getSelectedFieldValue($field['data_processor_id'],
          $configuration['date_end_datasource'], $configuration['date_end']);
      }

      foreach (array('separator', 'format_date_only') as $key) {
        if (isset($configuration[$key])) {
          $defaults[$key] = $configuration[$key];
        }
      }

      $form->setDefaults($defaults);
    }

  }

  /**
   * Returns template file name for the configuration form.
   *
   * @return false|string
   */
  public function getConfigurationTemplateFileName() {
    return "CRM/Dataprocessor/Form/Field/Configuration/DateRangeFieldOutputHandler.tpl";
  }

  /**
   * Process the submitted values and create a configuration array
   *
   * @param $submittedValues
   * @return array
   */
  public function processConfiguration($submittedValues) {
    $configuration = parent::processConfiguration($submittedValues);

    [$datasource, $field] = explode('::', $submittedValues['date_end'], 2);
    $configuration['date_end'] = $field;
    $configuration['date_end_datasource'] = $datasource;

    foreach (array('separator', 'format_date_only') as $key) {
      $configuration[$key] = isset($submittedValues[$key]) ? $submittedValues[$key] : false;
    }

    return $configuration;
  }

  /**
   * Returns the formatted value.
   *
   * @param $rawRecord
   * @param $formattedRecord
   *
   * @return \Civi\DataProcessor\FieldOutputHandler\FieldOutput
   */
  public function formatField($rawRecord, $formattedRecord) {
    $output = new FieldOutput($rawRecord[$this->inputFieldSpec->alias]);
    $rawValue = $rawRecord[$this->inputFieldSpec->alias];
    if (($this->format || $this->formatDateOnly) && $rawValue) {
      $output->formattedValue = $this->formatHelper($rawValue);
    }

    $output2 = new FieldOutput($rawRecord[$this->dateEnd->alias]);
    $rawValue2 = $rawRecord[$this->dateEnd->alias];
    if ($rawValue2) {
      if ($this->format || $this->formatDateOnly) {
        $output2->formattedValue = $this->formatHelper($rawValue2);
      }
      if ($this->separator) {
        $output->formattedValue .= $this->separator;
      }
      $output->formattedValue .= $output2->formattedValue;
    }

    return $output;
  }

  /**
   * Format raw date value.
   *
   * @param $raw_value
   *   Raw date value
   *
   * @return string
   *   Date string formatted according to configured format.
   *   If formatDateOnly is set it is used for default time (00:00:00).
   *
   * @throws \Exception
   */
  protected function formatHelper($raw_value) {
    $date = new \DateTime($raw_value);
    if ($this->formatDateOnly && str_ends_with($raw_value, ' 00:00:00')) {
      $format = $this->formatDateOnly;
    }
    else {
      $format = $this->format;
    }

    if (is_null($format)) {
      return NULL;
    }

    return $date->format($format);
  }

}
