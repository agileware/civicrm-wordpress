<?php
/**
 * Copyright (C) 2024  Jaap Jansma (jaap.jansma@civicoop.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace Civi\DataProcessor\FieldOutputHandler;

use CRM_Dataprocessor_ExtensionUtil as E;

class BoundedAggregateFunctionFieldOutputHandler extends AggregateFunctionFieldOutputHandler {

  /**
   * @var float
   */
  protected $minBound = null;

  /**
   * @var float
   */
  protected $minValue = 0.00;

  /**
   * @var float
   */
  protected $maxBound = null;

  /**
   * @var float
   */
  protected $maxValue = 0.00;

  /**
   * @var float
   */
  protected $multiplier = 1;

  /**
   * Initialize the processor
   *
   * @param String $alias
   * @param String $title
   * @param array $configuration
   * @param \Civi\DataProcessor\ProcessorType\AbstractProcessorType $processorType
   */
  public function initialize($alias, $title, $configuration) {
    parent::initialize($alias, $title, $configuration);
    if (isset($configuration['min_bound']) && strlen($configuration['min_bound'])) {
      $this->minBound = (float) $configuration['min_bound'];
      $this->minValue = (float) $configuration['min_value'];
    }
    if (isset($configuration['max_bound']) && strlen($configuration['max_bound'])) {
      $this->maxBound = (float) $configuration['max_bound'];
      $this->maxValue = (float) $configuration['max_value'];
    }
    if (!empty($configuration['multiplier']) && $configuration['multiplier'] != '1') {
      $this->multiplier = (float) $configuration['multiplier'];
    }
  }

  /**
   * Returns the formatted value
   *
   * @param $rawRecord
   * @param $formattedRecord
   *
   * @return \Civi\DataProcessor\FieldOutputHandler\FieldOutput
   */
  public function formatField($rawRecord, $formattedRecord) {
    $value = (float) $rawRecord[$this->aggregateField->alias];
    $pureValue = $value;
    if ($this->multiplier > 0) {
      $pureValue = $pureValue / $this->multiplier;
    }
    if ($this->minBound !== null && $pureValue < $this->minBound) {
      $value = $this->minValue;
    } elseif ($this->maxBound !== null && $pureValue > $this->maxBound) {
      $value = $this->maxValue;
    } elseif ($this->roundToNearestWholeNumber) {
      $value = $this->roundUpToNearest($value, $this->roundToNearestWholeNumber);
    }
    return $this->formatOutput($value);
  }

  /**
   * When this handler has additional configuration you can add
   * the fields on the form with this function.
   *
   * @param \CRM_Core_Form $form
   * @param array $field
   */
  public function buildConfigurationForm(\CRM_Core_Form $form, $field=array()) {
    parent::buildConfigurationForm($form, $field);
    $form->add('number', 'min_bound', E::ts('Bound Value (if value is less then)'), ['class' => 'six', 'min' => 1], true);
    $form->add('number', 'min_value', E::ts('Min Value'), ['class' => 'six', 'min' => 1], true);
    $form->add('number', 'max_bound', E::ts('Bound Value (if value is more than)'), ['class' => 'six', 'min' => 1], true);
    $form->add('number', 'max_value', E::ts('Max Value'), ['class' => 'six', 'min' => 1], true);
    if (isset($field['configuration'])) {
      $configuration = $field['configuration'];
      if (isset($configuration['min_bound'])) {
        $this->defaults['min_bound'] = $configuration['min_bound'];
      }
      if (isset($configuration['min_value'])) {
        $this->defaults['min_value'] = $configuration['min_value'];
      }
      if (isset($configuration['max_bound'])) {
        $this->defaults['max_bound'] = $configuration['max_bound'];
      }
      if (isset($configuration['max_value'])) {
        $this->defaults['max_value'] = $configuration['max_value'];
      }
      $form->setDefaults($this->defaults);
    }
  }

  /**
   * When this handler has configuration specify the template file name
   * for the configuration form.
   *
   * @return false|string
   */
  public function getConfigurationTemplateFileName() {
    return "CRM/Dataprocessor/Form/Field/Configuration/BoundedAggregateFunctionFieldOutputHandler.tpl";
  }

  /**
   * Process the submitted values and create a configuration array
   *
   * @param $submittedValues
   * @return array
   */
  public function processConfiguration($submittedValues) {
    $configuration = parent::processConfiguration($submittedValues);
    $configuration['min_bound'] = $submittedValues['min_bound'];
    $configuration['min_value'] = $submittedValues['min_value'];
    $configuration['max_bound'] = $submittedValues['max_bound'];
    $configuration['max_value'] = $submittedValues['max_value'];
    return $configuration;
  }

}
