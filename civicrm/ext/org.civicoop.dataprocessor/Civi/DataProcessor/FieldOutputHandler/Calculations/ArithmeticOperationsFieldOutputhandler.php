<?php
/**
 * @author Jaap Jansma <jaap.jansma@civicoop.org>
 * @license AGPL-3.0
 */

namespace Civi\DataProcessor\FieldOutputHandler\Calculations;

use Civi\DataProcessor\FieldOutputHandler\AbstractFormattedNumberOutputHandler;

use CRM_Dataprocessor_ExtensionUtil as E;
use CRM_Dataprocessor_Utils_DataSourceFields;
use CRM_Utils_Type;

class ArithmeticOperationsFieldOutputhandler extends AbstractFormattedNumberOutputHandler {

  /**
   * @var \Civi\DataProcessor\DataSpecification\FieldSpecification
   */
  protected $outputFieldSpec;

  protected $value;

  protected $operation;

  /**
   * Initialize the processor
   *
   * @param String $alias
   * @param String $title
   * @param array $configuration
   * @param \Civi\DataProcessor\ProcessorType\AbstractProcessorType $processorType
   */
  public function initialize($alias, $title, $configuration) {
    parent::initialize($alias, $title, $configuration);
    $this->value = $configuration['value'];
    $this->operation = $configuration['operation'];
    if ($this->value) {
      $this->inputFieldSpec->setMySqlFunction('`%1`.`%2` ' . $this->operation . ' ' . CRM_Utils_Type::escape($this->value, 'Integer'));
    }
  }

  /**
   * When this handler has additional configuration you can add
   * the fields on the form with this function.
   *
   * @param \CRM_Core_Form $form
   * @param array $field
   */
  public function buildConfigurationForm(\CRM_Core_Form $form, $field=array()) {
    parent::buildConfigurationForm($form, $field);
    $form->add('text', 'value', E::ts('Value'), ['class' => 'huge'], true);
    $form->addRule('value', E::ts('Enter a valid number'), 'numeric');
    $operators = [
      '+' => '+ (Add)',
      '-' => '- (Substract)',
      '*' => '* (Multiply)',
      '/' => '/ (Divide)'
    ];
    $form->add('select', 'operation', E::ts('Arithmetic Operation'), $operators, true, array(
      'style' => 'min-width:250px',
      'class' => 'crm-select2 huge',
      'placeholder' => E::ts('- select -'),
      'multiple' => false,
    ));

    if (isset($field['configuration'])) {
      $configuration = $field['configuration'];
      if (isset($configuration['value'])) {
        $this->defaults['value'] = $configuration['value'];
      }
      if (isset($configuration['operation'])) {
        $this->defaults['operation'] = $configuration['operation'];
      }
      $form->setDefaults($this->defaults);
    }
  }

  /**
   * When this handler has configuration specify the template file name
   * for the configuration form.
   *
   * @return false|string
   */
  public function getConfigurationTemplateFileName() {
    return "CRM/Dataprocessor/Form/Field/Configuration/ArithmeticOperationsFieldOutputHandler.tpl";
  }


  /**
   * Process the submitted values and create a configuration array
   *
   * @param $submittedValues
   * @return array
   */
  public function processConfiguration($submittedValues) {
    $configuration = parent::processConfiguration($submittedValues);
    $configuration['value'] = $submittedValues['value'];
    $configuration['operation'] = $submittedValues['operation'];
    return $configuration;
  }


}
