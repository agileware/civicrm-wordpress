<?php
/**
 * Copyright (C) 2023  Jaap Jansma (jaap.jansma@civicoop.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

class CRM_Dataprocessor_Utils_Smartgroup {

  public static function checkOrRefresh(array $groupIds, $timeOut=120) {
    $key = 'dataprocessor_smartgroup_check_'.md5(json_encode($groupIds));
    $cache = CRM_Dataprocessor_Utils_Cache::singleton();
    $lastCheckTime = $cache->get($key);
    $timeOut = time() - $timeOut;
    if (empty($toCheck) || $lastCheckTime < $timeOut) {
      CRM_Contact_BAO_GroupContactCache::check($groupIds);
      $cache->set($key, time());
    }
  }

}
