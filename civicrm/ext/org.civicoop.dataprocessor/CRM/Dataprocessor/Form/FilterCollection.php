<?php

use CRM_Dataprocessor_ExtensionUtil as E;

/**
 * Form controller class
 *
 * @see https://wiki.civicrm.org/confluence/display/CRMDOC/QuickForm+Reference
 */
class CRM_Dataprocessor_Form_FilterCollection extends CRM_Core_Form {

  private $dataProcessorId;

  private $id;

  private $filterCollection;

  /**
   * Function to perform processing before displaying form (overrides parent function)
   *
   * @access public
   */
  function preProcess() {
    $this->dataProcessorId = CRM_Utils_Request::retrieve('data_processor_id', 'Integer');
    $this->assign('data_processor_id', $this->dataProcessorId);

    $this->id = CRM_Utils_Request::retrieve('id', 'Integer');
    $this->assign('id', $this->id);

    if ($this->id) {
      $this->filterCollection = civicrm_api3('DataProcessorFilterCollection', 'getsingle', array('id' => $this->id));
    }
    if (!$this->filterCollection) {
      $this->filterCollection['data_processor_id'] = $this->dataProcessorId;
      $this->filterCollection['condition'] = 'AND';
      $this->filterCollection['link_condition'] = 'AND';
    }
    $this->assign('filterCollection', $this->filterCollection);

    $title = E::ts('Data Processor Filter Collection');
    CRM_Utils_System::setTitle($title);
  }

  public function buildQuickForm() {
    $this->add('hidden', 'data_processor_id');
    $this->add('hidden', 'id');
    if ($this->_action == CRM_Core_Action::DELETE) {
      $this->addButtons(array(
        array('type' => 'next', 'name' => E::ts('Delete'), 'isDefault' => TRUE,),
        array('type' => 'cancel', 'name' => E::ts('Cancel'))));
    } else {
      $this->add('text', 'name', E::ts('Name'), array('size' => CRM_Utils_Type::HUGE), FALSE);
      $this->add('text', 'title', E::ts('Title'), array('size' => CRM_Utils_Type::HUGE), TRUE);
      $conditions = [
        'OR' => E::ts('Or: one of the condition criteria in this collection should be met'),
        'AND' => E::ts('And: all of the condition criteria in this collection should be met'),
      ];
      $linkConditions = [
        'OR' => E::ts('Or: either this filter collection or the previous filter collection should be met'),
        'AND' => E::ts('And: this filter collection and the previous filter collection should be met'),
      ];
      $this->add('select', 'condition', E::ts('Condition'), $conditions, true, array('style' => 'min-width:250px',
        'class' => 'crm-select2 huge40',
        'placeholder' => E::ts('- select -'),));
      $this->add('select', 'link_condition', E::ts('Link condition'), $linkConditions, true, array('style' => 'min-width:250px',
        'class' => 'crm-select2 huge40',
        'placeholder' => E::ts('- select -'),));
      $this->add('wysiwyg', 'description', E::ts('Description'));
      $this->add('checkbox', 'show_collapsed', E::ts('Collapse in search form'));

      $this->addButtons(array(
        array('type' => 'next', 'name' => E::ts('Save'), 'isDefault' => TRUE,),
        array('type' => 'cancel', 'name' => E::ts('Cancel'))));
    }
    parent::buildQuickForm();
  }

  function setDefaultValues() {
    return  $this->filterCollection;
  }

  /**
   * Function that can be defined in Form to override or.
   * perform specific action on cancel action
   */
  public function cancelAction() {
    $this->dataProcessorId = CRM_Utils_Request::retrieve('data_processor_id', 'Integer');
    $redirectUrl = CRM_Utils_System::url('civicrm/dataprocessor/form/edit', array('reset' => 1, 'action' => 'update', 'id' => $this->dataProcessorId));
    CRM_Utils_System::redirect($redirectUrl);
  }

  public function postProcess() {
    $session = CRM_Core_Session::singleton();
    $redirectUrl = CRM_Utils_System::url('civicrm/dataprocessor/form/edit', array('reset' => 1, 'action' => 'update', 'id' => $this->dataProcessorId));
    if ($this->_action == CRM_Core_Action::DELETE) {
      civicrm_api3('DataProcessorFilterCollection', 'delete', array('id' => $this->id));
      $session->setStatus(E::ts('Filter Collection removed'), E::ts('Removed'), 'success');
      CRM_Utils_System::redirect($redirectUrl);
    }

    $values = $this->exportValues();
    if (!empty($values['name'])) {
      $params['name'] = $values['name'];
    }
    $params['title'] = $values['title'];
    $params['description'] = $values['description'];
    $params['condition'] = $values['condition'];
    $params['link_condition'] = $values['link_condition'];
    $params['show_collapsed'] = $values['show_collapsed'] ? '1' : '0';
    if ($this->dataProcessorId) {
      $params['data_processor_id'] = $this->dataProcessorId;
    }
    if ($this->id) {
      $params['id'] = $this->id;
    }

    civicrm_api3('DataProcessorFilterCollection', 'create', $params);
    CRM_Utils_System::redirect($redirectUrl);
    parent::postProcess();
  }

}
