<?php

use Civi\DataProcessor\FilterCollection\FilterCollection;
use CRM_Dataprocessor_ExtensionUtil as E;

class CRM_Dataprocessor_BAO_DataProcessorFilterCollection extends CRM_Dataprocessor_DAO_DataProcessorFilterCollection {

  /**
   * Returns the filter colelction number from its ID.
   *
   * @param $data_processor_id
   * @param $id
   *
   * @return int
   */
  public static function getFilterFilloctionNumberById($data_processor_id, $id): int {
    static $filterCollectionIdToNr = [];
    if (empty($filterCollectionIdToNr)) {
      try {
        $filterCollections = civicrm_api3('DataProcessorFilterCollection', 'get', [
          'data_processor_id' => $data_processor_id,
          'options' => ['limit' => 0],
        ]);
        $i = 1;
        foreach ($filterCollections['values'] as $filterCollection) {
          $filterCollectionIdToNr[$filterCollection['id']] = $i;
          $i++;
        }
      } catch (CRM_Core_Exception $e) {
      }
    }
    if ($id && isset($filterCollectionIdToNr[$id])) {
      $filterCollectionIdToNr[$id];
    }
    return 0;
  }

  public static function checkName($title, $data_processor_id, $id=null,$name=null) {
    if (!$name) {
      $name = preg_replace('@[^a-z0-9_]+@','_',strtolower($title));
    }

    $name = preg_replace('@[^a-z0-9_]+@','_',strtolower($name));
    $name_part = $name;

    $sql = "SELECT COUNT(*) FROM `civicrm_data_processor_filter_collection` WHERE `name` = %1 AND `data_processor_id` = %2";
    $sqlParams[1] = array($name, 'String');
    $sqlParams[2] = array($data_processor_id, 'String');
    if ($id) {
      $sql .= " AND `id` != %3";
      $sqlParams[3] = array($id, 'Integer');
    }

    $i = 1;
    while(CRM_Core_DAO::singleValueQuery($sql, $sqlParams) > 0) {
      $i++;
      $name = $name_part .'_'.$i;
      $sqlParams[1] = array($name, 'String');
    }
    return $name;
  }

  /**
   * Delete function so that the hook for deleting an output gets invoked.
   *
   * @param $id
   */
  public static function del($id) {
    CRM_Utils_Hook::pre('delete', 'DataProcessorFilterCollection', $id, CRM_Core_DAO::$_nullArray);

    $dao = new CRM_Dataprocessor_BAO_DataProcessorFilterCollection();
    $dao->id = $id;
    if ($dao->find(true)) {
      $dao->delete();
    }

    CRM_Utils_Hook::post('delete', 'DataProcessorFilterCollection', $id, CRM_Core_DAO::$_nullArray);
  }

  /**
   * Function to delete a Data Processor Filter with id
   *
   * @param int $id
   * @throws Exception when $id is empty
   * @access public
   * @static
   */
  public static function deleteWithDataProcessorId($id) {
    if (empty($id)) {
      throw new Exception('id can not be empty when attempting to delete a data processor filter');
    }

    $field = new CRM_Dataprocessor_DAO_DataProcessorFilterCollection();
    $field->data_processor_id = $id;
    $field->find(FALSE);
    while ($field->fetch()) {
      civicrm_api3('DataProcessorFilterCollection', 'delete', array('id' => $field->id));
    }
  }

}
