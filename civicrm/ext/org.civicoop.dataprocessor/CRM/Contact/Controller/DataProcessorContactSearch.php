<?php
/**
 * @author Jaap Jansma <jaap.jansma@civicoop.org>
 * @license AGPL-3.0
 */

/**
 * This class is used by the Search functionality.
 *
 *  - the search controller is used for building/processing multiform
 *    searches.
 *
 * Typically the first form will display the search criteria and it's results
 *
 * The second form is used to process search results with the associated actions.
 */
class CRM_Contact_Controller_DataProcessorContactSearch extends CRM_DataprocessorSearch_Controller_Search {

  /**
   * @param string|null $title
   * @param bool $modal
   * @param int|mixed $action
   */
  public function __construct($title = NULL, $modal = TRUE, $action = CRM_Core_Action::NONE) {
    parent::__construct($title, $modal, $action);
    $this->set('entity', 'Contact');
  }

  /**
   * @return mixed
   */
  public function selectorName() {
    return 'CRM_Contact_Selector_DataProcessorContactSearch';
  }

  public function getUrlForSearchRedirectReplacement():? string {
    return '/civicrm/contact/search';
  }

  public function getSearchName(): string {
    return 'Basic';
  }

  public function getSearchClass(): string {
    return 'CRM_Contact_Form_DataProcessorContactSearch';
  }

  /**
   * @param string $selectedTask
   *
   * @return string|array|null
   */
  public function getTaskClass(string $selectedTask) {
    [$task] = CRM_Contact_Task::getTask($selectedTask);
    if ($task) {
      return $task;
    }
    return null;
  }

}
