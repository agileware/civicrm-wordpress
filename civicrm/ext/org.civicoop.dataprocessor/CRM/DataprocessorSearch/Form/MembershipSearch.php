<?php
/**
 * @author Jaap Jansma <jaap.jansma@civicoop.org>
 * @license AGPL-3.0
 */

use CRM_Dataprocessor_ExtensionUtil as E;

class CRM_DataprocessorSearch_Form_MembershipSearch extends CRM_DataprocessorSearch_Form_AbstractSearch {

  /**
   * Returns the name of the default Entity
   *
   * @return string
   */
  public function getDefaultEntity(): string {
    return 'Membership';
  }

  /**
   * Returns the url for view of the record action
   *
   * @param $row
   *
   * @return false|string
   */
  protected function link($row):? string {
    $contact_id = false;
    try {
      $contact_id = civicrm_api3('Membership', 'getvalue', [
        'return' => 'contact_id',
        'id' => $row['id']
      ]);
    } catch (CiviCRM_API3_Exception $ex) {
      // Do nothing.
    }
    return CRM_Utils_System::url('civicrm/contact/view/membership', 'reset=1&id='.$row['id'].'&cid='.$contact_id.'&action=view');
  }

  /**
   * Returns the link text for view of the record action
   *
   * @param $row
   *
   * @return false|string
   */
  protected function linkText($row):? string {
    return E::ts('View membership');
  }

  /**
   * Checks whether the output has a valid configuration
   *
   * @return bool
   */
  protected function isConfigurationValid(): bool {
    if (!isset($this->dataProcessorOutput['configuration']['membership_id_field'])) {
      return false;
    }
    return true;
  }

  /**
   * Return the data processor ID
   *
   * @return String
   */
  protected function getDataProcessorName(): string {
    return str_replace('civicrm/dataprocessor_membership_search/', '', CRM_Utils_System::currentPath());
  }

  /**
   * Returns the name of the output for this search
   *
   * @return string
   */
  protected function getOutputName(): string {
    return 'membership_search';
  }

  /**
   * Returns the name of the ID field in the dataset.
   *
   * @return string
   */
  public function getIdFieldName(): string {
    return $this->dataProcessorOutput['configuration']['membership_id_field'];
  }

  /**
   * @return string
   */
  protected function getEntityTable(): string {
    return 'civicrm_membership';
  }

  /**
   * Returns whether we want to use the prevnext cache.
   * @return bool
   */
  protected function usePrevNextCache(): bool {
    return false;
  }

  /**
   * Builds the list of tasks or actions that a searcher can perform on a result set.
   *
   * @return array
   */
  public function buildTaskList(): array {
    if (!$this->_taskList) {
      $taskParams = [];
      $this->_taskList = CRM_Member_Task::permissionedTaskTitles(CRM_Core_Permission::getPermission(), $taskParams);
      $this->filterTaskList();
    }
    return $this->_taskList;
  }

  /**
   * Return altered rows
   *
   * Save the ids into the queryParams value. So that when an action is done on the selected record
   * or on all records, the queryParams will hold all the activity ids so that in the next step only the selected record,
   * or all records are populated.
   */
  protected function retrieveEntityIds() {
    $this->_queryParams[0] = array(
      'membership_id',
      '=',
      array(
        'IN' => $this->getSelectedIds(),
      ),
      0,
      0
    );
    $this->controller->set('queryParams', $this->_queryParams);
  }

}
