<?php
/**
 * Copyright (C) 2024  Jaap Jansma (jaap.jansma@civicoop.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

use CRM_Dataprocessor_ExtensionUtil as E;

class CRM_DataprocessorSearch_Utils_Tasks {

  /**
   * Build configuration form for adding retricted tasks
   *
   * @param $allTasks
   * @param \CRM_Core_Form $form
   * @param $output
   *
   * @return void
   */
  public static function buildConfigurationForm($allTasks, \CRM_Core_Form $form, $output=array()) {
    $form->add('checkbox', 'restrict_tasks', E::ts('Restrict or reorder tasks?'));
    $tasks = [];
    if (isset($output['configuration']['tasks']) && is_array($output['configuration']['tasks'])) {
      foreach($output['configuration']['tasks'] as $task) {
        $label = $allTasks[substr($task, 5)];
        $tasks[$label] = $task;;
      }
    }
    foreach($allTasks as $task => $label) {
      if (!in_array('task_'.$task, $tasks)) {
        $tasks[$label] = 'task_' . $task;
      }
    }
    $form->addCheckBox('tasks_checkboxes',  E::ts('Restrict to tasks'), $tasks);
    $form->assign('tasks', $tasks);
    if ($form->isSubmitted()) {
      $form->add('hidden', 'sorted_tasks', NULL, ['id' => 'sorted_tasks']);
    }

    $defaults = array();
    if ($output && isset($output['configuration']) && is_array($output['configuration'])) {
      if (isset($output['configuration']['restrict_tasks'])) {
        $defaults['restrict_tasks'] = $output['configuration']['restrict_tasks'];
      }
      if (isset($output['configuration']['tasks'])) {
        $defaults['tasks_checkboxes'] = [];
        foreach($output['configuration']['tasks'] as $task) {
          $defaults['tasks_checkboxes'][$task] = 1;
        }
      }
    }
    $form->setDefaults($defaults);
  }

  /**
   * Process the submitted values and create a configuration array
   *
   * @param $submittedValues
   * @return array
   */
  public static function processConfiguration($submittedValues, $configuration) {
    $configuration['restrict_tasks'] = isset($submittedValues['restrict_tasks']) ? $submittedValues['restrict_tasks'] : false;
    $configuration['tasks'] = [];
    if (!empty($submittedValues['sorted_tasks'])) {
      $sortedTasks = explode(',', $submittedValues['sorted_tasks']);
      foreach ($sortedTasks as $val) {
        if ($val && isset($submittedValues['tasks_checkboxes']['task_' . $val])) {
          $configuration['tasks'][] = 'task_' . $val;
        }
      }
    } elseif (isset($submittedValues['tasks_checkboxes'])) {
      $configuration['tasks'] = array_keys($submittedValues['tasks_checkboxes']);
    }
    return $configuration;
  }

}
