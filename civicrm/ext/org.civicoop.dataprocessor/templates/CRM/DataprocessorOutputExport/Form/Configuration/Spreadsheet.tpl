{crmScope extensionKey='dataprocessor'}
  <div class="crm-section">
      <div class="label">{$form.altfilename.label}</div>
      <div class="content">{$form.altfilename.html}</div>
      <div class="clear"></div>
  </div>
  {include file=$spreadsheet_file_format_configuration}
  <div class="crm-section">
    <div class="label">{$form.anonymous.label}</div>
    <div class="content">{$form.anonymous.html}
      <p class="description">
        {ts}Tick this box when you want to make the file available for non-logged in users. <br>
        This could be necessary when another system is importing this file on a regular basis. E.g. a website with
        a public agenda of the upcoming events.
        <br><strong>Caution:</strong> when you check this box the data becomes available without logging so this might lead to a data breach.{/ts}</p>
    </div>
    <div class="clear"></div>
  </div>
{/crmScope}
