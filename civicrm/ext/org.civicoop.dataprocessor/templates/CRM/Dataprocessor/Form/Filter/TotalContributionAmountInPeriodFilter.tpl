{crmScope extensionKey='dataprocessor'}
{assign var=fieldOp     value=$filter.alias|cat:"_op"}
{assign var=filterStatusIds   value=$filter.alias|cat:"_status_ids"}
{assign var=filterFinancialTypeIds   value=$filter.alias|cat:"_financial_type_ids"}
{assign var=filterCampaignIds   value=$filter.alias|cat:"_campaign_ids"}
{assign var=filterVal   value=$filter.alias|cat:"_amount"}
{assign var=filterMin   value=$filter.alias|cat:"_amount_min"}
{assign var=filterMax   value=$filter.alias|cat:"_amount_max"}

<tr>
    <td class="label">{$filter.title}</td>
    <td>
      {if $form.$fieldOp.html}
      <span class="filter-processor-element filter-{$filter.alias}">{$form.$fieldOp.html}</span>
      <span class="filter-processor-show-close filter-{$filter.alias}">&nbsp;</span>
      {/if}
    </td>
    <td>
      <span id="{$filterVal}_cell">{$form.$filterVal.label}&nbsp;{$form.$filterVal.html}</span>
      <span id="{$filterMin}_max_cell">{$form.$filterMin.label}&nbsp;{$form.$filterMin.html}&nbsp;&nbsp;{$form.$filterMax.label}&nbsp;{$form.$filterMax.html}</span>
      <p>
          {ts}In period{/ts} {include file="CRM/Dataprocessor/Form/Filter/DateRange.tpl" fieldName=$filter.alias from='_low' to='_high'}
      </p>
      <p>
      {ts}With status: {/ts}<br /><span id="{$filterStatusIds}_cell">{$form.$filterStatusIds.html}</span> <br />
      {ts}With financial type: {/ts}<br /><span id="{$filterFinancialTypeIds}_cell">{$form.$filterFinancialTypeIds.html}</span> <br />
      {ts}With campaign: {/ts}<br /><span id="{$filterCampaignIds}_cell">{$form.$filterCampaignIds.html}</span>
      </p>
      {include file="CRM/Dataprocessor/Form/Filter/InExcludeEntityRef.js.tpl"  inExcludeFieldId=$filterCampaignIds}

      {literal}
      <script type="text/javascript">
        CRM.$(function($) {
          cj("#{/literal}{$fieldOp}{literal}").change(function() {
            var val = $(this).val();
            if (val == 'bw' || val == 'nbw') {
              cj("#{/literal}{$filterMin}_max_cell{literal}").removeClass('hiddenElement');
              cj("#{/literal}{$filterVal}_cell{literal}").addClass('hiddenElement');
            } else {
              cj("#{/literal}{$filterMin}_max_cell{literal}").addClass('hiddenElement');
              cj("#{/literal}{$filterVal}_cell{literal}").removeClass('hiddenElement');
            }
          }).change();
        });
      </script>
        {/literal}

    </td>
</tr>
{/crmScope}
