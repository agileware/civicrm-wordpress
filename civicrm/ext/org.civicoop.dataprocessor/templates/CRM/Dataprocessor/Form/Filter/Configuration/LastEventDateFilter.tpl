{crmScope extensionKey='dataprocessor'}
  <div class="crm-section">
    <div class="label">{$form.contact_id_field.label}</div>
    <div class="content">{$form.contact_id_field.html}</div>
    <div class="clear"></div>
  </div>
  <div class="crm-section">
    <div class="label">{$form.event_types.label}</div>
    <div class="content">{$form.event_types.html}</div>
    <div class="clear"></div>
  </div>
{/crmScope}
