{crmScope extensionKey='dataprocessor'}
  {include file="CRM/Dataprocessor/Form/Field/Configuration/DateFieldOutputHandler.tpl"}

  <hr >
  <div class="crm-section">
    <div class="label">{$form.date_start.label}</div>
    <div class="content">
      {$form.date_start.html}
      <p class="description">
        {ts}If start has default time <em>00:00:00</em> and end has time <em>23:59:00</em> output will be formatted with above date format and time format below, e.g. <em>1973-11-29 00:00</em>.{/ts}
        <br>
        {ts}Otherwise output will be formatted with above date format and time format below.{/ts}
        <br>
        {ts}Do not include time formating in above date format.{/ts}
      </p>
    </div>
    <div class="clear"></div>
  </div>
  <div class="crm-section">
    <div class="label">{$form.format_time.label}</div>
    <div class="content">
      {$form.format_time.html}
      <p class="description">
        {ts}Time format (e.g. <em>H:i</em>). Do not include date formating.{/ts}<br />
        {ts 1='https://www.php.net/manual/en/datetime.format.php#refsect1-datetime.format-parameters'}See <a href="%1" target="_blank">DateTime format on php.net</a> for help.{/ts}
      </p>
    </div>
    <div class="clear"></div>
  </div>
{/crmScope}
