{crmScope extensionKey='dataprocessor'}
{include file="CRM/Dataprocessor/Form/Field/Configuration/SimpleFieldOutputHandler.tpl"}
    <p class="help">{ts}Count the number of cases, where the given CiviCRM has the client role and the cases fulfill the following criteria:{/ts}</p>
<div class="crm-section">
  <div class="label">{$form.case_type.label}</div>
  <div class="content">{$form.case_type.html}</div>
  <div class="clear"></div>
  </div>
<div class="crm-section">
  <div class="label">{$form.case_status.label}</div>
  <div class="content">{$form.case_status.html}</div>
  <div class="clear"></div>
  </div>
<div class="crm-section">
  <div class="label">{$form.case_subject.label}</div>
  <div class="content">{$form.case_subject.html}</div>
  <div class="clear"></div>
  </div>
<div class="crm-section">
  <div class="label">{$form.include_deleted.label}</div>
  <div class="content">{$form.include_deleted.html}</div>
  <div class="clear"></div>
  </div>
<div class="crm-section">
  <div class="label">{$form.ignore_permission.label}</div>
  <div class="content">{$form.ignore_permission.html}</div>
  <div class="clear"></div>
  </div>
  <p class="help">{ts}Next you can define up to three additional criteria, where 'field' corresponds to a field of an APIv4 call to CaseContact and 'value to match' to the value it should correspond to.{/ts}</p>
<div class="crm-section">
  <div class="label">{$form.generic_input_1.label}</div>
  <div class="content">{$form.generic_input_1.html}</div>
  <div class="label">{$form.generic_match_1.label}</div>
  <div class="content">{$form.generic_match_1.html}</div>
  <div class="clear"></div>
  </div>
<div class="crm-section">
  <div class="label">{$form.generic_input_2.label}</div>
  <div class="content">{$form.generic_input_2.html}</div>
  <div class="label">{$form.generic_match_2.label}</div>
  <div class="content">{$form.generic_match_2.html}</div>
  <div class="clear"></div>
  </div>
<div class="crm-section">
  <div class="label">{$form.generic_input_3.label}</div>
  <div class="content">{$form.generic_input_3.html}</div>
  <div class="label">{$form.generic_match_3.label}</div>
  <div class="content">{$form.generic_match_3.html}</div>
  <div class="clear"></div>
  </div>
{/crmScope}
