{crmScope extensionKey='dataprocessor'}
  {include file="CRM/Dataprocessor/Form/Field/Configuration/DateFieldOutputHandler.tpl"}

  <hr >
  <div class="crm-section">
    <div class="label">{$form.date_end.label}</div>
    <div class="content">{$form.date_end.html}</div>
    <div class="clear"></div>
  </div>
  <div class="crm-section">
    <div class="label">{$form.separator.label}</div>
    <div class="content">
      {$form.separator.html}
      <p class="description">
        {ts}The string to separate the start and end date (including spaces).{/ts}
      </p>
    </div>
    <div class="clear"></div>
  </div>
  <hr >
  <div class="crm-section">
    <div class="label">{$form.format_date_only.label}</div>
    <div class="content">
      {$form.format_date_only.html}
      <p class="description">
        {ts}Used if time is not set / default time <em>00:00:00</em>.{/ts}<br />
        {ts 1='https://www.php.net/manual/en/datetime.format.php#refsect1-datetime.format-parameters'}See <a href="%1" target="_blank">DateTime format on php.net</a> for help.{/ts}
      </p>
    </div>
    <div class="clear"></div>
  </div>
{/crmScope}
