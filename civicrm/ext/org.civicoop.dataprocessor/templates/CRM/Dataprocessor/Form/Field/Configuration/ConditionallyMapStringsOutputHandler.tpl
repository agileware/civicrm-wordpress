{crmScope extensionKey='dataprocessor'}
{include file="CRM/Dataprocessor/Form/Field/Configuration/SimpleFieldOutputHandler.tpl"}
    <p class="help">{ts}Compare the input field with up to 5 values. Replace the input value with the replacement of the first value that matches.{/ts}</p>
<div class="crm-section">
  <div class="label">{$form.condition_1.label}</div>
  <div class="content">{$form.condition_1.html}</div>
  <div class="label">{$form.replacement_1.label}</div>
  <div class="content">{$form.replacement_1.html}</div>
  <div class="clear"></div>
  </div>
<div class="crm-section">
  <div class="label">{$form.condition_2.label}</div>
  <div class="content">{$form.condition_2.html}</div>
  <div class="label">{$form.replacement_2.label}</div>
  <div class="content">{$form.replacement_2.html}</div>
  <div class="clear"></div>
  </div>
<div class="crm-section">
  <div class="label">{$form.condition_3.label}</div>
  <div class="content">{$form.condition_3.html}</div>
  <div class="label">{$form.replacement_3.label}</div>
  <div class="content">{$form.replacement_3.html}</div>
  <div class="clear"></div>
  </div>
<div class="crm-section">
  <div class="label">{$form.condition_4.label}</div>
  <div class="content">{$form.condition_4.html}</div>
  <div class="label">{$form.replacement_4.label}</div>
  <div class="content">{$form.replacement_4.html}</div>
  <div class="clear"></div>
  </div>
<div class="crm-section">
  <div class="label">{$form.condition_5.label}</div>
  <div class="content">{$form.condition_5.html}</div>
  <div class="label">{$form.replacement_5.label}</div>
  <div class="content">{$form.replacement_5.html}</div>
  <div class="clear"></div>
  </div>
<div class="crm-section">
  <div class="label">{$form.default.label}</div>
  <div class="content">{$form.default.html}</div>
  <div class="clear"></div>
</div>
    <p class="help">{ts}If no default value is provided, the original input is returned, if none of the comparison values matches.{/ts}</p>
{/crmScope}