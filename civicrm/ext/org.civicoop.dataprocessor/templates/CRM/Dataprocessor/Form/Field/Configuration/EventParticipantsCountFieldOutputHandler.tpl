{crmScope extensionKey='dataprocessor'}
    <div class="crm-section">
        <div class="label">{$form.field.label}</div>
        <div class="content">{$form.field.html}</div>
        <div class="clear"></div>
    </div>
    <div class="crm-section">
        <div class="label">{$form.role_ids.label}</div>
        <div class="content">{$form.role_ids.html}</div>
        <div class="clear"></div>
    </div>
    <div class="crm-section">
        <div class="label">{$form.status_ids.label}</div>
        <div class="content">{$form.status_ids.html}</div>
        <div class="clear"></div>
    </div>
  <div class="crm-section">
    <div class="label">{$form.show_as_link.label}</div>
    <div class="content">{$form.show_as_link.html}</div>
    <div class="clear"></div>
  </div>
  <div class="crm-section">
    <div class="label"></div>
    <div class="content"><p class="description">{ts 1=https://lab.civicrm.org/partners/civicoop/snj/snjfixparticipantsearch}When you have selected show link to manage participant you also might need to install <a href="%1">SNJ Fix for Participant Search Extension</a>{/ts}</p></div>
    <div class="clear"></div>
  </div>
{/crmScope}
