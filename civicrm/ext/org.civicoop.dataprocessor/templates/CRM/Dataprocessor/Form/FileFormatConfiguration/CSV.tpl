{crmScope extensionKey='dataprocessor'}
  <div class="crm-section">
    <div class="label">{$form.delimiter.label}</div>
    <div class="content">{$form.delimiter.html}</div>
    <div class="clear"></div>
  </div>
  <div class="crm-section">
    <div class="label">{$form.enclosure.label}</div>
    <div class="content">{$form.enclosure.html}</div>
    <div class="clear"></div>
  </div>
  <div class="crm-section">
    <div class="label">{$form.escape_char.label}</div>
    <div class="content">{$form.escape_char.html}</div>
    <div class="clear"></div>
  </div>
{/crmScope}
