{assign var=relativeName   value=$fieldName|cat:"_relative"}
<span>
  {$form.$relativeName.html}<br />
  <span class="crm-absolute-date-range">
    <span class="crm-absolute-date-from">
      {assign var=fromName   value=$fieldName|cat:$from}
        {$form.$fromName.label}
        {$form.$fromName.html}
    </span>
    <span class="crm-absolute-date-to">
      {assign var=toName   value=$fieldName|cat:$to}
        {$form.$toName.label}
        {$form.$toName.html}
    </span>
  </span>
</span>
    {literal}
    <script type="text/javascript">
      cj("#{/literal}{$relativeName}{literal}").change(function() {
        console.log('change '+cj(this).val());
        var n = cj(this).parent();
        if (cj(this).val() == "0") {
          cj(".crm-absolute-date-range", n).show();
        } else {
          cj(".crm-absolute-date-range", n).hide();
          cj('.crm-absolute-date-range :text', n).val('');
        }
      }).change();
    </script>
    {/literal}
