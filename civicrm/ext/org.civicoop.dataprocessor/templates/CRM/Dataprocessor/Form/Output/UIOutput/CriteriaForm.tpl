{crmScope extensionKey='dataprocessor'}
{if $has_exposed_filters || $hard_limit_enabled}
  <div class="crm-form-block crm-search-form-block">
    <div id="searchForm" class="form-item">
      <div class="crm-accordion-wrapper crm-advanced_search_form-accordion {if (!empty($dataprocessor_rows))}collapsed{/if}">
          <div class="crm-accordion-header crm-master-accordion-header">
              {if isset($criteriaFormTitle)}{$criteriaFormTitle}{else}{ts}Edit Search Criteria{/ts}{/if}
          </div>
          <!-- /.crm-accordion-header -->
          <div class="crm-accordion-body">
              {if $has_exposed_filters}
              {foreach from=$filters item=filterCollection}
                  {if $filterCollection.title}
                    <div class="crm-accordion-wrapper {$filterCollection.class}">
                    <div class="crm-accordion-header">{$filterCollection.title}</div>
                    <div class="crm-accordion-body">
                      {if $filterCollection.description}<div class="help">{$filterCollection.description}</div>{/if}
                  {/if}

                  <table>
                      <tr>
                          <th>{ts}Name{/ts}</th>
                          <th>{ts}Operator{/ts}</th>
                          <th>{ts}Value{/ts}</th>
                      </tr>
                      {foreach from=$filterCollection.filters key=filterName item=filter}
                          {assign var="filterAlias" value=$filterName}
                          {if isset($filter.alias)}{assign var="filterAlias" value="`$filter.alias`"}{/if}
                          {include file=$filter.template filterName=$filterAlias filter=$filter.filter}
                      {/foreach}
                      {if $additional_criteria_template && is_array($additional_criteria_template)}
                        {foreach from=$additional_criteria_template item=template}
                          {include file=$template}
                        {/foreach}
                      {elseif $additional_criteria_template}
                        {include file=$additional_criteria_template}
                      {/if}
                  </table>
                  {if $filterCollection.title}
                  </div>
                  </div>
                  {/if}
              {/foreach}
              {/if}
              {if $hard_limit_enabled}
                <div class="crm-accordion-wrapper">
                  <div class="crm-accordion-header">{ts}Limit results to a maximum number of records.{/ts}</div>
                  <div class="crm-accordion-body">
                    <table>
                      <tr>
                        <td class="label">{$form.hard_limit.label}</td>
                        <td>{$form.hard_limit.html}</td>
                      </tr>
                    </table>
                  </div>
                </div>
              {/if}
                  <div class="crm-submit-buttons">{include file="CRM/common/formButtons.tpl" location="botton"}</div>
              </div>
          </div>
      </div>
  </div>

  <script type="text/javascript">
    {foreach from=$filters item=filterCollection}
      {foreach from=$filterCollection.filters key=filterName item=filter}
        {literal}var val = "dnc";{/literal}
        {assign var=fieldOp     value=$filterName|cat:"_op"}
        {if isset($form.$fieldOp)}
                {literal}var val = document.getElementById("{/literal}{$fieldOp}{literal}").value;{/literal}
        {/if}
        {literal}showHideMaxMinVal( "filter-{/literal}{$filterName}{literal}", val );{/literal}
        {literal}initializeOperator( "filter-{/literal}{$filterName}{literal}");{/literal}
        {literal}
          CRM.$(function($) {
            cj("#{/literal}{$fieldOp}{literal}").change(function() {
              var val = $(this).val();
              {/literal}{if $filterName}showHideMaxMinVal("filter-{$filterName}", val );{/if}{literal}
            });
          });
        {/literal}
      {/foreach}
    {/foreach}

      {literal}
      function showHideMaxMinVal( field, val ) {
        var fldVal    = field + "_value_cell";
        var fldMinMax = field + "_min_max_cell";
        if ( val == "bw" || val == "nbw" ) {
          cj('#' + fldVal ).hide();
          cj('#' + fldMinMax ).show();
        } else if (val =="nll" || val == "nnll") {
          cj('#' + fldVal).hide() ;
          cj('#' + field + '_value').val('');
          cj('#' + fldMinMax ).hide();
        } else {
          cj('#' + fldVal ).show();
          cj('#' + fldMinMax ).hide();
        }
      }

      function initializeOperator(filterName) {
        var currentOp = cj('.filter-processor-element.'+filterName+' select option:selected');
        cj('.filter-processor-element.'+filterName).addClass('hiddenElement');
        cj('.filter-processor-show-close.'+filterName).html(currentOp.html() + '&nbsp;<i class="crm-i fa-pencil">&nbsp;</i>');
        cj('.filter-processor-show-close.'+filterName).attr('title', '{/literal}{ts}Change{/ts}{literal}');
        cj('.filter-processor-show-close.'+filterName).addClass('crm-editable-enabled');
        cj('.filter-processor-show-close.'+filterName).click(function () {
          cj('.filter-processor-element.'+filterName).removeClass('hiddenElement');
          cj('.filter-processor-show-close.'+filterName).removeClass('crm-editable-enabled');
          cj('.filter-processor-show-close.'+filterName).addClass('hiddenElement');
        });
      }

  </script>
  {/literal}
{/if}
{/crmScope}
