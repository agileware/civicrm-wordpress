{crmScope extensionKey='dataprocessor'}
    <h3>{ts}Filter Collections{/ts}</h3>
    <div class="crm-block crm-form-block crm-data-processor_filter_collections-block">
        <table>
            <tr>
                <th>{ts}Title{/ts}</th>
                <th>{ts}Link to previous filter collection{/ts}</th>
                <th>{ts}Condition within this filter collection{/ts}</th>
                <th></th>
                <th></th>
            </tr>
            {foreach from=$filterCollections item=filterCollection}
                <tr>
                    <td>
                        {$filterCollection.title}
                        <span class="description">{$filter.name}</span>
                    </td>
                    <td>
                      {$filterCollection.link_condition}
                    </td>
                    <td>
                        {$filterCollection.condition}
                    </td>
                    <td style="width: 60px">{if ($filterCollection.weight && !is_numeric($filterCollection.weight))}{$filterCollection.weight}{/if}</td>
                    <td class="right nowrap" style="width: 100px;">
                        <div class="crm-configure-actions">
                        <span class="btn-slide crm-hover-button">{ts}Configure{/ts}
                        <ul class="panel">
                            <li><a class="action-item crm-hover-button" href="{crmURL p="civicrm/dataprocessor/form/filtercollection" q="reset=1&action=update&data_processor_id=`$filterCollection.data_processor_id`&id=`$filterCollection.id`"}">{ts}Configuration{/ts}</a></li>
                            <li><a class="action-item crm-hover-button" href="{crmURL p="civicrm/dataprocessor/form/filtercollection" q="reset=1&action=delete&data_processor_id=`$filterCollection.data_processor_id`&id=`$filterCollection.id`"}">{ts}Remove{/ts}</a></li>
                        </ul>
                        </span>
                        </div>
                    </td>
                </tr>
            {/foreach}
        </table>
    </div>
{/crmScope}
