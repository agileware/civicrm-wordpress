<div class="crm-section">
  <div class="label">{$form.restrict_tasks.label}</div>
  <div class="content">{$form.restrict_tasks.html}</div>
  <div class="clear"></div>
</div>
<div class="crm-section" id="section-restrict-tasks">
  <input type="hidden" name="sorted_tasks" id="sorted_tasks" value="" />
  <div class="label">{$form.tasks_checkboxes.label}</div>
  <div class="content">
    <ul id="tasks" class="crm-checkbox-list crm-sortable-list" style="width: 600px;">
      {foreach from=$tasks item="task"}
        <li id="{$task}">
          {$form.tasks_checkboxes.$task.html}
        </li>
      {/foreach}
    </ul>
  </div>
  <div class="clear"></div>
</div>
{literal}
  <script type="text/javascript">
    cj("#restrict_tasks").click(restrictTasksChange);
    function restrictTasksChange() {
      if (document.getElementById("restrict_tasks").checked) {
        cj('#section-restrict-tasks').show();
      } else {
        cj('#section-restrict-tasks').hide();
      }
    }
    restrictTasksChange();

    function getTaskSorting(e, ui) {
      var params = [];
      var items = cj("#tasks li");
      if (items.length > 0) {
        for (var y = 0; y < items.length; y++) {
          var idState = items[y].id.substring(5);
          params[y + 1] = idState;
        }
      }
      cj('#sorted_tasks').val(params.toString());
    }

    cj("#tasks").sortable({
      placeholder: 'ui-state-highlight',
      update: getTaskSorting
    });
  </script>
{/literal}
