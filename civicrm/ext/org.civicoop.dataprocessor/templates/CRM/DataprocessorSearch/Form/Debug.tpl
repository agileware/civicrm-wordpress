{if $debug && (isset($debug_info.query) || isset($debug_info.count_query))}
    <div class="crm-block crm-form-block">
        <h3>Executes queries</h3>
        {foreach from=$debug_info.query item=query}
            <pre id="debug_info" class="linenums prettyprint prettyprinted" style="font-size: 11px; padding: 1em; border: 1px solid lightgrey; margin-top: 1em; overflow: auto;">{strip}
                {$query|replace:"SELECT":"SELECT\r\n "|replace:"FROM":"\r\nFROM"|replace:"INNER JOIN":"\r\nINNER JOIN"|replace:"LEFT JOIN":"\r\nLEFT JOIN"|replace:"WHERE":"\r\nWHERE"|replace:"GROUP BY":"\r\nGROUP BY"|replace:"ORDER BY":"\r\nORDER BY"|replace:"LIMIT":"\r\nLIMIT"|replace:"AND":"\r\n  AND"|replace:"`, `":"`,\r\n  `"}
            {/strip}</pre>
            <br />
        {/foreach}
        {foreach from=$debug_info.count_query item=query}
          <pre id="debug_info" class="linenums prettyprint prettyprinted" style="font-size: 11px; padding: 1em; border: 1px solid lightgrey; margin-top: 1em; overflow: auto;">{strip}
                {$query|replace:"SELECT":"SELECT\r\n "|replace:"FROM":"\r\nFROM"|replace:"INNER JOIN":"\r\nINNER JOIN"|replace:"LEFT JOIN":"\r\nLEFT JOIN"|replace:"WHERE":"\r\nWHERE"|replace:"GROUP BY":"\r\nGROUP BY"|replace:"ORDER BY":"\r\nORDER BY"|replace:"LIMIT":"\r\nLIMIT"|replace:"AND":"\r\n  AND"|replace:"`, `":"`,\r\n  `"}
            {/strip}</pre>
          <br />
        {/foreach}
    </div>
{/if}
{if $debug && (isset($debug_info.query_time) || isset($debug_info.count_query_time))}
<div class="crm-block crm-form-block"><h3>Timings</h3>
  {if (isset($debug_info.count_query_time))}<p>Count Query Time: {$debug_info.count_query_time}</p>{/if}
  {if (isset($debug_info.query_time))}<p>Query Time: {$debug_info.query_time}</p>{/if}
</div>
{/if}
{if $debug && !is_array($debug_info)}
<div class="crm-block crm-form-block">
  <pre>
    {$debug}
  </pre>
</div>
{/if}
