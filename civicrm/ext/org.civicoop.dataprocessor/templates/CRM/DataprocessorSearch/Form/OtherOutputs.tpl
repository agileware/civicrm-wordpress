{if (isset($other_outputs) && !empty($other_outputs))}
    <div class="crm-block">
        {foreach from=$other_outputs item=other_output}
          <span class="action-link">
            <a class="other-output-button {$other_output.class}" href="#" data-output-id="{$other_output.id}">
                {if ($other_output.icon)}
                    {$other_output.icon}
                {/if}
                {$other_output.title}
            </a>
           </span>
        {/foreach}
        {if $exportButtonName}
            {$form.$exportButtonName.html} &nbsp; &nbsp;
        {/if}
    </div>
    <div class="crm-clear"></div>
{/if}
