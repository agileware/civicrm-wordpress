--
-- Alter table structure `civicrm_job_scheduled`
--

ALTER TABLE `civicrm_job_scheduled`
DROP `weekday`,
DROP `day`,
DROP `hour`,
DROP `minute`;
