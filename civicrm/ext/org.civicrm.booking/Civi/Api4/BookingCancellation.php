<?php
namespace Civi\Api4;

/**
 * BookingCancellation entity.
 *
 * Provided by the CiviBooking extension.
 *
 * @package Civi\Api4
 */
class BookingCancellation extends Generic\DAOEntity {

}
