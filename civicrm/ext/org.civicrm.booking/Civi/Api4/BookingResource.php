<?php
namespace Civi\Api4;

/**
 * BookingResource entity.
 *
 * Provided by the CiviBooking extension.
 *
 * @package Civi\Api4
 */
class BookingResource extends Generic\DAOEntity {

}
