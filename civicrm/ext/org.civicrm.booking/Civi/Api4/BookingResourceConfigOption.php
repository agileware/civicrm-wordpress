<?php
namespace Civi\Api4;

/**
 * BookingResourceConfigOption entity.
 *
 * Provided by the CiviBooking extension.
 *
 * @package Civi\Api4
 */
class BookingResourceConfigOption extends Generic\DAOEntity {

}
