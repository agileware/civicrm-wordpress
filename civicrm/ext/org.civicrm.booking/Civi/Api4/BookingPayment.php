<?php
namespace Civi\Api4;

/**
 * BookingPayment entity.
 *
 * Provided by the CiviBooking extension.
 *
 * @package Civi\Api4
 */
class BookingPayment extends Generic\DAOEntity {

}
