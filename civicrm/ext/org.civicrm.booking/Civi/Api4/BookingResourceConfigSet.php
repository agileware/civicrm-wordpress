<?php
namespace Civi\Api4;

/**
 * BookingResourceConfigSet entity.
 *
 * Provided by the CiviBooking extension.
 *
 * @package Civi\Api4
 */
class BookingResourceConfigSet extends Generic\DAOEntity {

}
