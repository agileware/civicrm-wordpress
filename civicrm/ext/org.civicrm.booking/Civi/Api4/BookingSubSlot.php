<?php
namespace Civi\Api4;

/**
 * BookingSubSlot entity.
 *
 * Provided by the CiviBooking extension.
 *
 * @package Civi\Api4
 */
class BookingSubSlot extends Generic\DAOEntity {

}
