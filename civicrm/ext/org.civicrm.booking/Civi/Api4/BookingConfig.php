<?php
namespace Civi\Api4;

/**
 * BookingConfig entity.
 *
 * Provided by the CiviBooking extension.
 *
 * @package Civi\Api4
 */
class BookingConfig extends Generic\DAOEntity {

}
