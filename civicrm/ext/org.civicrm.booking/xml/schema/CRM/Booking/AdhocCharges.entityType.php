<?php
// This file declares a new entity type. For more details, see "hook_civicrm_entityTypes" at:
// https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_entityTypes
return [
  [
    'name' => 'BookingAdhocCharges',
    'class' => 'CRM_Booking_DAO_AdhocCharges',
    'table' => 'civicrm_booking_adhoc_charges',
  ],
];
