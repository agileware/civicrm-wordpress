<?php
// This file declares a new entity type. For more details, see "hook_civicrm_entityTypes" at:
// https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_entityTypes
return [
  [
    'name' => 'BookingCancellation',
    'class' => 'CRM_Booking_DAO_Cancellation',
    'table' => 'civicrm_booking_cancellation',
  ],
];
