<?php
// This file declares a new entity type. For more details, see "hook_civicrm_entityTypes" at:
// https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_entityTypes
return [
  [
    'name' => 'BookingSubSlot',
    'class' => 'CRM_Booking_DAO_SubSlot',
    'table' => 'civicrm_booking_sub_slot',
  ],
];
