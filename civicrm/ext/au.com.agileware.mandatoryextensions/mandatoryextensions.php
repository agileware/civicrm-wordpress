<?php

require_once 'mandatoryextensions.civix.php';
use CRM_Mandatoryextensions_ExtensionUtil as E;

/**
 * Implements hook_civicrm_config().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_config/
 */
function mandatoryextensions_civicrm_config(&$config) {
  _mandatoryextensions_civix_civicrm_config($config);
}

/**
 * Implements hook_civicrm_install().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_install
 */
function mandatoryextensions_civicrm_install() {
  _mandatoryextensions_civix_civicrm_install();
}

/**
 * Implements hook_civicrm_postInstall().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_postInstall
 */
function mandatoryextensions_civicrm_postInstall() {
  _mandatoryextensions_civix_civicrm_postInstall();
}

/**
 * Implements hook_civicrm_uninstall().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_uninstall
 */
function mandatoryextensions_civicrm_uninstall() {
  _mandatoryextensions_civix_civicrm_uninstall();
}

/**
 * Implements hook_civicrm_enable().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_enable
 */
function mandatoryextensions_civicrm_enable() {
  _mandatoryextensions_civix_civicrm_enable();
}

/**
 * Implements hook_civicrm_disable().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_disable
 */
function mandatoryextensions_civicrm_disable() {
  _mandatoryextensions_civix_civicrm_disable();
}

/**
 * Implements hook_civicrm_upgrade().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_upgrade
 */
function mandatoryextensions_civicrm_upgrade($op, CRM_Queue_Queue $queue = NULL) {
  return _mandatoryextensions_civix_civicrm_upgrade($op, $queue);
}

/**
 * Implements hook_civicrm_entityTypes().
 *
 * Declare entity types provided by this module.
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_entityTypes
 */
function mandatoryextensions_civicrm_entityTypes(&$entityTypes) {
  _mandatoryextensions_civix_civicrm_entityTypes($entityTypes);
}

/**
 * Implements hook_civicrm_postProcess().
 */
function mandatoryextensions_civicrm_postProcess($formName, &$form) {
  // If the user submits the ScheduleReminders form with "- neither -", unset the group id.
  if (($form instanceof CRM_Admin_Form_ScheduleReminders) &&
    (($form->getSubmittedValue('limit_to') == '') || is_null($form->getSubmittedValue('limit_to')))) {
    // The string "null" because an actual null would indicate don't save this field.
    CRM_Core_DAO::setFieldValue('CRM_Core_DAO_ActionSchedule', $form->get('id'), 'group_id', 'null');
  }
}

// --- Functions below this ship commented out. Uncomment as required. ---

/**
 * Implements hook_civicrm_preProcess().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_preProcess
 *

 // */

/**
 * Implements hook_civicrm_navigationMenu().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_navigationMenu
 *
function mandatoryextensions_civicrm_navigationMenu(&$menu) {
  _mandatoryextensions_civix_insert_navigation_menu($menu, 'Mailings', array(
    'label' => E::ts('New subliminal message'),
    'name' => 'mailing_subliminal_message',
    'url' => 'civicrm/mailing/subliminal',
    'permission' => 'access CiviMail',
    'operator' => 'OR',
    'separator' => 0,
  ));
  _mandatoryextensions_civix_navigationMenu($menu);
} // */
