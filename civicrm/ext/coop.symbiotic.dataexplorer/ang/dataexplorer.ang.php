<?php
// This file declares an Angular module which can be autoloaded
// in CiviCRM. See also:
// https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_angularModules
return array(
  'js' => array(
    0 => 'ang/dataexplorer-utils.js',
    1 => 'ang/dataexplorer-export.js',
    2 => 'ang/dataexplorer.js',
    3 => 'ang/dataexplorer/*.js',
    4 => 'ang/dataexplorer/*/*.js',
  ),
  'css' => array(
    // 0 => 'ang/dataexplorer.css',
  ),
  'partials' => array(
    0 => 'ang/dataexplorer',
  ),
  'requires' => array(
    0 => 'crmUi',
    1 => 'crmUtil',
    2 => 'ngRoute',
  ),
  'settings' => array(
  ),
);
