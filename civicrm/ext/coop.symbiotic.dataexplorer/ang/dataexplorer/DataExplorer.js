(function(angular, $, _) {

  if (typeof CRM.vars.dataexplorer == 'undefined') {
    return;
  }

  // FIXME: cleanup, copied from api4
  // Schema metadata
  var schema = CRM.vars.dataexplorer.schema;
  // Cache list of entities
  var entities = [];
  // Cache list of actions
  var actions = [];
  // Field options
  var fieldOptions = {};

  var all_measures = [];
  var all_filters = [];
  var all_groups = [];

  angular.module('dataexplorer').config(function($routeProvider) {
    $routeProvider.when('/dataexplorer/:id?/:key?', {
      controller: 'DataexplorerDataExplorer',
      templateUrl: '~/dataexplorer/DataExplorer.html',
      resolve: {
        // FIXME: This should use crmApi4 instead of CRM.api4,
        // but it does not get resolved?
        explorerconfig: function($route) {
          // Do not send more params ot the API than necessary, because it will do its own filtering of results
          if (typeof $route.current.params.id != 'undefined' && $route.current.params.key) {
            return CRM.api4('Dataexplorer', 'get', {where: [['id', '=', $route.current.params.id], ['share_key', '=', $route.current.params.key]]});
          }
          else if (typeof $route.current.params.id != 'undefined') {
            return CRM.api4('Dataexplorer', 'get', {where: [['id', '=', $route.current.params.id]]});
          }
          return {};
        }
      }
    });
  });

  // The controller uses *injection*. This default injects a few things:
  //   $scope -- This is the set of variables shared between JS and HTML.
  //   crmApi, crmStatus, crmUiHelp -- These are services provided by civicrm-core.
  //   myContact -- The current contact, defined above in config().
  angular.module('dataexplorer').controller('DataexplorerDataExplorer', function($scope, $location, $timeout, $filter, $routeParams, crmApi, crmStatus, crmUiHelp, explorerconfig) {
    // The ts() and hs() functions help load strings for this module.
    var ts = $scope.ts = CRM.ts('dataexplorer');
    var hs = $scope.hs = crmUiHelp({file: 'CRM/dataexplorer/DataExplorer'}); // See: templates/CRM/dataexplorer/DataExplorer.hlp

    // FIXME Presumably a side-effect of not using crmApi4 in the resolve
    if (typeof explorerconfig[0] != 'undefined') {
      explorerconfig = explorerconfig[0];
    }

    // Define early on, because it is accessed by the loading of existing displays
    $scope.view_mode = {
      'table': false,
      'barchart': false,
      'linechart': false,
      'piechart': false,
      'map': false
    };

    // This is the list of enabled selections (from the select2 fields)
    // but for filter/groupby they have more info in (..todo).
    $scope.measure = '';
    $scope.filter = '';
    $scope.groupby = '';

    $scope.all_measures = [];
    $scope.all_filters = [];
    $scope.all_groups = [];

    // Values that get sent to the 'Generator' classes (ajax requests)
    $scope.measure_values = {};
    $scope.filter_values = {};
    $scope.groupby_values = {};

    $scope.sharing_is_enabled = (explorerconfig.share_enabled ? true : false);
    $scope.share_key = explorerconfig.share_key || '';
    $scope.share_link = '';

    $scope.dashlet_is_enabled = (explorerconfig.dashlet_enabled ? true : false);
    $scope.dashlet_title = explorerconfig.dashlet_title || '';
    $scope.dashlet_more_link = explorerconfig.dashlet_more_link || '';

    // https://stackoverflow.com/a/20262097
    $scope.share_expire_date = null;

    // ID suffix for divs referenced to display/hide charts
    // Important for when there are multiple visualisations displayed (ex: dashlets)
    $scope.div_suffix = '';

    if (explorerconfig.share_expire_date) {
      $scope.share_expire_date = new Date(explorerconfig.share_expire_date + 'T23:59:59'); // , 'yyyy-MM-dd');
    }
    if (explorerconfig.share_key) {
      // FIXME: redundant with generateSharingLink(), but not yet defined at this point
      $scope.share_link = $location.absUrl() + '/' + $scope.share_key;
    }

    // There is a lot of legacy in how 'data' is handled, this makes little sense
    var data = JSON.stringify(explorerconfig.data) || '';

    $scope.id = explorerconfig.id || 0;
    $scope.dz = new dzSettings($scope.id, data);
    $scope.charts = {};

    if ($scope.id) {
      CRM.dataexplorer_load_preferences($scope, data);
      CRM.dataexplorer_refresh($scope.dz, $scope);
    }
    else {
      // Default to 'table' view
      $scope.view_mode.table = true;
      $scope.has_view_mode = true;
    }

    // Check if the user tried to access a visualisation but did not have access
    if ($routeParams.id && typeof explorerconfig.id == 'undefined') {
      CRM.alert(ts('You do not have access to this visualisation. The link may have expired or it may have been deleted.'), ts('Access Denied'), 'error');
      $location.url('/dataexplorer');
    }

    if (!$scope.all_measures.length) {
      formatForSelect2(CRM.vars.dataexplorer.measures, $scope.all_measures);
    }
    if (!$scope.all_filters.length) {
      // @todo More duplicated code (c.f. watch and dataexplorer.js)
      _.each(CRM.vars.dataexplorer.filters, function(g, key) {
        var formatted = {id: key, text: g.label};
        $scope.all_filters.push(formatted);
      });
    }
    if (!$scope.all_groups.length) {
      _.each(CRM.vars.dataexplorer.groups, function(g, key) {
        var formatted = {id: key, text: g.label};
        $scope.all_groups.push(formatted);
      });
    }

    $scope.dataExplorerConfig = CRM.vars.dataexplorer;

    // This is to make sure that the help message displays
    CRM.dataexplorer_refresh($scope.dz, $scope);

    // Set the selected measures in 'dz' (which will hopefully go away soon).
    // and eventually update the available filters/groups.
    $scope.$watch('measure', function(newVal, oldVal) {
      if (newVal == oldVal) {
        return;
      }

      if (!$.isEmptyObject(oldVal)) {
        _.each(oldVal.split(','), function(item) {
          if (typeof $scope.measure_values[item] != 'undefined') {
            delete $scope.measure_values[item];
          }
        });
      }

      if (!$.isEmptyObject(newVal)) {
        _.each(newVal.split(','), function(item) {
          $scope.measure_values[item] = 'count';
        });
      }

      // Refresh available filters
      all_filters = [];

      // Because we are in a watch, this is the only way to update select2 options?
      $timeout(function() {
        // This 'if' is because we are simulating dynamic filters for now
        // it should go away.
        if (newVal) {
          _.each(CRM.vars.dataexplorer.filters, function(g, key) {
            var formatted = {id: key, text: g.label};
            all_filters.push(formatted);
          });
        }

        $scope.all_filters = all_filters;
      }, 100);

      // Refresh available groupbys/display
      all_groups = [];

      // Because we are in a watch, this is the only way to update select2 options?
      $timeout(function() {
        // This 'if' is because we are simulating dynamic filters for now
        // it should go away.
        if (newVal) {
          _.each(CRM.vars.dataexplorer.groups, function(g, key) {
            var formatted = {id: key, text: g.label};
            all_groups.push(formatted);
          });
        }

        $scope.all_groups = all_groups;
      }, 100);

      CRM.dataexplorer_refresh($scope.dz, $scope);
    });

    // FIXME: This is a really weird way to get updates from the 'filter' select2
    // which either:
    // - if it's initialized too early, it's not clear how to updates its options
    // - if it's in an if-ng scope, it loses its binding.
    $scope.$watch(function() {
      return $('#dataexplorer-input-filter').val();
    }, function(newVal, oldVal) {
      if (newVal == oldVal) {
        return;
      }

      $scope.filter = newVal;

      if (!$.isEmptyObject(oldVal)) {
        _.each(oldVal.split(','), function(item) {
          $scope.dz.remove('filter-' + item);
        });
      }

      if (!$.isEmptyObject(newVal)) {
        _.each(newVal.split(','), function(item) {
          // FIXME this should set the actual filter value, or not set it for now and wait for selections, ex: dz.set('filter-[foo]-123', 1)
          // FIXME save in measure_values.
          $scope.dz.set('filter-' + item, 1);
        });
      }

      CRM.dataexplorer_refresh($scope.dz, $scope);
    });

    // @todo Horrible copy-paste of bad code.
    $scope.$watch(function() {
      return $('#dataexplorer-input-groupby').val();
    }, function(newVal, oldVal) {
      if (newVal == oldVal) {
        return;
      }

      $scope.groupby = newVal;

      if (!$.isEmptyObject(oldVal)) {
        _.each(oldVal.split(','), function(item) {
          $scope.dz.remove('groupby-' + item);
        });
      }

      if (!$.isEmptyObject(newVal)) {
        _.each(newVal.split(','), function(item) {
          // FIXME this should set the actual filter value, or not set it for now and wait for selections, ex: dz.set('filter-[foo]-123', 1)
          // FIXME save in measure_values.
          $scope.dz.set('groupby-' + item, 1);
        });
      }

      CRM.dataexplorer_refresh($scope.dz, $scope);
    });

    // Reformat an existing array of objects for compatibility with select2
    // Adapted from the api4 extension.
    function formatForSelect2(input, container, key, extra, prefix) {
      _.each(input, function(label, key) {
        var formatted = {id: key, text: label};
        container.push(formatted);
      });
      return container;
    }

    /**
     * Toggle the view mode (table, barchart, etc).
     */
    $scope.viewModeToggle = function viewModeToggle(view_mode) {
      $scope.view_mode[view_mode] = $scope.view_mode[view_mode] ? false : true;
      $scope.has_view_mode = false;

      _.each($scope.view_mode, function(val, key) {
        if (val) {
          $scope.has_view_mode = true;
        }
      });

      CRM.dataexplorer_refresh($scope.dz, $scope);
    };

    /**
     * Measure type selection
     */
    $scope.updateCalculate = function updateCalculate(item, value) {
      $scope.measure_values[item] = value;
      CRM.dataexplorer_refresh($scope.dz, $scope);
    };

    /**
     * Filter selection
     *
     * Is this function required at all? Since most elements bind the model.
     */
    $scope.updateFilter = function filter(key, value) {
      // FIXME: Better check if the object is a date?
      if (typeof $scope.filter_values[key] == 'object') {
        // TODO: remove the filter if empty.
        // FIXME dz.set is deprecated
        // and we do not need to set in filter_values because of the model binding
        // $scope.dz.set('filter-' + key, $filter('date')($scope.filter_values[key], "yyyy-MM-dd"));
      }
      else {
        $scope.filter_values[key] = value;
      }
      CRM.dataexplorer_refresh($scope.dz, $scope);
    };

    /**
     * Filter toggle (checkboxes)
     */
    $scope.toggleFilter = function filter(key, value) {
      if (typeof $scope.filter_values[key] != 'undefined') {
        // This is a toggle, we should probably have a toggleFilter instead
        delete $scope.filter_values[key];
      }
      else {
        $scope.filter_values[key] = value;
      }
      CRM.dataexplorer_refresh($scope.dz, $scope);
    };

    /**
     * Groupby selection
     */
    $scope.updateGroupby = function groupby(key, value) {
      if (typeof $scope.groupby_values[key] != 'undefined') {
        delete $scope.groupby_values[key];
      }
      else {
        $scope.groupby_values[key] = value;
      }

      CRM.dataexplorer_refresh($scope.dz, $scope);
    };

    /**
     * Clicking on a menu keeps it open.
     * Helps for accessibility and touch.
     */
    $scope.menuKeepOpen = function menuKeepOpen(category, key) {
      if ($('#dataexplorer-' + category + '-' + key).hasClass('dataexplorer-menu-forceopen')) {
        // Close the already opened menu
        $('#dataexplorer-group-' + category + ' .dataexplorer-menu-forceopen').removeClass('dataexplorer-menu-forceopen');
      }
      else {
        $('#dataexplorer-group-' + category + ' .dataexplorer-menu-forceopen').removeClass('dataexplorer-menu-forceopen');
        $('#dataexplorer-' + category + '-' + key).addClass('dataexplorer-menu-forceopen');
      }
    };

    /**
     * Saves the visualisation settings to the database.
     */
    $scope.save = function save() {
      var data = {
        view_mode: $scope.view_mode,
        measure: $scope.measure_values,
        filter: $scope.filter_values,
        groupby: $scope.groupby_values,
        dashlet_enabled: ($scope.dashlet_is_enabled ? 1 : 0),
        dashlet_title: $scope.dashlet_title,
        dashlet_more_link: $scope.dashlet_more_link
      };

      if ($scope.id) {
        CRM.api4('Dataexplorer', 'update', {
          where: [['id', '=', $scope.id]],
          values: {
            data: data,
            share_enabled: ($scope.sharing_is_enabled ? 1 : 0),
            share_key: $scope.share_key,
            share_expire_date: $scope.share_expire_date
          }
        })
        .then(function(result) {
          CRM.status('Saved');
          $scope.generateSharingLink();
        }, function(failure) {
          CRM.alert('Error saving, see console');
          console.log(failure);
        });
      }
      else {
        CRM.api4('Dataexplorer', 'create', {
          values: {
            data: data
          }
        })
        .then(function(result) {
          CRM.status(ts('Saved'));
          $scope.id = result[0];
          $location.url('/dataexplorer/' + $scope.id);
        }, function(failure) {
          CRM.alert(ts('Error saving, see console'));
          console.log(failure);
        });
      }
    };

    /**
     *
     */
    $scope.updateSharing = function updateSharing(status) {
      if (!$scope.id) {
        CRM.alert(ts("You must first save this visualisation before enabling sharing."), '', 'warning');
        return;
      }

      $scope.sharing_is_enabled = status;

      // Generate a sharing key if there isn't one already
      // We are careful about not wiping an existing key, in case
      // the sharing was temporarily disabled.
      if ($scope.sharing_is_enabled && !$scope.share_key) {
        // This will call 'save()'
        $scope.generateSharingKey();
      }
      else {
        $scope.save();
      }
    };

    /**
     *
     */
    $scope.updateSharingDate = function updateSharingDate() {
      $scope.save();
    };

    /**
     *
     */
    $scope.updateDashlet = function updateDashlet(status) {
      if (!$scope.id) {
        CRM.alert(ts("You must first save this visualisation before enabling the dashlet."), '', 'warning');
        return;
      }

      $scope.dashlet_is_enabled = status;
      $scope.save();
    };

    /**
     * Generate a short unique ID.
     *
     * Source: http://stackoverflow.com/questions/6248666/how-to-generate-short-uid-like-ax4j9z-in-js
     */
    $scope.generateSharingKey = function generateSharingKey() {
      // TODO: if a key already exists, prompt the user?
      $scope.share_key = ("000000" + (Math.random()*Math.pow(36,6) << 0).toString(36)).slice(-6);
      $scope.save();
    };

    /**
     * Generate the sharing link (URL).
     */
    $scope.generateSharingLink = function generateSharingLink() {
      $scope.share_link = $location.absUrl() + '/' + $scope.share_key;
    };

  });

})(angular, CRM.$, CRM._);
