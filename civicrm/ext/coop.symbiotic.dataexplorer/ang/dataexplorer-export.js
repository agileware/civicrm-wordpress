cj(function($) {
  /**
   * Export SVG graph to JPG/PNG
   * Based on:
   * http://techslides.com/demos/d3/svg-to-image-2.html
   */
  $('.dataexplorer-button-canvas-to-image').click(function(event) {
    var html = d3.select("#chartContainer svg")
          .attr("version", 1.1)
          .attr("xmlns", "http://www.w3.org/2000/svg")
          .node().parentNode.innerHTML;

    var filetype = $(this).data('dataexplorerexportformat');

    html = html.replace('é', 'e', 'gi');
    html = html.replace('ê', 'e', 'gi');
    html = html.replace('è', 'e', 'gi');
    html = html.replace('à', 'a', 'gi');
    html = html.replace('ô', 'o', 'gi');

    var imgsrc = 'data:image/svg+xml;base64,' + window.btoa(html);
    var img = '<img src="' + imgsrc + '">';

    var canvas = document.querySelector("#chartCanvass");
    var context = canvas.getContext("2d");
    context.clearRect(0, 0, canvas.width, canvas.height);

    // set a white background color
    // http://blogs.adobe.com/digitalmedia/2011/01/setting-the-background-color-when-generating-images-from-canvas-todataurl/
    if (filetype == 'jpeg') {
      context.globalCompositeOperation = "destination-over";
      context.fillStyle = '#fff';
      context.fillRect(0, 0, canvas.width, canvas.height);
    }

    var image = new Image;
    image.src = imgsrc;
    image.onload = function() {
      context.drawImage(image, 0, 0);
      dataexplorer_canvas_to_image_download(canvas, filetype);
    };

    event.preventDefault();
  });

  /**
   * Export Leaftlet map to JPG/PNG
   * very similar to the previous function, except with special hacks for the map legend.
   */
  $('.dataexplorer-button-map-to-image').click(function(event) {
    var html = d3.select("#map svg")
          .attr("version", 1.1)
          .attr("xmlns", "http://www.w3.org/2000/svg")
          .node().parentNode.innerHTML;

    var filetype = $(this).data('dataexplorerexportformat');

    html = html.replace('é', 'e', 'gi');
    html = html.replace('ê', 'e', 'gi');
    html = html.replace('è', 'e', 'gi');
    html = html.replace('à', 'a', 'gi');
    html = html.replace('ô', 'o', 'gi');

    var imgsrc = 'data:image/svg+xml;base64,' + window.btoa(html);
    var img = '<img src="' + imgsrc + '">';

    var canvasmap = document.querySelector("#mapCanvass");
    var context = canvasmap.getContext("2d");
    context.clearRect(0, 0, canvasmap.width, canvasmap.height);

    // set a white background color
    // http://blogs.adobe.com/digitalmedia/2011/01/setting-the-background-color-when-generating-images-from-canvas-todataurl/
/* (causes problems with the legend)
    context.globalCompositeOperation = "destination-over";
*/
    if (filetype == 'jpeg') {
      context.fillStyle = '#fff';
      context.fillRect(0, 0, canvasmap.width, canvasmap.height);
    }

    var image = new Image;
    image.src = imgsrc;
    image.onload = function() {
      // Add the SVG of the map
      var ctxmap = canvasmap.getContext("2d");
      ctxmap.drawImage(image, 0, 0);

      html2canvas(document.querySelector('.map-legend')).then(function(canvas) {
        // Add the legend onto the background.
        ctxmap.drawImage(canvas, canvasmap.width - 175, canvasmap.height - 200);

        dataexplorer_canvas_to_image_download(canvasmap, filetype);
      })
    };

    event.preventDefault();
  });

  /**
   * Helper fun to convert a canvas to JPG/PNG and force download.
   */
  function dataexplorer_canvas_to_image_download(canvas, filetype) {
    var canvasdata = canvas.toDataURL('image/' + filetype);
    var pngimg = '<img src="' + canvasdata + '">';

    var a = document.createElement("a");
    a.download = "image." + filetype;
    a.href = canvasdata;
    document.body.appendChild(a);
    a.click();
  }
});
