{* @todo This would load multiple times if multiple dashlets. Maybe do like civicalendar does? *}
{if $dataexplorer_error}
  {$dataexplorer_error}
{else}
  {crmScope extensionKey="dataexplorer"}
  <div>
    <div id="dataexplorer-loading{$dataexplorer_id}">{ts}Loading...{/ts}</div>
    {* @todo *}
    {*
    <div id="mapContainerWrapper{$dataexplorer_id}">
      <div id="mapContainer"></div>
      <canvas id="mapCanvass" width="1199" height="550" class="hide"></canvas>
      <div id="svgmapdataurl" class="hide"></div>
    </div>
    *}
    <div id="barChartContainerWrapper{$dataexplorer_id}" style="height: 500px;">
      <canvas id="barChartContainer{$dataexplorer_id}"></canvas>
    </div>
    <div id="lineChartContainerWrapper{$dataexplorer_id}" style="height: 500px;">
      <canvas id="lineChartContainer{$dataexplorer_id}"></canvas>
    </div>
    <div id="doughnutChartContainerWrapper{$dataexplorer_id}" style="height: 500px;">
      <canvas id="doughnutChartContainer{$dataexplorer_id}"></canvas>
    </div>
    <div id="tableContainer{$dataexplorer_id}"></div>
  </div>
  {/crmScope}
{literal}
<script>
  var data = {/literal}{$dataexplorer_data}{literal};

  var dz = new dzSettings('test', '');
  var $scope = {};
  $scope.div_suffix = '{/literal}{$dataexplorer_id}{literal}';
  $scope.measure_values = data.data.measure;
  $scope.filter_values = data.data.filter;
  $scope.groupby_values = data.data.groupby;
  $scope.view_mode = data.data.view_mode;
  $scope.charts = {};

  CRM.dataexplorer_refresh(dz, $scope);
</script>
{/literal}
{/if}
