<?php

require_once 'dataexplorer.civix.php';
use CRM_Dataexplorer_ExtensionUtil as E;

/**
 * Implements hook_civicrm_config().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_config/
 * @link https://docs.civicrm.org/dev/en/latest/hooks/usage/symfony/
 */
function dataexplorer_civicrm_config(&$config) {
  if (isset(Civi::$statics[__FUNCTION__])) { return; }
  Civi::$statics[__FUNCTION__] = 1;

  Civi::dispatcher()->addListener('dataexplorer.boot', ['\Civi\DataExplorer\Events', 'fireDataExplorerBoot'], 90);

  _dataexplorer_civix_civicrm_config($config);
}

/**
 * Implements hook_civicrm_navigationMenu().
 *
 * @param array $menu
 */
function dataexplorer_civicrm_navigationMenu(&$menu) {
  _dataexplorer_civix_insert_navigation_menu($menu, 'Reports', [
    'label' => E::ts('Data Explorer'),
    'name' => 'dataexplorer',
    'url' => 'civicrm/dataexplorer#/dataexplorer',
    'permission' => 'access CiviCRM',
    'operator' => NULL,
    'separator' => NULL,
  ]);
}

/**
 * Implements hook_civicrm_pageRun().
 */
function dataexplorer_civicrm_pageRun(&$page) {
  $pageName = get_class($page);

  if ($pageName == 'CRM_Contact_Page_DashBoard') {
    // This needs to be added too, but AngularJS loads them automatically when in the normal full page view
    Civi::resources()
      ->addScriptFile('dataexplorer', 'ang/dataexplorer.js', 1, 'html-header')
      ->addScriptFile('dataexplorer', 'ang/dataexplorer-utils.js', 1, 'html-header');

    CRM_Dataexplorer_Utils::addGraphicsResources();
  }
}

/**
 * Implements hook_civicrm_install().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_install
 */
function dataexplorer_civicrm_install() {
  _dataexplorer_civix_civicrm_install();
}

/**
 * Implements hook_civicrm_enable().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_enable
 */
function dataexplorer_civicrm_enable() {
  _dataexplorer_civix_civicrm_enable();
}
