<?php

namespace Civi\DataExplorer;

use CRM_Dataexplorer_ExtensionUtil as E;
use \Civi\DataExplorer\Event\DataExplorerEvent;

class Events {

  /**
   * Implements the dataexplorer.boot event
   *
   * This declares the entities that we support out of the box.
   * Based on coding from a long time ago and it would be best to use something
   * more generic.
   */
  static public function fireDataExplorerBoot(DataExplorerEvent $event) {
    $sources = $event->getDataSources();
    $filters = $event->getFilters();
    $groups = $event->getGroupBy();

    // If CiviContribute is disabled, it will cause some API calls to fatal
    $hasCiviContribute = \CRM_Core_Component::isEnabled('CiviContribute');

    $sources['activity'] = ts('Activities');
    $sources['contact'] = ts('Contacts');

    if ($hasCiviContribute) {
      $sources['contribution'] = ts('Contributions');
    }
    $sources['membership'] = ts('Memberships (new)');
    $sources['participant'] = ts('Events Participants');
    $sources['event'] = ts('Events (n/a)');
    $sources['groupsubscription'] = ts('Group Subscriptions');
    $sources['groupunsubscription'] = ts('Group Unsubscriptions');
    $sources['mailing'] = ts('Mailings (n/a)');
    $sources['mailingrecipient'] = ts('Mailings Recipients');
    $sources['mailingclick'] = ts('Mailings Clics (n/a)');
    $sources['mailingbounce'] = ts('Mailings Bounces');
    $sources['mailingunsubscribe'] = ts('Mailings Unsubscribes (n/a)');

    // Filters
    if ($hasCiviContribute) {
      $ft = civicrm_api3('Contribution', 'getoptions', [
        'field' => 'financial_type_id',
      ])['values'];

      $payment_instruments = civicrm_api3('Contribution', 'getoptions', [
        'field' => 'payment_instrument_id',
      ])['values'];

      $payment_processors = \CRM_Dataexplorer_PseudoConstant::getPaymentProcessors();

      $contribution_statuses = \Civi\Api4\OptionValue::get(false)
        ->setSelect(['label', 'value'])
        ->addWhere('option_group_id:name', '=', 'contribution_status')
        ->addOrderBy('weight', 'ASC')
        ->execute()
        ->indexBy('value')
        ->column('label');

      $contrib_pages  = \Civi\Api4\ContributionPage::get()
        ->setCheckPermissions(FALSE)
        ->addSelect('title')
        ->addOrderBy('title', 'ASC')
        ->execute()
        ->indexBy('id')
        ->column('title');
    }

    $mtype = civicrm_api3('Membership', 'getoptions', [
      'field' => 'membership_type_id',
    ])['values'];

    $atype = civicrm_api3('Activity', 'getoptions', [
      'field' => 'activity_type_id',
    ])['values'];

    $astatus = civicrm_api3('Activity', 'getoptions', [
      'field' => 'activity_status_id',
    ])['values'];

    $relative_date_options = [
      '' => ts('- any -'),
    ] + \CRM_Core_OptionGroup::values('relative_date_filters');

    $civigroups = \Civi\Api4\Group::get()
      ->setCheckPermissions(FALSE)
      ->addSelect('title')
      ->addWhere('is_active', '=', TRUE)
      ->addOrderBy('title', 'ASC')
      ->execute()
      ->indexBy('id')
      ->column('title');

    $filters += [
      'activity_type_id' => [
        'type' => 'items',
        'label' => ts('Activity Type'),
        'items' => $atype,
        'depends' => [
          'activity',
        ],
      ],
      'activity_status_id' => [
        'type' => 'items',
        'label' => ts('Activity Status'),
        'items' => $astatus,
        'depends' => [
          'activity',
        ],
      ],
      'activity_subject' => [
        'type' => 'text',
        'label' => ts('Activity Subject'),
        'widgets' => [
          'like' => [
            'label' => ts('Activity Subject'),
            'type' => 'text',
          ],
        ],
        'depends' => [
          'activity',
        ],
      ],
    ];

    if ($hasCiviContribute) {
      $filters += [
        'ft' => [
          'type' => 'items',
          'label' => ts('Financial Type'),
          'items' => $ft,
          'depends' => [
            'contribution',
            'event',
            'membership',
            'participant',
          ],
        ],
        'contribution_page_id' => [
          'type' => 'items',
          'label' => ts('Contribution Page'),
          'items' => $contrib_pages,
          'depends' => [
            'contribution',
          ],
        ],
        'payment_instrument_id' => [
          'type' => 'items',
          'label' => ts('Payment Instrument'),
          'items' => $payment_instruments,
          'depends' => [
            'contribution',
          ],
        ],
        'payment_processor_id' => [
          'type' => 'items',
          'label' => ts('Payment Processor'),
          'items' => $payment_processors,
          'depends' => [
            'contribution',
          ],
        ],
        'contribution_status_id' => [
          'type' => 'items',
          'label' => ts('Contribution Status'),
          'items' => $contribution_statuses,
          'depends' => [
            'contribution',
          ],
        ],
      ];
    }

    $filters += [
      'membership_type_id' => [
        'type' => 'items',
        'label' => ts('Membership Type'),
        'items' => $mtype,
        'depends' => [
          'membership',
        ],
      ],
      'group_id' => [
        'type' => 'items',
        'label' => ts('Group'),
        'items' => $civigroups,
        'depends' => [
          'groupsubscription',
          'groupunsubscription',
        ],
      ],
      'gender_id' => [
        'type' => 'items',
        'label' => ts('Gender'),
        'items' => \CRM_Dataexplorer_PseudoConstant::getGenders(),
      ],
      'period' => [
        'type' => 'dates',
        'label' => ts('Date (fixed)'),
        'widgets' => [
          'start' => [
            'label' => ts('From'),
            'type' => 'date',
          ],
          'end' => [
            'label' => ts('To'),
            'type' => 'date',
          ],
        ],
      ],
      'relative_date' => [
        'type' => 'items',
        'label' => ts('Date (relative)'),
        'items' => $relative_date_options,
      ],
    ];

    // Groups
    $groups += [
      'region' => [
        'type' => 'items',
        'label' => E::ts('Geography'),
        'items' => [
          'country' => E::ts('By country'),
          'state_province' => E::ts('By state/province'),
          'city' => E::ts('By city'),
        ],
      ],
      'period' => [
        'label' => E::ts('Time'),
        'items' => [
          'year' => E::ts('By Year'),
          'month' => E::ts('By Month'),
          'week' => E::ts('By Week'),
          'day' => E::ts('By Day'),
          'hour' => E::ts('By Hour'),
        ],
      ],
      'contribution' => [
        'label' => E::ts('Contributions'),
        'items' => [
          'ft' => E::ts('By Financial Type'),
          'payment_instrument_id' => E::ts('By Payment Instrument'),
          'payment_processor_id' => E::ts('By Payment Processor'),
          'contribution_page_id' => E::ts('By Contribution Page'),
          'contribution_status_id' => E::ts('By Contribution Status'),
        ],
        'depends' => [
          'contribution',
        ],
      ],
      'participant' => [
        'label' => E::ts('Event Participant'),
        'items' => [
          'ft' => E::ts('By Financial Type'),
          'event' => E::ts('By Event'),
        ],
        'depends' => [
          'event',
        ],
      ],
      'other' => [
        'type' => 'items',
        'label' => E::ts('Other'),
        'items' => [
          'gender' => E::ts('By gender'),
          'contact' => 'By Contact', // spécifique à "punchs"?
          'case' => 'By Case', // spécifique à "punchs" // FIXME remove
          'task' => 'By Task', // spécifique à "punchs" // FIXME remove
        ],
      ],
    ];

    // Custom Fields (filters and groupbys)
    $customGroups = \Civi\Api4\CustomGroup::get()
      ->addWhere('is_active', '=', TRUE)
      ->addOrderBy('weight', 'ASC')
      ->execute();

    foreach ($customGroups as $customGroup) {
      $customFields = \Civi\Api4\CustomField::get()
        ->addWhere('is_active', '=', TRUE)
        ->addWhere('is_searchable', '=', TRUE)
        ->execute();

      foreach ($customFields as $customField) {
        // @todo Handle date fields, or anything other than a list
        if ($customField['option_group_id']) {
          $optionValues = \Civi\Api4\OptionValue::get()
            ->addWhere('option_group_id', '=', $customField['option_group_id'])
            ->addOrderBy('weight', 'ASC')
            ->execute()
            ->indexBy('value')
            ->column('label');

          $depends = strtolower($customGroup['extends']);

          // We don't really have measures that are specific to contact types?
          if ($depends == 'individual' || $depends == 'organization') {
            $depends = 'contact';
          }

          // Adding the Entity in here so that eventually we can validate faster
          // when executing the query (api4 will throw an error if the field is not valid).
          $key = $customGroup['extends'] . '.' . $customGroup['name'] . '.' . $customField['name'];

          $filters[$key] = [
            'type' => 'items',
            'label' => $customGroup['title'] . ' - ' . $customField['label'],
            'items' => $optionValues,
            'depends' => [
              $depends,
            ],
          ];

          $groups[$key] = [
            'type' => 'items',
            'label' => $customGroup['title'] . ' - ' . $customField['label'],
            // @todo Temporary until we fix how this is displayed
            // If we select the field in the select2 field, no need for "items"
            'items' => [
              0 => E::ts('No'),
              1 => E::ts('Yes'),
            ],
            'depends' => [
              $depends,
            ],
          ];
        }
      }
    }

    $event->setDataSources($sources);
    $event->setFilters($filters);
    $event->setGroupBy($groups);
  }

}

