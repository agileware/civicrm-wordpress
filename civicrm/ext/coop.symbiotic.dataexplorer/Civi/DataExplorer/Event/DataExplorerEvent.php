<?php

namespace Civi\DataExplorer\Event;

use Symfony\Component\EventDispatcher\Event;

/**
 * Class DataExplorerEvent
 * @package Civi\DataExplorer\Event
 */
class DataExplorerEvent extends Event {
  protected $sources;
  protected $filters;
  protected $group_bys;

  public const NAME = 'dataexplorer.boot';

  public function __construct() {
    $this->sources = [];
    $this->filters = [];
    $this->group_bys = [];
  }

  /**
   * @return array
   */
  public function getDataSources() {
    return $this->sources;
  }

  /**
   * @return array
   */
  public function getFilters() {
    return $this->filters;
  }

  /**
   * @return array
   */
  public function getGroupBy() {
    return $this->group_bys;
  }

  /**
   *
   */
  public function setDataSources(array $sources) {
    $this->sources = $sources;
  }

  /**
   *
   */
  public function setFilters(array $filters) {
    $this->filters = $filters;
  }

  /**
   *
   */
  public function setGroupBy(array $group_bys) {
    $this->group_bys = $group_bys;
  }

}
