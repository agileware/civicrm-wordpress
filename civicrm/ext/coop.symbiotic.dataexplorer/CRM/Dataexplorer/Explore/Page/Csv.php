<?php

class CRM_Dataexplorer_Explore_Page_Csv extends CRM_Core_Page {
  function run() {
    $measures = CRM_Utils_Request::retrieve('measure', 'String', $this, TRUE);
    $format = CRM_Utils_Request::retrieve('format', 'String', $this, TRUE);
    $download = CRM_Utils_Request::retrieve('download', 'Boolean', $this, FALSE);

    $headers = [];
    $data = [];
    $debug = [];

    $measures = explode(',', CRM_Utils_Request::retrieve('measure', 'String', $this, TRUE));

    foreach ($measures as $measure) {
      $class = $this->measureVariableToClass($measure);
      $generator = new $class();

      $data[] = $generator->data();
      $headers[] = $generator->config();
      $debug[] = implode('; ', $generator->getQueryLog());
    }

    // Currently only json is really tested
    // CSV and Excel are buggy and may be removed.
    if ($format == 'json') {
      $output = [];
      $output['data'] = $this->fixJsonData($data);
      $output['headers'] = $headers;

      if (CRM_Core_Permission::check('view report sql')) {
        $output['debug'] = $debug;
      }

      echo json_encode($output);
    }
    elseif ($format == 'csv') {
      // Convert to a multi-dimension table and normalize.
      $data1 = $this->normalizeDataCsv($headers, $data);

      header('Content-Type: text/csv');

      // always modified
      header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');

      if ($download) {
        header('Content-Disposition: attachment; filename="data.csv"');
      }

      $out = fopen('php://output', 'w');

      foreach ($data1 as $d) {
        fputcsv($out, $d);
      }
      fclose($out);
    }
    elseif ($format == 'excel2007') {
      // Convert to a multi-dimension table and normalize.
      $data1 = $this->normalizeDataCsv($headers, $data);

      header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');

      // always modified
      header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');

      if ($download) {
        header('Content-Disposition: attachment;filename="data.xlsx"');
      }

      $foo = [];
      $header_titles = $this->flattenHeadersTitles($headers);

      foreach ($header_titles as $h) {
        $foo[] = ['type' => CRM_Utils_Type::T_STRING];
      }

      $data_flat = $this->flattenData($data1);
      CRM_CiviExportExcel_Utils_SearchExport::makeExcel($header_titles, $foo, $data_flat);
    }

    CRM_Utils_System::civiExit();
  }

  /**
   *
   */
  function measureVariableToClass($measure) {
    $parts = explode('-', $measure);

    foreach ($parts as &$p) {
      $p = ucfirst($p);
    }

    // Duct tape. Might be better to lookup a mapping in getDataSources().
    if (count($parts) == 3) {
      $ext = array_shift($parts);
      $measure = implode('_', $parts);
      return 'CRM_' . $ext . '_Explore_Generator_' . $measure;
    }

    $measure = implode('_', $parts);
    return 'CRM_Dataexplorer_Explore_Generator_' . $measure;
  }

  /**
   * Merge multiple data sets into one.
   *
   * $data is an array of data, i.e. 0 = { dataset of first measure }, 1 => { data of second measure }
   * we want to return an (n x n) table for all datasets, assuming that their 'x' is the same.
   *
   * This code is buggy and should probably be removed.
   * CSV/Excel export is a nice to have, although the one
   * provided by the tabulator JS lib is good enough for now.
   */
  function normalizeDataCsv(&$headers, &$data) {
    $keys = array();
    $merged = array();

    /**
     * Merge datasets of different 'mesures' (usually 1 or 2 measures max).
     *
     * Ex 1: measures: "NB contributions", by campaign, by month:
     * $data = [
     *   0 = {
     *     2014-01 = {
     *        CampaignA = 111
     *        CampaignB = 222
     *     }
     *     2014-02 = {
     *        CampaignA = 333
     *        CampaignB = 444
     *     }
     *   }
     * ]
     *
     * Becomes:
     * | Year    | CampaignA | CampaignB |
     * | 2014-01 | 111       | 222       |
     * | 2014-02 | 333       | 444       |
     *
     * Ex 2: measures: "NB contributions", "NB orders", "by campaign".
     * $Data = [
     *   0 = {
     *     Spring = {
     *       Donations $ = 1234,00
     *     }
     *     Autumn = {
     *       Donations $ = 4321,00
     *     }
     *   }
     *   1 = {
     *     Spring = {
     *       Orders = 123
     *     }
     *     Autumn = {
     *       Orders = 234
     *     }
     *   }
     * ]
     *
     * Ex 3: measures: "NB orders", "Avg items"
     * $data = [
     *   0 = {
     *     Total = {
     *       Nb orders = 952
     *     }
     *   }
     *   1 = {
     *     Total = {
     *       Avg items = 4
     *     }
     *   }
     * ]
     */

    // For each row, Extract the column headers from each measure.
    // This is kind of redundant with the first row of labels, except we don't have the 'X' axis label.
    // From the 1st example above, it would result in:
    //   $keys = { 'CampaignA' => 'CampaignA', 'CampaignB' => 'CampaignB' }
    // From the 2nd example above, it would result in:
    //   $keys = { 'Donations', 'Orders' }
    foreach ($data as $measure) {
      foreach ($measure as $key => $val) {
        foreach ($val as $key2 => $val2) {
          $keys[$key2] = $key2;
        }
      }
    }

    // Merge headers labels
    $merged[0] = $this->flattenHeadersTitles($headers, $keys);
    $base_type_x = $this->getBaseTypeForX($headers);

    // For each result set, i.e. result from a measure/generator.
    // XXX why ksort? this breaks ordering of data/headers in some cases.
    // ksort($keys);

    foreach ($data as $measure) {
      foreach ($measure as $xval => $yvals) {
        foreach ($keys as $k) {
          $merged[$xval][$base_type_x] = $xval;
          $col = 1;

          ksort($yvals);

          // Keep in mind our keys might be that we have multiple yvals for one measure (with groupby),
          // or we have multiple yvals because we have multiple measures.
          // If merging two measures, the yval of one measure won't be available in the other measure.
          if (isset($yvals[$k])) {
            $merged[$xval][$k] = $yvals[$k];
          }
          else {
            // The cell might already have a value, if we are merging from two datasets/measures
            // but in some cases, the two sets might not overlap, so we should set to zero,
            // otherwise the CSV will look wonky.
            if (! isset($merged[$xval][$k])) {
              $merged[$xval][$k] = 0;
            }
          }
          $col++;
        }
      }
    }

    return $merged;
  }

  function normalizeHeadersJson(&$headers) {
    $base_type_x = '';
    $data = array(
      'all' => array(),
    );

    foreach ($headers as $d) {
      if (! $base_type_x) {
        // Axe X
        $base_type_x = $d['axis_x']['type'];
        $data['all'][] = array(
          'axis' => 'x',
          'type' => $d['axis_x']['type'],
          'label' => $d['axis_x']['label'],
        );
      }
      elseif ($base_type_x && $base_type_x != $d['axis_x']['type']) {
        CRM_Core_Error::fatal('Type mismatch for X: ' . $base_type_x . ' vs ' . $d['axis_x']['type']);
      }

      // Axes Y (there may be more than one)
      foreach ($d['axis_y'] as $y) {
        $data['all'][] = array(
          'axis' => 'y',
          'type' => $y['type'],
          'series' => $y['series'],
          'label' => $y['label'],
          'id' => $y['id'],
        );
      }
    }

    return $data;
  }

  /**
   * This groups the data so that sets with the same 'key' (usually x-axis)
   * are grouped together.
   *
   * It's a workaround until we completely get rid of CSV and then
   * maybe we can cleanup to something less clunky. The aim was to get
   * something that worked plug & play in tabulator.
   */
  function fixJsonData($series) {
    $keyed = [];

    foreach ($series as $key => $val) {
      $s = [];

      foreach ($val as $kk => $vv) {
        if (isset($keyed[$kk])) {
          $keyed[$kk] += $vv;
        }
        else {
          // This assumes that the first column is the row label
          $keyed[$kk] = ['id' => $kk] + $vv;
        }
      }
    }

    // Get rid of the keys so that we have a json array, not an object.
    $fixed = [];

    foreach ($keyed as $key => $val) {
      $fixed[] = $val;
    }

    return $fixed;
  }

  /**
   * Returns the 'type' of the 'X' axis, and makes sure that all
   * measures use the same X axis (unless there is a bug in a generator,
   * the x axis should always be the same).
   */
  function getBaseTypeForX($headers) {
    $base_type_x = '';

    foreach ($headers as $key => $val) {
      // We check to make sure that all 'x' axises are of the same type.
      if (! $base_type_x) {
        $base_type_x = $val['axis_x']['type'];
      }
      else {
        if ($base_type_x != $val['axis_x']['type']) {
          CRM_Core_Error::fatal('Type mismatch for X: ' . $base_type_x . ' vs ' . $val['axis_x']['type']);
        }
      }
    }

    return $base_type_x;
  }

  /**
   * Convert $headers to something usable for column headers.
   *
   * @param Array $headers Header column definitions.
   * @returns Array $labels Labels of column headers.
   */
  function flattenHeadersTitles($headers, $keys = []) {
    $labels = [];
    $label_x = '';

    // Each header $val contains a 'measure', with an axis_x/axis_y.
    if (!empty($keys)) {
      foreach ($headers as $key => $val) {
        // We should have only one 'x' axis, but can have multiple 'y'.
        // Assuming the axis is the same, since getBaseTypeForX() should
        // already have been called.
        if (!$label_x) {
          $label_x = $val['axis_x']['label'];
          $id = $val['axis_x']['type'];
          $labels[$id] = $val['axis_x']['label'];
        }

        // We need to produce the labels in the same order as the data.
        foreach ($keys as $k) {
          foreach ($val['axis_y'] as $key2 => $val2) {
            $id = $val2['id'];

            // This will depend on whether we have two or one "group by" (respectively).
            if ($id == $k || $val2['label'] == $k) {
              $labels[$id] = $val2['label'];
            }
          }
        }
      }
    }
    else {
      foreach ($headers as $key => $val) {
        // We should have only one 'x' axis, but can have multiple 'y'.
        // Assuming the axis is the same, since getBaseTypeForX() should
        // already have been called.
        if (! $label_x) {
          $label_x = $val['axis_x']['label'];
          $id = $val['axis_x']['type'];
          $labels[$id] = $val['axis_x']['label'];
        }

        foreach ($val['axis_y'] as $key2 => $val2) {
          $id = $val2['id'];
          $labels[$id] = $val2['label'];
        }
      }
    }

    return $labels;
  }

  /**
   *
   */
  function flattenData($data) {
    $ret = array();

    // skip first row with headers
    unset($data[0]);

    foreach ($data as $key => $val) {
      $ret[] = array_values($val);
    }

    return $ret;
  }
}
