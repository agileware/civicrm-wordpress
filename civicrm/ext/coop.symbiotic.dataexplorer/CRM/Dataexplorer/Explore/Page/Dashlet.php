<?php

use CRM_Dataexplorer_ExtensionUtil as E;

class CRM_Dataexplorer_Explore_Page_Dashlet extends CRM_Core_Page {

  public function run() {
    $id = CRM_Utils_Request::retrieveValue('id', 'Positive');
    $share_key = CRM_Utils_Request::retrieveValue('share', 'String');

    $data = CRM_Dataexplorer_BAO_Dataexplorer::get([
      'id' => $id,
      'share_key' => $share_key,
    ]);

    if (empty($data[0])) {
      $this->assign('dataexplorer_error', E::ts('Access denied or missing visualisation'));
      return parent::run();
    }

    $data = $data[0];

    $this->assign('dataexplorer_id', $id);
    $this->assign('dataexplorer_data', json_encode($data));

    $url = Civi::resources()->getUrl('dataexplorer');
    $this->assign('dataexplorer_baseurl', $url);

    return parent::run();
  }

}
