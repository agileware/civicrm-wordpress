<?php

use CRM_Dataexplorer_ExtensionUtil as E;

class CRM_Dataexplorer_Explore_Generator_Contact extends CRM_Dataexplorer_Explore_Generator {

  /**
   *
   */
  function config($options = []) {
    if ($this->_configDone) {
      return $this->_config;
    }

    $defaults = [
      'y_label' => E::ts('Contacts'),
      'y_series' => 'Contacts',
      'y_type' => 'number',
    ];

    $this->_from[] = "civicrm_contact as c ";

    $this->_options = array_merge($defaults, $options);

    // It helps to call this here as well, because some filters affect the groupby options.
    // FIXME: see if we can call it only once, i.e. remove from data(), but not very intensive, so not a big deal.
    $params = [];
    $this->whereClause($params);

    // We can only have 2 groupbys, otherwise would be too complicated
    switch (count($this->_groupBy)) {
      case 0:
        $this->_config['axis_x'] = array(
          'label' => E::ts('Total'),
          'type' => 'number',
        );
        $this->_config['axis_y'][] = array(
          'label' => $this->_options['y_label'],
          'type' => $this->_options['y_type'],
          'series' => $this->_options['y_series'],
          'id' => 1,
        );

        $this->_select[] = '"Total" as x';
        break;

      case 1:
      case 2:
        // Find all the labels for this type of group by
        if (in_array('period-year', $this->_groupBy)) {
          $this->configGroupByPeriodYear();
        }
        if (in_array('period-month', $this->_groupBy)) {
          $this->configGroupByPeriodMonth();
        }
        if (in_array('period-day', $this->_groupBy)) {
          $this->configGroupByPeriodDay();
        }
        if (in_array('region-country', $this->_groupBy)) {
          $this->configGroupByRegionCountry();
        }
        if (in_array('region-stateprovince', $this->_groupBy)) {
          $this->configGroupByRegionStateProvince();
        }
        if (in_array('region-city', $this->_groupBy)) {
          $this->configGroupByRegionCity();
        }
        if (in_array('other-campaign', $this->_groupBy)) {
          $this->configGroupByOtherCampaign();
        }
        if (in_array('other-gender', $this->_groupBy)) {
          $this->configGroupByOtherGender();
        }
        if (in_array('other-deleted', $this->_groupBy)) {
          $this->configGroupByOtherDeleted();
        }
        if (in_array('other-fiche', $this->_groupBy)) {
          $this->configGroupByOtherContactSubType();
        }

        break;

      default:
        CRM_Core_Error::fatal('Cannot groupby on ' . count($this->_groupBy) . ' elements. Max 2 allowed.');
    }

    // This happens if we groupby 'period' (month), but nothing else.
    if (empty($this->_config['axis_y'])) {
      $this->_config['axis_y'][] = array(
        'label' => $this->_options['y_label'],
        'type' => $this->_options['y_type'],
        'series' => $this->_options['y_series'],
        'id' => 1,
      );
    }

    $this->_configDone = TRUE;
    return $this->_config;
  }

  /**
   *
   */
  function data() {
    $data = [];
    $params = [];

    // This makes it easier to check specific exceptions later on.
    $this->config();

    if (in_array('period-year', $this->_groupBy)) {
      $this->queryAlterPeriod('year');
    }
    if (in_array('period-month', $this->_groupBy)) {
      $this->queryAlterPeriod('month');
    }
    if (in_array('period-day', $this->_groupBy)) {
      $this->queryAlterPeriod('day');
    }
    if (in_array('region-country', $this->_groupBy)) {
      $this->queryAlterRegionCountry();
    }
    if (in_array('region-stateprovince', $this->_groupBy)) {
      $this->queryAlterRegionStateProvince();
    }
    if (in_array('region-city', $this->_groupBy)) {
      $this->queryAlterRegionCity();
    }
    if (in_array('other-campaign', $this->_groupBy)) {
      $this->queryAlterOtherCampaign();
    }
    if (in_array('other-gender', $this->_groupBy)) {
      $this->queryAlterOtherGender();
    }
    if (in_array('other-deleted', $this->_groupBy)) {
      $this->queryAlterOtherDeleted();
    }

    $this->runQuery($where, $params, $data, NULL);

    // FIXME: if we don't have any results, and we are querying two
    // types of data, the 2nd column of results (CSV) might get bumped into
    // the first column. This really isn't ideal, should fix the CSV merger.
    if (empty($data)) {
      $tlabel = $this->_config['axis_x']['label'];
      $data[$tlabel][$ylabel] = 0;
    }

    return $data;
  }

  function whereClause(&$params) {
    $where_clauses = [];
    $where_extra = '';

    $this->whereClauseCommon($params);

    $membership_types = [];

    // @todo Why commented out?
    # $where_clauses[] = 'is_deleted <> 0';

    foreach ($this->_filters as $filter) {
      // foo[0] will have 'period-start' and foo[1] will have 2014-09-01
      $foo = explode(':', $filter);

      // bar[0] will have 'period' and bar[1] will have 'start'
      // bar[0] will have 'diocese' and bar[1] will have '1'
      $bar = explode('-', $foo[0]);

      if ($bar[0] == 'period') {
        // Transform to MySQL date: remove the dashes in the date (2014-09-01 -> 20140901).
        $foo[1] = str_replace('-', '', $foo[1]);

        if ($bar[1] == 'start' && ! empty($foo[1])) {
          $params[1] = array($foo[1], 'Timestamp');
          $where_clauses[] = 'created_date >= %1';
        }
        elseif ($bar[1] == 'end' && ! empty($foo[1])) {
          $params[2] = array($foo[1] . '235959', 'Timestamp');
          $where_clauses[] = 'created_date <= %2';
        }
      }
      elseif ($bar[0] == 'relative_date') {
        $dates = CRM_Utils_Date::getFromTo($bar[1], NULL, NULL);

        $params[1] = array($dates[0], 'Timestamp');
        $where_clauses[] = 'created_date >= %1';

        $params[2] = array($dates[1], 'Timestamp');
        $where_clauses[] = 'created_date <= %2';
      }
      elseif ($bar[0] == 'membership_type_id' && !empty($bar[1])) {
        $membership_types[] = $bar[1];
      }
    }

    // @todo ugh..
    if (! empty($this->_config['filters']['arrondissements'])) {
      $where_clauses[] = 'arrondissement__militant_code_54 IN (' . implode(',', $this->_config['filters']['arrondissements']) . ')';
    }

    if (! empty($this->_config['filters']['districts'])) {
      $where_clauses[] = 'district__code_55 IN (' . implode(',', $this->_config['filters']['districts']) . ')';
    }

    if (! empty($this->_config['filters']['campaigns'])) {
      $where_clauses[] = 'campaign IN (' . implode(',', $this->_config['filters']['campaigns']) . ')';
    }

    if (! empty($this->_config['filters']['provinces'])) {
      $where_clauses[] = 'province_id IN (' . implode(',', $this->_config['filters']['provinces']) . ')';
    }

    if (! empty($this->_config['filters']['admregions'])) {
      $where_clauses[] = 'dio.region_68 IN (' . implode(',', $this->_config['filters']['admregions']) . ')';
    }

    if (! empty($this->_config['filters']['gender'])) {
      $where_clauses[] = 'gender_id IN (' . implode(',', $this->_config['filters']['gender']) . ')';
    }

    if (!empty($membership_types)) {
      // @todo Add only active memberships by default? or separate filter?
      $this->_from[] = 'INNER JOIN civicrm_membership ON (civicrm_membership.contact_id = c.id AND civicrm_membership.membership_type_id IN (' . implode(',', $membership_types) . '))';
      $where_clauses[] = 'membership_type_id IN (' . implode(',', $membership_types) . ')';
    }

    $where = implode(' AND ', $where_clauses);
    $where = trim($where);

    return $where;
  }

  function configGroupByPeriodYear() {
    // Assume that if we are grouping by year, it's always a line chart.
    // that's why we check for the period groupby first.
    $this->_config['axis_x'] = array(
      'label' => E::ts('Year'),
      'type' => 'date',
    );

    $this->_select[] = "DATE_FORMAT(created_date, '%Y') as x";
  }

  function configGroupByPeriodMonth() {
    // Assume that if we are grouping by month, it's always a line chart.
    // that's why we check for the period groupby first.
    $this->_config['axis_x'] = array(
      'label' => E::ts('Month'),
      'type' => 'date',
    );

    $this->_select[] = "DATE_FORMAT(created_date, '%Y-%m') as x";
  }

  function configGroupByPeriodDay() {
    // Assume that if we are grouping by month, it's always a line chart.
    // that's why we check for the period groupby first.
    $this->_config['axis_x'] = array(
      'label' => E::ts('Day'),
      'type' => 'date',
    );

    $this->_select[] = "DATE_FORMAT(created_date, '%Y-%m-%d') as x";
  }

  /**
   * @param String $type = { day, month, year }
   */
  function queryAlterPeriod($type) {
    // NB: date itself has already been put in the select[] by config().
    switch ($type) {
      case 'year':
        $this->_group[] = "DATE_FORMAT(created_date, '%Y')";
        break;
      case 'month':
        $this->_group[] = "DATE_FORMAT(created_date, '%Y-%m')";
        break;
      case 'day':
        $this->_group[] = "DATE_FORMAT(created_date, '%Y-%m-%d')";
        break;
      default:
        CRM_Core_Error::fatal('Unknown type of period');
    }
  }

  /**
   * Group by is_deleted (Menu: Afficher -> Autres -> Deleted).
   */
  function configGroupByOtherDeleted() {
    $is_deleted = array(
      0 => 'régulier',
      1 => 'supprimé',
    );

    if (empty($this->_config['axis_x'])) {
      // If X is empty, it's a bar chart.
      $this->_config['axis_x'] = array(
        'label' => 'Supprimé',
        'type' => 'number',
      );

      $this->_config['axis_y'][] = array(
        'label' => $this->_options['y_label'],
        'type' => $this->_options['y_type'],
        'series' => $this->_options['y_series'],
        'id' => 1,
      );

      $this->_config['x_translate'] = $is_deleted;
      $this->_select[] = 'is_deleted as x';
      return;
    }

    foreach ($is_deleted as $key => $val) {
      if (empty($this->_config['filters']['deleted']) || in_array($key, $this->_config['filters']['deleted'])) {
        $this->_config['axis_y'][] = array(
          'label' => $val,
          'type' => $this->_options['y_type'],
          'series' => $this->_options['y_series'],
          'id' => $key,
        );
      }
    }

    $this->_select[] = 'is_deleted as yy';
  }

  function queryAlterOtherDeleted() {
    $this->_group[] = 'is_deleted';
  }
}
