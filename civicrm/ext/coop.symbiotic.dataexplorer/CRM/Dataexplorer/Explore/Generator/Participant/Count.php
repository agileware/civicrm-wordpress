<?php

use CRM_Dataexplorer_ExtensionUtil as E;

class CRM_Dataexplorer_Explore_Generator_Participant_Count extends CRM_Dataexplorer_Explore_Generator_Participant {

  function config($options = []) {
    $options['y_label'] = 'Participants';
    $options['y_series'] = 'Count';
    $options['y_type'] = 'number';

    return parent::config($options);
  }

  function data() {
    $this->_select[] = "count(*) as y";

    return parent::data();
  }
}
