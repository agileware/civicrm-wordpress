<?php

use CRM_Dataexplorer_ExtensionUtil as E;

class CRM_Dataexplorer_Explore_Generator_Membership extends CRM_Dataexplorer_Explore_Generator {
  protected $_grouping_ignore_dates = FALSE;

  function config($options = []) {
    if ($this->_configDone) {
      return $this->_config;
    }

    $defaults = [
      'y_label' => E::ts('New Members'),
      'y_series' => 'Membres',
      'y_type' => 'number',
    ];

    $this->_options = array_merge($defaults, $options);

    // It helps to call this here as well, because some filters affect the groupby options.
    // FIXME: see if we can call it only once, i.e. remove from data(), but not very intensive, so not a big deal.
    $params = [];
    $this->whereClause($params);

    // We can only have 2 groupbys, otherwise would be too complicated
    switch (count($this->_groupBy)) {
      case 0:
        $this->_config['axis_x'] = array(
          'label' => E::ts('Total'),
          'type' => 'number',
        );
        $this->_config['axis_y'][] = array(
          'label' => $this->_options['y_label'],
          'type' => $this->_options['y_type'],
          'series' => $this->_options['y_series'],
        );

        $this->_select[] = '"Total" as x';
        break;

      case 1:
      case 2:
        // Find all the labels for this type of group by
        if (in_array('period-year', $this->_groupBy)) {
          $this->configGroupByPeriodYear();
        }
        if (in_array('period-month', $this->_groupBy)) {
          $this->configGroupByPeriodMonth();
        }
        if (in_array('period-day', $this->_groupBy)) {
          $this->configGroupByPeriodDay();
        }
        if (in_array('other-campaign', $this->_groupBy)) {
          $this->configGroupByOtherCampaign();
        }
        if (in_array('other-gender', $this->_groupBy)) {
          $this->configGroupByOtherGender();
        }
        if (in_array('other-fiche', $this->_groupBy)) {
          $this->configGroupByOtherContactSubType();
        }

        break;

      default:
        CRM_Core_Error::fatal('Cannot groupby on ' . count($this->_groupBy) . ' elements. Max 2 allowed.');
    }

    // This happens if we groupby 'period' (month), but nothing else.
    if (empty($this->_config['axis_y'])) {
      $this->_config['axis_y'][] = array(
        'label' => $this->_options['y_label'],
        'type' => $this->_options['y_type'],
        'series' => $this->_options['y_series'],
        'id' => 3,
      );
    }

    $this->_configDone = TRUE;
    return $this->_config;
  }

  function data() {
    $data = [];
    $params = [];

    // This makes it easier to check specific exceptions later on.
    $this->config();

    // Get the number of members
    $this->_select[] = "count(*) as y";

    $this->_from[] = "civicrm_membership as memb
                      LEFT JOIN civicrm_membership_type mt ON (mt.id = memb.membership_type_id)
                      LEFT JOIN civicrm_contact as c ON (c.id = memb.contact_id) ";
 
    if (in_array('region-arrondissement', $this->_groupBy)) {
      $this->queryAlterRegionArrondissement();
    }
    if (in_array('other-campaign', $this->_groupBy)) {
      $this->queryAlterOtherCampaign();
    }
    if (in_array('other-gender', $this->_groupBy)) {
      $this->queryAlterOtherGender();
    }

    // Generate an array with the number of periods that we want.
    $periods = [];

    if (in_array('period-year', $this->_groupBy)) {
      $this->queryAlterPeriod('year');
    }
    if (in_array('period-month', $this->_groupBy)) {
      $this->queryAlterPeriod('month');
    }
    if (in_array('period-day', $this->_groupBy)) {
      $this->queryAlterPeriod('day');
    }

    // FIXME: This initially tried to calculate active members in a given period
    // but there is a mixup and we should query that against the civicrm_membership_log table
    // So I'm renaming this "New Members" and we will calculate "active members" in a different 'generator'.
    // Also, this should run based on the years returned by the group-by, not the filters, because
    // the user may have not selected any years.
    if (FALSE && $periods) {
      $this->_grouping_ignore_dates = TRUE;
      $where = $this->whereClause($params);

      foreach ($periods as $p) {
        // membership.join_date <= (filter date end)
        // membership.end_date >= (filter date start)
        $where_tmp = $where . ' AND join_date <= ' . $p['end'] . ' AND end_date >= ' . $p['start'];

        // The 'x' passed is used to ensure the correct X-avis label, ex: 2015, not 2015-01-01.
        $this->runQuery($where_tmp, $params, $data, $p['x']);
      }
    }
    else {
      $where = $this->whereClause($params);
      $this->runQuery($where, $params, $data, NULL);
    }

    return $data;
  }

  function whereClause(&$params) {
    $where_clauses = [];
    $where_extra = '';

    $this->whereClauseCommon($params);

    $where_clauses[] = 'is_test = 0';

    $financial_types = [];
    $membership_types = [];

    foreach ($this->_filters as $filter) {
      // foo[0] will have 'period-start' and foo[1] will have 2014-09-01
      $foo = explode(':', $filter);

      // bar[0] will have 'period' and bar[1] will have 'start'
      // bar[0] will have 'diocese' and bar[1] will have '1'
      $bar = explode('-', $foo[0]);

      if ($bar[0] == 'period' && ! $this->_grouping_ignore_dates) {
        // Transform to MySQL date: remove the dashes in the date (2014-09-01 -> 20140901).
        // NB: to find a valid membership in a given period:
        // membership.join_date <= (filter date end)
        // membership.end_date >= (filter date start)
        //
        // However, if we are searching for new memberships only, then:
        // memberships.join_date >= (filter date start)
        // memberships.join_date <= (filter date end)
        //
        // and if we want to exclude new members (kind of silly?)
        // membership.join_date <= (filter date end) AND membership.join_date <= (filter date start)
        // membership.end_date >= (filter date start)
        // FIXME: Not relevant in this 'generator'
        if (in_array('memberships-new:1', $this->_filters)) {
          // Only new members
          $foo[1] = str_replace('-', '', $foo[1]);

          if ($bar[1] == 'start' && ! empty($foo[1])) {
            $params[1] = array($foo[1], 'Timestamp');
            $where_clauses[] = 'join_date >= %1';
          }
          elseif ($bar[1] == 'end' && ! empty($foo[1])) {
            $params[2] = array($foo[1] . '235959', 'Timestamp');
            $where_clauses[] = 'join_date <= %2';
          }
        }
        elseif (in_array('memberships-new:2', $this->_filters)) {
          // Exclude new members
          $foo[1] = str_replace('-', '', $foo[1]);

          if ($bar[1] == 'start' && ! empty($foo[1])) {
            $params[1] = array($foo[1], 'Timestamp');
            $where_clauses[] = 'end_date >= %1';
          }
          elseif ($bar[1] == 'end' && ! empty($foo[1])) {
            $params[2] = array($foo[1] . '235959', 'Timestamp');
            $where_clauses[] = 'join_date <= %2 AND join_date <= %1';
          }
        }
        elseif (!empty($bar[1]) && !empty($foo[1]) && $foo[1] !== 'null') {
          $foo[1] = str_replace('-', '', $foo[1]);

          if ($bar[1] == 'start') {
            $params[1] = array($foo[1], 'Timestamp');
            $where_clauses[] = 'join_date >= %1';
          }
          elseif ($bar[1] == 'end') {
            $params[2] = array($foo[1] . '235959', 'Timestamp');
            $where_clauses[] = 'join_date <= %2';
          }
        }
      }
      elseif ($bar[0] == 'relative_date' && !$this->_grouping_ignore_dates) {
        // @todo Not particularly tested
        $dates = CRM_Utils_Date::getFromTo($bar[1], NULL, NULL);

        $params[1] = array($dates[0], 'Timestamp');
        $where_clauses[] = 'join_date >= %1';

        $params[2] = array($dates[1], 'Timestamp');
        $where_clauses[] = 'join_date <= %2';
      }
      elseif ($bar[0] == 'ft' && !empty($bar[1])) {
        $financial_types[] = $bar[1];
      }
      elseif ($bar[0] == 'membership_type_id' && !empty($bar[1])) {
        $membership_types[] = $bar[1];
      }
    }

    if (!empty($financial_types)) {
      $where_clauses[] = 'financial_type_id IN (' . implode(',', $financial_types) . ')';
    }

    if (!empty($membership_types)) {
      $where_clauses[] = 'membership_type_id IN (' . implode(',', $membership_types) . ')';
    }

    if (! empty($this->_config['filters']['arrondissements'])) {
      $where_clauses[] = 'arrondissement__militant_code_54 IN (' . implode(',', $this->_config['filters']['arrondissements']) . ')';
    }

    if (! empty($this->_config['filters']['districts'])) {
      $where_clauses[] = 'district__code_55 IN (' . implode(',', $this->_config['filters']['districts']) . ')';
    }

    if (! empty($this->_config['filters']['campaigns'])) {
      $where_clauses[] = 'campaign IN (' . implode(',', $this->_config['filters']['campaigns']) . ')';
    }

    if (! empty($this->_config['filters']['contact_sub_type'])) {
      $where_clauses[] = 'contact_sub_type IN (' . implode(',', $this->_config['filters']['contact_sub_type']) . ')';
    }

    if (! empty($this->_config['filters']['gender'])) {
      $where_clauses[] = 'gender IN (' . implode(',', $this->_config['filters']['gender']) . ')';
    }

    $where = implode(' AND ', $where_clauses);
    $where = trim($where);

    return $where;
  }

  function configGroupByPeriodYear() {
    // Assume that if we are grouping by month, it's always a line chart.
    // that's why we check for the period groupby first.
    $this->_config['axis_x'] = array(
      'label' => E::ts('Year'),
      'type' => 'date',
    );

    $this->_select[] = "DATE_FORMAT(join_date, '%Y') as x";
  }

  function configGroupByPeriodMonth() {
    // Assume that if we are grouping by month, it's always a line chart.
    // that's why we check for the period groupby first.
    $this->_config['axis_x'] = array(
      'label' => E::ts('Month'),
      'type' => 'date',
    );

    $this->_select[] = "DATE_FORMAT(join_date, '%Y-%m') as x";
  }

  function configGroupByPeriodDay() {
    // Assume that if we are grouping by month, it's always a line chart.
    // that's why we check for the period groupby first.
    $this->_config['axis_x'] = array(
      'label' => E::ts('Day'),
      'type' => 'date',
    );

    $this->_select[] = "DATE_FORMAT(join_date, '%Y-%m-%d') as x";
  }

  /**
   * @param String $type = { day, month, year }
   */
  function queryAlterPeriod($type) {
    // NB: date itself has already been put in the select[] by config().
    switch ($type) {
      case 'year':
        $this->_group[] = "DATE_FORMAT(join_date, '%Y')";
        break;
      case 'month':
        $this->_group[] = "DATE_FORMAT(join_date, '%Y-%m')";
        break;
      case 'day':
        $this->_group[] = "DATE_FORMAT(join_date, '%Y-%m-%d')";
        break;
      default:
        CRM_Core_Error::fatal('Unknown type of period');
    }
  }
}
