<?php

use CRM_Dataexplorer_ExtensionUtil as E;

trait CRM_Dataexplorer_Explore_Generator_FinancialTrait {

  function configGroupByContributionsFt() {
    $ft = civicrm_api3('Contribution', 'getoptions', [
      'field' => 'financial_type_id',
    ])['values'];

    $this->_select[] = "financial_type_id";

    $this->addAxis([
      'label' => E::ts('Financial Type'),
      'type' => $this->_options['y_type'],
      'series' => $this->_options['y_series'],
      'id' => 'ft',
      'translate' => $ft,
    ]);
  }

  function queryAlterContributionsFt() {
    $this->_group[] = "financial_type_id";
  }

  function configGroupByContributionsPaymentInstrumentId() {
    $values = civicrm_api3('Contribution', 'getoptions', [
      'field' => 'payment_instrument_id',
    ])['values'];

    $this->_select[] = "payment_instrument_id";

    $this->addAxis([
      'label' => E::ts('Payment Instrument'),
      'type' => $this->_options['y_type'],
      'series' => $this->_options['y_series'],
      'id' => 'payment_instrument_id',
      'translate' => $values,
    ]);
  }

  function queryAlterContributionsPaymentInstrumentId() {
    $this->_group[] = "payment_instrument_id";
  }

  function configGroupByContributionsPaymentProcessorId() {
    $this->add_where_trxn = TRUE;

    // Only display the processors that we are showing to the user
    // ex: do not display test processors
    $this->_config['x_translate_strict'] = TRUE;

    $values = CRM_Dataexplorer_PseudoConstant::getPaymentProcessors();

    $this->_select[] = "payment_processor_id";

    $this->addAxis([
      'label' => E::ts('Payment Processor'),
      'type' => $this->_options['y_type'],
      'series' => $this->_options['y_series'],
      'id' => 'payment_processor_id',
      'translate' => $values,
    ]);
  }

  function queryAlterGroupContributionsPaymentProcessorId() {
    $this->_group[] = "payment_processor_id";
    $this->queryAlterJoinContributionsPaymentProcessorId();
  }

  function queryAlterJoinContributionsPaymentProcessorId() {
    static $run_once = false;

    if ($run_once) {
      return;
    }

    $this->_from[] = "
      LEFT JOIN civicrm_entity_financial_trxn etrx ON (etrx.entity_table = 'civicrm_contribution' AND etrx.entity_id = contrib.id)
      LEFT JOIN civicrm_financial_trxn trx ON (trx.id = etrx.financial_trxn_id)";

    $run_once = true;
  }

  function queryAlterGroupContributionsStatusId() {
    $this->_group[] = "contribution_status_id";
  }

  function configGroupByContributionsStatusId() {
    // Only display the statuses that we are showing to the user
    // $this->_config['x_translate_strict'] = TRUE;

    $values = \Civi\Api4\OptionValue::get(false)
      ->setSelect(['label', 'value'])
      ->addWhere('option_group_id:name', '=', 'contribution_status')
      ->addOrderBy('weight', 'ASC')
      ->execute()
      ->indexBy('value')
      ->column('label');

    $this->_select[] = "contribution_status_id";

    $this->addAxis([
      'label' => E::ts('Contribution Status'),
      'type' => $this->_options['y_type'],
      'series' => $this->_options['y_series'],
      'id' => 'contribution_status_id',
      'translate' => $values,
    ]);
  }

  function queryAlterGroupContributionsPageId() {
    $this->_group[] = "contribution_page_id";
  }

  function configGroupByContributionsPageId() {
    // Only display the statuses that we are showing to the user
    $this->_config['x_translate_strict'] = TRUE;

    $values = \Civi\Api4\ContributionPage::get(false)
      ->setSelect(['title', 'id'])
      ->addWhere('is_active', '=', 1)
      ->addOrderBy('title', 'ASC')
      ->execute()
      ->indexBy('id')
      ->column('title');

    $this->_select[] = "contribution_page_id";

    $this->addAxis([
      'label' => E::ts('Contribution Page'),
      'type' => $this->_options['y_type'],
      'series' => $this->_options['y_series'],
      'id' => 'contribution_page_id',
      'translate' => $values,
    ]);
  }

}
