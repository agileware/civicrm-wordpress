<?php

use CRM_Dataexplorer_ExtensionUtil as E;

class CRM_Dataexplorer_Explore_Generator_Groupunsubscription_Count extends CRM_Dataexplorer_Explore_Generator_Groupunsubscription {

  /**
   *
   */
  function config($options = []) {
    $options['y_label'] = E::ts('Unsubscriptions');
    $options['y_series'] = 'Unsubscriptions';
    $options['y_type'] = 'number';

    $this->_select[] = "count(*) as y";

    return parent::config($options);
  }

}
