<?php

class CRM_Dataexplorer_Explore_Generator_Contribution_Sum extends CRM_Dataexplorer_Explore_Generator_Contribution {

  function data() {
    $this->_select[] = "sum(contrib.total_amount) as y";
    return parent::data();
  }

}
