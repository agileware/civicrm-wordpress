<?php

class CRM_Dataexplorer_Explore_Generator_Contribution_Max extends CRM_Dataexplorer_Explore_Generator_Contribution {
  function __construct() {
    parent::__construct();
  }

  function data() {
    $this->_select[] = "max(amount) as y";

    return parent::data();
  }
}
