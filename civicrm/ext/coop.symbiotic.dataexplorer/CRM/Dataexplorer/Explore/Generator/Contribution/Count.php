<?php

class CRM_Dataexplorer_Explore_Generator_Contribution_Count extends CRM_Dataexplorer_Explore_Generator_Contribution {

  function config($options = []) {
    $options['y_label'] = 'Contributions';
    $options['y_series'] = 'Count';
    $options['y_type'] = 'number';

    return parent::config($options);
  }

  function data() {
    $this->_select[] = "count(*) as y";
    return parent::data();
  }

}
