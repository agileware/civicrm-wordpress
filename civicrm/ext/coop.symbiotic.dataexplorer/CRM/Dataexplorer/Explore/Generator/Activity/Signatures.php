<?php

class CRM_Dataexplorer_Explore_Generator_Activity_Signature extends CRM_Dataexplorer_Explore_Generator_Activity {
  function __construct() {
    parent::__construct();
  }

  function data() {
    $this->_select[] = "sum(number_of_petition_signatures_66) as y";

    return parent::data();
  }
}
