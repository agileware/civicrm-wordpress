<?php

use CRM_Dataexplorer_ExtensionUtil as E;

trait CRM_Dataexplorer_Explore_Generator_CustomFieldTrait {

  function extractCustomFields($entity, $values) {
    // Ex: "[Entity].[Group].[Field']-1"
    $fields = [];

    foreach ($values as $v) {
      $parts = explode('-', $v);
      $subparts = explode('.', $parts[0]);

      if (count($subparts) == 3 && $subparts[0] == $entity) {
        $fields[$parts[0]] = $parts[1];
      }
    }

    return $fields;
  }

  function joinCustomGroupTable($table) {
    static $join_once = [];

    if (empty($join_once[$table])) {
      // @todo Hardcoded table (a.id)
      $this->_from[] = "LEFT JOIN {$table} ON ({$table}.entity_id = a.id)";
      $join_once[$table] = 1;
    }
  }

  function whereClauseCustomFields($entity, &$params, &$where_clauses) {
    $fields = $this->extractCustomFields($entity, $this->_filters);

    if (empty($fields)) {
      return;
    }

    // @todo Lazyness, the param counter could be on $this
    $counter = 1000;

    foreach ($fields as $key => $value) {
      $parts = explode('.', $key);

      $customField = \Civi\Api4\CustomField::get()
        ->addWhere('name', '=', $parts[2])
        ->addWhere('custom_group_id:name', '=', $parts[1])
        ->execute()
        ->first();

      $params[$counter] = [$value, 'String'];
      $where_clauses[] = $customField['column_name'] . ' = %' . $counter;

      // Need to join the table
      $customGroup = \Civi\Api4\CustomGroup::get()
        ->addWhere('name', '=', $parts[1])
        ->execute()
        ->first();

      $this->joinCustomGroupTable($customGroup['table_name']);
    }
  }

  function configGroupByCustomField($entity) {
    // Check if there was is CustomField GroupBy
    // Ex: "[Entity].[Group].[Field']-1"
    $group_fields = $this->extractCustomFields($entity, $this->_groupBy);

    // We don't need the values
    $group_fields = array_keys($group_fields);

    if (empty($group_fields)) {
      return;
    }

    if (count($group_fields) > 1) {
      throw new Exception('Only one display (groupby) option is supported.');
    }

    $cf_field = $group_fields[0];
    $parts = explode('.', $cf_field);

    // Fetch the label
    $customField = \Civi\Api4\CustomField::get()
      ->addWhere('name', '=', $parts[2])
      ->addWhere('custom_group_id:name', '=', $parts[1])
      ->execute()
      ->first();

    $this->_config['axis_x'] = [
      'label' => $customField['label'],
      'type' => $this->_options['y_type'],
      'series' => $this->_options['y_series'],
      'id' => 'cf' . $customField['id'],
    ];

    // @todo Convert to api4
    $options = civicrm_api3($entity, 'getoptions', [
      'field' => 'custom_' . $customField['id'],
    ])['values'];

    $this->_config['x_translate'] = $options;

    $this->_select[] = $customField['column_name'] . " as x";

    $customGroup = \Civi\Api4\CustomGroup::get()
      ->addWhere('name', '=', $parts[1])
      ->execute()
      ->first();

    $this->_group[] = $customField['column_name'];
    $this->joinCustomGroupTable($customGroup['table_name']);
  }

}
