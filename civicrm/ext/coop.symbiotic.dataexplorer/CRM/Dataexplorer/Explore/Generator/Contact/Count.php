<?php

class CRM_Dataexplorer_Explore_Generator_Contact_Count extends CRM_Dataexplorer_Explore_Generator_Contact {

  function config($options = []) {
    $options['y_label'] = 'Contacts';
    $options['y_series'] = 'Count';
    $options['y_type'] = 'number';

    $this->_select[] = "count(*) as y";
    return parent::config($options);
  }

}
