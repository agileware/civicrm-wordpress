<?php

class CRM_Dataexplorer_Explore_Generator_Memberships_Monthly extends CRM_Dataexplorer_Explore_Generator_Memberships {
  function __construct() {
    parent::__construct();
  }

  function config($options = array()) {
    $options['y_label'] = 'Membres donateurs mensuels';
    $options['y_series'] = 'Membres';
    $options['y_type'] = 'number';

    return parent::config($options);
  }

  function data() {
    return parent::data();
  }

  function whereClause(&$params) {
    $where = parent::whereClause($params);

    if ($where) {
      $where .= ' AND ';
    }

    $where .= 'indicator_id = 270';
    return $where;
  }
}
