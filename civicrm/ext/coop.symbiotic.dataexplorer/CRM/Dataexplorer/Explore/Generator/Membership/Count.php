<?php

use CRM_Dataexplorer_ExtensionUtil as E;

class CRM_Dataexplorer_Explore_Generator_Membership_Count extends CRM_Dataexplorer_Explore_Generator_Membership {

  /**
   *
   */
  function config($options = []) {
    $options['y_label'] = E::ts('New Members');
    $options['y_series'] = 'Membres';
    $options['y_type'] = 'number';

    return parent::config($options);
  }

}
