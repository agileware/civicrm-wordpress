<?php

use CRM_Dataexplorer_ExtensionUtil as E;

class CRM_Dataexplorer_Explore_Generator_Mailingrecipient_Count extends CRM_Dataexplorer_Explore_Generator_Mailingrecipient {

  /**
   *
   */
  function config($options = []) {
    $options['y_label'] = E::ts('Mailing Recipients');
    $options['y_series'] = 'Recipients';
    $options['y_type'] = 'number';

    $this->_select[] = "count(*) as y";

    return parent::config($options);
  }

}
