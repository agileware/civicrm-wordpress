<?php

use CRM_Dataexplorer_ExtensionUtil as E;

trait CRM_Dataexplorer_Explore_Generator_DateTrait {

  /**
   * @param String $type = { hour, day, month, year }
   */
  function configGroupByPeriod($type, $field) {
    $map_axis_label = [
      'year' => E::ts('Year'),
      'month' => E::ts('Month'),
      'week' => E::ts('Week'),
      'day' => E::ts('Day'),
      'hour' => E::ts('Hour'),
    ];

    $this->addAxis([
      'label' => $map_axis_label[$type] ?? 'invalid configGroupByPeriod label',
      'type' => 'date',
    ]);

    $map_axis_format = [
      'year' => "DATE_FORMAT($field, '%Y')",
      'month' => "DATE_FORMAT($field, '%Y-%m')",
      'week' => "YEARWEEK($field)",
      'day' => "DATE_FORMAT($field, '%Y-%m-%d')",
      'hour' => "DATE_FORMAT($field, '%Y-%m-%d %H')",
    ];

    $this->_select[] = $map_axis_format[$type] ?? 'invalid configGroupByPeriod format';
  }

  /**
   * @deprecated
   */
  function configGroupByPeriodYear($field) {
    $this->configGroupByPeriod('year', $field);
  }
  function configGroupByPeriodMonth($field) {
    $this->configGroupByPeriod('month', $field);
  }
  function configGroupByPeriodWeek($field) {
    $this->configGroupByPeriod('week', $field);
  }
  function configGroupByPeriodDay($field) {
    $this->configGroupByPeriod('day', $field);
  }
  function configGroupByPeriodHour($field) {
    $this->configGroupByPeriod('hour', $field);
  }

  /**
   * @param String $type = { hour, day, month, year }
   */
  function queryAlterPeriod($type, $field) {
    // NB: date itself has already been put in the select[] by config().
    switch ($type) {
      case 'year':
        $this->_group[] = "DATE_FORMAT($field, '%Y')";
        $this->_order[] = "DATE_FORMAT($field, '%Y') ASC";
        break;
      case 'month':
        $this->_group[] = "DATE_FORMAT($field, '%Y-%m')";
        $this->_order[] = "DATE_FORMAT($field, '%m') ASC";
        break;
      case 'week':
        $this->_group[] = "YEARWEEK($field)";
        $this->_order[] = "YEARWEEK($field) ASC";
        break;
      case 'day':
        $this->_group[] = "DATE_FORMAT($field, '%Y-%m-%d')";
        $this->_order[] = "DATE_FORMAT($field, '%m-%d') ASC";
        break;
      case 'hour':
        $this->_group[] = "DATE_FORMAT($field, '%Y-%m-%d %H')";
        $this->_order[] = "DATE_FORMAT($field, '%m-%d %H') ASC";
        break;
      default:
        throw new Exception('Unknown type of period: ' . $type);
    }
  }

}
