<?php

use CRM_Dataexplorer_ExtensionUtil as E;

class CRM_Dataexplorer_Explore_Generator_Groupsubscription_Count extends CRM_Dataexplorer_Explore_Generator_Groupsubscription {

  /**
   *
   */
  function config($options = []) {
    $options['y_label'] = E::ts('Subscriptions');
    $options['y_series'] = 'Subscriptions';
    $options['y_type'] = 'number';

    $this->_select[] = "count(*) as y";

    return parent::config($options);
  }

}
