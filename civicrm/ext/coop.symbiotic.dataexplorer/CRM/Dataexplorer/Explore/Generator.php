<?php

use CRM_Dataexplorer_ExtensionUtil as E;

class CRM_Dataexplorer_Explore_Generator {
  protected $_config;
  protected $_configDone;
  protected $_extraSelect;
  protected $_groupBy;

  protected $_select;
  protected $_from;
  protected $_where;
  protected $_group;
  protected $_order;

  protected $_options;
  protected $_sqlLog;

  function __construct() {
    $this->_extraSelect = [];
    $this->_groupBy = [];
    $this->_filters = [];
    $this->_options = [];
    $this->_sqlLog = [];

    $this->_config = array(
      'axis_x' => NULL,
      'axis_y' => [],
      'filters' => [],
    );

    $this->_configDone = FALSE;

    // Clauses for the generated SQL query.
    $this->_select = [];
    $this->_from = [];
    $this->_where = [];
    $this->_group = [];
    $this->_order = [];

    $this->parseFilters();
    $this->parseGroupBy();
  }

  /**
   * The first axis added is usually 'y' (count), then 'x', then 'yy'
   * so this function helps see if we're adding a first dimension (x) or second (yy)
   * I guess it would make more sense to call it xx, not yy, or z?
   *
   * @see runQuery
   */
  public function addAxis($axis) {
    if (!isset($this->_config['axis_x'])) {
      $this->_config['axis_x'] = $axis;
    }
    else {
      $this->_config['axis_y'][] = $axis;
    }
  }

  function parseFilters() {
    // Filters are sent as, for example:
    // ?filter=period-start=2014-09-01,period-end=2014-12-31,arrondissement-1=1,arrondissement-2=1
    if ($f = CRM_Utils_Request::retrieveValue('filter', 'String')) {
      $this->_filters = explode(',', $f);
    }
  }

  function parseGroupBy() {
    if (!empty($_REQUEST['groupby'])) {
      $this->_groupBy = explode(',', $_REQUEST['groupby']);
    }
  }

  /**
   * see a whereClause() from a generator class.
   * The aim was to reduce code duplication from whereClause() functions,
   * but currently this does not really do anything?
   * It might be more intuitive if it generated actual SQL code.
   */
  function whereClauseCommon(&$params) {
    // Filters are sent as, for example:
    // ?filter=period-start=2014-09-01,period-end=2014-12-31,diocese-1=1,diocese-2=1
    foreach ($this->_filters as $filter) {
      // foo[0] will have 'period-start' and foo[1] will have 2014-09-01
      $foo = explode(':', $filter);

      // bar[0] will have 'period' and bar[1] will have 'start'
      // bar[0] will have 'diocese' and bar[1] will have '1'
      $bar = explode('-', $foo[0]);

      if ($bar[0] == 'arrondissements') {
        $this->_config['filters']['arrondissements'][] = intval($bar[1]);
      }
      elseif ($bar[0] == 'districts') {
        $this->_config['filters']['districts'][] = intval($bar[1]);
      }
      elseif ($bar[0] == 'campaigns') {
        $this->_config['filters']['campaigns'][] = intval($bar[1]);
      }
      elseif ($bar[0] == 'gender') {
        $this->_config['filters']['gender'][] = intval($bar[1]);
      }
    }
  }

  /**
   *
   */
  function configGroupByRegionCountry() {
    $countries = CRM_Core_PseudoConstant::country();

    if (empty($this->_config['axis_x'])) {
      // If X is empty, it's a bar chart.
      $this->_config['axis_x'] = [
        'label' => E::ts('Country'),
        'type' => 'number',
      ];

      $this->_config['axis_y'][] = [
        'label' => $this->_options['y_label'],
        'type' => $this->_options['y_type'],
        'series' => $this->_options['y_series'],
      ];

      $this->_select[] = 'civicrm_address.country_id as x';
      $this->_from[] = 'LEFT JOIN civicrm_address ON (civicrm_address.contact_id = c.id AND civicrm_address.is_primary = 1)';
      $this->_config['x_translate'] = $countries;
      return;
    }

    foreach ($countries as $key => $val) {
      if (empty($this->_config['filters']['countries']) || in_array($key, $this->_config['filters']['countries'])) {
        $this->_config['axis_y'][] = [
          'label' => $val,
          'type' => $this->_options['y_type'],
          'series' => $this->_options['y_series'],
          'id' => $key,
        ];
      }
    }

    $this->_select[] = 'civicrm_address.country_id as yy';
  }

  function queryAlterRegionCountry() {
    $this->_group[] = 'civicrm_address.country_id';
  }

  /**
   * Districts de Montreal
   */
  function configGroupByRegionDistrict() {
    $districts = CRM_Dataexplorer_PseudoConstant::getDistricts();

    if (empty($this->_config['axis_x'])) {
      // If X is empty, it's a bar chart.
      $this->_config['axis_x'] = array(
        'label' => 'District',
        'type' => 'number',
      );

      $this->_config['axis_y'][] = array(
        'label' => $this->_options['y_label'],
        'type' => $this->_options['y_type'],
        'series' => $this->_options['y_series'],
        'id' => 1,
      );

      $this->_config['x_translate'] = $districts;
      $this->_select[] = 'district__code_55 as x';
      return;
    }

    foreach ($provinces as $key => $val) {
      if (empty($this->_config['filters']['districts']) || in_array($key, $this->_config['filters']['districts'])) {
        $this->_config['axis_y'][] = array(
          'label' => $val,
          'type' => $this->_options['y_type'],
          'series' => $this->_options['y_series'],
          'id' => $key,
        );
      }
    }

    $this->_select[] = 'district__code_55 as yy';
  }

  function queryAlterRegionDistrict() {
    $this->_group[] = 'district__code_55';
  }

  /**
   * Group by gender.
   */
  function configGroupByOtherGender() {
    $genders = CRM_Dataexplorer_PseudoConstant::getGenders();

    if (empty($this->_config['axis_x'])) {
      // If X is empty, it's a bar chart.
      $this->_config['axis_x'] = array(
        'label' => 'Genre',
        'type' => 'number',
      );

      $this->_config['axis_y'][] = array(
        'label' => $this->_options['y_label'],
        'type' => $this->_options['y_type'],
        'series' => $this->_options['y_series'],
        'id' => 1,
      );

      $this->_config['x_translate'] = $genders;
      $this->_select[] = 'gender_id as x';
      return;
    }

    foreach ($genders as $key => $val) {
      if (empty($this->_config['filters']['gender']) || in_array($key, $this->_config['filters']['gender'])) {
        $this->_config['axis_y'][] = array(
          'label' => $val,
          'type' => $this->_options['y_type'],
          'series' => $this->_options['y_series'],
          'id' => $key,
        );
      }
    }

    $this->_select[] = 'gender_id as yy';
  }

  function queryAlterOtherGender() {
    $this->_group[] = 'gender_id';
  }

  /**
   * Group by campaign (Menu: Afficher -> Autres -> Campagne).
   */
  function configGroupByOtherCampaign() {
    $campaigns = CRM_Dataexplorer_PseudoConstant::getCampaigns();

    if (empty($this->_config['axis_x'])) {
      // If X is empty, it's a bar chart.
      $this->_config['axis_x'] = array(
        'label' => 'Campagne',
        'type' => 'number',
      );

      $this->_config['axis_y'][] = array(
        'label' => $this->_options['y_label'],
        'type' => $this->_options['y_type'],
        'series' => $this->_options['y_series'],
        'id' => 1,
      );

      $this->_select[] = 'campaign as x';
      $this->_config['x_translate'] = $campaigns;
      return;
    }

    foreach ($campaigns as $key => $val) {
      if (empty($this->_config['filters']['campaigns']) || in_array($key, $this->_config['filters']['campaigns'])) {
        $this->_config['axis_y'][] = array(
          'label' => $val,
          'type' => $this->_options['y_type'],
          'series' => $this->_options['y_series'],
          'id' => $key,
        );
      }
    }

    $this->_select[] = 'campaign as yy';
  }

  function queryAlterOtherCampaign() {
    $this->_group[] = 'campaign';
  }

  /**
   *
   */
  function configGroupByOtherContactSubType() {
    $types = CRM_Dataexplorer_PseudoConstant::getContactSubTypes();

    if (empty($this->_config['axis_x'])) {
      // If X is empty, it's a bar chart.
      $this->_config['axis_x'] = [
        'label' => E::ts('Contact SubType'),
        'type' => 'number',
      ];

      $this->_config['axis_y'][] = [
        'label' => $this->_options['y_label'],
        'type' => $this->_options['y_type'],
        'series' => $this->_options['y_series'],
        'id' => 1,
      ];

      $this->_select[] = 'record_type as x';
      $this->_config['x_translate'] = $types;
      return;
    }

    foreach ($types as $key => $val) {
      if (empty($this->_config['filters']['contact_sub_type']) || in_array($key, $this->_config['filters']['contact_sub_type'])) {
        $this->_config['axis_y'][] = [
          'label' => $val,
          'type' => $this->_options['y_type'],
          'series' => $this->_options['y_series'],
          'id' => $key,
        ];
      }
    }

    $this->_select[] = 'contact_sub_type as yy';
  }

  function queryAlterOtherContactSubType() {
    $this->_group[] = 'contact_sub_type';
  }

  /**
   * Given two dates (ex: 2018-02-01 - 2020-04-15), returns the years
   * present in that range (2018, 2019, 2020).
   */
  function getListOfYearsFromFilter($filter_name) {
    $periods = [];

    $start = NULL;
    $end = NULL;

    foreach ($this->_filters as $f) {
      // Ex: period-start:2015-01-01
      $parts = explode(':', $f);

      if ($parts[0] == $filter_name . '-start') {
        // Take only 2015
        $start = substr($parts[1], 0, 4);
      }
      if ($parts[0] == $filter_name . '-end') {
        // Take only 2015
        $end = substr($parts[1], 0, 4);
      }
    }

    if ($start && $end) {
      for ($i = $start; $i <= $end; $i++) {
        $periods[] = array(
          'x' => $i,
          // FIXME why 1215 and 1231?
          'start' => $i . '1215',
          'end' => $i . '1231',
        );
      }
    }

    return $periods;
  }

  /**
   * Logs and executes an SQL query.
   */
  public function executeQuery($sql, $params = []) {
    $query = CRM_Core_DAO::composeQuery($sql, $params, FALSE);
    $this->_sqlLog[] = $query;

    return CRM_Core_DAO::executeQuery($sql, $params);
  }

  /**
   * Assembles the SQL query.
   */
  function runQuery($where, $params, &$data, $x_override) {
    // The query should have max 3 parts: y (count), x (main axis), and yy (secondary count/groupby)
    // Previously, it was up to the functions to say if it's "as y", "as x" or "as yy", which made
    // the code pretty horrible.
    // So now we just assume that the first select is an 'y', second is 'x' and third is 'yy'
    // and for now we will be backwards compatible.

    if (empty($this->_select)) {
      throw new Exception('SELECT query cannot be empty');
    }

    if (isset($this->_select[0])) {
      $this->_select[0] = preg_replace('/ as \w$/', '', $this->_select[0]);
      $this->_select[0] .= ' as y';
    }
    if (isset($this->_select[1])) {
      $this->_select[1] = preg_replace('/ as \w$/', '', $this->_select[1]);
      $this->_select[1] .= ' as x';
    }
    if (isset($this->_select[2])) {
      $this->_select[2] = preg_replace('/ as \w$/', '', $this->_select[2]);
      $this->_select[2] .= ' as yy';
    }

    $sql = 'SELECT ' . implode(', ', $this->_select) . ' '
         . ' FROM ' . implode(' ', $this->_from)
         . (!empty($where) ? ' WHERE ' . $where : '')
         . (!empty($this->_group) ? ' GROUP BY ' . implode(', ', $this->_group) : '')
         . (!empty($this->_order) ? ' ORDER BY ' . implode(', ', $this->_order) : '');

    $dao = $this->executeQuery($sql, $params);

    while ($dao->fetch()) {
      if (($x_override || $dao->x) && $dao->y) {
        $x = (isset($x_override) ? $x_override : $dao->x);
        if (isset($this->_config['x_translate']) && isset($this->_config['x_translate'][$x])) {
          $x = $this->_config['x_translate'][$x];
        }
        elseif (!empty($this->_config['x_translate_strict'])) {
          continue;
        }

        if (count($this->_groupBy) == 2) {
          if (empty($dao->yy)) {
            $data[$x]['NULL'] = $dao->y;
          }
          else {
            $data[$x][$dao->yy] = $dao->y;
          }
        }
        else {
          $ylabel = $this->_options['y_label'];
          $data[$x][$ylabel] = $dao->y;
        }
      }
    }
  }

  /**
   * Returns the log of queries.
   */
  public function getQueryLog() {
    return $this->_sqlLog;
  }

}
