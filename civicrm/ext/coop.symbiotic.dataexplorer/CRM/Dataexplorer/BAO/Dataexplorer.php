<?php

class CRM_Dataexplorer_BAO_Dataexplorer {

  /**
   *
   */
  static public function get($params) {
    $results = [];

    $contact_id = CRM_Core_Session::singleton()->get('userID');

    // FIXME: Just testing.. obviously not right.
    // Happens when we call 'get' from api4?
    if (is_object($params)) {
      $where = $params->getWhere();
      $params = [];

      foreach ($where as $w) {
        $params[$w[0]] = $w[2];
      }
    }

    // Admins can view any visualition
    // hence the SQL condition "1 = %3" (which would be 1=1 if is_admin)
    $is_admin = (int) CRM_Core_Permission::check('administer CiviCRM');

    // This is because of the string validation, but the condition should be removed if empty.
    $key = $params['share_key'] ?: ' ';

    // @todo Horrible code, should use a DAO Entity instead
    $dao = CRM_Core_DAO::executeQuery('SELECT *
      FROM civicrm_dataexplorer
      WHERE ' . (!empty($params['id']) ? ' id = %1 ' : ' 1=1 ') . '
        AND ((contact_id = %2 OR 1 = %3) OR (share_enabled = 1 AND share_key IS NOT NULL AND share_key <> "" AND share_key = %4 AND (share_expire_date IS NULL OR share_expire_date > NOW())))', [
      1 => [$params['id'] ?? 0, 'Integer'],
      2 => [$contact_id, 'Positive'],
      3 => [$is_admin, 'Integer'],
      4 => [$key, 'String'],
    ]);

    while ($dao->fetch()) {
      $data = json_decode($dao->data, true);
      $label = '';

      // @todo Would be nice to have a more clear label
      if (!empty($data['measure'])) {
        $label = implode(', ', array_keys($data['measure']));
      }

      $results[] = [
        'id' => $dao->id,
        'contact_id' => $dao->contact_id,
        'data' => $data,
        'share_enabled' => $dao->share_enabled,
        'share_key' => $dao->share_key,
        'share_expire_date' => ($dao->share_expire_date ? substr($dao->share_expire_date, 0, 10) : ''),
        'label' => $label,
      ];
    }

    return $results;
  }

  /**
   *
   */
  static public function create($params) {
    $contact_id = CRM_Core_Session::singleton()->get('userID');

    CRM_Core_DAO::executeQuery('INSERT INTO civicrm_dataexplorer (contact_id, data) VALUES (%1, %2)', [
      1 => [$contact_id, 'Positive'],
      2 => [$params['data'], 'String'],
    ]);

    // FIXME Presumably a better way..
    $id = CRM_Core_DAO::singleValueQuery('SELECT max(id) from civicrm_dataexplorer');
    return $id;
  }

  /**
   *
   */
  static public function update($params) {
    $contact_id = CRM_Core_Session::singleton()->get('userID');

    // Validate the contact_id
    $creator_contact_id = CRM_Core_DAO::singleValueQuery('SELECT contact_id FROM civicrm_dataexplorer WHERE id = %1', [
      1 => [$params['id'], 'Positive'],
    ]);

    if ($creator_contact_id != $contact_id) {
      throw new \Exception('Access denied - You are not the creator of this visualisation. ' . $contact_id . ' -- ' . $creator_contact_id);
    }

    // FIXME sloppy copy-paste
    $data = json_encode($params['data']);

    CRM_Core_DAO::executeQuery('UPDATE civicrm_dataexplorer SET data = %1 WHERE id = %2', [
      1 => [$data, 'String'],
      2 => [$params['id'], 'Positive'],
    ]);

    if (!empty($params['share_enabled'])) {
      CRM_Core_DAO::executeQuery('UPDATE civicrm_dataexplorer SET share_enabled = %1 WHERE id = %2', [
        1 => [$params['share_enabled'], 'Boolean'],
        2 => [$params['id'], 'Positive'],
      ]);
    }

    if (!empty($params['share_key'])) {
      CRM_Core_DAO::executeQuery('UPDATE civicrm_dataexplorer SET share_key = %1 WHERE id = %2', [
        1 => [$params['share_key'], 'String'],
        2 => [$params['id'], 'Positive'],
      ]);
    }

    if (!empty($params['share_expire_date'])) {
      $date = substr($params['share_expire_date'], 0, 10) . ' 23:59:59';

      CRM_Core_DAO::executeQuery('UPDATE civicrm_dataexplorer SET share_expire_date = %1 WHERE id = %2', [
        1 => [$date, 'String'],
        2 => [$params['id'], 'Positive'],
      ]);
    }

    if (!empty($params['data']['dashlet_enabled'])) {
      $dashlet_id = CRM_Core_DAO::singleValueQuery('SELECT id FROM civicrm_dashboard WHERE name = %1', [
        1 => ["dataexplorer/{$params['id']}", 'String'],
      ]);

      $dashletParams = [];

      if ($dashlet_id) {
        $dashletParams['id'] = $dashlet_id;
      }

      $dashletParams['label'] = $params['data']['dashlet_title'];
      $dashletParams['permissions'] = ['access CiviCRM'];
      $dashletParams['cache_minutes'] = 15;
      $dashletParams['is_active'] = 1;
      $dashletParams['name'] = "dataexplorer/{$params['id']}";
      $dashletParams['url'] = "civicrm/dataexplorer/dashlet?id={$params['id']}&share={$params['share_key']}";
      $dashletParams['fullscreen_url'] = "civicrm/civicrm/dataexplorer#/dataexplorer/{$params['id']}";
      $dashletParams['instanceURL'] = "civicrm/civicrm/dataexplorer#/dataexplorer/{$params['id']}";
      CRM_Core_BAO_Dashboard::create($dashletParams);
    }
    else {
      $dashlet_id = CRM_Core_DAO::singleValueQuery('SELECT id FROM civicrm_dashboard WHERE name = %1', [
        1 => ["dataexplorer/{$params['id']}", 'String'],
      ]);

      if ($dashlet_id) {
        CRM_Core_BAO_Dashboard::create([
          'id' => $dashlet_id,
          'is_active' => 0,
        ]);
      }
    }

    return $params['id'];
  }

}
