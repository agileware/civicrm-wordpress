<?php

class CRM_Dataexplorer_PseudoConstant {
  /**
   * Returns a keyed array of campaigns (custom field #58).
   *
   * @param bool $add_select Whether to add an empty item for forms.
   * @returns Array key/val.
   */
  public static function getCampaigns($add_select = FALSE) {
    return array();
    // return self::getOptionValues(DEVPDATAVIZ_OPTION_GROUP_CAMPAIGN, $add_select);
  }

  /**
   * Returns a keyed array of montreal boroughs.
   *
   * @param bool $add_select Whether to add an empty item for forms.
   *
   * @returns Array key/val.
   */
  public static function getArrondissements($add_select = FALSE) {
    $items = array();

    if ($add_select) {
      $items[''] = ts('- select -');
    }

    $result = civicrm_api3('Contact', 'getoptions', array(
      'field' => 'custom_54',
      'option.limit' => 0,
    ));

    foreach ($result['values'] as $key => $val) {
      $items[$key] = $val;
    }

    $items['NULL'] = ts('n/a');

    return $items;
  }

  /**
   * Returns a keyed array of montreal districts.
   *
   * @param bool $add_select Whether to add an empty item for forms.
   *
   * @returns Array key/val.
   */
  public static function getDistricts($add_select = FALSE) {
    $items = array();

    if ($add_select) {
      $items[''] = ts('- select -');
    }

    $result = civicrm_api3('Contact', 'getoptions', array(
      'field' => 'custom_55',
      'option.limit' => 0,
      'option.language' => 'fr_CA',
    ));

    foreach ($result['values'] as $key => $val) {
      $items[$key] = $val;
    }

    return $items;
  }

  /**
   *
   */
  public static function getContactSubTypes() {
    return civicrm_api3('Contact', 'getoptions', [
      'field' => 'contact_sub_type',
    ])['values'];
  }

  /**
   *
   */
  public static function getGenders() {
    return civicrm_api3('Contact', 'getoptions', [
      'field' => 'gender_id',
    ])['values'];
  }

  /**
   *
   */
  public static function getPaymentProcessors() {
    $options = [];

    $resetCache = FALSE;
    $isCurrentDomainOnly = TRUE;
    $isActive = FALSE;

    $processors = \CRM_Financial_BAO_PaymentProcessor::getAllPaymentProcessors('live', $resetCache, $isCurrentDomainOnly, $isActive);

    foreach ($processors as $key => $val) {
      $options[$key] = $val['name'];
    }

    return $options;
  }

  /**
   * Returns option values for a given option_group_id.
   *
   * @param Int $option_group_id
   * @param bool $add_select Whether to add an empty item for forms.
   * @returns Array key/val.
   */
  public static function getOptionValues($option_group_id, $add_select = FALSE) {
    $items = array();

    if ($add_select) {
      $items[''] = ts('- select -');
    }

    $result = civicrm_api3('OptionValue', 'get', array(
      'option_group_id' => $option_group_id,
      'is_active' => 1,
      'option.limit' => 0,
    ));

    foreach ($result['values'] as $key => $val) {
      $items[$val['value']] = $val['label'];
    }

    return $items;
  }
}
