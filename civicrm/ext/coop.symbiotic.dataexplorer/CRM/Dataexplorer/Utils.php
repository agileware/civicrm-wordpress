<?php

class CRM_Dataexplorer_Utils {

  /**
   *
   */
  public static function addGraphicsResources() {
    CRM_Core_Resources::singleton()->addScriptFile('dataexplorer', 'js/accounting.min.js');
    # CRM_Core_Resources::singleton()->addScriptFile('dataexplorer', 'dist/html2canvas/html2canvas.min.js');

    CRM_Core_Resources::singleton()->addStyleFile('dataexplorer', 'css/dataexplorer.css');

    // @todo Can we avoid loading leaflet or chartjs if it's not being used?
    // Not a problem for the explorer, but slight problem for embeds
    Civi::resources()
      ->addStyleFile('dataexplorer', 'node_modules/tabulator-tables/dist/css/tabulator.min.css', 1, 'html-header')
      ->addScriptFile('dataexplorer', 'node_modules/tabulator-tables/dist/js/tabulator.min.js', 1, 'html-header')
      ->addScriptFile('dataexplorer', 'node_modules/chart.js/dist/Chart.bundle.min.js', 1, 'html-header')
      ->addStyleFile('dataexplorer', 'node_modules/chart.js/dist/Chart.min.css', 2, 'html-header')
      ->addScriptFile('dataexplorer', 'node_modules/google-palette/palette.js', 2, 'html-header')
      ->addStyleFile('dataexplorer', 'node_modules/leaflet/dist/leaflet.css', 2, 'html-header')
      ->addScriptFile('dataexplorer', 'node_modules/leaflet/dist/leaflet.js', 2, 'html-header')
      ->addScriptFile('dataexplorer', 'js/data-countries.js', 2, 'html-header');
  }

}
