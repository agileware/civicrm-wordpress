CREATE TABLE `civicrm_dataexplorer` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Unique ID',
  `contact_id` int(10) unsigned DEFAULT NULL COMMENT 'FK to Contact ID',
  `data` text COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Data explorer visualisation settings',
  `share_enabled` tinyint(4) unsigned DEFAULT 0 COMMENT 'Whether sharing is enabled',
  `share_key` varchar(16) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Secret key to share read-only access',
  `share_expire_date` datetime DEFAULT NULL COMMENT 'Expiration date if shared',
  PRIMARY KEY (`id`),
  KEY `FK_civicrm_dataexplorer_contact_id` (`contact_id`),
  CONSTRAINT `FK_civicrm_dataexplorer_contact_id` FOREIGN KEY (`contact_id`) REFERENCES `civicrm_contact` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
