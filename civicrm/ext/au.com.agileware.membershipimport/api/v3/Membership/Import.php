<?php

/**
 * Membership.Import API specification (optional)
 * This is used for documentation and validation.
 *
 * @param array $spec description of fields supported by this API call
 * @return void
 * @see http://wiki.civicrm.org/confluence/display/CRMDOC/API+Architecture+Standards
 */
function _civicrm_api3_membership_Import_spec(&$spec) {
  $spec['source']['api.required']    = 1;
}

/**
 * Membership.Import API
 *
 * @param array $params
 * @return array API result descriptor
 * @see civicrm_api3_create_success
 * @see civicrm_api3_create_error
 * @throws API_Exception
 */
function civicrm_api3_membership_Import($params) {
  $source = $params['source'];
  $handle = fopen($source, 'r');
  $headers = array_map('trim', fgetcsv($handle));
  $result = array();
  while ($row = fgetcsv($handle)) {
    $properties                    = array_combine($headers, array_map('trim', $row));
    $contact_id                    = _membershipimport_contact_upsert($properties);
    $membership_id                 = _membershipimport_membership_upsert($contact_id, $properties);
    $contribution_id               = _membershipimport_contribution_upsert($contact_id, $properties);
    $membershippayment_id          = _membershipimport_membershippayment_upsert($membership_id, $contribution_id);
    $result[$membershippayment_id] = array(
      'id'              => $membershippayment_id,
      'contact_id'      => $contact_id,
      'membership_id'   => $membership_id,
      'contribution_id' => $contribution_id,
    );
  }


  $returnValues = $result;
  // ALTERNATIVE: $returnValues = array(); // OK, success
  // ALTERNATIVE: $returnValues = array("Some value"); // OK, return a single value

  // Spec: civicrm_api3_create_success($values = 1, $params = array(), $entity = NULL, $action = NULL)
  return civicrm_api3_create_success($returnValues, $params, 'Membership', 'Import');
}

function _membershipimport_contact_upsert($properties) {
  $first_name = $properties['First Name'];
  $last_name  = $properties['Last Name'];
  $country = !empty($properties['Country']) ? $properties['Country'] : 'Australia';
  $country_id = _membershipimport_country_id($country);

  $unique_properties = array(
    'first_name'   => $first_name,
    'last_name'    => $last_name,
    'contact_type' => 'Individual',
  );

  $address_properties = array_filter(array(
    'street_address'         => $properties['Home-Street Address'],
    'supplemental_address_1' => $properties['Home-Supplemental Address 1'],
    'city'                   => $properties['Home-City'],
    'state_province_id'      => _membershipimport_state_id($country_id, $properties['Home-State']),
    'postal_code'            => $properties['Home-Postal Code'],
    'country'                => $country_id,
  ));

  _membershipimport_log("Upserting contact: $first_name $last_name");

  $existing_result = civicrm_api3('Contact', 'get', array(
    'sequential' => 1,
    'is_deleted' => 0,
  ) + $unique_properties);

  if (!empty($existing_result['is_error'])) {
    _membershipimport_log("Error retrieving contact $first_name $last_name");
  }

  $count = (int)$existing_result['count'];
  if ($count > 1) {
    $error_message = "$count contacts found for $first_name $last_name. ";
    $error_message .= "This contact will not be imported until this is fixed manually in CiviCRM.";
    _membershipimport_log($error_message);
    return;
  }
  elseif ($count == 1) {
    // When there is only one result, the API response has a top-level 'id'
    // parameter corresponding to the one result Contact ID.
    $contact_id = $existing_result['id'];
    $result = civicrm_api3('Contact', 'create', array(
      'id' => $contact_id,
    ) + $unique_properties);
    if (!empty($result['is_error'])) {
      _membershipimport_log("Could not update contact $first_name $last_name");
    }
    $contact_id = $result['id'];
    _membershipimport_log("Updated contact $contact_id: $first_name $last_name");
  }
  else {
    $result = civicrm_api3('Contact', 'create', $unique_properties);
    if (!empty($result['is_error'])) {
      _membershipimport_log("Could not create contact $first_name $last_name");
    }
    $contact_id = $result['id'];
    _membershipimport_log("Created new contact $contact_id: $first_name $last_name");
  }

  if (!empty($address_properties)) {
    _membershipimport_address_upsert($contact_id, $address_properties);
  }

  return $contact_id;
}

// Takes street_address, supplemental_address_1, city, state_province_id,
// and postal_code keys
function _membershipimport_address_upsert($contact_id, $address_params) {
  $result = civicrm_api3('Address', 'get', array(
    'sequential' => 1,
    'contact_id' => $contact_id,
  ));
  if (!empty($result['is_error'])) {
    _membershipimport_log("Could not retrieve address details for contact id $contact_id", 'error');
  }
  else {
    $api_params = array(
      'contact_id'       => $contact_id,
      'location_type_id' => 'Main',
      'is_primary'       => 1,
    );
    $count = (int)$result['count'];
    if ($count>0) {
      $addresses = $result['values'];
      foreach ($addresses as $address) {
        if ($address['is_primary'] == '1') {
          $address_id = $address['id'];
          $address_params['id'] = $address_id;
          _membershipimport_log("Updating Address entity $address_id");
          break;
        }
      }
    }
    $params = $api_params + $address_params;
    $address_create = civicrm_api3('Address', 'create', $params);
    if (!empty($address_create['is_error'])) {
      _membershipimport_log("Could not create address for contact $contact_id", 'error');
    }
  }
}

function _membershipimport_country_id($country_name) {
  $countries = array_flip(CRM_Core_PseudoConstant::country());
  return $countries[$country_name];
}

function _membershipimport_state_id($country_id, $state_abbreviation) {
  $query = "SELECT id FROM civicrm_state_province WHERE country_id = %1 AND abbreviation = %2";
  $params = array(
    1 => array($country_id, 'Integer'),
    2 => array(strtoupper($state_abbreviation), 'String'));
  $state_id = CRM_Core_DAO::singleValueQuery($query, $params);
  return $state_id;
}

function _membershipimport_membership_upsert($contact_id, $properties) {
  $membership_type = $properties['Membership Type'];
  $start_date = _membershipimport_date_rewrite($properties['Received Date']);

  $membership_properties = array(
    'membership_type_id' => $membership_type,
    'start_date' => $start_date,
    'contact_id' => $contact_id,
  );

  $existing_result = civicrm_api3('Membership', 'get', $membership_properties);
  if (!empty($existing_result['is_error'])) {
    _membershipimport_log("Could not retrieve membership details for contact id $contact_id", 'error');
  }
  $count = (int)$existing_result['count'];
  if ($count > 1) {
    $error_message = "$count memberships with the same start date and type found for $contact_id. ";
    $error_message .= "This membership will not be imported until this is fixed manually in CiviCRM.";
    _membershipimport_log($error_message, 'error');
    return;
  }
  elseif ($count == 1) {
    // When there is only one result, the API response has a top-level 'id'
    // parameter corresponding to the one result Contact ID.
    $membership_id = $existing_result['id'];
    $result = civicrm_api3('Membership', 'create', array(
      'id' => $membership_id,
    ) + $membership_properties);
    if (!empty($result['is_error'])) {
      _membershipimport_log("Could not update membership $membership_id");
    }
    _membershipimport_log("Updated membership $membership_id for contact $contact_id");
  }
  else {
    $membership_create = civicrm_api3('Membership', 'create', $membership_properties);
    if (!empty($membership_create['is_error'])) {
      _membershipimport_log("Could not create membership for contact $contact_id", 'error');
    }
    $membership_id = $membership_create['id'];
    _membershipimport_log("Created new membership $membership_id for $contact_id");
  }

  return $membership_id;
}

function _membershipimport_contribution_upsert($contact_id, $properties) {
  $total_amount = $properties['Total Amount'];
  $receive_date = _membershipimport_date_rewrite($properties['Received Date']);

  $contribution_properties = array(
    'total_amount'      => $total_amount,
    'receive_date'      => $receive_date,
    'contact_id'        => $contact_id,
    'financial_type_id' => 'Member Dues',
  );

  $existing_result = civicrm_api3('Contribution', 'get', $contribution_properties);
  if (!empty($existing_result['is_error'])) {
    _membershipimport_log("Could not retrieve contribution details for contact id $contact_id", 'error');
  }
  $count = (int)$existing_result['count'];
  if ($count > 1) {
    $error_message = "$count contributions with the same receive date and type found for $contact_id. ";
    $error_message .= "This contribution will not be imported until this is fixed manually in CiviCRM.";
    _membershipimport_log($error_message, 'error');
    return;
  }
  elseif ($count == 1) {
    // When there is only one result, the API response has a top-level 'id'
    // parameter corresponding to the one result Contact ID.
    $contribution_id = $existing_result['id'];
    $result = civicrm_api3('Contribution', 'create', array(
      'id' => $contribution_id,
    ) + $contribution_properties);
    if (!empty($result['is_error'])) {
      _membershipimport_log("Could not update contribution $contribution_id");
    }
    _membershipimport_log("Updated contribution $contribution_id for contact $contact_id");
  }
  else {
    $contribution_create = civicrm_api3('Contribution', 'create', $contribution_properties);
    if (!empty($contribution_create['is_error'])) {
      _membershipimport_log("Could not create contribution for contact $contact_id", 'error');
    }
    $contribution_id = $contribution_create['id'];
    _membershipimport_log("Created new contribution $contribution_id for $contact_id");
  }

  return $contribution_id;
}

function _membershipimport_membershippayment_upsert($membership_id, $contribution_id) {

  $membershippayment_properties = array(
    'membership_id' => $membership_id,
    'contribution_id' => $contribution_id,
  );

  $existing_result = civicrm_api3('MembershipPayment', 'get', $membershippayment_properties);
  if (!empty($existing_result['is_error'])) {
    _membershipimport_log("Could not retrieve membershippayment details for membership $membership_id and contribution $contribution_id", 'error');
  }
  $count = (int)$existing_result['count'];
  if ($count > 1) {
    $error_message = "$count results found for membership $membership_id and contribution $contribution_id. ";
    $error_message .= "This membershippayment will not be imported until this is fixed manually in CiviCRM.";
    _membershipimport_log($error_message, 'error');
    return;
  }
  elseif ($count == 1) {
    // When there is only one result, the API response has a top-level 'id'
    // parameter corresponding to the one result Contact ID.
    $membershippayment_id = $existing_result['id'];
    $result = civicrm_api3('MembershipPayment', 'create', array(
      'id' => $membershippayment_id,
    ) + $membershippayment_properties);
    if (!empty($result['is_error'])) {
      _membershipimport_log("Could not update membershippayment $membershippayment_id.");
    }
    _membershipimport_log("Updated membershippayment $membershippayment_id.");
  }
  else {
    $membershippayment_create = civicrm_api3('MembershipPayment', 'create', $membershippayment_properties);
    if (!empty($membershippayment_create['is_error'])) {
      _membershipimport_log("Could not create membershippayment for membership $membership_id and contribution $contribution_id", 'error');
    }
    $membershippayment_id = $membershippayment_create['id'];
    _membershipimport_log("Created new membershippayment $membershippayment_id.");
  }

  return $membershippayment_id;
}

function _membershipimport_date_rewrite($datestring) {
  return preg_replace('/(\d+)\/(\d+)\/(\d+)/', '$3-$2-$1', $datestring);
}

function _membershipimport_log($message, $priority=NULL) {
  CRM_Core_Error::debug_log_message($message, FALSE, 'membershipimport', $priority);
}
