SLM Membership Import
=====================

This is a CiviCRM extension that imports Contacts, Contributions, Memberships,
and MembershipPayments as per #20644. It provides a Membership import API call
that takes a 'source' parameter which can be a file path or URL.

# Installation
1. Copy/clone this repository into the CiviCRM extensions directory.
2. Enable it in CiviCRM. A cache clear may be required.
3. Set up a scheduled job or use the API Explorer with the new Membership import action.
