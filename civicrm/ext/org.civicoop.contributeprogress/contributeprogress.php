<?php

require_once 'contributeprogress.civix.php';
use CRM_Contributeprogress_ExtensionUtil as E;

/**
 * Implements hook_civicrm_config().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_config/ 
 */
function contributeprogress_civicrm_config(&$config) {
  _contributeprogress_civix_civicrm_config($config);
}

/**
 * Implements hook_civicrm_xmlMenu().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_xmlMenu
 */
function contributeprogress_civicrm_xmlMenu(&$files) {
  _contributeprogress_civix_civicrm_xmlMenu($files);
}

/**
 * Implements hook_civicrm_install().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_install
 */
function contributeprogress_civicrm_install() {
  _contributeprogress_civix_civicrm_install();
}

/**
 * Implements hook_civicrm_postInstall().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_postInstall
 */
function contributeprogress_civicrm_postInstall() {
  _contributeprogress_civix_civicrm_postInstall();
}

/**
 * Implements hook_civicrm_uninstall().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_uninstall
 */
function contributeprogress_civicrm_uninstall() {
  _contributeprogress_civix_civicrm_uninstall();
}

/**
 * Implements hook_civicrm_enable().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_enable
 */
function contributeprogress_civicrm_enable() {
  _contributeprogress_civix_civicrm_enable();
}

/**
 * Implements hook_civicrm_disable().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_disable
 */
function contributeprogress_civicrm_disable() {
  _contributeprogress_civix_civicrm_disable();
}

/**
 * Implements hook_civicrm_upgrade().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_upgrade
 */
function contributeprogress_civicrm_upgrade($op, CRM_Queue_Queue $queue = NULL) {
  return _contributeprogress_civix_civicrm_upgrade($op, $queue);
}

/**
 * Implements hook_civicrm_managed().
 *
 * Generate a list of entities to create/deactivate/delete when this module
 * is installed, disabled, uninstalled.
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_managed
 */
function contributeprogress_civicrm_managed(&$entities) {
  _contributeprogress_civix_civicrm_managed($entities);
}

/**
 * Implements hook_civicrm_alterSettingsFolders().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_alterSettingsFolders
 */
function contributeprogress_civicrm_alterSettingsFolders(&$metaDataFolders = NULL) {
  _contributeprogress_civix_civicrm_alterSettingsFolders($metaDataFolders);
}


/**
 * Implements hook_civicrm_buildForm().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_buildForm
 */
function contributeprogress_civicrm_buildForm($formName, &$form) {
  if ($formName == 'CRM_Contribute_Form_Contribution_Main') {
    CRM_Contributeprogress_Contribute_Form_Contribution_Main::buildForm($form);
  }
}

/**
 * Implements hook_civicrm_navigationMenu().
 *
 * @param array $menu
 */
function contributeprogress_civicrm_navigationMenu(&$menu) {
  _contributeprogress_civix_insert_navigation_menu($menu, 'Administer/CiviContribute', [
    'label' => E::ts('Contribution Page Progress Bar'),
    'name' => 'contributeprogress',
    'url' => 'civicrm/admin/setting/contributeprogress',
    'permission' => 'administer CiviCRM',
    'operator' => NULL,
    'separator' => NULL,
  ]);
}
