{if $widget_embed}
  <!DOCTYPE html>
  <html>
    <head>
{/if}
{literal}
<style>
  .crm-contribute-widget-regular {
    margin: 2em 0;
  }
  .crm-contribute-widget-amount-raised {
    font-size: 140%;
  }
  .crm-contribute-widget .crm-amount-bar {
    background-color: #eee;
    width: 100%;
    display: block;
    margin: .8em 0;
    text-align: left;
  }
  .crm-contribute-widget .crm-amount-fill {
    height: 2em;
    display: block;
    text-align: left;
  }
  .crm-contribute-widget {
    background-color: {/literal}{$widget.colors.main}{literal}; /* background color */
    border-color: {/literal}{$widget.colors.bg}{literal}; /* border color */
  }
  .crm-contribute-widget .crm-amount-fill {
    background-color: {/literal}{$widget.colors.bar}{literal};
  }
  .crm-contribute-widget-stats {
    display: flex;
  }
  .crm-contribute-widget-stats > div {
    flex: 1;
  }
  /* do not add extra padding if embedding */
  .crm-contribute-widget-regular .crm-contribute-widget-stats > div {
    padding-right: 1em;
  }
  .crm-contribute-widget-stats > div:last-child {
    padding-right: 0;
  }
  .crm-contribute-widget-stats .crm-contribute-widget-stats-metric {
    display: block;
    font-weight: bold;
  }
  .crm-contribute-widget-embed .crm-contribute-widget-stats {
    font-size: 14px;
  }
  .crm-contribute-widget-embed .crm-contribute-widget-stats .crm-contribute-widget-stats-metric {
    font-size: 16px;
  }
  .crm-contribute-widget-regular .crm-contribute-widget-stats .crm-contribute-widget-stats-metric {
    font-size: 140%;
  }
  /* takes too much space in widget form */
  .crm-contribute-widget-embed .crm-contribute-widget-stats-cpt-donors {
    display: none;
  }
</style>
{/literal}
{if $widget_embed}
  {if $widget_css}
    <style>
      {$widget_css}
    </style>
  {/if}
  </head>
  <body>
{/if}

<div class="crm-contribute-widget {if $widget_embed}crm-contribute-widget-embed{else}crm-contribute-widget-regular{/if}">
  <div class="crm-contribute-widget-amounts">{ts 1=$widget.money_raised_formatted 2=$widget.money_target_formatted}<span class="crm-contribute-widget-amount-raised">%1</span><span class="crm-contribute-widget-amount-of"> of</span> <span class="crm-contribute-widget-amount-objective">%2</span> Raised{/ts}</div>
  <div class="crm-amount-bar">
    <div class="crm-amount-fill" style="width: {$widget.money_raised_percentage_width}%;"></div>
  </div>
  <div class="crm-contribute-widget-stats">
    <div class="crm-contribute-widget-stats-pct-goal">{ts 1=$widget.money_raised_percentage}<span class="crm-contribute-widget-stats-metric">%1</span> Goal Met{/ts}</div>
    <div class="crm-contribute-widget-stats-amount-left">{ts 1=$widget.amount_left}<span class="crm-contribute-widget-stats-metric">%1</span> Still Needed{/ts}</div>
    <div class="crm-contribute-widget-stats-cpt-donors">{ts count=$widget.num_donors plural="<span class='crm-contribute-widget-stats-metric'>%count</span> Supporters"}<span class="crm-contribute-widget-stats-metric">1</span> Supporter{/ts}</div>
    {if $widget.show_days_left}
      <div class="crm-contribute-widget-stats-days-left">{ts count=$widget.days_left plural="<span class='crm-contribute-widget-stats-metric'>%count</span> Days Left"}<span class="crm-contribute-widget-stats-metric">1</span> Day Left{/ts}</div>
    {/if}
  </div>
  {if $widget_embed}
    <div class="crm-contribute-button-wrapper" style="background: {$widget.colors.about_link}; padding: 10px; margin-top: 1em;">
      <a href="{$widget.contribution_page_url}" onclick="return crmContributeWidgetOpen(this);" style="color: white; text-decoration: none; display: block; text-align: center; font-family: sans-serif;"><span class="crm-contribute-button-inner">{$widget.button_title}</span></a>
    </div>
  {/if}
</div>

{literal}
<script>
  function crmContributeWidgetOpen(element) {
    var url = element.href;
    window.open(url);
    return false;
  }
</script>
{/literal}

{if $widget_embed}
    </body>
  </html>
{/if}
