{literal}
(function() {

  document.addEventListener("DOMContentLoaded", function(event) {
    var elements = document.querySelectorAll(".crm-contributeprogress-widget");

    // Inspired by: https://www.doorkeeper.jp/developer/embeddable-javascript-widget?locale=en
    for (let i = 0; i < elements.length; i++) {
      let el = elements[i];

      // Replace the URL bits for /civicrm/contribute/transact for /civicrm/contributeprogress-widget
      let href = el.href.replace(/contribute\/transact/, 'contributeprogress-widget');

      // Replace the original link with the new iframe
      let iframe = document.createElement('iframe');
      iframe.setAttribute('src', href);
      iframe.setAttribute('frameborder', '0');
      iframe.setAttribute('scrolling', 'no');
      iframe.setAttribute('width', '{/literal}{$widget_width}{literal}');
      iframe.setAttribute('height', '{/literal}{$widget_height}{literal}');

      el.parentNode.replaceChild(iframe, el);
    }
  });

})();
{/literal}
