<?php

use CRM_Contributeprogress_ExtensionUtil as E;

class CRM_Contributeprogress_Page_WidgetJS extends CRM_Core_Page {

  public function run() {
    CRM_Utils_System::setHttpHeader('Content-Type', 'application/javascript');

    $width = Civi::settings()->get('contributeprogress_width');
    $height = Civi::settings()->get('contributeprogress_height');

    $smarty = CRM_Core_Smarty::singleton();
    $smarty->assign('widget_width', $width);
    $smarty->assign('widget_height', $height);
    $html = $smarty->fetch('CRM/Contributeprogress/Contribute/Form/Contribution/widgetjs.tpl');
    echo $html;

    CRM_Utils_System::civiExit();
  }

}
