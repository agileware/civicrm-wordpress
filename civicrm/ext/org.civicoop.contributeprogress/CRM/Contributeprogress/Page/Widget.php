<?php

use CRM_Contributeprogress_ExtensionUtil as E;

class CRM_Contributeprogress_Page_Widget extends CRM_Core_Page {

  public function run() {
    $id = CRM_Utils_Request::retrieveValue('id', 'Positive');

    $dao = CRM_Core_DAO::executeQuery('select * from civicrm_contribution_widget where contribution_page_id = %1 AND is_active = 1', [
      1 => [$id, 'Positive'],
    ]);

    if (!$dao->fetch()) {
      // Intentionally vague, we never know what value metadata can have
      throw new Exception('This contribution widget is not active or the page does not exist.');
      return;
    }

    $widget_id = $dao->id;
    $include_pending = false;

    $data = CRM_Contributeprogress_Utils::getContributionPageData($id, $widget_id, $include_pending);

    if (empty($data['is_active'])) {
      throw new Exception('This contribution widget is not active or the page does not exist.');
    }

    // Custom CSS, if any
    $css = Civi::settings()->get('contributeprogress_css');

    $smarty = CRM_Core_Smarty::singleton();
    $smarty->assign('widget', $data);
    $smarty->assign('widget_embed', true);
    $smarty->assign('widget_css', $css);
    $html = $smarty->fetch('CRM/Contributeprogress/Contribute/Form/Contribution/widget.tpl');
    echo $html;

    CRM_Utils_System::civiExit();
  }

}
