<?php

class CRM_Contributeprogress_Contribute_Form_Contribution_Main {

  /**
   * @see contributeprogress_civicrm_buildForm().
   */
  public static function buildForm(&$form) {
    $id = $form->_id;

    $dao = CRM_Core_DAO::executeQuery('select * from civicrm_contribution_widget where contribution_page_id = %1 AND is_active = 1', [
      1 => [$id, 'Positive'],
    ]);

    if (!$dao->fetch()) {
      return;
    }

    $widget_id = $dao->id;
    $include_pending = false;

    $data = CRM_Contributeprogress_Utils::getContributionPageData($id, $widget_id, $include_pending);

    $form->assign('widget', $data);

    CRM_Core_Region::instance('form-top')->add([
      'template' => 'CRM/Contributeprogress/Contribute/Form/Contribution/widget.tpl',
    ]);
  }

}
