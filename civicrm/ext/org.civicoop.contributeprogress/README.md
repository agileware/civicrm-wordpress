# Contribution Page Progress Bar

![Screenshot](images/screenshot.png)

Displays the contribution progress bar widget on contribution pages.

Initially developed by Roshani Kothari
([@roshani](https://chat.civicrm.org/civicrm/messages/@roshani)), with funding of the [Plastic
Pollution Coalition](https://www.plasticpollutioncoalition.org/) and a bit of help from
[Coop Symbiotic](https://www.symbiotic.coop/en).

The extension is licensed under [AGPL-3.0](LICENSE.txt).

## Requirements

* PHP v7.0+
* CiviCRM 5.latest

## Installation

Install as a regular CiviCRM extension.

## Usage

* On a Contribution Page, go to the "Widgets" tab to enable the widget.
* Fill in the fields. Some are mandatory and are not displayed (because they would be redundant), such as the title, button title, about, etc.
* Customize the widget colours to match your site's design. This might require a bit of fiddling since the names are not very relevant, but the preview should be fairly accurate (except that the widget added to the contribution page will not include the title, about, etc, so not all colour settings are used).

To view the end result, visit the live or test version of your contribution page.

A few global settings are available under Administer > CiviContribute > Contribution Page Progress Bar. Notably, when embedding the widget into another website, it is possible to add custom CSS, and to specify the iframe width and height.

## Embeddable widget

It is possible to integrate a contribution widget into an external site using the following example:

```
<script type="text/javascript" src="https://civicrm.org/civicrm/contributeprogress-widget.js"></script>
<a class="crm-contributeprogress-widget" href="https://civicrm.org/civicrm/contribute/transact?reset=1&id=122">Make it Happen!</a>
```

Notes:

* This has not been tested on WordPress yet, but should work if "clean URLs" are enabled.
* The "href" part of the link is the link to the contribution page. The widget code will replace "contribute/transact" by "contributeprogress-widget".
* If the user does not support Javascript (or uses an ancient browser such as IE9), they will see a plain link, instead of the embedded iframe.

## Future ideas

* Display an HTML snippet from the contribution page widget admin, for an easy copy-paste.
* Hide the old widget options that aren't used.
* Allow a configurable option per contrib form for "include pending contributions" (currently they are not included).
* Display the widget on the ThankYou page (so that the donor can see before and after).

## Support

Please post bug reports in the issue tracker of this project on CiviCRM's Gitlab:
https://lab.civicrm.org/extensions/contributeprogress/issues

This extension was written thanks to the financial support of organisations
using it, as well as the very helpful and collaborative CiviCRM community.

While we do our best to provide free community support for this extension,
please consider financially contributing to support or development of this
extension.

Support via Coop Symbiotic:
https://www.symbiotic.coop/en

Coop Symbiotic is a worker-owned co-operative based in Canada. We have a strong
experience working with non-profits and CiviCRM. We provide affordable, fast,
turn-key hosting with regular upgrades and proactive monitoring, as well as custom
development and training.
