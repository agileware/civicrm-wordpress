<?php

use CRM_Pricesetvisibility_ExtensionUtil as E;

class CRM_Pricesetvisibility_Contribute_Form_Contribution_Main {

  /**
   * @see pricesetvisibility_civicrm_buildForm().
   */
  public static function buildForm(&$form) {
    $price_set_id = $form->_priceSetId;

    try {
      $priceset = civicrm_api3('PriceSet', 'getsingle', [
        'id' => $price_set_id,
     ]);
    } catch (CiviCRM_API3_Exception $e) {
      return;
    }

    // Ignore "quick" pricesets
    if ($priceset['is_quick_config']) {
      return;
    }

    $pf_params = [
      'price_set_id' => $price_set_id,
      'is_active' => 1,
      'sort' => [
        'weight ASC',
      ],
    ];

    // If viewing a ContributionPage as admin, admin-only fields will be visible
    // This has an impact on whether we hide the Total Amount section or not.
    if (!CRM_Core_Permission::check('edit contributions')) {
      // Show only public fields
      $pf_params['visibility_id'] = 1;
    }

    $fields = civicrm_api3('PriceField', 'get', $pf_params)['values'];

    self::improveOtherAmountField($form, $priceset, $fields);
    self::showHidePriceFieldOptions($form, $priceset, $fields);
  }

  /**
   * Hides the "Total Amount" if this is a non-quick priceset with only an
   * amount + other field. This mimics how the 'quick' priceset works.
   * Also shows the currency in the Other Amount field.
   */
  private static function improveOtherAmountField($form, $priceset, $fields) {
    // Expect the first field to be a radio button
    $first_field = array_shift($fields);

    if ($first_field['html_type'] != 'Radio') {
      return;
    }

    // Expect the second field to be a text field and has a label "Other Amount"
    // or "Other" (using core translation on purpose).
    $second_field = array_shift($fields);

    if (!($second_field['html_type'] == 'Text' && ($second_field['label'] == ts('Other Amount') || $second_field['label'] == ts('Other')))) {
      return;
    }

    // Hide the Total Amount if there are only 2 fields (Amount and Other Amount)
    // We check for empty because we have been shifting the array above.
    if (empty($fields)) {
      Civi::resources()->addStyle('#pricesetTotal {display: none;}');
    }

    $first_id = 'price_' . $first_field['id'];
    $other_id = 'price_' . $second_field['id'];

    // When Other Amount is clicked, unselect radio amounts
    Civi::resources()->addScript('CRM.$("#' . $other_id . '").focus(function() {
      if (CRM.$("input[name=' . $first_id . ']").attr("type") == "radio") {
        // Removing "selected" class on the parent for radiobuttons compatibility.
        CRM.$("input[name=' . $first_id . ']").prop("checked", false).trigger("change").parent().removeClass("selected");
        // This is horrible, but required to reset the line_raw_total on the input
        // otherwise calculateAmount() will still think this is selected.
        calculateRadioLineItemValue("input[name=' . $first_id . ']");
      }
    });');

    // When radio amounts are clicked, remove the Other Amount
    Civi::resources()->addScript('CRM.$("input[name=' . $first_id . ']").click(function() {CRM.$("#' . $other_id . '").val("").trigger("change"); });');

    Civi::resources()->addScript('
      // Disable autocomplete/autofill
      CRM.$("#' . $other_id . '").attr("autocomplete", "off");

      // Convert to a type=number if inputmask is enabled
      if (typeof CRM.inputmasksNumericOnlyApply != "undefined") {
        CRM.inputmasksNumericOnlyApply("#' . $other_id . '");
      }
    ');

    // Remove the "- none -" from the first field.
    Civi::resources()->addStyle("#priceset .{$first_field['name']}-content .price-set-row:last-child {display: none;}");

    // Show the currency in the Other Amount field
    // This is a bit offtopic, but convenient.
/* [ML] was showing oddly on PPC, with another icon under it */
/*
    $currency = civicrm_api3('ContributionPage', 'getsingle', [
      'id' => $form->get('id'),
      'return' => 'currency',
    ])['currency'];

    if ($currency == 'EUR') {
      Civi::resources()->addStyle('.other_amount-content {position: relative;} .other_amount-content:after {position: absolute; top: 20px; content:"€"; right: 20px; font-size: 24px;}');
    }
    elseif ($currency == 'USD') {
      Civi::resources()->addStyle('.other_amount-content {position: relative;} .other_amount-content:after {position: absolute; top: 20px; content:"$"; right: 20px; font-size: 24px;}');
    }
    elseif ($currency == 'CAD') {
      Civi::resources()->addStyle('.other_amount-content {position: relative;} .other_amount-content:after {position: absolute; top: 20px; content:"$"; right: 20px; font-size: 24px;}');
    }
*/
  }

  /**
   *
   */
  private static function showHidePriceFieldOptions($form, $priceset, $fields) {
    $has_showhide_options = false;

    $recur_visibility_options = [];

    foreach ($fields as $field) {
      $dao = CRM_Core_DAO::executeQuery('SELECT * FROM civicrm_price_set_visibility vis LEFT JOIN civicrm_price_field_value pfv ON pfv.id = vis.price_field_value_id AND vis.price_field_id = pfv.price_field_id WHERE vis.price_field_id = %1 AND vis.recur_visibility != "always" AND pfv.is_active = 1', [
        1 => [$field['id'], 'Positive'],
      ]);

      while ($dao->fetch()) {
        $options = explode(CRM_Core_DAO::VALUE_SEPARATOR, $dao->recur_visibility);

        foreach ($options as $option) {
          if (!isset($recur_visibility_options[$option])) {
            $recur_visibility_options[$option] = [];
          }

          $recur_visibility_options[$option][$dao->price_field_id][] = $dao->price_field_value_id;
          $has_showhide_options = true;
        }
      }
    }

    // Bail if we do not have any special show/hide configurations
    if (!$has_showhide_options) {
      return;
    }

    Civi::resources()->addVars('pricesetvisibility', [
      'recur_visibility' => $recur_visibility_options,
    ]);

    Civi::resources()->addScriptFile('pricesetvisibility', 'pricesetvisibility.js');
  }

}
