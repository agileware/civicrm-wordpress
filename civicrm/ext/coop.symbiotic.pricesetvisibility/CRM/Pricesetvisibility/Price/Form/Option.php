<?php

use CRM_Pricesetvisibility_ExtensionUtil as E;

class CRM_Pricesetvisibility_Price_Form_Option {

  /**
   * @see pricesetvisibility_civicrm_buildForm().
   */
  public static function buildForm(&$form) {
    $form->add('select', 'recur_visibility', E::ts('Visibility - Recurring'), [
      'always' => E::ts('Always display'),
      'onetime' => E::ts('One-time donation'),
      'recurring' => E::ts('Recurring donation'),
      'day' => E::ts('Recurring donation - Daily'),
      'week' => E::ts('Recurring donation - Weekly'),
      'month' => E::ts('Recurring donation - Monthly'),
      'year' => E::ts('Recurring donation - Yearly'),
    ], FALSE, ['class' => 'crm-select2 huge', 'placeholder' => ts('- select -'), 'multiple' => TRUE]);

    $field_id = $form->getVar('_fid');
    $option_id = $form->getVar('_oid');

    // Set the default
    if ($option_id) {
      $dao = CRM_Core_DAO::executeQuery('SELECT * FROM civicrm_price_set_visibility WHERE price_field_id = %1 AND price_field_value_id = %2', [
        1 => [$field_id, 'Positive'],
        2 => [$option_id, 'Positive'],
      ]);

      if ($dao->fetch()) {
        $form->setDefaults([
          'recur_visibility' => explode(CRM_Core_DAO::VALUE_SEPARATOR, $dao->recur_visibility),
        ]);
      }
    }

    CRM_Core_Region::instance('form-bottom')->add([
      'template' => 'CRM/Pricesetvisibility/Price/Form/Option.tpl',
    ]);
  }

  /**
   * @see pricesetvisibility_civicrm_postProcess().
   */
  public static function postProcess(&$form) {
    $values = $form->exportValues();

    $field_id = $values['fieldId'];
    $option_id = $values['optionId'];
    $recur_visibility = implode(CRM_Core_DAO::VALUE_SEPARATOR, $values['recur_visibility']);

    // FIXME: what's the right way to get the newly created ID?
    if (!$option_id) {
     $option_id = CRM_Core_DAO::singleValueQuery('SELECT max(id) FROM civicrm_price_field_value');
    }

    // Check if it already exists
    $id = CRM_Core_DAO::singleValueQuery('SELECT id FROM civicrm_price_set_visibility WHERE price_field_id = %1 AND price_field_value_id = %2', [
      1 => [$field_id, 'Positive'],
      2 => [$option_id, 'Positive'],
    ]);

    if ($id) {
      CRM_Core_DAO::executeQuery('UPDATE civicrm_price_set_visibility SET recur_visibility = %1 WHERE id = %2', [
        1 => [$recur_visibility, 'String'],
        2 => [$id, 'Positive'],
      ]);
    }
    else {
      CRM_Core_DAO::executeQuery('INSERT INTO civicrm_price_set_visibility (price_field_id, price_field_value_id, recur_visibility) VALUES (%1, %2, %3)', [
        1 => [$field_id, 'Positive'],
        2 => [$option_id, 'Positive'],
        3 => [$recur_visibility, 'String'],
      ]);
    }
  }

}
