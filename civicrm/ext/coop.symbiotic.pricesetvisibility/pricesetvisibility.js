(function ($, _, ts) {
    var allUnits = ["day", "week", "month", "year"];

    CRM.pricesetvisibilityIsRecur = function () {
        var isRecur = false;

        // Check if the recurring checkbox is present
        var el_recur = document.getElementById("is_recur");

        if (el_recur === null) {
            return false;
        }

        if (el_recur.type == "hidden") {
            isRecur = (el_recur.value == 1);
        }
        else {
            isRecur = Boolean(el_recur.checked);
        }

        return isRecur;
    };

    /**
     * Returns the selected recurring frequency unit, if enabled. False otherwise.
     */
    CRM.pricesetvisibilityFreqUnit = function () {
        var el_freq = document.getElementById("frequency_unit");

        if (el_freq === null) {
            return false;
        }

        return el_freq.value;
    };

    CRM.pricesetvisibilityShowOptions = function (type) {
        $.each(CRM.vars.pricesetvisibility.recur_visibility[type], function (field_id, options) {
            $.each(options, function (key, option) {
                $("input[name=price_" + field_id + "][value=" + option + "]").closest(".price-set-row").show();
                // For compatibility with radiobuttons extension
                $("input[name=price_" + field_id + "][value=" + option + "]").closest(".crm-radio-wrapper").show();
            });
        });
    };

    CRM.pricesetvisibilityHideOptions = function (type) {
        $.each(CRM.vars.pricesetvisibility.recur_visibility[type], function (field_id, options) {
            $.each(options, function (key, option) {
                $("input[name=price_" + field_id + "][value=" + option + "]").closest(".price-set-row").hide();
                // For compatibility with radiobuttons extension
                $("input[name=price_" + field_id + "][value=" + option + "]").closest(".crm-radio-wrapper").hide();
            });
        });
    };

    CRM.pricesetvisibilityInitialState = function() {
      if (CRM.pricesetvisibilityIsRecur()) {
          CRM.pricesetvisibilityHideOptions("onetime");

          var freqUnit = CRM.pricesetvisibilityFreqUnit();

          if (freqUnit) {
              // First hide fields, then show.
              // Some fields might have two unit (ex: month,year), so they might get hidden then shown.
              $.each(allUnits, function (key, unit) {
                  if (unit != freqUnit) {
                      CRM.pricesetvisibilityHideOptions(unit);
                  }
              });

              $.each(allUnits, function (key, unit) {
                  if (unit == freqUnit) {
                      CRM.pricesetvisibilityShowOptions(unit);
                  }
              });
          }
      }
      else {
          CRM.pricesetvisibilityHideOptions("recurring");

          $.each(allUnits, function (key, unit) {
              CRM.pricesetvisibilityHideOptions(unit);
          });

          // An option might be "onetime and monthly", for example
          CRM.pricesetvisibilityShowOptions("onetime");
      }
    };

    CRM.pricesetvisibilityInitialState();

    // Update the options when the is_recur value changes
    CRM.$("#is_recur").change(function () {
        var isRecur = CRM.pricesetvisibilityIsRecur();

        if (isRecur) {
            // We will display unit-specific items in the function below
            // because the value of the is_recur might be updated before frequency_unit.
            CRM.pricesetvisibilityHideOptions("onetime");
            CRM.pricesetvisibilityShowOptions("recurring");
            $.each(allUnits, function (key, unit) {
                CRM.pricesetvisibilityShowOptions(unit);
            });
        }
        else {
            CRM.pricesetvisibilityHideOptions("recurring");
            $.each(allUnits, function (key, unit) {
                CRM.pricesetvisibilityHideOptions(unit);
            });
            CRM.pricesetvisibilityShowOptions("onetime");
        }
    });

    CRM.$("#frequency_unit").change(function () {
        var isRecur = CRM.pricesetvisibilityIsRecur();
        var freqUnit = CRM.pricesetvisibilityFreqUnit();

        if (isRecur && freqUnit) {
            // Hide all other units
            $.each(allUnits, function (key, unit) {
                if (unit != freqUnit) {
                    CRM.pricesetvisibilityHideOptions(unit);
                }
            });

            CRM.pricesetvisibilityShowOptions(freqUnit);
        }
    });

})(CRM.$, CRM._, CRM.ts("pricesetvisibility"));
