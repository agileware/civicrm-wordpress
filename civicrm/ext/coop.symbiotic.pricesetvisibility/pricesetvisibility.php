<?php

require_once 'pricesetvisibility.civix.php';
use CRM_Pricesetvisibility_ExtensionUtil as E;

/**
 * Implements hook_civicrm_config().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_config/ 
 */
function pricesetvisibility_civicrm_config(&$config) {
  _pricesetvisibility_civix_civicrm_config($config);
}

/**
 * Implements hook_civicrm_install().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_install
 */
function pricesetvisibility_civicrm_install() {
  _pricesetvisibility_civix_civicrm_install();
}

/**
 * Implements hook_civicrm_enable().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_enable
 */
function pricesetvisibility_civicrm_enable() {
  _pricesetvisibility_civix_civicrm_enable();
}

/**
 * Implements hook_civicrm_buildForm().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_buildForm
 */
function pricesetvisibility_civicrm_buildForm($formName, &$form) {
  if ($formName == 'CRM_Contribute_Form_Contribution_Main' ||
      $formName === 'CRM_Event_Form_Registration_Register') {
    CRM_Pricesetvisibility_Contribute_Form_Contribution_Main::buildForm($form);
  }
  elseif ($formName == 'CRM_Price_Form_Option') {
    CRM_Pricesetvisibility_Price_Form_Option::buildForm($form);
  }
}

/**
 * Implements hook_civicrm_postProcess().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_postProcess
 */
function pricesetvisibility_civicrm_postProcess($formName, &$form) {
  if ($formName == 'CRM_Price_Form_Option') {
    CRM_Pricesetvisibility_Price_Form_Option::postProcess($form);
  }
}
