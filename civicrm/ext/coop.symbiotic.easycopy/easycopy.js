(function($, _, ts) {

  $(window).on('crmLoad', function(event) {
    // Email
    $('#crm-email-content .crm-contact_email', event.target).each(function() {
      $(this).parent().append('<i title="' + ts('Copy to clipboard') + '" class="fa fa-clipboard crm-easycopy-button crm-easycopy-email" aria-hidden="true"></i>');
    });

    // Phone
    $('#crm-phone-content .crm-contact_phone', event.target).each(function() {
      $(this).parent().parent().append('<i title="' + ts('Copy to clipboard') + '" class="fa fa-clipboard crm-easycopy-button crm-easycopy-phone" aria-hidden="true"></i>');
    });

    // Address
    $('.crm-address-block > .address', event.target).each(function() {
      // Check if there is an address, or if it's the "add address" link
      if ($(this).find('.crm-summary-row .crm-content').children().size() > 0) {
        $(this).append('<i title="' + ts('Copy to clipboard') + '" class="fa fa-clipboard crm-easycopy-button crm-easycopy-address" aria-hidden="true"></i>');
      }
    });

    // Must be mouseup to stopPropagation from crmFormInline
    // c.f. templates/CRM/Contact/Page/View/Summary.js
    $('.crm-easycopy-email', event.target).on('mouseup', function(event) {
      event.preventDefault();
      event.stopPropagation();
      $(this).addClass('crm-easycopy-animated');

      $(this).parent().find('a.crm-popup').each(function() {
        var txt = $(this).text().trim();
        navigator.clipboard.writeText(txt);
      });

      return false;
    });

    $('.crm-easycopy-phone', event.target).on('mouseup', function(event) {
      event.preventDefault();
      event.stopPropagation();
      $(this).addClass('crm-easycopy-animated');

      $(this).parent().find('.crm-contact_phone').each(function() {
        var txt = $(this).text().trim();
        navigator.clipboard.writeText(txt);
      });

      return false;
    });

    $('.crm-easycopy-address', event.target).on('mouseup', function(event) {
      event.preventDefault();
      event.stopPropagation();
      $(this).addClass('crm-easycopy-animated');

      $(this).parent().find('.crm-content').each(function() {
        var txt = $(this).text().trim();
        navigator.clipboard.writeText(txt);
      });

      return false;
    });

    $('.crm-easycopy-button', event.target).on('animationend', function(event) {
      $(this).removeClass('crm-easycopy-animated');
    });
  });

})(CRM.$, CRM._, CRM.ts('easycopy'));
