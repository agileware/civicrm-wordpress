# Easy Copy

![Screenshot](/images/screenshot.png)

Easily copy contact record information (phone, email, address) to clipboard.

Currently this applies mainly to the contact record. Clipboard icons will be displayed
next to each address, phone and email, so that they can quickly be copied elsewhere
(instead of either clicking the inline edit, or tediously trying to select/copy before
inline-edit is triggered).

The extension is licensed under [AGPL-3.0](LICENSE.txt).

## Requirements

* PHP v7.2+
* CiviCRM 5.latest

The extension should work on most configurations. It has mostly been tested on
[The Island](https://civicrm.org/extensions/island-theme) theme and is
regularly tested with the Contact Layout Summary Editor extension.

## Installation

Install as a regular CiviCRM extension.

## Usage

Once enabled, clipboard icons are displayed on the "View Contact" screen.

Click the icon to copy the information.

## Support

Please post bug reports in the issue tracker of this project:  
https://lab.civicrm.org/extensions/easycopy/issues

Commercial support is available through Coop SymbioTIC:  
https://www.symbiotic.coop/en

Coop Symbiotic is a worker-owned co-operative based in Canada. We have a strong
experience working with non-profits and CiviCRM. We provide affordable, fast,
turn-key hosting with regular upgrades and proactive monitoring, as well as
custom development and training.
