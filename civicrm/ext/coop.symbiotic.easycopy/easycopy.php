<?php

require_once 'easycopy.civix.php';
use CRM_Easycopy_ExtensionUtil as E;

/**
 * Implements hook_civicrm_config().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_config
 */
function easycopy_civicrm_config(&$config) {
  _easycopy_civix_civicrm_config($config);
}

/**
 * Implements hook_civicrm_install().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_install
 */
function easycopy_civicrm_install() {
  _easycopy_civix_civicrm_install();
}

/**
 * Implements hook_civicrm_enable().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_enable
 */
function easycopy_civicrm_enable() {
  _easycopy_civix_civicrm_enable();
}

/**
 * Implements hook_civicrm_pageRun().
 */
function easycopy_civicrm_pageRun(&$page) {
  $pageName = get_class($page);

  if ($pageName == 'CRM_Contact_Page_View_Summary') {
    Civi::resources()->addScriptFile('easycopy', 'easycopy.js');
    Civi::resources()->addStyleFile('easycopy', 'easycopy.css');
  }
}
