{literal}
<style type="text/css">
  #resp-table {
    width: 100%;
    display: table;
  }
  #resp-table-header{
    display: table-header-group;
    background-color: lightgray;
    font-weight: bold;
  }
  .table-header-cell{
    display: table-cell;
    padding: 10px;
    text-align: justify;
    border-bottom: 1px solid black;
  }
  #resp-table-body{
    display: table-row-group;
  }
  .resp-table-row{
    display: table-row;
  }
  .table-body-cell{
    display: table-cell;
    padding: 10px;
  }
  button.emlexport {
    a:link,
    a:visited {
      color: white !important;
    }
    a:hover {
      text-decoration: none;
    }
  }
</style>
{/literal}
{if $warning != ''}
  {$warning}
{/if}

{crmScope extensionKey='com.ixiam.modules.mailreader'}
  <fieldset>
   <div class="crm-element"><h3>{$total_rows} {ts}records have been found in database{/ts}</h3></div>
   <div class="crm-element">{$form.limit_to.label}{$form.limit_to.html} {ts}results on page{/ts}</div>
   <div class="crm-element">{$form.show_last.label}{$form.show_last.html}</div>
  </fieldset>
  <div class="crm-submit-buttons">{include file="CRM/common/formButtons.tpl" location="top"}</div>
  <fieldset>
  <legend>{ts}Listing of stored emails in the database{/ts}</legend>
  <div class="form-layout" id="resp-table">
    <div id="resp-table-header" class="columnheader">
      <div class="table-header-cell">{ts}Job ID{/ts}</div>
      <div class="table-header-cell">{ts}Recipient Email{/ts}</div>
      <div class="table-header-cell">{ts}Headers{/ts}</div>
      <div class="table-header-cell">{ts}Body{/ts}</div>
      <div class="table-header-cell">{ts}Attachments{/ts}</div>
      <div class="table-header-cell">{ts}Added at{/ts}</div>
      <div class="table-header-cell">{ts}Removed at{/ts}</div>
      <div class="table-header-cell">{ts}Action{/ts}</div>
    </div>

    <div id="resp-table-body">
      {foreach from=$records item=row}
        <div class="resp-table-row">
          <div class="table-body-cell">{$row.job_id}</div>
          <div class="table-body-cell">{$row.recipient_email}</div>
          <div class="table-body-cell">
            <div class="crm-accordion-wrapper collapsed">
              <div class="crm-accordion-header">{ts}Message Headers - Click to expand{/ts}</div>
              <div class="crm-accordion-body">
                <div class="">{$row.headers|htmlize}</div>
              </div>
            </div>
          </div>
          <div class="table-body-cell">
            <div class="crm-accordion-wrapper collapsed">
              <div class="crm-accordion-header">{ts}Message Body - Click to expand{/ts}</div>
              <div class="crm-accordion-body">
                <div class="">{$row.body|htmlize}</div>
              </div>
            </div>
          </div>
          <div class="table-body-cell">{foreach from=$row.attachments item=attachment}<div>{$attachment}</div>{/foreach}</div>
          <div class="table-body-cell">{$row.added_at}</div>
          <div class="table-body-cell">{$row.removed_at}</div>
          <button class="table-body-cell emlexport crm-button crm-button-type-submit"><i aria-hidden="true" class="crm-i fa-archive"></i><a href="{crmURL p='civicrm/admin/mailreader' q="exportid=`$row.id`"}" title="Export to EML format"> Export ID #{$row.id}</a></button>
        </div>
      {/foreach}
    </div>
  </div>

  <div class="description">
    Note: Please be aware that the export function is still experimental
  </div>

  {* FOOTER *}
  <div class="crm-submit-buttons">
   {include file="CRM/common/formButtons.tpl" location="bottom"}
  </div>

  {literal}
    <script type="text/javascript">
      CRM.$(function ($) {
        $('#Mailreader .crm-submit-buttons #_qf_Mailreader_submit_delete_all-top').click(function (event) {
          if (confirm(ts("You are about to delete ALL entries of the table 'civicrm_mailing_spool'? This action cannot be undone! If you agree, click OK to continue"))) {
            $('#Mailreader .crm-submit-buttons #_qf_Mailreader_submit_delete_all-top').submit();
          } else {
            event.preventDefault();
          }
        });
      });
    </script>
  {/literal}
{/crmScope}
