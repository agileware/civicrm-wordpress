# Changelog

## 1.3.2

* Upgrade civix to 23.02
* Minor bugfix on the confirmation page when deleting all records

## 1.3.1

* Minor visual modifications
* Removes repeating warning log entry if a function is missing. Implements #3

## 1.3.0

* Smarty tweaks and list file attachments (credits to @bgm)

## 1.2.2

 * Adds composer file
 * Always show mail in database

## 1.2.1

  * Fixes wrong naming output while exporting EML
  * Fixes typo in release date inside info.xml

## 1.2.0

  * Regenerate "civix" code for PHP7.4 support.
  * Adds (experimental) support for email export to EML

## 1.1.3

  * Minor documentation/info.xml update

## 1.1.2

  * Resolved bug in menu paths.

## 1.1.1

  * Proper menu structure

## 1.1

  * Change of menu and URL path
  * Proper URL redirection to CiviCRM SMTP settings (civicrm/admin/setting/smtp)
  * Shows count of records in DB
  * Better code formatting

## 1.0

  * Initial release
