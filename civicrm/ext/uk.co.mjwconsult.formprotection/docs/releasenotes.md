## Information

Releases use the following numbering system:
**{major}.{minor}.{incremental}**

* major: Major refactoring or rewrite - make sure you read and test very carefully!
* minor: Breaking change in some circumstances, or a new feature. Read carefully and make sure you understand the impact of the change.
* incremental: A "safe" change / improvement. Should *always* be safe to upgrade.

* **[BC]**: Items marked with [BC] indicate a breaking change that will require updates to your code if you are using that code in your extension.

## Release 1.6.1 (2024-01-19)

* [!20](https://lab.civicrm.org/extensions/formprotection/-/merge_requests/20) Removes invalid URL on settings page that is no longer valid, fixes cv on D9+.

## Release 1.6 (2024-01-18)

* PHP8.2 compatibility (re-run civix).
* [!18](https://lab.civicrm.org/extensions/formprotection/-/merge_requests/18) Avoid inserting honey pot field within price fields.
* [!19](https://lab.civicrm.org/extensions/formprotection/-/merge_requests/19) configurable recaptcha threshold.
* [!16](https://lab.civicrm.org/extensions/formprotection/-/merge_requests/16) Initialize token var.

## Release 1.5.1 (2023-04-17)

* [!15](https://lab.civicrm.org/extensions/formprotection/-/merge_requests/15) Don't use floodcontrol on unsubscribe/optout/resubscribe.

## Release 1.5 (2023-04-12)

* [!10](https://lab.civicrm.org/extensions/formprotection/-/merge_requests/10) Add ReCAPTCHAv2 and v3 to formbuilder. Note v3 not working properly yet.
* Add score to firewall logging, where available.
* [!13](https://lab.civicrm.org/extensions/formprotection/-/merge_requests/13) Fix user feedback on reCAPTCHA validation failure.
* [!12](https://lab.civicrm.org/extensions/formprotection/-/merge_requests/12)Fix reCAPTCHA v3 validation when token is in $_POST['captcha'].
* [!11](https://lab.civicrm.org/extensions/formprotection/-/merge_requests/11) Fix type error on recaptcha validation.
* Fix crash when CiviEvent not enabled.

## Release 1.4.1 (2023-01-12)

* Fix issue with retrieving context information.
* [!8](https://lab.civicrm.org/extensions/formprotection/-/merge_requests/8) Enable recaptcha v2 to work with stripe.

## Release 1.4.0 (2022-12-09)

* Fix [#9](https://lab.civicrm.org/extensions/formprotection/-/issues/9) ReCAPTCHA validation issues v2 and v3.
* Fix [#10](https://lab.civicrm.org/extensions/formprotection/-/issues/10) Remove unsupported recaptcha options.
* [#8](https://lab.civicrm.org/extensions/formprotection/-/issues/8) Add optional integration with Firewall extension.
* Use a more ambiguous but better-worded error for honeypot.

## Release 1.3.1 (2022-11-24)

* Add another form class.

## Release 1.3 (2022-11-22)
**Breaking Changes: You need to reconfigure your settings and select the forms or *ALL* to control if honeypot/reCAPTCHA is loaded.**

* Add Stripe authorize listener for ReCAPTCHA (used to authorize Stripe API events):
  *Listens for `civi.stripe.authorize` event triggered by API calls and uses reCAPTCHA verification to authorize the API call.*
* Remove force_recaptcha setting - Allow to enable reCAPTCHA on specific forms and specific event/contribution pages:
  *There is no automatic upgrader - you will need to reconfigure your reCAPTCHA settings*.
* Add check for recommended firewall extension.

## Release 1.2 (2022-11-16)

* Add separate ReCAPTCHA validate function.
* Refactor ReCAPTCHA and add javascript function to support reloading (`reloadReCAPTCHA()` and `getReCAPTCHAToken()` are now available in the browser).

## Release 1.1.2 (2022-11-13)

* Fix [#3](https://lab.civicrm.org/extensions/formprotection/-/issues/3) Extension does not uninstall safely.

## Release 1.1.1 (2022-09-24)

* Fix [#5](https://lab.civicrm.org/extensions/formprotection/-/issues/5) formprotection fails silently, reloads contribution page (failed to load composer libraries).

## Release 1.1 (2022-08-19)

* Support ReCAPTCHA (v3 + v2 checkbox).

## Release 1.0.1

* Fix [#2](https://lab.civicrm.org/extensions/formprotection/-/issues/2) Honeypot input sometimes visible (with broken HTML).

## Release 1.0

* Initial release implementing Honeypot and flood control for forms.
