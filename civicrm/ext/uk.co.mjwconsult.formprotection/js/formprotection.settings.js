(function($, ts) {
  function setupForm() {
    $('<tr><td colspan="2"><h3>Flood Control</h3></td></tr>').insertBefore($('tr.crm--form-block-formprotection_enable_floodcontrol'));
    $('<tr><td colspan="2"><h3>Honeypot</h3></td></tr>').insertBefore($('tr.crm--form-block-formprotection_enable_honeypot'));
    $('<tr><td colspan="2"><h3>ReCAPTCHA</h3></td></tr>').insertBefore($('tr.crm--form-block-formprotection_enable_recaptcha'));
  }

  function showHideElements() {
    if ($('#formprotection_enable_floodcontrol_formprotection_enable_floodcontrol').prop('checked')) {
      $('[class^=crm--form-block-formprotection_floodcontrol]').show();
    }
    else {
      $('[class^=crm--form-block-formprotection_floodcontrol]').hide();
    }

    if ($('#formprotection_enable_honeypot_formprotection_enable_honeypot').prop('checked')) {
      $('[class^=crm--form-block-formprotection_honeypot]').show();
    }
    else {
      $('[class^=crm--form-block-formprotection_honeypot]').hide();
    }

    if (document.getElementById('formprotection_enable_recaptcha').value === 'none') {
      $('[class^=crm--form-block-formprotection_recaptcha]').hide();
    }
    else {
      $('[class^=crm--form-block-formprotection_recaptcha]').show();
    }
  }

  document.addEventListener('DOMContentLoaded', function() {
    setupForm();
    showHideElements();
  });

  $('#formprotection_enable_floodcontrol_formprotection_enable_floodcontrol').on('change', function() {
    showHideElements();
  });

  $('#formprotection_enable_honeypot_formprotection_enable_honeypot').on('change', function() {
    showHideElements();
  });

  $('#formprotection_enable_recaptcha').on('change', function() {
    showHideElements();
  });

}(CRM.$, CRM.ts('formprotection')));
