<?php
// Module for recaptcha Afform element
return [
  'js' => [
    'ang/fayzaan.gRecaptcha.v3/angularjs-google-recaptcha-v3.js',
  ],
  'partials' => [],
  'css' => [],
  'basePages' => [],
  'requires' => [],
  'bundles' => [],
  'exports' => [],
];
