<?php
// Module for recaptcha Afform element
return [
  'js' => [
    'ang/crmRecaptcha3.module.js',
    'ang/crmRecaptcha3/*.js',
  ],
  'partials' => [
    'ang/crmRecaptcha3',
  ],
  'css' => [],
  'basePages' => [],
  'requires' => ['fayzaan.gRecaptcha.v3'],
  'settingsFactory' => ['Civi\Formprotection\AfformReCaptcha', 'getRecaptchaSettings'],
  'bundles' => [],
  'exports' => [
    // This triggers Afform to automatically require this module on forms using recaptcha
    'crm-recaptcha3' => 'E',
  ],
];
