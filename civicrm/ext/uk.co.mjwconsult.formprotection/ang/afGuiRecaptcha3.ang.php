<?php
// Module for editing ReCaptcha element in the AfformAdmin GUI
return [
  'js' => [
    'ang/afGuiRecaptcha3.module.js',
    'ang/afGuiRecaptcha3/*.js',
  ],
  'partials' => [
    'ang/afGuiRecaptcha3',
  ],
  'css' => ['ang/css/afGuiRecaptcha.css'],
  // Ensure module is loaded on the afform_admin GUI page
  'basePages' => ['civicrm/admin/afform'],
  'requires' => [],
  'bundles' => [],
  'exports' => [],
];
