<?php

namespace Civi\Formprotection;

use Civi\Angular\ChangeSet;
use CRM_Formprotection_ExtensionUtil as E;
use Civi\Afform\AHQ;
use Civi\Afform\Event\AfformValidateEvent;
use Civi\Core\Event\GenericHookEvent;
use Civi\Core\Service\AutoService;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Provides ReCaptcha validation element to Afform
 * @internal
 * @service
 */
class AfformReCaptcha extends AutoService implements EventSubscriberInterface {

  /**
   * @return array
   */
  public static function getSubscribedEvents() {
    return [
      'civi.afform_admin.metadata' => ['onAfformGetMetadata'],
      'hook_civicrm_alterAngular' => ['alterAngular'],
      'civi.afform.validate' => ['onAfformValidate'],
    ];
  }

  public static function onAfformGetMetadata(GenericHookEvent $event) {
    $recaptchaType = \Civi::settings()->get('formprotection_enable_recaptcha');
    switch ($recaptchaType) {
      /*
       * We can't enable ReCAPTCHA v3 because of https://lab.civicrm.org/extensions/formprotection/-/merge_requests/10#note_87351
      case 'v3':
        $event->elements['recaptcha3'] = [
          'title' => E::ts('ReCaptcha v3'),
          'afform_type' => ['form'],
          'directive' => 'crm-recaptcha3',
          'admin_tpl' => '~/afGuiRecaptcha3/afGuiRecaptcha3.html',
          'element' => [
            '#tag' => 'crm-recaptcha3',
          ],
        ];
        break;
      */

      case 'v2checkbox':
        $event->elements['recaptcha2'] = [
          'title' => E::ts('ReCaptcha v2'),
          'afform_type' => ['form'],
          'directive' => 'crm-recaptcha2',
          'admin_tpl' => '~/afGuiRecaptcha2/afGuiRecaptcha2.html',
          'element' => [
            '#tag' => 'crm-recaptcha2',
          ],
        ];
        break;
    }
  }

  public static function alterAngular(GenericHookEvent $event) {
    $changeSet = ChangeSet::create('reCaptcha')
      ->alterHtml(';\\.aff\\.html$;', function($doc, $path) {
        // Add publicKey to recaptcha elements
        foreach (pq('crm-recaptcha2') as $captcha) {
          $recaptchaPublicKey = \Civi::settings()->get('formprotection_recaptcha_publickey');
          pq($captcha)->attr('recaptchakey', $recaptchaPublicKey ?: 'foo');
        }
        foreach (pq('crm-recaptcha3') as $captcha) {
          $recaptchaPublicKey = \Civi::settings()->get('formprotection_recaptcha_publickey');
          pq($captcha)->attr('recaptchakey', $recaptchaPublicKey ?: 'foo');
        }
      });
    $event->angular->add($changeSet);
  }

  public static function onAfformValidate(AfformValidateEvent $event) {
    $layout = AHQ::makeRoot($event->getAfform()['layout']);
    if (AHQ::getTags($layout, 'crm-recaptcha2') || AHQ::getTags($layout, 'crm-recaptcha3')) {
      $response = $event->getApiRequest()->getValues()['extra']['recaptcha'] ?? NULL;
      $errors = [];
      if (!isset($response) || !Recaptcha::validate($response, $errors)) {
        if (empty($errors)) {
          $errors = [E::ts('Error validating form')];
        }
        $event->setError(implode(', ', $errors));
      }
    }
  }

  public static function getRecaptchaSettings() {
    return [
      'recaptchakey' => \Civi::settings()->get('formprotection_recaptcha_publickey'),
    ];
  }

}
