<?php
/*
 +--------------------------------------------------------------------+
 | Copyright CiviCRM LLC. All rights reserved.                        |
 |                                                                    |
 | This work is published under the GNU AGPLv3 license with some      |
 | permitted exceptions and without any warranty. For full license    |
 | and copyright information, see https://civicrm.org/licensing       |
 +--------------------------------------------------------------------+
 */

namespace Civi\Formprotection;

use CRM_Formprotection_ExtensionUtil as E;

class Recaptcha {

  // To be implemented: https://github.com/google/recaptcha

  /**
   * @var Forms
   */
  private $forms;

  /**
   * @var string
   */
  private static $apiURL = 'https://www.google.com/recaptcha/api.js';

  public function __construct() {
    $this->forms = new \Civi\Formprotection\Forms();
  }

  /**
   * This function is used by API / AJAX requests
   * So we won't have a $form object
   *
   * @return bool
   */
  public function isRecaptchaActiveForRequest() {
    $context = [];

    // Content-Type: text/html with a params data element, or Content-Type: application/json with json data element.
    $json = \CRM_Utils_Request::retrieveValue('params', 'Json', NULL, FALSE, 'POST');
    if (!$json) {
      $postRequest['json'] = NULL;
      parse_str(file_get_contents('php://input'), $postRequest);
      $json = $postRequest['json'];
    }

    if ($json) {
      $json = json_decode($json, TRUE);
      if (array_key_exists('csrfToken', $json)) {
        $context = \CRM_Core_Session::singleton()->get('csrf.' . $json['csrfToken'], 'civi.firewall');
      }
    }

    // Check the formprotection settings to see if we actually enabled ReCAPTCHA.
    if (!$this->forms->isFormProtectionActive('recaptcha', $context ?? [])) {
      return FALSE;
    }

    if (!$this->shouldAddCaptchaToForm($context)) {
      return FALSE;
    }

    return TRUE;
  }

  /**
   * For forms with IDs (eg. Contribution pages, Event registration forms) we can filter by ID
   *   and only activate for some IDs. Check if active for ID here.
   *
   * @param array $context
   *
   * @return bool
   */
  private function isActiveForFormID(array $context): bool {
    $formName = $context['formName'] ?? '';
    $formID = $context['formID'] ?? NULL;
    switch ($formName) {
      case 'CRM_Contribute_Form_Contribution_Main':
      case 'CRM_Financial_Form_Payment':
        $contributionPageIDs = \Civi::settings()->get('formprotection_recaptcha_contribution_form_ids');
        if (!empty($contributionPageIDs)) {
          if (!in_array($formID, $contributionPageIDs)) {
            return FALSE;
          }
        }
        break;

      case 'CRM_Event_Form_Registration_Register':
        $eventIDs = \Civi::settings()->get('formprotection_recaptcha_event_form_ids');
        if (!empty($eventIDs)) {
          if (!in_array($formID, $eventIDs)) {
            return FALSE;
          }
        }

        // Check for presence of "skip" button which means we're on Additional Participant form.
        // We only show reCAPTCHA on the primary form.
        $button = substr($context['button'] ?? '', -4);
        if ($button === 'skip') {
          return FALSE;
        }
        break;
    }

    // Now check if we should enable on test forms?
    if (\Civi::settings()->get('formprotection_enable_test')) {
      return TRUE;
    }
    if (!empty($context['test'])) {
      \Civi::log('formprotection')->debug('formprotection: ignoring form in test mode.');
      return FALSE;
    }
    return TRUE;
  }

  /**
   * @param \CRM_Core_Form $form
   */
  public function buildForm(\CRM_Core_Form &$form) {
    $context = $this->forms::getContextFromQuickform($form);
    if (!$this->forms->isFormProtectionActive('recaptcha', $context)) {
      return;
    }

    if (!$this->isActiveForFormID($context)) {
      return;
    }

    if ($this->shouldAddCaptchaToForm($context)) {
      \CRM_Utils_ReCAPTCHA::enableCaptchaOnForm($form);
    }
  }

  /**
   * @param \CRM_Core_Form $form
   * @param array $fields
   * @param array $errors
   */
  public function validateForm(\CRM_Core_Form &$form, array $fields, array &$errors) {
    $context = $this->forms::getContextFromQuickform($form);

    if (!$this->forms->isFormProtectionActive('recaptcha', $context)) {
      return;
    }

    if (!$this->shouldAddCaptchaToForm($context)) {
      return;
    }

    $token = '';
    if (!empty($_POST['g-recaptcha-token'])) {
      $token = $_POST['g-recaptcha-token'];
    }
    else if (!empty($_POST['g-recaptcha-response'])) {
      $token = $_POST['g-recaptcha-response'];
    }
    else if (!empty($_POST['captcha'])) {
      $token = $_POST['captcha'];
    }

    // If this form has a Stripe payment processor, don't check twice, it's
    // already required for the payment intent.
    $paymentProcessor = $form->getVar('_paymentProcessor');
    if ($paymentProcessor && $paymentProcessor['payment_processor_type'] === 'Stripe') {
      return;
    }

    self::validate($token, $errors);
  }

  /**
   * Validate the ReCAPTCHA
   *
   * @param string $token
   * @param array $errors
   *
   * @return bool
   */
  public static function validate(string $token, array &$errors): bool {
    switch (\Civi::settings()->get('formprotection_enable_recaptcha')) {
      case 'v3':
        // reCAPTCHAv3
        $recaptcha = new \ReCaptcha\ReCaptcha(\Civi::settings()->get('formprotection_recaptcha_privatekey'));
        $resp = $recaptcha
          ->setScoreThreshold(\Civi::settings()->get('formprotection_recaptcha_threshold'))
          ->verify($token, Utils::getIPAddress());
        if (!$resp->isSuccess()) {
          // If Firewall is enabled, log it there
          if (\Civi::settings()->get('formprotection_enable_firewall_integration') && class_exists('\Civi\Firewall\Event\FormProtectionEvent')) {
            $reason = 'recaptcha: v3 failed';
            if (!empty($resp->getScore())) {
              $reason .= ' | score ' . $resp->getScore();
            }
            \Civi\Firewall\Event\FormProtectionEvent::trigger(Utils::getIPAddress(), $reason);
          }
          $score = $resp->getScore();

          $errors['qfKey'] = E::ts('Your reCAPTCHA validation was unsuccessful. Please try again, or contact us if the problem persists.');
        }
        return $resp->isSuccess();

      case 'v2checkbox':
        // reCAPTCHAv2
        $recaptcha = new \ReCaptcha\ReCaptcha(\Civi::settings()->get('formprotection_recaptcha_privatekey'));
        $resp = $recaptcha->verify($token, Utils::getIPAddress());
        if (!$resp->isSuccess()) {

          // If Firewall is enabled, log it there
          if (\Civi::settings()->get('formprotection_enable_firewall_integration') && class_exists('\Civi\Firewall\Event\FormProtectionEvent')) {
            \Civi\Firewall\Event\FormProtectionEvent::trigger(Utils::getIPAddress(), 'recaptcha: v2 failed');
          }

          $errors['recaptcha'] = $resp->getErrorCodes();
        }
        return $resp->isSuccess();
    }

    return FALSE;
  }

  /**
   * Gets the challenge v3 HTML (javascript and non-javascript version).
   * This is called from the browser, and the resulting reCAPTCHA HTML widget
   * is embedded within the HTML form it was called from.
   * @param string $pubkey A public key for reCAPTCHA
   *
   * @return string - The HTML to be embedded in the user's form.
   */
  public static function getV3HTML(string $pubkey) {
    if ($pubkey == NULL || $pubkey == '') {
      throw new \CRM_Core_Exception('To use reCAPTCHA you must get an API key from <a href="https://www.google.com/recaptcha/admin/create">https://www.google.com/recaptcha/admin/create</a>');
    }

    return '
  <input type="hidden" id="g-recaptcha-token" name="g-recaptcha-token">
  <script src="' . self::$apiURL . '?render=' . $pubkey . '"></script>
  <script type="text/javascript">
  function reloadReCAPTCHA() {
    let self = this;
    return new Promise((resolve, reject) => {
        grecaptcha.ready(function() {
            grecaptcha.execute(\'' .  $pubkey . '\', {action: "validate_captcha"}).then(function(token) {
              document.getElementById("g-recaptcha-token").value = token;
              resolve();
            });
       });
    });
  }
  function getReCAPTCHAToken() {
    var captchaElement = document.getElementById("g-recaptcha-token");
    return captchaElement ? captchaElement.value : "";
  }
  grecaptcha.ready(function() {
    reloadReCAPTCHA();
  });
  setInterval(function() {
  grecaptcha.ready(function() {
    reloadReCAPTCHA();
  });
}, 60 * 1000);
  </script>
  ';
  }

  /**
   * Gets the challenge v2 HTML (javascript and non-javascript version).
   * This is called from the browser, and the resulting reCAPTCHA HTML widget
   * is embedded within the HTML form it was called from.
   * @param string $pubkey A public key for reCAPTCHA
   *
   * @return string - The HTML to be embedded in the user's form.
   */
  public static function getV2HTML(string $pubkey) {
    if ($pubkey == NULL || $pubkey == '') {
      throw new \CRM_Core_Exception('To use reCAPTCHA you must get an API key from <a href="https://www.google.com/recaptcha/admin/create">https://www.google.com/recaptcha/admin/create</a>');
    }

    return '
  <div class="g-recaptcha" data-sitekey="' . $pubkey . '"></div>
  <script type="text/javascript" src="' . self::$apiURL . '"></script>
  <script type="text/javascript">
  function reloadReCAPTCHA() {
    // No-op. recaptcha v2 reloads itself when necessary.
    return new Promise((resolve,reject) => {
      resolve();
    });
  }

  function getReCAPTCHAToken() {
    var captchaElement = document.getElementById("g-recaptcha-response");
    return captchaElement ? captchaElement.value : "";
  }
  </script>
  ';
  }

  /**
   * Check the standard conditions for adding a ReCAPTCHA to the form
   * Return TRUE if we should enable ReCAPTCHA
   *
   * @return bool
   */
  public static function checkAreStandardConditionsMetForEnableReCAPTCHA(): bool {
    if (!\CRM_Core_Session::getLoggedInContactID()) {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Check if reCaptcha settings is available to add on form.
   */
  public static function hasSettingsAvailable() {
    return (bool) \Civi::settings()->get('formprotection_recaptcha_publickey');
  }

  /**
   * This checks the form criteria to see if reCAPTCHA should be added and then it adds it to the form if required.
   *
   * @param array $context
   *
   * @throws \API_Exception
   * @throws \Civi\API\Exception\UnauthorizedException
   */
  private function shouldAddCaptchaToForm(array $context): bool {
    $addCaptcha = FALSE;
    $ufGroupIDs = [];

    // Standard conditions for enabling ReCAPTCHA (eg. don't enable if contact is logged in).
    if (!self::checkAreStandardConditionsMetForEnableReCAPTCHA()) {
      return FALSE;
    }

    switch ($context['formName']) {
      case 'CRM_Contribute_Form_Contribution_Main':
      case 'CRM_Event_Form_Registration_Register':
      case 'CRM_Financial_Form_Payment':
        if ($this->isActiveForFormID($context)) {
          $addCaptcha = TRUE;
        }
        break;

      case 'CRM_Mailing_Form_Subscribe':
      case 'CRM_Event_Cart_Form_Checkout_Payment':
        $addCaptcha = TRUE;
        break;

      case 'CRM_PCP_Form_PCPAccount':
      case 'CRM_Campaign_Form_Petition_Signature':
        $ufGroupIDs = $context['profileIDs'] ?? [];
        break;

      case 'CRM_Profile_Form_Edit':
        // add captcha only for create mode.
        if (!empty($context['profileCreateMode'])) {
          $ufGroupIDs = $context['profileIDs'] ?? [];
        }
        break;

    }

    if (!empty($ufGroupIDs) && empty($addCaptcha)) {
      foreach ($ufGroupIDs as $ufGroupID) {
        $addCaptcha = \Civi\Api4\UFGroup::get(FALSE)
          ->addWhere('id', '=', $ufGroupID)
          ->execute()
          ->first()['add_captcha'];
        if ($addCaptcha) {
          break;
        }
      }
    }

    return $addCaptcha;
  }

  public static function validateRecaptchaThreshold(float &$value) : bool {
    if ($value < 0) {
      $value = 0;
    }
    if ($value > 1) {
      $value = 1;
    }
    return TRUE;
  }
}
