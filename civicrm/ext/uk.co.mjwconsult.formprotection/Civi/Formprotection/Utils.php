<?php
/*
 +--------------------------------------------------------------------+
 | Copyright CiviCRM LLC. All rights reserved.                        |
 |                                                                    |
 | This work is published under the GNU AGPLv3 license with some      |
 | permitted exceptions and without any warranty. For full license    |
 | and copyright information, see https://civicrm.org/licensing       |
 +--------------------------------------------------------------------+
 */

namespace Civi\Formprotection;

use CRM_Formprotection_ExtensionUtil as E;

class Utils {

  /**
   * @return string|null
   */
  public static function getIPAddress(): string {
    if (method_exists('\Civi\Firewall\Firewall', 'getIPAddress')) {
      return \Civi\Firewall\Firewall::getIPAddress();
    }
    else {
      return \CRM_Utils_System::ipAddress();
    }
  }
}
