<?php return array(
    'root' => array(
        'name' => 'civicrm/formprotection',
        'pretty_version' => 'dev-main',
        'version' => 'dev-main',
        'reference' => '306fc923795cd732917f152fd8de406947515e01',
        'type' => 'library',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(),
        'dev' => true,
    ),
    'versions' => array(
        'civicrm/formprotection' => array(
            'pretty_version' => 'dev-main',
            'version' => 'dev-main',
            'reference' => '306fc923795cd732917f152fd8de406947515e01',
            'type' => 'library',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'google/recaptcha' => array(
            'pretty_version' => '1.2.4',
            'version' => '1.2.4.0',
            'reference' => '614f25a9038be4f3f2da7cbfd778dc5b357d2419',
            'type' => 'library',
            'install_path' => __DIR__ . '/../google/recaptcha',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
    ),
);
