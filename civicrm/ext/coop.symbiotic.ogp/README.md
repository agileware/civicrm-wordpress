# ogp

Adds Open Graph Protocol (OGP) tags on PCP pages for Facebook and other sites
where you can share links.

For more information: http://ogp.me/

Once the extension is enabled, it will automatically generate the og:description
tags for Contribution, Event Registration, Profiles and PCP pages (og:title is disabled
for now, since usually provided by the CMS).  For PCP pages, it can also
display the PCP logo.

There are settings CiviCRM > Administer > Customize Data and Screens > OGP, where you
can configure a fallback site logo, fallback PCP page logo, etc. For example, Facebook
requires that images be minimum 200x200 pixels.

This extension is licensed under [AGPL-3.0](LICENSE.txt).

## Requirements

* PHP v7.0+
* CiviCRM 5.49 or later

## Installation

Install as a regular CiviCRM extension.

## Usage

* Event Registration/Event Info Page
  * The event 'summary' field (on the first event settings page) is used for the og:description tag.
  * The first image found in the event 'intro text' (from the online registration settings) is used for the og:image tag.
  * If no image is found, the site logo (from ogp settings) is used for the og:image tag.
* Contribution pages
  * The first image found in the 'intro text' (from the Title and Settings tab) is used for the og:image tag.
  * If no image is found, the site logo (from ogp settings) is used for the og:image tag.
* Profiles
  * If the first field of a profile is a markup/html field, and it includes an image, that image will be used.
  * If no image found, fallsback on the site logo.
  * If a profile description is found, it is used as the og:description (nb: this could change in the future, since the description is usually meant to be administrative).

## Roadmap

* Generate tags on Contribution/Event "Thank You" pages, which, if shared
  as-is, will point to invalid CiviCRM pages because the URL will not include
  the contribution/event ID.
* Better on-screen documentation? Which fields map to which tags?

Please consider funding these improvements if they can be useful for you. Each items takes at most 5h of work.

## Support

Community issue tracking:
https://lab.civicrm.org/extensions/ogp/issues

Support also available from Coop Symbiotic:
https://www.symbiotic.coop/en

Coop Symbiotic is a worker-owned co-operative based in Canada. We have a strong
experience working with non-profits and CiviCRM. We provide affordable, fast,
turn-key hosting with regular upgrades and proactive monitoring, as well as custom
development and training.
