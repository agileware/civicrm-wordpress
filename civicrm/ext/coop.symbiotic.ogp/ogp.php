<?php

require_once 'ogp.civix.php';
use CRM_Ogp_ExtensionUtil as E;

/**
 * Implements hook_civicrm_config().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_config
 */
function ogp_civicrm_config(&$config) {
  _ogp_civix_civicrm_config($config);
}

/**
 * Implements hook_civicrm_install().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_install
 */
function ogp_civicrm_install() {
  _ogp_civix_civicrm_install();
}

/**
 * Implements hook_civicrm_enable().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_enable
 */
function ogp_civicrm_enable() {
  _ogp_civix_civicrm_enable();
}

function ogp_civicrm_buildForm($formName, &$form) {
  $tags = [];

  if ($formName == 'CRM_Contribute_Form_Contribution_Main') {
    $tags['title'] = htmlspecialchars($form->_values['title']);

    // og:description
    $t = $form->_values['intro_text'];
    $t = strip_tags($t);
    $t = htmlspecialchars($t);
    $tags['description'] = $t;

    $intro = $form->_values['intro_text'];
    if (preg_match('/<img.+src=[\'"](?P<src>.+?)[\'"].*>/i', $intro, $matches)) {
      $tags['image'] = $matches['src'];
    }
    else {
      $tags['image'] = CRM_Ogp_Utils::getSiteLogo();
    }

    CRM_Ogp_Utils::addMetatags($tags);
  }
  elseif ($formName == 'CRM_Event_Form_Registration_Register') {
    $smarty = CRM_Core_Smarty::singleton();
    $tags['title'] = htmlspecialchars($smarty->get_template_vars()['event']['title']);

    // og:description
    $t = $smarty->get_template_vars()['event']['summary'];
    $t = strip_tags($t);
    $t = htmlspecialchars($t);
    $tags['description'] = $t;

    // og:image
    // Extract the first image from the intro text (text displayed on the registration form)
    // Based on: https://stackoverflow.com/a/7480031
    $intro = $smarty->get_template_vars()['event']['intro_text'];

    if (preg_match('/<img.+src=[\'"](?P<src>.+?)[\'"].*>/i', $intro, $matches)) {
      $tags['image'] = $matches['src'];
    }
    else {
      $tags['image'] = CRM_Ogp_Utils::getSiteLogo();
    }

    CRM_Ogp_Utils::addMetatags($tags);
  }
  elseif ($formName == 'CRM_Profile_Form_Edit') {
    $profile_id = $form->getVar('_gid');
    $smarty = CRM_Core_Smarty::singleton();

    // og:title
    // Find the public profile title from the database
    $result = civicrm_api3('UFGroup', 'get', [
      'id' => $profile_id,
      'sequential' => 1,
    ]);

    if (!empty($result['values'][0]['frontend_title'])) {
      $tags['title'] = htmlspecialchars($result['values'][0]['frontend_title']);
    }

    // @todo Might not be the best option, since it is historically internal?
    if (!empty($result['values'][0]['description'])) {
      $t = $result['values'][0]['description'];
      $t = strip_tags($t);
      $t = htmlspecialchars($t);
      $tags['description'] = $t;
    }

    // og:image
    // Check if there is an HTML formatting field as the field profile field, and if so, check for an image
    $keys = array_keys($smarty->get_template_vars()['fields'] ?? []);
    $first_field = $smarty->get_template_vars()['fields'][$keys[0]];

    if ($first_field['field_type'] == 'Formatting') {
      $html = $first_field['help_pre'] ?? '';
      if (preg_match('/<img.+src=[\'"](?P<src>.+?)[\'"].*>/i', $html, $matches)) {
        $tags['image'] = $matches['src'];
      }
    }

    if (empty($tags['image'])) {
      $tags['image'] = CRM_Ogp_Utils::getSiteLogo();
    }

    CRM_Ogp_Utils::addMetatags($tags);
  }
  else if ($formName == 'CRM_Campaign_Form_Petition_Signature') {
    $tags['title'] = htmlspecialchars($form->petition['title']);

    // og:description
    $t = $form->petition['instructions'];
    $t = strip_tags($t);
    $t = htmlspecialchars($t);
    $tags['description'] = $t;

    $intro = $form->petition['instructions'];
    if (preg_match('/<img.+src=[\'"](?P<src>.+?)[\'"].*>/i', $intro, $matches)) {
      $tags['image'] = $matches['src'];
    }
    else {
      $tags['image'] = CRM_Ogp_Utils::getSiteLogo();
    }
    CRM_Ogp_Utils::addMetatags($tags);
  }
}

function ogp_civicrm_pageRun(&$page) {
  $pageName = get_class($page);
  if ($pageName == 'CRM_PCP_Page_PCPInfo') {
    $id = $page->get('id');

    $dao = new CRM_PCP_DAO_PCP();
    $dao->id = $id;

    // @todo Cleanup this function to use CRM_Ogp_Utils::addMetatags().
    if ($dao->find(true)) {
      $description = $dao->intro_text;
      $description = strip_tags($description);
      $description = htmlspecialchars($description);

      // Provided by the CMS?
      // CRM_Utils_System::addHTMLHead('<meta property="og:title" content="' . $dao->title . '">');

      CRM_Utils_System::addHTMLHead('<meta property="og:description" content="' . $description . '">');

      // Add the PCP image
      $entityFile = CRM_Core_BAO_File::getEntityFile('civicrm_pcp', $id);

      if (!empty($entityFile)) {
        $fileInfo = reset($entityFile);
        $fileId = $fileInfo['fileID'];
        $fileHash = CRM_Core_BAO_File::generateFileHash($id, $fileId);

        // Using UF function adds language prefix and htmlencodes parameters.. breaks Facebook
        $image = CRM_Utils_System::url('civicrm/file', "reset=1&id=$fileId&eid={$id}&fcs={$fileHash}", TRUE);

        // adds: <meta property="og:image" content="http://[...]" />
        CRM_Utils_System::addHTMLHead('<meta property="og:image" content="' . $image . '">');
      }
      elseif ($image = CRM_Ogp_Utils::getPcpDefaultLogo($dao->pcp_block_id)) {
        CRM_Utils_System::addHTMLHead('<meta property="og:image" content="' . $image . '">');
      }
      elseif ($image = CRM_Ogp_Utils::getSiteLogo()) {
        CRM_Utils_System::addHTMLHead('<meta property="og:image" content="' . $image . '">');
      }
    }
  }
  elseif ($pageName == 'CRM_Event_Page_EventInfo') {
    $eventId = $page->getVar('_id');
    if ($eventId > 0) {
      $event = civicrm_api3('Event', 'getsingle', [
        'id' => $eventId,
      ]);
      $tags['title'] = htmlspecialchars($event['title']);
      $summary = strip_tags($event['summary']);
      $summary = htmlspecialchars($summary);
      $tags['description'] = $summary;

      if (preg_match('/<img.+src=[\'"](?P<src>.+?)[\'"].*>/i', $event['description'], $matches)) {
        $tags['image'] = $matches['src'];
      }
      else {
        $tags['image'] = CRM_Ogp_Utils::getSiteLogo();
      }
      CRM_Ogp_Utils::addMetatags($tags);
    }
  }
}

/**
 * Implements hook_civicrm_alterContent().
 *
 * @todo Depends on a patch that needs to be submitted upstream.
 */
function ogp_civicrm_alterContent(&$content, $context, $tplName, &$object) {
  if ($context == 'page' && $tplName == 'CRM/Mailing/Page/View.tpl') {
    $mailing = $object->getVar('_mailing');

    $tags = [];
    $tags['title'] = htmlspecialchars($mailing->subject);

    // Look for a full-width Mosaico image
    if (preg_match('/img[^>]*class="mobile-full"[^>]*src="([^"]+)"/', $content, $matches)) {
      $tags['image'] = $matches[1];
    }

    $headers = CRM_Ogp_Utils::getMetaTagsHTML($tags);
    $html = implode('', $headers);

    $content = preg_replace('#</head>#', $html . '</head>', $content);
  }
}

/**
 * Implements hook_civicrm_navigationMenu().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_navigationMenu/
 */
function ogp_civicrm_navigationMenu(&$menu) {
  _ogp_civix_insert_navigation_menu($menu, 'Administer/Customize Data and Screens', [
    'label' => E::ts('Open Graph Protocol (ogp)'),
    'name' => 'ogp_settings',
    'url' => 'civicrm/admin/setting/ogp',
    'permission' => 'administer CiviCRM',
    'operator' => 'OR',
    'separator' => 0,
  ]);
  _ogp_civix_navigationMenu($menu);
}
