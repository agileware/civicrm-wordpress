<?php

class CRM_Ogp_Utils {

  /**
   * Returns the default site logo, as configured in the settings.
   */
  public static function getSiteLogo() {
    if (CRM_Core_I18n::isMultilingual()) {
      $tsLocale = CRM_Core_I18n::getLocale();
      return Civi::settings()->get('ogp_sitelogo_' . $tsLocale);
    }

    return Civi::settings()->get('ogp_sitelogo');
  }

  /**
   * Returns the default PCP page logo, as configured in the settings.
   * Note that we use the block_id, since that is unique to both the
   * contribution and event pages.
   */
  public static function getPcpDefaultLogo($block_id) {
    if (CRM_Core_I18n::isMultilingual()) {
      $tsLocale = CRM_Core_I18n::getLocale();
      return Civi::settings()->get('ogp_pcp_' . $block_id . '_' . $tsLocale);
    }

    return Civi::settings()->get('ogp_pcp_' . $block_id);
  }

  /**
   * Returns an array of HTML headers. Used directly when we alter the $content directly
   * instead of calling addHTMLHead().
   */
  public static function getMetaTagsHTML($tags) {
    $html = [];

    $html[] = '<meta property="og:type" content="website">';

    // ogp:title might already be set by the CMS
    if (!Civi::settings()->get('ogp_title') && !empty($tags['title'])) {
      unset($tags['title']);
    }

    foreach ($tags as $key => $val) {
      $html[] = '<meta property="og:' . $key . '" content="' . $val . '">';
    }

    // Add twitter tags, if enabled
    if ($twitter_handle = Civi::settings()->get('ogp_twitter')) {
      $html[] = '<meta property="twitter:card" content="summary_large_image">';
      $html[] = '<meta property="twitter:site" content="' . $twitter_handle . '">';
      $html[] = '<meta property="twitter:creator" content="' . $twitter_handle . '">';

      // Twitter also uses 'title', 'description', 'image'
      foreach ($tags as $key => $val) {
        $html[] = '<meta property="twitter:' . $key . '" content="' . $val . '">';
      }
    }

    // @todo add fb:app_id ?

    return $html;
  }

  /**
   *
   */
  public static function addMetatags($tags) {
    $html = self::getMetaTagsHTML($tags);

    foreach($html as $header) {
      CRM_Utils_System::addHTMLHead($header);
    }
  }

}
