<?php
use CRM_Membershipreport_ExtensionUtil as E;

class CRM_Membershipreport_Page_ReportAudit extends CRM_Core_Page {

  public function run() {
    CRM_Utils_System::setTitle(E::ts('Report Audit'));

    $className = CRM_Utils_Request::retrieveValue('class', 'String');
    $function = CRM_Utils_Request::retrieveValue('function', 'String');
    $params = CRM_Utils_Request::retrieveValue('params', 'String');

    // Minor security checks
    if (substr($className, 0, 4) != 'CRM_') {
      throw new Exception('Not a CRM class');
    }
    if (strpos($className, '_Report_') === FALSE) {
      throw new Exception('Not a Report class');
    }
    if (strpos($className, '_Form_') === FALSE) {
      throw new Exception('Not a Form class');
    }

    // Fetch the rows
    $params = json_decode($params, true);
    $params['reportaudit'] = true;

    $report = new $className();
    $rows = $report->{$function}($params);

    if (!empty($rows)) {
      // Extract the headers
      $headers = array_keys($rows[0]);

      // Check if the report provides human-readable headers
      if (method_exists($report, 'getAuditHeaders')) {
        $headers = $report->getAuditHeaders($function, $params);
      }

      // Provide table alignment hints depending on the data
      // This is not very reliable, but cheap/easy/helpful.
      $align = [];

      foreach ($rows[0] as $key => $val) {
        $align[$key] = $this->getAlign($val);
      }

      // Handle result reformatting (i.e. link to URLs)
      foreach ($rows as $cpt => &$row) {
        foreach ($row as $key => &$val) {
          if (!empty($row[$key . '.display'])) {
            $val = $row[$key . '.display'];
            unset($row[$key . '.display']);

            // Update the alignment
            // ex: if it was a Contact ID but now displays the Contact Name.
            if ($cpt == 0) {
              $align[$key] = $this->getAlign($val);
            }
          }

          if (!empty($row[$key . '.url.blank'])) {
            $url = $row[$key . '.url.blank'];
            $val = "<a href='$url' target='_blank'>" . $val . '</a>';
            unset($row[$key . '.url.blank']);
          }
          elseif (!empty($row[$key . '.url.popup'])) {
            $url = $row[$key . '.url.popup'];
            $val = "<a href='$url' class='crm-popup'>" . $val . '</a>';
            unset($row[$key . '.url.popup']);
          }
        }
      }

      $this->assign('reportaudit_headers', $headers);
      $this->assign('reportaudit_rows', $rows);
      $this->assign('reportaudit_align', $align);
    }

    parent::run();
  }

  /**
   * Returns the recommended alignment for the value.
   */
  private function getAlign($val) {
    if (is_numeric($val)) {
      return 'right';
    }
    elseif (preg_match('/^\d{4}-\d{2}-\d{2}$/', $val)) {
      return 'center';
    }
    else {
      return 'left';
    }
  }

}
