<?php

use CRM_Membershipreport_ExtensionUtil as E;

// This file declares a managed database record of type "ReportTemplate".
// The record will be automatically inserted, updated, or deleted from the
// database as appropriate. For more details, see "hook_civicrm_managed" at:
return [
  0 => [
    'name' => 'CRM_Membershipreport_Form_Report_MembershipRenewal',
    'entity' => 'ReportTemplate',
    'params' => [
      'version' => 3,
      'label' => E::ts('Membership Renewal Report'),
      'description' => E::ts('Membership Renewal Report'),
      'class_name' => 'CRM_Membershipreport_Form_Report_MembershipRenewal',
      'report_url' => 'membership-renewal',
      'component' => 'CiviMember',
    ],
  ],
];
