(function(angular, $, _) {

  angular.module('inferredtags').config(function($routeProvider) {
      $routeProvider.when('/inferredtags', {
        controller: 'InferredtagsInferredTags',
        templateUrl: '~/inferredtags/InferredTags.html',

        // If you need to look up data when opening the page, list it out
        // under "resolve".
        resolve: {
          myContact: function(crmApi) {
            return crmApi('Contact', 'getsingle', {
              id: 'user_contact_id',
              return: ['first_name', 'last_name']
            });
          }
        }
      });
    }
  );

  // The controller uses *injection*. This default injects a few things:
  //   $scope -- This is the set of variables shared between JS and HTML.
  //   crmApi, crmStatus, crmUiHelp -- These are services provided by civicrm-core.
  //   myContact -- The current contact, defined above in config().
  angular.module('inferredtags').controller('InferredtagsInferredTags', function($scope, crmApi, crmStatus, crmUiHelp, myContact) {
    // The ts() and hs() functions help load strings for this module.
    var ts = $scope.ts = CRM.ts('inferredtags');
    var hs = $scope.hs = crmUiHelp({file: 'CRM/inferredtags/InferredTags'}); // See: templates/CRM/inferredtags/InferredTags.hlp


    $scope.state = 'loading';
    $scope.config = {tagsets: {}};
    $scope.mappedConfig = {};

    crmStatus(
        // Status messages. For defaults, just use "{}"
        {start: ts('Loading...'), success: ts('Ready')},
        // The save action. Note that crmApi() returns a promise.
        crmApi(
          [
            [ 'Setting', 'getvalue', { name: 'inferredtags' }],
            [ 'RelationshipType', 'get', { is_active: 1, options: {limit: 10000}, sequential: 1 } ],
            [ 'Tag', 'get', { is_tagset: 1, options: {limit: 10000}, sequential: 1 } ]
          ])
        .then(r => {
          if (!r[0]) {
            // No settings yet.
            r[0] = '{"tagsets": {}}';
          }
          $scope.config = JSON.parse(r[0]);
          $scope.relTypes = r[1].values.map(reltype => { reltype.id = parseInt(reltype.id); return reltype; });
          $scope.tagsets = r[2].values;

          // Create mappedConfig
          $scope.mappedConfig = {};


          $scope.tagsets.forEach(tagset => {

            const inUse = tagset.name in $scope.config.tagsets;
            const tagsetConfig = inUse ? $scope.config.tagsets[tagset.name] : {};

            $scope.mappedConfig[tagset.name] = {
              aSide: {},
              bSide: {},
              inUse,
              autoInfer: inUse ? ((tagsetConfig.autoInfer === undefined) ? true : tagsetConfig.autoInfer) : true,
              includeExpired: inUse ? ((tagsetConfig.includeExpired === undefined) ? false : tagsetConfig.includeExpired) : false,
              includeInactive: inUse ? ((tagsetConfig.includeInactive === undefined) ? false : tagsetConfig.includeInactive) : false,
            };

            const configured = ($scope.config.tagsets[tagset.name] || {aSide: [], bSide: []});

            var m = $scope.mappedConfig[tagset.name].aSide;
            $scope.relTypes.forEach(reltype => {
              m[reltype.id] = configured.aSide.indexOf(reltype.id) > -1;
            });

            m = $scope.mappedConfig[tagset.name].bSide;
            $scope.relTypes.forEach(reltype => {
              m[reltype.id] = configured.bSide.indexOf(reltype.id) > -1;
            });
          });
          console.log('config' , $scope.config);
          console.log('mapped 2' , $scope.mappedConfig);
          $scope.state = 'loaded';
        })
      );

    $scope.dirty = false;
    $scope.setDirty = function() {
      $scope.dirty = true;
    };
    $scope.save = function save() {

      // Create config from mapped config.
      const config = $scope.config;
      const mc = $scope.mappedConfig;
      $scope.tagsets.forEach(tagset => {
        if (mc[tagset.name].inUse) {
          if (! (tagset.name in config.tagsets)) {
            config.tagsets[tagset.name] = {};
          }

          ['autoInfer', 'includeExpired', 'includeInactive'].forEach(prop => {
            config.tagsets[tagset.name][prop] = mc[tagset.name][prop];
          });

          config.tagsets[tagset.name].aSide = [];
          config.tagsets[tagset.name].bSide = [];
          $scope.relTypes.forEach(reltype => {
            if (mc[tagset.name].aSide[reltype.id]) {
              config.tagsets[tagset.name].aSide.push(reltype.id);
            }
            if (mc[tagset.name].bSide[reltype.id]) {
              config.tagsets[tagset.name].bSide.push(reltype.id);
            }
          });
        }
        else {
          delete config.tagsets[tagset.name];
        }
      });

      console.log("Saving config", $scope.config);

      return crmStatus(
        // Status messages. For defaults, just use "{}"
        {start: ts('Saving...'), success: ts('Saved')},
        // The save action. Note that crmApi() returns a promise.
        crmApi('Setting', 'create', {
          inferredtags: JSON.stringify(config)
        })
      );
    };
  });

})(angular, CRM.$, CRM._);
