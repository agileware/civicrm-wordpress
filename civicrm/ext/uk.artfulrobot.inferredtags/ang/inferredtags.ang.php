<?php
// This file declares an Angular module which can be autoloaded
// in CiviCRM. See also:
// \https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_angularModules/n
return array (
  'js' => 
  array (
    0 => 'ang/inferredtags.js',
    1 => 'ang/inferredtags/*.js',
    2 => 'ang/inferredtags/*/*.js',
  ),
  'css' => 
  array (
    0 => 'ang/inferredtags.css',
  ),
  'partials' => 
  array (
    0 => 'ang/inferredtags',
  ),
  'requires' => 
  array (
    0 => 'crmUi',
    1 => 'crmUtil',
    2 => 'ngRoute',
  ),
  'settings' => 
  array (
  ),
);
