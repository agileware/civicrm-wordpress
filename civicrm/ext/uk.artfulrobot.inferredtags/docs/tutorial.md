# Tutorial

This is a bit brief, but should get you started:

1. Create a tagset called Region.

2. Create tags within this, e.g. North, South. **Also create a tag called
   _Inferred_**

3. Add one of your region tags, e.g. *North* to a contact that has
   relationships. e.g. choose an organisation.

4. Now configure the Inferred tags. Go to **Administer » Inferred Tags**

5. Open up your *Region* tagset and check the checkbox that says *Use
   inferred tags with this tagset*.

6. Select the relationship(s) that you want to use for the inferring. e.g.
   if you are putting tags on organisations that you want to infer onto
   employees, you'd choose *Employee of* but not *Employer of*.

7. Click **Save** (it's at the top.)

8. Wait for it to run. No, of course you don't want to do that, you want
   to make it do it *now*, so head over to **Administer » System Settings
   » Scheduled Jobs**, find the **Update Inferred Tags** job and choose
   **Execute**.

9. Visit a suitably related contact - you should see two tags now: the one
   you placed on the organisation in step (3), and the *Inferred* tag,
   which means that those tags have been automatically Inferred, and will
   inherit any updates.
