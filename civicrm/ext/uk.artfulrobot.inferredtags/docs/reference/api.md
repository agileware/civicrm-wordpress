# Inferred Tags API

## Job.UpdateinferredTags

Parameters:

- `tagset_name` (string) The name of the tagset you want to update. If
  ommitted then all configured tagsets are updated.

Returns an array of performance data, showing how long the various stages
took (and how many iterations there were).
