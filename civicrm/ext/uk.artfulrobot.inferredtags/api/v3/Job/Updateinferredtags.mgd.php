<?php
// This file declares a managed database record of type "Job".
// The record will be automatically inserted, updated, or deleted from the
// database as appropriate. For more details, see "hook_civicrm_managed"
return array(
  0 =>
  array(
    'name' => 'Cron:Job.Updateinferredtags',
    'entity' => 'Job',
    'params' =>
    array(
      'version' => 3,
      'name' => 'Update inferred tags',
      'description' => 'Recreate all inferred tags.',
      'run_frequency' => 'Daily',
      'api_entity' => 'Job',
      'api_action' => 'Updateinferredtags',
      'parameters' => '',
    ),
  ),
);
