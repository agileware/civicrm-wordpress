<?php
return [
  'pelf_config' => [
    'name'        => 'inferredtags',
    'title'       => ts('Inferred Tags Configuration'),
    'description' => ts('JSON encoded settings.'),
    'group_name'  => 'domain',
    'type'        => 'String',
    'default'     => FALSE,
    'add'         => '5.28',
    'is_domain'   => 1,
    'is_contact'  => 0,
  ]
];
