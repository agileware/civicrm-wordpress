<?php

require_once 'inferredtags.civix.php';
use CRM_Inferredtags_ExtensionUtil as E;

/**
 * Implements hook_civicrm_config().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_config/ 
 */
function inferredtags_civicrm_config(&$config) {
  _inferredtags_civix_civicrm_config($config);
}

/**
 * Implements hook_civicrm_xmlMenu().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_xmlMenu
 */
function inferredtags_civicrm_xmlMenu(&$files) {
  _inferredtags_civix_civicrm_xmlMenu($files);
}

/**
 * Implements hook_civicrm_install().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_install
 */
function inferredtags_civicrm_install() {
  _inferredtags_civix_civicrm_install();
}

/**
 * Implements hook_civicrm_postInstall().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_postInstall
 */
function inferredtags_civicrm_postInstall() {
  _inferredtags_civix_civicrm_postInstall();
}

/**
 * Implements hook_civicrm_uninstall().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_uninstall
 */
function inferredtags_civicrm_uninstall() {
  _inferredtags_civix_civicrm_uninstall();
}

/**
 * Implements hook_civicrm_enable().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_enable
 */
function inferredtags_civicrm_enable() {
  _inferredtags_civix_civicrm_enable();
}

/**
 * Implements hook_civicrm_disable().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_disable
 */
function inferredtags_civicrm_disable() {
  _inferredtags_civix_civicrm_disable();
}

/**
 * Implements hook_civicrm_upgrade().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_upgrade
 */
function inferredtags_civicrm_upgrade($op, CRM_Queue_Queue $queue = NULL) {
  return _inferredtags_civix_civicrm_upgrade($op, $queue);
}

/**
 * Implements hook_civicrm_managed().
 *
 * Generate a list of entities to create/deactivate/delete when this module
 * is installed, disabled, uninstalled.
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_managed
 */
function inferredtags_civicrm_managed(&$entities) {
  _inferredtags_civix_civicrm_managed($entities);
}

/**
 * Implements hook_civicrm_caseTypes().
 *
 * Generate a list of case-types.
 *
 * Note: This hook only runs in CiviCRM 4.4+.
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_caseTypes
 */
function inferredtags_civicrm_caseTypes(&$caseTypes) {
  _inferredtags_civix_civicrm_caseTypes($caseTypes);
}

/**
 * Implements hook_civicrm_angularModules().
 *
 * Generate a list of Angular modules.
 *
 * Note: This hook only runs in CiviCRM 4.5+. It may
 * use features only available in v4.6+.
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_angularModules
 */
function inferredtags_civicrm_angularModules(&$angularModules) {
  _inferredtags_civix_civicrm_angularModules($angularModules);
}

/**
 * Implements hook_civicrm_alterSettingsFolders().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_alterSettingsFolders
 */
function inferredtags_civicrm_alterSettingsFolders(&$metaDataFolders = NULL) {
  _inferredtags_civix_civicrm_alterSettingsFolders($metaDataFolders);
}

/**
 * Implements hook_civicrm_entityTypes().
 *
 * Declare entity types provided by this module.
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_entityTypes
 */
function inferredtags_civicrm_entityTypes(&$entityTypes) {
  _inferredtags_civix_civicrm_entityTypes($entityTypes);
}

/**
 * Implements hook_civicrm_thems().
 */
function inferredtags_civicrm_themes(&$themes) {
  _inferredtags_civix_civicrm_themes($themes);
}

/**
 * Implements hook_civicrm_navigationMenu().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_navigationMenu
 */
function inferredtags_civicrm_navigationMenu(&$menu) {
  _inferredtags_civix_insert_navigation_menu($menu, 'Administer', array(
    'label' => E::ts('Inferred Tags'),
    'name' => 'inferred_tags',
    'url' => 'civicrm/a#/inferredtags',
    'permission' => 'administer Tagsets',
    'operator' => 'OR',
    'separator' => 0,
  ));
  _inferredtags_civix_navigationMenu($menu);
} // */
/**
 * Implements hook_civicrm_alterAPIPermissions().
 *
 * Specify permissions for API calls required in ang
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_alterAPIPermissions/
 */
function inferredtags_civicrm_alterAPIPermissions($entity, $action, &$params, &$permissions) {

  $permDefaults = ['administer Tagsets'];

  if ($entity === 'setting') {

    if (($params['name'] ?? '') === 'inferredtags') {
      $permissions['setting']['getvalue'] = $permDefaults;
    }

    // Check 'inferredtags' is the *only* setting to "create" in the parameters for the api call before granting access.
    if (array_diff ( array_keys($params), ['check_permissions', 'prettyprint', 'version'] ) === ['inferredtags'] ) {
      $permissions['setting']['create'] = $permDefaults;
    }

  }
  elseif ($entity === 'Job') {
    $permissions['job']['updateinferredtags'] = $permDefaults;
  }


}
