<?php
namespace Civi\Mailgunny;

use Civi;

class Config {

  /**
   * Get API key for Mailgun account.
   *
   * @return string
   */
  public static function getApiKey() {
    if (defined('MAILGUN_API_KEY')) {
      return MAILGUN_API_KEY;
    }
    return Civi::settings()->get('mailgun_api_key');
  }

  /**
   * Get API endpoint
   *
   * @return string
   */
  public static function getApiEndpoint() {
    $url = rtrim(Civi::settings()->get('mailgun_api_endpoint'), '/') . '/';
    if ($url === '/') {
      // Provide a default if missing. Not sure this is really necessary.
      $url = 'https://api.eu.mailgun.net/v3/';
    }
    return $url;
  }

  public static function getMailDomain() {
    if (!isset(Civi::$statics[__FUNCTION__])) {
      Civi::$statics[__FUNCTION__] = \Civi\Api4\MailSettings::get(FALSE)
      ->addSelect('domain')
      ->addWhere('is_default', '=', TRUE)
      ->execute()->first()['domain'];
    }
    return Civi::$statics[__FUNCTION__];
  }

}
